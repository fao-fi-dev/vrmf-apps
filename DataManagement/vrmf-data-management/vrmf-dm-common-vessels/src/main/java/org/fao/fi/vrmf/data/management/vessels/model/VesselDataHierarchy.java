/**
 * 
 */
package org.fao.fi.vrmf.data.management.vessels.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.vrmf.data.management.common.model.AbstractManagedDataHierarchy;

/**
 * @author Fiorellato
 *
 */
@XmlRootElement(name="VesselDataHierarchy")
@XmlAccessorType(XmlAccessType.FIELD)
public class VesselDataHierarchy extends AbstractManagedDataHierarchy<VesselData, VesselDataHierarchy> {
	private static final long serialVersionUID = -1568191382963114855L;
	
	public VesselDataHierarchy() {
		super();
	}
	
	public VesselDataHierarchy(VesselData data) {
		super(data);
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(VesselDataHierarchy o) {
		return this._data.getUpdateDate().compareTo(o._data.getUpdateDate());
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this._children == null) ? 0 : this._children.hashCode());
		result = prime * result + ((this._data == null) ? 0 : this._data.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VesselDataHierarchy other = (VesselDataHierarchy) obj;
		if (this._children == null) {
			if (other._children != null)
				return false;
		} else if (!this._children.equals(other._children))
			return false;
		if (this._data == null) {
			if (other._data != null)
				return false;
		} else if (!this._data.equals(other._data))
			return false;
		return true;
	}
}
