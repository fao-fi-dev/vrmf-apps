/**
 * 
 */
package org.fao.fi.vrmf.data.management.vessels.spi;

import java.util.Collection;

import org.fao.fi.vrmf.data.management.common.spi.DataManagementOperationsProvider;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.DataManagementOperationException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.UnmanagedException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.DifferentSourcesException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemHasMappingsException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemNotFoundException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.MultipleItemsFoundException;
import org.fao.fi.vrmf.data.management.vessels.model.MultipleIMOByUIDCheckResult;
import org.fao.fi.vrmf.data.management.vessels.model.MultipleUIDByIMOCheckResult;
import org.fao.fi.vrmf.data.management.vessels.model.VesselData;
import org.fao.fi.vrmf.data.management.vessels.model.VesselDataHierarchy;

/**
 * Method definitions for operations provided either by a local or by a remote implementation of the underlying business logic.
 *  
 * @author Fiorellato
 */
public interface VesselsDataManagementOperationsProvider extends DataManagementOperationsProvider<VesselData, VesselDataHierarchy> {
	/**
	 * @param source a vessel source
	 * @param identifier a vessel source identifier (primary)
	 * @return the VRMF ID for the vessel having the provided identifier as <b>latest</b> source primary identifier
	 * @throws ItemNotFoundException if no vessel can be identified by vessel source and vessel source primary identifier
	 * @throws MultipleItemsFoundException if more than one vessel can be identified by vessel source and vessel source latest primary identifier
	 * @throws UnmanagedException if anything else goes wrong
	 */
	int getVesselIDByCurrentIdentifier(String source, String identifier) throws ItemNotFoundException, MultipleItemsFoundException, UnmanagedException, DataManagementOperationException;

	/**
	 * @param source a vessel source
	 * @param identifier a vessel source identifier (primary)
	 * @return the VRMF ID for all vessels having the provided identifier as either <b>latest</b> or <b>historical</b> vessel source primary identifier
	 * @throws ItemNotFoundException if no vessel can be identified by vessel source and vessel source primary identifier
	 * @throws UnmanagedException if anything else goes wrong
	 */
	int[] getVesselIDByHistoricalIdentifier(String source, String identifier) throws ItemNotFoundException, UnmanagedException, DataManagementOperationException;

	/**
	 * Merges data from a source vessel into a target vessel, removing the source vessel from the registry when possible
	 * @param sourceVesselId the source vessel internal identifier
	 * @param targetVesselId the target vessel internal identifier
	 * @throws ItemNotFoundException if no vessel can be identified either by source vessel internal identifier or by target vessel internal identifier
	 * @throws ItemHasMappingsException if the source vessel is already mapped to another target vessel
	 * @throws DifferentSourcesException if the vessels identified by the source and target internal identifier belong to different sources
	 * @throws UnmanagedException if anything else goes wrong
	 */
	void mergeVessels(int sourceVesselId, int targetVesselId) throws ItemNotFoundException, ItemHasMappingsException, DifferentSourcesException, UnmanagedException, DataManagementOperationException;
	
	Collection<MultipleIMOByUIDCheckResult> checkMultipleIMOByUID(boolean historical) throws UnmanagedException, DataManagementOperationException;

	Collection<MultipleUIDByIMOCheckResult> checkMultipleUIDByIMO(boolean historical) throws UnmanagedException, DataManagementOperationException;
}