/**
 * 
 */
package org.fao.fi.vrmf.data.management.vessels.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.vrmf.data.management.common.model.AbstractManagedData;

/**
 * @author Fiorellato
 *
 */
@XmlRootElement(name="VesselData")
@XmlAccessorType(XmlAccessType.FIELD)
public class VesselData extends AbstractManagedData<VesselData> {
	private static final long serialVersionUID = -462569084004234894L;
	
	@XmlElement(name="SourceIdentifier")
	private String _identifier;
	
	@XmlElement(name="SourceAlternateIdentifier")
	private String _alternateIdentifier;

	@XmlElement(name="Name")
	private String _name;
	
	@XmlElement(name="Flag")
	private String _flag;
	
	@XmlElement(name="IMO")
	private String _imo;
	
	@XmlElement(name="IRCS")
	private String _ircs;
	
	@XmlElement(name="NRN")
	private String _nrn;
	
	@XmlElement(name="LOA")
	private Double _loa;
	
	@XmlElement(name="GT")
	private Double _gt;
	
	@XmlElementWrapper(name="Authorizations")
	@XmlElement(name="AuthorizationData")
	private Collection<AuthorizationData> _authorizations;
		
	public VesselData() {
		super();
		
		this._authorizations = new ArrayList<AuthorizationData>();
	}

	public VesselData(int id, int uid, String source, Date update, 
					  String identifier, String alternateIdentifier, 
					  String name, String flag, String imo, String ircs, String nrn, Double loa, Double gt, 
					  Collection<AuthorizationData> authorizations, 
					  Integer mapsTo, Double mappingScore, Date mappingDate, String mappingUser, String mappingComment) {
		super();
		this._id = id;
		this._uid = uid;
		this._sourceSystem = source;
		this._updateDate = update;
		this._identifier = identifier;
		this._alternateIdentifier = alternateIdentifier;
		this._name = name;
		this._flag = flag;
		this._imo = imo;
		this._ircs = ircs;
		this._nrn = nrn;
		this._loa = loa;
		this._gt = gt;
		this._authorizations = authorizations;
		this._mapsTo = mapsTo;
		this._mappingWeight = mappingScore;
		this._mappingDate = mappingDate;
		this._mappingUser = mappingUser;
		this._mappingComment = mappingComment;
	}

	/**
	 * @return the identifier
	 */
	public String getIdentifier() {
		return this._identifier;
	}

	/**
	 * @param identifier the identifier to set
	 */
	public void setIdentifier(String identifier) {
		this._identifier = identifier;
	}

	/**
	 * @return the alternateIdentifier
	 */
	public String getAlternateIdentifier() {
		return this._alternateIdentifier;
	}

	/**
	 * @param alternateIdentifier the alternateIdentifier to set
	 */
	public void setAlternateIdentifier(String alternateIdentifier) {
		this._alternateIdentifier = alternateIdentifier;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return this._name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this._name = name;
	}

	/**
	 * @return the flag
	 */
	public String getFlag() {
		return this._flag;
	}

	/**
	 * @param flag the flag to set
	 */
	public void setFlag(String flag) {
		this._flag = flag;
	}

	/**
	 * @return the imo
	 */
	public String getImo() {
		return this._imo;
	}

	/**
	 * @param imo the imo to set
	 */
	public void setImo(String imo) {
		this._imo = imo;
	}

	/**
	 * @return the ircs
	 */
	public String getIrcs() {
		return this._ircs;
	}

	/**
	 * @param ircs the ircs to set
	 */
	public void setIrcs(String ircs) {
		this._ircs = ircs;
	}

	/**
	 * @return the nrn
	 */
	public String getNrn() {
		return this._nrn;
	}

	/**
	 * @param nrn the nrn to set
	 */
	public void setNrn(String nrn) {
		this._nrn = nrn;
	}

	/**
	 * @return the loa
	 */
	public Double getLoa() {
		return this._loa;
	}

	/**
	 * @param loa the loa to set
	 */
	public void setLoa(Double loa) {
		this._loa = loa;
	}

	/**
	 * @return the gt
	 */
	public Double getGt() {
		return this._gt;
	}

	/**
	 * @param gt the gt to set
	 */
	public void setGt(Double gt) {
		this._gt = gt;
	}

	public Collection<AuthorizationData> getAuthorizations() {
		return this._authorizations;
	}

	public void setAuthorizations(Collection<AuthorizationData> authorizations) {
		this._authorizations = authorizations;
	}
	
	public int compareTo(VesselData o) {
		return o == null ? -1 : this._id < o._id ? -1 : this._id == o._id ? 0 : 1;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return 
			"[ ID: " + this._id + " UID: " + this._uid + " ] " +
			"[ SRC: " + this._sourceSystem + " SID: " + this._identifier + " SAID: " + this._alternateIdentifier + " ] " +
			"[ IMO: " + ( this._imo == null ? "<NOT SET>" : this._imo ) + " ] " + 
			"[ UPD: " + this._updateDate + " ] " +
			( this._flag == null ? "<FLAG NOT SET>" : this._flag ) + " " + 
			( this._name == null ? "<NAME NOT SET>" : this._name ) + " " +
			( this._ircs == null ? "<IRCS NOT SET>" : this._ircs ) + " " +
			( this._nrn == null ? "<NRN NOT SET>" : this._nrn ) + " " +
			( this._loa == null ? "<LOA NOT SET>" : this._loa ) + " " +
			( this._gt == null ? "<GT NOT SET>" : this._gt ) + " " +
			( this._authorizations == null || this._authorizations.isEmpty() ? "<AUTHS NOT SET>" : this._authorizations );
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this._alternateIdentifier == null) ? 0 : this._alternateIdentifier.hashCode());
		result = prime * result + ((this._authorizations == null) ? 0 : this._authorizations.hashCode());
		result = prime * result + ((this._flag == null) ? 0 : this._flag.hashCode());
		result = prime * result + ((this._gt == null) ? 0 : this._gt.hashCode());
		result = prime * result + ((this._identifier == null) ? 0 : this._identifier.hashCode());
		result = prime * result + ((this._imo == null) ? 0 : this._imo.hashCode());
		result = prime * result + ((this._ircs == null) ? 0 : this._ircs.hashCode());
		result = prime * result + ((this._loa == null) ? 0 : this._loa.hashCode());
		result = prime * result + ((this._name == null) ? 0 : this._name.hashCode());
		result = prime * result + ((this._nrn == null) ? 0 : this._nrn.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		VesselData other = (VesselData) obj;
		if (this._alternateIdentifier == null) {
			if (other._alternateIdentifier != null)
				return false;
		} else if (!this._alternateIdentifier.equals(other._alternateIdentifier))
			return false;
		if (this._authorizations == null) {
			if (other._authorizations != null)
				return false;
		} else if (!this._authorizations.equals(other._authorizations))
			return false;
		if (this._flag == null) {
			if (other._flag != null)
				return false;
		} else if (!this._flag.equals(other._flag))
			return false;
		if (this._gt == null) {
			if (other._gt != null)
				return false;
		} else if (!this._gt.equals(other._gt))
			return false;
		if (this._identifier == null) {
			if (other._identifier != null)
				return false;
		} else if (!this._identifier.equals(other._identifier))
			return false;
		if (this._imo == null) {
			if (other._imo != null)
				return false;
		} else if (!this._imo.equals(other._imo))
			return false;
		if (this._ircs == null) {
			if (other._ircs != null)
				return false;
		} else if (!this._ircs.equals(other._ircs))
			return false;
		if (this._loa == null) {
			if (other._loa != null)
				return false;
		} else if (!this._loa.equals(other._loa))
			return false;
		if (this._name == null) {
			if (other._name != null)
				return false;
		} else if (!this._name.equals(other._name))
			return false;
		if (this._nrn == null) {
			if (other._nrn != null)
				return false;
		} else if (!this._nrn.equals(other._nrn))
			return false;
		return true;
	}
}
