/**
 * 
 */
package org.fao.fi.vrmf.data.management.vessels.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Fiorellato
 *
 */
@XmlRootElement(name="MultipleIMOByUIDCheckResult")
@XmlAccessorType(XmlAccessType.FIELD)
public class MultipleIMOByUIDCheckResult implements Serializable {
	private static final long serialVersionUID = -5974808652009508803L;

	@XmlAttribute(name="uid")
	private Integer _uid;
	
	@XmlAttribute(name="numberOfDistinctIMOs")
	private Integer _distinctIMOs;
	
	public MultipleIMOByUIDCheckResult() {
	}

	public MultipleIMOByUIDCheckResult(Integer uid, Integer differentImos) {
		super();
		this._uid = uid;
		this._distinctIMOs = differentImos;
	}

	public Integer getUid() {
		return this._uid;
	}

	public void setUid(Integer uid) {
		this._uid = uid;
	}

	public Integer getDistinctIMOs() {
		return this._distinctIMOs;
	}

	public void setDistinctIMOs(Integer distinctIMOs) {
		this._distinctIMOs = distinctIMOs;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this._distinctIMOs == null) ? 0 : this._distinctIMOs.hashCode());
		result = prime * result + ((this._uid == null) ? 0 : this._uid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MultipleIMOByUIDCheckResult other = (MultipleIMOByUIDCheckResult) obj;
		if (this._distinctIMOs == null) {
			if (other._distinctIMOs != null)
				return false;
		} else if (!this._distinctIMOs.equals(other._distinctIMOs))
			return false;
		if (this._uid == null) {
			if (other._uid != null)
				return false;
		} else if (!this._uid.equals(other._uid))
			return false;
		return true;
	}
}
