/**
 * 
 */
package org.fao.fi.vrmf.data.management.vessels.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Fiorellato
 *
 */
@XmlRootElement(name="MultipleUIDByIMOCheckResult")
@XmlAccessorType(XmlAccessType.FIELD)
public class MultipleUIDByIMOCheckResult implements Serializable {
	private static final long serialVersionUID = 7108942817171308577L;
	
	@XmlAttribute(name="imo")
	private String _imo;
	
	@XmlAttribute(name="numberOfDistinctIDs")
	private Integer _distinctIDs;
	
	@XmlAttribute(name="numberOfDistinctUIDs")
	private Integer _distinctUIDs;
	
	public MultipleUIDByIMOCheckResult() {
	}

	public MultipleUIDByIMOCheckResult(String imo, Integer distinctIDs, Integer distinctUIDs) {
		super();
		this._imo = imo;
		this._distinctIDs = distinctIDs;
		this._distinctUIDs = distinctUIDs;
	}

	public String getImo() {
		return this._imo;
	}

	public void setImo(String imo) {
		this._imo = imo;
	}

	public Integer getDistinctIDs() {
		return this._distinctIDs;
	}

	public void setDistinctIDs(Integer distinctIDs) {
		this._distinctIDs = distinctIDs;
	}

	public Integer getDistinctUIDs() {
		return this._distinctUIDs;
	}

	public void setDistinctUIDs(Integer distinctUIDs) {
		this._distinctUIDs = distinctUIDs;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this._distinctIDs == null) ? 0 : this._distinctIDs.hashCode());
		result = prime * result + ((this._distinctUIDs == null) ? 0 : this._distinctUIDs.hashCode());
		result = prime * result + ((this._imo == null) ? 0 : this._imo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MultipleUIDByIMOCheckResult other = (MultipleUIDByIMOCheckResult) obj;
		if (this._distinctIDs == null) {
			if (other._distinctIDs != null)
				return false;
		} else if (!this._distinctIDs.equals(other._distinctIDs))
			return false;
		if (this._distinctUIDs == null) {
			if (other._distinctUIDs != null)
				return false;
		} else if (!this._distinctUIDs.equals(other._distinctUIDs))
			return false;
		if (this._imo == null) {
			if (other._imo != null)
				return false;
		} else if (!this._imo.equals(other._imo))
			return false;
		return true;
	}
}
