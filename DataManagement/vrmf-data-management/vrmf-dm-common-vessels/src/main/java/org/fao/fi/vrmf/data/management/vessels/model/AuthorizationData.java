/**
 * 
 */
package org.fao.fi.vrmf.data.management.vessels.model;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Fiorellato
 *
 */
@XmlRootElement(name="AuthorizationData")
@XmlAccessorType(XmlAccessType.FIELD)
public class AuthorizationData implements Serializable {
	private static final long serialVersionUID = 8960251008063099877L;

	@XmlAttribute(name="typeId")
	private String _typeId;
	
	@XmlElement(name="AuthFrom")
	private Date _authFrom;
	
	@XmlElement(name="AuthTo")
	private Date _authTo;
	
	@XmlElement(name="AuthTerm")
	private Date _authTerm;
	
	@XmlElement(name="AuthTermReasonCode")
	private Integer _authTermReasonCode;
	
	@XmlElement(name="AuthTermReason")
	private String _authTermReason;

	public AuthorizationData() {
		super();
	}

	public AuthorizationData(String typeId, Date authFrom, Date authTo, Date authTerm, Integer authTermReasonCode, String authTermReason) {
		super();
		this._typeId = typeId;
		this._authFrom = authFrom;
		this._authTo = authTo;
		this._authTerm = authTerm;
		this._authTermReasonCode = authTermReasonCode;
		this._authTermReason = authTermReason;
	}

	public String getTypeId() {
		return this._typeId;
	}

	public void setTypeId(String typeId) {
		this._typeId = typeId;
	}

	public Date getAuthFrom() {
		return this._authFrom;
	}

	public void setAuthFrom(Date authFrom) {
		this._authFrom = authFrom;
	}

	public Date getAuthTo() {
		return this._authTo;
	}

	public void setAuthTo(Date authTo) {
		this._authTo = authTo;
	}

	public Date getAuthTerm() {
		return this._authTerm;
	}

	public void setAuthTerm(Date authTerm) {
		this._authTerm = authTerm;
	}

	public Integer getAuthTermReasonCode() {
		return this._authTermReasonCode;
	}

	public void setAuthTermReasonCode(Integer authTermReasonCode) {
		this._authTermReasonCode = authTermReasonCode;
	}

	public String getAuthTermReason() {
		return this._authTermReason;
	}

	public void setAuthTermReason(String authTermReason) {
		this._authTermReason = authTermReason;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return
			( this._typeId == null ? "<TYPE NOT SET>" : this._typeId ) + " " +
			( this._authFrom == null ? "<AUTH FROM NOT SET>" : this._authFrom ) + " " +
			( this._authTo == null ? "<AUTH TO NOT SET>" : this._authTo ) + " " +
			( this._authTerm == null ? "<AUTH TERM NOT SET>" : this._authTerm ) + " " +
			( this._authTermReasonCode == null ? "<AUTH TERM REASON CODE NOT SET>" : this._authTermReasonCode ) + " " +
			( this._authTermReason == null ? "<AUTH TERM REASON NOT SET>" : this._authTermReason );
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this._authFrom == null) ? 0 : this._authFrom.hashCode());
		result = prime * result + ((this._authTerm == null) ? 0 : this._authTerm.hashCode());
		result = prime * result + ((this._authTermReason == null) ? 0 : this._authTermReason.hashCode());
		result = prime * result + ((this._authTermReasonCode == null) ? 0 : this._authTermReasonCode.hashCode());
		result = prime * result + ((this._authTo == null) ? 0 : this._authTo.hashCode());
		result = prime * result + ((this._typeId == null) ? 0 : this._typeId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuthorizationData other = (AuthorizationData) obj;
		if (this._authFrom == null) {
			if (other._authFrom != null)
				return false;
		} else if (!this._authFrom.equals(other._authFrom))
			return false;
		if (this._authTerm == null) {
			if (other._authTerm != null)
				return false;
		} else if (!this._authTerm.equals(other._authTerm))
			return false;
		if (this._authTermReason == null) {
			if (other._authTermReason != null)
				return false;
		} else if (!this._authTermReason.equals(other._authTermReason))
			return false;
		if (this._authTermReasonCode == null) {
			if (other._authTermReasonCode != null)
				return false;
		} else if (!this._authTermReasonCode.equals(other._authTermReasonCode))
			return false;
		if (this._authTo == null) {
			if (other._authTo != null)
				return false;
		} else if (!this._authTo.equals(other._authTo))
			return false;
		if (this._typeId == null) {
			if (other._typeId != null)
				return false;
		} else if (!this._typeId.equals(other._typeId))
			return false;
		return true;
	}
}
