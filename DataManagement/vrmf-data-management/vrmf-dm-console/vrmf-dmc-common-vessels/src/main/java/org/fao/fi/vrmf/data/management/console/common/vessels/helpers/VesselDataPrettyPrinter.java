/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.common.vessels.helpers;

import static org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter.bold;
import static org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter.color;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.fao.fi.vrmf.data.management.common.model.ConsistencyCheckResult;
import org.fao.fi.vrmf.data.management.common.model.DBUpdateResult;
import org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter.Color;
import org.fao.fi.vrmf.data.management.vessels.model.AuthorizationData;
import org.fao.fi.vrmf.data.management.vessels.model.MultipleIMOByUIDCheckResult;
import org.fao.fi.vrmf.data.management.vessels.model.MultipleUIDByIMOCheckResult;
import org.fao.fi.vrmf.data.management.vessels.model.VesselData;
import org.fao.fi.vrmf.data.management.vessels.model.VesselDataHierarchy;

/**
 * @author Fiorellato
 *
 */
public class VesselDataPrettyPrinter {
	static final public SimpleDateFormat DATE_FORMAT 	   = new SimpleDateFormat("yyyy/MM/dd");
	static final public SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	
	static final public String SEPARATOR = "§";
	static final private String CURRENT_VESSEL_MARKER = "[*] ";
	static final private String CURRENT_VESSEL_PADDER = pad(CURRENT_VESSEL_MARKER.length());
	
	static final private String LABEL = color(Color.CYAN);
	
	static final private String HIGHLIGHT_DATA = bold(Color.GREEN);
	static final private String PLAIN_DATA = color(Color.GREEN);
	static final private String HIGHLIGHT_WRONG_DATA = bold(Color.RED);
	static final private String EMPTY_DATA = bold(Color.MAGENTA);
	
	static final private String UNAUTHORIZED = bold(Color.RED);
	static final private String TO_BE_AUTHORIZED = bold(Color.MAGENTA);
	static final private String AUTHORIZED = bold(Color.GREEN);
	static final private String EXPIRED = bold(Color.YELLOW);
	
	static final private String wrap(String text, String color, String defaultColor) {
		return color + text + defaultColor;
	}
	
	static final private String wrap(Object object, String color, String defaultColor) {
		return wrap(object.toString(), color, defaultColor);
	}
	
	static final private String format(String defaultColor, Object toFormat) {
		if(toFormat == null)
			return wrap("<NOT SET>", EMPTY_DATA, defaultColor);
		
		return toFormat.toString();
	}
	
	static final private String format(String defaultColor, Double toFormat) {
		if(toFormat == null)
			return wrap("<NOT SET>", EMPTY_DATA, defaultColor);
		
		return new DecimalFormat("0.00").format(toFormat);
	}
	
	@SuppressWarnings("unused")
	static final private String formatDate(String defaultColor, Date toFormat) {
		if(toFormat == null)
			return wrap("<NOT SET>", EMPTY_DATA, defaultColor);
		
		return DATE_FORMAT.format(toFormat);
	}

	static final private String formatDateTime(String defaultColor, Date toFormat) {
		if(toFormat == null)
			return wrap("<NOT SET>", EMPTY_DATA, defaultColor);
		
		return DATE_TIME_FORMAT.format(toFormat);
	}
	
	static final private String format(String defaultColor, AuthorizationData toFormat, boolean first) {
		if(toFormat == null)
			return wrap("<NOT SET>", EMPTY_DATA, defaultColor);
		
		Date now = new Date();
		
		boolean isUnauthorized = toFormat.getAuthTerm() != null;
		boolean isExpired = toFormat.getAuthTo() != null && toFormat.getAuthTo().before(now);
		boolean isToBeAuthorized = toFormat.getAuthFrom() != null && toFormat.getAuthFrom().after(now);
		
		return (
			isUnauthorized ? UNAUTHORIZED :
				isToBeAuthorized ? TO_BE_AUTHORIZED :
					isExpired ? EXPIRED :
						AUTHORIZED
		) + 
		( first ? "" : "|          " ) +
		defaultColor +
		"[ " +
			( isUnauthorized ?	   wrap("UNAUTHORIZED    ", UNAUTHORIZED, defaultColor) :
				isToBeAuthorized ? wrap("TO BE AUTHORIZED", TO_BE_AUTHORIZED, defaultColor) :
					isExpired ?    wrap("EXPIRED         ", EXPIRED, defaultColor) :
								   wrap("AUTHORIZED      ", AUTHORIZED, defaultColor)
			) +
		defaultColor
		+ " ] [ " +
			wrap("AUTH TYPE: ", LABEL, defaultColor) + wrap(format(defaultColor, toFormat.getTypeId()), HIGHLIGHT_DATA, defaultColor) + ", " +
			wrap("AUTH FROM: ", LABEL, defaultColor) + wrap(formatDateTime(defaultColor, toFormat.getAuthFrom()), HIGHLIGHT_DATA, defaultColor) + ", " +
			wrap("AUTH TO: "  , LABEL, defaultColor) + wrap(formatDateTime(defaultColor, toFormat.getAuthTo()), HIGHLIGHT_DATA, defaultColor) + ", " +
			wrap("AUTH TERM: ", LABEL, defaultColor) + wrap(formatDateTime(defaultColor, toFormat.getAuthTerm()), HIGHLIGHT_WRONG_DATA, defaultColor) + " " +
			( toFormat.getAuthTerm() == null ? "" : wrap("REASON: ", LABEL, defaultColor) + wrap(toFormat.getAuthTermReason() + " ", bold(Color.MAGENTA), defaultColor)) +
		"]" + SEPARATOR;
	}

	static final public String format(String defaultColor, VesselData toFormat) {
		if(toFormat == null)
			throw new NullPointerException("Vessel data to format are NULL");
		
		boolean hasAuths = toFormat.getAuthorizations() != null && !toFormat.getAuthorizations().isEmpty();
		
		String authsPart = "[ " + wrap("<NOT SET>", EMPTY_DATA, defaultColor) + " ]";
		
		if(hasAuths) {
			authsPart = "";
			
			boolean isFirst = true;
			for(AuthorizationData in : toFormat.getAuthorizations()) {
				authsPart += format(defaultColor, in, isFirst);
				
				isFirst = false;
			}
		}
		
		List<AuthorizationData> auths = toFormat.getAuthorizations() == null ? null : new ArrayList<AuthorizationData>(toFormat.getAuthorizations());
		
		boolean isUnauthorized = false;
		boolean isExpired = false;
		boolean isToBeAuthorized = false;
		
		if(auths != null && !auths.isEmpty()) {
			Date now = new Date();
			
			AuthorizationData auth = auths.get(0);
			
			isUnauthorized = auth.getAuthTerm() != null;
			isExpired = auth.getAuthTo() != null && auth.getAuthTo().before(now);
			isToBeAuthorized = auth.getAuthFrom() != null && auth.getAuthFrom().after(now);
		}

		String color = isToBeAuthorized ? TO_BE_AUTHORIZED :
							isExpired ? EXPIRED :
								isUnauthorized ? UNAUTHORIZED :
									AUTHORIZED;
		
		String identifier = wrap(toFormat.getIdentifier(), HIGHLIGHT_DATA, defaultColor);
		
		if(toFormat.getAlternateIdentifier() != null && !toFormat.getIdentifier().equals(toFormat.getAlternateIdentifier()))
			identifier +=  ( " [ #" + wrap(toFormat.getAlternateIdentifier(), PLAIN_DATA, defaultColor) + " ]" );
		
		return 
			wrap("# IDs    : ", color, defaultColor) + "[ " + wrap("ID: ", LABEL, defaultColor) + wrap(toFormat.getId(), HIGHLIGHT_DATA, defaultColor) + ", " + wrap("UID: ", LABEL, defaultColor) + wrap(toFormat.getUid(), HIGHLIGHT_DATA, defaultColor) + " ]" + SEPARATOR + 
			wrap("| Meta   : ", color, defaultColor) + "[ " +
				wrap("SOURCE: ", LABEL, defaultColor) + wrap(toFormat.getSourceSystem(), HIGHLIGHT_DATA, defaultColor) + ", " + 
				wrap("SOURCE_ID: ", LABEL, defaultColor) + identifier + " " +
			"] " + 
			"[ " +
				wrap("DATE UPDATED: ", LABEL, defaultColor) + wrap(formatDateTime(defaultColor, toFormat.getUpdateDate()), PLAIN_DATA, defaultColor) + 
			"]" + SEPARATOR + 
			wrap("| Data   : ", color, defaultColor) + "[ " +
				wrap("FLAG: ", LABEL, defaultColor) + wrap(format(defaultColor, toFormat.getFlag()), HIGHLIGHT_DATA, defaultColor) + ", " +
				wrap("NAME: ", LABEL, defaultColor) + wrap(format(defaultColor, toFormat.getName()), HIGHLIGHT_DATA, defaultColor) + " " +
			"] " +
			"[ " +
				wrap("IMO: ", LABEL, defaultColor) + wrap(format(defaultColor, toFormat.getImo()), HIGHLIGHT_DATA, defaultColor) + ", " +
				wrap("IRCS: ", LABEL, defaultColor) + wrap(format(defaultColor, toFormat.getIrcs()), HIGHLIGHT_DATA, defaultColor) + ", " +
				wrap("NRN: ", LABEL, defaultColor) + wrap(format(defaultColor, toFormat.getNrn()), HIGHLIGHT_DATA, defaultColor) + " " +
			"] " +
			"[ " + 
				wrap("LOA: ", LABEL, defaultColor) + wrap(format(defaultColor, toFormat.getLoa()), HIGHLIGHT_DATA, defaultColor) + ", " +
				wrap("GT: ", LABEL, defaultColor) + wrap(format(defaultColor, toFormat.getGt()), HIGHLIGHT_DATA, defaultColor)  + " " +
			"]" + SEPARATOR + 
			wrap(toFormat.getMapsTo() == null ? "#" : "|", color, defaultColor) + 
			wrap(" Auths  : ", color, defaultColor) + authsPart + SEPARATOR + 
			( toFormat.getMapsTo() == null ? "" : 
				wrap("# Mapping: ", color, defaultColor) + "[ " +
					wrap("MAPS TO: ", LABEL, defaultColor) + wrap(toFormat.getMapsTo(), PLAIN_DATA, defaultColor) + ", " +
					wrap("SCORE: "  , LABEL, defaultColor) + wrap(toFormat.getMappingWeight(), PLAIN_DATA, defaultColor) + ", " +
					wrap("DATE: "   , LABEL, defaultColor) + wrap(formatDateTime(defaultColor, toFormat.getMappingDate()), PLAIN_DATA, defaultColor) + ", " + 
					wrap("USER: "   , LABEL, defaultColor) + wrap(format(defaultColor, toFormat.getMappingUser()), PLAIN_DATA, defaultColor) + " - " +
					wrap(format(defaultColor, toFormat.getMappingComment()), PLAIN_DATA, defaultColor) + " " +
				"] "
			);
	}
	
	static final public String format(String defaultColor, VesselDataHierarchy hierarchy) {
		return format(defaultColor, hierarchy, Integer.MIN_VALUE, 0);
	}
	
	static final public String format(String defaultColor, VesselDataHierarchy hierarchy, Integer targetVesselId) {
		return format(defaultColor, hierarchy, targetVesselId, 0);
	}
	
	static final public String format(String defaultColor, VesselDataHierarchy hierarchy, Integer targetVesselId, int level) {
		if(hierarchy == null)
			throw new NullPointerException("Vessel hierarchy to format is NULL");
		
		VesselData current = hierarchy.getData();
		
		boolean isTarget = targetVesselId != null && targetVesselId.equals(current.getId());

		StringBuilder result = new StringBuilder(isTarget ? wrap(CURRENT_VESSEL_MARKER, HIGHLIGHT_DATA, defaultColor) : CURRENT_VESSEL_PADDER);

		int counter = 0;
		
		for(String part : format(defaultColor, current).split(SEPARATOR, -1)) {
			if(!part.equals("")) {
				result.append(pad(level * 3 + ( counter == 0 ? 0 : CURRENT_VESSEL_MARKER.length() ))).
					   append(part).
					   append(SEPARATOR);
				
				counter++;
			}
		}
		
		result.append(SEPARATOR);
		
		for(VesselDataHierarchy in : hierarchy.children()) {
			result.append(format(defaultColor, in, targetVesselId, level+1));
			result.append(SEPARATOR);
		}
		
		return result.toString().replaceAll(SEPARATOR + "$", "");
	}
	
	static final public String pad(int level) {
		return pad(level, ' ');
	}
	
	static final public String pad(int level, char padding) {
		StringBuilder padder = new StringBuilder();
		
		for(int i=0; i<level; i++)
			padder.append(padding);
		
		return padder.toString();
	}
	
	static final public String format(String defaultColor, ConsistencyCheckResult toFormat) {
		return toFormat == null ? "<NOT SET>" : (
				"[ " +
				  wrap("VRMF ID: ", LABEL, defaultColor) + wrap(format(defaultColor, toFormat.getId()), PLAIN_DATA, defaultColor) + ", " +
				  wrap("VRMF UID KO: ", LABEL, defaultColor) + wrap(format(defaultColor, toFormat.getUidKo()), HIGHLIGHT_WRONG_DATA, defaultColor) + ", " +
				  wrap("VRMF UID OK: ", LABEL, defaultColor) + wrap(format(defaultColor, toFormat.getUidOk()), HIGHLIGHT_DATA, defaultColor) + ", " +
				  wrap("Attribute: ", LABEL, defaultColor) + wrap(format(defaultColor, toFormat.getAttribute()), PLAIN_DATA, defaultColor) + ", " +
				  wrap("Occurrencies: ", LABEL, defaultColor) + wrap(format(defaultColor, toFormat.getOccurrencies()), PLAIN_DATA, defaultColor) + " ]");
	}
	
	static final public String format(String defaultColor, DBUpdateResult toFormat) {
		return toFormat == null ? "<NOT SET>" : (
				"[ " +
				wrap("Current:", LABEL, defaultColor) + " { " +
					wrap("Items: ", LABEL, defaultColor) + wrap(format(defaultColor, toFormat.getCurrentItems()), HIGHLIGHT_DATA, defaultColor) + ", " +
					wrap("Last update date: ", LABEL, defaultColor) + wrap(formatDateTime(defaultColor, toFormat.getCurrentItemsLastUpdate()), HIGHLIGHT_DATA, defaultColor) + ", " +
					wrap("Last reference date: ", LABEL, defaultColor) +  wrap(formatDateTime(defaultColor, toFormat.getCurrentItemsLastReference()), HIGHLIGHT_DATA, defaultColor) + " }" +
				" ]" + SEPARATOR +
				"[ " +
				wrap("Updated:", LABEL, defaultColor) + " { " + 
				  	wrap("Items: ", LABEL, defaultColor) + wrap(format(defaultColor, toFormat.getUpdatedItems()), HIGHLIGHT_DATA, defaultColor) + ", " +
				  	wrap("Last update date: ", LABEL, defaultColor) +  wrap(formatDateTime(defaultColor, toFormat.getUpdatedItemsLastUpdate()), HIGHLIGHT_DATA, defaultColor) + ", " +
				  	wrap("Last reference date: ", LABEL, defaultColor) +  wrap(formatDateTime(defaultColor, toFormat.getUpdatedItemsLastReference()), HIGHLIGHT_DATA, defaultColor) + " }" +
				" ]");
	}
	
	static final public String format(String defaultColor, MultipleIMOByUIDCheckResult toFormat) {
		return "[ " +
				  wrap("UID: ", LABEL, defaultColor) + wrap(toFormat.getUid(), HIGHLIGHT_DATA, defaultColor) + ", " +
				  wrap("Distinct IMOs: ", LABEL, defaultColor) + wrap(toFormat.getDistinctIMOs(), HIGHLIGHT_WRONG_DATA, defaultColor) + " ]";
	}
	
	static final public String format(String defaultColor, MultipleUIDByIMOCheckResult toFormat) {
		return "[ " +
				  wrap("IMO: ", LABEL, defaultColor) + wrap(toFormat.getImo(), HIGHLIGHT_DATA, defaultColor) + ", " +
				  wrap("Distinct IDs: ", LABEL, defaultColor) + wrap(toFormat.getDistinctIDs(), HIGHLIGHT_WRONG_DATA, defaultColor) + ", " + 
				  wrap("Distinct UIDs: ", LABEL, defaultColor) + wrap(toFormat.getDistinctUIDs(), HIGHLIGHT_WRONG_DATA, defaultColor) + " ]";
	}
}