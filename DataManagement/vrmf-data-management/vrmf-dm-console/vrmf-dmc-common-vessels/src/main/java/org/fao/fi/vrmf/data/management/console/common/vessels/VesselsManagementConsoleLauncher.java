/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.common.vessels;

import java.io.File;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Fiorellato
 *
 */
public class VesselsManagementConsoleLauncher {
	final public static void launch(String config, String[] args) throws Exception {
		System.out.println("Please wait: initializing VRMF Vessels Management Console...");
		System.out.println();

		File cd = new File(".");

		System.getProperties().put("app.path", cd.getAbsolutePath());

		try(ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext(config)) {
			ctx.getBean(VesselsManagementConsole.class).launch(args);
		};
	}
	
	static public void main(String[] args) throws Exception {
		launch("config/spring/vrmf-dmc-vessels-local-spring-context.xml", args);
	}
}
