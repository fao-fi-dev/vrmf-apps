/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.common.vessels.commands;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.inject.Named;
import javax.inject.Singleton;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.fao.fi.vrmf.data.management.common.model.ConsistencyCheckResult;
import org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter;
import org.fao.fi.vrmf.data.management.console.common.helpers.ArgumentsChecker;
import org.fao.fi.vrmf.data.management.console.common.spi.exceptions.ConsoleCommandException;
import org.fao.fi.vrmf.data.management.console.common.vessels.helpers.VesselDataPrettyPrinter;
import org.fao.fi.vrmf.data.management.vessels.model.MultipleIMOByUIDCheckResult;
import org.fao.fi.vrmf.data.management.vessels.model.MultipleUIDByIMOCheckResult;
import org.fao.fi.vrmf.data.management.vessels.spi.VesselsDataManagementOperationsProvider;

import au.com.bytecode.opencsv.CSVWriter;

/**
 * @author Fiorellato
 *
 */
@Singleton @Named("vessel.console.command.sys.check")
public class CheckSystemConsoleCommand extends AbstractVesselConsoleCommand {
	static final private String OUT_FILE_OPT			 = "f";
	static final private String OUT_FILE_OPT_LONG		 = "file";
	
	static final private String DB_CONSISTENCY_OPT		 = "c";
	static final private String DB_CONSISTENCY_OPT_LONG	 = "consistency";
	
	static final private String MULTIPLE_IMO_BY_UID_CONSISTENCY_OPT		 = "mi";
	static final private String MULTIPLE_IMO_BY_UID_CONSISTENCY_OPT_LONG = "multipleIMO";
	
	static final private String MULTIPLE_UID_BY_IMO_CONSISTENCY_OPT		 = "mu";
	static final private String MULTIPLE_UID_BY_IMO_CONSISTENCY_OPT_LONG = "multipleUID";
	
	static final private String OUT_FILE_FOLDER = "./checks";
		
	private Date _executionDate;
	
	@Override
	public String getCommandName() {
		return "check";
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.vessels.commands.AbstractVesselConsoleCommand#getOptions()
	 */
	@SuppressWarnings("static-access")
	@Override
	public Options getOptions() {
		Options opts = new Options();
		opts.addOption(OptionBuilder.withDescription("Store check results as CSV files under the " + OUT_FILE_FOLDER + " folder").withLongOpt(OUT_FILE_OPT_LONG).create(OUT_FILE_OPT));
		opts.addOption(OptionBuilder.withDescription("Perform a consistency check on the basis of current mappings and of existing vessels VRMF IDs and UIDs").withLongOpt(DB_CONSISTENCY_OPT_LONG).create(DB_CONSISTENCY_OPT));
		opts.addOption(OptionBuilder.withDescription("Perform a consistency check to identify vessels sharing the same UIDs and having different IMOs at the same time").withLongOpt(MULTIPLE_IMO_BY_UID_CONSISTENCY_OPT_LONG).create(MULTIPLE_IMO_BY_UID_CONSISTENCY_OPT));
		opts.addOption(OptionBuilder.withDescription("Perform a consistency check to identify IMOs shared by vessels having different UIDs").withLongOpt(MULTIPLE_UID_BY_IMO_CONSISTENCY_OPT_LONG).create(MULTIPLE_UID_BY_IMO_CONSISTENCY_OPT));
		
		return opts;
	}
	
	@Override
	public String getCommandDescription() {
		return "Execute various sanity checks over the current database content";
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.spi.ConsoleCommand#hasArguments()
	 */
	@Override
	public boolean hasArguments() {
		return false;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.spi.ConsoleCommand#getCommandArguments()
	 */
	@Override
	public String getCommandArguments() {
		return null;
	}
	
	private File getOutFile(String type) {
		return new File(OUT_FILE_FOLDER + "/check_" + type + "_" + new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(this._executionDate) + ".csv");
	}
	
	private CSVWriter getWriter(File file) throws IOException {
		return new CSVWriter(new OutputStreamWriter(new FileOutputStream(file)), ',', '"');
	}

	@Override
	protected void doExecute(Properties properties, CommandLine commandLine, VesselsDataManagementOperationsProvider provider, AbstractEmitter emitter) throws Exception {
		ArgumentsChecker.doesNotRequireArguments(this, commandLine.getArgs());

		this._executionDate = new Date();
		
		boolean consistencyCheck = commandLine.hasOption(DB_CONSISTENCY_OPT);
		boolean imoCheck		 = commandLine.hasOption(MULTIPLE_IMO_BY_UID_CONSISTENCY_OPT);
		boolean uidCheck		 = commandLine.hasOption(MULTIPLE_UID_BY_IMO_CONSISTENCY_OPT);
		
		boolean noOptions = !consistencyCheck && !imoCheck && !uidCheck;
		
		consistencyCheck |= noOptions;
		imoCheck |= noOptions;
		uidCheck |= noOptions;
		
		boolean confirm = false;
		
		if(!noOptions || ( confirm = emitter.confirm("This will launch all the currently available system checks. Are you sure you want to continue") )) {
			if(noOptions)
				emitter.nl();
			
			if(consistencyCheck) {
				try {
					this.doExecuteConsistencyCheck(properties, commandLine, provider, emitter);
				} catch(Exception e) {
					if(!noOptions)
						throw e;
					else {
						emitter.nl();
						emitter.error("Unable to perform the required consistency check: " + e.getMessage());
					}
				}
			}
			
			if(imoCheck) {
				try {
					if(consistencyCheck)
						emitter.nl();
					
					this.doExecuteIMOCheck(properties, commandLine, provider, emitter);
				} catch(Exception e) {
					if(!noOptions)
						throw e;
					else {
						emitter.nl();
						emitter.error("Unable to perform the required multiple IMOs check: " + e.getMessage());
					}
				}
			}
			
			if(uidCheck) {
				try {
					if(consistencyCheck || imoCheck)
						emitter.nl();
					
					this.doExecuteUIDCheck(properties, commandLine, provider, emitter);
				} catch(Exception e) {
					if(!noOptions)
						throw e;
					else {
						emitter.nl();
						emitter.error("Unable to perform the required multiple UIDs check: " + e.getMessage());
					}
				}
			}
		}
		
		if(noOptions && !confirm)
			emitter.abort();
	}
	
	protected void doExecuteConsistencyCheck(Properties properties, CommandLine commandLine, VesselsDataManagementOperationsProvider provider, AbstractEmitter emitter) throws Exception {
		try {
			emitter.banner("Data consistency check");
			emitter.nl();
			
			emitter.info(" >> Invoking the data consistency check procedure... Please wait!");

			List<ConsistencyCheckResult> results = new ArrayList<ConsistencyCheckResult>(provider.checkConsistency());
			
			emitter.nl();
			emitter.info(" -> The DB has been checked: " + ( results.size() == 0 ? "no" : String.valueOf(results.size()) ) + " unconsistent mappings found!");
			
			if(!results.isEmpty()) {
				int occurrencies = 0;
				
				for(ConsistencyCheckResult in : results)
					occurrencies += in.getOccurrencies();
				
				emitter.info(" -> The " + results.size() + " unconsistent mappings yield " + occurrencies + " wrong records occurrencies among all vessel attributes...");
				
				emitter.nl();
				emitter.highlight("Non-consistent records:");
				emitter.nl();
				
				Collections.sort(results);
				
				for(ConsistencyCheckResult in : results)
					emitter.info(" -> " + VesselDataPrettyPrinter.format(AbstractEmitter.DEFAULT_INFO_COLOR, in));
				
				if(commandLine.hasOption(OUT_FILE_OPT)) {
					File out = this.getOutFile("consistency");
					try(CSVWriter writer = this.getWriter(out)) {
						writer.writeNext("ATTRIBUTE", "VESSEL_ID", "VESSEL_UID_OK", "VESSEL_UID_KO", "OCCURRENCIES");
						
						for(ConsistencyCheckResult in : results) {
							writer.writeNext(in.getAttribute(), 
											 String.valueOf(in.getId()),
											 String.valueOf(in.getUidOk()),
											 String.valueOf(in.getUidKo()),
											 String.valueOf(in.getOccurrencies()));
						}
						
						writer.flush();
						
						emitter.nl();
						emitter.warn(" -> Consistency check results have been stored in " + OUT_FILE_FOLDER + "/" + out.getName());
					}
				}
			}
		} catch(Throwable t) {
			emitter.nl();
			
			throw new ConsoleCommandException("Unable to perform consistency check: " + t.getMessage(), t);
		}
	}
	
	protected void doExecuteIMOCheck(Properties properties, CommandLine commandLine, VesselsDataManagementOperationsProvider provider, AbstractEmitter emitter) throws Exception {
		try {
			emitter.banner("Multiple IMO consistency check");
			emitter.nl();
			
			emitter.info(" >> Invoking the multiple IMOs by UID consistency check procedure... Please wait!");

			List<MultipleIMOByUIDCheckResult> results = new ArrayList<MultipleIMOByUIDCheckResult>(provider.checkMultipleIMOByUID(false));
			
			emitter.nl();
			emitter.info(" -> The DB has been checked: " + ( results.size() == 0 ? "no" : String.valueOf(results.size()) ) + " UIDs have been identified whose partecipating vessels do have different IMOs!");
			
			if(!results.isEmpty()) {
				emitter.nl();
				emitter.highlight("UIDs sharing different IMOs:");
				emitter.nl();
				
				for(MultipleIMOByUIDCheckResult in : results)
					emitter.info(" -> " + VesselDataPrettyPrinter.format(AbstractEmitter.DEFAULT_INFO_COLOR, in));
				
				if(commandLine.hasOption(OUT_FILE_OPT)) {
					File out = this.getOutFile("multiple_imo_by_uid");
					
					try(CSVWriter writer = this.getWriter(out)) {
						writer.writeNext("UID", "DISTINCT_IMOS");
						
						for(MultipleIMOByUIDCheckResult in : results) {
							writer.writeNext(String.valueOf(in.getUid()), 
											 String.valueOf(in.getDistinctIMOs()));
						}
						
						writer.flush();
						
						emitter.nl();
						emitter.warn(" -> Multiple IMO by UID check results have been stored in " + OUT_FILE_FOLDER + "/" + out.getName());
					}
				}
			}
		} catch(Throwable t) {
			emitter.nl();
			
			throw new ConsoleCommandException("Unable to perform multiple IMOs check: " + t.getMessage(), t);
		}
	}
	
	protected void doExecuteUIDCheck(Properties properties, CommandLine commandLine, VesselsDataManagementOperationsProvider provider, AbstractEmitter emitter) throws Exception {
		try {
			emitter.banner("Multiple UID consistency check");
			emitter.nl();
			
			emitter.info(" >> Invoking the multiple UIDs by IMO consistency check procedure... Please wait!");

			List<MultipleUIDByIMOCheckResult> results = new ArrayList<MultipleUIDByIMOCheckResult>(provider.checkMultipleUIDByIMO(false));
			
			emitter.nl();
			emitter.info(" -> The DB has been checked: " + ( results.size() == 0 ? "no" : String.valueOf(results.size()) ) + " IMOs have been identified which are shared by different UIDs!");
			
			if(!results.isEmpty()) {
				emitter.nl();
				emitter.highlight("IMOs shared by different UIDs:");
				emitter.nl();
				
				for(MultipleUIDByIMOCheckResult in : results)
					emitter.info(" -> " + VesselDataPrettyPrinter.format(AbstractEmitter.DEFAULT_INFO_COLOR, in));
				
				if(commandLine.hasOption(OUT_FILE_OPT)) {
					File out = this.getOutFile("multiple_uid_by_imo");
					
					try(CSVWriter writer = this.getWriter(out)) {
						writer.writeNext("IMO", "DISTINCT_VESSEL_IDS", "DISTINCT_VESSEL_UIDS");
						
						for(MultipleUIDByIMOCheckResult in : results) {
							writer.writeNext(in.getImo(), 
											 String.valueOf(in.getDistinctIDs()),
											 String.valueOf(in.getDistinctUIDs()));
						}
						
						writer.flush();
						
						emitter.nl();
						emitter.warn(" -> Multiple UID by IMO check results have been stored in " + OUT_FILE_FOLDER + "/" + out.getName());
					}
				}
			}
		} catch(Throwable t) {
			emitter.nl();
			
			throw new ConsoleCommandException("Unable to perform multiple UIDs check: " + t.getMessage(), t);
		}
	}
}
