/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.common.vessels.commands;

import java.util.Properties;

import javax.inject.Named;
import javax.inject.Singleton;

import org.apache.commons.cli.CommandLine;
import org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter;
import org.fao.fi.vrmf.data.management.console.common.helpers.ArgumentsChecker;
import org.fao.fi.vrmf.data.management.vessels.spi.VesselsDataManagementOperationsProvider;

/**
 * @author Fiorellato
 *
 */
@Singleton @Named("vessel.console.command.getVesselUID")
public class GetVesselUIDConsoleCommand extends AbstractVesselConsoleCommand {
	@Override
	public String getCommandName() {
		return "uid";
	}

	@Override
	public String getCommandDescription() {
		return "Retrieve a VRMF vessel UID by IMO / RFMO / VRMF identifier";
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.spi.ConsoleCommand#hasArguments()
	 */
	@Override
	public boolean hasArguments() {
		return true;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.spi.ConsoleCommand#getCommandArguments()
	 */
	@Override
	public String getCommandArguments() {
		return "<vesselID>";
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.spi.impl.AbstractConsoleCommand#getCommandArgumentsDescriptions()
	 */
	@Override
	public String getCommandArgumentsDescriptions() {
		return "where <vesselID> is an IMO / RFMO / VRMF vessel identifier";
	}

	@Override
	protected void doExecute(Properties properties, CommandLine commandLine, VesselsDataManagementOperationsProvider provider, AbstractEmitter emitter) throws Exception {
		String[] parameters = commandLine.getArgs();
		
		ArgumentsChecker.requiresExactly(this, parameters, 1);
		
		String providedID = parameters[0];
		Integer ID = this.getVesselID(providedID, provider, emitter), UID = provider.getUID(ID);
		
		boolean isRFMOId = this.isRFMOID(providedID);

		String rfmo = this.getRFMO(providedID);
		
		boolean isIMO = IMO_ID.equals(rfmo);
		
		if(UID != null) {
			emitter.info(" -> VRMF UID for vessel with " + ( isIMO ? "IMO " : ( ( isRFMOId ? "RFMO" : "VRMF" ) + " ID " ) ) + providedID + " is: [ " + UID + " ]");
		}
	}
}