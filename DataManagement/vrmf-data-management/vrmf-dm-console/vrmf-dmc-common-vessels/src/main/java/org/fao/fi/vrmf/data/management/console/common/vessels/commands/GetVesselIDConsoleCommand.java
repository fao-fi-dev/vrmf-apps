/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.common.vessels.commands;

import java.util.Properties;

import javax.inject.Named;
import javax.inject.Singleton;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter;
import org.fao.fi.vrmf.data.management.console.common.helpers.ArgumentsChecker;
import org.fao.fi.vrmf.data.management.console.common.spi.exceptions.ConsoleCommandException;
import org.fao.fi.vrmf.data.management.vessels.spi.VesselsDataManagementOperationsProvider;

/**
 * @author Fiorellato
 *
 */
@Singleton @Named("vessel.console.command.getVesselID")
public class GetVesselIDConsoleCommand extends AbstractVesselConsoleCommand {
	static final private String HISTORICAL_OPT		 = "h";
	static final private String HISTORICAL_OPT_LONG	 = "historical";
	
	@Override
	public String getCommandName() {
		return "id";
	}

	@Override
	public String getCommandDescription() {
		return "Retrieve a VRMF vessel ID by IMO or RFMO identifier, either current or historical. If the historical search is enabled via the \"-\"" + HISTORICAL_OPT + "\" (\"--\"" + HISTORICAL_OPT_LONG + "\") flag, then multiple results could be returned";
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.vessels.commands.AbstractVesselConsoleCommand#getOptions()
	 */
	@SuppressWarnings("static-access")
	@Override
	public Options getOptions() {
		Options opts = new Options();
		opts.addOption(OptionBuilder.withDescription("Search in each vessel history as well, and not only among the current vessels identifiers").withLongOpt(HISTORICAL_OPT_LONG).create(HISTORICAL_OPT));
		
		return opts;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.spi.ConsoleCommand#hasArguments()
	 */
	@Override
	public boolean hasArguments() {
		return true;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.spi.ConsoleCommand#getCommandArguments()
	 */
	@Override
	public String getCommandArguments() {
		return "<vesselID>";
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.spi.impl.AbstractConsoleCommand#getCommandArgumentsDescriptions()
	 */
	@Override
	public String getCommandArgumentsDescriptions() {
		return "where <vesselID> is either an IMO or a RFMO vessel identifier";
	}

	@Override
	protected void doExecute(Properties properties, CommandLine commandLine, VesselsDataManagementOperationsProvider provider, AbstractEmitter emitter) throws Exception {
		String[] parameters = commandLine.getArgs();
		
		ArgumentsChecker.requiresExactly(this, parameters, 1);
		
		String providedID = parameters[0];
		
		boolean historical = commandLine.hasOption(HISTORICAL_OPT);
		
		if(this.isRFMOID(providedID)) {
			String rfmo = this.getRFMO(providedID), rfmoID = this.getRFMOID(providedID);
			
			boolean isIMO = IMO_ID.equals(rfmo);

			if(historical) {
				int[] ids = provider.getVesselIDByHistoricalIdentifier(rfmo, rfmoID);
				
				emitter.highlight(ids.length + " vessels found having (or having had) " + rfmoID + " as " + ( isIMO ? "IMO" : ( rfmo + " identifier:" ) ));
				
				emitter.nl();
				
				for(int id : ids) {
					emitter.info(" -> VRMF ID for historical " + rfmoID + ( isIMO ? "" : " identifier" ) + " " + providedID + " is: [ " + id + " ]");
				}
			} else 
				emitter.info(" -> VRMF ID for current " + rfmoID + ( isIMO ? "" : " identifier" ) + " " + providedID +  " is: [ " + this.getVesselID(providedID, provider, emitter) + " ]");
		} else {
			throw new ConsoleCommandException("Provided ID (" + providedID + ") is not a valid IMO or RFMO identifier");
		}
	}
}
