/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.common.vessels.commands;

import java.util.Properties;

import javax.inject.Named;
import javax.inject.Singleton;

import org.apache.commons.cli.CommandLine;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemIsNotMappedException;
import org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter;
import org.fao.fi.vrmf.data.management.console.common.helpers.ArgumentsChecker;
import org.fao.fi.vrmf.data.management.vessels.model.VesselData;
import org.fao.fi.vrmf.data.management.vessels.model.VesselDataHierarchy;
import org.fao.fi.vrmf.data.management.vessels.spi.VesselsDataManagementOperationsProvider;

/**
 * @author Fiorellato
 *
 */
@Singleton @Named("vessel.console.command.unlinkVesselByID")
public class UnlinkVesselByIDConsoleCommand extends AbstractVesselConsoleCommand {
	@Override
	public String getCommandName() {
		return "unlink";
	}

	@Override
	public String getCommandDescription() {
		return "Unlink a vessel (and the hierarchy below) from its parent";
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.spi.ConsoleCommand#hasArguments()
	 */
	@Override
	public boolean hasArguments() {
		return true;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.spi.ConsoleCommand#getCommandArguments()
	 */
	@Override
	public String getCommandArguments() {
		return "<vesselID>";
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.spi.impl.AbstractConsoleCommand#getCommandArgumentsDescriptions()
	 */
	@Override
	public String getCommandArgumentsDescriptions() {
		return "where <vesselID> is an IMO / RFMO / VRMF vessel identifier";
	}

	@Override
	protected void doExecute(Properties properties, CommandLine commandLine, VesselsDataManagementOperationsProvider provider, AbstractEmitter emitter) throws Exception {
		String[] parameters = commandLine.getArgs();
		
		ArgumentsChecker.requiresExactly(this, parameters, 1);

		String providedID = parameters[0];
		Integer ID, newUID;
		
		ID = this.getVesselID(providedID, provider, emitter);
		
		VesselData toUnlink = provider.getDataByItemID(ID);

		if(toUnlink.getMapsTo() == null) {
			throw new ItemIsNotMappedException(this.describe(providedID, toUnlink) + " is not currently mapped to any other vessel");
		}
		
		VesselDataHierarchy currentHierarchy = null, oldHierarchy = null, newHierarchy = null;

		currentHierarchy = provider.getHierarchyByItemID(ID);
			
		if(this.isVerbose(properties)) {
			emitter.highlight("Current hierarchy for " + this.describe(providedID, toUnlink)  + ":");
			
			emitter.nl();
			this.emitHierarchy(ID, currentHierarchy, emitter);
			emitter.nl();
		}
		
		if(this.isInteractive(properties) && !emitter.confirm("Are you sure you want to unlink " + this.describe(providedID, toUnlink) + " and its linked vessels from its parent hierarchy")) {
			emitter.abort();
			
			return;
		}

		newUID = provider.unlink(ID);
	
		emitter.nl();
		
		emitter.info(" -> " + this.describe(providedID, toUnlink) + " has been unlinked from its parent hierarchy and was assigned " + newUID + " as new UID");

		if(this.isVerbose(properties)) {
			emitter.nl();
			
			oldHierarchy = provider.getHierarchyByItemID(toUnlink.getMapsTo());
			newHierarchy = provider.getHierarchyByItemID(ID);
			
			emitter.highlight("Previous parent hierarchy for unlinked " + this.describe(providedID, toUnlink) + ":");
	
			emitter.nl();
			
			this.emitHierarchy(null, oldHierarchy, emitter);
			
			emitter.nl();
				
			emitter.highlight("New hierarchy for unlinked " + this.describe(providedID, toUnlink) + ":");
			
			emitter.nl();
			
			this.emitHierarchy(ID, newHierarchy, emitter);
		}
	}
}
