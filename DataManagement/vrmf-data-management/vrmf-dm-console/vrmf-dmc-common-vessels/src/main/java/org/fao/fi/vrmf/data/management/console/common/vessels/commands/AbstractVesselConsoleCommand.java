/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.common.vessels.commands;

import java.util.regex.Pattern;

import org.apache.commons.cli.Options;
import org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter;
import org.fao.fi.vrmf.data.management.console.common.spi.exceptions.InvalidIDException;
import org.fao.fi.vrmf.data.management.console.common.spi.impl.AbstractConsoleCommand;
import org.fao.fi.vrmf.data.management.console.common.vessels.helpers.VesselDataPrettyPrinter;
import org.fao.fi.vrmf.data.management.vessels.model.VesselData;
import org.fao.fi.vrmf.data.management.vessels.model.VesselDataHierarchy;
import org.fao.fi.vrmf.data.management.vessels.spi.VesselsDataManagementOperationsProvider;

/**
 * @author Fiorellato
 */
abstract public class AbstractVesselConsoleCommand extends AbstractConsoleCommand<VesselData, VesselDataHierarchy, VesselsDataManagementOperationsProvider> {
	static final private String VRMF_ID_PATTERN_REGEXP = "[0-9]+";
	static final private Pattern VRMF_ID_PATTERN = Pattern.compile(VRMF_ID_PATTERN_REGEXP);
	
	static final private String RFMO_ID_PATTERN_REGEXP = "([a-zA-Z_]+)\\#([a-zA-Z0-9]+)";
	static final private Pattern RFMO_ID_PATTERN = Pattern.compile(RFMO_ID_PATTERN_REGEXP);
	
	static final protected String IMO_ID = "IMO";

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.spi.impl.AbstractConsoleCommand#getOptions()
	 */
	@Override
	public Options getOptions() {
		return new Options();
	}
	
	final protected boolean isVRMFID(String ID) {
		if(ID == null || "".equals(ID.trim())) {
			return false;
		}
		
		return VRMF_ID_PATTERN.matcher(ID).matches();
	}
	
	final protected boolean isRFMOID(String ID) {
		if(ID == null || "".equals(ID.trim())) {
			return false;
		}
		
		return RFMO_ID_PATTERN.matcher(ID).matches();
	}
	
	final protected String getRFMO(String ID) {
		return ID.replaceAll(RFMO_ID_PATTERN_REGEXP, "$1");
	}

	final protected String getRFMOID(String ID) {
		return ID.replaceAll(RFMO_ID_PATTERN_REGEXP, "$2");
	}
	
	final protected Integer getVesselID(String providedID, VesselsDataManagementOperationsProvider provider, AbstractEmitter emitter) throws Exception {
		String rfmo, rfmoID;
		Integer ID;
		
		if(this.isRFMOID(providedID)) {
			rfmo = this.getRFMO(providedID);
			rfmoID = this.getRFMOID(providedID);
			
			ID = provider.getVesselIDByCurrentIdentifier(rfmo, rfmoID);
		} else {
			try {
				ID = Integer.parseInt(providedID);
			} catch(NumberFormatException NFe) {
				throw new InvalidIDException("Provided ID (" + providedID + ") is neither an IMO number nor a RFMO or a VRMF ID");
			}
		}
		
		return ID;
	}
	
	final protected void emitBasicVesselData(String vesselID, VesselData data, AbstractEmitter emitter) {
		emitter.highlight("Basic data for " + this.describe(vesselID, data) + ":");

		emitter.nl();

		for(String part : VesselDataPrettyPrinter.format(AbstractEmitter.DEFAULT_INFO_COLOR, data).split(VesselDataPrettyPrinter.SEPARATOR, -1)) {
			if(part != null && !"".equals(part.trim()))
				emitter.info(part);
		}
	}
	
	final protected void emitBasicVesselData(String vesselID, AbstractEmitter emitter) {
		emitter.highlight("Basic data for " + this.describe(vesselID) + ":");
	}
	
	final protected void emitHierarchy(VesselDataHierarchy data, AbstractEmitter emitter) {
		this.emitHierarchy(null, data, emitter);
	}
	
	final protected void emitHierarchy(Integer vesselID, VesselDataHierarchy data, AbstractEmitter emitter) {
		for(String part : VesselDataPrettyPrinter.format(AbstractEmitter.DEFAULT_INFO_COLOR, data, vesselID).split(VesselDataPrettyPrinter.SEPARATOR, -1)) {
			if(part != null)
				emitter.info(part);
		}
	}
	
	final protected String describe(Integer vesselID) {
		return this.describe(vesselID == null ? null : vesselID.toString());
	}
	
	final protected String describe(String vesselID) {
		return this.describe(vesselID, null);
	}
	
	final protected String describe(String vesselID, VesselData data) {
		boolean isRFMOID = this.isRFMOID(vesselID);
		
		String rfmo = null, rfmoID = null;
		
		if(isRFMOID) {
			rfmo = this.getRFMO(vesselID);
			rfmoID = this.getRFMOID(vesselID);
		}
		
		boolean isIMO = isRFMOID && IMO_ID.equals(rfmo);
		
		String description = "";
		
		if(isIMO) {
			description += "vessel with IMO #";
			description += rfmoID;
		} else {
			description += isRFMOID ? rfmo : "VRMF";
			description += " vessel #";
			description += isRFMOID ? rfmoID : vesselID;
		}

		if(data != null) {
			description += " (";
			
			if(isRFMOID)
				description += "VRMF ID #" + data.getId() + " - ";
			
			description += "VRMF UID #" + data.getUid();
			
			description += ")";
		}
		
		return description;
	}
}