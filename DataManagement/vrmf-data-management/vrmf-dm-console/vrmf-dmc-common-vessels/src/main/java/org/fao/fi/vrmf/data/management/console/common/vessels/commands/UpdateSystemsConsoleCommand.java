/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.common.vessels.commands;

import java.util.Properties;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.fao.fi.vrmf.data.management.common.model.DBUpdateResult;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.UnmanagedException;
import org.fao.fi.vrmf.data.management.console.common.AbstractConsole;
import org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter;
import org.fao.fi.vrmf.data.management.console.common.helpers.ArgumentsChecker;
import org.fao.fi.vrmf.data.management.console.common.spi.exceptions.ConsoleCommandException;
import org.fao.fi.vrmf.data.management.console.common.vessels.helpers.VesselDataPrettyPrinter;
import org.fao.fi.vrmf.data.management.vessels.spi.VesselsDataManagementOperationsProvider;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

/**
 * @author Fiorellato
 *
 */
@Singleton @Named("vessel.console.command.sys.update")
public class UpdateSystemsConsoleCommand extends AbstractVesselConsoleCommand {
	static final private String UPDATE_DB_OPT		 = "d";
	static final private String UPDATE_DB_OPT_LONG	 = "database";

	static final private String UPDATE_CACHES_OPT	 = "c";
	static final private String UPDATE_CACHES_LONG	 = "caches";

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.vessels.commands.AbstractVesselConsoleCommand#getOptions()
	 */
	@SuppressWarnings("static-access")
	@Override
	public Options getOptions() {
		Options opts = new Options();
		opts.addOption(OptionBuilder.withDescription("Update the database accessory tables whose content might depend on the current status of the mappings").withLongOpt(UPDATE_DB_OPT_LONG).create(UPDATE_DB_OPT));
		opts.addOption(OptionBuilder.withDescription("Update the dissemination (front-end) caches to reflect changes in the database").withLongOpt(UPDATE_CACHES_LONG).create(UPDATE_CACHES_OPT));
		
		return opts;
	}
	
	@Override
	public String getCommandName() {
		return "update";
	}

	@Override
	public String getCommandDescription() {
		return "Perform various update tasks on the database and / or the dissemination caches";
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.spi.ConsoleCommand#hasArguments()
	 */
	@Override
	public boolean hasArguments() {
		return false;
	}

	@Override
	protected void doExecute(Properties properties, CommandLine commandLine, VesselsDataManagementOperationsProvider provider, AbstractEmitter emitter) throws Exception {
		ArgumentsChecker.doesNotRequireArguments(this, commandLine.getArgs());

		boolean updateDB 	 = commandLine.hasOption(UPDATE_DB_OPT);
		boolean updateCaches = commandLine.hasOption(UPDATE_CACHES_OPT);
		
		boolean noOptions = !updateDB && !updateCaches;
		
		updateDB |= noOptions;
		updateCaches |= noOptions;
		
		boolean confirm = false;
		
		if(!noOptions || ( confirm = emitter.confirm("This will launch all the currently available update processes. Are you sure you want to continue") )) {
			if(noOptions)
				emitter.nl();
			
			if(updateDB) {
				try {
					this.doExecuteUpdateDB(properties, commandLine, provider, emitter);
				} catch(Exception e) {
					if(!noOptions)
						throw e;
					else {
						emitter.nl();
						emitter.error("Unable to perform the required database update: " + e.getMessage());
					}
				}
			}
			
			if(updateCaches) {
				try {
					if(updateDB)
						emitter.nl();
					
					this.doExecuteUpdateCaches(properties, commandLine, provider, emitter);
				} catch(Exception e) {
					if(!noOptions)
						throw e;
					else {
						emitter.nl();
						emitter.error("Unable to perform the required caches update: " + e.getMessage());
					}
				}
			}
		}
		
		if(noOptions && !confirm)
			emitter.abort();
	}
	
	private void doExecuteUpdateDB(Properties properties, CommandLine commandLine, VesselsDataManagementOperationsProvider provider, AbstractEmitter emitter) throws Exception {
		try {
			emitter.banner("Database update process");
			emitter.nl();
			
			if(!this.isInteractive(properties) || emitter.confirm("Are you sure you want to launch the database update process")) {
				emitter.nl();
				
				emitter.info(" >> Invoking the DB update procedure... Please wait!");
				
				DBUpdateResult result = provider.updateDB();
				
				emitter.nl();
				emitter.info(" -> The DB has been correctly updated");
				emitter.nl();
				
				emitter.highlight("DB update report:");
				emitter.nl();
				
				for(String in : VesselDataPrettyPrinter.format(AbstractEmitter.DEFAULT_INFO_COLOR, result).split(VesselDataPrettyPrinter.SEPARATOR, -1))
					if(in != null && !"".equals(in)) emitter.info(" -> " + in);
			}
		} catch(Throwable t) {
			emitter.nl();
			
			throw new ConsoleCommandException("Unable to update database: " + t.getMessage(), t);
		}
	}
	
	private void doExecuteUpdateCaches(Properties properties, CommandLine commandLine, VesselsDataManagementOperationsProvider provider, AbstractEmitter emitter) throws Exception {
		String cacheEndpoint = properties.getProperty(AbstractConsole.CACHE_MANAGEMENT_ENDPOINT_PROPERTY);
		boolean cacheRequiresAuthentication = Boolean.parseBoolean(properties.getProperty(AbstractConsole.CACHE_MANAGEMENT_SHOULD_AUTHENTICATE_PROPERTY));
		String cacheUser = properties.getProperty(AbstractConsole.CACHE_MANAGEMENT_USERNAME_PROPERTY);
		String cachePassword = properties.getProperty(AbstractConsole.CACHE_MANAGEMENT_PASSWORD_PROPERTY);
		
		try {
			emitter.banner("Front-end cache update process");
			emitter.nl();
			
			if(!this.isInteractive(properties) || emitter.confirm("Are you sure you want to launch the front-end cache update process")) {
				emitter.nl();
				emitter.info(" -> Clearing front-end caches through a DELETE request to " + cacheEndpoint);
				
				Client client = ClientBuilder.newClient();
				
				if(cacheRequiresAuthentication)
					client.register(HttpAuthenticationFeature.basic(cacheUser, cachePassword));
				
				Response response = client.target(cacheEndpoint).request().delete();
				
				if(Status.OK.getStatusCode() == response.getStatusInfo().getStatusCode()) {
					emitter.nl();
					
					emitter.highlight("Response text - BEGIN", '-');
					
					String message = response.readEntity(String.class);
					
					for(String line : message.split("\n|\r")) {
						line = line == null ? null : line.trim();
						
						if(!"".equals(line))
							emitter.info("   " + line);
					}
					
					emitter.highlight("Response text - END", '-');
				} else 
					throw new UnmanagedException("[ HTTP " + response.getStatus() + " - " + response.getStatusInfo().getReasonPhrase() + " ] caught while attempting to clear caches through a DELETE request to " + cacheEndpoint); 
			}
		} catch(Throwable t) {
			emitter.nl();
			
			throw new ConsoleCommandException("Unable to reset dissemination caches: " + t.getMessage(), t);
		}
	}
}
