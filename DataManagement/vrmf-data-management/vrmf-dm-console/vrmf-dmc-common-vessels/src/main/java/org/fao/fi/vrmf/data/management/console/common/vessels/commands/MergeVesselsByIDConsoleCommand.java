/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.common.vessels.commands;

import java.util.Properties;

import javax.inject.Named;
import javax.inject.Singleton;

import org.apache.commons.cli.CommandLine;
import org.fao.fi.sh.utility.topology.GraphNode;
import org.fao.fi.sh.utility.topology.WeightedGraph;
import org.fao.fi.sh.utility.topology.algorithms.cycles.detection.CyclesDetector;
import org.fao.fi.sh.utility.topology.algorithms.cycles.detection.impl.Tarjan;
import org.fao.fi.sh.utility.topology.impl.SimpleWeightValue;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.DifferentSourcesException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.HierarchyHasCyclesException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemAlreadyMappedException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemNotFoundException;
import org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter;
import org.fao.fi.vrmf.data.management.console.common.helpers.ArgumentsChecker;
import org.fao.fi.vrmf.data.management.console.common.spi.exceptions.ConsoleCommandException;
import org.fao.fi.vrmf.data.management.console.common.spi.exceptions.InvalidIDException;
import org.fao.fi.vrmf.data.management.console.common.vessels.helpers.VesselDataPrettyPrinter;
import org.fao.fi.vrmf.data.management.vessels.model.VesselData;
import org.fao.fi.vrmf.data.management.vessels.model.VesselDataHierarchy;
import org.fao.fi.vrmf.data.management.vessels.spi.VesselsDataManagementOperationsProvider;

/**
 * @author Fiorellato
 *
 */
@Singleton @Named("vessel.console.command.mergeVesselsByID")
public class MergeVesselsByIDConsoleCommand extends AbstractVesselConsoleCommand {
	@Override
	public String getCommandName() {
		return "merge";
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.spi.ConsoleCommand#hasArguments()
	 */
	@Override
	public boolean hasArguments() {
		return true;
	}

	@Override
	public String getCommandDescription() {
		return "Merge a source vessel with a target vessel, at the same time ensuring that data from the source vessel will not overlap with those of the target vessel";
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.spi.ConsoleCommand#getCommandArguments()
	 */
	@Override
	public String getCommandArguments() {
		return "<sourceVesselID> <targetVesselID>";
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.spi.impl.AbstractConsoleCommand#getCommandArgumentsDescriptions()
	 */
	@Override
	public String getCommandArgumentsDescriptions() {
		return "where <sourceVesselID> and <targetVesselID> are the source and target IMO / RFMO / VRMF vessel identifiers";
	}

	@Override
	protected void doExecute(Properties properties, CommandLine commandLine, VesselsDataManagementOperationsProvider provider, AbstractEmitter emitter) throws Exception {
		String[] parameters = commandLine.getArgs();
		
		ArgumentsChecker.requiresExactly(this, parameters, 2);
		
		try {
			String sourceVesselID = parameters[0];
			String targetVesselID = parameters[1];
	
			Integer sourceID, targetID, sourceUID, targetUID, sourceMapsTo, targetMapsTo;
	
			VesselDataHierarchy sourceHierarchy, targetHierarchy;
			
			sourceID = this.getVesselID(sourceVesselID, provider, emitter);
			targetID = this.getVesselID(targetVesselID, provider, emitter);
			
			VesselData source, target;
			
			source = provider.getDataByItemID(sourceID);
			target = provider.getDataByItemID(targetID);
			
			if(!source.getSourceSystem().equals(target.getSourceSystem()))
				throw new DifferentSourcesException("SOURCE " + this.describe(sourceVesselID, source) + " and TARGET " + this.describe(targetVesselID, target) + " belong to two different sources (" + source.getSourceSystem() + " and " + target.getSourceSystem() + " respectively)");

			sourceUID = source.getUid();
			sourceMapsTo = source.getMapsTo();
			
			targetUID = target.getUid();
			targetMapsTo = target.getMapsTo();
			
			if(sourceMapsTo != null && targetMapsTo != null && !sourceUID.equals(targetUID))
				throw new ItemAlreadyMappedException("SOURCE " + this.describe(sourceVesselID, source) + " and TARGET " + this.describe(targetVesselID, target) + " belong to two different hierarchies");
			
			sourceHierarchy = provider.getHierarchyByItemID(sourceID);
			targetHierarchy = provider.getHierarchyByItemID(targetID);

			WeightedGraph<Integer> graph = new WeightedGraph<>();
			
			graph = this.buildGraph(graph, targetHierarchy);

			for(VesselDataHierarchy in : sourceHierarchy.getChildren()) {
				in.getData().setMapsTo(targetID);
			}
			
			for(VesselDataHierarchy in : sourceHierarchy.getChildren()) {
				graph = this.buildGraph(graph, in);
			}
			
			CyclesDetector<Integer> detector = new Tarjan<Integer>();
			
			if(graph.hasCycles(detector)) {
				throw new HierarchyHasCyclesException("Merging " + this.describe(sourceVesselID, source) + " to " + this.describe(targetVesselID, target)  + " will introduce one or more cycles in the hierarchy");
			}

			if(this.isVerbose(properties)) {
				emitter.highlight("Current hierarchy for SOURCE " + this.describe(sourceVesselID, source) + ":");
				
				emitter.nl();
				this.emitHierarchy(sourceID, sourceHierarchy, emitter);
				emitter.nl();
				
				emitter.highlight("Current hierarchy for TARGET " + this.describe(targetVesselID, target) + ":");
				
				emitter.nl();
				this.emitHierarchy(targetID, targetHierarchy, emitter);
				emitter.nl();
			}
			
			if(this.isInteractive(properties) && !emitter.confirm("Are you sure you want to merge SOURCE " + this.describe(sourceVesselID, source) + " to TARGET " + this.describe(targetVesselID, target))) { 
				emitter.abort();
				
				return;
			}
			
			emitter.nl();
			
			provider.mergeVessels(sourceID, targetID);

			if(this.isVerbose(properties)) {
				targetHierarchy = provider.getHierarchyByItemID(targetID);
	
				emitter.highlight("New hierarchy for TARGET " + this.describe(targetVesselID, target) + ":");
				
				emitter.nl();
				emitter.info(VesselDataPrettyPrinter.format(AbstractEmitter.DEFAULT_INFO_COLOR, targetHierarchy, targetID));
			}
		} catch(ItemNotFoundException|ItemAlreadyMappedException|InvalidIDException|HierarchyHasCyclesException e) {
			throw new ConsoleCommandException(e.getMessage());
		}
	}
	
	private WeightedGraph<Integer> buildGraph(WeightedGraph<Integer> graph, VesselDataHierarchy hierarchy) {
		for(VesselDataHierarchy in : hierarchy.children()) {
			graph.link(
				new GraphNode<Integer>(in.getData().getId()),
				new GraphNode<Integer>(hierarchy.getData().getId()), 
				new SimpleWeightValue(in.getData().getMappingWeight())
			);
			
			graph = this.buildGraph(graph, in);
		}
		
		return graph;
	}
}