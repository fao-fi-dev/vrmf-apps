/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.common.vessels.commands;

import java.util.Properties;

import javax.inject.Named;
import javax.inject.Singleton;

import org.apache.commons.cli.CommandLine;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemHasMappingsException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemNotFoundException;
import org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter;
import org.fao.fi.vrmf.data.management.console.common.helpers.ArgumentsChecker;
import org.fao.fi.vrmf.data.management.console.common.spi.exceptions.ConsoleCommandException;
import org.fao.fi.vrmf.data.management.console.common.spi.exceptions.InvalidIDException;
import org.fao.fi.vrmf.data.management.vessels.model.VesselData;
import org.fao.fi.vrmf.data.management.vessels.spi.VesselsDataManagementOperationsProvider;

/**
 * @author Fiorellato
 *
 */
@Singleton @Named("vessel.console.command.deleteVessel")
public class DeleteVesselConsoleCommand extends AbstractVesselConsoleCommand {
	@Override
	public String getCommandName() {
		return "delete";
	}

	@Override
	public String getCommandDescription() {
		return "Delete a VRMF vessel by IMO / RFMO / VRMF identifier";
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.spi.ConsoleCommand#hasArguments()
	 */
	@Override
	public boolean hasArguments() {
		return true;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.spi.ConsoleCommand#getCommandArguments()
	 */
	@Override
	public String getCommandArguments() {
		return "<vesselID1> [<vesselID2> ...]";
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.spi.impl.AbstractConsoleCommand#getCommandArgumentsDescriptions()
	 */
	@Override
	public String getCommandArgumentsDescriptions() {
		return "where <vesselIDn> is an IMO / RFMO / VRMF vessel identifier";
	}

	@Override
	protected void doExecute(Properties properties, CommandLine commandLine, VesselsDataManagementOperationsProvider provider, AbstractEmitter emitter) throws Exception {
		String[] parameters = commandLine.getArgs();
		
		ArgumentsChecker.requiresAtLeast(this, parameters, 1);
		
		Integer ID = null;
		VesselData data = null;
		
		boolean isLast = false;
		
		int counter = 0;
		for(String providedID : parameters) {
			counter++;
			
			isLast = counter == parameters.length;
			
			try {
				ID = this.getVesselID(providedID, provider, emitter);
				
				if(ID != null) {
					data = provider.getDataByItemID(ID);

					if(this.isVerbose(properties)) {
						this.emitBasicVesselData(providedID, data, emitter);
					
						emitter.nl();
					}
					
					if(!this.isInteractive(properties) || emitter.confirm("Are you sure you want to delete this vessel")) {
						provider.delete(ID);
						
						emitter.nl();
						emitter.info(" -> The requested vessel was successfully deleted");
						if(!isLast) emitter.nl();
					} else {
						emitter.abort();
						if(!isLast) emitter.nl();
					}
				}
				
				ID = null;
			} catch(ItemNotFoundException e) {
				if(parameters.length == 1) {
					emitter.nl();
					throw new ConsoleCommandException(e.getMessage());
				}
				
				this.emitBasicVesselData(providedID, emitter);
				
				emitter.nl();
				
				emitter.error(e.getMessage());
				
				if(!isLast) emitter.nl();
			} catch(InvalidIDException|ItemHasMappingsException e) {
				if(parameters.length == 1) {
					emitter.nl();
					throw new ConsoleCommandException(e.getMessage());
				}
				
				emitter.nl();
				emitter.error(e.getMessage());
				
				if(!isLast) emitter.nl();
			}
		};
	}
}
