/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.common.vessels.commands;

import java.util.Properties;

import javax.inject.Named;
import javax.inject.Singleton;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemNotFoundException;
import org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter;
import org.fao.fi.vrmf.data.management.console.common.helpers.ArgumentsChecker;
import org.fao.fi.vrmf.data.management.console.common.spi.exceptions.ConsoleCommandException;
import org.fao.fi.vrmf.data.management.console.common.spi.exceptions.InvalidIDException;
import org.fao.fi.vrmf.data.management.vessels.model.VesselDataHierarchy;
import org.fao.fi.vrmf.data.management.vessels.spi.VesselsDataManagementOperationsProvider;

/**
 * @author Fiorellato
 *
 */
@Singleton @Named("vessel.console.command.getHierarchy")
public class GetHierarchyConsoleCommand extends AbstractVesselConsoleCommand {
	static final private String BELOW_OPT		 = "b";
	static final private String BELOW_OPT_LONG	 = "below";

	static final private String UID_OPT			 = "u";
	static final private String UID_OPT_LONG	 = "uid";

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.vessels.commands.AbstractVesselConsoleCommand#getOptions()
	 */
	@SuppressWarnings("static-access")
	@Override
	public Options getOptions() {
		Options opts = new Options();
		opts.addOption(OptionBuilder.withDescription("Selects the hierarchy below (and including) the vessel(s) with the provided ID(s). Does not work in combination with the \"-" + UID_OPT + "\" (\"--" + UID_OPT_LONG + "\") option").withLongOpt(BELOW_OPT_LONG).create(BELOW_OPT));
		opts.addOption(OptionBuilder.withDescription("Forces the command argument(s) to be interpreted as a vessel UID. Does not work in combination with the \"-" + BELOW_OPT + "\" (\"--" + BELOW_OPT_LONG + "\") option").withLongOpt(UID_OPT_LONG).create(UID_OPT));
		
		return opts;
	}
	
	@Override
	public String getCommandName() {
		return "hierarchy";
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.spi.ConsoleCommand#hasArguments()
	 */
	@Override
	public boolean hasArguments() {
		return true;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.spi.ConsoleCommand#getCommandArguments()
	 */
	@Override
	public String getCommandArguments() {
		return "<vesselID1|vesselUID1> [<vesselID2|vesselUID2> ...]";
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.spi.impl.AbstractConsoleCommand#getCommandArgumentsDescriptions()
	 */
	@Override
	public String getCommandArgumentsDescriptions() {
		return "where <vesselIDn|vesselUIDn> are either an IMO / RFMO / VRMF vessel identifier or a VRMF vessel UID";
	}

	@Override
	public String getCommandDescription() {
		return "Display a vessel hierarchy (\"full\" by default, otherwise below and including the selected vessel(s) if the \"-" + BELOW_OPT + "\" / \"--" + BELOW_OPT_LONG + "\" option is enabled)";
	}
	
	@Override
	protected void doExecute(Properties properties, CommandLine commandLine, VesselsDataManagementOperationsProvider provider, AbstractEmitter emitter) throws Exception {
		String[] parameters = commandLine.getArgs();

		ArgumentsChecker.requiresAtLeast(this, parameters, 1);
		
		boolean optBelow = commandLine.hasOption(BELOW_OPT);
		boolean optUid = commandLine.hasOption(UID_OPT);
		
		if(optBelow && optUid) 
			throw new ConsoleCommandException("The \"-" + BELOW_OPT + "\" (\"--" + BELOW_OPT_LONG + "\") and \"-" + UID_OPT + "\" (\"--" + UID_OPT_LONG + "\") options cannot be enabled at the same time");
		
		Integer ID = null, UID = null;
		
		boolean isFirst = true;
		
		int numIDs = parameters.length;
		
		VesselDataHierarchy hierarchy = null;
		
		for(String providedID : parameters) {
			try {
				if(optUid) {
					try {
						UID = Integer.parseInt(providedID);
					} catch(NumberFormatException NFe) {
						throw new InvalidIDException("Provided UID (" + providedID + ") is not a valid VRMF UID");
					}
					
					hierarchy = provider.getHierarchyByItemUID(UID);
				} else {
					ID = this.getVesselID(providedID, provider, emitter);

					if(ID != null) {
						hierarchy = optBelow ? provider.getHierarchyBelowByItemID(ID) : provider.getHierarchyByItemID(ID);
					}
				}
				
				if(hierarchy != null) {
					if(isFirst)
						isFirst = false;
					else
						emitter.nl();
					
					if(optUid)
						emitter.highlight("Hierarchy for vessels with VRMF UID #" + providedID + ":");
					else
						emitter.highlight("Hierarchy for " + this.describe(providedID, hierarchy.getData()) + ":");

					emitter.nl();
					
					if(optUid)
						this.emitHierarchy(hierarchy, emitter);
					else
						this.emitHierarchy(ID, hierarchy, emitter);
				}

			} catch(ItemNotFoundException|InvalidIDException e) {
				if(!isFirst) emitter.nl();
				
				if(optUid)
					emitter.highlight("Hierarchy for vessels with VRMF UID #" + providedID + ":");
				else
					emitter.highlight("Hierarchy for " + this.describe(providedID) + ":");

				emitter.nl();
				emitter.error(e.getMessage());
				
				if(isFirst && numIDs > 1) emitter.nl();
			}
		}
	}

}
