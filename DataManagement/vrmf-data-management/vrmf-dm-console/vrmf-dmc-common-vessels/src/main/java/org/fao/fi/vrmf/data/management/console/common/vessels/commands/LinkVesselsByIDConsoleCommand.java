/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.common.vessels.commands;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.inject.Named;
import javax.inject.Singleton;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.fao.fi.sh.utility.topology.GraphNode;
import org.fao.fi.sh.utility.topology.WeightedGraph;
import org.fao.fi.sh.utility.topology.algorithms.cycles.detection.CyclesDetector;
import org.fao.fi.sh.utility.topology.algorithms.cycles.detection.impl.Tarjan;
import org.fao.fi.sh.utility.topology.impl.SimpleWeightValue;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.HierarchyHasCyclesException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemAlreadyMappedException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemNotFoundException;
import org.fao.fi.vrmf.data.management.console.common.AbstractConsole;
import org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter;
import org.fao.fi.vrmf.data.management.console.common.helpers.ArgumentsChecker;
import org.fao.fi.vrmf.data.management.console.common.spi.exceptions.ConsoleCommandException;
import org.fao.fi.vrmf.data.management.console.common.spi.exceptions.InvalidIDException;
import org.fao.fi.vrmf.data.management.vessels.model.VesselData;
import org.fao.fi.vrmf.data.management.vessels.model.VesselDataHierarchy;
import org.fao.fi.vrmf.data.management.vessels.spi.VesselsDataManagementOperationsProvider;

/**
 * @author Fiorellato
 *
 */
@Singleton @Named("vessel.console.command.linkVesselsByID")
public class LinkVesselsByIDConsoleCommand extends AbstractVesselConsoleCommand {
	static final private String DATE_FORMAT_PATTERN = "yyyy/MM/dd HH:mm:ss";
	static final private SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(DATE_FORMAT_PATTERN);
	
	static final private String MAPPING_SCORE_OPT		 = "s";
	static final private String MAPPING_SCORE_OPT_LONG	 = "score";

	static final private String MAPPING_USER_OPT		 = "u";
	static final private String MAPPING_USER_OPT_LONG	 = "user";

	static final private String MAPPING_DATE_OPT		 = "d";
	static final private String MAPPING_DATE_OPT_LONG	 = "date";

	static final private String MAPPING_COMMENT_OPT		 = "c";
	static final private String MAPPING_COMMENT_OPT_LONG = "comment";

	@Override
	public String getCommandName() {
		return "link";
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.vessels.commands.AbstractVesselConsoleCommand#getOptions()
	 */
	@SuppressWarnings("static-access")
	@Override
	public Options getOptions() {
		Options opts = new Options();
		opts.addOption(OptionBuilder.withArgName("mapping score").hasArg().withDescription("The mapping score (a decimal value in the range (0.0, 1.0]). Defaults to 1.0").withLongOpt(MAPPING_SCORE_OPT_LONG).create(MAPPING_SCORE_OPT));
		opts.addOption(OptionBuilder.withArgName("mapping user ID").hasArg().withDescription("The ID of the user responsible for creating the mapping. Defaults to the current value set in the '" + AbstractConsole.UPDATER_PROPERTY + "' console property").withLongOpt(MAPPING_USER_OPT_LONG).create(MAPPING_USER_OPT));
		opts.addOption(OptionBuilder.withArgName("mapping date").hasArg().withDescription("The mapping date (in '" + DATE_FORMAT_PATTERN + "' format). Defaults to the time of the request").withLongOpt(MAPPING_DATE_OPT_LONG).create(MAPPING_DATE_OPT));
		opts.addOption(OptionBuilder.withArgName("mapping comment").hasArg().withDescription("The mapping comment").withLongOpt(MAPPING_COMMENT_OPT_LONG).create(MAPPING_COMMENT_OPT));
		
		return opts;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.spi.ConsoleCommand#hasArguments()
	 */
	@Override
	public boolean hasArguments() {
		return true;
	}

	@Override
	public String getCommandDescription() {
		return "Link a source vessel to a target vessel";
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.spi.ConsoleCommand#getCommandArguments()
	 */
	@Override
	public String getCommandArguments() {
		return "<sourceVesselID> <targetVesselID>";
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.spi.impl.AbstractConsoleCommand#getCommandArgumentsDescriptions()
	 */
	@Override
	public String getCommandArgumentsDescriptions() {
		return "where <sourceVesselID> and <targetVesselID> are the source and target IMO / RFMO / VRMF vessel identifiers";
	}

	@Override
	protected void doExecute(Properties properties, CommandLine commandLine, VesselsDataManagementOperationsProvider provider, AbstractEmitter emitter) throws Exception {
		String[] parameters = commandLine.getArgs();
		
		ArgumentsChecker.requiresExactly(this, parameters, 2);
		
		try {
			String sourceVesselID = parameters[0];
			String targetVesselID = parameters[1];
			
			Date mappingDate = new Date();
			Double mappingScore = 1D;
			
			String mappingUser = properties.getProperty(AbstractConsole.UPDATER_PROPERTY);
			String mappingComment = "Explicitly mapped through the VRMF Vessel Data Management console";
			
			if(commandLine.hasOption(MAPPING_DATE_OPT)) {
				try {
					mappingDate = DATE_FORMAT.parse(commandLine.getOptionValue(MAPPING_DATE_OPT));
				} catch(ParseException Pe) {
					throw new ConsoleCommandException("Mapping date '" + commandLine.getOptionValue(MAPPING_DATE_OPT) + "' is not in the " + DATE_FORMAT_PATTERN + " format");
				}
			}
			
			if(commandLine.hasOption(MAPPING_SCORE_OPT)) {
				try {
					mappingScore = Double.parseDouble(commandLine.getOptionValue(MAPPING_SCORE_OPT));
				} catch(NumberFormatException NFe) {
					throw new ConsoleCommandException("Mapping score '" + commandLine.getOptionValue(MAPPING_SCORE_OPT) + "' is not a valid value");
				}
			}
			
			if(Double.compare(mappingScore, 0D) <= 0 || Double.compare(mappingScore, 1D) > 0)
				throw new ConsoleCommandException("Mapping score should be in the range (0.0, 1.0] (currently: " + mappingScore + ")");
			
			if(commandLine.hasOption(MAPPING_USER_OPT))
				mappingUser = commandLine.getOptionValue(MAPPING_USER_OPT);
			
			if(commandLine.hasOption(MAPPING_COMMENT_OPT))
				mappingComment = commandLine.getOptionValue(MAPPING_COMMENT_OPT);
	
			Integer sourceID, targetID;
	
			VesselDataHierarchy sourceHierarchy, targetHierarchy;
			
			sourceID = this.getVesselID(sourceVesselID, provider, emitter);
			targetID = this.getVesselID(targetVesselID, provider, emitter);
			
			VesselData source, target;
			
			source = provider.getDataByItemID(sourceID);
			target = provider.getDataByItemID(targetID);
			
			if(target.getId().equals(source.getMapsTo()))
				throw new ItemAlreadyMappedException("Source " + this.describe(sourceVesselID, source) + " is already mapped to TARGET " + this.describe(targetVesselID, target));

			if(source.getMapsTo() != null)
				throw new ItemAlreadyMappedException("Source " + this.describe(sourceVesselID, source) + " is already mapped to " + this.describe(source.getMapsTo()));
			
			sourceHierarchy = provider.getHierarchyByItemID(sourceID);
			targetHierarchy = provider.getHierarchyByItemID(targetID);
			
			WeightedGraph<Integer> graph = new WeightedGraph<>();
			
			graph = this.buildGraph(graph, sourceHierarchy);
			graph = this.buildGraph(graph, targetHierarchy);
			
			graph.link(
				new GraphNode<Integer>(sourceID), 
				new GraphNode<Integer>(targetID), 
				new SimpleWeightValue(mappingScore)
			);
			
			CyclesDetector<Integer> detector = new Tarjan<Integer>();
			
			if(graph.hasCycles(detector)) {
				throw new HierarchyHasCyclesException("Linking " + this.describe(sourceVesselID, source) + " to " + this.describe(targetVesselID, target)  + " will introduce one or more cycles in the hierarchy");
			}
			
			if(this.isVerbose(properties)) {
				emitter.highlight("Current hierarchy for SOURCE " + this.describe(sourceVesselID, source) + "):");
				
				emitter.nl();
				this.emitHierarchy(sourceID, sourceHierarchy, emitter);
				emitter.nl();
				
				emitter.highlight("Current hierarchy for TARGET " + this.describe(targetVesselID, target) + "):");
				
				emitter.nl();
				this.emitHierarchy(targetID, targetHierarchy, emitter);
				emitter.nl();
			}
			
			if(this.isInteractive(properties) && !emitter.confirm("Are you sure you want to link SOURCE " + this.describe(sourceVesselID, source) + " to TARGET " + this.describe(targetVesselID, target))) { 
				emitter.abort();
				
				return;
			}
			
			emitter.nl();
			
			provider.link(sourceID, targetID, mappingUser, mappingScore, mappingDate, mappingComment);

			if(this.isVerbose(properties)) {
				sourceHierarchy = provider.getHierarchyByItemID(sourceID);
	
				emitter.highlight("New hierarchy for SOURCE " + this.describe(sourceVesselID, source) + ":");
				
				emitter.nl();
				this.emitHierarchy(sourceID, sourceHierarchy, emitter);
			}
		} catch(ItemNotFoundException|ItemAlreadyMappedException|InvalidIDException|HierarchyHasCyclesException e) {
			throw new ConsoleCommandException(e.getMessage());
		}
	}
	
	private WeightedGraph<Integer> buildGraph(WeightedGraph<Integer> graph, VesselDataHierarchy hierarchy) {
		for(VesselDataHierarchy in : hierarchy.children()) {
			graph.link(
				new GraphNode<Integer>(in.getData().getId()),
				new GraphNode<Integer>(hierarchy.getData().getId()), 
				new SimpleWeightValue(in.getData().getMappingWeight())
			);
			
			graph = this.buildGraph(graph, in);
		}
		
		return graph;
	}
}