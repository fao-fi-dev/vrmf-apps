/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.common.vessels.commands;

import java.awt.Desktop;
import java.net.URI;
import java.util.Collection;
import java.util.Properties;

import javax.inject.Named;
import javax.inject.Singleton;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemNotFoundException;
import org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter;
import org.fao.fi.vrmf.data.management.console.common.helpers.ArgumentsChecker;
import org.fao.fi.vrmf.data.management.console.common.spi.exceptions.ConsoleCommandException;
import org.fao.fi.vrmf.data.management.console.common.spi.exceptions.InvalidIDException;
import org.fao.fi.vrmf.data.management.vessels.model.VesselData;
import org.fao.fi.vrmf.data.management.vessels.spi.VesselsDataManagementOperationsProvider;

/**
 * @author Fiorellato
 *
 */
@Singleton @Named("vessel.console.command.showVesselData")
public class ShowVesselDataConsoleCommand extends AbstractVesselConsoleCommand {
	static final private String UID_OPT			 = "u";
	static final private String UID_OPT_LONG	 = "uid";

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.vessels.commands.AbstractVesselConsoleCommand#getOptions()
	 */
	@SuppressWarnings("static-access")
	@Override
	public Options getOptions() {
		Options opts = new Options();
		opts.addOption(OptionBuilder.withDescription("Forces the command argument(s) to be interpreted as a vessel UID.").withLongOpt(UID_OPT_LONG).create(UID_OPT));
		
		return opts;
	}
	
	@Override
	public String getCommandName() {
		return "show";
	}

	@Override
	public String getCommandDescription() {
		return "Show vessel's basic data as reported by the original data owner (will open a browser window)";
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.spi.ConsoleCommand#hasArguments()
	 */
	@Override
	public boolean hasArguments() {
		return true;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.spi.ConsoleCommand#getCommandArguments()
	 */
	@Override
	public String getCommandArguments() {
		return "<vesselID1|vesselUID1> [<vesselID2|vesselUID2> ...]";
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.spi.impl.AbstractConsoleCommand#getCommandArgumentsDescriptions()
	 */
	@Override
	public String getCommandArgumentsDescriptions() {
		return "where <vesselIDn|vesselUIDn> is either an IMO / RFMO / VRMF vessel identifier or a VRMF vessel UID";
	}

	@Override
	protected void doExecute(Properties properties, CommandLine commandLine, VesselsDataManagementOperationsProvider provider, AbstractEmitter emitter) throws Exception {
		String[] parameters = commandLine.getArgs();
		
		ArgumentsChecker.requiresAtLeast(this, parameters, 1);
		
		boolean optUid = commandLine.hasOption(UID_OPT);

		Integer ID = null, UID = null;
		
		int counter = 0, subCounter = 0, numIDs = parameters.length;
		
		for(String providedID : parameters) {
			subCounter = 0;
			
			try {
				if(optUid) {
					emitter.banner("Showing original vessel data for vessels with VRMF UID #" + providedID);
					emitter.nl();
					
					try {
						UID = Integer.parseInt(providedID);
					} catch(NumberFormatException NFe) {
						throw new InvalidIDException("Provided UID (" + providedID + ") is not a valid VRMF UID");
					}
					
					try {
						Collection<VesselData> all = provider.getDataByItemUID(UID);
						
						for(VesselData in : all) {
							this.showData(emitter, in);
	
							subCounter++;
							
							if(subCounter < all.size())
								emitter.nl();
						}
					} catch(ItemNotFoundException e) {
						emitter.error(e.getMessage());
					} catch(Exception e) {
						throw e;
					}
				} else {
					ID = this.getVesselID(providedID, provider, emitter);
					
					VesselData selected = provider.getDataByItemID(ID);
					
					this.showData(emitter, selected);
				}
			} catch(Exception e) {
				if(numIDs == 1)
					throw new ConsoleCommandException(e.getMessage(), e);
				else {
					emitter.underline("Showing data for vessel #" + providedID);
					
					emitter.nl();
					emitter.error(e.getMessage());
				}
			} 

			counter++;
			
			if(counter < numIDs)
				emitter.nl();
		}
	}
	
	private void showData(AbstractEmitter emitter, VesselData data) throws ConsoleCommandException {
		String URL = this.getURL(data.getSourceSystem(), data.getIdentifier(), data.getAlternateIdentifier());

		emitter.underline("Showing " + data.getSourceSystem() + " data for vessel #" + data.getIdentifier());
		emitter.info(" -> Opening URL " + URL);
		
		try {
			Desktop.getDesktop().browse(new URI(URL));
		} catch(Throwable t) {
			throw new ConsoleCommandException("Unable to show " + data.getSourceSystem() + " data for vessel #" + data.getIdentifier() + ": " + t.getMessage(), t);
		}
	}
	
	private String getURL(String source, String identifier, String alternateIdentifier) throws ConsoleCommandException {
		switch(source) {
			case "CCSBT":
				return "http://www.ccsbt.org/site/authorised_vessels_detail.php?id=" + alternateIdentifier;
			case "IATTC":
				return "http://www.iattc.org/VesselRegister/VesselDetails.aspx?Lang=en&VesNo=" + identifier;
			case "ICCAT":
				return "http://www.iccat.int/en/VesselsRecordDet.asp?id=" + alternateIdentifier;
			case "IOTC":
				return "http://www.iotc.org/vessels/history/" + alternateIdentifier;
			case "WCPFC":
				return "http://www.wcpfc.int/record-fishing-vessel-database?vid=" + identifier;
		}
		
		throw new ConsoleCommandException("No public dissemination URL is available for " + source + " vessel #" + identifier);
	}
}