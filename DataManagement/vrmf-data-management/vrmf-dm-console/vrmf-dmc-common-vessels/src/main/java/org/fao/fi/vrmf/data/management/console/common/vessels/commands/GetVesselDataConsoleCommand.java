/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.common.vessels.commands;

import java.util.Collection;
import java.util.Properties;

import javax.inject.Named;
import javax.inject.Singleton;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemNotFoundException;
import org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter;
import org.fao.fi.vrmf.data.management.console.common.helpers.ArgumentsChecker;
import org.fao.fi.vrmf.data.management.console.common.spi.exceptions.ConsoleCommandException;
import org.fao.fi.vrmf.data.management.console.common.spi.exceptions.InvalidIDException;
import org.fao.fi.vrmf.data.management.vessels.model.VesselData;
import org.fao.fi.vrmf.data.management.vessels.spi.VesselsDataManagementOperationsProvider;

/**
 * @author Fiorellato
 *
 */
@Singleton @Named("vessel.console.command.getVesselData")
public class GetVesselDataConsoleCommand extends AbstractVesselConsoleCommand {
	static final private String UID_OPT			 = "u";
	static final private String UID_OPT_LONG	 = "uid";

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.vessels.commands.AbstractVesselConsoleCommand#getOptions()
	 */
	@SuppressWarnings("static-access")
	@Override
	public Options getOptions() {
		Options opts = new Options();
		opts.addOption(OptionBuilder.withDescription("Forces the command argument(s) to be interpreted as a vessel UID.").withLongOpt(UID_OPT_LONG).create(UID_OPT));
		
		return opts;
	}
	
	@Override
	public String getCommandName() {
		return "data";
	}

	@Override
	public String getCommandDescription() {
		return "Display vessel's basic data (including VRMF ID and mapping details)";
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.spi.ConsoleCommand#hasArguments()
	 */
	@Override
	public boolean hasArguments() {
		return true;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.spi.ConsoleCommand#getCommandArguments()
	 */
	@Override
	public String getCommandArguments() {
		return "<vesselID1|vesselUID1> [<vesselID2|vesselUID2> ...]";
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.spi.impl.AbstractConsoleCommand#getCommandArgumentsDescriptions()
	 */
	@Override
	public String getCommandArgumentsDescriptions() {
		return "where <vesselIDn|vesselUIDn> is either an IMO / RFMO / VRMF vessel identifier or a VRMF vessel UID";
	}

	@Override
	protected void doExecute(Properties properties, CommandLine commandLine, VesselsDataManagementOperationsProvider provider, AbstractEmitter emitter) throws Exception {
		String[] parameters = commandLine.getArgs();
		
		ArgumentsChecker.requiresAtLeast(this, parameters, 1);
		
		boolean optUid = commandLine.hasOption(UID_OPT);

		Integer ID = null, UID = null;
		
		int counter = 0, subCounter = 0, numIDs = parameters.length;
		
		for(String providedID : parameters) {
			subCounter = 0;
			
			try {
				if(optUid) {
					emitter.banner("Basic vessel data for vessels with VRMF UID #" + providedID);
					emitter.nl();
					
					try {
						UID = Integer.parseInt(providedID);
					} catch(NumberFormatException NFe) {
						throw new InvalidIDException("Provided UID (" + providedID + ") is not a valid VRMF UID");
					}
					
					try {
						Collection<VesselData> all = provider.getDataByItemUID(UID);
						
						for(VesselData in : all) {
							this.emitBasicVesselData(in.getId().toString(), in, emitter);
	
							subCounter++;
							
							if(subCounter < all.size())
								emitter.nl();
						}
					} catch(ItemNotFoundException e) {
						emitter.error(e.getMessage());
					} catch(Exception e) {
						throw e;
					}
				} else {
					ID = this.getVesselID(providedID, provider, emitter);
					
					VesselData selected = provider.getDataByItemID(ID);
					
					this.emitBasicVesselData(providedID, selected, emitter);
				}
			} catch(ItemNotFoundException|InvalidIDException e) {
				if(numIDs == 1)
					throw new ConsoleCommandException(e.getMessage());
				else {
					this.emitBasicVesselData(providedID, emitter);
					
					emitter.nl();
					emitter.error(e.getMessage());
				}
			} 

			counter++;
			
			if(counter < numIDs)
				emitter.nl();
		}
	}
}
