/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.common.vessels;

import static org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter.bold;
import static org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter.DEFAULT_COMMAND_COLOR;
import static org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter.DEFAULT_COMMAND_DESC_COLOR;

import java.util.List;
import java.util.Properties;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.vrmf.data.management.console.common.AbstractConsole;
import org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter.Color;
import org.fao.fi.vrmf.data.management.console.common.helpers.ArgumentsChecker;
import org.fao.fi.vrmf.data.management.console.common.spi.exceptions.ConsoleCommandException;
import org.fao.fi.vrmf.data.management.console.common.vessels.commands.AbstractVesselConsoleCommand;
import org.fao.fi.vrmf.data.management.vessels.model.VesselData;
import org.fao.fi.vrmf.data.management.vessels.model.VesselDataHierarchy;
import org.fao.fi.vrmf.data.management.vessels.spi.VesselsDataManagementOperationsProvider;

/**
 * @author Fiorellato
 *
 */
@Singleton @Named("management.console.vessels")
public class VesselsManagementConsole extends AbstractConsole<VesselData, VesselDataHierarchy, VesselsDataManagementOperationsProvider, AbstractVesselConsoleCommand> {
	static final private String IMO_ALIAS = "imo";
	static final private String IMO_ALIAS_COMMAND = "id --historical IMO#{0}";
	
	@Inject @Named("vessels.data.management.provider") protected void setOperationsProvider(VesselsDataManagementOperationsProvider provider) {
		this._provider = provider;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.AbstractConsole#setAvailableCommands(java.util.List)
	 */
	@Override
	@Inject protected void setAvailableCommands(List<AbstractVesselConsoleCommand> commands) {
		this._availableCommands = commands;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.AbstractConsole#parseAndExecuteCustomCommands(java.lang.String, java.lang.String[])
	 */
	@Override
	protected boolean parseAndExecuteCustomCommands(String command, String[] parameters) throws Exception {
		return false;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.AbstractConsole#doParseAndExecuteAliases(java.lang.String, java.lang.String[])
	 */
	@Override
	protected boolean doParseAndExecuteAliases(String alias, String[] parameters) throws Exception {
		String command = null;
		
		switch(alias) {
			case IMO_ALIAS:
				ArgumentsChecker.requiresExactly(IMO_ALIAS, parameters, 1);
				command = IMO_ALIAS_COMMAND;
				break;
			default:
				command = null;
		}
		
		if(command != null) {
			int counter = 0;
			for(String parameter : parameters) {
				command = command.replaceAll("\\{" + counter++ + "\\}", parameter);
			}
			
			super.parseAndExecute(command, true);
			
			return true;
		}
		
		return false;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.AbstractConsole#doShowAliases()
	 */
	@Override
	protected void doShowAliases() {
		this._emitter.emit(DEFAULT_COMMAND_COLOR + " - " + IMO_ALIAS + " " + bold(Color.GREEN) + "<IMO number>");
		this._emitter.info(DEFAULT_COMMAND_DESC_COLOR + "   => Same as '" + bold(Color.YELLOW) + "id --historical IMO#" + bold(Color.GREEN) + "<IMO number>" + DEFAULT_COMMAND_DESC_COLOR + "'");
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.AbstractConsole#doGetAdditionalProperties()
	 */
	@Override
	protected Properties doGetAdditionalProperties() {
		return new Properties();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.AbstractConsole#notifyPropertyChanged(java.lang.String, java.lang.String)
	 */
	@Override
	protected String doNotifyPropertyChanged(String property, String value) throws ConsoleCommandException {
		return value;
	}
}