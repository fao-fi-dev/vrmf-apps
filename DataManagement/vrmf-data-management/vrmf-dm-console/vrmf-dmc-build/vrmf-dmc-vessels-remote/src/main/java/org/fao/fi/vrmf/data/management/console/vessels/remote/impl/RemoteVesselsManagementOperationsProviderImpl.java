/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.vessels.remote.impl;

import java.net.MalformedURLException;
import java.util.Collection;
import java.util.Date;
import java.util.Properties;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.vrmf.data.management.common.model.ConsistencyCheckResult;
import org.fao.fi.vrmf.data.management.common.model.DBUpdateResult;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.DataManagementOperationException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.DataManagementOperationPropertyException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.UnmanagedException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.DifferentSourcesException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.HierarchyHasCyclesException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemAlreadyMappedException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemHasMappingsException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemIsNotMappedException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemNotFoundException;
import org.fao.fi.vrmf.data.management.console.web.client.vessels.VesselsDataManagementRemoteOperationsProviderClient;
import org.fao.fi.vrmf.data.management.vessels.model.MultipleIMOByUIDCheckResult;
import org.fao.fi.vrmf.data.management.vessels.model.MultipleUIDByIMOCheckResult;
import org.fao.fi.vrmf.data.management.vessels.model.VesselData;
import org.fao.fi.vrmf.data.management.vessels.model.VesselDataHierarchy;
import org.fao.fi.vrmf.data.management.vessels.spi.VesselsDataManagementOperationsProvider;

/**
 * @author Fiorellato
 */

@Singleton @Named("vessels.data.management.provider")
public class RemoteVesselsManagementOperationsProviderImpl implements VesselsDataManagementOperationsProvider {
	static final public String REMOTE_CLIENT_ENDPOINT_PROPERTY = "remote.client.endpoint";
	static final public String REMOTE_CLIENT_USERNAME_PROPERTY = "remote.client.username";
	static final public String REMOTE_CLIENT_PASSWORD_PROPERTY = "remote.client.password";
	
	@Inject private @Named("remote.client.vessels") VesselsDataManagementRemoteOperationsProviderClient _client;
	
	public RemoteVesselsManagementOperationsProviderImpl() {
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.common.spi.DataManagementOperationsProvider#notifyPropertyChanged(java.lang.String, java.lang.String)
	 */
	@Override
	public String notifyPropertyChanged(String property, String value) throws DataManagementOperationPropertyException {
		try {
			switch(property) {
				case REMOTE_CLIENT_ENDPOINT_PROPERTY: 
					this._client.setEndpoint(value);
					return this._client.getEndpoint();
				case REMOTE_CLIENT_USERNAME_PROPERTY:
					this._client.setUsername(value);
					return this._client.getUsername();
				case REMOTE_CLIENT_PASSWORD_PROPERTY:
					this._client.setPassword(value);
					return this._client.getPassword();
				default:
					return value;
			}
		} catch(MalformedURLException MFe) {
			throw new DataManagementOperationPropertyException(value + " is not a valid endpoint URL");
		}
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.common.spi.DataManagementOperationsProvider#properties()
	 */
	@Override
	public Properties properties() {
		Properties current = new Properties();
		current.put(REMOTE_CLIENT_ENDPOINT_PROPERTY, this._client.getEndpoint());
		current.put(REMOTE_CLIENT_USERNAME_PROPERTY, this._client.getUsername());
		current.put(REMOTE_CLIENT_PASSWORD_PROPERTY, this._client.getPassword());
		
		return current;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.common.spi.DataManagementOperationsProvider#description()
	 */
	@Override
	public String description() {
		return "An implementation of the Vessels Data Management operations provider that accesses data remotly through a REST WS client pointing to " + this._client.getEndpoint();
	}

	@Override
	public int getVesselIDByCurrentIdentifier(String source, String identifier) throws ItemNotFoundException, UnmanagedException, DataManagementOperationException {
		return this._client.getIDByCurrentIdentifier(source, identifier).getVesselId();
	}
	
	@Override
	public int[] getVesselIDByHistoricalIdentifier(String source, String identifier) throws ItemNotFoundException, UnmanagedException, DataManagementOperationException {
		return this._client.getIDByHistoricalIdentifier(source, identifier).getVesselIds();
	}

	@Override
	public int getUID(int itemId) throws ItemNotFoundException, UnmanagedException, DataManagementOperationException {
		return this._client.getUID(itemId).getVesselUid();
	}

	@Override
	public VesselDataHierarchy getHierarchyByItemUID(int itemUid) throws ItemNotFoundException, UnmanagedException, DataManagementOperationException {
		return this._client.getHierarchyByItemUID(itemUid);
	}
	
	@Override
	public VesselDataHierarchy getHierarchyByItemID(int itemId) throws ItemNotFoundException, UnmanagedException, DataManagementOperationException {
		return this._client.getHierarchyByItemID(itemId);
	}

	@Override
	public VesselDataHierarchy getHierarchyBelowByItemID(int itemId) throws ItemNotFoundException, UnmanagedException, DataManagementOperationException {
		return this._client.getHierarchyBelowByItemID(itemId);
	}

	@Override
	public VesselData getDataByItemID(int itemId) throws ItemNotFoundException, UnmanagedException, DataManagementOperationException {
		return this._client.getDataByItemID(itemId);
	}
	
	@Override
	public Collection<VesselData> getDataByItemUID(int itemUid) throws ItemNotFoundException, UnmanagedException, DataManagementOperationException {
		return this._client.getDataByItemUID(itemUid).getData();
	}

	@Override
	public void link(int sourceitemId, int targetitemId, String user, double score, Date date, String comment) throws ItemNotFoundException, ItemAlreadyMappedException, HierarchyHasCyclesException, UnmanagedException, DataManagementOperationException {
		this._client.link(sourceitemId, targetitemId, user, score, date == null ? null : date.getTime(), comment);
	}

	@Override
	public int unlink(int itemId) throws ItemNotFoundException, ItemIsNotMappedException, UnmanagedException, DataManagementOperationException {
		return this._client.unlink(itemId).getNewVesselUid();
	}

	@Override
	public void delete(int itemId) throws ItemNotFoundException, ItemHasMappingsException, UnmanagedException, DataManagementOperationException {
		this._client.delete(itemId);
	}

	@Override
	public void mergeVessels(int sourceItemId, int targetItemId) throws ItemNotFoundException, ItemHasMappingsException, DifferentSourcesException, UnmanagedException, DataManagementOperationException {
		this._client.merge(sourceItemId, targetItemId);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.common.spi.DataManagementOperationsProvider#checkConsistency()
	 */
	@Override
	public Collection<ConsistencyCheckResult> checkConsistency() throws UnmanagedException, DataManagementOperationException {
		return this._client.checkConsistency().getResults();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.common.spi.DataManagementOperationsProvider#updateDB()
	 */
	@Override
	public DBUpdateResult updateDB() throws UnmanagedException, DataManagementOperationException {
		return this._client.updateDB();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.vessels.spi.VesselsDataManagementOperationsProvider#checkMultipleIMOByUID(boolean)
	 */
	@Override
	public Collection<MultipleIMOByUIDCheckResult> checkMultipleIMOByUID(boolean historical) throws UnmanagedException, DataManagementOperationException {
		return this._client.checkMultipleIMOByUID().getData();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.vessels.spi.VesselsDataManagementOperationsProvider#checkMultipleUIDByIMO(boolean)
	 */
	@Override
	public Collection<MultipleUIDByIMOCheckResult> checkMultipleUIDByIMO(boolean historical) throws UnmanagedException, DataManagementOperationException {
		return this._client.checkMultipleUIDByIMO().getData();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.common.spi.DataManagementOperationsProvider#executeStatement(java.lang.String)
	 */
	@Override
	public Collection<String[]> executeStatement(String statement) throws UnmanagedException, DataManagementOperationException {
		return this._client.executeStatement(statement).getData();
	}
}