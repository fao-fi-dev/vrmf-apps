package org.fao.fi.vrmf.data.management.console.vessels.remote;

import org.fao.fi.vrmf.data.management.console.common.vessels.VesselsManagementConsoleLauncher;

public class RemoteVesselsManagementConsoleLauncher extends VesselsManagementConsoleLauncher {
	static final public void main(String[] args) throws Exception {
		VesselsManagementConsoleLauncher.launch("config/spring/vrmf-dmc-vessels-remote-spring-context.xml", args);
	}
}
