package org.fao.fi.vrmf.data.management.console.vessels.local;

import org.fao.fi.vrmf.data.management.console.common.vessels.VesselsManagementConsoleLauncher;

public class LocalVesselsManagementConsoleLauncher extends VesselsManagementConsoleLauncher {
	static final public void main(String[] args) throws Exception {
		VesselsManagementConsoleLauncher.main(args);
	}
}