/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.vessels.local.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.sh.utility.topology.GraphNode;
import org.fao.fi.sh.utility.topology.WeightedGraph;
import org.fao.fi.sh.utility.topology.WeightedGraphLink;
import org.fao.fi.sh.utility.topology.impl.SimpleWeightValue;
import org.fao.fi.vrmf.business.dao.FullVesselsDAO;
import org.fao.fi.vrmf.business.dao.generated.VesselsDAO;
import org.fao.fi.vrmf.common.models.extended.ExtendedAuthorizations;
import org.fao.fi.vrmf.common.models.extended.FullVessel;
import org.fao.fi.vrmf.common.models.generated.Vessels;
import org.fao.fi.vrmf.common.models.generated.VesselsExample;
import org.fao.fi.vrmf.common.models.generated.VesselsToIdentifiers;
import org.fao.fi.vrmf.common.models.generated.VesselsToLengths;
import org.fao.fi.vrmf.common.models.generated.VesselsToTonnage;
import org.fao.fi.vrmf.data.management.common.model.ConsistencyCheckResult;
import org.fao.fi.vrmf.data.management.common.model.DBUpdateResult;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.DataManagementOperationException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.DataManagementOperationPropertyException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.UnmanagedException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.DifferentSourcesException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.HierarchyHasCyclesException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemAlreadyMappedException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemHasMappingsException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemIsNotMappedException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemNotFoundException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.MultipleItemsFoundException;
import org.fao.fi.vrmf.data.management.vessels.model.AuthorizationData;
import org.fao.fi.vrmf.data.management.vessels.model.MultipleIMOByUIDCheckResult;
import org.fao.fi.vrmf.data.management.vessels.model.MultipleUIDByIMOCheckResult;
import org.fao.fi.vrmf.data.management.vessels.model.VesselData;
import org.fao.fi.vrmf.data.management.vessels.model.VesselDataHierarchy;
import org.fao.fi.vrmf.data.management.vessels.spi.VesselsDataManagementOperationsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ibatis.sqlmap.client.SqlMapClient;

/**
 * @author Fiorellato
 */
@Singleton @Named("vessels.data.management.provider")
public class LocalVesselsManagementOperationsProviderImpl implements VesselsDataManagementOperationsProvider {
	final static private boolean HISTORICAL_SEARCH = true;
	final static private boolean CURRENT_SEARCH    = !HISTORICAL_SEARCH;
	
	final private Properties EMPTY_PROPERTIES = new Properties();
	
	@SuppressWarnings("unused")
	static final private Logger LOG = LoggerFactory.getLogger(LocalVesselsManagementOperationsProviderImpl.class);
	
	@Inject private @Named("vrmf.sqlMapClient") SqlMapClient _sqlMapClient;
	@Inject private @Named("dao.vessels") VesselsDAO _vesselsDAO;
	@Inject private @Named("dao.vessels.full") FullVesselsDAO _fullVesselsDAO;
		
	public LocalVesselsManagementOperationsProviderImpl() {
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.common.spi.DataManagementOperationsProvider#properties()
	 */
	@Override
	public Properties properties() {
		return EMPTY_PROPERTIES;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.common.spi.DataManagementOperationsProvider#notifyPropertyChanged(java.lang.String, java.lang.String)
	 */
	@Override
	public String notifyPropertyChanged(String property, String value) throws DataManagementOperationPropertyException {
		return value;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.common.spi.DataManagementOperationsProvider#description()
	 */
	@Override
	public String description() {
		return "An implementation of the Vessels Data Management operations provider that accesses data directly over JDBC";
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.vessels.console.common.spi.ManagementOperationsProvider#getVesselID(java.lang.String, java.lang.String)
	 */
	@Override
	public int getVesselIDByCurrentIdentifier(String source, String identifier) throws ItemNotFoundException, MultipleItemsFoundException, UnmanagedException {
		boolean isIMO = "IMO".equals(source);
		
		try {
			Integer[] results = this.getVesselIDByIdentifier(source, identifier, CURRENT_SEARCH);

			if(results.length > 1) {
				String identifiers = "";
				
				for(Integer in : results)
					identifiers += in + ", ";

				if(!"".equals(identifiers))
					identifiers = identifiers.substring(0, identifiers.length() - 2);
				
				identifiers = "[ " + identifiers + " ]";
				
				if(isIMO)
					throw new MultipleItemsFoundException("Multiple vessels found (" + results.length + ") with IMO " + identifier + ": " + identifiers);
				
				throw new MultipleItemsFoundException("Multiple vessels found (" + results.length + ") with " + source + " ID " + identifier + ": " + identifiers);
			}
			
			return results[0];
		} catch(MultipleItemsFoundException|ItemNotFoundException e) {
			throw e;
		} catch(Throwable t) {
			if(isIMO)
				throw new UnmanagedException("Unable to identify VRMF vessel ID by IMO " + identifier + ". Unexpected " + t.getClass().getName() + " caught: " + t.getMessage(), t);
			
			throw new UnmanagedException("Unable to identify VRMF vessel ID by " + source + " ID #" + identifier + ". Unexpected " + t.getClass().getName() + " caught: " + t.getMessage(), t);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.vessels.spi.VesselsDataManagementOperationsProvider#getVesselIDByIdentifier(java.lang.String, java.lang.String)
	 */
	@Override
	public int[] getVesselIDByHistoricalIdentifier(String source, String identifier) throws ItemNotFoundException, UnmanagedException {
		boolean isIMO = "IMO".equals(source);
		
		try {
			Integer[] results = this.getVesselIDByIdentifier(source, identifier, HISTORICAL_SEARCH);
			
			int[] unboxed = new int[results.length];
			
			int counter = 0;
			for(Integer ID : results)
				unboxed[counter++] = ID;
			
			return unboxed;
		} catch(ItemNotFoundException e) {
			throw e;
		} catch(Throwable t) {
			if(isIMO) { 
				throw new UnmanagedException("Unable to identify VRMF vessel ID by IMO " + identifier + ". Unexpected " + t.getClass().getName() + " caught: " + t.getMessage(), t);
			}
			
			throw new UnmanagedException("Unable to identify VRMF vessel ID by " + source + " ID #" + identifier + ". Unexpected " + t.getClass().getName() + " caught: " + t.getMessage(), t);
		}
	}
	
	private Integer[] getVesselIDByIdentifier(String source, String identifier, boolean historical) throws Exception {
		Map<String, String> params = new HashMap<String, String>();
		params.put("source", source);
		params.put("identifier", identifier);
		
		@SuppressWarnings("unchecked")
		List<Integer> results = (List<Integer>)this._sqlMapClient.queryForList("management.vesselIDBy" + ( historical == HISTORICAL_SEARCH ? "" : "Latest" ) + "Identifier", params);

		boolean isIMO = "IMO".equals(source);
		
		if(results == null || results.isEmpty()) {
			if(isIMO) 
				throw new ItemNotFoundException("Unable to identify any vessel by IMO " + identifier);
			
			throw new ItemNotFoundException("Unable to identify any " + source + " vessel by source ID " + identifier);
		}
		
		return results.toArray(new Integer[results.size()]);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.common.spi.DataManagementOperationsProvider#getUID(int)
	 */
	@Override
	public int getUID(int vesselId) throws ItemNotFoundException, UnmanagedException {
		Vessels current = null;
		
		try {
			current = this._vesselsDAO.selectByPrimaryKey(vesselId);
		} catch(Throwable t) {
			throw new UnmanagedException("Unable to get VRMF vessel UID by vessel ID #" + vesselId + ". Unexpected " + t.getClass().getName() + " caught: " + t.getMessage(), t);
		}
			
		if(current == null)
			throw new ItemNotFoundException("Unable to identify any vessel by VRMF ID #" + vesselId);
			
		return current.getUid();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.common.spi.DataManagementOperationsProvider#getHierarchyItemUID(int)
	 */
	@Override
	public VesselDataHierarchy getHierarchyByItemUID(int itemUid) throws ItemNotFoundException, UnmanagedException, DataManagementOperationException {
		try {
			VesselsExample filter = new VesselsExample();
			filter.createCriteria().andUidEqualTo(itemUid);
			
			List<Vessels> mapped = this._vesselsDAO.selectByExample(filter);
			
			if(mapped == null || mapped.isEmpty())
				throw new ItemNotFoundException("Unable to identify any vessel by VRMF UID #" + itemUid);

			Integer firstID = null, unmappedID = null;
			
			for(Vessels in : mapped) {
				if(in.getMapsTo() == null) {
					if(unmappedID == null)
						unmappedID = in.getId();
				} else {
					if(firstID == null)
						firstID = in.getId();
				}
			}
			
			return this.getHierarchyByItemID(unmappedID != null ? unmappedID : firstID);
		} catch(ItemNotFoundException VNFe) {
			throw VNFe;
		} catch(Throwable t) {
			throw new UnmanagedException("Unable to get vessel hierarchy for vessel with VRMF UID #" + itemUid + ". Unexpected " + t.getClass().getName() + " caught: " + t.getMessage(), t);
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.common.spi.DataManagementOperationsProvider#getHierarchyByItemID(int)
	 */
	@Override
	public VesselDataHierarchy getHierarchyByItemID(int vesselId) throws ItemNotFoundException, UnmanagedException {
		WeightedGraph<Integer> graph = new WeightedGraph<Integer>();
		
		try {
			int UID = this.getUID(vesselId);
			
			VesselsExample filter = new VesselsExample();
			filter.createCriteria().andUidEqualTo(UID).andMapsToIsNotNull();
			
			List<Vessels> mapped = this._vesselsDAO.selectByExample(filter);
			
			for(Vessels in : mapped) {
				graph.link(
					new GraphNode<Integer>(in.getId()),
					new GraphNode<Integer>(in.getMapsTo()), 
					new SimpleWeightValue(in.getMappingWeight())
				);
			}
			
			graph.reverseGraph();
			
			List<GraphNode<Integer>> roots = new ArrayList<GraphNode<Integer>>(graph.getSourceRootNodeSet());
			
			if(roots.isEmpty()) {
				return new VesselDataHierarchy(this.getDataByItemID(vesselId));
			}
			
			if(roots.size() > 1) {
				Collection<Integer> rootIndexes = new ArrayList<Integer>();
				
				for(GraphNode<Integer> in : roots)
					rootIndexes.add(in.getID());
				
				throw new UnmanagedException("Multiple roots found for vessel with VRMF ID #" + vesselId + " and UID #" + UID + ": " + rootIndexes);
			}

			GraphNode<Integer> rootNode = roots.get(0);
			
			return this.updateHierarchy(new VesselDataHierarchy(this.getDataByItemID(rootNode.getID())), graph, graph.getAdjacents(rootNode));
		} catch(ItemNotFoundException VNFe) {
			throw VNFe;
		} catch(Throwable t) {
			throw new UnmanagedException("Unable to get vessel hierarchy for vessel with VRMF ID #" + vesselId + ". Unexpected " + t.getClass().getName() + " caught: " + t.getMessage(), t);
		}
	}
	
	private VesselDataHierarchy updateHierarchy(VesselDataHierarchy current, WeightedGraph<Integer> graph, Collection<WeightedGraphLink<Integer>> siblings) throws Exception {
		VesselDataHierarchy child;
		
		if(siblings != null) {
			for(WeightedGraphLink<Integer> in : siblings) {
				child = new VesselDataHierarchy(this.getDataByItemID(in.getTarget().getID()));
				
				current.add(child);
				
				this.updateHierarchy(child, graph, graph.getAdjacents(in.getTarget()));
			}
		}
		
		return current;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.common.spi.DataManagementOperationsProvider#getHierarchyBelowByItemID(int)
	 */
	@Override
	public VesselDataHierarchy getHierarchyBelowByItemID(int vesselId) throws ItemNotFoundException, UnmanagedException {
		return this.findInHierarchy(vesselId, this.getHierarchyByItemID(vesselId));
	}

	private VesselDataHierarchy findInHierarchy(int vesselId, VesselDataHierarchy hierarchy) {
		if(hierarchy.getData().getId() == vesselId)
			return hierarchy;
		
		VesselDataHierarchy found = null;
		
		for(VesselDataHierarchy in : hierarchy.children()) {
			found = this.findInHierarchy(vesselId, in);
			
			if(found != null)
				return found;
		}
		
		return null;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.common.spi.DataManagementOperationsProvider#getDataByItemUID(int)
	 */
	@Override
	public Collection<VesselData> getDataByItemUID(int vesselUid) throws ItemNotFoundException, UnmanagedException, DataManagementOperationException {
		Collection<VesselData> data = new ArrayList<VesselData>();
		
		try {
			VesselsExample filter = new VesselsExample();
			filter.createCriteria().andUidEqualTo(vesselUid);
			
			List<Vessels> vessels = this._vesselsDAO.selectByExample(filter);
			
			if(vessels == null || vessels.isEmpty())
				throw new ItemNotFoundException("Unable to identify any vessel by VRMF UID #" + vesselUid);
			
			for(Vessels in : vessels)
				data.add(this.getDataByItemID(in.getId()));
			
			return data;
		} catch(ItemNotFoundException VNFe) {
			throw VNFe;
		} catch(Throwable t) {
			throw new UnmanagedException("Unable to get vessel data for vessels with VRMF UID #" + vesselUid + ". Unexpected " + t.getClass().getName() + " caught: " + t.getMessage(), t);
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.common.spi.DataManagementOperationsProvider#getDataByItemID(int)
	 */
	@Override
	public VesselData getDataByItemID(int vesselId) throws ItemNotFoundException, UnmanagedException {
		FullVessel fullVessel = null;
		
		try { 
			fullVessel = this._fullVesselsDAO.selectFullByPrimaryKey(vesselId);
			
			if(fullVessel == null)
				throw new ItemNotFoundException("Unable to identify any vessel by VRMF ID #" + vesselId);
			
			VesselData data = new VesselData();
			
			data.setId(fullVessel.getId());
			data.setUid(fullVessel.getUid());
			
			data.setMapsTo(fullVessel.getMapsTo());
			data.setMappingWeight(fullVessel.getMappingWeight());
			data.setMappingUser(fullVessel.getMappingUser());
			data.setMappingDate(fullVessel.getMappingDate());
			data.setMappingComment(fullVessel.getMappingComment());
			
			data.setSourceSystem(fullVessel.getSourceSystem());
			
			data.setUpdateDate(fullVessel.getUpdateDate());
			
			if(fullVessel.getFlags() != null && !fullVessel.getFlags().isEmpty())
				data.setFlag(fullVessel.getFlags().get(0).getVesselFlag().getIso3Code());

			if(fullVessel.getNameData() != null && !fullVessel.getNameData().isEmpty())
				data.setName(fullVessel.getNameData().get(0).getName());

			data.setImo(fullVessel.findIMO());
			
			if(fullVessel.getIdentifiers() != null && !fullVessel.getIdentifiers().isEmpty()) {
				for(VesselsToIdentifiers in : fullVessel.getIdentifiers()) {
					if((data.getSourceSystem() + "_ID").equals(in.getTypeId())) {
						data.setIdentifier(in.getIdentifier());
						
						if(in.getAlternateIdentifier() != null && !in.getAlternateIdentifier().equals(in.getIdentifier()))
							data.setAlternateIdentifier(in.getAlternateIdentifier());
						
						break;
					}
				}
			}

			if(fullVessel.getCallsignData() != null && !fullVessel.getCallsignData().isEmpty())
				data.setIrcs(fullVessel.getCallsignData().get(0).getCallsignId());

			if(fullVessel.getRegistrationData() != null && !fullVessel.getRegistrationData().isEmpty())
				data.setNrn(fullVessel.getRegistrationData().get(0).getRegistrationNumber());
			
			if(fullVessel.getLengthData() != null && !fullVessel.getLengthData().isEmpty()) {
				for(VesselsToLengths in : fullVessel.getLengthData()) {
					if("LOA".equals(in.getTypeId())) {
						data.setLoa(new Double(in.getValue()));
						
						break;
					}
				}
			}
			
			if(fullVessel.getTonnageData() != null && !fullVessel.getTonnageData().isEmpty()) {
				for(VesselsToTonnage in : fullVessel.getTonnageData()) {
					if("GT".equals(in.getTypeId())) {
						data.setGt(new Double(in.getValue()));
						
						break;
					}
				}
			}

			if(fullVessel.getAuthorizations() != null && !fullVessel.getAuthorizations().isEmpty()) {
				Set<String> processedTypes = new HashSet<String>();
				
				for(ExtendedAuthorizations auth : fullVessel.getAuthorizations()) {
					if(!processedTypes.contains(auth.getTypeId())) {
						AuthorizationData authData = new AuthorizationData();
						authData.setTypeId(auth.getTypeId());
						authData.setAuthFrom(auth.getValidFrom());
						authData.setAuthTo(auth.getValidTo());
						authData.setAuthTerm(auth.getTerminationReferenceDate());
						authData.setAuthTermReasonCode(auth.getTerminationReasonCode());
						authData.setAuthTermReason(auth.getTerminationReason());

						data.getAuthorizations().add(authData);
						
						processedTypes.add(auth.getTypeId());
					}	
				};
			}

			return data;
		} catch(ItemNotFoundException VNFe) {
			throw VNFe;
		} catch(Throwable t) {
			t.printStackTrace();
			
			throw new UnmanagedException("Unable to get vessel data for vessel with VRMF ID #" + vesselId + ". Unexpected " + t.getClass().getName() + " caught: " + t.getMessage(), t);
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.vessels.console.common.spi.ManagementOperationsProvider#link(int, int, java.lang.String, double, java.util.Date, java.lang.String)
	 */
	@Override
	public void link(int sourceVesselId, int targetVesselId, String user, double score, Date date, String comment) throws ItemNotFoundException, ItemAlreadyMappedException, HierarchyHasCyclesException, UnmanagedException {
		VesselData source = this.getDataByItemID(sourceVesselId);
		
		@SuppressWarnings("unused")
		VesselData target = this.getDataByItemID(targetVesselId);

		if(source.getMapsTo() != null && source.getMapsTo() == targetVesselId)
			throw new ItemAlreadyMappedException("Source vessel with VRMF ID #" + sourceVesselId + " is already mapped to vessel with VRMF ID #" + source.getMapsTo());
		
		try {
			this._sqlMapClient.startTransaction();
			
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("sourceVesselId", sourceVesselId);
			parameters.put("targetVesselId", targetVesselId);
			parameters.put("mappingUser", user);
			parameters.put("mappingScore", score);
			parameters.put("mappingDate", date);
			parameters.put("mappingComment", comment);
			
			this._sqlMapClient.update("management.link", parameters);
			
			this._sqlMapClient.commitTransaction();
		} catch(Throwable t) {
			throw new UnmanagedException("Unable to properly link vessel with VRMF ID #" + sourceVesselId + " to vessel with VRMF ID #" + targetVesselId + ": " + t.getMessage(), t);
		} finally {
			try {
				this._sqlMapClient.endTransaction();
			} catch(SQLException SQLe) {
				throw new UnmanagedException("Unable to properly end transaction: " + SQLe.getMessage(), SQLe);
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.vessels.console.common.spi.ManagementOperationsProvider#unlink(int)
	 */
	@Override
	public int unlink(int vesselId) throws ItemNotFoundException, ItemIsNotMappedException, UnmanagedException {
		VesselData vessel = this.getDataByItemID(vesselId);
		
		if(vessel.getMapsTo() == null)
			throw new ItemIsNotMappedException("Vessel with VRMF ID #" + vesselId + " is not currently mapped to any other vessel");
		
		try {
			this._sqlMapClient.startTransaction();
			
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("vesselId", vesselId);
			
			Integer newUID = (Integer)this._sqlMapClient.queryForObject("management.unlink", parameters);
			
			this._sqlMapClient.commitTransaction();
			
			return newUID;
		} catch(Throwable t) {
			throw new UnmanagedException("Unable to properly unlink vessel with VRMF ID #" + vesselId + ": " + t.getMessage(), t);
		} finally {
			try {
				this._sqlMapClient.endTransaction();
			} catch(SQLException SQLe) {
				throw new UnmanagedException("Unable to properly end transaction: " + SQLe.getMessage(), SQLe);
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.vessels.console.common.spi.ManagementOperationsProvider#delete(int)
	 */
	@Override
	public void delete(int vesselId) throws ItemNotFoundException, ItemHasMappingsException, UnmanagedException {
		@SuppressWarnings("unused")
		VesselData vessel = this.getDataByItemID(vesselId);
		
		VesselDataHierarchy hierarchy = this.getHierarchyBelowByItemID(vesselId);
		
		if(!hierarchy.isEmpty()) {
			Set<Integer> childrenIDs = new TreeSet<Integer>();
			
			for(VesselDataHierarchy in : hierarchy.children())
				childrenIDs.add(in.getData().getId());
			
			throw new ItemHasMappingsException("Cannot delete vessel with VRMF ID #" + vesselId + " as there are currently " + hierarchy.countChildren() + " other vessels linked to it (" + childrenIDs + ")");
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.vessels.console.common.spi.ManagementOperationsProvider#merge(int, int)
	 */
	@Override
	public void mergeVessels(int sourceVesselId, int targetVesselId) throws ItemNotFoundException, ItemHasMappingsException, DifferentSourcesException, UnmanagedException {
		VesselData source = this.getDataByItemID(sourceVesselId);
		VesselData target = this.getDataByItemID(targetVesselId);
		
		if(!source.getSourceSystem().equals(target.getSourceSystem()))
			throw new DifferentSourcesException("Source vessel and target vessel belong to different data sources (" + source.getSourceSystem() + " and " + target.getSourceSystem() + ", respectively)");
		
		try {
			this._sqlMapClient.startTransaction();
			
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("sourceVesselId", sourceVesselId);
			parameters.put("targetVesselId", targetVesselId);
			
			this._sqlMapClient.update("management.merge", parameters);
			
			this._sqlMapClient.commitTransaction();
		} catch(Throwable t) {
			throw new UnmanagedException("Unable to properly merge source vessel with VRMF ID #" + sourceVesselId + " with target vessel with VRMF ID #" + targetVesselId + ": " + t.getMessage(), t);
		} finally {
			try {
				this._sqlMapClient.endTransaction();
			} catch(SQLException SQLe) {
				throw new UnmanagedException("Unable to properly end transaction: " + SQLe.getMessage(), SQLe);
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.common.spi.DataManagementOperationsProvider#updateDB()
	 */
	@Override
	public DBUpdateResult updateDB() throws UnmanagedException, DataManagementOperationException {
		try {
			this._sqlMapClient.startTransaction();
		
			DBUpdateResult result = (DBUpdateResult)this._sqlMapClient.queryForObject("management.updateDB");
			
			this._sqlMapClient.commitTransaction();
			
			return result;
		} catch(Throwable t) {
			throw new UnmanagedException("Unable to properly update database: " + t.getMessage(), t);
		} finally {
			try {
				this._sqlMapClient.endTransaction();
			} catch(SQLException SQLe) {
				throw new UnmanagedException("Unable to properly end transaction: " + SQLe.getMessage(), SQLe);
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.common.spi.DataManagementOperationsProvider#checkConsistency()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<ConsistencyCheckResult> checkConsistency() throws UnmanagedException, DataManagementOperationException {
		try {
			return (List<ConsistencyCheckResult>)this._sqlMapClient.queryForList("management.consistencyCheck");
		} catch(Throwable t) {
			throw new UnmanagedException("Unable to properly check ID / UID consistency: " + t.getMessage(), t);
		} 
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.vessels.spi.VesselsDataManagementOperationsProvider#checkMultipleIMOByUID(boolean)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<MultipleIMOByUIDCheckResult> checkMultipleIMOByUID(boolean historical) throws UnmanagedException, DataManagementOperationException {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("historical", Boolean.valueOf(historical));
			
			return (List<MultipleIMOByUIDCheckResult>)this._sqlMapClient.queryForList("management.multipleIMOByUID", params);
		} catch(Throwable t) {
			throw new UnmanagedException("Unable to properly check for multiple IMO by UID: " + t.getMessage(), t);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.vessels.spi.VesselsDataManagementOperationsProvider#checkMultipleUIDByIMO(boolean)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<MultipleUIDByIMOCheckResult> checkMultipleUIDByIMO(boolean historical) throws UnmanagedException, DataManagementOperationException {
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("historical", Boolean.valueOf(historical));
			
			return (List<MultipleUIDByIMOCheckResult>)this._sqlMapClient.queryForList("management.multipleUIDByIMO", params);
		} catch(Throwable t) {
			throw new UnmanagedException("Unable to properly check for multiple UID by IMO: " + t.getMessage(), t);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.common.spi.DataManagementOperationsProvider#executeStatement(java.lang.String)
	 */
	@Override
	public Collection<String[]> executeStatement(String sqlStatement) throws UnmanagedException, DataManagementOperationException {
		Collection<String[]> results = new ArrayList<String[]>();
		
		try(Connection connection = this._sqlMapClient.getDataSource().getConnection()) {
			try(Statement statement = connection.createStatement()) {
				boolean executed = statement.execute(sqlStatement);
				
				ResultSet resultSet = executed ? statement.getResultSet() : null; 
				
				if(!connection.getAutoCommit()) connection.commit();
				
				if(resultSet != null) {
					ResultSetMetaData meta = resultSet.getMetaData();
	
					Collection<String> values = new ArrayList<String>();
					
					for(int c=1; c<=meta.getColumnCount(); c++) {
						values.add(meta.getColumnName(c) + " [ " + meta.getColumnTypeName(c) + " ]");
					}
					
					results.add(values.toArray(new String[values.size()]));
					
					while(resultSet.next()) {
						values = new ArrayList<String>();
						
						for(int c=1; c<=meta.getColumnCount(); c++) {
							values.add(resultSet.getString(c));
						}
						
						results.add(values.toArray(new String[values.size()]));
					}
					
					resultSet.close();
				}
			} catch(Throwable e) {
				if(!connection.getAutoCommit()) connection.rollback();
				
				throw e;
			}
			
			return results;
		} catch(Throwable t) {
			throw new UnmanagedException("Unable to properly execute statement: " + t.getMessage(), t);
		}
	}
}