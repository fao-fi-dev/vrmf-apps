/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.web.vessels.support;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.fao.fi.vrmf.data.management.common.spi.exceptions.DataManagementOperationException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.DifferentSourcesException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.HierarchyHasCyclesException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemAlreadyMappedException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemIsNotMappedException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemNotFoundException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.MultipleItemsFoundException;
import org.fao.fi.vrmf.data.management.console.web.common.vessels.model.VesselsDataManagementOperationExceptionEntity;

/**
 * @author Fiorellato
 *
 */
@Provider
public class VesselsDataManagementExceptionMapper implements ExceptionMapper<DataManagementOperationException> {
	public Response toResponse(DataManagementOperationException e) {
		return Response.status(this.toStatusCode(e))
					   .entity(new VesselsDataManagementOperationExceptionEntity(e))
					   .type(MediaType.APPLICATION_XML).
					    build();
	}
 
	protected Status toStatusCode(DataManagementOperationException e) {
		switch(e.getExceptionCode()) {
			case ItemNotFoundException.EXCEPTION_CODE:
				return Status.NOT_FOUND;
			case MultipleItemsFoundException.EXCEPTION_CODE:
			case ItemIsNotMappedException.EXCEPTION_CODE:
			case ItemAlreadyMappedException.EXCEPTION_CODE:
			case HierarchyHasCyclesException.EXCEPTION_CODE:
				return Status.CONFLICT;
			case DifferentSourcesException.EXCEPTION_CODE:
				return Status.BAD_REQUEST;
			default:
				return Status.INTERNAL_SERVER_ERROR;
		}
	}
}
