/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.web.vessels.support;

import org.fao.fi.vrmf.data.management.console.web.common.vessels.support.VesselsDataCustomJAXBContext;
import org.fao.fi.vrmf.data.management.console.web.vessels.ws.rest.VesselsDataManagementRemoteOperationsProvider;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

/**
 * @author Fiorellato
 *
 */
public class Application extends ResourceConfig {
	public Application() {
		register(JacksonFeature.class);
		register(VesselsDataManagementExceptionMapper.class);
		register(VesselsDataManagementRemoteOperationsProvider.class);
		register(VesselsDataCustomJAXBContext.class);
	}
}
