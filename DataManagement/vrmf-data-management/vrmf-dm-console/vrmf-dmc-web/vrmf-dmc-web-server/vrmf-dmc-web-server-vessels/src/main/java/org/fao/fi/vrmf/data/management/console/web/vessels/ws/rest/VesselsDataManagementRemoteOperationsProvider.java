/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.web.vessels.ws.rest;

import java.util.Date;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.fao.fi.vrmf.data.management.common.model.DBUpdateResult;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.DataManagementOperationException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.UnmanagedException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.DifferentSourcesException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.HierarchyHasCyclesException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemAlreadyMappedException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemHasMappingsException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemIsNotMappedException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemNotFoundException;
import org.fao.fi.vrmf.data.management.console.web.common.support.response.ExecuteStatementResponse;
import org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.VesselDataManagementRESTInterface;
import org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response.CheckMultipleIMOByUIDResponse;
import org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response.CheckMultipleUIDByIMOResponse;
import org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response.CheckConsistencyResponse;
import org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response.GetVesselDataByUIDResponse;
import org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response.GetVesselIDMultipleResponse;
import org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response.GetVesselIDResponse;
import org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response.GetVesselUIDResponse;
import org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response.UnlinkVesselResponse;
import org.fao.fi.vrmf.data.management.vessels.model.VesselData;
import org.fao.fi.vrmf.data.management.vessels.model.VesselDataHierarchy;
import org.fao.fi.vrmf.data.management.vessels.spi.VesselsDataManagementOperationsProvider;
import org.springframework.stereotype.Component;

/**
 * @author Fiorellato
 *
 */
@Singleton @Component @Path("/operations")
public class VesselsDataManagementRemoteOperationsProvider implements VesselDataManagementRESTInterface {
	static final public String MEDIA_TYPE_XML_UTF8 = MediaType.APPLICATION_XML + ";charset=UTF-8";
	static final public String MEDIA_TYPE_JSON_UTF8 = MediaType.APPLICATION_JSON + ";charset=UTF-8";

	@Inject @Named("vessels.data.management.provider") private VesselsDataManagementOperationsProvider _provider;
	
	public VesselsDataManagementRemoteOperationsProvider() {
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.web.vessels.ws.rest.VesselDataManagementWebInterface#delete(int)
	 */
	@Override
	@DELETE @Path("delete/{vesselId}")
	public void delete(@PathParam("vesselId") int vesselId) throws ItemNotFoundException, ItemHasMappingsException, UnmanagedException, DataManagementOperationException {
		this._provider.delete(vesselId);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.VesselDataManagementRESTInterface#getHierarchyByItemID(int)
	 */
	@Override
	@GET @Path("hierarchy/id/{vesselId}") @Produces({ MEDIA_TYPE_XML_UTF8, MEDIA_TYPE_JSON_UTF8 })
	public VesselDataHierarchy getHierarchyByItemID(@PathParam("vesselId") int vesselId) throws ItemNotFoundException, UnmanagedException, DataManagementOperationException {
		VesselDataHierarchy h = this._provider.getHierarchyByItemID(vesselId);
		
		return h;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.VesselDataManagementRESTInterface#getHierarchyBelowByItemID(int)
	 */
	@Override
	@GET @Path("hierarchy/below/id/{vesselId}") @Produces({ MEDIA_TYPE_XML_UTF8, MEDIA_TYPE_JSON_UTF8 })
	public VesselDataHierarchy getHierarchyBelowByItemID(@PathParam("vesselId") int vesselId) throws ItemNotFoundException, UnmanagedException, DataManagementOperationException {
		return this._provider.getHierarchyBelowByItemID(vesselId);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.VesselDataManagementRESTInterface#getHierarchyBelowByItemID(int)
	 */
	@Override
	@GET @Path("hierarchy/uid/{vesselUid}") @Produces({ MEDIA_TYPE_XML_UTF8, MEDIA_TYPE_JSON_UTF8 })
	public VesselDataHierarchy getHierarchyByItemUID(@PathParam("vesselUid") int vesselUid) throws ItemNotFoundException, UnmanagedException, DataManagementOperationException {
		return this._provider.getHierarchyByItemUID(vesselUid);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.VesselDataManagementRESTInterface#getDataByItemID(int)
	 */
	@Override
	@GET @Path("data/id/{vesselId}") @Produces({ MEDIA_TYPE_XML_UTF8, MEDIA_TYPE_JSON_UTF8 })
	public VesselData getDataByItemID(@PathParam("vesselId") int vesselId) throws ItemNotFoundException, UnmanagedException, DataManagementOperationException {
		return this._provider.getDataByItemID(vesselId);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.VesselDataManagementRESTInterface#getDataByItemUID(int)
	 */
	@Override
	@GET @Path("data/uid/{vesselUid}") @Produces({ MEDIA_TYPE_XML_UTF8, MEDIA_TYPE_JSON_UTF8 })
	public GetVesselDataByUIDResponse getDataByItemUID(@PathParam("vesselUid") int vesselUid) throws ItemNotFoundException, UnmanagedException, DataManagementOperationException {
		return new GetVesselDataByUIDResponse(this._provider.getDataByItemUID(vesselUid));
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.web.vessels.ws.rest.VesselDataManagementWebInterface#getVesselID(java.lang.String, java.lang.String)
	 */
	@Override
	@GET @Path("ID/current/{rfmo}/{rfmoId}") @Produces({ MEDIA_TYPE_XML_UTF8, MEDIA_TYPE_JSON_UTF8 })
	public GetVesselIDResponse getIDByCurrentIdentifier(@PathParam("rfmo") String rfmo, @PathParam("rfmoId") String rfmoId) throws ItemNotFoundException, UnmanagedException, DataManagementOperationException {
		return new GetVesselIDResponse(rfmo, rfmoId, this._provider.getVesselIDByCurrentIdentifier(rfmo, rfmoId));
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.VesselDataManagementRESTInterface#getIDByHistoricalIdentifier(java.lang.String, java.lang.String)
	 */
	@Override
	@GET @Path("ID/historical/{rfmo}/{rfmoId}") @Produces({ MEDIA_TYPE_XML_UTF8, MEDIA_TYPE_JSON_UTF8 })
	public GetVesselIDMultipleResponse getIDByHistoricalIdentifier(@PathParam("rfmo") String rfmo, @PathParam("rfmoId") String rfmoId) throws DataManagementOperationException {
		return new GetVesselIDMultipleResponse(rfmo, rfmoId, this._provider.getVesselIDByHistoricalIdentifier(rfmo, rfmoId));
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.web.vessels.ws.rest.VesselDataManagementWebInterface#getVesselUID(int)
	 */
	@Override
	@GET @Path("UID/{vesselId}") @Produces({ MEDIA_TYPE_XML_UTF8, MEDIA_TYPE_JSON_UTF8 })
	public GetVesselUIDResponse getUID(@PathParam("vesselId")int vesselId) throws ItemNotFoundException, UnmanagedException, DataManagementOperationException { 
		return new GetVesselUIDResponse(vesselId, this._provider.getUID(vesselId));
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.web.vessels.ws.rest.VesselDataManagementWebInterface#link(java.lang.Integer, java.lang.Integer, java.lang.String, java.lang.Double, java.util.Date, java.lang.String)
	 */
	@Override
	@POST @Path("link") @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public void link(@FormParam("sourceVesselId") Integer sourceVesselId, 
					 @FormParam("targetVesselId") Integer targetVesselId, 
					 @FormParam("user") String user, 
					 @FormParam("score") Double score, 
					 @FormParam("mappingDate") Long mappingDateTimestamp, 
					 @FormParam("mappingComment") String mappingComment) throws ItemNotFoundException, ItemAlreadyMappedException, HierarchyHasCyclesException, UnmanagedException, DataManagementOperationException {
		this._provider.link(sourceVesselId, targetVesselId, user, score, mappingDateTimestamp == null ? null : new Date(mappingDateTimestamp), mappingComment);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.web.vessels.ws.rest.VesselDataManagementWebInterface#merge(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	@POST @Path("merge") @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public void merge(@FormParam("sourceVesselId") Integer sourceVesselId, 
					  @FormParam("targetVesselId") Integer targetVesselId) throws ItemNotFoundException, ItemHasMappingsException, DifferentSourcesException, UnmanagedException, DataManagementOperationException {
		this._provider.mergeVessels(sourceVesselId, targetVesselId);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.web.vessels.ws.rest.VesselDataManagementWebInterface#unlink(java.lang.Integer)
	 */
	@Override
	@POST @Path("unlink") @Produces({ MEDIA_TYPE_XML_UTF8, MEDIA_TYPE_JSON_UTF8 }) @Consumes(MediaType.APPLICATION_FORM_URLENCODED) 
	public UnlinkVesselResponse unlink(@FormParam("vesselId") Integer vesselId) throws ItemNotFoundException, ItemIsNotMappedException, UnmanagedException, DataManagementOperationException {
		return new UnlinkVesselResponse(vesselId, this._provider.unlink(vesselId));
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.VesselDataManagementRESTInterface#checkConsistency()
	 */
	@Override
	@GET @Path("db/check") @Produces({ MEDIA_TYPE_XML_UTF8, MEDIA_TYPE_JSON_UTF8 }) 
	public CheckConsistencyResponse checkConsistency() throws DataManagementOperationException {
		return new CheckConsistencyResponse(this._provider.checkConsistency());
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.VesselDataManagementRESTInterface#updateDB()
	 */
	@Override
	@POST @Path("db/update") @Produces({ MEDIA_TYPE_XML_UTF8, MEDIA_TYPE_JSON_UTF8 }) 
	public DBUpdateResult updateDB() throws DataManagementOperationException {
		return this._provider.updateDB();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.VesselDataManagementRESTInterface#checkMultipleIMOByUID()
	 */
	@Override
	@GET @Path("db/check/IMO/UID") @Produces({ MEDIA_TYPE_XML_UTF8, MEDIA_TYPE_JSON_UTF8 }) 
	public CheckMultipleIMOByUIDResponse checkMultipleIMOByUID() throws DataManagementOperationException {
		return new CheckMultipleIMOByUIDResponse(this._provider.checkMultipleIMOByUID(false));
	}
	
	@Override
	@GET @Path("db/check/UID/IMO") @Produces({ MEDIA_TYPE_XML_UTF8, MEDIA_TYPE_JSON_UTF8 }) 
	public CheckMultipleUIDByIMOResponse checkMultipleUIDByIMO() throws DataManagementOperationException {
		return new CheckMultipleUIDByIMOResponse(this._provider.checkMultipleUIDByIMO(false));
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.web.vessels.ws.rest.VesselDataManagementWebInterface#merge(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	@POST @Path("execute") @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public ExecuteStatementResponse executeStatement(@FormParam("statement") String statement) throws DataManagementOperationException {
		return new ExecuteStatementResponse(statement, this._provider.executeStatement(statement));
	}
}