/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.web.client.vessels;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.fao.fi.vrmf.data.management.common.model.DBUpdateResult;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.DataManagementOperationException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.UnmanagedException;
import org.fao.fi.vrmf.data.management.console.web.client.AbstractRemoteOperationsProviderClient;
import org.fao.fi.vrmf.data.management.console.web.common.model.DataManagementOperationExceptionEntity;
import org.fao.fi.vrmf.data.management.console.web.common.support.response.ExecuteStatementResponse;
import org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.VesselDataManagementRESTInterface;
import org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response.CheckConsistencyResponse;
import org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response.CheckMultipleIMOByUIDResponse;
import org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response.CheckMultipleUIDByIMOResponse;
import org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response.GetVesselDataByUIDResponse;
import org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response.GetVesselIDMultipleResponse;
import org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response.GetVesselIDResponse;
import org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response.GetVesselUIDResponse;
import org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response.UnlinkVesselResponse;
import org.fao.fi.vrmf.data.management.console.web.common.vessels.support.VesselsDataCustomJAXBContext;
import org.fao.fi.vrmf.data.management.vessels.model.VesselData;
import org.fao.fi.vrmf.data.management.vessels.model.VesselDataHierarchy;

/**
 * @author Fiorellato
 */
@Singleton @Named("remote.client.vessels")
public class VesselsDataManagementRemoteOperationsProviderClient extends AbstractRemoteOperationsProviderClient implements VesselDataManagementRESTInterface {
	public VesselsDataManagementRemoteOperationsProviderClient() {
		super();
		
		this._config.register(VesselsDataCustomJAXBContext.class);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.web.client.AbstractRemoteOperationsProviderClient#requiresAuthentication()
	 */
	@Override
	protected boolean requiresAuthentication() {
		return true;
	}

	protected DataManagementOperationException fromResponse(int httpStatusCode, DataManagementOperationExceptionEntity entity) {
		if(entity == null)
			return new UnmanagedException("Unmanaged exception resulting from an HTTP " + httpStatusCode + " status code");
		
		return this.convertToException(httpStatusCode, entity);
	}

	@Override
	public void delete(int vesselId) throws DataManagementOperationException {
		this.evaluateResponse(this.request("delete/" + vesselId).delete());
	}

	@Override
	public VesselDataHierarchy getHierarchyByItemUID(int vesselUid) throws DataManagementOperationException {
		return this.evaluateResponse(this.request("hierarchy/uid/" + vesselUid).get(), VesselDataHierarchy.class);
	}
	
	@Override
	public VesselDataHierarchy getHierarchyByItemID(int vesselId) throws DataManagementOperationException {
		return this.evaluateResponse(this.request("hierarchy/id/" + vesselId).get(), VesselDataHierarchy.class);
	}

	@Override
	public VesselDataHierarchy getHierarchyBelowByItemID(int vesselId) throws DataManagementOperationException {
		return this.evaluateResponse(this.request("hierarchy/below/id/" + vesselId).get(), VesselDataHierarchy.class);
	}

	@Override
	public VesselData getDataByItemID(int vesselId) throws DataManagementOperationException {
		return this.evaluateResponse(this.request("data/id/" + vesselId).get(), VesselData.class);
	}
	
	@Override
	public GetVesselDataByUIDResponse getDataByItemUID(int vesselUid) throws DataManagementOperationException {
		return this.evaluateResponse(this.request("data/uid/" + vesselUid).get(), GetVesselDataByUIDResponse.class);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.VesselDataManagementRESTInterface#getIDByCurrentIdentifier(java.lang.String, java.lang.String)
	 */
	@Override
	public GetVesselIDResponse getIDByCurrentIdentifier(String rfmo, String rfmoId) throws DataManagementOperationException {
		return this.evaluateResponse(this.request("ID/current/" + rfmo + "/" + rfmoId).get(), GetVesselIDResponse.class);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.VesselDataManagementRESTInterface#getIDByHistoricalIdentifier(java.lang.String, java.lang.String)
	 */
	@Override
	public GetVesselIDMultipleResponse getIDByHistoricalIdentifier(String rfmo, String rfmoId) throws DataManagementOperationException {
		return this.evaluateResponse(this.request("ID/historical/" + rfmo + "/" + rfmoId).get(), GetVesselIDMultipleResponse.class);
	}

	@Override
	public GetVesselUIDResponse getUID(int vesselId) throws DataManagementOperationException {
		return this.evaluateResponse(this.request("UID/" + vesselId).get(), GetVesselUIDResponse.class);
	}

	@Override
	public void link(Integer sourceVesselId, Integer targetVesselId, String user, Double score, Long mappingDateTimestamp, String mappingComment) throws DataManagementOperationException {
		Form form = new Form();
		
		form.param("sourceVesselId", String.valueOf(sourceVesselId));
		form.param("targetVesselId", String.valueOf(targetVesselId));
		form.param("score", score == null ? null : String.valueOf(score));
		form.param("user", user);
		form.param("mappingDate", mappingDateTimestamp == null ? null : String.valueOf(mappingDateTimestamp));
		form.param("mappingComment", mappingComment);

		this.evaluateResponse(this.request("link").post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE), Response.class));
	}

	@Override
	public UnlinkVesselResponse unlink(Integer vesselId) throws DataManagementOperationException {
		Form form = new Form();
		
		form.param("vesselId", String.valueOf(vesselId));

		return this.evaluateResponse(this.request("unlink").post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE), Response.class), UnlinkVesselResponse.class);
	}

	@Override
	public void merge(Integer sourceVesselId, Integer targetVesselId) throws DataManagementOperationException {
		Form form = new Form();
		
		form.param("sourceVesselId", String.valueOf(sourceVesselId));
		form.param("targetVesselId", String.valueOf(targetVesselId));

		this.evaluateResponse(this.request("merge").post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE), Response.class));
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.VesselDataManagementRESTInterface#checkConsistency()
	 */
	@Override
	public CheckConsistencyResponse checkConsistency() throws DataManagementOperationException {
		return this.evaluateResponse(this.request("db/check").get(), CheckConsistencyResponse.class);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.VesselDataManagementRESTInterface#checkMultipleIMOByUID()
	 */
	@Override
	public CheckMultipleIMOByUIDResponse checkMultipleIMOByUID() throws DataManagementOperationException {
		return this.evaluateResponse(this.request("db/check/IMO/UID").get(), CheckMultipleIMOByUIDResponse.class);

	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.VesselDataManagementRESTInterface#checkMultipleUIDByIMO()
	 */
	@Override
	public CheckMultipleUIDByIMOResponse checkMultipleUIDByIMO() throws DataManagementOperationException {
		return this.evaluateResponse(this.request("db/check/UID/IMO").get(), CheckMultipleUIDByIMOResponse.class);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.VesselDataManagementRESTInterface#updateDB()
	 */
	@Override
	public DBUpdateResult updateDB() throws DataManagementOperationException {
		return this.evaluateResponse(this.request("db/update").post(Entity.entity(new Form(), MediaType.APPLICATION_FORM_URLENCODED_TYPE)), DBUpdateResult.class);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.VesselDataManagementRESTInterface#executeStatement(java.lang.String)
	 */
	@Override
	public ExecuteStatementResponse executeStatement(String statement) throws DataManagementOperationException {
		Form form = new Form();
		form.param("statement", statement);

		return this.evaluateResponse(this.request("execute").post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE), Response.class), ExecuteStatementResponse.class);
	}
	
	final static public void main(String[] args) throws Throwable {
		VesselsDataManagementRemoteOperationsProviderClient c = new VesselsDataManagementRemoteOperationsProviderClient();
		c.setEndpoint("http://localhost:80/management/console/vessels/operations");
		c.setUsername("fabio");
		c.setPassword("fabio");
		c.postConstruct();
		System.out.println(c.getHierarchyByItemID(2021));
		System.out.println(c.getIDByCurrentIdentifier("CCSBT", "2").getVesselId());
		System.out.println(c.getIDByCurrentIdentifier("CCSBT", "2ab"));
	}
}