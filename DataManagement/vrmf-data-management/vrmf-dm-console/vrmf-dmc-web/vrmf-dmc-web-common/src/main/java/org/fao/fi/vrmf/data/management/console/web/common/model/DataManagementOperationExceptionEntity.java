/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.web.common.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Fiorellato
 *
 */
@XmlRootElement(name="DataManagementOperationExceptionEntity")
@XmlAccessorType(XmlAccessType.FIELD)
public class DataManagementOperationExceptionEntity implements Serializable {
	private static final long serialVersionUID = 1616843258589155462L;

	@XmlElement(name="Code")
	private int _code;
	
	@XmlElement(name="OriginalType")
	private String _originalType;
	
	@XmlElement(name="Message")
	private String _message;
	
	public DataManagementOperationExceptionEntity() {
	}

	public DataManagementOperationExceptionEntity(int code, String originalType, String message) {
		super();
		this._code = code;
		this._originalType = originalType;
		this._message = message;
	}

	public int getCode() {
		return this._code;
	}

	public void setCode(int code) {
		this._code = code;
	}

	public String getOriginalType() {
		return this._originalType;
	}

	public void setOriginalType(String originalType) {
		this._originalType = originalType;
	}

	public String getMessage() {
		return this._message;
	}

	public void setMessage(String message) {
		this._message = message;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this._code;
		result = prime * result + ((this._message == null) ? 0 : this._message.hashCode());
		result = prime * result + ((this._originalType == null) ? 0 : this._originalType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DataManagementOperationExceptionEntity other = (DataManagementOperationExceptionEntity) obj;
		if (this._code != other._code)
			return false;
		if (this._message == null) {
			if (other._message != null)
				return false;
		} else if (!this._message.equals(other._message))
			return false;
		if (this._originalType == null) {
			if (other._originalType != null)
				return false;
		} else if (!this._originalType.equals(other._originalType))
			return false;
		return true;
	}
}
