/**
 * (c) 2015 FAO / UN (project: vrmf-data-management-console-web-common)
 */
package org.fao.fi.vrmf.data.management.console.web.common.support.response;

import java.io.Serializable;
import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 13, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 13, 2015
 */
@XmlRootElement(name="ExecuteStatementResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class ExecuteStatementResponse implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 584445836839272685L;

	@XmlElement private String statement;
	@XmlElementWrapper(name="data") @XmlElement(name="row") private Collection<String[]> data;

	/**
	 * Class constructor
	 *
	 */
	public ExecuteStatementResponse() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param statement
	 * @param data
	 */
	public ExecuteStatementResponse(String statement, Collection<String[]> data) {
		super();
		this.statement = statement;
		this.data = data;
	}
	
	/**
	 * @return the 'statement' value
	 */
	public String getStatement() {
		return statement;
	}
	/**
	 * @param statement the 'statement' value to set
	 */
	public void setStatement(String statement) {
		this.statement = statement;
	}
	/**
	 * @return the 'data' value
	 */
	public Collection<String[]> getData() {
		return data;
	}
	/**
	 * @param data the 'data' value to set
	 */
	public void setData(Collection<String[]> data) {
		this.data = data;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( data == null ) ? 0 : data.hashCode() );
		result = prime * result + ( ( statement == null ) ? 0 : statement.hashCode() );
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if(getClass() != obj.getClass()) return false;
		ExecuteStatementResponse other = (ExecuteStatementResponse) obj;
		if(data == null) {
			if(other.data != null) return false;
		} else if(!data.equals(other.data)) return false;
		if(statement == null) {
			if(other.statement != null) return false;
		} else if(!statement.equals(other.statement)) return false;
		return true;
	}
}
