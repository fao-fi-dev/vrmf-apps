/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.web.common.vessels.support;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.fao.fi.vrmf.data.management.console.web.common.vessels.model.VesselsDataManagementOperationExceptionEntity;
import org.fao.fi.vrmf.data.management.vessels.model.VesselData;
import org.fao.fi.vrmf.data.management.vessels.model.VesselDataHierarchy;

/**
 * @author Fiorellato
 *
 */
@Provider
public class VesselsDataCustomJAXBContext implements ContextResolver<JAXBContext> {
	private JAXBContext _context = null;

	static final private Set<Class<?>> CUSTOM_CLASSES = 
		new HashSet<Class<?>>(
			Arrays.asList(new Class<?>[] {
				VesselDataHierarchy.class,
				VesselsDataManagementOperationExceptionEntity.class
			}));
	
	public JAXBContext getContext(Class<?> type) {
		if(!CUSTOM_CLASSES.contains(type)) {
			return null;
		}

		if(this._context == null) {
			try {
				this._context = JAXBContext.newInstance(
					VesselDataHierarchy.class,
					VesselData.class,
					VesselsDataManagementOperationExceptionEntity.class
				);
			} catch(JAXBException e) {
				//
			}
		}

		return this._context;
	}
}
