/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.web.common.vessels.spi;

import org.fao.fi.vrmf.data.management.common.model.DBUpdateResult;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.DataManagementOperationException;
import org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response.CheckMultipleIMOByUIDResponse;
import org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response.CheckMultipleUIDByIMOResponse;
import org.fao.fi.vrmf.data.management.console.web.common.support.response.ExecuteStatementResponse;
import org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response.CheckConsistencyResponse;
import org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response.GetVesselDataByUIDResponse;
import org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response.GetVesselIDMultipleResponse;
import org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response.GetVesselIDResponse;
import org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response.GetVesselUIDResponse;
import org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response.UnlinkVesselResponse;
import org.fao.fi.vrmf.data.management.vessels.model.VesselData;
import org.fao.fi.vrmf.data.management.vessels.model.VesselDataHierarchy;

/**
 * @author Fiorellato
 *
 */
public interface VesselDataManagementRESTInterface {
	void delete(int vesselId) throws DataManagementOperationException;

	VesselDataHierarchy getHierarchyByItemUID(int vesselUid) throws DataManagementOperationException;
	VesselDataHierarchy getHierarchyByItemID(int vesselId) throws DataManagementOperationException;
	VesselDataHierarchy getHierarchyBelowByItemID(int vesselId) throws DataManagementOperationException;
	
	VesselData getDataByItemID(int vesselId) throws DataManagementOperationException;

	GetVesselDataByUIDResponse getDataByItemUID(int vesselUid) throws DataManagementOperationException;
	
	GetVesselIDResponse getIDByCurrentIdentifier(String rfmo, String rfmoId) throws DataManagementOperationException;
	GetVesselIDMultipleResponse getIDByHistoricalIdentifier(String rfmo, String rfmoId) throws DataManagementOperationException;
	
	GetVesselUIDResponse getUID(int vesselId) throws DataManagementOperationException;

	void link(Integer sourceVesselId, Integer targetVesselId, String user, Double score, Long mappingDateTimestamp, String mappingComment) throws DataManagementOperationException;
	UnlinkVesselResponse unlink(Integer vesselId) throws DataManagementOperationException;

	void merge(Integer sourceVesselId, Integer targetVesselId) throws DataManagementOperationException;
	
	DBUpdateResult updateDB() throws DataManagementOperationException;
	
	CheckConsistencyResponse checkConsistency() throws DataManagementOperationException;
	CheckMultipleIMOByUIDResponse checkMultipleIMOByUID() throws DataManagementOperationException;
	CheckMultipleUIDByIMOResponse checkMultipleUIDByIMO() throws DataManagementOperationException;
	
	ExecuteStatementResponse executeStatement(String statement) throws DataManagementOperationException;
}