/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Fiorellato
 *
 */
@XmlRootElement(name="GetVesselIDResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetVesselIDResponse extends AbstractGetVesselIDResponse {
	private static final long serialVersionUID = -2933637333651930772L;
	
	@XmlAttribute(name="VesselId")
	private Integer _vesselId;
	
	public GetVesselIDResponse() {
	}

	public GetVesselIDResponse(String rfmo, String rfmoId, Integer vesselId) {
		super(rfmo, rfmoId);
		this._vesselId = vesselId;
	}

	public Integer getVesselId() {
		return this._vesselId;
	}

	public void setVesselId(Integer vesselId) {
		this._vesselId = vesselId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this._vesselId == null) ? 0 : this._vesselId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		GetVesselIDResponse other = (GetVesselIDResponse) obj;
		if (this._vesselId == null) {
			if (other._vesselId != null)
				return false;
		} else if (!this._vesselId.equals(other._vesselId))
			return false;
		return true;
	}
}
