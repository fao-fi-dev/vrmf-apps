/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response;

import java.io.Serializable;
import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.vrmf.data.management.common.model.ConsistencyCheckResult;

/**
 * @author Fiorellato
 *
 */
@XmlRootElement(name="CheckConsistencyResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class CheckConsistencyResponse implements Serializable {
	private static final long serialVersionUID = 2494259449761861734L;
	
	@XmlElementWrapper(name="ConsistencyCheckResults")
	@XmlElement(name="ConsistencyCheckResult")
	private Collection<ConsistencyCheckResult> _results;
	
	public CheckConsistencyResponse() {
	}

	public CheckConsistencyResponse(Collection<ConsistencyCheckResult> results) {
		super();
		this._results = results;
	}

	public Collection<ConsistencyCheckResult> getResults() {
		return this._results;
	}

	public void setResults(Collection<ConsistencyCheckResult> results) {
		this._results = results;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this._results == null) ? 0 : this._results.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckConsistencyResponse other = (CheckConsistencyResponse) obj;
		if (this._results == null) {
			if (other._results != null)
				return false;
		} else if (!this._results.equals(other._results))
			return false;
		return true;
	}
}
