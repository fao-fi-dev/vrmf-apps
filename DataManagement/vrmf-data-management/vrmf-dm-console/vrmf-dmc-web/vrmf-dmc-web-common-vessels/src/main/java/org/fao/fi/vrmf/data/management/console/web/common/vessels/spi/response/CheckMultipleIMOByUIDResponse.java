/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response;

import java.io.Serializable;
import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.vrmf.data.management.vessels.model.MultipleIMOByUIDCheckResult;

/**
 * @author Fiorellato
 *
 */
@XmlRootElement(name="CheckMultipleIMOByUIDResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class CheckMultipleIMOByUIDResponse implements Serializable {
	private static final long serialVersionUID = -4163860593894863308L;
	
	@XmlElementWrapper(name="Results")
	@XmlElement(name="MultipleIMOByUIDCheckResult")
	private Collection<MultipleIMOByUIDCheckResult> _data;
	
	public CheckMultipleIMOByUIDResponse() {
	}

	public CheckMultipleIMOByUIDResponse(Collection<MultipleIMOByUIDCheckResult> data) {
		super();
		this._data = data;
	}

	public Collection<MultipleIMOByUIDCheckResult> getData() {
		return this._data;
	}

	public void setData(Collection<MultipleIMOByUIDCheckResult> data) {
		this._data = data;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this._data == null) ? 0 : this._data.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckMultipleIMOByUIDResponse other = (CheckMultipleIMOByUIDResponse) obj;
		if (this._data == null) {
			if (other._data != null)
				return false;
		} else if (!this._data.equals(other._data))
			return false;
		return true;
	}
}
