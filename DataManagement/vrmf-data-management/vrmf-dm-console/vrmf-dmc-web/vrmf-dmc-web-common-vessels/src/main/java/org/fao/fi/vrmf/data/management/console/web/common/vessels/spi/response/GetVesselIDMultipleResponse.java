/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response;

import java.util.Arrays;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Fiorellato
 *
 */
@XmlRootElement(name="GetVesselIDMultipleResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetVesselIDMultipleResponse extends AbstractGetVesselIDResponse {
	private static final long serialVersionUID = -2933637333651930772L;
	
	@XmlElementWrapper(name="VesselIds")
	@XmlElement(name="VesselId")
	private int[] _vesselIds;
	
	public GetVesselIDMultipleResponse() {
	}

	public GetVesselIDMultipleResponse(String rfmo, String rfmoId, int[] vesselIds) {
		super(rfmo, rfmoId);
		this._vesselIds = vesselIds;
	}

	public int[] getVesselIds() {
		return this._vesselIds;
	}

	public void setVesselIds(int[] vesselIds) {
		this._vesselIds = vesselIds;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Arrays.hashCode(this._vesselIds);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		GetVesselIDMultipleResponse other = (GetVesselIDMultipleResponse) obj;
		if (!Arrays.equals(this._vesselIds, other._vesselIds))
			return false;
		return true;
	}
}
