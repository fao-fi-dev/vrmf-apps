/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Fiorellato
 *
 */
@XmlRootElement(name="GetVesselIDResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetVesselUIDResponse implements Serializable {
	private static final long serialVersionUID = -2933637333651930772L;

	@XmlAttribute(name="vesselId")
	private Integer _vesselId;
	
	@XmlElement(name="VesselUid")
	private Integer _vesselUid;
	
	public GetVesselUIDResponse() {
	}

	public GetVesselUIDResponse(Integer vesselId, Integer vesselUid) {
		super();
		this._vesselId = vesselId;
		this._vesselUid = vesselUid;
	}

	public Integer getVesselId() {
		return this._vesselId;
	}

	public void setVesselId(Integer vesselId) {
		this._vesselId = vesselId;
	}

	public Integer getVesselUid() {
		return this._vesselUid;
	}

	public void setVesselUid(Integer vesselUid) {
		this._vesselUid = vesselUid;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this._vesselId == null) ? 0 : this._vesselId.hashCode());
		result = prime * result + ((this._vesselUid == null) ? 0 : this._vesselUid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GetVesselUIDResponse other = (GetVesselUIDResponse) obj;
		if (this._vesselId == null) {
			if (other._vesselId != null)
				return false;
		} else if (!this._vesselId.equals(other._vesselId))
			return false;
		if (this._vesselUid == null) {
			if (other._vesselUid != null)
				return false;
		} else if (!this._vesselUid.equals(other._vesselUid))
			return false;
		return true;
	}
}
