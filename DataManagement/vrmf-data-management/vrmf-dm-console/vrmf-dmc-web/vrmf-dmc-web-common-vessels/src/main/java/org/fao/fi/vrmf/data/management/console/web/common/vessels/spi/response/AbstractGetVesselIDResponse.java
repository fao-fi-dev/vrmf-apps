/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Fiorellato
 *
 */
@XmlRootElement(name="AbstractGetVesselIDResponse")
@XmlAccessorType(XmlAccessType.FIELD)
abstract public class AbstractGetVesselIDResponse implements Serializable {
	private static final long serialVersionUID = -2933637333651930772L;

	@XmlAttribute(name="RFMO")
	private String _rfmo;
	
	@XmlAttribute(name="RFMOId")
	private String _rfmoId;
	
	public AbstractGetVesselIDResponse() {
	}

	public AbstractGetVesselIDResponse(String rfmo, String rfmoId) {
		super();
		this._rfmo = rfmo;
		this._rfmoId = rfmoId;
	}

	public String getRfmo() {
		return this._rfmo;
	}

	public void setRfmo(String rfmo) {
		this._rfmo = rfmo;
	}

	public String getRfmoId() {
		return this._rfmoId;
	}

	public void setRfmoId(String rfmoId) {
		this._rfmoId = rfmoId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this._rfmo == null) ? 0 : this._rfmo.hashCode());
		result = prime * result + ((this._rfmoId == null) ? 0 : this._rfmoId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractGetVesselIDResponse other = (AbstractGetVesselIDResponse) obj;
		if (this._rfmo == null) {
			if (other._rfmo != null)
				return false;
		} else if (!this._rfmo.equals(other._rfmo))
			return false;
		if (this._rfmoId == null) {
			if (other._rfmoId != null)
				return false;
		} else if (!this._rfmoId.equals(other._rfmoId))
			return false;
		return true;
	}
}
