/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.web.common.vessels.model;

import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.vrmf.data.management.common.spi.exceptions.DataManagementOperationException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.UnmanagedException;
import org.fao.fi.vrmf.data.management.console.web.common.model.DataManagementOperationExceptionEntity;

/**
 * @author Fiorellato
 *
 */
@XmlRootElement(name="DataManagementOperationExceptionEntity")
public class VesselsDataManagementOperationExceptionEntity extends DataManagementOperationExceptionEntity {
	private static final long serialVersionUID = -3412087428862113691L;
	
	public VesselsDataManagementOperationExceptionEntity() {
		super();
	}

	public VesselsDataManagementOperationExceptionEntity(int code, String originalType, String message) {
		super(code, originalType, message);
	}
	
	public VesselsDataManagementOperationExceptionEntity(Throwable t) {
		this.setOriginalType(t.getClass().getName());
		this.setMessage(t.getMessage());
		
		if(DataManagementOperationException.class.isAssignableFrom(t.getClass()))
			this.setCode(((DataManagementOperationException)t).getExceptionCode());
		else
			this.setCode(UnmanagedException.EXCEPTION_CODE);
	}
}
