/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Fiorellato
 *
 */
@XmlRootElement(name="UnlinkVesselResponse")
public class UnlinkVesselResponse implements Serializable {
	private static final long serialVersionUID = -1521191811859396681L;

	@XmlAttribute(name="vesselId")
	private int _vesselId;
	
	@XmlElement(name="NewVesselUid")
	private int _newVesselUid;

	public UnlinkVesselResponse() {
	}

	public UnlinkVesselResponse(int vesselId, int newVesselUId) {
		super();
		this._vesselId = vesselId;
		this._newVesselUid = newVesselUId;
	}

	public int getVesselId() {
		return this._vesselId;
	}

	public void setVesselId(int vesselId) {
		this._vesselId = vesselId;
	}

	public int getNewVesselUid() {
		return this._newVesselUid;
	}

	public void setNewVesselUId(int newVesselUId) {
		this._newVesselUid = newVesselUId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this._newVesselUid;
		result = prime * result + this._vesselId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnlinkVesselResponse other = (UnlinkVesselResponse) obj;
		if (this._newVesselUid != other._newVesselUid)
			return false;
		if (this._vesselId != other._vesselId)
			return false;
		return true;
	}
}
