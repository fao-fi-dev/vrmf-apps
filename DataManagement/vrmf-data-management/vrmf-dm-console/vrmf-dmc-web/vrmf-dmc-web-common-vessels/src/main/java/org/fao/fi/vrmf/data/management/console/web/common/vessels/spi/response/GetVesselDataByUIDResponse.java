/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.web.common.vessels.spi.response;

import java.io.Serializable;
import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.vrmf.data.management.vessels.model.VesselData;

/**
 * @author Fiorellato
 *
 */
@XmlRootElement(name="GetVesselDataByUIDResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class GetVesselDataByUIDResponse implements Serializable {
	private static final long serialVersionUID = 7230072710624529013L;
	
	@XmlElementWrapper(name="VesselsData")
	@XmlElement(name="VesselData")
	private Collection<VesselData> _data;
	
	public GetVesselDataByUIDResponse() {
	}
	
	public GetVesselDataByUIDResponse(Collection<VesselData> data) {
		this._data = data;
	}

	public Collection<VesselData> getData() {
		return this._data;
	}

	public void setData(Collection<VesselData> data) {
		this._data = data;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this._data == null) ? 0 : this._data.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GetVesselDataByUIDResponse other = (GetVesselDataByUIDResponse) obj;
		if (this._data == null) {
			if (other._data != null)
				return false;
		} else if (!this._data.equals(other._data))
			return false;
		return true;
	}
}
