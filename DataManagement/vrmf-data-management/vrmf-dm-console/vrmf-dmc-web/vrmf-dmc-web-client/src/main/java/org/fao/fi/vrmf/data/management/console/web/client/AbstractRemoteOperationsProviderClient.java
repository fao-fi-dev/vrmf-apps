/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.web.client;

import java.net.MalformedURLException;
import java.net.URL;

import javax.annotation.PostConstruct;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.fao.fi.vrmf.data.management.common.spi.exceptions.DataManagementOperationException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.UnmanagedException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.DifferentSourcesException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.HierarchyHasCyclesException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemAlreadyMappedException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemHasMappingsException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemIsNotMappedException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemNotFoundException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.MultipleItemsFoundException;
import org.fao.fi.vrmf.data.management.console.web.common.model.DataManagementOperationExceptionEntity;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.message.internal.MessageBodyProviderNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

/**
 * @author Fiorellato
 */
abstract public class AbstractRemoteOperationsProviderClient {
	protected Logger _log = LoggerFactory.getLogger(this.getClass());
	
	@Value("${remote.client.endpoint}") protected String _endpoint;
	
	@Value("${remote.client.username}") protected String _username;
	@Value("${remote.client.password}") protected String _password;
	
	protected ClientConfig _config;
	protected Client _client;
	
	public AbstractRemoteOperationsProviderClient() {
		this._config = new ClientConfig();
	}
	
	@PostConstruct
	protected void postConstruct() {
		this._client = ClientBuilder.newClient(this._config);
		
		if(this.requiresAuthentication()) {
			this._client.register(HttpAuthenticationFeature.basic(this._username, this._password));
		}
	}
	
	abstract protected boolean requiresAuthentication();
	
	abstract protected DataManagementOperationException fromResponse(int httpStatusCode, DataManagementOperationExceptionEntity entity);
	
	
	final public String getEndpoint() {
		return this._endpoint;
	}
	
	final public void setEndpoint(String endpoint) throws MalformedURLException {
		this.setEndpoint(new URL(endpoint));
	}
	
	final protected void setEndpoint(URL endpoint) {
		this._endpoint = endpoint.toString();
		
		if(!this._endpoint.endsWith("/"))
			this._endpoint += "/";
		
		this._log.info("{} will use {} as service endpoint", this.getClass().getSimpleName(), this._endpoint);
	}
	
	final public String getUsername() {
		return this._username;
	}
	
	final public void setUsername(String username) {
		this._username = username;
	}

	final public String getPassword() {
		return this._password;
	}

	final public void setPassword(String password) {
		this._password = password;
	}
	
	final private WebTarget target(String path) {
		String endpoint = this._endpoint;
		
		if(!endpoint.endsWith("/"))
			endpoint += "/";
		
		if(path.startsWith("/"))
			path = path.substring(1);
		
		endpoint = endpoint + path;
		
		this._log.debug("Targeting {} for {}", endpoint, path);
		
		return this._client.target(endpoint);
	}
	
	final protected Builder request(String path) {
		Builder builder = this.target(path).request();
		
		if(this.requiresAuthentication()) {
			builder.
				property(HttpAuthenticationFeature.HTTP_AUTHENTICATION_BASIC_USERNAME, this._username).
				property(HttpAuthenticationFeature.HTTP_AUTHENTICATION_BASIC_PASSWORD, this._password);
		}
		
		return builder;
	}
	
	final protected void evaluateResponse(Response toEvaluate) throws DataManagementOperationException {
		if(toEvaluate.getStatusInfo().getFamily() != Status.Family.SUCCESSFUL) {
			throw this.fromResponse(toEvaluate.getStatus(), toEvaluate.readEntity(DataManagementOperationExceptionEntity.class));
		}
	}
	
	final protected <E> E evaluateResponse(Response toEvaluate, Class<E> clazz) throws DataManagementOperationException {
		if(toEvaluate.getStatusInfo().getFamily() != Status.Family.SUCCESSFUL) {
			try {
				throw this.fromResponse(toEvaluate.getStatus(), toEvaluate.readEntity(DataManagementOperationExceptionEntity.class));
			} catch(MessageBodyProviderNotFoundException MBPNFe) {
				throw new UnmanagedException("Unexpected HTTP " + toEvaluate.getStatus() + " error caught [ " + toEvaluate.getStatusInfo().getReasonPhrase() + " ]");
			}
		}
		
		return toEvaluate.readEntity(clazz);
	}
	
	protected DataManagementOperationException convertToException(int httpStatusCode, DataManagementOperationExceptionEntity entity) {
		switch(entity.getCode()) {
			case ItemNotFoundException.EXCEPTION_CODE:
				return new ItemNotFoundException(entity.getMessage());
			case MultipleItemsFoundException.EXCEPTION_CODE:
				return new MultipleItemsFoundException(entity.getMessage());
			case ItemIsNotMappedException.EXCEPTION_CODE:
				return new ItemIsNotMappedException(entity.getMessage());
			case ItemAlreadyMappedException.EXCEPTION_CODE:
				return new ItemAlreadyMappedException(entity.getMessage());
			case ItemHasMappingsException.EXCEPTION_CODE:
				return new ItemHasMappingsException(entity.getMessage());
			case HierarchyHasCyclesException.EXCEPTION_CODE:
				return new HierarchyHasCyclesException(entity.getMessage());
			case DifferentSourcesException.EXCEPTION_CODE:
				return new DifferentSourcesException(entity.getMessage());
			default:
				return new UnmanagedException(entity.getMessage());
		}
	}
}