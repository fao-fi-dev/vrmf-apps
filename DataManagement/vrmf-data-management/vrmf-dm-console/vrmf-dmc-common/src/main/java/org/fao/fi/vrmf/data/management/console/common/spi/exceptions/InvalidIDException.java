/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.common.spi.exceptions;

/**
 * @author Fiorellato
 *
 */
public class InvalidIDException extends ConsoleCommandException {
	private static final long serialVersionUID = 8416459407602556719L;

	public InvalidIDException() {
	}

	/**
	 * @param message
	 */
	public InvalidIDException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public InvalidIDException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public InvalidIDException(String message, Throwable cause) {
		super(message, cause);
	}
}
