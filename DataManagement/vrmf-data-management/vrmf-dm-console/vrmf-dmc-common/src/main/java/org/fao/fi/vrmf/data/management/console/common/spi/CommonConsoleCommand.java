/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.common.spi;

import java.util.Properties;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.fao.fi.vrmf.data.management.common.spi.DataManagementOperationsProvider;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.DataManagementOperationException;
import org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter;
import org.fao.fi.vrmf.data.management.console.common.spi.exceptions.ConsoleCommandError;
import org.fao.fi.vrmf.data.management.console.common.spi.exceptions.ConsoleCommandException;

/**
 * @author Fiorellato
 *
 */
public interface CommonConsoleCommand extends Command {
	default void execute(Properties properties, String[] parameters, DataManagementOperationsProvider<?, ?> provider, AbstractEmitter emitter) throws ConsoleCommandException, ConsoleCommandError {
		try {
			CommandLine cmd = new PosixParser().parse(this.getOptions(), parameters);
			
			this.doExecute(properties, cmd, provider, emitter);
		} catch(ParseException|DataManagementOperationException e) {
			throw new ConsoleCommandException(e.getMessage());
		} catch(ConsoleCommandException CCe) {
			throw CCe;
		} catch(Throwable t) {
			throw this.raiseError(t);
		}
	}
	
	void doExecute(Properties properties, CommandLine commandLine, DataManagementOperationsProvider<?, ?> provider, AbstractEmitter emitter) throws Exception;
}