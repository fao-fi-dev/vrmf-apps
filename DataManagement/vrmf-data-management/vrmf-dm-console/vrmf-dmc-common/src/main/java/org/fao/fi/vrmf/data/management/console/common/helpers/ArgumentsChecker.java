/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.common.helpers;

import org.fao.fi.vrmf.data.management.console.common.spi.Command;
import org.fao.fi.vrmf.data.management.console.common.spi.exceptions.ConsoleCommandException;

/**
 * @author Fiorellato
 *
 */
final public class ArgumentsChecker {
	static final public int UNBOUNDED = Integer.MAX_VALUE;
	
	private ArgumentsChecker() {
	}
	
	static public void requires(String command, String[] arguments, int minArguments, int maxArguments) throws ConsoleCommandException {
		int numArguments = arguments == null ? null : arguments.length;
		
		if(minArguments == maxArguments && minArguments != numArguments)
			throw new ConsoleCommandException("The '" + command + "' command requires exactly " + minArguments + " argument" + ( minArguments > 1 ? "s" : "" ) + " to be executed (provided number of arguments is " + numArguments + ")");
		
		if(numArguments < minArguments) {
			throw new ConsoleCommandException("The '" + command + "' command requires at least " + minArguments + " argument" + ( minArguments > 1 ? "s" : "" ) + " to be executed (provided number of arguments is " + numArguments + ")");
		}
		
		if(Integer.compare(maxArguments, UNBOUNDED) < 0 && numArguments > maxArguments) {
			throw new ConsoleCommandException("The '" + command + "' command requires at most " + maxArguments + " argument" + ( maxArguments > 1 ? "s" : "" ) + " to be executed (provided number of arguments is " + numArguments + ")");
		}
	}
	
	static public void requires(Command command, String[] arguments, int minArguments, int maxArguments) throws ConsoleCommandException {
		ArgumentsChecker.requires(command.getCommandName(), arguments, minArguments, maxArguments);
	}
	
	static public void requiresExactly(String command, String[] arguments, int numArguments) throws ConsoleCommandException {
		ArgumentsChecker.requires(command, arguments, numArguments, numArguments);
	}

	static public void requiresExactly(Command command, String[] arguments, int numArguments) throws ConsoleCommandException {
		ArgumentsChecker.requires(command, arguments, numArguments, numArguments);
	}

	static public void requiresAtLeast(String command, String[] arguments, int minArguments) throws ConsoleCommandException {
		ArgumentsChecker.requires(command, arguments, minArguments, UNBOUNDED);
	}
	
	static public void requiresAtLeast(Command command, String[] arguments, int minArguments) throws ConsoleCommandException {
		ArgumentsChecker.requires(command, arguments, minArguments, UNBOUNDED);
	}
	
	static public void requiresOneOrAtMost(String command, String[] arguments, int maxArguments) throws ConsoleCommandException {
		ArgumentsChecker.requires(command, arguments, 1, maxArguments);
	}
	
	static public void requiresOneOrAtMost(Command command, String[] arguments, int maxArguments) throws ConsoleCommandException {
		ArgumentsChecker.requires(command, arguments, 1, maxArguments);
	}
	
	static public void requiresAtMost(String command, String[] arguments, int maxArguments) throws ConsoleCommandException {
		ArgumentsChecker.requires(command, arguments, 0, maxArguments);
	}
	
	static public void requiresAtMost(Command command, String[] arguments, int maxArguments) throws ConsoleCommandException {
		ArgumentsChecker.requires(command, arguments, 0, maxArguments);
	}
	
	static public void doesNotRequireArguments(String command, String[] arguments) throws ConsoleCommandException {
		if(arguments.length > 0)
			throw new ConsoleCommandException("The '" + command + "' command does not require any argument (provided number of arguments is " + arguments.length + ")");
	}
	
	static public void doesNotRequireArguments(Command command, String[] arguments) throws ConsoleCommandException {
		ArgumentsChecker.doesNotRequireArguments(command.getCommandName(), arguments);
	}
}