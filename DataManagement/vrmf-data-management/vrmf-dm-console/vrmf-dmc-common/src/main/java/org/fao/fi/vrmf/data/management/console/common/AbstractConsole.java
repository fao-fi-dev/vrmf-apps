package org.fao.fi.vrmf.data.management.console.common;

import static org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter.DEFAULT_COMMAND_ARGS_COLOR;
import static org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter.DEFAULT_COMMAND_COLOR;
import static org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter.DEFAULT_COMMAND_DESC_COLOR;
import static org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter.DEFAULT_COMMAND_OPTS_COLOR;
import static org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter.DEFAULT_INFO_COLOR;
import static org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter.RESET_COLOR;
import static org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter.bold;
import static org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter.color;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.filechooser.FileFilter;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.vrmf.data.management.common.spi.DataManagementOperationsProvider;
import org.fao.fi.vrmf.data.management.common.spi.ManagedData;
import org.fao.fi.vrmf.data.management.common.spi.ManagedDataHierarchy;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.DataManagementOperationPropertyException;
import org.fao.fi.vrmf.data.management.console.common.exceptions.CommandLineParsingException;
import org.fao.fi.vrmf.data.management.console.common.exceptions.ConsoleException;
import org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter;
import org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter.Color;
import org.fao.fi.vrmf.data.management.console.common.helpers.ArgumentsChecker;
import org.fao.fi.vrmf.data.management.console.common.helpers.emitters.JLineConsoleEmitter;
import org.fao.fi.vrmf.data.management.console.common.helpers.emitters.StandardConsoleEmitter;
import org.fao.fi.vrmf.data.management.console.common.spi.CommonConsoleCommand;
import org.fao.fi.vrmf.data.management.console.common.spi.ConsoleCommand;
import org.fao.fi.vrmf.data.management.console.common.spi.exceptions.ConsoleCommandException;
import org.jodah.typetools.TypeResolver;
import org.springframework.beans.factory.annotation.Value;

/**
 * @author Fiorellato
 */
abstract public class AbstractConsole<DATA extends ManagedData<DATA>, HIERARCHY extends ManagedDataHierarchy<DATA, HIERARCHY>, OPERATIONS_PROVIDER extends DataManagementOperationsProvider<DATA, HIERARCHY>, COMMAND extends ConsoleCommand<DATA, HIERARCHY, OPERATIONS_PROVIDER>> {
	static final public String INTERACTIVE_PROPERTY		   = "console.interactive";
	static final public String VERBOSE_PROPERTY			   = "console.verbose";

	static final public String UPDATER_PROPERTY			  = "data.updater";
	private @Value("${" + UPDATER_PROPERTY + "}")   String UPDATER_ID = "ANONYMOUS";

	static final public String CACHE_MANAGEMENT_ENDPOINT_PROPERTY 			 = "cache.management.endpoint";
	static final public String CACHE_MANAGEMENT_SHOULD_AUTHENTICATE_PROPERTY = "cache.management.authenticate";
	static final public String CACHE_MANAGEMENT_USERNAME_PROPERTY 			 = "cache.management.username";
	static final public String CACHE_MANAGEMENT_PASSWORD_PROPERTY 			 = "cache.management.password";
	
	private @Value("${" + CACHE_MANAGEMENT_ENDPOINT_PROPERTY + "}")			   String CACHE_ENDPOINT = "http://localhost:8080";
	private @Value("${" + CACHE_MANAGEMENT_SHOULD_AUTHENTICATE_PROPERTY + "}") String CACHE_SHOULD_AUTHENTICATE = "true";
	private @Value("${" + CACHE_MANAGEMENT_USERNAME_PROPERTY + "}")			   String CACHE_USER = "anonymous";
	private @Value("${" + CACHE_MANAGEMENT_PASSWORD_PROPERTY + "}")			   String CACHE_PASSWORD = "";
	
	static final protected String HELP_COMMAND			   = "help";
	static final protected String HELP_COMMAND_SHORT	   = "?";

	static final protected String HISTORY_COMMAND		   = "history";
	static final protected String HISTORY_COMMAND_SHORT    = "h";

	static final protected String LOAD_COMMAND			   = "load";
	static final protected String LOAD_COMMAND_SHORT	   = "l";
	
	static final protected String SAVE_COMMAND			   = "save";
	static final protected String SAVE_COMMAND_SHORT	   = "s";
	
	static final protected String MORE_COMMAND			   = "more";
	static final protected String MORE_COMMAND_SHORT	   = "m";
	
	static final protected String CLEAR_COMMAND			   = "clear";
	static final protected String CLEAR_COMMAND_SHORT	   = "c";
	
	static final protected String SHOW_PROPERTIES_COMMAND		 = "properties"; 
	static final protected String SHOW_PROPERTIES_COMMAND_SHORT  = "p";
	
	static final protected String SET_PROPERTIES_COMMAND	   = "set";
	static final protected String SET_PROPERTIES_COMMAND_SHORT = ":=";
	
	static final protected String SET_ALIAS_COMMAND			   = "alias";
	static final protected String SET_ALIAS_COMMAND_SHORT	   = "==";

	static final protected String QUIT_COMMAND			   = "quit";
	static final protected String QUIT_COMMAND_SHORT	   = "q";
	
	static final private String VERBOSE_ALIAS = "verbose";
	static final private String VERBOSE_ALIAS_COMMAND = "set " + VERBOSE_PROPERTY + " {0}";
	
	static final private String SET_VERBOSE_ALIAS = "v+";
	static final private String SET_VERBOSE_ALIAS_COMMAND = "set " + VERBOSE_PROPERTY + " true";

	static final private String SET_NON_VERBOSE_ALIAS = "v-";
	static final private String SET_NON_VERBOSE_ALIAS_COMMAND = "set " + VERBOSE_PROPERTY + " false";
	
	static final private String INTERACTIVE_ALIAS = "interactive";
	static final private String INTERACTIVE_ALIAS_COMMAND = "set " + INTERACTIVE_PROPERTY + " {0}";
	
	static final private String SET_INTERACTIVE_ALIAS = "i+";
	static final private String SET_INTERACTIVE_ALIAS_COMMAND = "set " + INTERACTIVE_PROPERTY + " true";
	
	static final private String SET_NON_INTERACTIVE_ALIAS = "i-";
	static final private String SET_NON_INTERACTIVE_ALIAS_COMMAND = "set " + INTERACTIVE_PROPERTY + " false";
	
	static final private String ENABLE_MORE_ALIAS = "m+";
	static final private String ENABLE_MORE_ALIAS_COMMAND = MORE_COMMAND + " on";
	
	static final private String DISABLE_MORE_ALIAS = "m-";
	static final private String DISABLE_MORE_ALIAS_COMMAND = MORE_COMMAND + " off";
	
	static final protected String SCRIPT_OPT 	  = "s";
	static final protected String SCRIPT_OPT_LONG = "script";
	
	static final protected String HALT_ON_ERROR_OPT 	 = "hoe";
	static final protected String HALT_ON_ERROR_OPT_LONG = "haltOnError";

	static final protected String COLOR_OPT 	  = "c";
	static final protected String COLOR_OPT_LONG = "color";
	
	final private Date LAUNCH_TIME = new Date();
	
	final private String PROMPT = bold(Color.GREEN) + "VRMF [" + bold(Color.GRAY) + "{}" + bold(Color.GREEN) + "] > " + RESET_COLOR;
	
	protected Map<String, CommonConsoleCommand> _commonCommandsMap = new HashMap<String, CommonConsoleCommand>();
	protected Map<String, COMMAND> _commandsMap = new HashMap<String, COMMAND>();
	
	protected List<? extends CommonConsoleCommand> _commonCommands; 
	protected List<? extends COMMAND> _availableCommands; 
		
	protected Class<DATA> _target;

	protected AbstractEmitter _emitter;
	
	protected OPERATIONS_PROVIDER _provider;
	
	private List<String> _history = new ArrayList<String>();
	
	@Inject abstract protected void setOperationsProvider(OPERATIONS_PROVIDER provider);
	@Inject abstract protected void setAvailableCommands(List<COMMAND> commands);
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.AbstractConsole#setAvailableCommands(java.util.List)
	 */
	@Inject protected void setCommonCommands(List<CommonConsoleCommand> commands) {
		this._commonCommands = commands;
	}
	
	protected Properties _properties = new Properties();
	
	protected boolean _isScripted = false, _haltOnError = false;
	
	public AbstractConsole() {
		this._target = this.getTarget();
	}
	
	private String[] getCommandNames(AbstractEmitter emitter) {
		Set<String> commandNames = new HashSet<String>();
		commandNames.addAll(this._commandsMap.keySet());
		commandNames.add(HELP_COMMAND);
		commandNames.add(HISTORY_COMMAND);
		commandNames.add(SAVE_COMMAND);
		
		if(emitter.isMoreAvailable())
			commandNames.add(MORE_COMMAND);
		
		if(emitter.isClearAvailable()) 
			commandNames.add(CLEAR_COMMAND);
		
		commandNames.add(SHOW_PROPERTIES_COMMAND);
		commandNames.add(SET_PROPERTIES_COMMAND);
//		commandNames.add(SET_ALIAS_COMMAND);
		commandNames.add(QUIT_COMMAND);
		
		return commandNames.toArray(new String[commandNames.size()]);
	}

	@PostConstruct
	private void postConstruct() {
		for(CommonConsoleCommand in : this._commonCommands) {
			this._commonCommandsMap.put(in.getCommandName(), in);
		}
		for(COMMAND in : this._availableCommands) {
			this._commandsMap.put(in.getCommandName(), in);
		}
	}
	
	final protected Properties getDefaultProperties() {
		Properties defaultProperties = new Properties();
		defaultProperties.put(INTERACTIVE_PROPERTY, new Boolean(!this._isScripted).toString());
		defaultProperties.put(VERBOSE_PROPERTY, Boolean.TRUE.toString());
		defaultProperties.put(UPDATER_PROPERTY, this.UPDATER_ID);
		defaultProperties.put(CACHE_MANAGEMENT_ENDPOINT_PROPERTY, this.CACHE_ENDPOINT);
		defaultProperties.put(CACHE_MANAGEMENT_SHOULD_AUTHENTICATE_PROPERTY, this.CACHE_SHOULD_AUTHENTICATE);
		defaultProperties.put(CACHE_MANAGEMENT_USERNAME_PROPERTY, this.CACHE_USER);
		defaultProperties.put(CACHE_MANAGEMENT_PASSWORD_PROPERTY, this.CACHE_PASSWORD);
		
		defaultProperties.putAll(this.doGetAdditionalProperties());
		
		return defaultProperties;
	}
	
	final protected boolean isInteractive() {
		return Boolean.parseBoolean(this._properties.getProperty(INTERACTIVE_PROPERTY));
	}
	
	final public boolean isVerbose() {
		return Boolean.parseBoolean(this._properties.getProperty(VERBOSE_PROPERTY));
	}
	
	abstract protected Properties doGetAdditionalProperties();
	
	abstract protected boolean parseAndExecuteCustomCommands(String command, String[] arguments) throws Exception;
	
	final protected void showAliases() {
		this._emitter.nl();
		this._emitter.underline("Available aliases:");
		this._emitter.nl();
		
		this.doShowLocalAliases();

		this._emitter.nl();

		this.doShowAliases();
	}
	
	final protected void doShowLocalAliases() {
		this._emitter.emit(DEFAULT_COMMAND_COLOR, " - " + VERBOSE_ALIAS + " " + bold(Color.GREEN) + "<true|false>");
		this._emitter.info(DEFAULT_COMMAND_DESC_COLOR,  "   => Same as '" + bold(Color.YELLOW) + "set " + VERBOSE_PROPERTY + " " + bold(Color.GREEN) + "<true|false>" + DEFAULT_COMMAND_DESC_COLOR + "'");

		this._emitter.nl();
		
		this._emitter.emit(DEFAULT_COMMAND_COLOR, " - " + SET_VERBOSE_ALIAS);
		this._emitter.info(DEFAULT_COMMAND_DESC_COLOR,  "   => Same as '" + bold(Color.YELLOW) + "set " + VERBOSE_PROPERTY + " " + bold(Color.GREEN) + "true" + DEFAULT_COMMAND_DESC_COLOR + "'");

		this._emitter.nl();
		
		this._emitter.emit(DEFAULT_COMMAND_COLOR, " - " + SET_NON_VERBOSE_ALIAS);
		this._emitter.info(DEFAULT_COMMAND_DESC_COLOR,  "   => Same as '" + bold(Color.YELLOW) + "set " + VERBOSE_PROPERTY + " " + bold(Color.GREEN) + "false" + DEFAULT_COMMAND_DESC_COLOR + "'");

		this._emitter.nl();

		this._emitter.emit(DEFAULT_COMMAND_COLOR, " - " + INTERACTIVE_ALIAS + " " + bold(Color.GREEN) + "<true|false>");
		this._emitter.info(DEFAULT_COMMAND_DESC_COLOR,  "   => Same as '" + bold(Color.YELLOW) + "set " + INTERACTIVE_PROPERTY + " " + bold(Color.GREEN) + "<true|false>" + DEFAULT_COMMAND_DESC_COLOR + "'");

		this._emitter.nl();

		this._emitter.emit(DEFAULT_COMMAND_COLOR, " - " + SET_INTERACTIVE_ALIAS);
		this._emitter.info(DEFAULT_COMMAND_DESC_COLOR,  "   => Same as '" + bold(Color.YELLOW) + "set " + INTERACTIVE_PROPERTY + " " + bold(Color.GREEN) + "true" + DEFAULT_COMMAND_DESC_COLOR + "'");

		this._emitter.nl();

		this._emitter.emit(DEFAULT_COMMAND_COLOR, " - " + SET_NON_INTERACTIVE_ALIAS);
		this._emitter.info(DEFAULT_COMMAND_DESC_COLOR,  "   => Same as '" + bold(Color.YELLOW) + "set " + INTERACTIVE_PROPERTY + " " + bold(Color.GREEN) + "false" + DEFAULT_COMMAND_DESC_COLOR + "'");

		if(this._emitter.isMoreAvailable()) {
			this._emitter.nl();
	
			this._emitter.emit(DEFAULT_COMMAND_COLOR, " - " + ENABLE_MORE_ALIAS);
			this._emitter.info(DEFAULT_COMMAND_DESC_COLOR,  "   => Same as '" + bold(Color.YELLOW) + MORE_COMMAND + " " + bold(Color.GREEN) + "on" + DEFAULT_COMMAND_DESC_COLOR + "'");
	
			this._emitter.nl();
	
			this._emitter.emit(DEFAULT_COMMAND_COLOR, " - " + DISABLE_MORE_ALIAS);
			this._emitter.info(DEFAULT_COMMAND_DESC_COLOR,  "   => Same as '" + bold(Color.YELLOW) + MORE_COMMAND + " " + bold(Color.GREEN) + "off" + DEFAULT_COMMAND_DESC_COLOR + "'");
		}
	}
	
	abstract protected void doShowAliases();
	
	final protected boolean parseAndExecuteAliases(String alias, String[] arguments) throws Exception {
		boolean local = this.doParseAndExecuteLocalAliases(alias, arguments);
		
		if(!local)
			return this.doParseAndExecuteAliases(alias, arguments);
		
		return local;
	}
	
	final protected boolean doParseAndExecuteLocalAliases(String alias, String[] arguments) throws Exception {
		String command = null;
		
		switch(alias) {
			case VERBOSE_ALIAS:
				ArgumentsChecker.requiresExactly(VERBOSE_ALIAS, arguments, 1);
				command = VERBOSE_ALIAS_COMMAND;
				break;
			case SET_VERBOSE_ALIAS:
				ArgumentsChecker.doesNotRequireArguments(SET_VERBOSE_ALIAS, arguments);
				command = SET_VERBOSE_ALIAS_COMMAND;
				break;
			case SET_NON_VERBOSE_ALIAS:
				ArgumentsChecker.doesNotRequireArguments(SET_NON_VERBOSE_ALIAS, arguments);
				command = SET_NON_VERBOSE_ALIAS_COMMAND;
				break;
			case INTERACTIVE_ALIAS:
				ArgumentsChecker.requiresExactly(INTERACTIVE_ALIAS, arguments, 1);
				command = INTERACTIVE_ALIAS_COMMAND;
				break;
			case SET_INTERACTIVE_ALIAS:
				ArgumentsChecker.doesNotRequireArguments(SET_INTERACTIVE_ALIAS, arguments);
				command = SET_INTERACTIVE_ALIAS_COMMAND;
				break;
			case SET_NON_INTERACTIVE_ALIAS:
				ArgumentsChecker.doesNotRequireArguments(SET_NON_INTERACTIVE_ALIAS, arguments);
				command = SET_NON_INTERACTIVE_ALIAS_COMMAND;
				break;
			case ENABLE_MORE_ALIAS:
				ArgumentsChecker.doesNotRequireArguments(ENABLE_MORE_ALIAS, arguments);
				command = ENABLE_MORE_ALIAS_COMMAND;
				break;
			case DISABLE_MORE_ALIAS:
				ArgumentsChecker.doesNotRequireArguments(DISABLE_MORE_ALIAS, arguments);
				command = DISABLE_MORE_ALIAS_COMMAND;
				break;
			default:
				command = null;
		}
		
		if(command != null) {
			int counter = 0;
			for(String parameter : arguments) {
				command = command.replaceAll("\\{" + counter++ + "\\}", parameter);
			}
			
			this.parseAndExecute(command, true);
			
			return true;
		}
		
		return false;
	}
	
	abstract protected boolean doParseAndExecuteAliases(String alias, String[] arguments) throws Exception;
	
	final protected String notifyPropertyChanged(String property, String value) throws ConsoleCommandException {
		try {
			switch(property) {
				case VERBOSE_PROPERTY:
					break;
				case INTERACTIVE_PROPERTY:
					this._emitter.setInteractive(Boolean.valueOf(value));
				case UPDATER_PROPERTY:
					break;
				default: 
					break;
			}
			
			return this.doNotifyPropertyChanged(property, value);
		} catch(Throwable t) {
			throw new ConsoleCommandException(t.getMessage(), t);
		}
	}
	
	abstract protected String doNotifyPropertyChanged(String property, String value) throws ConsoleCommandException;

	protected OPERATIONS_PROVIDER getOperationsProvider() {
		return this._provider;
	}
	
	@SuppressWarnings("unchecked")
	protected Class<DATA> getTarget() {
		Class<?>[] typeArgs = TypeResolver.resolveRawArguments(AbstractConsole.class, this.getClass());
		
		return (Class<DATA>)typeArgs[0];
	}
	
	/** Taken from: http://svn.apache.org/repos/asf/ant/core/trunk/src/main/org/apache/tools/ant/types/Commandline.java */
	private String[] parseLine(String line) throws ConsoleException {
		if (line == null || line.length() == 0) {
			return new String[0];
		}
		
		final int normal = 0;
		final int inQuote = 1;
		final int inDoubleQuote = 2;
		
		int state = normal;
		
		final StringTokenizer tok = new StringTokenizer(line, "\"\' ", true);
		final ArrayList<String> result = new ArrayList<String>();
		final StringBuilder current = new StringBuilder();
		
		boolean lastTokenHasBeenQuoted = false;

		while (tok.hasMoreTokens()) {
			String nextTok = tok.nextToken();
			
			switch (state) {
				case inQuote:
					if ("\'".equals(nextTok)) {
						lastTokenHasBeenQuoted = true;
						state = normal;
					} else {
						current.append(nextTok);
					}
					break;
				case inDoubleQuote:
					if ("\"".equals(nextTok)) {
						lastTokenHasBeenQuoted = true;
						state = normal;
					} else {
						current.append(nextTok);
					}
					break;
				default:
					if ("\'".equals(nextTok)) {
						state = inQuote;
					} else if ("\"".equals(nextTok)) {
						state = inDoubleQuote;
					} else if (" ".equals(nextTok)) {
						if (lastTokenHasBeenQuoted || current.length() != 0) {
							result.add(current.toString());
							current.setLength(0);
						}
					} else {
						current.append(nextTok);
					}
					lastTokenHasBeenQuoted = false;
					break;
				}
		}
		
		if (lastTokenHasBeenQuoted || current.length() != 0) {
			result.add(current.toString());
		}
		
		if (state == inQuote || state == inDoubleQuote) {
			throw new ConsoleException("Unbalanced quotes in: " + line);
		}
		
		return result.toArray(new String[result.size()]);
	}
	
	private String getCommand(String[] lineTokens) throws CommandLineParsingException {
		if(lineTokens == null || lineTokens.length == 0 || lineTokens[0] == null)
			throw new CommandLineParsingException("Unable to parse command line (tokens: " + lineTokens + ")");
		
		return lineTokens[0];
	}

	private String[] getParameters(String[] lineTokens) throws CommandLineParsingException {
		if(lineTokens == null || lineTokens.length == 0 || lineTokens[0] == null)
			throw new CommandLineParsingException("Unable to parse command line (tokens: " + lineTokens + ")");
		
		if(lineTokens.length == 1)
			return new String[0];
		
		return Arrays.copyOfRange(lineTokens, 1, lineTokens.length);
	}
	
	private void parseAndExecute(String line) throws Exception {
		this.parseAndExecute(line, false);
	}
	
	final protected void parseAndExecute(String line, boolean reExecute) throws Exception {
		line = StringsHelper.trim(line);
		
		if(line == null)
			return;
		
		String[] tokens = this.parseLine(line);
		
		String command = this.getCommand(tokens);
		String[] arguments = this.getParameters(tokens);
		
		CommonConsoleCommand commonConsoleCommand;
		ConsoleCommand<DATA, HIERARCHY, OPERATIONS_PROVIDER> consoleCommand;
		
		this._emitter.nl();
		
		command = command == null ? null : command.trim(); 

		if(command != null) {
			try {
				if(command.startsWith("#"))
					this._emitter.info(" -- " + line);
				else if(HELP_COMMAND.equals(command) || HELP_COMMAND_SHORT.equals(command)) {
					this.help(arguments);
				} else if (HISTORY_COMMAND.equals(command) || HISTORY_COMMAND_SHORT.equals(command)) {
					this.history(arguments);
				} else if (LOAD_COMMAND.equals(command) || LOAD_COMMAND_SHORT.equals(command)) {
					this.load(arguments);
				} else if (SAVE_COMMAND.equals(command) || SAVE_COMMAND_SHORT.equals(command)) {
					this.save(arguments, new Date());
				} else if ( this._emitter.isMoreAvailable() && (MORE_COMMAND.equals(command) || MORE_COMMAND_SHORT.equals(command)) ) {
					this.more(arguments);
				} else if ( this._emitter.isClearAvailable() && (CLEAR_COMMAND.equals(command) || CLEAR_COMMAND_SHORT.equals(command)) ) {
					this.clear(arguments);
				} else if (SHOW_PROPERTIES_COMMAND.equals(command) || SHOW_PROPERTIES_COMMAND_SHORT.equals(command)) {
					this.showProperties(arguments);
				} else if (SET_PROPERTIES_COMMAND.equals(command) || SET_PROPERTIES_COMMAND_SHORT.equals(command)) {
					this.setProperty(arguments);
				} else if (QUIT_COMMAND.equals(command) || QUIT_COMMAND_SHORT.equals(command)) {
					this.quit(arguments);
				} else if(this.parseAndExecuteCustomCommands(command, arguments)) {
					//
				} else if(this.parseAndExecuteAliases(command, arguments)) {
					//
				} else {
					if((commonConsoleCommand = this._commonCommandsMap.get(command)) != null) {
						commonConsoleCommand.execute(this._properties, arguments, this._provider, this._emitter);
					} else {
						if((consoleCommand = this._commandsMap.get(command)) != null) {
							consoleCommand.execute(this._properties, arguments, this._provider, this._emitter);
						} else {
							this._emitter.error("Unknown command '" + command + "'");
						}
					}
				}
			
				if(!reExecute)
					this._emitter.nl();
			} finally {
				this._history.add(line);
			}
		}
	}
	
	private void help(String[] arguments) throws ConsoleException, ConsoleCommandException {
		ArgumentsChecker.doesNotRequireArguments(HELP_COMMAND, arguments);
		
		this._emitter.underline("Available commands:");
		this._emitter.nl();
		
		String description; 
		String help;
		
		CommonConsoleCommand commonConsoleCommand;
		ConsoleCommand<DATA, HIERARCHY, OPERATIONS_PROVIDER> consoleCommand;
		
		String[] parts;
		
		for(String command : new TreeSet<String>(this._commonCommandsMap.keySet())) {
			commonConsoleCommand = this._commonCommandsMap.get(command);
				
			description = commonConsoleCommand.getFullCommandDescription();
			
			if(description != null) {
				description = description.replaceAll("\\r|\\n", " ");
				description = description.replaceAll("^\\s+|\\s+$", "");
				
				if("".equals(description))
					description = null;
			}
			
			parts = description.split("§", -1);
			
			this._emitter.emit(DEFAULT_COMMAND_COLOR, " - " + parts[0]);
			
			if(parts.length > 1)
				for(int p=1; p<parts.length; p++) {
					if(parts[p] != null && !"".equals(parts[p].trim()))
						this._emitter.emit(DEFAULT_COMMAND_DESC_COLOR, "   " + parts[p]);
				}
			
			if(commonConsoleCommand.hasOptions()) {
				this._emitter.nl();
				this._emitter.emit(DEFAULT_COMMAND_DESC_COLOR, "      Where " + DEFAULT_COMMAND_OPTS_COLOR + "[options]" + DEFAULT_INFO_COLOR + " are:");
				
				help = commonConsoleCommand.getHelp(this._emitter);
				
				for(String part : help.split("\\n|\\r")) {
					part = part == null ? null : part.trim();
					
					if(part != null && !"".equals(part))
						this._emitter.emit(DEFAULT_COMMAND_DESC_COLOR, "       " + part);
				}
			}
			
			this._emitter.nl();
		}
		
		for(String command : new TreeSet<String>(this._commandsMap.keySet())) {
			consoleCommand = this._commandsMap.get(command);
				
			description = consoleCommand.getFullCommandDescription();
			
			if(description != null) {
				description = description.replaceAll("\\r|\\n", " ");
				description = description.replaceAll("^\\s+|\\s+$", "");
				
				if("".equals(description))
					description = null;
			}
			
			parts = description.split("§", -1);
			
			this._emitter.emit(DEFAULT_COMMAND_COLOR, " - " + parts[0]);
			
			if(parts.length > 1)
				for(int p=1; p<parts.length; p++) {
					if(parts[p] != null && !"".equals(parts[p].trim()))
						this._emitter.emit(DEFAULT_COMMAND_DESC_COLOR, "   " + parts[p]);
				}
			
			if(consoleCommand.hasOptions()) {
				this._emitter.nl();
				this._emitter.emit(DEFAULT_COMMAND_DESC_COLOR, "      Where " + DEFAULT_COMMAND_OPTS_COLOR + "[options]" + DEFAULT_INFO_COLOR + " are:");
				
				help = consoleCommand.getHelp(this._emitter);
				
				for(String part : help.split("\\n|\\r")) {
					part = part == null ? null : part.trim();
					
					if(part != null && !"".equals(part))
						this._emitter.emit(DEFAULT_COMMAND_DESC_COLOR, "       " + part);
				}
			}
			
			this._emitter.nl();
		}
		
		this._emitter.emit(DEFAULT_COMMAND_COLOR, " - " + SHOW_PROPERTIES_COMMAND + DEFAULT_COMMAND_DESC_COLOR + " or " + DEFAULT_COMMAND_COLOR + SHOW_PROPERTIES_COMMAND_SHORT);
		this._emitter.emit(DEFAULT_COMMAND_DESC_COLOR, "   => Show the currently set configuration properties.");
		this._emitter.nl();
		this._emitter.emit(DEFAULT_COMMAND_COLOR, " - " + SET_PROPERTIES_COMMAND + DEFAULT_COMMAND_DESC_COLOR + " or " + DEFAULT_COMMAND_COLOR + SET_PROPERTIES_COMMAND_SHORT + DEFAULT_COMMAND_ARGS_COLOR + " <propertyName> <propertyValue>");
		this._emitter.emit(DEFAULT_COMMAND_DESC_COLOR, "   => Set (or update) the <propertyName> configuration property to <propertyValue>.");
		this._emitter.nl();
		this._emitter.emit(DEFAULT_COMMAND_COLOR, " - " + HISTORY_COMMAND + DEFAULT_COMMAND_ARGS_COLOR + DEFAULT_COMMAND_DESC_COLOR + " or " + DEFAULT_COMMAND_COLOR + HISTORY_COMMAND_SHORT + DEFAULT_COMMAND_ARGS_COLOR + " [ clear|<commandNum> ]");
		this._emitter.emit(DEFAULT_COMMAND_DESC_COLOR, "   => Print the command history for this console session (if no argument is provided).");
		this._emitter.emit(DEFAULT_COMMAND_DESC_COLOR, "      Clear the command history for this console session (if the 'clear' argument is provided).");
		this._emitter.emit(DEFAULT_COMMAND_DESC_COLOR, "      Re-execute the command at <commandNum> position (when none of the previous arguments is provided).");
		this._emitter.nl();
		this._emitter.emit(DEFAULT_COMMAND_COLOR, " - " + SAVE_COMMAND + DEFAULT_COMMAND_ARGS_COLOR + DEFAULT_COMMAND_DESC_COLOR + " or " + DEFAULT_COMMAND_COLOR + SAVE_COMMAND_SHORT + DEFAULT_COMMAND_ARGS_COLOR + " [ <filename> ]");
		this._emitter.emit(DEFAULT_COMMAND_DESC_COLOR, "   => Save the current command history for this console session in a file under the './session' folder (if no argument is provided).");
		this._emitter.emit(DEFAULT_COMMAND_DESC_COLOR, "      If an argument is provided, it is interpreted as the file path under which the current command history will be saved.");
		this._emitter.nl();
		this._emitter.emit(DEFAULT_COMMAND_COLOR, " - " + LOAD_COMMAND + DEFAULT_COMMAND_ARGS_COLOR + DEFAULT_COMMAND_DESC_COLOR + " or " + DEFAULT_COMMAND_COLOR + LOAD_COMMAND_SHORT + DEFAULT_COMMAND_ARGS_COLOR + " [ <filename> ]");
		this._emitter.emit(DEFAULT_COMMAND_DESC_COLOR, "   => Load and execute a script file. If <filename> is not provided, a dialog window will open to let users choose the file to execute.");
		this._emitter.nl();
		
		if(this._emitter.isMoreAvailable()) {
			this._emitter.emit(DEFAULT_COMMAND_COLOR, " - " + MORE_COMMAND + DEFAULT_COMMAND_DESC_COLOR + " or " + DEFAULT_COMMAND_COLOR + MORE_COMMAND_SHORT + DEFAULT_COMMAND_ARGS_COLOR + " [ on|off ]");
			this._emitter.emit(DEFAULT_COMMAND_DESC_COLOR, "   => Show the current abilitation status of the console '" + MORE_COMMAND + "' mode (if no arguments is provided).");
			this._emitter.emit(DEFAULT_COMMAND_DESC_COLOR, "      Set the console '" + MORE_COMMAND + "' mode (enabled / disabled).");
			this._emitter.nl();
		}
		
		if(this._emitter.isClearAvailable()) {
			this._emitter.emit(DEFAULT_COMMAND_COLOR, " - " + CLEAR_COMMAND + DEFAULT_COMMAND_DESC_COLOR + " or " + DEFAULT_COMMAND_COLOR + CLEAR_COMMAND_SHORT);
			this._emitter.emit(DEFAULT_COMMAND_DESC_COLOR, "   => Clear the console.");
			this._emitter.nl();
		}
		
		this._emitter.emit(DEFAULT_COMMAND_COLOR, " - " + HELP_COMMAND + DEFAULT_COMMAND_DESC_COLOR + " or " + DEFAULT_COMMAND_COLOR + HELP_COMMAND_SHORT);
		this._emitter.emit(DEFAULT_COMMAND_DESC_COLOR, "   => Print this help.");
		this._emitter.nl();
		this._emitter.emit(DEFAULT_COMMAND_COLOR, " - " + QUIT_COMMAND + DEFAULT_COMMAND_DESC_COLOR + " or " + DEFAULT_COMMAND_COLOR + QUIT_COMMAND_SHORT); 
		this._emitter.emit(DEFAULT_COMMAND_DESC_COLOR, "   => Exit the console and save current history in a script file under ./sessions.");
		
		this.showAliases();
	}
	
	private void quit(String[] arguments) throws ConsoleException, ConsoleCommandException, IOException {
		ArgumentsChecker.doesNotRequireArguments(QUIT_COMMAND, arguments);
		
		if(this._emitter.confirm("Are you sure you want to quit")) {
			if(this.isInteractive()) this._emitter.nl();
			
			if(!this._history.isEmpty()) {
				if(this._emitter.confirm("Do you want to save current session history as a script file")) {
					if(this.isInteractive()) this._emitter.nl();
					
					this.save(new String[0], null);
				} else {
					if(this.isInteractive()) this._emitter.nl();
					
					this._emitter.warn(" -> Current session history was NOT been saved as a script file");
					this._emitter.nl();
				}
			}
			
			System.exit(0);
		} else {
			this._emitter.nl();
			this._emitter.warn(" -> OK, glad to see that you changed idea!");
		}
	}
	
	private void history(String[] arguments) throws Exception {
		ArgumentsChecker.requiresAtMost(HISTORY_COMMAND, arguments, 1);
		
		if(arguments.length == 0) {
			this._emitter.underline("Command history (since this console was started):", '-');
			this._emitter.nl();
			
			int counter = 1;
			for(String command : this._history)
				this._emitter.info(counter++ + ") " + command);
		} else {
			if("clear".equals(arguments[0])) {
				this._history.clear();
				
				this._emitter.info(" -> The command history has been cleared");
			} else {
				try {
					Integer num = Integer.parseInt(arguments[0]); 
					
					if(num > this._history.size() || num <= 0) {
						throw new ConsoleCommandException(arguments[0] + " exceeds the boundaries of the current command history");
					} 
					
					String line = this._history.get(num - 1);
					this._emitter.emit(this._emitter.getPrompt() + DEFAULT_INFO_COLOR + "[ Re-execute ] " + RESET_COLOR + line);
					
					this.parseAndExecute(line, true);
				} catch(NumberFormatException NFe) {
					throw new ConsoleCommandException(arguments[0] + " is not a valid command history number");
				}
			}
		}
	}
	
	private void save(String[] arguments, Date date) throws ConsoleCommandException, IOException {
		ArgumentsChecker.requiresAtMost(SAVE_COMMAND, arguments, 1);
		
		String sessionID = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(date != null ? date : this.LAUNCH_TIME);
		
		String sessionFileName = arguments.length == 1 ? arguments[0] : "./sessions/" + this.getTarget().getSimpleName() + "_session_" + sessionID + ".vrmfscript";

		File sessionFile = new File(sessionFileName);
		
		this._emitter.info(" -> Storing current session history (" + this._history.size() + " entries)...");
		
		FileOutputStream fos = new FileOutputStream(sessionFile);
		OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
		
		for(String in : this._history)
			osw.write(in + "\n");
		
		osw.flush();
		fos.flush();
		
		osw.close();
		fos.close();
		
		this._emitter.nl();
		
		this._emitter.warn(" -> Current session history (" + this._history.size() + " entries) has been stored into " + ( arguments.length == 0 ? "./sessions/" + sessionFile.getName() : sessionFileName ));
	}
	
	private void load(String[] arguments) throws ConsoleCommandException, IOException {
		ArgumentsChecker.requiresAtMost(LOAD_COMMAND, arguments, 1);

		String filename = arguments.length == 1 ? arguments[0] : null;

		InputStream in = this._emitter.getIn();
		boolean scripted = this._isScripted;
		
		try {
			final AbstractConsole<DATA, HIERARCHY, OPERATIONS_PROVIDER, COMMAND> $console = this;
			
			if(filename == null) {
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new File("./sessions"));
				chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
				
				FileFilter filter = new FileFilter() {
					@Override
					public String getDescription() {
						return "VRMF " + $console.getTarget().getSimpleName() + " management script files";
					}
					
					@Override
					public boolean accept(File f) {
						return f != null && (f.isDirectory() || ( f.getName().startsWith($console.getTarget().getSimpleName()) && f.getName().endsWith(".vrmfscript") ));
					}
				}; 
				
				chooser.addChoosableFileFilter(filter);
				chooser.setFileFilter(filter);
				
				int result = chooser.showOpenDialog(null);
				
				if(result == JFileChooser.APPROVE_OPTION) {
					filename = chooser.getSelectedFile().getAbsolutePath();
				}
				
				if(filename == null) {
					this._emitter.warn(" -> Operation aborted!");
					
					return;
				}
			}
			
			File script = new File(filename);
			
			if(!script.exists() || !script.isFile() || !script.canRead()) {
				this._emitter.error(" -> " + filename + " does not exist or is not a proper script file or cannot be read");
				return;
			}
			
			this._emitter.info(" -> Reading and executing script file " + filename);
			this._emitter.nl();
			
			this._isScripted = true;
	
			this._emitter.setIn(new FileInputStream(filename));
				
			this.parse();
		} finally {
			this._emitter.setIn(in);
			
			this._isScripted = scripted;
		}
	}
	
	private void more(String[] arguments) throws Exception {
		ArgumentsChecker.requiresAtMost(MORE_COMMAND, arguments, 1);
		
		if(arguments.length == 0) {
			this._emitter.info(" -> The console '" + MORE_COMMAND + "' mode is " + bold(Color.GREEN) + ( this._emitter.getMore() ? "enabled" : "disabled" ).toUpperCase());
		} else {
			boolean enable = "on".equals(arguments[0]);
			
			this._emitter.info(" -> " + bold(Color.GREEN) + ( enable ? "Enabling" : "Disabling" ) + DEFAULT_INFO_COLOR + " the console '" + MORE_COMMAND + "' mode...");
			
			this._emitter.setMore(enable);
		}
	}
	
	private void clear(String[] arguments) throws Exception {
		ArgumentsChecker.doesNotRequireArguments(CLEAR_COMMAND, arguments);

		this._emitter.clear();
	}
	
	private void showProperties(String[] arguments) throws ConsoleException, ConsoleCommandException {
		ArgumentsChecker.doesNotRequireArguments(SHOW_PROPERTIES_COMMAND, arguments);
		
		this._emitter.underline("Current properties:", '-');
		this._emitter.nl();

		Set<String> keys = new TreeSet<String>();
		
		for(Object key : this._properties.keySet()) {
			keys.add(key.toString());
		}
		
		for(String key : keys)
			this._emitter.info(color(Color.GREEN) + key + DEFAULT_INFO_COLOR + " := " + bold(Color.GREEN) + this._properties.getProperty(key.toString()));
	}
	
	private void setProperty(String[] arguments) throws ConsoleException, ConsoleCommandException {
		ArgumentsChecker.requiresExactly(SET_PROPERTIES_COMMAND, arguments, 2);

		String property = arguments[0], value = arguments[1];
		
		value = this.notifyPropertyChanged(property, value);
		
		this._properties.put(property, value);

		try {
			value = this._provider.notifyPropertyChanged(property, value);
			
			this._emitter.info(" -> Property '" + bold(Color.GREEN) + property + DEFAULT_INFO_COLOR + "' has been set to '" + bold(Color.GREEN) + value + DEFAULT_INFO_COLOR + "'");
		} catch(DataManagementOperationPropertyException DMOPe) {
			throw new ConsoleCommandException(DMOPe.getMessage());
		}
	}
	
	@SuppressWarnings("static-access")
	public Options getOptions() {
		Options opts = new Options();
		opts.addOption(OptionBuilder.hasArg().withArgName("script file").withDescription("Read the console commands from a file (the console will be in non-interactive mode)").withLongOpt(SCRIPT_OPT_LONG).create(SCRIPT_OPT));
		opts.addOption(OptionBuilder.withDescription("When a script file is provided (with the \"-" + SCRIPT_OPT + "\" or \"--" + SCRIPT_OPT_LONG + "\" option) halts the script execution as soon as a blocking error is detected").withLongOpt(HALT_ON_ERROR_OPT_LONG).create(HALT_ON_ERROR_OPT));
		opts.addOption(OptionBuilder.withDescription("Enable color console with advanced capabilities").withLongOpt(COLOR_OPT_LONG).create(COLOR_OPT));

		return opts;
	}
	
	final public void launch(String[] args) throws IOException {
		InputStream in = System.in;
		PrintStream out = System.out;

		String cmdLine = this.getClass().getSimpleName() + " ";
		
		for(String arg : args)
			cmdLine += arg + " ";
		
		cmdLine = cmdLine.replaceAll("\\s$", "");

		try {
			Options opts = this.getOptions();
			
			CommandLine cmd = new PosixParser().parse(opts, args);
			
			this._isScripted = cmd.hasOption(SCRIPT_OPT);
			this._haltOnError = cmd.hasOption(HALT_ON_ERROR_OPT);
			
			if(!this._isScripted && this._haltOnError)
				throw new ParseException("The \"-" + HALT_ON_ERROR_OPT + "\" (\"--" + HALT_ON_ERROR_OPT_LONG + "\") option can be used only in combination with the \"-" + SCRIPT_OPT + "\" (\"--" + SCRIPT_OPT_LONG + "\") option");
			
			if(this._isScripted) {
				File script = new File(cmd.getOptionValue(SCRIPT_OPT));
				
				if(!script.exists() || !script.canRead() || !script.isFile())
					throw new FileNotFoundException("The provided script file (" + script.getAbsolutePath() + ") doesn't exist or cannot be accessed");
				
				in = new FileInputStream(script);
			}
			
			AbstractEmitter emitter = cmd.hasOption(COLOR_OPT) ? new JLineConsoleEmitter() : new StandardConsoleEmitter();
			
			emitter.setCommandNames(this.getCommandNames(emitter));
			emitter.setPrompt(PROMPT.replace("{}", this._target.getSimpleName()));
			emitter.setIn(in);
			emitter.setOut(out);
			
			try {
				JFrame.setDefaultLookAndFeelDecorated(true);
				JDialog.setDefaultLookAndFeelDecorated(true);

				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch (Throwable t) {
				//
			}
			
			this.launch(emitter);
		} catch(ParseException Pe) {
			System.err.println("Wrong command line arguments: " + cmdLine + ". " + Pe.getMessage());
		} catch(FileNotFoundException FNFe) {
			System.err.println("Invalid command line arguments: " + cmdLine + ". " + FNFe.getMessage());
		} finally {
			if(in != System.in) {
				try {
					in.close();
				} catch(Throwable t) {
					//Suffocate
				}
			}
		}
	}
	
	private void launch(AbstractEmitter emitter) throws IOException {
		this._properties.putAll(this.getDefaultProperties());
		this._properties.putAll(this._provider.properties());

		this._emitter = emitter;
		
		this._emitter.banner("VRMF Management Console for " + this.getTarget().getSimpleName());
		this._emitter.nl();
		this._emitter.info(" -> Using " + bold(Color.YELLOW) + this._provider.getClass().getSimpleName() + DEFAULT_INFO_COLOR + " as operations provider.");
		this._emitter.info(" -> < " + this._provider.description() + " >");
		this._emitter.nl();
		this._emitter.underline("Type '" + HELP_COMMAND + "' or '" + HELP_COMMAND_SHORT + "' to get a list of the available commands", '-');
		this._emitter.nl();
		
		this.parse();
	}
	
	private void parse() throws IOException {
		String line;
		
		try {
			while((line = this._emitter.prompt()) != null) {
				if(this._emitter.isScripted()) {
					this._emitter.emit(line);
				}
				
				try {
					this.parseAndExecute(line);
				} catch(ConsoleException|ConsoleCommandException Ce) {
					this._emitter.error(Ce.getMessage());
					this._emitter.nl();
					
					if(this._haltOnError) {
						this._emitter.banner("Halting execution at first error");
						this._emitter.nl();
						break;
					}
				} catch(Throwable t) {
					this._emitter.fatal(t.getMessage(), t);
					
					if(this._haltOnError) {
						this._emitter.banner("Halting execution at first error");
						this._emitter.nl();
						break;
					}
				}
			}
		} catch(IOException IOe) {
			IOe.printStackTrace();
			
			return;
		}		
	}
}