/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.common.helpers;

import java.io.BufferedReader;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;

import org.fao.fi.vrmf.data.management.console.common.exceptions.ConsoleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Fiorellato
 *
 */
abstract public class AbstractEmitter {
	protected Logger _log = LoggerFactory.getLogger(AbstractEmitter.class);
	
	protected int _numLines = 0;

	protected InputStream _in;
	protected PrintStream _out;
	
	protected BufferedReader _reader;

	protected String _prompt;
	
	protected boolean _interactive = true;
	protected boolean _more = true;

	static public String DEFAULT_BANNER_COLOR	   =  bold(Color.GREEN);
	static public String DEFAULT_HIGHLIGHT_COLOR   = color(Color.GREEN);
	static public String DEFAULT_UNDERLINE_COLOR   =  bold(Color.BLUE);
	static public String DEFAULT_INFO_COLOR		   = color(Color.YELLOW);
	static public String DEFAULT_WARN_COLOR		   =  bold(Color.MAGENTA);
	
	static public String DEFAULT_COMMAND_COLOR	   =  bold(Color.YELLOW);
	static public String DEFAULT_COMMAND_DESC_COLOR= color(Color.YELLOW);
	static public String DEFAULT_COMMAND_ARGS_COLOR=  bold(Color.GREEN);
	static public String DEFAULT_COMMAND_ARGD_COLOR= color(Color.GRAY);
	static public String DEFAULT_COMMAND_OPTS_COLOR= color(Color.GREEN);
	
	static public String DEFAULT_PROMPT_COLOR	   =  bold(Color.GREEN);
	static public String DEFAULT_CONFIRM_COLOR	   =  bold(Color.CYAN);
	
	static public String DEFAULT_ERROR_COLOR	   =  bold(Color.RED);
	static public String DEFAULT_ERROR_TRACE_COLOR = color(Color.RED);
	
	static public String RESET_COLOR			   = color(Color.RESET);
	
	static public char BANNER_STROKE 	= '#';
	static public char HIGHLIGHT_STROKE = '*';
	static public char UNDERLINE_STROKE = '-';
	
	static public enum Color {
		RESET(0),
		BLACK(30),
		RED(31),
		GREEN(32),
		YELLOW(33),
		BLUE(34),
		MAGENTA(35),
		CYAN(36),
		GRAY(37);
		
		private int _code;
		
		private Color(int code) {
			this._code = code;
		}
		
		public int getCode() {
			return this._code;
		}
	}
	
	public AbstractEmitter() {
		super();
	}
	
	public InputStream getIn() {
		return this._in;
	}

	public void setIn(InputStream in) {
		this._in = in;
		
		this._reader = new BufferedReader(new InputStreamReader(in)); 
	}

	public PrintStream getOut() {
		return this._out;
	}

	public void setOut(PrintStream out) {
		this._out = out;
	}

	abstract public int getConsoleWidth();
		
	static public String color(Color color) {
		return color(color, false);
	}
	
	static public String bold(Color color) {
		return color(color, true);
	}
	
	static public String reset() {
		return RESET_COLOR;
	}
	
	static public String color(Color color, boolean bold) {
		return color(color.getCode(), bold);
	}
	
	static public String color(int colorCode, boolean bold) {
		boolean isReset = Color.RESET.getCode() == colorCode;
		
		if(isReset)
			return ansi(Color.RESET.getCode());
		
		return ansi(Color.RESET.getCode()) + 
			   ansi(colorCode + ( bold ? ";1" : "" ) + "m");
	}
	
	static public String ansi(int code) {
		return ansi(code + "m");
	}
	
	static public String ansi(String code) {
		return "\u001B[" + code;
	}
	
	final public boolean isScripted() {
		return !this.isStdIn(this._in);
	}
	
	final public boolean isStdIn(InputStream in) {
		if(in == null)
			return false;
		
		if(in == System.in)
			return true;
		
		try {
			return ( in instanceof FileInputStream && ((FileInputStream)in).getFD() == FileDescriptor.in );
		} catch(IOException IOe) {
			return false;
		}
	}
	
	final public boolean isInteractive() {
		return this._interactive;
	}
	
	final public void setInteractive(boolean interactive) {
		this._interactive = interactive;
	}
	
	abstract public void setCommandNames(String[] commandNames);
	
	abstract public boolean isClearAvailable();
	abstract public void clear();
	
	public void setPrompt(String prompt) {
		this._prompt = prompt;
	}
	
	final public String getPrompt() {
		return this._prompt;
	}
	
	abstract public boolean isMoreAvailable();
	
	final public boolean getMore() {
		return this._more;
	}

	public void setMore(boolean more) {
		this._more = more;
	}

	final public String prompt() throws IOException {
		return this.readLine(this._prompt);
	}
	
	final public String readLine() throws IOException {
		return this.readLine("");
	}
	
	final public String readLine(String prompt) throws IOException {
		this._numLines = 0;
		
		return this.doReadLine(prompt);
	}
	
	abstract protected String doReadLine(String prompt) throws IOException;
	
	
	final public boolean confirm(String question) throws ConsoleException {
		if(this.isScripted() || !this.isInteractive())
			return true;
		
		try {
			question = DEFAULT_CONFIRM_COLOR + question + " [ y / N ] ? " + RESET_COLOR;

			return "y".equalsIgnoreCase(this.readLine(question));
		} catch(IOException IOe) {
			throw new ConsoleException("Unable to read from input source: " + IOe.getMessage(), IOe);
		}
	}

	final public String strokeFor(String text, char stroke) {
		if(text == null)
			return "";
		
		String result = "";
		
		while(result.length() < text.length())
			result += stroke;
		
		return result;
	}
	
	final public void nl() {
		this.emit("");
	}
	
	final public void emit(String text) {
		this.emit(null, text);
	}
	
	abstract public void emit(String color, String text);
	
	final public void banner(String text) {
		this.banner(DEFAULT_BANNER_COLOR, text);
	}
	
	final public void banner(String text, char stroke) {
		this.banner(DEFAULT_BANNER_COLOR, text, stroke);
	}
	
	final public void banner(String color, String text) {
		this.banner(color, text, BANNER_STROKE);
	}
	
	final public void banner(String color, String text, char stroke) {
		text = String.valueOf(stroke) + String.valueOf(stroke) + " " + ( text == null ? "" : text ) + " " + String.valueOf(stroke) + String.valueOf(stroke);
		
		this.highlight(color, text, stroke);
	}
	
	final public void highlight(String text) {
		this.highlight(DEFAULT_HIGHLIGHT_COLOR, text);
	}
	
	final public void highlight(String text, char stroke) {
		this.highlight(DEFAULT_HIGHLIGHT_COLOR, text, stroke);
	}
	
	final public void highlight(String color, String text) {
		this.highlight(color, text, HIGHLIGHT_STROKE);
	}
	
	final public void highlight(String color, String text, char stroke) {
		this.emit(color, this.strokeFor(text, stroke));
		this.emit(color, text);
		this.emit(color, this.strokeFor(text, stroke));
	}
	
	final public void underline(String text) {
		this.underline(DEFAULT_UNDERLINE_COLOR, text);
	}
	
	final public void underline(String text, char stroke) {
		this.underline(DEFAULT_UNDERLINE_COLOR, text, stroke);
	}
	
	final public void underline(String color, String text) {
		this.underline(color, text, UNDERLINE_STROKE);
	}
	
	final public void underline(String color, String text, char stroke) {
		this.emit(color, text);
		this.emit(color, this.strokeFor(text, stroke));
	}

	final public void info(String text) {
		this.info(DEFAULT_INFO_COLOR, text);
	}

	final public void warn(String text) {
		this.warn(DEFAULT_WARN_COLOR, text);
	}
	
	final public void warn(String color, String text) {
		this.emit(color, text);
	}
	
	final public void info(String color, String text) {
		this.emit(color, text);
	}
	
	final public void error(String text) {
		this._log.error("[ ERROR ] : {}", text);

		this.error("ERROR", text);
	}
	
	final public void error(String prefix, String text) {
		text = "[ " + prefix + " ] : " + text;
		this.emit(DEFAULT_ERROR_COLOR + text);
	}
	
	final public void fatal(String text, Throwable t) {
		this._log.error("[ FATAL ] : {}", text);
		
		this.error("FATAL", text);
		
		if(t != null) {
			StringWriter sw = new StringWriter();
			
			PrintWriter pw = new PrintWriter(sw);
			
			t.printStackTrace(pw);
			
			this.nl();
			this.emit(DEFAULT_ERROR_TRACE_COLOR, "Stack trace:");
			this.emit(DEFAULT_ERROR_TRACE_COLOR, sw.toString());
			this.nl();
			
			try {
				sw.close();
				pw.close();
			} catch(Throwable tt) {
				//Suffocate
			}
			
			try {
				sw.close();
				pw.close();
			} catch(Throwable tt) {
				//Suffocate
			}
		}
	}
	
	final public void abort() {
		this.nl();
		
		this.warn(" -> Operation aborted!");
	}
}