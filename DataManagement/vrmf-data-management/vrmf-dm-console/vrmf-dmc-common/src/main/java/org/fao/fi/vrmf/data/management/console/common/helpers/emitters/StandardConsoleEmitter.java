/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.common.helpers.emitters;

import java.io.IOException;

import org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter;

/**
 * @author Fiorellato
 *
 */
public class StandardConsoleEmitter extends AbstractEmitter {
	public StandardConsoleEmitter() {
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter#getConsoleWidth()
	 */
	@Override
	public int getConsoleWidth() {
		return 192;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter#setCommandNames(java.lang.String[])
	 */
	@Override
	public void setCommandNames(String[] commandNames) {
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter#isClearAvailable()
	 */
	@Override
	public boolean isClearAvailable() {
		return false;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter#clear()
	 */
	@Override
	public void clear() {
		this.warn(" -> This type of console (" + this.getClass().getSimpleName() + ") does not allow clearing the terminal");
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter#isMoreAvailable()
	 */
	@Override
	public boolean isMoreAvailable() {
		return false;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter#setMore(boolean)
	 */
	@Override
	public void setMore(boolean more) {
		super.setMore(more);
		
		this.nl();
		this.warn(" -> This type of console (" + this.getClass().getSimpleName() + ") does not allow pausing the output when the screen is full. This setting will have no effect.");
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter#doReadLine(java.lang.String)
	 */
	@Override
	protected String doReadLine(String prompt) throws IOException {
		this._out.print(this.sanitize(prompt));
		
		return this._reader.readLine();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter#emit(java.lang.String, java.lang.String)
	 */
	@Override
	public void emit(String color, String text) {
		this._out.println(this.sanitize(text));
	}
	
	private String sanitize(String text) {
		if(text == null)
			return "";
		
		return text.replaceAll("\\u001B\\[[0-9\\;]+m", "");
	}
}