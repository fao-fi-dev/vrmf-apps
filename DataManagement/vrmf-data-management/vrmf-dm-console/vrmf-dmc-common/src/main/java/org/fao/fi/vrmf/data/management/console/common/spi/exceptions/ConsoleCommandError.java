/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.common.spi.exceptions;

/**
 * @author Fiorellato
 *
 */
public class ConsoleCommandError extends Error {
	private static final long serialVersionUID = 8416459407602556719L;

	/**
	 * 
	 */
	public ConsoleCommandError() {
	}

	/**
	 * @param message
	 */
	public ConsoleCommandError(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public ConsoleCommandError(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ConsoleCommandError(String message, Throwable cause) {
		super(message, cause);
	}
}
