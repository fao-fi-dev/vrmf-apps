/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.common.exceptions;

/**
 * @author Fiorellato
 *
 */
public class CommandLineParsingException extends ConsoleException {
	private static final long serialVersionUID = -5368340063921791156L;

	/**
	 * 
	 */
	public CommandLineParsingException() {
	}

	/**
	 * @param message
	 * @param cause
	 */
	public CommandLineParsingException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public CommandLineParsingException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public CommandLineParsingException(Throwable cause) {
		super(cause);
	}
}
