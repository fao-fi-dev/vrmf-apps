/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.common.helpers.emitters;

import java.io.IOException;
import java.io.PrintWriter;

import jline.console.ConsoleReader;
import jline.console.completer.StringsCompleter;

import org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter;

/**
 * @author Fiorellato
 *
 */
public final class JLineConsoleEmitter extends AbstractEmitter {
	private ConsoleReader _cReader;
	private PrintWriter _writer;

	public JLineConsoleEmitter() {
		try {
			this._cReader = new ConsoleReader();
			this._writer = new PrintWriter(this._cReader.getOutput());
		} catch(IOException IOe) {
			//SUFFOCATE
		}
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter#setCommandNames(java.lang.String[])
	 */
	@Override
	public void setCommandNames(String[] commandNames) {
		this._cReader.addCompleter(new StringsCompleter(commandNames));
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter#setPrompt(java.lang.String)
	 */
	@Override
	public void setPrompt(String prompt) {
		super.setPrompt(prompt);
		
		this._cReader.setPrompt(prompt);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter#getConsoleWidth()
	 */
	@Override
	public int getConsoleWidth() {
		return jline.TerminalFactory.get().getWidth();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter#isClearAvailable()
	 */
	@Override
	public boolean isClearAvailable() {
		return true;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter#clear()
	 */
	public void clear() {
		try {
			this._cReader.clearScreen();
		} catch(IOException IOe) {
			//SUFFOCATE
		}
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter#isMoreAvailable()
	 */
	@Override
	public boolean isMoreAvailable() {
		return true;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter#doReadLine(java.lang.String)
	 */
	@Override
	protected String doReadLine(String prompt) throws IOException {
		if(!this.isScripted())
			return this._cReader.readLine(prompt);
		else {
			this._writer.print(this.getPrompt());
			
			return this._reader.readLine();
		}
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter#emit(java.lang.String, java.lang.String)
	 */
	@Override
	public void emit(String color, String text) {
		try {
			this._writer.println((color == null ? "" : color) + text + RESET_COLOR);
			this._writer.flush();
		} catch(Throwable t) {
			t.printStackTrace();//SUFFOCATE
		}
		
		if(!this.isScripted() && this.isInteractive() && this._more) {
			int height = jline.TerminalFactory.get().getHeight();
			
			this._numLines++;

			if(this._numLines > height - 4) {
				this._numLines = 0;
				
				this.nl();
				
				try {
					this.readLine(DEFAULT_CONFIRM_COLOR + "[ PRESS ENTER TO CONTINUE ]" + RESET_COLOR);
					this.nl();
				} catch (IOException IOe) {
				}
			}
		}
	}
}