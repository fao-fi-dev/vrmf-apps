/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.common.spi;

import java.util.Properties;

import org.fao.fi.vrmf.data.management.common.spi.DataManagementOperationsProvider;
import org.fao.fi.vrmf.data.management.common.spi.ManagedData;
import org.fao.fi.vrmf.data.management.common.spi.ManagedDataHierarchy;
import org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter;
import org.fao.fi.vrmf.data.management.console.common.spi.exceptions.ConsoleCommandError;
import org.fao.fi.vrmf.data.management.console.common.spi.exceptions.ConsoleCommandException;

/**
 * @author Fiorellato
 *
 */
public interface ConsoleCommand<DATA extends ManagedData<DATA>, HIERARCHY extends ManagedDataHierarchy<DATA, HIERARCHY>, OPERATIONS_PROVIDER extends DataManagementOperationsProvider<DATA, HIERARCHY>> extends Command {
	void execute(Properties properties, String[] parameters, OPERATIONS_PROVIDER provider, AbstractEmitter emitter) throws ConsoleCommandException, ConsoleCommandError;
}
