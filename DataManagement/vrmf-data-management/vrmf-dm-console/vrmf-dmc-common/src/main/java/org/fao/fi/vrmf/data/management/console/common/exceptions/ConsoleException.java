/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.common.exceptions;

/**
 * @author Fiorellato
 *
 */
public class ConsoleException extends Exception {
	private static final long serialVersionUID = 6948619063116053060L;

	public ConsoleException() {
		super();
	}

	public ConsoleException(String message, Throwable cause) {
		super(message, cause);
	}

	public ConsoleException(String message) {
		super(message);
	}

	public ConsoleException(Throwable cause) {
		super(cause);
	}
}
