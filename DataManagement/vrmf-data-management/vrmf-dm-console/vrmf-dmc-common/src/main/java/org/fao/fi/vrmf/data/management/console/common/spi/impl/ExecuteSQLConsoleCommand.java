/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.common.spi.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Properties;

import javax.inject.Named;
import javax.inject.Singleton;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.fao.fi.vrmf.data.management.common.spi.DataManagementOperationsProvider;
import org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter;
import org.fao.fi.vrmf.data.management.console.common.helpers.ArgumentsChecker;
import org.fao.fi.vrmf.data.management.console.common.spi.CommonConsoleCommand;

import au.com.bytecode.opencsv.CSVWriter;

/**
 * @author Fiorellato
 *
 */
@Singleton @Named("common.console.command.execute.sql")
public class ExecuteSQLConsoleCommand implements CommonConsoleCommand {
	static final private String IN_FILE_OPT			 = "f";
	static final private String IN_FILE_OPT_LONG	 = "file";
	
	static final private String OUT_FILE_FOLDER = "./sqlResults";
		
	private Date _executionDate;
	
	@Override
	public String getCommandName() {
		return "sql";
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.vessels.commands.AbstractVesselConsoleCommand#getOptions()
	 */
	@SuppressWarnings("static-access")
	@Override
	public Options getOptions() {
		Options opts = new Options();
		opts.addOption(OptionBuilder.withArgName("file path").hasArg().withDescription("Read statements from a text file available at <file path>").withLongOpt(IN_FILE_OPT_LONG).create(IN_FILE_OPT));
		return opts;
	}
	
	@Override
	public String getCommandDescription() {
		return "Execute SQL statements over the current database content";
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.spi.ConsoleCommand#hasArguments()
	 */
	@Override
	public boolean hasArguments() {
		return true;
	}
		
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.spi.ConsoleCommand#getCommandArguments()
	 */
	@Override
	public String getCommandArguments() {
		return "<SQL statement>";
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.spi.impl.AbstractConsoleCommand#getCommandArgumentsDescriptions()
	 */
	@Override
	public String getCommandArgumentsDescriptions() {
		return "An SQL statement enclosed by double quotes";
	}
	
	private String readStatement(String file) throws IOException {
		try(InputStream is = new FileInputStream(file)) {
			byte[] buffer = new byte[8192];
			int len = -1;
			
			StringBuilder read = new StringBuilder();
			
			while((len = is.read(buffer)) != -1) {
				read.append(new String(buffer, 0, len, Charset.forName("UTF-8")));
			}
			
			return read.toString();
		}
	}
	
	private void writeReport(File file, String report) throws IOException {
		try(FileWriter fw = new FileWriter(file)) {
			
			fw.write(report);
		}
	}
	
	private File getOutFile() {
		return new File(OUT_FILE_FOLDER + "/sql_result_" + new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(this._executionDate) + ".csv");
	}
	
	private File getOutFileReport() {
		return new File(OUT_FILE_FOLDER + "/sql_result_" + new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(this._executionDate) + ".log");
	}
	
	private CSVWriter getWriter(File file) throws IOException {
		return new CSVWriter(new OutputStreamWriter(new FileOutputStream(file)), ',', '"');
	}

	@Override
	public void doExecute(Properties properties, CommandLine commandLine, DataManagementOperationsProvider<?, ?> provider, AbstractEmitter emitter) throws Exception {
		String[] parameters = commandLine.getArgs();
		
		this._executionDate = new Date();
		
		boolean specifiesFile = commandLine.hasOption(IN_FILE_OPT);

		if(specifiesFile) { 
			ArgumentsChecker.doesNotRequireArguments(this, parameters);
		} else {
			ArgumentsChecker.requiresExactly(this, parameters, 1);
		}
		
		boolean confirm = false;
		
		String file = specifiesFile ? commandLine.getOptionValue(IN_FILE_OPT) : null;
		String statement = specifiesFile ? readStatement(file) : parameters[0];
		
		if(specifiesFile) {
			emitter.banner("SQL to be executed (as reported in the specified file (" + file + "):");
			emitter.nl();
			emitter.warn(statement);
			emitter.nl();
		}
	
		if(confirm = emitter.confirm(
			specifiesFile ? 
				"This will execute the SQL statement in the specified file (" + file + "). Are you sure you want to continue" : 
				"This will execute the specified SQL statement on the current database. Are you sure you want to continue"
			)) {
			emitter.nl();
			
			try {
				Collection<String[]> results = provider.executeStatement(statement);

				String report = "Statement successfully executed. " + (( results == null || results.isEmpty() ) ? "No" : results.size()) + " rows returned";
				
				writeReport(getOutFileReport(), report);
				
				emitter.highlight(report);
				
				if(results != null && !results.isEmpty()) {
					CSVWriter writer = getWriter(getOutFile());

					StringBuilder rowData;
					
					boolean isHeader = true;
					
					int counter = 1;
					
					for(String[] row : results) {
						rowData = new StringBuilder();
						
						for(String in : row)
							rowData.append("[ ").append(in).append(" ] ");
						
						if(isHeader) {
							emitter.emit(AbstractEmitter.DEFAULT_BANNER_COLOR, "### HEADER ### - " + rowData.toString());
							isHeader = false;
						} else {
							emitter.emit(AbstractEmitter.DEFAULT_HIGHLIGHT_COLOR, "### ROW #" + counter++ + " ### - " + rowData.toString());
						}
						
						writer.writeNext(row);
					}
					
					writer.flush();
					writer.close();
				}
				
				emitter.nl();
				
				emitter.info("Execution report is available under " + getOutFileReport());
				if(results != null && !results.isEmpty()) emitter.info("Returned resultset is available under " + getOutFile());
			} catch(Throwable t) {
				writeReport(getOutFileReport(), "Unable to execute statement: unexpected " + t.getClass().getSimpleName() + " caught. Reason: " + t.getMessage());
				
				throw t;
			}
		}
		
		if(!confirm)
			emitter.abort();
	}
	
}
