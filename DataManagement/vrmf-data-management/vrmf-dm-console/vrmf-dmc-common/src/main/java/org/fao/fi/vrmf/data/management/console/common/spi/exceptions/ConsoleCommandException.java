/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.common.spi.exceptions;

/**
 * @author Fiorellato
 *
 */
public class ConsoleCommandException extends Exception {
	private static final long serialVersionUID = 8416459407602556719L;

	/**
	 * 
	 */
	public ConsoleCommandException() {
	}

	/**
	 * @param message
	 */
	public ConsoleCommandException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public ConsoleCommandException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ConsoleCommandException(String message, Throwable cause) {
		super(message, cause);
	}
}
