/**
 * 
 */
package org.fao.fi.vrmf.data.management.console.common.spi.impl;

import java.util.Properties;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.fao.fi.vrmf.data.management.common.spi.DataManagementOperationsProvider;
import org.fao.fi.vrmf.data.management.common.spi.ManagedData;
import org.fao.fi.vrmf.data.management.common.spi.ManagedDataHierarchy;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.DataManagementOperationException;
import org.fao.fi.vrmf.data.management.console.common.AbstractConsole;
import org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter;
import org.fao.fi.vrmf.data.management.console.common.spi.ConsoleCommand;
import org.fao.fi.vrmf.data.management.console.common.spi.exceptions.ConsoleCommandError;
import org.fao.fi.vrmf.data.management.console.common.spi.exceptions.ConsoleCommandException;

/**
 * @author Fiorellato
 *
 */
abstract public class AbstractConsoleCommand<DATA extends ManagedData<DATA>, HIERARCHY extends ManagedDataHierarchy<DATA, HIERARCHY>, OPERATIONS_PROVIDER extends DataManagementOperationsProvider<DATA, HIERARCHY>> implements ConsoleCommand<DATA, HIERARCHY, OPERATIONS_PROVIDER> {
	final protected boolean isInteractive(Properties properties) {
		return Boolean.parseBoolean(properties.getProperty(AbstractConsole.INTERACTIVE_PROPERTY));
	}
	
	final public boolean isVerbose(Properties properties) {
		return Boolean.parseBoolean(properties.getProperty(AbstractConsole.VERBOSE_PROPERTY));
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.spi.ConsoleCommand#hasArguments()
	 */
	@Override
	public boolean hasArguments() {
		return false;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.spi.ConsoleCommand#getCommandArguments()
	 */
	@Override
	public String getCommandArguments() {
		return null;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.spi.ConsoleCommand#hasOptions()
	 */
	final public boolean hasOptions() {
		Options opts = this.getOptions();
		
		return opts != null && !opts.getOptions().isEmpty(); 
	}	
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.console.common.spi.ConsoleCommand#execute(java.util.Properties, java.lang.String[], org.fao.fi.vrmf.data.management.common.spi.DataManagementOperationsProvider, org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter)
	 */
	final public void execute(Properties properties, String[] parameters, OPERATIONS_PROVIDER provider, AbstractEmitter emitter) throws ConsoleCommandException, ConsoleCommandError {
		try {
			CommandLine cmd = new PosixParser().parse(this.getOptions(), parameters);
			
			this.doExecute(properties, cmd, provider, emitter);
		} catch(ParseException|DataManagementOperationException e) {
			throw new ConsoleCommandException(e.getMessage());
		} catch(ConsoleCommandException CCe) {
			throw CCe;
		} catch(Throwable t) {
			throw this.raiseError(t);
		}
	}
	
	abstract protected void doExecute(Properties properties, CommandLine commandLine, OPERATIONS_PROVIDER provider, AbstractEmitter emitter) throws Exception;
}