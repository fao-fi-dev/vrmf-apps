/**
 * (c) 2015 FAO / UN (project: vrmf-data-management-console-common)
 */
package org.fao.fi.vrmf.data.management.console.common.spi;

import static org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter.DEFAULT_COMMAND_ARGD_COLOR;
import static org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter.DEFAULT_COMMAND_ARGS_COLOR;
import static org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter.DEFAULT_COMMAND_COLOR;
import static org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter.DEFAULT_COMMAND_DESC_COLOR;
import static org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter.DEFAULT_COMMAND_OPTS_COLOR;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.fao.fi.vrmf.data.management.console.common.exceptions.ConsoleException;
import org.fao.fi.vrmf.data.management.console.common.helpers.AbstractEmitter;
import org.fao.fi.vrmf.data.management.console.common.spi.exceptions.ConsoleCommandError;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 13, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 13, 2015
 */
public interface Command {
	default boolean hasOptions()  {
		Options opts = this.getOptions();
		
		return opts != null && !opts.getOptions().isEmpty(); 
	};
	
	boolean hasArguments();

	String getCommandName();
	String getCommandDescription();	
	String getCommandArguments();
	
	default String getCommandArgumentsDescriptions() {
		return null;
	};
	
	default String getFullCommandDescription() {
		String description = 
			   DEFAULT_COMMAND_COLOR + 
			   this.getCommandName() + 
			 ( this.hasOptions() ? DEFAULT_COMMAND_OPTS_COLOR + " [options]" : "" ) +
			 ( this.hasArguments() ? DEFAULT_COMMAND_ARGS_COLOR + " " + this.getCommandArguments() : "" ) +
			 ( this.hasArguments() ? DEFAULT_COMMAND_ARGD_COLOR  + " [ " +  this.getCommandArgumentsDescriptions() + " ] " : "" ) + 
			  "§" +
			   DEFAULT_COMMAND_DESC_COLOR + 
			  "=> " + ( DEFAULT_COMMAND_DESC_COLOR + this.getCommandDescription());
		
		if(!description.endsWith("."))
			description += ".";
		
		return description;
	};
	
	Options getOptions();
	
	default String getHelp(AbstractEmitter emitter) throws ConsoleException {
		HelpFormatter formatter = new HelpFormatter();
			
		try(StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw)) {
			formatter.printOptions(pw, Math.max(emitter.getConsoleWidth() - 8, 80), this.getOptions(), 0, 1);
		
			return sw.toString();
		} catch(IOException IOe) {
			throw new ConsoleException(IOe.getMessage(), IOe);
		}
	}
	
	default ConsoleCommandError raiseError(Throwable t) {
		return new ConsoleCommandError("Unable to execute '" + this.getCommandName() + "' command: unexpected " + t.getClass().getName() + " caught. Reason: " + t.getMessage(), t);
	}
}