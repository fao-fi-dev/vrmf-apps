/**
 * 
 */
package org.fao.fi.vrmf.data.management.common.spi.exceptions.managed;

import org.fao.fi.vrmf.data.management.common.spi.exceptions.DataManagementOperationException;


/**
 * @author Fiorellato
 *
 */
public class ItemNotFoundException extends DataManagementOperationException {
	private static final long serialVersionUID = -4176848247271456752L;

	final static public int EXCEPTION_CODE = 1005;

	public ItemNotFoundException() {
		super(EXCEPTION_CODE);
	}

	public ItemNotFoundException(String message, Throwable cause) {
		super(EXCEPTION_CODE, message, cause);
	}

	public ItemNotFoundException(String message) {
		super(EXCEPTION_CODE, message);
	}

	public ItemNotFoundException(Throwable cause) {
		super(EXCEPTION_CODE, cause);
	}
}