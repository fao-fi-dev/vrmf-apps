/**
 * 
 */
package org.fao.fi.vrmf.data.management.common.spi.exceptions;

/**
 * @author Fiorellato
 *
 */
public class UnmanagedException extends DataManagementOperationException {
	private static final long serialVersionUID = -6341708211032671985L;

	final static public int EXCEPTION_CODE = -1;
	
	/**
	 * @param code
	 */
	public UnmanagedException() {
		super(EXCEPTION_CODE);
	}

	/**
	 * @param message
	 */
	public UnmanagedException(String message) {
		super(EXCEPTION_CODE, message);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public UnmanagedException(String message, Throwable cause) {
		super(EXCEPTION_CODE, message, cause);
	}

	/**
	 * @param cause
	 */
	public UnmanagedException(Throwable cause) {
		super(EXCEPTION_CODE, cause);
	}
}
