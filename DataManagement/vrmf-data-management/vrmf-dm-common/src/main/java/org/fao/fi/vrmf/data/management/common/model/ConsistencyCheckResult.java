/**
 * 
 */
package org.fao.fi.vrmf.data.management.common.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Fiorellato
 *
 */
@XmlRootElement(name="ConsistencyCheckResult")
@XmlAccessorType(XmlAccessType.FIELD)
public class ConsistencyCheckResult implements Serializable, Comparable<ConsistencyCheckResult> {
	private static final long serialVersionUID = -2826053903518825009L;

	@XmlElement(name="Attribute")
	private String _attribute;
	
	@XmlElement(name="ItemId")
	private Integer _id;
	
	@XmlElement(name="ItemUidOK")
	private Integer _uidOk;
	
	@XmlElement(name="ItemUidKO")
	private Integer _uidKo;
	
	@XmlElement(name="Occurrencies")
	private Integer _occurrencies;
	
	public ConsistencyCheckResult() {
	}

	public ConsistencyCheckResult(String attribute, Integer id, Integer uidOk, Integer uidKo, Integer occurrencies) {
		super();
		this._attribute = attribute;
		this._id = id;
		this._uidOk = uidOk;
		this._uidKo = uidKo;
		this._occurrencies = occurrencies;
	}

	public String getAttribute() {
		return this._attribute;
	}

	public void setAttribute(String attribute) {
		this._attribute = attribute;
	}

	public Integer getId() {
		return this._id;
	}

	public void setId(Integer id) {
		this._id = id;
	}

	public Integer getUidOk() {
		return this._uidOk;
	}

	public void setUidOk(Integer uidOk) {
		this._uidOk = uidOk;
	}

	public int getUidKo() {
		return this._uidKo;
	}

	public void setUidKo(int uidKo) {
		this._uidKo = uidKo;
	}

	public Integer getOccurrencies() {
		return this._occurrencies;
	}

	public void setOccurrencies(Integer occurrencies) {
		this._occurrencies = occurrencies;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(ConsistencyCheckResult o) {
		if(this._id < o._id)
			return -1;
		
		if(this._id > o._id)
			return 1;
		
		if(this._uidKo < o._uidKo)
			return -1;
		
		if(this._uidKo > o._uidKo)
			return 1;

		if(this._attribute.compareTo(o._attribute) < 0)
			return -1;
		
		if(this._attribute.compareTo(o._attribute) > 0)
			return 1;

		return this._occurrencies.compareTo(o._occurrencies);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this._attribute == null) ? 0 : this._attribute.hashCode());
		result = prime * result + ((this._id == null) ? 0 : this._id.hashCode());
		result = prime * result + ((this._occurrencies == null) ? 0 : this._occurrencies.hashCode());
		result = prime * result + ((this._uidKo == null) ? 0 : this._uidKo.hashCode());
		result = prime * result + ((this._uidOk == null) ? 0 : this._uidOk.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConsistencyCheckResult other = (ConsistencyCheckResult) obj;
		if (this._attribute == null) {
			if (other._attribute != null)
				return false;
		} else if (!this._attribute.equals(other._attribute))
			return false;
		if (this._id == null) {
			if (other._id != null)
				return false;
		} else if (!this._id.equals(other._id))
			return false;
		if (this._occurrencies == null) {
			if (other._occurrencies != null)
				return false;
		} else if (!this._occurrencies.equals(other._occurrencies))
			return false;
		if (this._uidKo == null) {
			if (other._uidKo != null)
				return false;
		} else if (!this._uidKo.equals(other._uidKo))
			return false;
		if (this._uidOk == null) {
			if (other._uidOk != null)
				return false;
		} else if (!this._uidOk.equals(other._uidOk))
			return false;
		return true;
	}
}
