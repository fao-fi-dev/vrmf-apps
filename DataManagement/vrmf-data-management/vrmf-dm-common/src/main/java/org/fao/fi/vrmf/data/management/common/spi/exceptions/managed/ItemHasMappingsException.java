/**
 * 
 */
package org.fao.fi.vrmf.data.management.common.spi.exceptions.managed;

import org.fao.fi.vrmf.data.management.common.spi.exceptions.DataManagementOperationException;


/**
 * @author Fiorellato
 *
 */
public class ItemHasMappingsException extends DataManagementOperationException {
	private static final long serialVersionUID = -4176848247271456752L;
	
	final static public int EXCEPTION_CODE = 1003;

	public ItemHasMappingsException() {
		super(EXCEPTION_CODE);
	}

	public ItemHasMappingsException(String message, Throwable cause) {
		super(EXCEPTION_CODE, message, cause);
	}

	public ItemHasMappingsException(String message) {
		super(EXCEPTION_CODE, message);
	}

	public ItemHasMappingsException(Throwable cause) {
		super(EXCEPTION_CODE, cause);
	}
}
