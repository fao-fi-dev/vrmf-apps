/**
 * 
 */
package org.fao.fi.vrmf.data.management.common.spi.exceptions.managed;

import org.fao.fi.vrmf.data.management.common.spi.exceptions.DataManagementOperationException;


/**
 * @author Fiorellato
 *
 */
public class MultipleItemsFoundException extends DataManagementOperationException {
	private static final long serialVersionUID = -4176848247271456752L;

	final static public int EXCEPTION_CODE = 1006;

	public MultipleItemsFoundException() {
		super(EXCEPTION_CODE);
	}

	public MultipleItemsFoundException(String message, Throwable cause) {
		super(EXCEPTION_CODE, message, cause);
	}

	public MultipleItemsFoundException(String message) {
		super(EXCEPTION_CODE, message);
	}

	public MultipleItemsFoundException(Throwable cause) {
		super(EXCEPTION_CODE, cause);
	}
}