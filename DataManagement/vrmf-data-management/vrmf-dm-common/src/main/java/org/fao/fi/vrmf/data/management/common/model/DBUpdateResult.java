/**
 * 
 */
package org.fao.fi.vrmf.data.management.common.model;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Fiorellato
 *
 */
@XmlRootElement(name="DBUpdateResult")
@XmlAccessorType(XmlAccessType.FIELD)
public class DBUpdateResult implements Serializable {
	private static final long serialVersionUID = -5526922196240180742L;

	@XmlElement(name="CurrentItems")
	private Integer _currentItems;
	
	@XmlElement(name="CurrentItemsLastUpdateDate")
	private Date _currentItemsLastUpdate;
	
	@XmlElement(name="CurrentItemsLastReferenceDate")
	private Date _currentItemsLastReference;
	
	@XmlElement(name="UpdatedItems")
	private Integer _updatedItems;
	
	@XmlElement(name="UpdatedItemsLastUpdateDate")
	private Date _updatedItemsLastUpdate;
	
	@XmlElement(name="UpdatedItemsLastReferenceDate")
	private Date _updatedItemsLastReference;
 
	public DBUpdateResult() {
	}

	public DBUpdateResult(Integer currentItems, Date currentItemsLastUpdate, Date currentItemsLastReference, Integer updatedItems, Date updatedItemsLastUpdate, Date updatedItemsLastReference) {
		super();
		this._currentItems = currentItems;
		this._currentItemsLastUpdate = currentItemsLastUpdate;
		this._currentItemsLastReference = currentItemsLastReference;
		this._updatedItems = updatedItems;
		this._updatedItemsLastUpdate = updatedItemsLastUpdate;
		this._updatedItemsLastReference = updatedItemsLastReference;
	}

	public Integer getCurrentItems() {
		return this._currentItems;
	}

	public void setCurrentItems(Integer currentItems) {
		this._currentItems = currentItems;
	}

	public Date getCurrentItemsLastUpdate() {
		return this._currentItemsLastUpdate;
	}

	public void setCurrentItemsLastUpdate(Date currentItemsLastUpdate) {
		this._currentItemsLastUpdate = currentItemsLastUpdate;
	}

	public Date getCurrentItemsLastReference() {
		return this._currentItemsLastReference;
	}

	public void setCurrentItemsLastReference(Date currentItemsLastReference) {
		this._currentItemsLastReference = currentItemsLastReference;
	}

	public Integer getUpdatedItems() {
		return this._updatedItems;
	}

	public void setUpdatedItems(Integer updatedItems) {
		this._updatedItems = updatedItems;
	}

	public Date getUpdatedItemsLastUpdate() {
		return this._updatedItemsLastUpdate;
	}

	public void setUpdatedItemsLastUpdate(Date updatedItemsLastUpdate) {
		this._updatedItemsLastUpdate = updatedItemsLastUpdate;
	}

	public Date getUpdatedItemsLastReference() {
		return this._updatedItemsLastReference;
	}

	public void setUpdatedItemsLastReference(Date updatedItemsLastReference) {
		this._updatedItemsLastReference = updatedItemsLastReference;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this._currentItems == null) ? 0 : this._currentItems.hashCode());
		result = prime * result
				+ ((this._currentItemsLastReference == null) ? 0 : this._currentItemsLastReference.hashCode());
		result = prime * result
				+ ((this._currentItemsLastUpdate == null) ? 0 : this._currentItemsLastUpdate.hashCode());
		result = prime * result + ((this._updatedItems == null) ? 0 : this._updatedItems.hashCode());
		result = prime * result
				+ ((this._updatedItemsLastReference == null) ? 0 : this._updatedItemsLastReference.hashCode());
		result = prime * result
				+ ((this._updatedItemsLastUpdate == null) ? 0 : this._updatedItemsLastUpdate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DBUpdateResult other = (DBUpdateResult) obj;
		if (this._currentItems == null) {
			if (other._currentItems != null)
				return false;
		} else if (!this._currentItems.equals(other._currentItems))
			return false;
		if (this._currentItemsLastReference == null) {
			if (other._currentItemsLastReference != null)
				return false;
		} else if (!this._currentItemsLastReference.equals(other._currentItemsLastReference))
			return false;
		if (this._currentItemsLastUpdate == null) {
			if (other._currentItemsLastUpdate != null)
				return false;
		} else if (!this._currentItemsLastUpdate.equals(other._currentItemsLastUpdate))
			return false;
		if (this._updatedItems == null) {
			if (other._updatedItems != null)
				return false;
		} else if (!this._updatedItems.equals(other._updatedItems))
			return false;
		if (this._updatedItemsLastReference == null) {
			if (other._updatedItemsLastReference != null)
				return false;
		} else if (!this._updatedItemsLastReference.equals(other._updatedItemsLastReference))
			return false;
		if (this._updatedItemsLastUpdate == null) {
			if (other._updatedItemsLastUpdate != null)
				return false;
		} else if (!this._updatedItemsLastUpdate.equals(other._updatedItemsLastUpdate))
			return false;
		return true;
	}
}
