/**
 * 
 */
package org.fao.fi.vrmf.data.management.common.spi.exceptions;

/**
 * @author Fiorellato
 *
 */
public class DataManagementOperationException extends Exception {
	private static final long serialVersionUID = 3988752933581884976L;

	private int _exceptionCode = 0;
	
	public DataManagementOperationException(int code) {
		this._exceptionCode = code;
	}

	/**
	 * @param message
	 */
	public DataManagementOperationException(int code, String message) {
		super(message);
		
		this._exceptionCode = code;
	}

	public DataManagementOperationException(int code, String message, Throwable cause) {
		super(message, cause);
		
		this._exceptionCode = code;
	}

	public DataManagementOperationException(int code, Throwable cause) {
		super(cause);
		
		this._exceptionCode = code;
	}

	public int getExceptionCode() {
		return this._exceptionCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this._exceptionCode;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DataManagementOperationException other = (DataManagementOperationException) obj;
		if (this._exceptionCode != other._exceptionCode)
			return false;
		return true;
	}
}
