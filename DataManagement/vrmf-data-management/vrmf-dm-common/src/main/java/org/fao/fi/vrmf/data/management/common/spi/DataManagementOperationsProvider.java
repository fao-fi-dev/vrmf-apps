/**
 * 
 */
package org.fao.fi.vrmf.data.management.common.spi;

import java.util.Collection;
import java.util.Date;
import java.util.Properties;

import org.fao.fi.vrmf.data.management.common.model.ConsistencyCheckResult;
import org.fao.fi.vrmf.data.management.common.model.DBUpdateResult;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.DataManagementOperationException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.DataManagementOperationPropertyException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.UnmanagedException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.HierarchyHasCyclesException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemAlreadyMappedException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemHasMappingsException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemIsNotMappedException;
import org.fao.fi.vrmf.data.management.common.spi.exceptions.managed.ItemNotFoundException;

/**
 * @author Fiorellato
 *
 */
public interface DataManagementOperationsProvider<DATA extends ManagedData<DATA>, HIERARCHY extends ManagedDataHierarchy<DATA, HIERARCHY> > {
	/**
	 * @return a free-text description of the data management operations provider
	 */
	String description();
	
	/**
	 * @return the current set of properties for the data management operations provider
	 */
	Properties properties();
	
	/**
	 * Notifies the data management operations provider that a configuration property has changed
	 * @param property a property
	 * @param value its value
	 * @return the actual value of the property
	 * @throws DataManagementOperationPropertyException if the new value set for the property is invalid
	 */
	String notifyPropertyChanged(String property, String value) throws DataManagementOperationPropertyException; 
	
	/**
	 * @param itemId the item internal identifier
	 * @return the item UID given the item internal identifier
	 * @throws ItemNotFoundException if no item can be identified by the given item identifier
	 * @throws UnmanagedException if anything else goes wrong
	 */	
	int getUID(int itemId) throws ItemNotFoundException, UnmanagedException, DataManagementOperationException;

	/**
	 * @param itemId the item internal identifier
	 * @return the full hierarchy for the item with the given internal identifier. Not necessarily the item with the given identifier will be the root of the hierarchy
	 * @throws ItemNotFoundException if no item can be identified by the given item internal identifier
	 * @throws UnmanagedException if anything else goes wrong
	 */
	HIERARCHY getHierarchyByItemID(int itemId) throws ItemNotFoundException, UnmanagedException, DataManagementOperationException;
	
	/**
	 * @param itemId the item internal identifier
	 * @return the item hierarchy for the item with the given internal identifier and its children. The item with the given identifier will be the root of the hierarchy
	 * @throws ItemNotFoundException if no item can be identified by the given item internal identifier
	 * @throws UnmanagedException if anything else goes wrong
	 */
	HIERARCHY getHierarchyBelowByItemID(int itemId) throws ItemNotFoundException, UnmanagedException, DataManagementOperationException;

	/**
	 * @param itemUid an item UID
	 * @return the hierarchy for items with the given UID. 
	 * @throws ItemNotFoundException if no item can be identified by the given item UID
	 * @throws UnmanagedException if anything else goes wrong
	 */
	HIERARCHY getHierarchyByItemUID(int itemUid) throws ItemNotFoundException, UnmanagedException, DataManagementOperationException;
	
	/**
	 * @param itemId the item internal identifier
	 * @return the item current data (including IDs and metadata) and its mapping information
	 * @throws ItemNotFoundException if no item can be identified by the given item internal identifier
	 * @throws UnmanagedException if anything else goes wrong
	 */	
	DATA getDataByItemID(int itemId) throws ItemNotFoundException, UnmanagedException, DataManagementOperationException;

	/**
	 * @param itemUid an item UID
	 * @return the collection of item data sharing the provided UID
	 * @throws ItemNotFoundException if no item can be identified by the given item UID
	 * @throws UnmanagedException if anything else goes wrong
	 */	
	Collection<DATA> getDataByItemUID(int itemUid) throws ItemNotFoundException, UnmanagedException, DataManagementOperationException;

	/**
	 * Links a source item to a target item, creating a new parent-child relationship
	 * @param sourceItemId the source item internal identifier
	 * @param targetItemId the target item internal identifier
	 * @param user the user responsible for committing this mapping
	 * @param score the mapping score
	 * @param date the mapping date
	 * @param comment the mapping comment
	 * @throws ItemNotFoundException if no item can be identified either by source item internal identifier or by target item internal identifier
	 * @throws ItemAlreadyMappedException if the source item is already mapped to another target item
	 * @throws HierarchyHasCyclesException if the proposed mapping yields cycles in the mapping graph
	 * @throws UnmanagedException if anything else goes wrong
	 */
	void link(int sourceItemId, int targetItemId, String user, double score, Date date, String comment) throws ItemNotFoundException, ItemAlreadyMappedException, HierarchyHasCyclesException, UnmanagedException, DataManagementOperationException;

	/**
	 * Removes an item (and its children, when available) from its current hierarchy, creating a new hierarchy that spans from the removed item downwards and 
	 * assigning a new UID to all items participating to the new relationship
	 * @param itemId the item internal identifier
	 * @return the new item UID for the removed item and its children (when available)
	 * @throws ItemNotFoundException if no item can be identified by the given item internal identifier
	 * @throws UnmanagedException if anything else goes wrong
	 */	
	int unlink(int itemId) throws ItemNotFoundException, ItemIsNotMappedException, UnmanagedException, DataManagementOperationException;
	
	/**
	 * Deletes an item and its data 
	 * @param itemId the item internal identifier
	 * @throws ItemNotFoundException if no item can be identified by the given item internal identifier
	 * @throws ItemHasMappingsException if other items are mapped to the item identified by the given item internal identifier
	 * @throws UnmanagedException if anything else goes wrong
	 */
	void delete(int itemId) throws ItemNotFoundException, ItemHasMappingsException, UnmanagedException, DataManagementOperationException;
	
	Collection<ConsistencyCheckResult> checkConsistency() throws UnmanagedException, DataManagementOperationException;
	
	DBUpdateResult updateDB() throws UnmanagedException, DataManagementOperationException;
	
	Collection<String[]> executeStatement(String statement) throws UnmanagedException, DataManagementOperationException;
}