/**
 * 
 */
package org.fao.fi.vrmf.data.management.common.spi.exceptions;

/**
 * @author Fiorellato
 *
 */
public class DataManagementOperationPropertyException extends UnmanagedException {
	private static final long serialVersionUID = 4778961164041321532L;

	public DataManagementOperationPropertyException() {
		super();
	}

	public DataManagementOperationPropertyException(String message, Throwable cause) {
		super(message, cause);
	}

	public DataManagementOperationPropertyException(String message) {
		super(message);
	}

	public DataManagementOperationPropertyException(Throwable cause) {
		super(cause);
	}
}
