/**
 * 
 */
package org.fao.fi.vrmf.data.management.common.spi;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.sh.model.core.spi.Mapped;
import org.fao.fi.sh.model.core.spi.Updatable;
import org.fao.fi.vrmf.common.core.behavior.Sourced;

/**
 * @author Fiorellato
 */
@XmlRootElement(name="ManagedData")
@XmlAccessorType(XmlAccessType.FIELD)
abstract public class ManagedData<SELF extends ManagedData<SELF>> implements Comparable<SELF>, Mapped<Integer, Integer>, Sourced, Updatable {
	private static final long serialVersionUID = 6503179321399808204L;
}
