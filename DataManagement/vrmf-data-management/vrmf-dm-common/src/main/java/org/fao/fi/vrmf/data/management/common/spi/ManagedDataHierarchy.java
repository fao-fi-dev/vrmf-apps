/**
 * 
 */
package org.fao.fi.vrmf.data.management.common.spi;

import java.io.Serializable;

/**
 * @author Fiorellato
 *
 */
public interface ManagedDataHierarchy<DATA extends ManagedData<DATA>, SELF extends ManagedDataHierarchy<DATA, SELF>> extends Serializable, Comparable<SELF> {
	DATA getData();
	
	Iterable<SELF> children();
	int countChildren();
	
	boolean add(SELF toAdd);
	boolean remove(SELF toRemove);
	boolean isEmpty();
}
