/**
 * 
 */
package org.fao.fi.vrmf.data.management.common.spi.exceptions.managed;

import org.fao.fi.vrmf.data.management.common.spi.exceptions.DataManagementOperationException;


/**
 * @author Fiorellato
 *
 */
public class ItemAlreadyMappedException extends DataManagementOperationException {
	private static final long serialVersionUID = -4176848247271456752L;

	final static public int EXCEPTION_CODE = 1002;
	
	public ItemAlreadyMappedException() {
		super(EXCEPTION_CODE);
	}

	public ItemAlreadyMappedException(String message, Throwable cause) {
		super(EXCEPTION_CODE, message, cause);
	}

	public ItemAlreadyMappedException(String message) {
		super(EXCEPTION_CODE, message);
	}

	public ItemAlreadyMappedException(Throwable cause) {
		super(EXCEPTION_CODE, cause);
	}
}