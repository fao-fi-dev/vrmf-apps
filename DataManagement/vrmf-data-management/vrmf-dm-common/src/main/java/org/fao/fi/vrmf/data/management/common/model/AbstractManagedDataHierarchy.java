/**
 * 
 */
package org.fao.fi.vrmf.data.management.common.model;

import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.vrmf.data.management.common.spi.ManagedData;
import org.fao.fi.vrmf.data.management.common.spi.ManagedDataHierarchy;

/**
 * @author Fiorellato
 *
 */
@XmlRootElement(name="AbstractDataHierarchy")
@XmlAccessorType(XmlAccessType.FIELD)
abstract public class AbstractManagedDataHierarchy<DATA extends ManagedData<DATA>, SELF extends AbstractManagedDataHierarchy<DATA, SELF>> implements ManagedDataHierarchy<DATA, SELF> {
	private static final long serialVersionUID = 2065079814368498253L;

	@XmlElement(name="Data")
	protected DATA _data;
	
	@XmlElementWrapper(name="MappedData")
	@XmlElement(name="DataHierarchy")
	protected Collection<SELF> _children;
	
	public AbstractManagedDataHierarchy() {
		this._children = new TreeSet<SELF>();
	}

	/**
	 * @param data
	 */
	public AbstractManagedDataHierarchy(DATA data) {
		this();

		this._data = data;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	abstract public int compareTo(SELF o);
	
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.common.spi.ManagedDataHierarchy#getData()
	 */
	public DATA getData() {
		return this._data;
	}
	
	public Collection<SELF> getChildren() {
		return this._children;
	}

	public void setChildren(Set<SELF> children) {
		this._children = children;
	}

	public void setData(DATA data) {
		this._data = data;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.common.spi.ManagedDataHierarchy#children()
	 */
	public Iterable<SELF> children() {
		return this._children;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.common.spi.ManagedDataHierarchy#countChildren()
	 */
	public int countChildren() {
		return this._children.size();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.common.spi.ManagedDataHierarchy#add(org.fao.fi.vrmf.data.management.common.spi.ManagedDataHierarchy)
	 */
	public boolean add(SELF toAdd) {
		return this._children.add(toAdd);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.common.spi.ManagedDataHierarchy#remove(org.fao.fi.vrmf.data.management.common.spi.ManagedDataHierarchy)
	 */
	public boolean remove(SELF toRemove) {
		return this._children.remove(toRemove);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.data.management.common.spi.ManagedDataHierarchy#isEmpty()
	 */
	public boolean isEmpty() {
		return this._children.isEmpty();
	}
}