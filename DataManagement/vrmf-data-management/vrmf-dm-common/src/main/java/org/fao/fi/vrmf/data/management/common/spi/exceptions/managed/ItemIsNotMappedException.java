/**
 * 
 */
package org.fao.fi.vrmf.data.management.common.spi.exceptions.managed;

import org.fao.fi.vrmf.data.management.common.spi.exceptions.DataManagementOperationException;


/**
 * @author Fiorellato
 *
 */
public class ItemIsNotMappedException extends DataManagementOperationException {
	private static final long serialVersionUID = -4176848247271456752L;

	final static public int EXCEPTION_CODE = 1004;

	public ItemIsNotMappedException() {
		super(EXCEPTION_CODE);
	}

	public ItemIsNotMappedException(String message, Throwable cause) {
		super(EXCEPTION_CODE, message, cause);
	}

	public ItemIsNotMappedException(String message) {
		super(EXCEPTION_CODE, message);
	}

	public ItemIsNotMappedException(Throwable cause) {
		super(EXCEPTION_CODE, cause);
	}
}