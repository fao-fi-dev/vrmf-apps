/**
 * 
 */
package org.fao.fi.vrmf.data.management.common.spi.exceptions.managed;

import org.fao.fi.vrmf.data.management.common.spi.exceptions.DataManagementOperationException;


/**
 * @author Fiorellato
 *
 */
public class DifferentSourcesException extends DataManagementOperationException {
	private static final long serialVersionUID = -4176848247271456752L;

	final static public int EXCEPTION_CODE = 1000;

	public DifferentSourcesException() {
		super(EXCEPTION_CODE);
	}

	public DifferentSourcesException(String message, Throwable cause) {
		super(EXCEPTION_CODE, message, cause);
	}

	public DifferentSourcesException(String message) {
		super(EXCEPTION_CODE, message);
	}

	public DifferentSourcesException(Throwable cause) {
		super(EXCEPTION_CODE, cause);
	}
}
