/**
 * 
 */
package org.fao.fi.vrmf.data.management.common.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import org.fao.fi.vrmf.data.management.common.spi.ManagedData;

/**
 * @author Fiorellato
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
abstract public class AbstractManagedData<SELF extends AbstractManagedData<SELF>> extends ManagedData<SELF> {
	private static final long serialVersionUID = -9172662495087984151L;
	
	@XmlAttribute(name="id") protected Integer _id;
	@XmlAttribute(name="uid") protected Integer _uid;
	
	@XmlAttribute(name="source") protected String _sourceSystem;
	
	@XmlAttribute(name="mapsTo") protected Integer _mapsTo;
	@XmlAttribute(name="mappingWeight") protected Double _mappingWeight;
	@XmlAttribute(name="mappingDate") protected Date _mappingDate;
	@XmlAttribute(name="mappingUser") protected String _mappingUser;
	@XmlAttribute(name="mappingComment") protected String _mappingComment;
	
	@XmlElement(name="UpdaterId") protected String _updaterId;
	@XmlElement(name="DateOfUpdate") protected Date _updateDate;
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.behaviours.data.Identifiable#getId()
	 */
	public Integer getId() {
		return this._id;
	}
	
	public void setId(Integer id) {
		this._id = id;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.sh.model.core.spi.Mapped#getTargetId()
	 */
	@Override
	public Integer getTargetId() {
		return getMapsTo();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.behaviours.data.Groupable#getUid()
	 */
	public Integer getUid() {
		return this._uid;
	}
	
	public void setUid(Integer uid) {
		this._uid = uid;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.behaviours.data.Sourced#getSourceSystem()
	 */
	public String getSourceSystem() {
		return this._sourceSystem;
	}

	public void setSourceSystem(String sourceSystem) {
		this._sourceSystem = sourceSystem;
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.behaviours.data.Mappable#getMapsTo()
	 */
	public Integer getMapsTo() {
		return this._mapsTo;
	}
	
	public void setMapsTo(Integer mapsTo) {
		this._mapsTo = mapsTo;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.behaviours.data.Mappable#getMappingWeight()
	 */
	public Double getMappingWeight() {
		return this._mappingWeight;
	}
	
	public void setMappingWeight(Double mappingWeight) {
		this._mappingWeight = mappingWeight;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.behaviours.data.Mappable#getMappingDate()
	 */
	public Date getMappingDate() {
		return this._mappingDate;
	}
	
	public void setMappingDate(Date mappingDate) {
		this._mappingDate = mappingDate;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.behaviours.data.Mappable#getMappingUser()
	 */
	public String getMappingUser() {
		return this._mappingUser;
	}
	
	public void setMappingUser(String mappingUser) {
		this._mappingUser = mappingUser;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.behaviours.data.Mappable#getMappingComment()
	 */
	public String getMappingComment() {
		return this._mappingComment;
	}
	
	public void setMappingComment(String mappingComment) {
		this._mappingComment = mappingComment;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.behaviours.data.Updatable#getUpdaterId()
	 */
	public String getUpdaterId() {
		return this._updaterId;
	}
	
	public void setUpdaterId(String updaterId) {
		this._updaterId = updaterId;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.behaviours.data.Updatable#getUpdateDate()
	 */
	public Date getUpdateDate() {
		return this._updateDate;
	}
	
	public void setUpdateDate(Date updateDate) {
		this._updateDate = updateDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this._id == null) ? 0 : this._id.hashCode());
		result = prime * result + ((this._mappingComment == null) ? 0 : this._mappingComment.hashCode());
		result = prime * result + ((this._mappingDate == null) ? 0 : this._mappingDate.hashCode());
		result = prime * result + ((this._mappingUser == null) ? 0 : this._mappingUser.hashCode());
		result = prime * result + ((this._mappingWeight == null) ? 0 : this._mappingWeight.hashCode());
		result = prime * result + ((this._mapsTo == null) ? 0 : this._mapsTo.hashCode());
		result = prime * result + ((this._sourceSystem == null) ? 0 : this._sourceSystem.hashCode());
		result = prime * result + ((this._uid == null) ? 0 : this._uid.hashCode());
		result = prime * result + ((this._updateDate == null) ? 0 : this._updateDate.hashCode());
		result = prime * result + ((this._updaterId == null) ? 0 : this._updaterId.hashCode());
		return result;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractManagedData other = (AbstractManagedData) obj;
		if (this._id == null) {
			if (other._id != null)
				return false;
		} else if (!this._id.equals(other._id))
			return false;
		if (this._mappingComment == null) {
			if (other._mappingComment != null)
				return false;
		} else if (!this._mappingComment.equals(other._mappingComment))
			return false;
		if (this._mappingDate == null) {
			if (other._mappingDate != null)
				return false;
		} else if (!this._mappingDate.equals(other._mappingDate))
			return false;
		if (this._mappingUser == null) {
			if (other._mappingUser != null)
				return false;
		} else if (!this._mappingUser.equals(other._mappingUser))
			return false;
		if (this._mappingWeight == null) {
			if (other._mappingWeight != null)
				return false;
		} else if (!this._mappingWeight.equals(other._mappingWeight))
			return false;
		if (this._mapsTo == null) {
			if (other._mapsTo != null)
				return false;
		} else if (!this._mapsTo.equals(other._mapsTo))
			return false;
		if (this._sourceSystem == null) {
			if (other._sourceSystem != null)
				return false;
		} else if (!this._sourceSystem.equals(other._sourceSystem))
			return false;
		if (this._uid == null) {
			if (other._uid != null)
				return false;
		} else if (!this._uid.equals(other._uid))
			return false;
		if (this._updateDate == null) {
			if (other._updateDate != null)
				return false;
		} else if (!this._updateDate.equals(other._updateDate))
			return false;
		if (this._updaterId == null) {
			if (other._updaterId != null)
				return false;
		} else if (!this._updaterId.equals(other._updaterId))
			return false;
		return true;
	}
}
