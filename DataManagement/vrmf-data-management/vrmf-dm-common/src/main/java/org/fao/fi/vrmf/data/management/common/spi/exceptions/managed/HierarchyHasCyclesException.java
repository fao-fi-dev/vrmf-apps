/**
 * 
 */
package org.fao.fi.vrmf.data.management.common.spi.exceptions.managed;

import org.fao.fi.vrmf.data.management.common.spi.exceptions.DataManagementOperationException;


/**
 * @author Fiorellato
 *
 */
public class HierarchyHasCyclesException extends DataManagementOperationException {
	private static final long serialVersionUID = 5194022174547066523L;

	final static public int EXCEPTION_CODE = 1001;

	public HierarchyHasCyclesException() {
		super(EXCEPTION_CODE);
	}

	/**
	 * @param message
	 */
	public HierarchyHasCyclesException(String message) {
		super(EXCEPTION_CODE, message);
	}

	/**
	 * @param cause
	 */
	public HierarchyHasCyclesException(Throwable cause) {
		super(EXCEPTION_CODE, cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public HierarchyHasCyclesException(String message, Throwable cause) {
		super(EXCEPTION_CODE, message, cause);
	}
}