function UserManager() {
	this.loggedUser = null;
	this.NO_LOGGED_USER_ID = "NOT_SET";
};

UserManager.prototype.setLoggedUser = function(loggedUser) {
	this.loggedUser = loggedUser;
};

UserManager.prototype.getLoggedUser = function() {
	if(!$$.isSet(this.loggedUser)) {
		$$.Log.debug("No logged user data can be accessed through the UserManager!");
		
		return new $$.UserModel(this.NO_LOGGED_USER_ID);
	}
	
	return this.loggedUser;
};

UserManager.prototype.logIn = function(userData) {
	this.setLoggedUser(new UserModel().initialize(userData));
	
	return this.loggedUser;
};

UserManager.prototype.logOut = function(userData) {
	if($$.isSet(this.loggedUser)) //TODO: improve
		this.loggedUser = null;
};

UserManager.prototype.isUserLogged = function() {
	return $$.isSet(this.loggedUser) &&
		   this.loggedUser.id != this.NO_LOGGED_USER_ID;
};

UserManager.prototype.isLogged = function(user) {
	return $$.isSet(this.loggedUser) &&
		   $$.isSet(user) &&
		   this.loggedUser.id == user.id;
};

$.extend(true, $$, {
	UserManager: new UserManager()
});