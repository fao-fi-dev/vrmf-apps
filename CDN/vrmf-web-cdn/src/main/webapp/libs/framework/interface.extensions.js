function Interface() {
	$$.Log.debug("Initializing 'Interface' Object...");
	
	//Current notification severity level
	this.NOTIFICATION_SEVERITY_LEVEL = $$.Log.SEVERITIES.WARN;
	
	if(!$$.isSet(window._alert))
		window._alert = window.alert;

	window.alert = function(content) {
		$$.Interface.showDialog($$.I18n.getText("vrmf.common.interface.alert.label"), content, { dialogClass: 'alert' });
		
		$$.Log.warn(content);
	};

	if(!$$.isSet(window._confirm))
		window._confirm = window.confirm;

	window.confirm = function(content, onconfirm, oncancel) {
		var confirm = window._confirm(content);
		
		if(confirm)
			onconfirm;
		else
			oncancel;
		
		return confirm;
	};	
};

Interface.prototype.setSeverityLevel = function(level) {
	this.NOTIFICATION_SEVERITY_LEVEL = level;
};

Interface.prototype.getSeverityLevel = function() {
	return this.NOTIFICATION_SEVERITY_LEVEL;
};

Interface.prototype.disableUI = function() {
	if($$.isSet($.blockUI))
		$.blockUI();
	else {
		var overlay = $("#glasspane");

		if($$.isSet(overlay) && overlay.length == 1) {
			overlay.css("width", $(document).width() - 30);
			overlay.css("height", $(document).height() - 30);
			
			overlay.show();
		} else {
			$$.Log.info("Unable to block UI: no glasspane element is currently defined");
		}
	}
};

Interface.prototype.enableUI = function() {	
	if($$.isSet($.unblockUI))
		$.unblockUI();
	else {
		var overlay = $("#glasspane");
		
		if($$.isSet(overlay) && overlay.length == 1) { overlay.hide(); }
	}
};

Interface.prototype.disableInputs = function(target) {
	$("input:enabled, select:enabled, textarea:enabled", $$.isSet(target) ? $(target) : $("html")).each(function() {
		$(this).attr("disabled", "disabled").data("interface_disabled", true);
	});
};

Interface.prototype.enableInputs = function(target) {
	$("input:disabled, select:disabled, textarea:disabled", $$.isSet(target) ? $(target) : $("html")).each(function() {
		var $this = $(this);
		
		if($this.data("interface_disabled") != null && $this.data("interface_disabled"))
			$this.attr("disabled", null).removeData("interface_disabled");		
	});
};

Interface.prototype.setTitle = function(title) {
	try {
		document.title = title;
	} catch (E) {
		$$.Log.warn("Unable to set title '" + title + "' for current browser window. Reason: " + E);
	}
};

Interface.prototype.appendToTitle = function(additionalText) {
	try {
		document.title = document.title + additionalText;
	} catch (E) {
		$$.Log.warn("Unable to add text '" + additionalText + "' to current browser window title. Reason: " + E);
	}
};

Interface.prototype.startWork = function() {
	$(".wait").fadeIn();
};

Interface.prototype.stopWork = function() {
	$(".wait").fadeOut();
};

Interface.prototype.showDialog = function(title, content, options) {
	var formattedContent = "";
	
	if($.isArray(content)) {
		for(var l=0; l<content.length; l++) {
			formattedContent += this._getRealContent(content[l]);
		}
	} else
		formattedContent = this._getRealContent(content);
	
	window._alert("[ " + title + " ] : " + content);
};

Interface.prototype._getRealContent = function(content) {
	if(typeof content === 'string')
		return content + "\n";
	else if($.isFunction(content.html) && $.isFunction(content.text))
		return content.text() + "\n";
	
	return content;
};

Interface.prototype.isSeverityAllowed = function(severityLevel) {
	return $$.Log.isSeverityAllowed(this.NOTIFICATION_SEVERITY_LEVEL, severityLevel);
};

Interface.prototype.error = function(content, header) {
	if(this.isSeverityAllowed($$.Log.SEVERITIES.ERROR))
		this.showDialog($$.I18n.getText("vrmf.common.interface.error.label") + ( $$.isSet(header) ? ": " + header : "" ), content, { dialogClass: 'error' });
	
	$$.Log.error(content);
};

Interface.prototype.warn = function(content, header) {
	if(this.isSeverityAllowed($$.Log.SEVERITIES.WARN))
		this.showDialog($$.I18n.getText("vrmf.common.interface.warn.label") + ( $$.isSet(header) ? ": " + header : "" ), content, { dialogClass: 'warning' });
	
	$$.Log.warn(content);
};

Interface.prototype.info = function(content, header) {
	if(this.isSeverityAllowed($$.Log.SEVERITIES.INFO))
		this.showDialog($$.I18n.getText("vrmf.common.interface.info.label") + ( $$.isSet(header) ? ": " + header : "" ), content, { dialogClass: 'info' });
	
	$$.Log.info(content);
};

Interface.prototype.focusOnField = function(field) {
	var toFocus = null;
	
	if(!$$.isSet(field)) {
		toFocus = $("input, select, textarea").get(0);
		
		if(toFocus != null)
			toFocus = $(toFocus);
	} else {
		if(typeof field == 'string')
			toFocus = $("#" + field);
		else 
			toFocus = $(field);		
	}
	
	if(toFocus != null) {
		$$.Log.debug("Focusing on " + toFocus);
		
		toFocus.focus();
	}
};

Interface.prototype.debug = function(content) {
	if(isSeverityAllowed($$.Log.SEVERITIES.DEBUG))
		this.showDialog("Debug", content, { dialogClass: 'debug' });
	
	$$.Log.debug(content);
};

Interface.prototype.openWindow = function(url, title, optionsArray) {
	var serializedOptions = $$.isSet(optionsArray) && $$.isArray(optionsArray) ? optionsArray.join(", ") : null;
	
	return window.open(url, title, serializedOptions);
};

Interface.prototype.deselectAllText = function() {
	if(document.selection) 
		document.selection.empty(); 
	else if(window.getSelection)
	    window.getSelection().removeAllRanges();			
};

Interface.prototype.selectText = function(selector) {
	var $i = this;
    this.deselectAllText();

    var target = $(selector);
    
    if(target.size() > 1)
    	target.each(function() { $i._selectText($(this)); });
    else
    	$i._selectText(target);    
};

Interface.prototype._selectText = function(element) {
	var elem = $(element).get(0);
	
	if(document.selection) {
    	var range = document.body.createTextRange();
    	
        range.moveToElementText(elem);
        
        range.select();    		
	} else if (window.getSelection) {
		var range = document.createRange();
		
		range.selectNodeContents(elem);
		
		window.getSelection().addRange(range);
	}
};

Interface.prototype._getManagedTabLinks = function(target) {
	var tabLinks = new Array();
	
	$("li", target).each(function() {
		var link = $("a:first", $(this));
		var anchor;
		
		if(link.length == 1) {
			anchor = $$.getAnchor(link);
			
			if(anchor != null)
				tabLinks.push(link);
		}
	});
	
	return tabLinks;
};

Interface.prototype.tabs = function(target, options) {
	var tabLinks = this._getManagedTabLinks(target);

	if(tabLinks.length > 0) {
		var link;
		var anchor;

		for(var l=0; l<tabLinks.length; l++) {
			link = tabLinks[l];
			
			anchor = $$.getAnchor($(link));
	
			$("#" + anchor).addClass("hidden");
			
			link.parent().addClass("-interface-tabs").addClass("-interface-tabs-selectable");
				
			link.click(function() {
				var tab = $(this).parent("li");

				if(tab.hasClass("-interface-tab-disabled")) {					
					return false;
				} else {					
					$$.Interface._showTab(target, $(this));
					
					return false;
				}
			});
		}

		this._showTab(target, tabLinks[0]);
	}
};

Interface.prototype.selectTab = function(tabID, target) {
	var tabLinks = this._getManagedTabLinks(target);
	
	for(var t=0; t<tabLinks.length; t++)
		if(tabID === tabLinks[t].attr('href')) {
			tabLinks[t].click();
			
			return;
		}
};

Interface.prototype._findTabLink = function(tabID, target) {
	var tabLinks = this._getManagedTabLinks(target);
	
	for(var t=0; t<tabLinks.length; t++)
		if(tabID === tabLinks[t].attr('href')) {
			return tabLinks[t];
		}
	
	return null;
};

Interface.prototype.disableTab = function(tabID, target) {
	$$.Log.info("Disabling tab #" + tabID);
	
	var tabLink = this._findTabLink(tabID, target);
	
	if(tabLink != null) {
		var tab = tabLink.parent("li");
		
		tab.addClass("-interface-tab-disabled");
	}
};

Interface.prototype.enableTab = function(tabID, target) {
	$$.Log.info("Enabling tab #" + tabID);
	
	var tabLink = this._findTabLink(tabID, target);
	
	if(tabLink != null) {
		var tab = tabLink.parent("li");
	
		tab.removeClass("-interface-tab-disabled");
	}
};

Interface.prototype._showTab = function(target, link) {
	var anchor = $$.getAnchor(link);
	
	$("li > a", target).each(function() {
		var $this = $(this);
		$this.parent().removeClass("-interface-tabs-selected");
		
		var currentAnchor = $$.getAnchor($this); 
		
		if(currentAnchor != anchor) {
			$("#" + currentAnchor).addClass("hidden").removeClass("-interface-tabs-content-current");
		} 
	});
	
	link.parent().addClass("-interface-tabs-selected");

	$("#" + anchor).removeClass("hidden").addClass("-interface-tabs-content-current");
};

$$.Dep.require("vrmf.interface.extensions", { "vrmf.i18n": { identifier: "$$.I18n" } }, function() {
	$.extend($$, { 
		Interface: new Interface()
	});
});