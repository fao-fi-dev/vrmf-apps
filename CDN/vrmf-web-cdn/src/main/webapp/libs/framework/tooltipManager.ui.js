$$.TooltipManager.MANAGERS.UI = {
	options: { 
		tooltipClass: "ui-tooltip-shadow",
		position: {
			my: "left bottom",
			at: "left top",
			collision: "flipfit flip" 
		},
		show: 0,
		hide: 0,
		content: function() {
			return $(this).attr("title");
		}
	},
	additionalClasses: [ 'hint' ],
	initialize: function(options) {
		
	},
	applyStatic: function(target, options) {
		var extendedOptions = $.extend(true, {}, $$.TooltipManager.MANAGERS.UI.options, options);
		extendedOptions = $.extend(true, {}, extendedOptions, $$.TooltipManager.getConfiguration(target));
		
		$(target).addClass($$.TooltipManager.TOOLTIP_MARKER_CLASS);
		$(target).tooltip($.extend(true, extendedOptions));
	},
	applyHTML: function(target, options) {
		var $target = $(target);
		
		var extendedOptions = $.extend(true, {}, $$.TooltipManager.MANAGERS.UI.options, options);
		extendedOptions = $.extend(true, {}, extendedOptions, $$.TooltipManager.getConfiguration(target));

		$target.data("htmlTitle", $($target.attr("title")));
		
		$target.addClass($$.TooltipManager.TOOLTIP_MARKER_CLASS);
		
		var isA = $target.is("a");
		
		$target.tooltip($.extend(true, extendedOptions, {
			disabled: isA ? true : false,
			position: { my: isA ? "left top" : "left bottom", at: isA ? "left bottom" : "left top" },
			content: function() {
				return $(this).data("htmlTitle");
			}
		}));
		
		$target.click(function() {
			$(this).tooltip("enable").tooltip("open");
			
			return true;
		});
	},
	applyAJAX: function(target, options) {
		$$.warn("Unimplemented!");
	},
	show: function(target, callback) {
		var $target = $(target);
		
		if($$.isSet(callback))
			callback($target);
		
		try {
			if($$.UI.isTooltipped($target))
				$target.tooltip("open");
		} catch(E) {
			$$.Log.error(E);
		}
	},
	close: function(target, options) {
		var $target = $(target);
		
		if($$.UI.isTooltipped($target))
			$target.tooltip("close");
	},
	destroy: function(target, options) {
		var $target = $(target);
		
		if($$.UI.isTooltipped($target))
			$target.tooltip("destroy");
	},
	destroyAll: function(targets, options) {
		var $targets = $(targets);
		
		$targets.each(function() {
			var $target = $(this);
			
			if($$.UI.isTooltipped($target))
				$target.tooltip("destroy");
		});
	}
};

$$.Dep.require("vrmf.tooltip.manager.ui", { "vrmf.tooltip.manager": { identifier: "$$.TooltipManager" }, "jquery.ui.tooltip": { identifier: "$.ui.tooltip" } }, function() {
	$$.TooltipManager.setManagerID("UI");
});