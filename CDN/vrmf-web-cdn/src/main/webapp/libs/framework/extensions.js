$$.Dep.require("vrmf.extensions", { "vrmf.utils": { identifier: "$$.Utils" } });

Math.sgn = function(value) {
	  return value == 0 ? 0 : ( value < 0 ? -1 : 1 );
};

String.prototype.trim = function() {
    return this.replace(/^\s*/, "").replace(/\s*$/, "");
};

String.prototype.endsWith = function(tail) {
	if($$.isSet($$.rawTrim(tail))) {
		return this.lastIndexOf(tail) + tail.length == this.length;
	}
		
	return false;
};

String.prototype.startsWith = function(head) {
	if($$.isSet($$.rawTrim(head)))
		return this.indexOf(head) == 0;
		
	return false;
};

String.prototype.pad = function(left, padding, maxLength) {
	if(this.length > maxLength)
		return this.substring(0, maxLength);
	
	var toReturn = this;
	
	while(toReturn.length < maxLength)
		toReturn = (left ? padding + toReturn : toReturn + padding);
	
    return toReturn;
};

String.prototype.padLeft = function(padding, maxLength) {
    return this.pad(true, padding, maxLength);
};

String.prototype.padRight = function(padding, maxLength) {
    return this.pad(false, padding, maxLength);
};

String.prototype.emphasize = function(toEmphasize, wrapper) {
	var wrapperTag = $$.isSet(wrapper) ? wrapper : "em";
	return this.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + toEmphasize + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<" + wrapperTag + ">$1</" + wrapperTag + ">");
};

String.prototype.more = function(length, moreText) {
	var asString = this;
	var defaultLength = 60;
	var defaultMoreText = "...";
	
	if($$.isSet(length))
		defaultLength = length;
	
	if($$.isSet(moreText))
		defaultMoreText = moreText;
	
	if(this.length > defaultLength)
		return this.substring(0, defaultLength) + defaultMoreText;
	
	return asString;
};

String.prototype.hasWildcards = function() {
	return this.indexOf('%') >= 0 || this.indexOf('_') > 0;
};

Date.prototype.formatTime = function() {
	var hour = this.getHours();
	var minute = this.getMinutes();
	var second = this.getSeconds();
	var millis = this.getMilliseconds();
	
	return (hour < 10 ? "0" : "") + hour + ":" + 
		   (minute < 10 ? "0" : "") + minute + ":" +
		   (second < 10 ? "0" : "") + second + "," +
		   (millis < 10 ? "00" : ( millis < 100 ? "0" : "") ) + millis;
};

Date.prototype.formatDate = function() {
	var year = this.getFullYear();
	var month = this.getMonth() + 1;
	var day = this.getDate();
	
	return year + "-" + 
		   (month < 10 ? "0" : "") + month + "-" +
		   (day < 10 ? "0" : "") + day;
};

Date.prototype.format = function() {
	return this.formatDate() + " " + this.formatTime();
};

Date.prototype.daysBetween = function(otherDate) {
	if(otherDate == null)
		return NaN;
	
	var MS_IN_DAY = 1000 * 60 * 60 * 24;
	
	var thisTime = this.getTime();
	var otherTime = otherDate.getTime();
	
	var days = Math.floor(Math.abs(( otherTime - thisTime ) / MS_IN_DAY)); 
	
	if(thisTime < otherTime)
		days *= -1;
	
	return days; 
};

/**
 * Fixes a well-known IE 'feature'... :O (See: http://soledadpenades.com/2007/05/17/arrayindexof-in-internet-explorer/)
 */
if(!Array.prototype.indexOf) {
    Array.prototype.indexOf = function(obj){
        for(var i=0; i<this.length; i++){
            if(this[i] == obj) {
                return i;
            };
        }
        
        return -1;
    };
}