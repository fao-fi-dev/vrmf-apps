function Notification() {
	var $this = this;
	
	$this.NOTIFICATION_STACK_SEVERE = {"dir1": "down", "dir2": "right", "push": "top", "spacing1": 10, "spacing2": 0};
	$this.NOTIFICATION_SEVERE = null;
	
	$this.DEFAULT_OPTS = { 
		icon: true,
		closer: false,
		sticker: false,
		history: false,
		mouse_reset: false,
		delay: 3000,
		type: "notice",
		styling: "jqueryui"
	};

};

$$.Dep.require("vrmf.framework.notify", { "vrmf.ui": { identifier: "$$.UI" }, "pnotify": { identifier: "$.pnotify" } }, function() {
	$.extend($$, {
		Notification: new Notification()
	});	
});

Notification.prototype._notify = function(title, text, options, stack) {
	var opts = $.extend(true, {}, this.DEFAULT_OPTS, $$.isSet(options) ? options : {});
	
	if($$.isSet(title))
		opts.title = title;
	
	if($$.isSet(text))
		opts.text = text;
	
	if($$.isSet(stack))
		opts.stack = stack;
	
	return $.pnotify(opts);
};

Notification.prototype.notice = function(title, text, options) {
	var opts = $$.isSet(options) ? options : { };
	opts.type = 'notice';
	
	return this._notify(title, text, opts);
};

Notification.prototype.info = function(title, text, options) {
	var opts = $$.isSet(options) ? options : { };
	opts.type = 'info';
	
	return this._notify(title, text, opts);
};

Notification.prototype.error = function(title, text, options) {
	var opts = $$.isSet(options) ? options : { };
	opts.type = 'error';
	
	return this._notify(title, text, opts);
};

Notification.prototype.change = function(notification, type, title, text, options) {
	if($$.isSet(notification)) {
		type = $$.isSet(type) ? type : "notice";
		
		var opts = {
			animation: 'fade',
			hide: true,
			delay: 2000 
		};
		
		if($$.isSet(options)) {
			opts = options;
		}
		
		if($$.isSet(type)) {
			opts.type = type;
		}
		
		if($$.isSet(title)) {
			opts.title = title;
		}
		
		if($$.isSet(text)) {
			opts.text = text;
		}
				
		notification = notification.pnotify(opts);
	}
};

Notification.prototype.clear = function() {
	if($$.isSet(this.NOTIFICATION_SEVERE)) {
		this.NOTIFICATION_SEVERE.pnotify_remove();
		this.NOTIFICATION_SEVERE = null;
	}
	
	$.pnotify_remove_all();
};

Notification.prototype.severe = function(title, text, options) {
	var opts = $$.isSet(options) ? options : { };
	opts.type = "error";
	opts.cornerclass = "ui-pnotify-sharp";
	opts.addclass = "stack-bar-top";
	opts.width = "100%";
	opts.animation = "none";
	opts.hide = false;
	opts.closer = true;
	opts.closer_hover = false;
	
	if($$.isSet(this.NOTIFICATION_SEVERE)) {
		this.NOTIFICATION_SEVERE.pnotify_remove();
		this.NOTIFICATION_SEVERE = null;
	}
	
	this.NOTIFICATION_SEVERE = this._notify(title, text, opts, this.NOTIFICATION_STACK_SEVERE);
	return this.NOTIFICATION_SEVERE;
};