function Flags() {
	$$.Log.debug("Initializing 'Flags' Object...");

	this.EMPTY_IMG_URL = "empty.png";
	this.BASE_URL = "http://www.fao.org/countryprofiles/flags/";
	//This works only if accessed through FAO intranet...
	//this.BASE_URL = "http://www-data.fao.org/countryprofiles/flags/";
	this.IMG_TYPE = ".gif";
	this.IMG_CLASS = "flag";
	this.CASE = "upper";
};

Flags.prototype._getSanitizedBaseURL = function() {
	if($$.isSet(this.BASE_URL)) {
		return this.BASE_URL + 
			( $$.isSet(this.IMG_TYPE) ? ( this.BASE_URL.endsWith("/") ? "" : "/" ) : "" );
	}

	return null;
};

Flags.prototype.setBaseURL = function(baseURL) {
	if($$.isSet($$.rawTrim(baseURL)))
		this.BASE_URL = $$.rawTrim(baseURL);
};

Flags.prototype.setEmptyImgURL = function(emptyImgURL) {
	this.EMPTY_IMG_URL = emptyImgURL;
};

Flags.prototype.setImgType = function(imgType) {
	if($$.isSet($$.rawTrim(imgType)))
		this.IMG_TYPE = ( $$.rawTrim(imgType).startsWith(".") ? "" : "." ) + $$.rawTrim(imgType);
	else
		this.IMG_TYPE = null;
};

Flags.prototype.setCase = function(codeCase) {
	this.CASE = codeCase;
};

Flags.prototype.setImgClass = function(imgClass) {
	if($$.isSet($$.rawTrim(imgClass)))
		this.IMG_CLASS = $$.rawTrim(imgClass);
};

Flags.prototype.getImageWithFallbackByCountryID = function(countryId) {
	var image = $(this.getImage($$.Metadata.getCountryByID(countryId)));

	image.error(function() {
		$(this).replaceWith("<span>	" + $(this).attr("alt") + "</span>");
	});

	return image;
};

Flags.prototype.lazyGetImageWithFallbackByCountryID = function(countryId) {
	var image = $(this.lazyGetImage($$.Metadata.getCountryByID(countryId)));

	image.error(function() {
		$(this).replaceWith("<span>	" + $(this).attr("alt") + "</span>");
	});

	return image;
};

Flags.prototype.getImageByCountryID = function(countryId, additionalTooltipText) {
	return this.getImage($$.Metadata.COUNTRIES_MAP["cid_" + countryId], additionalTooltipText);
};

Flags.prototype.lazyGetImageByCountryID = function(countryId, additionalTooltipText) {
	return this.lazyGetImage($$.Metadata.COUNTRIES_MAP["cid_" + countryId], additionalTooltipText);
};

Flags.prototype.getImage = function(country, additionalTooltipText) {
	return this._getImage(country, false, additionalTooltipText);
};

Flags.prototype.lazyGetImage = function(country, additionalTooltipText) {
	return this._getImage(country, true, additionalTooltipText);
};

Flags.prototype._getImage = function(country, lazy, additionalTooltipText) {
	if($$.isSet(country) && $$.isSet(country.iso2Code)) {
		var code = country.iso2Code;

		if("upper" === this.CASE)
			code = code.toUpperCase();
		else if("lower" === this.CASE) 
			code = code.toLowerCase();

		return "<img " + ( lazy ? "data-original" : "src" ) + "='" + this._getSanitizedBaseURL() + code + ( $$.isSet(this.IMG_TYPE) ? this.IMG_TYPE : "" ) + "' class='" + this.IMG_CLASS + "' alt='" + country.iso2Code + "' title='" + country.name + ( $$.isSet(additionalTooltipText) ? ( " / " + additionalTooltipText ) : "" ) + "'/>";
	}

	return "<img src=\"" + this.EMPTY_IMG_URL + "\" class=\"" + this.IMG_CLASS + "\"/>";
};

Flags.prototype.handleMissingImage = function(image) {
	var $image = $(image);
	
	if($image.is("img")) {
		var src = $image.attr("src");
		
		if(!$$.isSet($image.data("alreadyHandled"))) {
			$$.Log.debug("Handling missing image (src: " + src + ")");
			$image.data("alreadyHandled", true);
			$image.attr("src", self.EMPTY_IMG_URL);
		} 
	} else {
		$$.Log.warn("Unable to handle missing image on an HTML element that is not a <IMG>...");
	}
};

Flags.prototype.getImageAndText = function(country, additionalTooltipText) {
	if($$.isSet(country)) {
		return this.getImage(country, additionalTooltipText) + 
			   "<span>" + ($$.isSet(country.iso2Code) ? country.iso2Code + " - " : "") + country.name + "</span>";
	} else {
		return "<span>&lt;UNKNOWN COUNTRY&gt;</span>";
	}
};

$$.Dep.require("vrmf.flag", { "vrmf.metadata": { identifier: "$$.Metadata" } }, function() {
	$.extend($$, {
		Flags: new Flags()
	});
});