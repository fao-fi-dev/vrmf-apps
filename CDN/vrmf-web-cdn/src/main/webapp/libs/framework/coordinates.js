function Coordinates() {
	$$.Log.debug("Initializing 'Coordinates' Object...");
};

Coordinates.prototype.convertToDecimalCoordinates = function(coordinate) {
	var signum = Math.sgn(coordinate);
	var integerPart = Math.floor(Math.abs(coordinate));
	var decimalPart = Math.abs(coordinate) - integerPart;
	
	return signum * ( integerPart + ( decimalPart * 100.0 / 60.0 ) );
};

Coordinates.prototype.convertFromDecimalCoordinates = function(coordinate) {
	var signum = Math.sgn(coordinate);
	var integerPart = Math.floor(Math.abs(coordinate));
	var decimalPart = Math.abs(coordinate) - integerPart;
	
	var roundedDecimal = Math.round(decimalPart * 60.0);
	
	if(roundedDecimal >= 60) {
		roundedDecimal -= 60;
		integerPart++;
	}
	
	return signum * ( integerPart + ( Math.round(roundedDecimal) / 100.0 ) );
};

Coordinates.prototype.getLatitudeAsString = function(latitude) {
	var signum = Math.sgn(latitude);
	var integerPart = Math.floor(Math.abs(latitude));
	var decimalPart = Math.abs(latitude) - integerPart;
	
	var mark = "N";
	
	if(signum < 0)
		mark = "S";
		
	return ("" + integerPart).padLeft("0", 2) + "." + ("" + Math.round(decimalPart * 100)).padLeft("0", 2) + mark;
};

Coordinates.prototype.getLongitudeAsString = function(longitude) {
	var signum = Math.sgn(longitude);
	var integerPart = Math.floor(Math.abs(longitude));
	var decimalPart = Math.abs(longitude) - integerPart;
	
	var mark = "W";
	
	if(signum >= 0)
		mark = "E";
		
	return ("" + integerPart).padLeft("0", 3) + "." + ("" + Math.round(decimalPart * 100)).padLeft("0", 2) + mark;
};

Coordinates.prototype.convertLat = function(gLatitude) {
	return this.getLatitudeAsString(this.convertFromDecimalCoordinates(gLatitude));
};

Coordinates.prototype.convertLon = function(gLongitude) {
	return this.getLongitudeAsString(this.convertFromDecimalCoordinates(gLongitude));
};

$$.Dep.require("vrmf.coordinates", { "vrmf.log": { identifier: "$$.Log" } }, function() {
	$.extend($$, {
		Coordinates: new Coordinates()
	});
});