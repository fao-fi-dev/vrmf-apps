$$.Interface.UI_DIALOG = {
	defaults: {
		modal: true,
		resizable: false,
		stack: true,  
		width: '512px',
		zIndex: 9999,
		open: function(event, ui) {
			$('.ui-widget-overlay').each(function() {
				var $this = $(this);
				
				if(!$this.hasClass('blockUI')) {
					$this.css('width', $(window).width());
					$this.css('height', $(window).height());
				}
			});
			
			$(this).dialog("open");
		}, 
	    close: function(event, ui) {
	    	$('.ui-widget-overlay').each(function() {
				var $this = $(this);
				
				if(!$this.hasClass('blockUI')) {
					$this.css('width', 'auto');
					$this.css('height', 'auto');
				}
			});

			$(this).dialog("close");
		} 
	}
};

$$.Interface.UI_AUTOCOMPLETE = {
	defaults: {
		html: true,
		delay: 800,
		minLength: 2,
		zIndex: 10000
	},
	ajax: {
	}	
};

$$.Interface.UI_AUTOCOMPLETE.ajax = $.extend(true, {}, $$.Interface.UI_AUTOCOMPLETE.defaults, {
	async: true,
	dataType: "json"
});

$$.Interface.UI_MULTISELECT = {
	defaults: {
		minWidth: "auto",
		height: 250,
		selectedList: 3,	
		position: {  
			my: 'left top',
		    at: 'left top'
		},
		selectedText: function(numChecked, numTotal, checkedItems) {
			return $(checkedItems).map(function(){ return this.title; }).get().join("<br/>");		
		},
		beforeclose: function(event, ui) {
			//updateFiltersReport();
		}
	},
	simple: {
		//To be initialized	
	},
	single: {
		//To be initialized
	}
};

$$.Interface.UI_MULTISELECT.simple = $.extend(true, {}, $$.Interface.UI_MULTISELECT.defaults, {
	selectedText: function(numChecked, numTotal, checkedItems) {
		return $(checkedItems).map(function(){ return this.value; }).get().join(", ");		
	}
});

$$.Interface.UI_MULTISELECT.single = $.extend(true, {}, $$.Interface.UI_MULTISELECT.defaults, {
	height: "auto",
	multiple: false,
	header: false,
	selectedList: 1,
	classes: "singleSelect",
	click: function() {
		$(this).multiselect("close");
	}
});

Interface.prototype._getRealContent = function(content) {
	if(typeof content === 'string')
		return $("<p class='moreLeftPad moreRightPad'/>").text(content);
		
	return $("<p class='moreLeftPad moreRightPad'/>").append(content);
};

Interface.prototype.showDialog = function(title, content, options) {
	var _dialog = $("<b/>");
					
	if($.isArray(content)) {
		for(var l=0; l<content.length; l++) {
			_dialog.append(this._getRealContent(content[l]));
		}
	} else
		_dialog.append(this._getRealContent(content));

	_dialog.wrap("<div/>");
	
	var _buttons = {};
	_buttons[$$.I18n.getText("vrmf.common.interface.ui.dialog.button.ok")] = function() { 
		$(this).dialog("destroy"); $(this).remove(); 
	};
	
	_dialog.dialog($.extend( $$.isSet(options) ? options : {} , $$.Interface.UI_DIALOG.defaults, {
		title: title,
		buttons: _buttons
	}));
};

$$.Dep.require("vrmf.interface.extensions.jquery.ui", { "jquery.ui.dialog": { identifier: "$.ui.dialog" },
														"vrmf.interface": { identifier: "$$.Interface" } }, function() {
	window.confirm = function(content, onconfirm, oncancel) {
		var _confirm = null;
		
		if(typeof content == "string") {
			_confirm = $("<div/>");
			_confirm.append($("<b/>").append(content));
		} else {
			_confirm = content;
		}
		
		var _buttons = {};
		_buttons[$$.I18n.getText("vrmf.common.interface.ui.confirm.button.confirm")] = function() {
			$(this).dialog("destroy");
			onconfirm();
			$(this).remove();
		};
		
		_buttons[$$.I18n.getText("vrmf.common.interface.ui.confirm.button.cancel")] = function() {
			$(this).dialog("destroy");
			oncancel();
			$(this).remove();
		};
		
		_confirm.dialog($.extend({ dialogClass: 'confirmation' }, $$.Interface.UI_DIALOG.defaults, {
			title: $$.I18n.getText("vrmf.common.interface.ui.confirm.title"),
			buttons: _buttons
		}));
	};
});