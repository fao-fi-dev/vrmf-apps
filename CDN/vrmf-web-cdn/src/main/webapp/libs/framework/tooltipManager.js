function TooltipManager() {
	var $t = this;
	
	this.EFFECTIVE_MANAGER_ID = "DEFAULT";
	this.TOOLTIP_CONFIGURATION_ATTRIBUTE = "vrmf-tooltip-configuration";
	this.TOOLTIP_MARKER_CLASS = "vrmf-tooltipped";
	
	this.initialize();
	
	this.MANAGERS = { 
		DEFAULT: {
			options: { },
			additionalClasses: [ 'hint' ],
			initialize: function(options) {
			},
			applyStatic: function(target, options) {
				//DO NOTHING
			},
			applyHTML: function(target, options) {
				var title = $(target).attr("title");
				var isHTML = title.match(/^\<html\>.*\<\/html\>/g) != null;
				
				if(isHTML && $$.isSet(title)) {
					try {
						var html = $(title).html();
						html = html.replace(/\<\/div\>/g, '</div>\n');
												
						$(target).attr("title", $(html).text());
					} catch (E) { 
						$$.Log.warn(E);
					}
				}
			},
			applyAJAX: function(target, options) {
				$$.warn("Unimplemented!");
			},
			show: function(target, callback) {
				
			},
			close: function(target) {
			},
			destroy: function(target) {
			},
			destroyAll: function(targets) {
			}
		}
	};
};

TooltipManager.prototype.initialize = function() {
	var vertical = [ "top", "center", "bottom" ];
	var horizontal = [ "left", "center", "right" ];
	
	for(var v=0; v<vertical.length; v++)
		for(var h=0; h<horizontal.length; h++) {
			TooltipManager.prototype["AT_" + 
			                         horizontal[h].toUpperCase() + "_" +
			                         vertical[v].toUpperCase() + "_DEFAULTS"] = { position: { at: horizontal[h] + " " + vertical[v] } };
			
			TooltipManager.prototype["MY_" + 
			                         horizontal[h].toUpperCase() + "_" +
			                         vertical[v].toUpperCase() + "_DEFAULTS"] = { position: { my: horizontal[h] + " " + vertical[v] } };
			
		}
	
		for(var h=0; h<horizontal.length; h++) 
			for(var v=0; v<vertical.length; v++) {
			TooltipManager.prototype["AT_" + 
			                         vertical[v].toUpperCase() + "_" +
			                         horizontal[h].toUpperCase() + "_DEFAULTS"] = { position: { at: horizontal[h] + " " + vertical[v] } };
			
			TooltipManager.prototype["MY_" + 
			                         vertical[v].toUpperCase() + "_" +
			                         horizontal[h].toUpperCase() + "_DEFAULTS"] = { position: { my: horizontal[h] + " " + vertical[v] } };
		} 
	
};

TooltipManager.prototype.setManagerID = function(managerID) {
	this.EFFECTIVE_MANAGER_ID = managerID;
};

TooltipManager.prototype.getManagerID = function() {
	return this.EFFECTIVE_MANAGER_ID;
};

TooltipManager.prototype.getManager = function() {
	if($$.isSet(this.MANAGERS[this.EFFECTIVE_MANAGER_ID]))
		return this.MANAGERS[this.EFFECTIVE_MANAGER_ID];
	
	$$.Log.warn("Unknown manager ID '" + this.EFFECTIVE_MANAGER_ID + "': returning default effective tooltip manager...");
	
	return this.MANAGERS.DEFAULT;
};

TooltipManager.prototype.getConfiguration = function(target) {
	var configuration = {};

	var metadata = target.metadata();
	var attribute = target.attr($$.TooltipManager.TOOLTIP_CONFIGURATION_ATTRIBUTE);
	
	if($$.isSet(attribute))
		try {
			attribute = JSON.parse(attribute);
			
			configuration = $.extend(true, {}, configuration, attribute);
		} catch(E) {
			$$.Log.warn("Unable to parse metadata from attribute '" + $$.TooltipManager.TOOLTIP_CONFIGURATION_ATTRIBUTE + "' on target " + target + ". " + E);
		}

	if($$.isSet(metadata) && $$.isSet(metadata.position)) {
		configuration = $.extend(true, {}, configuration, { position: metadata.position });
	}
	
	return configuration;
};

TooltipManager.prototype.destroy = function(target) {
	var manager = this.getManager();
	
	if($$.isSet(target)) {
		var $target = $(target);
		
		if($target.size() > 1)
			$$.Log.warn("Attempting to destroy multiple targets (" + $target.size() + ") with TooltipManager.destroy...");
		else
			manager.destroy($target.removeClass(manager.TOOLTIP_MARKER_CLASS));
	}
};

TooltipManager.prototype.destroyAll = function(targets) {
	var manager = this.getManager();
	
	var targets = null;
	
	if(!$$.isSet(target))
		targets = $("." + this.TOOLTIP_MARKER_CLASS);
	else
		targets = $("." + this.TOOLTIP_MARKER_CLASS, target);
	
	$(targets).each(function() {
		manager.destroy($(this));
	});
};

TooltipManager.prototype.close = function(container) {
	var manager = this.getManager();
	
	var targets = null;
	
	if(!$$.isSet(container))
		targets = $("." + this.TOOLTIP_MARKER_CLASS);
	else
		targets = $("." + this.TOOLTIP_MARKER_CLASS, container);
	
	$(targets).each(function() {
		manager.close($(this));
	});
};

TooltipManager.prototype.apply = function(targets, options, force) {
	if(typeof targets == 'undefined')
		targets = $("[title]");
	
	var $t = this;
	var manager = this.getManager();
	
	var end, start = new Date().getTime();
	
	var isOptionsBuilder = $$.isSet(options) && $.isFunction(options);
	
	var overallAlreadyApplied = true;
	
	$(targets).each(function() {
		var target = $(this);
				
		var realOptions = isOptionsBuilder ? options(target) : options;
			
		var alreadyApplied = target.hasClass($t.TOOLTIP_MARKER_CLASS);

		if($$.UI.isTooltipped(target))
			$t.destroy(target);
		
		alreadyApplied = $$.isSet(force) && 
						 force === false &&  
						 $$.isSet(alreadyApplied) && 
						 alreadyApplied === true;
		
		overallAlreadyApplied &= alreadyApplied;
		
		if(!alreadyApplied) {
			var additionalClasses = manager.additionalClasses;
			
			if($$.isSet(additionalClasses))
				if($$.isArray(additionalClasses))
					target.addClass(additionalClasses.join(" "));
				else
					target.addClass(additionalClasses);
			
			if($t._isHTML(target))
				manager.applyHTML(target, realOptions);
			else if($t._isAJAX(target))
				manager.applyAJAX(target, realOptions);
			else
				manager.applyStatic(target, realOptions);
			
			target.addClass($t.TOOLTIP_MARKER_CLASS);
		} else {
			$$.Log.warn("An attempt was caught to apply tooltip configuration on an item that was already configured!");
		}
	});
	
	end = new Date().getTime();
	
	var elems = targets.size();
	
	if(elems == 1)
		$$.Log.debug("The tooltip manager took " + ( end - start ) + " mSec. to complete on one element");
	else
		$$.Log.debug("The tooltip manager took " + ( end - start ) + " mSec. to complete on " + elems + " elements");
	
	return overallAlreadyApplied;
};

TooltipManager.prototype.show = function(target) {
	var manager = this.getManager();
	
	manager.show($(target));
};

TooltipManager.prototype._isHTML = function(target) {
	var $target = $(target);
	
	var isHTML = $target.data("isHTML");
	
	if($$.isSet(isHTML))
		return isHTML;
	
	var title = $(target).attr("title");
	isHTML = $$.isSet(title) && title.match(/^\<html\>.*\<\/html\>/g) != null;
	
	$target.data("isHTML", isHTML);
	
	return isHTML;
};

TooltipManager.prototype._isAJAX = function(target) {
	return $$.isSet(target.data("ajaxTooltip"));
};


$$.Dep.require("vrmf.tooltip.manager", { "vrmf.log": { identifier: "$$.Log" } }, function() {
	$.extend(true, $$, {
		TooltipManager: new TooltipManager()
	});
});