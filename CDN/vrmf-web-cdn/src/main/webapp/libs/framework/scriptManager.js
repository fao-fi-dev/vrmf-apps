function ScriptManager() {
	
};

ScriptManager.prototype.include = function(id, url, callback) {
	this._include(id, url, false, callback);
};

ScriptManager.prototype.includeAsFirst = function(id, url, callback) {
	this._include(id, url, true, callback);
};

ScriptManager.prototype._include = function(id, url, asFirst, callback) {
	var script = $("<script type=\"text/javascript\"></script>");
	
	if($$.isSet(id)) {
		script.attr("id", id);
	}
	
	script.attr("src", url);
	script.attr("async", "true");

	var scriptDetails = "at " + url + ( $$.isSet(id) ? " [ id: " + id + " ] " : "" );
	
	if($$.isSet(callback))
		script.ready(function() {
			$$.Log.info("Script " + scriptDetails + " has been successfully loaded. Invoking callback (if any)...");
			
			callback(); 
		});
	
	$$.Log.info("Including script " + scriptDetails + ( asFirst ? " at <head/> start" : "" ));
	
	if(asFirst)
		$("head").prepend(script);
	else
		$("head").append(script);
};

$.extend($$, {
	ScriptManager: new ScriptManager()
});