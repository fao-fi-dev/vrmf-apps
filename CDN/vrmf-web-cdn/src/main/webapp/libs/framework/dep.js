function Dep() {
	if(!this._isAvailable(window.$$))
		window.$$ = {};
	
	window.$$.Dep = this;
};

Dep.prototype._isAvailable = function(what) {
	return typeof what !== "undefined" && what != null;
};

Dep.prototype._isArray = function(what) {
	return typeof what !== "undefined" && what != null;
};

Dep.prototype._emit = function(text) {
	var paddedMsg = "JS Dep Check: " + text;
	
	if(this._isAvailable(window.console))
		window.console.log(paddedMsg);
	
	return paddedMsg;
};

Dep.prototype._warn = function(text) {
	try {
		alert(this._emit(text));
	} catch (E) { 
		this._emit("Unable to invoke 'alert': " + E);
		
		if(this._isAvailable(window._alert)) {
			try {
				window._alert(this._emit(text));
			} catch (E2) {
				this._emit("Unable to invoke original window.alert: " + E2);
				
				document.getElementsByName("body")[0].innerHtml = text;
			}
		}	
	}
};

Dep.prototype.require = function(requiringComponent, deps, onAvailableCallback, onUnavailableCallback) {
	var check = available = true;
	
	var key = dep = id = optional = null;
	
	var unavailableDeps = {};
	
	for(key in deps) {
		dep = deps[key];
		
		id = dep.identifier;
		optional = this._isAvailable(dep.optional) && dep.optional;
		
		if(this._isAvailable(id)) {
			try {
				available = this._isAvailable(typeof id === "string" ? eval(id) : id);
				
				check &= ( optional || available );
			} catch(E) {
				available = false;
				
				this._emit("Component: '" + requiringComponent + "' -> Unable to check " + ( optional ? "optional" : "mandatory" ) + " dependency " +
						   "'" + key + "' identified by '" + id + "': " + E);
				
				check &= optional;
			}
			
			this._emit("Component: '" + requiringComponent + "' -> " + (optional ? "Optional" : "Mandatory" ) + " dependency '" + key + "' (identified by '" + id + "') is " + ( available ? "" : "not " ) + "available");
		} else {
			this._warn("Component: '" + requiringComponent + "' -> Dependency identifier is not set for dependency '" + key + "'");
		}
		
		if(!available && !optional)
			unavailableDeps[key] = dep;
		
		if(!check)
			break;
	}
	
	if(check) {
		if(this._isAvailable(onAvailableCallback))
			try {
				onAvailableCallback();
			} catch (E) {
				this._warn("Unable to invoke onAvailableCallback for '" + requiringComponent + "': " + E);
			}
	} else {
		if(this._isAvailable(onUnavailableCallback))
			try {
				onUnavailableCallback();
			} catch (E) {
				this._warn("Unable to invoke onUnavailableCallback for '" + requiringComponent + "': " + E);
			}
		else {
			var uDep;
			var text = "Component '" + requiringComponent + "' has unsatisfied dependencies:\n\n";
			
			for(var key in unavailableDeps) {
				uDep = unavailableDeps[key];
				
				text += " !!! Mandatory dependency '" + key + "' (identified by '" + uDep.identifier + "') is  not available.\n";
			}
			
			this._warn(text);
		}
	}
};

new Dep();