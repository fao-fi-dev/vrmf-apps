function Set(initialArray) {
	this.array = new Array();
	
	if($$.isSet(initialArray) && $$.isArray(initialArray)) {
		for(var a=0; a<initialArray.length; a++)
			this.add(initialArray[a]);
	}
}

Set.prototype.indexOf = function(what) {
	for(var i=0; i<this.array.length; i++)
		if($$.Utils.areEqual(what, this.array[i]))
			return i;
	
	return -1;
};

Set.prototype.contains = function(what) {
	return this.indexOf(what) >= 0;
};

Set.prototype.fromArray = function(array) {	
	var toReturn = new Set();
	
	if($$.isSet(array)) {
		for(var i=0; i<array.length; i++)
			toReturn.add(array[i]);
	}
	
	return toReturn;
};

Set.prototype.asArray = function() {
	return this.array;
};

Set.prototype.add = function(element) {
	var already = this.indexOf(element) >= 0;
	
	if(!already)
		this.array.push(element);
	
	return !already;
};

Set.prototype.put = Set.prototype.add;
Set.prototype.push = Set.prototype.add;

Set.prototype.addAll = function(collection) {
	if($$.isSet(collection) && $$.isArray(collection)) {
		for(var c=0; c<collection.length; c++)
			this.add(collection[c]);
	}
};

Set.prototype.remove = function(element) {
	var index = this.indexOf(element);
	
	if(index >= 0)
		return this.array.splice(index, 1);
	
	return null;
};

Set.prototype.get = function(index) {
	return this.array[index];
};

Set.prototype.size = function() {
	return this.array.length;
};

Set.prototype.isEmpty = function() {
	return this.size() == 0;
};

Set.prototype.toArray = function() {
	return this.array;
};

Set.prototype.sort = function(sorterFunction) {
	return this.fromArray(this.array.sort(sorterFunction));
};

function Utils() {
};

Utils.prototype.isObject = function(what) {
	return $$.isSet(what) && 
		   typeof what == "object";
};

Utils.prototype.isArray = function(what) {
	return this.isObject(what) &&  
		   $$.isSet(what.length);		   
};

Utils.prototype.isEmpty = function(what) {
	if($$.isObject(what)) {
		if($$.isSet(Object.keys))
			return Object.keys(what).length == 0;
		
		var keys = 0;
		
		for(var key in what)
			if($$.isSet(key))
				keys++;
			
		return keys == 0;
	} 
	
	return !$$.isSet(what);
};

Utils.prototype.isNotEmpty = function(what) {
	return !$$.isEmpty(what);
};

Utils.prototype.isDefined = function(what) {
	return typeof what != undefined;
};

Utils.prototype.isSet = function(what) {
	return typeof what != undefined && ( typeof what == "function" || ( what != null && what !== "" && ( !this.isSet(what.length) || what.length > 0 ) ) );
};

Utils.prototype.rawTrim = function(what) {
	if(!this.isSet(what))
		return null;

	return typeof what == undefined ? null : ( typeof what == "string" ? ( what.trim() == "" ? null : what.trim() ) : ( typeof what == "object" && this.isSet(what.length)) ? (what.length == 0 ? null : what) : what );
};

Utils.prototype.getAnchor = function(link) {
	var href = this.rawTrim($(link).attr("href"));
	var anchor = null;

	if(href != null) {
		var index = href.indexOf("#");
		
		if(index >= 0)
			anchor = href.substring(index + 1);
	}
	
	return anchor;
};

Utils.prototype.isChecked = function(checkbox) {
	var attr = checkbox.prop("checked");
	
	return $$.isSet(attr) && ( "checked" === attr || "true" === attr || true === attr); 
};

Utils.prototype.check = function(checkbox) {
	checkbox.prop("checked", "checked");
	
	return checkbox;
};

Utils.prototype.uncheck = function(checkbox) {
	checkbox.removeProp("checked");
	
	return checkbox;
};

Utils.prototype.toggleCheck = function(checkbox) {
	if(this.isChecked(checkbox))
		this.uncheck(checkbox);
	else
		this.check(checkbox);
	
	return checkbox;
};

Utils.prototype.setChecked = function(checkbox, checked) {
	if(checked)
		this.check(checkbox);
	else
		this.uncheck(checkbox);
};

Utils.prototype.setEnabled = function(input, enabled) {
	if(enabled) {
		this.enable(input);
	} else {
		this.disable(input);
	}
	
	return input;
};

Utils.prototype.getSelected = function(container) {
	return $("option:selected", $(container));
};

Utils.prototype.select = function(option) {
	option.prop("selected", "selected");
};

Utils.prototype.deselect = function(option) {
	option.removeProp("selected");
};

Utils.prototype.setSelected = function(option, selected) {
	if(selected)
		this.select(option);
	else
		this.deselect(option);
};

Utils.prototype.isDisabled = function(input) {
	var disabled = input.attr("disabled");
	
	return disabled != null && ( disabled === "disabled" || disabled === "true" || disabled === true );
};

Utils.prototype.isEnabled = function(input) {
	var disabled = input.attr("disabled");
	
	return disabled == null || ( disabled != "disabled" && disabled != "true" && !disabled );
};

Utils.prototype.disable = function(input) {	
	if(this.isSet(input))
		input.attr("disabled", "disabled");
	
	try {
		//jquery UI button
		input.button("disable");
	} catch (E) {
		try {
			//jquery UI multiselect
			input.multiselect("disable");
		} catch (EE) {
			try {
				//jqyery UI datepicker
				input.datepicker("disable");
			}  catch (EEE) {
				;
			}
		}
	}
	
	return input;
};

Utils.prototype.enable = function(input) {
	if(this.isSet(input))
		input.removeAttr("disabled");
	
	try {
		//jquery UI button
		input.button("enable");
	} catch (E) {
		try {
			//jquery UI multiselect
			input.multiselect("enable");
		} catch (EE) {
			try {
				//jqyery UI datepicker
				input.datepicker("enable");
			}  catch (EEE) {
				;
			}
		}
	}
	
	return input;
};

Utils.prototype.isReadOnly = function(input) {
	var readonly = input.attr("readonly");
	
	return readonly != null && readonly;
};

Utils.prototype.isNotReadOnly = function(input) {
	return !this.isReadOnly(input);
};

Utils.prototype.readOnly = function(input) {
	if(this.isSet(input))
		input.attr("readonly", "readonly");
	
	return input;
};

Utils.prototype.notReadOnly = function(input) {
	if(this.isSet(input))
		input.removeAttr("readonly");
	
	return input;
};

Utils.prototype.setReadOnly = function(input, readonly) {
	if(this.isSet(input)) {
		if(readonly)
			this.readOnly(input);
		else
			this.notReadOnly(input);
	}
	
	return input;
};

Utils.prototype.setNotReadonly = function(input) {
	if(this.isSet(input))
		input.removeAttr("readonly");
	
	return input;
};

Utils.prototype.areEqual = function(first, second) {	
	//BOTH NULL CHECK
	var bothNull = first == null && second == null;
	
	if(bothNull)
		return true;

	//BOTH UNSET CHECK	
	var bothUnset = !this.isSet(first) && !this.isSet(second);
	
	if(bothUnset)
		return true;
	
	//BOTH SET CHECK	
	var bothSet = this.isSet(first) && this.isSet(second);
	
	if(!bothSet)
		return false;
	
	//SAME TYPE CHECK	
	var sameType = typeof first == typeof second;
	
	if(!sameType)
		return false;
	
	//PRIMITIVE TYPES CHECK	
	if(typeof first == 'string' || typeof first == 'number' || typeof first == 'boolean')
		return first == second;

	//ARRAY CHECK
	if(this.isArray(first)) {
		var equal = first.length == second.length;
		
		if(!equal)
			return false;
		
		for(var i=0; i<first.length; i++) {
			equal &= this.areEqual(first[i], second[i]);
		}
		
		return equal;
	}

	//OBJECT CHECK
	var equal = true;

	//FIRST AGAINST SECOND
	for(var k in first) {
		equal &= this.areEqual(first[k], second[k]); 
	}

	//SECOND AGAINST FIRST
	for(var k in second) {
		equal &= this.areEqual(first[k], second[k]); 
	}
		
	return equal;
};

Utils.prototype.sanitizeEMailLink = function(targets) {
	if($$.isSet(targets)) {
		$(targets).each(function() {
			var $this = $(this);
			
			if($this.is("[href]")) {
				$this.attr("href", $this.attr("href").replace(/\s?\(at\)\s?/g, "@").replace(/\s?\(dot\)\s?/g, "."));
			}
		});
	}
};

$$.Dep.require("vrmf.utils", { "jquery": { identifier: "$" }, "vrmf": { identifier: "$$.vrmf" } }, function() {
	$.extend(true, $$, {
		Utils: new Utils()
	}, Utils.prototype, { Set: Set });
});