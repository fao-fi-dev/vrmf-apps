$$.TooltipManager.MANAGERS.QTIP = {
	options: { 
		style: {
			classes: "ui-tooltip-rounded" + ( $$.Browser.msie ? "" : " ui-tooltip-shadow" ),
			widget: true
		},
		position: {
			viewport: $(window),
			adjust: {
				method: 'shift flip'
			}
		},
		show: {
			effect: false,
			delay: 0
		},
		hide: {
			effect: false,
			delay: 0
		}
	},
	additionalClasses: [ 'hint' ],
	initialize: function(options) {
		
	},
	applyStatic: function(target, options) {
		if(!$$.Browser.msie) {
			var extendedOptions = $.extend(true, {}, $$.TooltipManager.MANAGERS.QTIP.options, options);
			
			var metadata = target.attr($$.TooltipManager.TOOLTIP_CONFIGURATION_ATTRIBUTE);
			
			try {
				if($$.isSet(metadata))
					metadata = JSON.parse(metadata);
			} catch(E) {
				$$.Log.warn("Unable to parse metadata from attribute '" + $$.TooltipManager.TOOLTIP_CONFIGURATION_ATTRIBUTE + "' on target " + target + ". " + E);
			}
			
			if($$.isSet(metadata)) {
				extendedOptions = $.extend(true, {}, extendedOptions, metadata);

				if($$.isSet(metadata.my)) {
					extendedOptions = $.extend(true, {}, extendedOptions, eval("$$.TooltipManager.MY_" + metadata.my + "_DEFAULTS"));
				}

				if($$.isSet(metadata.at)) {
					extendedOptions = $.extend(true, {}, extendedOptions, eval("$$.TooltipManager.AT_" + metadata.at + "_DEFAULTS"));
				}
			}
			
			$(target).addClass($$.TooltipManager.TOOLTIP_MARKER_CLASS);
			$(target).qtip("destroy").qtip($.extend(true, extendedOptions, {
				content: {
					text: function(api) {
						return $(target).attr("title");
					}
				}
			}));
		}
	},
	applyHTML: function(target, options) {
		var extendedOptions = $.extend(true, {}, $$.TooltipManager.MANAGERS.QTIP.options, options);

		var metadata = target.attr($$.TooltipManager.TOOLTIP_CONFIGURATION_ATTRIBUTE);
		
		try {
			if($$.isSet(metadata))
				metadata = JSON.parse(metadata);
		} catch(E) {
			$$.Log.warn("Unable to parse metadata from attribute '" + $$.TooltipManager.TOOLTIP_CONFIGURATION_ATTRIBUTE + "' on target " + target + ". " + E);
		}
		
		if($$.isSet(metadata)) {
			extendedOptions = $.extend(true, {}, extendedOptions, metadata);
			
			if($$.isSet(metadata.my)) {
				extendedOptions = $.extend(true, {}, extendedOptions, eval("$$.TooltipManager.MY_" + metadata.my + "_DEFAULTS"));
			}

			if($$.isSet(metadata.at)) {
				extendedOptions = $.extend(true, {}, extendedOptions, eval("$$.TooltipManager.AT_" + metadata.at + "_DEFAULTS"));
			}
		}

		$(target).addClass($$.TooltipManager.TOOLTIP_MARKER_CLASS);
		$(target).qtip("destroy").qtip($.extend(true, extendedOptions, {
			content: {
				text: function(api) {
					return $($(target).attr("title"));
				}
			}
		}, $(target).is("a") ? { show: { event: 'click' }, hide: { event: 'unfocus' } } : {} ));
	},
	applyAJAX: function(target, options) {
		$$.warn("Unimplemented!");
	},
	show: function(target, callback) {
		var $target = $(target);
		
		if($$.isSet(callback))
			callback($target);
		
		$target.qtip("show");
	},
	close: function(target, options) {
		var $target = $(target);
		
		$target.qtip("hide");
	},
	destroy: function(target, options) {
		var $target = $(target);
		
		$target.qtip("destroy");
	}
};

$$.Dep.require("vrmf.tooltip.manager.qtip", { "vrmf.tooltip.manager": { identifier: "$$.TooltipManager" }, "jquery.qtip": { identifier: "$.qtip" } }, function() {
	$$.TooltipManager.setManagerID("QTIP");
});