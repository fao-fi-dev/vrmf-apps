function UserRole() {
	this.role = null;
	this.capabilities = new $$.Set();	
};

UserRole.prototype.getRole = function() {
	return this.role;
};

UserRole.prototype.setRole = function(role) {
	this.role = role;
};

UserRole.prototype.setCapabilities = function(capabilities) {
	this.capabilities = capabilities;
};

UserRole.prototype.getCapabilities = function() {
	return this.capabilities;
};

UserRole.prototype.getUniqueCapabilities = function() {
	return new $$.Set(this.capabilities);
};

function UserCapability() {
	this.capabilityName = null;
	this.capabilityParameters = null;
};

UserCapability.prototype.getName = function() {
	return this.capabilityName;
};

UserCapability.prototype.setName = function(capabilityName) {
	this.capabilityName = capabilityName;
};

UserCapability.prototype.setCapabilityParameters = function(capabilityParameters) {
	this.capabilityParameters = capabilityParameters;
};

UserCapability.prototype.getCapabilityParameters = function() {
	return this.capabilityParameters;
};

function UserModel(id, roles, capabilities, managedCountries, managedSources) {
	this.id = $$.isSet(id) ? id : null;
	this.roles = new $$.Set();
	this.capabilities = new $$.Set();
	this.managedCountries = new $$.Set();
	this.managedSources = new $$.Set();
	
	if($$.isSet(roles)) {
		if($$.isArray(roles)) {
			this.roles = this.roles.fromArray(roles);
		} else
			this.roles = roles;
	}
	
	if($$.isSet(capabilities)) {
		if($$.isArray(capabilities)) {
			this.capabilities = this.capabilities.fromArray(capabilities);
		} else
			this.capabilities = capabilities;
	}
	
	if($$.isSet(managedCountries)) {
		if($$.isArray(managedCountries)) {
			this.managedCountries = this.managedCountries.fromArray(managedCountries);
		} else
			this.managedCountries = managedCountries;
	}
	
	if($$.isSet(managedSources)) {
		if($$.isArray(managedSources)) {
			this.managedSources = this.managedSources.fromArray(managedSources);
		} else
			this.managedSources = managedSources;
	}
	
	this.cachedCapabilitiesCheck = { };
	this.cachedRolesCheck = { };
};

UserModel.prototype.getId = function() {
	return this.id;
};

UserModel.prototype.setId = function(id) {
	return this.id = id;
};

UserModel.prototype.addRole = function(role) {
	if($$.isSet(role)) {
		$$.Log.debug("Adding user role " + role.role);
		
		this.roles.add(role);
	
		if($$.isSet(role.capabilities)) {
			for(var c=0; c<role.capabilities.length; c++)
				this.addCapability(role.capabilities[c]);
		}
	} else {
		$$.Log.warn("Cannot add an undefined role");
	} 
};

UserModel.prototype.addRoles = function(roles) {
	if($$.isSet(roles) && $$.isArray(roles)) {
		for(var r=0; r<roles.length; r++)
			this.addRole(roles[r]);
	} else {
		$$.Log.warn("Cannot add an undefined set of roles");
	}
};

UserModel.prototype.addCapability = function(capability) {
	if($$.isSet(capability)) {
		$$.Log.debug("Adding capability " + capability.capabilityName + " {" + ( $$.isSet(capability.capabilityParameters) ? capability.capabilityParameters : "" ) + "}");
		
		this.capabilities.add(capability);
	} else {
		$$.Log.warn("Cannot add an undefined capability");
	}
};

UserModel.prototype.addCapabilities = function(capabilities) {
	if($$.isSet(capabilities) && $$.isArray(capabilities)) {
		for(var c=0; c<capabilities.length; c++)
			this.addCapability(capabilities[c]);
	} else {
		$$.Log.warn("Cannot add an undefined set of capabilities");
	}
};

UserModel.prototype.is = function(/** as many comma-separated role IDs as you want */) {
	if(arguments.length > 0) {
		var is = true;
	
		for(var r=0; r<arguments.length && is; r++) {
			if($$.isSet(this.cachedRolesCheck[arguments[r]])) {
				is &= this.cachedRolesCheck[arguments[r]];
				continue;
			}
			
			this.cachedRolesCheck[arguments[r]] = this._is(arguments[r]); 
			
			is &= this.cachedRolesCheck[arguments[r]];
		}
			
		return is;
	} else {
		$$.Log.warn("Cannot check roles against an empty role list");
		
		return false;
	}
};

UserModel.prototype._is = function(role) {
	if($$.isSet(role)) {
		var is = false;
	
		var roles = this.roles.toArray();
			
		for(var r=0; r<roles.length && !is; r++) {
			if(typeof roles[r] == "string")
				is |= ( this._check(this._toPattern(role), roles[r]) || this._check(this._toPattern(roles[r]), role) );
			else if(typeof roles[r] == "object" && $$.isSet(roles[r].role))
				is |= ( this._check(this._toPattern(role), roles[r].role) || this._check(this._toPattern(roles[r].role), role) );
		}
		
		$$.Log.debug("User is" + ( is ? "" : " not" ) + " " + role);

		return is;
	} else {
		$$.Log.warn("Cannot check roles against an undefined role ID");
		
		return false;
	}
};

UserModel.prototype.can = function(/** as many comma-separated capability string patterns as you want */) {
	if(arguments.length > 0) {
		var can = true;

		var filteredCapabilities = null;
		
		for(var c=0; c<arguments.length && can; c++) {
			if($$.isSet(this.cachedCapabilitiesCheck[arguments[c]])) {
				can &= this.cachedCapabilitiesCheck[arguments[c]];
				continue;
			}
		
			filteredCapabilities = this.filterCapabilities(arguments[c]);
			
			this.cachedCapabilitiesCheck[arguments[c]] = !filteredCapabilities.isEmpty();
			
			can &= this.cachedCapabilitiesCheck[arguments[c]];
			
			$$.Log.debug("User can" + ( can ? "" : "not" ) + " " + arguments[c]);
		}
					
		return can;
	} else {
		$$.Log.warn("Cannot check capabilities against an empty capability list");
		
		return false;
	}
};

UserModel.prototype.getRoles = function() {
	return this.roles;
};

UserModel.prototype.getCapabilities = function() {
	return this.capabilities;
};

UserModel.prototype.getCapabilityByName = function(capabilityName) {
	var capabilities = this.capabilities.toArray();
	
	for(var c=0; c<capabilities.length; c++) {
		if(capabilities[c].capabilityName === capabilityName)
			return capabilities[c];
	}
	
	return null;
};

UserModel.prototype._toPattern = function(roleOrCapability) {
	if($$.isSet(roleOrCapability)) {
		return roleOrCapability.replace(/\*/g, ".*").replace("/\?/g", ".?");
	}
	
	return null;
};

UserModel.prototype._check = function(pattern, string) {
	$$.Log.debug("Checking '" + string + "' against '" + pattern + "'");
	
	var result = false;
	
	try {
		result = new RegExp("^" + pattern + "$").test(string);
	} catch (E) {
		$$.Log.warn("Unable to check string '" + string + "' against pattern '" + pattern + "': " + E);
	}
	
	$$.Log.debug("Check result: " + result);
	
	return result;
};

UserModel.prototype.filterCapabilities = function(capabilityFilter) {
	var capabilities = this.capabilities.toArray();
	var filtered = new $$.Set();
	
	if($$.isSet(capabilityFilter)) {
		for(var c=0; c<capabilities.length; c++) {			
			var test = this._check(capabilityFilter, capabilities[c].capabilityName);
			var inverseTest = this._check(this._toPattern(capabilities[c].capabilityName), capabilityFilter);
			
			if(test || inverseTest)
				filtered.add(capabilities[c]);
		}
	}
	
	return filtered;
};

UserModel.prototype.getManagedCountriesBySystemMap = function() {
	var map = new Object();
	
	if($$.isSet(this.managedCountries)) {
		var managedCountry;
		
		for(var m=0; m<this.managedCountries.length; m++) {
			managedCountry = this.managedCountries[m];
			
			if(!$$.isSet(map[managedCountry.sourceSystem])) {
				map[managedCountry.sourceSystem] = new Array();
			}
			
			map[managedCountry.sourceSystem].push(managedCountry.countryId);			
		} 
	}
	
	return map;
};

UserModel.prototype.getManagedCountriesBySystem = function(system) {
	var map = this.getManagedCountriesBySystemMap();
	
	if($$.isSet(system))
		return map[system];
	
	var countries = new $$.Set();
	
	for(var key in map) {
		countries.addAll(map[key]);
	}
	
	return countries.asArray();
};

UserModel.prototype.getAllManagedCountries = function() {
	return this.getManagedCountriesBySystem(null);
};

UserModel.prototype.getManagedSourcesBySystemMap = function() {
	var map = new Object();
	
	if($$.isSet(this.managedSources)) {
		var managedSource;
		
		for(var m=0; m<this.managedSources.length; m++) {
			managedSource = this.managedSources[m];
			
			if(!$$.isSet(map[managedSource.sourceSystem])) {
				map[managedSource.sourceSystem] = new Array();
			}
			
			map[managedSource.sourceSystem].push(managedSource.managedSource);			
		} 
	}
	
	return map;
};

UserModel.prototype.getManagedSourcesBySystem = function(system) {
	var map = this.getManagedSourcesBySystemMap();
	
	if($$.isSet(system))
		return map[system];
	
	var sources = new $$.Set();
	
	for(var key in map) {
		sources.addAll(map[key]);
	}
	
	return sources.asArray();
};

UserModel.prototype.getAllManagedSources = function() {
	return this.getManagedSourcesBySystem(null);
};

UserModel.prototype.initialize = function(userData) {
	$$.Log.debug("Initializing user model");
	
	if($$.isSet(userData)) {
		$$.Log.debug("Setting user ID to " + userData.id);
		
		if($$.isSet(userData.id))
			this.setId(userData.id);
			
		if($$.isSet(userData.roles) && $$.isArray(userData.roles))
			for(var r=0; r<userData.roles.length; r++)		
				this.addRole(userData.roles[r]);

		if($$.isSet(userData.capabilities) && $$.isArray(userData.capabilities))
			for(var c=0; c<userData.capabilities.length; c++)		
				this.addCapability(userData.capabilities[c]);

		if($$.isSet(userData.managedCountries) && $$.isArray(userData.managedCountries))
			this.managedCountries = userData.managedCountries;
		
		if($$.isSet(userData.managedSources) && $$.isArray(userData.managedSources))
			this.managedSources = userData.managedSources;
		
		this.cachedCapabilitiesCheck = new Object();
		this.cachedRolesCheck = new Object();
		
		return this;
	}
};

$.extend(true, $$, { UserModel: UserModel });