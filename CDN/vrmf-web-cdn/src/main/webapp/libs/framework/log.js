function Log() {
	this.SEVERITIES = new Object();
	this.SEVERITIES.DEBUG 	= { level: 0, label: "DBG", cssClass: "debug" };
	this.SEVERITIES.INFO  	= { level: 1, label: "INF", cssClass: "info" };
	this.SEVERITIES.WARN  	= { level: 2, label: "WRN", cssClass: "warn" };
	this.SEVERITIES.ERROR	= { level: 3, label: "ERR", cssClass: "error" };
	this.SEVERITIES.SERVICE = { level: 666, label: "SRV", cssClass: "service", internal: true };
	
	this.SEVERITY = this.SEVERITIES.WARN;
	
	this.LOG_COOKIE_PREFIX = "Log.";
	this.LOG_ENABLED_COOKIE_PREFIX = this.LOG_COOKIE_PREFIX + "enabled";
	this.LOG_SEVERITY_COOKIE_PREFIX = this.LOG_COOKIE_PREFIX + "severity";
};

Log.prototype._doLog = function (caller, level, message) {
	if(this.isSeverityAllowed(this.SEVERITY, level)) {
		var now = new Date();
		
		var loggedMessage = "";
		
		try {
			loggedMessage += now.formatTime();
		} catch (E) {
			loggedMessage += now.getTime();
		}
		
		loggedMessage += " [ ";
		
		try {
			loggedMessage += level.label;
		} catch (E) {
			loggedMessage += "UNK";
			
			this.doLog(null, this.SEVERITIES.ERROR, "Unknown severity level '" + level + "'");
		}
		
		loggedMessage += " ] : ";
		
		loggedMessage += "{ " + ( $$.isSet(caller) ? caller.name : "" ) + " } ";
		
		loggedMessage += message;
		
		this._logToConsole(level, loggedMessage);
		
		if(this._isWindowLogAvailable())
			this._logToWindow(level, loggedMessage);
	}
};

Log.prototype._logToConsole = function(level, message) {
	if($$.isSet(window.console) && $$.isSet(window.console.log))
		console.log(message);
};

Log.prototype._isWindowLogAvailable = function(logPanel) {
	var $log = null;
	
	if($$.isSet(logPanel))
		$log = $(logPanel);
	else
		$log = $("#log");
	
	return $$.isSet($log) && $log.length == 1;
};

Log.prototype._initializeDefaultWindowLog = function() {	
	var $log = $("#log");
	
	if(!this._isWindowLogAvailable($log)) {
		var $logEnabler = $("<div id=\"logEnabler\" class=\"defaultLogEnabler\" title=\"Toggle log display\">Log</div>");
		
		$logEnabler.click(function() {
			$$.Log.toggle();
		});
		
		/*
		$logEnabler.draggable({ appendTo: 'body', containment: 'window', axis: "y", drag: function(event, ui) {
			var handle = $logEnabler;
			var top = handle.offset().top;
			var height = handle.height();
			
			var windowHeight = $(window).height();
			
			var newHeight = windowHeight - ( top + height ) + "px !important";
			
			$("#log").css('height', newHeight).css('maxHeight', newHeight);
		}});
		*/
		
		$log = $("<div id=\"log\" class=\"defaultLogContainer log hidden\"/>");
	
		var $logControls = $("<div id=\"logControls\" class=\"defaultLogControls logControls\">");
						
		$logControls.append("<span><a href='#' class='logMessage debug' onclick='javascript:$$.Log.clearLog(); return false;'>CLEAR LOG</a>&nbsp;|&nbsp;</span>");
		$logControls.append("<label>SEVERITY:&nbsp;</label>");
		
		for(var key in this.SEVERITIES) {
			if(!$$.isSet(this.SEVERITIES[key].internal))
				$logControls.append("<span class='logControl logMessage " + this.SEVERITIES[key].cssClass + "'>" + key + "</span>");
		};
		
		$log.append($logControls);
		
		var $logEntries = $("<div id=\"logEntries\" class=\"logEntriesContainer\"/>");
		
		$log.append($logEntries);
		
		$("body").append($logEnabler).append($log);
		
		$(".logControl", $log).click(function() {
			$("#log .logControl").removeClass("currentSeverity");
			
			$$.Log.setSeverity($(this).text().toUpperCase());
			
			$(this).addClass("currentSeverity");
		});
	} else {
		this.debug("Unable to initialize default window log: this document already defines a #log element...");
	}
		
	return $log;
};

Log.prototype._logToWindow = function(level, message) {
	var $log = $("#log");
	var $logEntries = $("#logEntries");
	
	if(this._isWindowLogAvailable($log)) {
		var entry = $("<span class=\"logMessage\"/>");
		entry.text(message);
		
		try {
			entry.addClass(level.cssClass);
		} catch (E) {
			this.doLog(this._logToWindow.caller, this.SEVERITIES.ERROR, "CSS class not set for severity level '" + level + "'");
		}
		
		$logEntries.append(entry);
		
		try {
			$log.scrollTo(entry);
		} catch (E) {
			//Do nothing: the scrollTo plugin is not available
		}
	};
};

Log.prototype.hide = function() {
	$("#log, #logEnabler").addClass("hidden");
};

Log.prototype.show = function() {
	$("#log").addClass("hidden");
	$("#logEnabler").removeClass("hidden").removeClass("top");
};

Log.prototype.open = function() {
	var $logEnabler = $("#logEnabler");
	var $log = $("#log");
	
	$log.scrollTo($(".logMessage:last"));
	$log.slideDown(function() { 
		$log.removeClass("hidden");
		$logEnabler.addClass("top").slideDown().removeClass("hidden");
		
		try {
			var cookieID = $$.Log.LOG_ENABLED_COOKIE_PREFIX + "." + $$.UserManager.getLoggedUser().getId();
			$.cookie(cookieID, true, { path: '/' });
		} catch (E) {
			this._doLog(this.toggle.caller, this.SEVERITIES.ERROR, "Unable to store cookie for log status: " + E);
		}
	});
};

Log.prototype.toggle = function() {
	var $logEnabler = $("#logEnabler");
	var $log = $("#log");
	var $logControls = $("#logControls");
	
	if($log.length > 0) {
		$logEnabler.slideToggle(function() {
			//$logControls.show();
			
			$log.slideToggle(function() { 
				$log.toggleClass("hidden");
				$logEnabler.toggleClass("top").slideToggle().removeClass("hidden");
				
				try {
					var cookieID = $$.Log.LOG_ENABLED_COOKIE_PREFIX + "." + $$.UserManager.getLoggedUser().getId();
					var enable = !$("#log").hasClass("hidden");
					$.cookie(cookieID, enable, { path: '/' });
				} catch (E) {
					this._doLog(this.toggle.caller, this.SEVERITIES.ERROR, "Unable to store cookie for log status: " + E);
				}				
			}).scrollTo($(".logMessage:last", $log));
		});
	}
};

Log.prototype.setSeverity = function(severity) {
	if($$.isSet(this.SEVERITIES[severity])) {
		this.SEVERITY = this.SEVERITIES[severity];

		try {
			var cookieID = this.LOG_SEVERITY_COOKIE_PREFIX + "." + $$.UserManager.getLoggedUser().getId();
			$.cookie(cookieID, severity, { path: '/' });
		} catch (E) {
			this._doLog(this.setSeverity.caller, this.SEVERITIES.ERROR, "Unable to store cookie for current log severity: " + E);
		}
				
		this._doLog(this.setSeverity.caller, this.SEVERITIES.SERVICE, "Setting severity to " + severity);
	}
};

Log.prototype.isSeverityAllowed = function(current, target) {
	return !$$.isSet(current) ? true : current.level <= target.level;
};

Log.prototype.debug = function(message) {
	this._doLog(this.debug.caller, this.SEVERITIES.DEBUG, message);
};

Log.prototype.info = function(message) {
	this._doLog(this.info.caller, this.SEVERITIES.INFO, message);
};

Log.prototype.warn = function(message) {
	this._doLog(this.warn.caller, this.SEVERITIES.WARN, message);
};

Log.prototype.error = function(message) {
	this._doLog(this.error.caller, this.SEVERITIES.ERROR, message);
};

Log.prototype.service = function(message) {
	this._doLog(this.service.caller, this.SEVERITIES.SERVICE, message);
};

Log.prototype.clearLog = function() {
	var $log = $("#log");
	
	if(this._isWindowLogAvailable($log)) {
		$("#logEntries").empty();
	
		this._doLog(this.clearLog.caller, this.SEVERITIES.SERVICE, "Log has been cleared");
	}
};

Log.prototype.setup = function() {
	this._initializeDefaultWindowLog();
	
	try {
		if($$.UserManager.isUserLogged()) {
			var loggedUser = $$.UserManager.getLoggedUser();
			var userID = loggedUser.getId();
			var severity = $.cookie(this.LOG_SEVERITY_COOKIE_PREFIX + "." + userID);
			var enabled = $.cookie(this.LOG_ENABLED_COOKIE_PREFIX + "." + userID);
			
			if($$.isSet(severity))
				this.setSeverity(severity);
			
			this.show();
			
			if($$.isSet(enabled) && "true" == enabled)
				this.open();
		} else
			this.hide();
	} catch(E) {
		this._doLog(this.setup.caller, this.SEVERITIES.ERROR, "Unable to read cookie to initialize log status: " + E);
	}	
};

$$.Dep.require("vrmf.log", { "vrmf.utils": { identifier: "$$.Utils" } }, function() {
	$.extend($$, {
		Log: new Log()
	});

	$(document).ready(function() {
		$$.Log.setup();
	});
});