function Ajax() {
	$$.Log.debug("Initializing 'Ajax' Object...");

	this.setup();

	this.errorParser = new Object();

	this.errorParser.name = 'Default Tomcat error pages parser';
	this.errorParser.parse = function(text) {
		return new RegExp("(.+)(\<h1\>)(.+)(\<\/h1\>)(.+)").exec(text)[3];
	};
};

Ajax.prototype.setErrorParser = function(parser) {
	this.errorParser = parser;
};

Ajax.prototype._defaultErrorManager = function(jqXHR, textStatus, errorThrown) {
	var parsed = $$.Ajax.parseError(jqXHR, textStatus, errorThrown);
	var msg = "[ " + parsed.code + " : " + parsed.error + " ] " + ( $$.isSet(parsed.message) && $$.rawTrim(parsed.message) != null ? " : " + parsed.message : "" );

	var fullMsg = $$.I18n.getText("vrmf.common.ajax.communication.error", msg);

	var isTimeout = parsed.error == "timeout"; 
	
	if(parsed.code == 0) {
		if(isTimeout)
			fullMsg = $$.I18n.getText("vrmf.common.ajax.communication.timeout");
		else if(parsed.error == "error") {
			$$.Log.warn(fullMsg + " [ possible page unload ? ]");
			return;
		}
	}
	
	var displayMessage = !isTimeout;

	var reportMessage = "<code class='selectable ajaxError asyncMessage'>";
	reportMessage += "<span class='debug'>" + this.type + " " + this.url + "</span>";
	reportMessage += "<br/>";
	reportMessage += "<br/>";

	if(isTimeout) {
		reportMessage += "<span>" + $$.I18n.getText("vrmf.common.ajax.reload.hint") + "</span>";

		if($$.isSet(this._lastRequestToken)) {
			var lastRequestToken = this._lastRequestToken();
			var requestToken = this.requestToken;

			displayMessage = !$$.isSet(requestToken) || requestToken === lastRequestToken;
		} else
			displayMessage = true;
	}

	reportMessage += "</code>";
	
	$$.Log.error(reportMessage);
	$$.Log.error(fullMsg);
	
	displayMessage &= !$$.isSet(this._onErrorCallback);
	
	if(displayMessage) {
		$$.Notification.severe(fullMsg, reportMessage);
	} 
		
	if($$.isSet(this._onErrorCallback)) {
		this._onErrorCallback(this);
	}

	$.unblockUI();
};

Ajax.prototype.setup = function(errorParser) {
	if($$.isSet(errorParser)) {
		this.errorParser = errorParser;
	}

	$(document).ajaxStart(function() {
		$$.Log.debug("Starting Ajax request...");

		$(".ajaxWait").show();
		$(":input.disableDuringAjaxRequest").each(function() {
			var $this = $(this);
			var enabled = $$.Utils.isEnabled($this);

			$this.data("vrmf.ajax.status", enabled);

			if(enabled)
				$$.Utils.disable($this);
		});
	});

	$(document).ajaxSend(function(event, request, settings) {
		$$.Log.info("Sending an Ajax " + settings.type + " request to " +  unescape(settings.url) + "...");
	});

	$(document).ajaxStop(function() {
		$$.Log.debug("Stopping Ajax request...");

		$(".ajaxWait").hide();
		$(".ui-autocomplete-loading").removeClass("ui-autocomplete-loading");
	});

	$(document).ajaxComplete(function() {
		$$.Log.debug("Completing Ajax request...");

		$(".ui-autocomplete-loading").removeClass(".ui-autocomplete-loading");

		$(":input.disableDuringAjaxRequest").each(function() {
			var $this = $(this);
			var enabled = $this.data("vrmf.ajax.status");

			if(enabled && $(".blockUI").size() == 0)
				$$.Utils.enable($this);
		});
	});

	$(document).ajaxError(function(event, jqXHR, settings, thrownError) {
		$$.Log.warn("An Ajax " + settings.type + " request to " + unescape(settings.url) + " has failed: " + thrownError + "...");
	});

	$(document).ajaxSuccess(function(event, request, settings) {
		$$.Log.info("...Successfully completed Ajax " + settings.type + " request to " + unescape(settings.url));
		$$.Log.debug("Returned XML is: " + request.responseText);
	});

	var $this = this;

	$.ajaxSetup({
		async: true,	/** Perform async requests by default */
		cache: true,  	/** Don't cache request by default */
		timeout: 5000,	/** Timeout set to 5 seconds */
		dataType: "json",	/** Default data type is 'json' */
		error: $this._defaultErrorManager,
		statusCode: {
			z400: function(jqXHR, textStatus, errorThrown) {
				var parsed = $$.Ajax.parseError(jqXHR, textStatus, errorThrown);

				$$.Interface.error([ $$.I18n.getText("vrmf.common.ajax.error.syntax") + ( $$.isSet(parsed.message) && $$.rawTrim(parsed.message) != null ? ": " + parsed.message : "." ), 
				                     $("<code class='ajaxError'>" + $$.I18n.getText("vrmf.common.ajax.request", this.type, this.url) + "</code>") ], "[ " + parsed.code + " : " + parsed.error + " ]");
			},
			z403: function(jqXHR, textStatus, errorThrown) {
				var parsed = $$.Ajax.parseError(jqXHR, textStatus, errorThrown);

				$$.Interface.error([ $$.I18n.getText("vrmf.common.ajax.error.unauthorized") + ( $$.isSet(parsed.message) && $$.rawTrim(parsed.message) != null ? ": " + parsed.message : "." ), 
				                     $("<code class='ajaxError'>" + $$.I18n.getText("vrmf.common.ajax.request", this.type, this.url) + "</code>") ], "[ " + parsed.code + " : " + parsed.error + " ]");
			},
			z404: function(jqXHR, textStatus, errorThrown) {
				var parsed = $$.Ajax.parseError(jqXHR, textStatus, errorThrown);

				$$.Interface.error([ $$.I18n.getText("vrmf.common.ajax.error.not.found") + ( $$.isSet(parsed.message) && $$.rawTrim(parsed.message) != null ? ": " + parsed.message : "." ), 
				                     $("<code class='ajaxError'>" + $$.I18n.getText("vrmf.common.ajax.request", this.type, this.url) + "</code>") ], "[ " + parsed.code + " : " + parsed.error + " ]");
			},
			z500: function(jqXHR, textStatus, errorThrown) {
				var parsed = $$.Ajax.parseError(jqXHR, textStatus, errorThrown);

				$$.Interface.error([ $$.I18n.getText("vrmf.common.ajax.error.server") + ( $$.isSet(parsed.message) && $$.rawTrim(parsed.message) != null ? ": " + parsed.message : "." ), 
				                     $("<code class='ajaxError'>" + $$.I18n.getText("vrmf.common.ajax.request", this.type, this.url) + "</code>") ], "[ " + parsed.code + " : " + parsed.error + " ]");
			},
			z509: function(jqXHR, textStatus, errorThrown) {
				var parsed = $$.Ajax.parseError(jqXHR, textStatus, errorThrown);

				$$.Interface.error([ $$.I18n.getText("vrmf.common.ajax.error.bandwidth.full") + ( $$.isSet(parsed.message) && $$.rawTrim(parsed.message) != null ? ": " + parsed.message : "." ), 
				                     $("<code class='ajaxError'>" + $$.I18n.getText("vrmf.common.ajax.request", this.type, this.url) + "</code>") ], "[ " + parsed.code + " : " + parsed.error + " ]");
			}
		}
	});
};

Ajax.prototype.parseError = function(jqXhr, textStatus, errorThrown, parser) {
	var parsed = new Object();

	if($$.isSet(errorThrown.code)) {
		parsed.code = errorThrown.code;
		parsed.error = errorThrown.message;
	} else if($$.isSet(jqXhr.status)) {
		parsed.code = jqXhr.status;
		parsed.error = $$.isSet(errorThrown) ? errorThrown : jqXhr.statusText;
	}

	if(jqXhr.status == 0) {
		parsed.message = jqXhr.responseText;
	} else {
		try {
			parsed.message = $$.isSet(parser) ? parser.parse(jqXhr.responseText) : this.errorParser.parse(jqXhr.responseText);
		} catch (E) {
			parsed.message = jqXhr.responseText;
		}
	}

	return parsed;
};

$$.Dep.require("vrmf.ajax", { "vrmf.i18n": { identifier: "$$.I18n" }, "vrmf.notification": { identifier: "$$.Notification" } }, function() {
	$.extend($$, {
		Ajax: new Ajax()
	});
});