function Metadata() {
	$$.Log.debug("Initializing 'Metadata' Object...");

	this.BASE_URL = "cdn";
	this.CACHE_RESPONSE = false;

	this.FACTSHEETS_URL = "http://www.fao.org/fishery/{domain}/{fid}/{language}";
	
	this.PORTS_MAP = new Object();

	this.COUNTRIES = new Array();
	this.COUNTRIES_MAP = new Object();
	this.COUNTRIES_BY_ISO2_MAP = new Object();
	this.COUNTRIES_BY_ISO3_MAP = new Object();

	this.COUNTRIES_GROUPS = new Array();
	this.COUNTRIES_GROUPS_MAP = new Object();

	this.SUBDIVISIONS = new Array();
	this.SUBDIVISIONS_MAP = new Object();

	this.OTHER_SYSTEMS = new Array();
	this.OTHER_SYSTEMS_MAP = new Object();

	this.SYSTEMS = new Array();
	this.SYSTEMS_MAP = new Object();

	this.SYSTEMS_GROUPS = new Array();
	this.AUTH_SYSTEMS = new Array();
	this.AUTH_SYSTEMS_GROUPS = new Array();

	this.VESSEL_IDENTIFIERS = new Array();
	this.VESSEL_IDENTIFIERS_MAP = new Object();

	this.VESSEL_STATUS = new Array();

	this.FILTERED_AUTHORIZATION_TYPES = new Array();
	this.FILTERED_AUTHORIZATION_TYPES_MAP = new Object();

	this.AUTHORIZATION_TYPES = new Array();
	this.AUTHORIZATION_TYPES_MAP = new Object();

	this.AUTHORIZATION_TERMINATION_REASONS = new Array();
	this.AUTHORIZATION_TERMINATION_REASONS_MAP = new Object();

	this.HULL_MATERIALS = new Array();
	this.HULL_MATERIALS_MAP = new Object();
	
	this.VESSEL_TYPES = new Array();
	this.VESSEL_TYPES_MAP = new Object();

	this.VESSEL_TYPES_CATEGORIES = new Array();
	this.VESSEL_TYPES_CATEGORIES_MAP = new Object();

	this.GEAR_TYPES = new Array();
	this.GEAR_TYPES_MAP = new Object();

	this.GEAR_TYPES_CATEGORIES = new Array();
	this.GEAR_TYPES_CATEGORIES_MAP = new Object();

	this.MEASURE_UNITS = new Array();
	this.MEASURE_UNITS_MAP = new Object();

	this.LENGTH_TYPES = new Array();
	this.LENGTH_TYPES_MAP = new Object();

	this.TONNAGE_TYPES = new Array();
	this.TONNAGE_TYPES_MAP = new Object();

	this.POWER_TYPES = new Array();
	this.POWER_TYPES_MAP = new Object();

	this.LENGTH_RANGES = new Array();
	this.LENGTH_RANGES_MAP = new Object();

	this.TONNAGE_RANGES = new Array();
	this.TONNAGE_RANGES_MAP = new Object();

	this.POWER_RANGES = new Array();
	this.POWER_RANGES_MAP = new Object();

	this.AGE_RANGES = new Array();
	
	this.AUTHORITY_ROLES = new Array();
	this.AUTHORITY_ROLES_MAP = new Object();

	this.AUTHORIZATION_HOLDERS = new Array();
	this.AUTHORIZATION_HOLDERS_MAP = new Object();
	
	this.INSPECTION_REPORT_TYPES = new Array();
	this.INSPECTION_REPORT_TYPES_MAP = new Object();
	
	this.INSPECTION_OUTCOME_TYPES = new Array();
	this.INSPECTION_OUTCOME_TYPES_MAP = new Object();
	
	this.INSPECTION_INFRINGEMENT_TYPES = new Array();
	this.INSPECTION_INFRINGEMENT_TYPES_MAP = new Object();
	
	this.RFMO_IUU_LISTS = new Array();
	this.RFMO_IUU_LISTS_MAP = new Object();
	
	this.FACTSHEETS = new Object();
	this.FACTSHEETS.VESSEL_TYPES = new Object(),
	this.FACTSHEETS.GEAR_TYPES = new Object(),
	this.FACTSHEETS.PORT_STATE_MEASURES = new Object();
	this.FACTSHEETS.SPECIES = new Object();
	
	this.timeout = 5000;
};

Metadata.prototype.setTimeout = function(timeout) {
	this.timeout = timeout;
};

Metadata.prototype.getTimeout = function() {
	return this.timeout;
};

Metadata.prototype.setBaseURL = function(baseURL) {
	this.BASE_URL = baseURL;
};

Metadata.prototype.setFactsheetsURL = function(factsheetsURL) {
	this.FACTSHEETS_URL = factsheetsURL;
};

Metadata.prototype.setCacheResponse = function(cacheResponse) {
	if($$.isSet(cacheResponse))
		this.CACHE_RESPONSE = "true" === cacheResponse || cacheResponse ? true : false;
};

Metadata.prototype._load = function(what, request, user, async, callback, managementCallback) {
	var metadata = this;

	var updatedRequest = $.extend({}, request);

	if($$.isSet(user)) {
		updatedRequest.userID = user.getId();
	}

	$$.Log.debug("Async is set to: " + async);

	$.ajax({
		dataType: 'json',
		timeout: metadata.timeout,
		type: 'get',
		async: $$.isSet(async) ? async : true,
		cache: this.CACHE_RESPONSE,
		data: updatedRequest,
		url: this.BASE_URL + "/data/" + ($$.isArray(what) ? what.join(",") : what) + ".json" + "/" + _CURRENT_LANG,
		success: function(results) {
			if($$.isSet(managementCallback))
				managementCallback(metadata, results, callback);
			else
				metadata._manageResults(metadata, results, callback);
		}
	});
};

Metadata.prototype._map = function(array, map, mapper) {
	var key;
	if($$.isSet(array) && $$.isSet(array.length))
		for(var a=0; a<array.length; a++) {
			key = mapper(array[a]);

			if($$.isSet(key))
				map[key] = array[a];
		}

	return map;
};

Metadata.prototype._arrayMap = function(array, map, mapper) {
	var key;
	if($$.isSet(array) && $$.isSet(array.length)) {
		for(var a=0; a<array.length; a++) {
			key = mapper(array[a]);

			if($$.isSet(key)) {
				if(!$$.isSet(map[mapper(array[a])]))
					map[mapper(array[a])] = new Array();

				map[mapper(array[a])].push(array[a]);
			}
		}
	}

	return map;
};

Metadata.prototype._storeIntoArray = function(data, property) {
	this[property] = data;

	if($$.isSet(this[property]) && $$.isSet(this[property].length))
		$$.Log.debug(this[property].length + " data loaded as '" + property + "' metadata");
	else
		$$.Log.warn("No data loaded as '" + property + "' metadata");
};

Metadata.prototype._storeIntoMap = function(data, mapper, property) {
	this[property + "_MAP"] = {};
	//this[property + "_MAP"] = this._map(this[property], this[property + "_MAP"], mapper);
	this[property + "_MAP"] = this._map(data, this[property + "_MAP"], mapper);
};

Metadata.prototype._storeIntoArrayMap = function(data, mapper, property) {
	this[property + "_MAP"] = {};
//	this[property + "_MAP"] = this._arrayMap(this[property], this[property + "_MAP"], mapper);
	this[property + "_MAP"] = this._arrayMap(data, this[property + "_MAP"], mapper);
};

Metadata.prototype.loadCountries = function(callback, user, async) {
	this._load("countries", {}, user, async, callback);
};

Metadata.prototype.loadSubdivisions = function(callback, user, async) {
	this._load("subdivisions", {}, user, async, callback);
};

Metadata.prototype.loadSystemsAndGroups = function(callback, user, async) {
	this._load("{systems}", {}, user, async, callback);
};

Metadata.prototype.loadAuthSystemsAndGroups = function(callback, user, async) {
	this._load("{authorizationSystems}", {}, user, async, callback);
};

Metadata.prototype.loadVesselIdentifiers = function(callback, user, async) {
	this._load("identifiers", {}, user, async, callback);
};

Metadata.prototype.loadVesselStatus = function(callback, async) {
	this._load("vesselStatus", {}, user, async, callback);
};

Metadata.prototype.loadFactsheets = function(callback, async) {
	this._load("factsheets", {}, user, async, callback);
};

Metadata.prototype.buildPortStateMeasuresFactsheetsMap = function() {
	this.FACTSHEETS.PORT_STATE_MEASURES.BY_ID = new Object();

	var availablePSMFactsheets = this.FACTSHEETS.PORT_STATE_MEASURES.availableIDs;

	if($$.isSet(availablePSMFactsheets)) {
		var id;
		for(var p=0; p<availablePSMFactsheets.length; p++) {
			id = availablePSMFactsheets[p];

			if(id.indexOf("_") >= 0)
				id = id.substring(0, id.indexOf("_"));

			if(!$$.isSet(this.FACTSHEETS.PORT_STATE_MEASURES.BY_ID[id]))
				this.FACTSHEETS.PORT_STATE_MEASURES.BY_ID[id] = new Array();

			this.FACTSHEETS.PORT_STATE_MEASURES.BY_ID[id].push(availablePSMFactsheets[p]);
		};
	} else {
		$$.Log.warn("Unable to retrieve any valid PSM factsheet. Probably the FS webservice is down?");
	}
};

Metadata.prototype.getVesselTypeFactsheetLink = function(vesselTypeID, language) {
	var factsheets = this.FACTSHEETS;

	if($$.isSet(factsheets) &&
	   $$.isSet(factsheets.VESSEL_TYPES) && 
	   $$.isSet(factsheets.VESSEL_TYPES.availableIDs) && 
	   factsheets.VESSEL_TYPES.availableIDs.indexOf(vesselTypeID) >= 0)
		return this.getFactsheetLink("vesselType", vesselTypeID, language);

	return null;
};

Metadata.prototype.getFactsheetLink = function(domain, fid, language) {
	return this.FACTSHEETS_URL.replace(/\{domain\}/g, domain).
							   replace(/\{fid\}/g, fid).
							   replace(/\{language\}/g, $$.isSet(language) ? language : "en" );
};

Metadata.prototype.getGearTypeFactsheetLink = function(gearTypeID, language) {
	var factsheets = this.FACTSHEETS;

	if($$.isSet(factsheets) &&
	   $$.isSet(factsheets.GEAR_TYPES) && 
	   $$.isSet(factsheets.GEAR_TYPES.availableIDs) && 
	   factsheets.GEAR_TYPES.availableIDs.indexOf(gearTypeID) >= 0)
		return this.getFactsheetLink("gearType", gearTypeID, language);

	return null;
};

Metadata.prototype.getSpeciesFactsheetLink = function(speciesID, language) {
	var factsheets = this.FACTSHEETS;

	if($$.isSet(factsheets) &&
	   $$.isSet(factsheets.SPECIES) && 
	   $$.isSet(factsheets.SPECIES.availableIDs)) {
		var index = factsheets.SPECIES.availableIDs.indexOf("" + speciesID);

		if(index >= 0)
			return this.getFactsheetLink("species", speciesID, language);
	} 

	return null;
};

Metadata.prototype.getPortStateMeasuresFactsheetIDsPrefix = function(country) {
	var factsheets = this.FACTSHEETS;

	if($$.isSet(country) && 
	   $$.isSet(country.iso3Code) && 
	   $$.isSet(factsheets) &&
	   $$.isSet(factsheets.PORT_STATE_MEASURES)) {
		var PSMIDsPrefixes = new Array();
		var countryGroups = $$.CountriesManager.getGroups(country.id);

		$$.Log.debug("Searching PSM factsheets ID prefixes for country " + country.id + " / " + country.iso2Code);

		if($$.isSet(factsheets.PORT_STATE_MEASURES.BY_ID)) {
			if($$.isSet(factsheets.PORT_STATE_MEASURES.BY_ID[country.iso3Code]))
				PSMIDsPrefixes.push(country.iso3Code);

			for(var g=0; g<countryGroups.length; g++)
				if($$.isSet(factsheets.PORT_STATE_MEASURES.BY_ID[countryGroups[g].groupID]))
					PSMIDsPrefixes.push(countryGroups[g].groupID);
		}

		return PSMIDsPrefixes;
	} 

	return null;
};

Metadata.prototype.getPortStateMeasuresFactsheetIDs = function(country) {
	var factsheets = this.FACTSHEETS;

	if($$.isSet(country) && 
	   $$.isSet(country.iso3Code) && 
	   $$.isSet(factsheets) &&
	   $$.isSet(factsheets.PORT_STATE_MEASURES)) {
		var PSMs = new Array();
		var PSMIDsPrefixes = this.getPortStateMeasuresFactsheetIDsPrefix(country);
		
		$$.Log.debug("Searching PSM factsheets for country " + country.id + " / " + country.iso2Code);

		for(var p=0; p<PSMIDsPrefixes.length; p++)
			if($$.isSet(factsheets.PORT_STATE_MEASURES.BY_ID)) {
				if($$.isSet(factsheets.PORT_STATE_MEASURES.BY_ID[PSMIDsPrefixes[p]]))
					PSMs = PSMs.concat(factsheets.PORT_STATE_MEASURES.BY_ID[PSMIDsPrefixes[p]]);
		}

		return PSMs;
	} 

	return null;
};

Metadata.prototype.getPortStateMeasuresFactsheetLink = function(portStateMeasureID, language) {
	var factsheets = this.FACTSHEETS;

	if($$.isSet(factsheets) && 
	   $$.isSet(factsheets.PORT_STATE_MEASURES) &&
	   $$.isSet(factsheets.PORT_STATE_MEASURES.availableIDs) && 
	   factsheets.PORT_STATE_MEASURES.availableIDs.indexOf(portStateMeasureID) >= 0)
		return this.getFactsheetLink("psm", portStateMeasureID, language);

	return null;
};

Metadata.prototype.loadCodes = function(callback, user, async) {
	this._load("{codes}", {}, user, async, callback);
};

Metadata.prototype.loadFilteredAuthorizationTypes = function(callback, issuers, user, async) {
	this._load("authorizationTypes", { s: issuers }, user, async, function(metadata, results) {
		if($$.isSet(results) && $$.isSet(results.data)) {
			metadata._storeIntoArray(results.data.authorizationTypes, "FILTERED_AUTHORIZATION_TYPES");
			metadata._storeIntoMap(results.data.authorizationTypes, function(authorizationType) { return authorizationType.sourceSystem + "_" + authorizationType.id; }, "FILTERED_AUTHORIZATION_TYPES");
		} else {
			$$.Interface.error("Unable to retrieve filtered authorization types metadata: please ensure to clear the browser cache and refresh this page");
		}

		if(typeof callback == 'function')
			callback(metadata);
	});
};

Metadata.prototype.loadAuthorizationTypesByIssuers = function(callback, issuers, user, async) {
	this._load("authorizationTypes", { s: issuers }, user, async, callback);
};

Metadata.prototype.loadRanges = function(callback, user, async) {
	this._load("{ranges}", {}, user, async, callback);
};

Metadata.prototype.loadAll = function(user, callback, async) {
	this.loadAllWithParameters(user, {}, callback, async);
};

Metadata.prototype.loadAllWithParameters = function(user, parameters, callback, async) {
	var self = this;
	this._load("all", 
			   $$.isSet(parameters) ? parameters : {}, 
			   user, 
			   async, 
			   function(metadata, results) {
				   self._manageResults(metadata, results, callback);
			   });
};

Metadata.prototype._manageResults = function(metadata, results, callback) {
	if($$.isSet(results) && $$.isSet(results.data)) {
		if($$.isSet(results.data.countries)) {
			metadata._storeIntoArray(results.data.countries, "COUNTRIES");
			metadata._storeIntoMap(results.data.countries, function(country) { return "cid_" + country.id; }, "COUNTRIES");
			metadata._storeIntoMap(results.data.countries, function(country) { return $$.isSet(country.iso2Code) ? country.iso2Code.toUpperCase() : null; }, "COUNTRIES_BY_ISO2");
			metadata._storeIntoMap(results.data.countries, function(country) { return $$.isSet(country.iso3Code) ? country.iso3Code.toUpperCase() : null; }, "COUNTRIES_BY_ISO3");
		}

		if($$.isSet(results.data.countriesGroups)) {
			metadata._storeIntoArray(results.data.countriesGroups, "COUNTRIES_GROUPS");
			metadata._storeIntoMap(results.data.countriesGroups, function(group) { return "cid_" + group.id; }, "COUNTRIES_GROUPS");
		}

		if($$.isSet(results.data.subdivisions)) {
			metadata._storeIntoArray(results.data.subdivisions, "SUBDIVISIONS");
			metadata._storeIntoMap(results.data.subdivisions, function(subdivision) { return "cid_" + subdivision.countryId + "_sid_" + subdivision.id; }, "SUBDIVISIONS");
		}

		if($$.isSet(results.data.otherSystems)) {
			metadata._storeIntoArray(results.data.otherSystems, "OTHER_SYSTEMS");
			metadata._storeIntoMap(results.data.otherSystems, function(system) { return system.id; }, "OTHER_SYSTEMS");
		}

		if($$.isSet(results.data.systems)) {
			metadata._storeIntoArray(results.data.systems, "SYSTEMS");
			metadata._storeIntoMap(results.data.systems, function(system) { return system.id; }, "SYSTEMS");
		}

		if($$.isSet(results.data.systemsGroups)) {
			metadata._storeIntoArray(results.data.systemsGroups, "SYSTEMS_GROUPS");
			metadata._storeIntoArrayMap(results.data.systemsGroups, function(group) { return group.description; }, "SYSTEMS_GROUPS");
		}

		if($$.isSet(results.data.authorizationSystems)) {
			metadata._storeIntoArray(results.data.authorizationSystems, "AUTH_SYSTEMS");
			metadata._storeIntoMap(results.data.authorizationSystems, function(system) { return system.id; }, "AUTH_SYSTEMS");
		}

		if($$.isSet(results.data.authorizationSystemsGroups)) {
			metadata._storeIntoArray(results.data.authorizationSystemsGroups, "AUTH_SYSTEMS_GROUPS");
			metadata._storeIntoArrayMap(results.data.authorizationSystemsGroups, function(group) { return group.description; }, "AUTH_SYSTEMS_GROUPS");
		}

		if($$.isSet(results.data.identifiers)) {
			metadata._storeIntoArray(results.data.identifiers, "VESSEL_IDENTIFIERS");
			metadata._storeIntoMap(results.data.identifiers, function(identifier) { return identifier.id; }, "VESSEL_IDENTIFIERS");
		}

		if($$.isSet(results.data.vesselStatus)) {
			metadata._storeIntoArray(results.data.vesselStatus, "VESSEL_STATUS");
		}
		
		if($$.isSet(results.data.hullMaterials)) {
			metadata._storeIntoArray(results.data.hullMaterials, "HULL_MATERIALS");
			metadata._storeIntoMap(results.data.hullMaterials, function(hullMaterial) { return hullMaterial.id; }, "HULL_MATERIALS");
		}

		if($$.isSet(results.data.vesselTypes)) {
			metadata._storeIntoArray(results.data.vesselTypes, "VESSEL_TYPES");
			metadata._storeIntoMap(results.data.vesselTypes, function(vesselType) { return vesselType.id; }, "VESSEL_TYPES");
		}

		if($$.isSet(results.data.vesselTypesCategories)) {
			metadata._storeIntoArray(results.data.vesselTypesCategories, "VESSEL_TYPES_CATEGORIES");
			metadata._storeIntoMap(results.data.vesselTypesCategories, function(vesselTypeCategory) { return vesselTypeCategory.isscfvCode; }, "VESSEL_TYPES_CATEGORIES");
		}

		if($$.isSet(results.data.gearTypes)) {
			metadata._storeIntoArray(results.data.gearTypes, "GEAR_TYPES");
			metadata._storeIntoMap(results.data.gearTypes, function(gearType) { return gearType.id; }, "GEAR_TYPES");
		}

		if($$.isSet(results.data.gearTypesCategories)) {
			metadata._storeIntoArray(results.data.gearTypesCategories, "GEAR_TYPES_CATEGORIES");
			metadata._storeIntoMap(results.data.gearTypesCategories, function(gearTypeCategory) { return gearTypeCategory.isscfgCode; }, "GEAR_TYPES_CATEGORIES");
		}

		if($$.isSet(results.data.measureUnits)) {
			metadata._storeIntoArray(results.data.measureUnits, "MEASURE_UNITS");
			metadata._storeIntoMap(results.data.measureUnits, function(measureUnit) { return measureUnit.id; }, "MEASURE_UNITS");
		}

		if($$.isSet(results.data.lengthTypes)) {
			metadata._storeIntoArray(results.data.lengthTypes, "LENGTH_TYPES");
			metadata._storeIntoMap(results.data.lengthTypes, function(lengthType) { return lengthType.id; }, "LENGTH_TYPES");
		}

		if($$.isSet(results.data.tonnageTypes)) {
			metadata._storeIntoArray(results.data.tonnageTypes, "TONNAGE_TYPES");
			metadata._storeIntoMap(results.data.tonnageTypes, function(tonnageType) { return tonnageType.id; }, "TONNAGE_TYPES");
		}

		if($$.isSet(results.data.powerTypes)) {
			metadata._storeIntoArray(results.data.powerTypes, "POWER_TYPES");
			metadata._storeIntoMap(results.data.powerTypes, function(powerType) { return powerType.id; }, "POWER_TYPES");
		}

		if($$.isSet(results.data.authorizationTypes)) {
			metadata._storeIntoArray(results.data.authorizationTypes, "AUTHORIZATION_TYPES");
			metadata._storeIntoMap(results.data.authorizationTypes, function(authorizationType) { return authorizationType.sourceSystem + "_" + authorizationType.id; }, "AUTHORIZATION_TYPES");
		}

		if($$.isSet(results.data.lengthRanges)) {
			metadata._storeIntoArray(results.data.lengthRanges, "LENGTH_RANGES");
			metadata._storeIntoArrayMap(results.data.lengthRanges, function(range) { return range.lengthTypeId; }, "LENGTH_RANGES");
		}

		if($$.isSet(results.data.powerRanges)) {
			metadata._storeIntoArray(results.data.powerRanges, "POWER_RANGES");
			metadata._storeIntoArrayMap(results.data.powerRanges, function(range) { return range.powerTypeId; }, "POWER_RANGES");
		}

		if($$.isSet(results.data.tonnageRanges)) {
			metadata._storeIntoArray(results.data.tonnageRanges, "TONNAGE_RANGES");
			metadata._storeIntoArrayMap(results.data.tonnageRanges, function(range) { return range.tonnageTypeId; }, "TONNAGE_RANGES");
		}

		if($$.isSet(results.data.ageRanges)) {
			metadata._storeIntoArray(results.data.ageRanges, "AGE_RANGES");
		}
		
		if($$.isSet(results.data.authorityRoles)) {
			metadata._storeIntoArray(results.data.authorityRoles, "AUTHORITY_ROLES");
			metadata._storeIntoMap(results.data.authorityRoles, function(authorityRoles) { return authorityRoles.id; }, "AUTHORITY_ROLES");
		}
		
		if($$.isSet(results.data.authorizationHolders)) {
			metadata._storeIntoArray(results.data.authorizationHolders, "AUTHORIZATION_HOLDERS");
			metadata._storeIntoMap(results.data.authorizationHolders, function(authorizationHolders) { return authorizationHolders.id; }, "AUTHORIZATION_HOLDERS");
		}
		
		if($$.isSet(results.data.inspectionInfringementTypes)) {
			metadata._storeIntoArray(results.data.inspectionInfringementTypes, "INSPECTION_INFRINGEMENT_TYPES");
			metadata._storeIntoMap(results.data.inspectionInfringementTypes, function(inspectionInfringementTypes) { return inspectionInfringementTypes.id; }, "INSPECTION_INFRINGEMENT_TYPES");
		}
		
		if($$.isSet(results.data.inspectionOutcomeTypes)) {
			metadata._storeIntoArray(results.data.inspectionOutcomeTypes, "INSPECTION_OUTCOME_TYPES");
			metadata._storeIntoMap(results.data.inspectionOutcomeTypes, function(inspectionOutcomeTypes) { return inspectionOutcomeTypes.id; }, "INSPECTION_OUTCOME_TYPES");
		}
		
		if($$.isSet(results.data.inspectionReportTypes)) {
			metadata._storeIntoArray(results.data.inspectionReportTypes, "INSPECTION_REPORT_TYPES");
			metadata._storeIntoMap(results.data.inspectionReportTypes, function(inspectionReportTypes) { return inspectionReportTypes.id; }, "INSPECTION_REPORT_TYPES");
		}
		
		if($$.isSet(results.data.rfmoIuuLists)) {
			metadata._storeIntoArray(results.data.rfmoIuuLists, "RFMO_IUU_LISTS");
			metadata._storeIntoMap(results.data.rfmoIuuLists, function(rfmoIuuLists) { return rfmoIuuLists.id; }, "RFMO_IUU_LISTS");
		}
		

		metadata.FILTERED_AUTHORIZATION_TYPES = new Array();
		metadata.FILTERED_AUTHORIZATION_TYPES = metadata.FILTERED_AUTHORIZATION_TYPES.concat(metadata.AUTHORIZATION_TYPES);
		metadata.FILTERED_AUTHORIZATION_TYPES_MAP = $.extend(true, {}, metadata.AUTHORIZATION_TYPES_MAP);

		if($$.isSet(results.data.authorizationTerminationReasons)) {
			metadata._storeIntoArray(results.data.authorizationTerminationReasons, "AUTHORIZATION_TERMINATION_REASONS");
			metadata._storeIntoMap(results.data.authorizationTerminationReasons, function(authorizationTerminationReason) { return authorizationTerminationReason.id; }, "AUTHORIZATION_TERMINATION_REASONS");
		}

		if($$.isSet(results.data.gearTypeFactsheets)) {
			metadata.FACTSHEETS.GEAR_TYPES = results.data.gearTypeFactsheets;
		}

		if($$.isSet(results.data.vesselTypeFactsheets)) {
			metadata.FACTSHEETS.VESSEL_TYPES = results.data.vesselTypeFactsheets;
		}

		if($$.isSet(results.data.speciesFactsheets)) {
			metadata.FACTSHEETS.SPECIES = results.data.speciesFactsheets;
		}

		if($$.isSet(results.data.portStateMeasureFactsheets)) {
			metadata.FACTSHEETS.PORT_STATE_MEASURES = results.data.portStateMeasureFactsheets;
			metadata.buildPortStateMeasuresFactsheetsMap();
		}
	};

	if($$.isSet(callback) && typeof callback == 'function')
		callback(metadata);
};

Metadata.prototype.getCountryByID = function(countryID) {
	if($$.isSet(countryID)) {
		if($$.isSet(this.COUNTRIES_MAP)) {
			return this.COUNTRIES_MAP["cid_" + countryID];
		} else {
			alert("The countries map has not been populated yet!");
		}
	}
};

Metadata.prototype.getCountryByIso2Code = function(iso2Code) {
	if($$.isSet(iso2Code)) {
		if($$.isSet(this.COUNTRIES_BY_ISO2_MAP)) {
			return this.COUNTRIES_BY_ISO2_MAP[iso2Code.toUpperCase()];
		} else {
			alert("The countries map has not been populated yet!");
		}
	}
};

Metadata.prototype.getCountryByIso3Code = function(iso3Code) {
	if($$.isSet(iso3Code)) {
		if($$.isSet(this.COUNTRIES_BY_ISO3_MAP)) {
			return this.COUNTRIES_BY_ISO3_MAP[iso3Code.toUpperCase()];
		} else {
			alert("The countries map has not been populated yet!");
		}
	}
};

Metadata.prototype.getPortByID = function(portID, cacheResponse) {
	var port = this.PORTS_MAP["port_" + portID];

	if($$.isSet(port))
		return port;

	$.ajax({
		async: false,
		cache: $$.isSet(cacheResponse) && "true" == cacheResponse || this.CACHE_RESPONSE,
		url: this.BASE_URL + "/data/port/get/id/" + portID + ".json",
		success: function(results) {
			port = results.data;
		}
	});

	return port;
};

$$.Dep.require("vrmf.metadata", { "vrmf.countries": { identifier: "$$.CountriesManager", optional: true }, 
								  "vrmf.interface": { identifier: "$$.Interface" }}, function() {
	$.extend($$, {
		Metadata: new Metadata()
	});
});