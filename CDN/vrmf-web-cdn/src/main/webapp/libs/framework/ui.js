function UI() {
	var $ui = this;
	
	this._UI_BUTTONS_OPTIONS = { 
		create: function(event, ui) {
			var $button = $(event.target);

			var isButtonset = $button.hasClass("buttonSet");

			try {
				if(!isButtonset)
					$ui._buildUIButtons($button);
			} catch (E) {
				$$.Log.warn("Unexpected error caught while attempting to update buttons on creation: " + E);
			}
		}
	};
	
	this.nicescrollEnabled = $$.Browser.isMobile() && $$.isSet($.niceScroll);
	this.pnotifyEnabled = $$.isSet($.pnotify);
	
	if($$.isSet(document.documentMode))
		$("body").addClass("ie v" + document.documentMode);
};

UI.prototype.install = function() {
	var $ui = this;
	
	$ui._initializeExternalPlugins();
	$ui._installDynamicBehaviour();
	$ui._prepareStaticElements();
	$ui._configureStaticElements();
};

UI.prototype._commonResizeAndScrollHandler = function(event) {
	var $target = $(event.target);
	
	$(".menu", $target).hide();
	$$.TooltipManager.close($target);
	
	return true;
};

UI.prototype._installDynamicScrollBehaviour = function() {
	var $ui = this;
	
	$(document).scroll(function(event) {
		return $ui._commonResizeAndScrollHandler(event);
	});
	
	$(".scrollable").on("scroll", function(event) {
		return $ui._commonResizeAndScrollHandler(event);
	});
};

UI.prototype._installDynamicResizeBehaviour = function() {
	var $ui = this;
	
	$(document).resize(function(event) {
		return $ui._commonResizeAndScrollHandler(event);
	});
};

UI.prototype._installDynamicClickBehaviour = function() {
	//Fixes a REALLY NASTY IE8 behaviour (bug?) with checkboxes / radio AND jQuery UI buttonset 
	if($$.Browser.msie && $$.Browser.version <= 8) {
		$(document).on("click", "label.ui-button[for]", function() {
			var $label = $(this);
						
			var $input = $("input#" + $label.attr("for"));
			
			if($input.size() > 0) {
				var isRadio = $input.is("[type='radio']");
				var checked = $input.is(":checked");
				
				if(isRadio || !checked) {
					$input.prop("checked", "checked");
					$input.attr("checked", "checked");
				} else {
					$input.removeAttr("checked");
					$input.removeProp("checked");
				}

				$input.change();
			}
			
			return true;
		});
	};
	
	$(document).on("click", ".selectableOnClick", function(event) {
		var domElement = $(this).get(0);
		
		if (document.selection) {
			var range = document.body.createTextRange();
			range.moveToElementText(domElement);
			range.select();
		} else if (window.getSelection) {
			var range = document.createRange();
			range.selectNode(domElement);
			window.getSelection().addRange(range);
		}
	});
	
	$(document).on("click", ".dropDownMenu", function(event) {
		var starter = $(this);
		var isMultiple = starter.hasClass("multiple");
				
		var menu = $(".menu", starter);
		
		if(!$$.isSet(menu.data("ui-menu"))) {
			menu = menu.menu();
			
			menu.data("starter", starter);
			menu.menu("widget").hide();
		} 
		
		var widget = menu.menu("widget");
		var widgetID = widget.attr("id");
		
		if(!isMultiple) {
			$(":data(ui-menu)").each(function() {
				var w = $(this).menu("widget");
				if(w.is(":visible") && w.attr("id") != widgetID)  
					w.hide();
			});
		}
		
		var widget = menu.menu('widget');
		
		widget.css("z-index", 12);
		widget.css("position", "fixed");
		widget.css("min-width", starter.width());
		widget.css("top", starter.offset().top + starter.height() - ( window.scrollY > 0 ? window.scrollY - 1 : 0 ));
		widget.css("left", starter.offset().left - window.scrollX);
		
		$("a", menu).css("min-width", starter.width);
		
		if(widget.is(":visible"))
			widget.hide();
		else
			widget.show();
		
		return true;
	});
};

UI.prototype._installDynamicMouseOverBehaviour = function() {
	$(document).on("mouseover", "[title]", function(event) {
		var $this = $(event.currentTarget);
				
		var isHTML = $$.TooltipManager._isHTML($this);
		
		if(!$$.UI.isTooltipped($this)) {
			$$.Log.debug("Applying tooltip behaviour on " + $this);
			
			$$.TooltipManager.apply($this);
		}
		
		if(isHTML)
			event.preventDefault();
		
		if(!isHTML || !$this.is("a"))
			$$.TooltipManager.show($this);
		
		return true;
	});
	
	$(document).on("mouseover", "a.external, a.externalStatic, a.newWin", function() {
		var $this = $(this);

		var metadata = $this.metadata();
		var target = $$.isSet(metadata) ? metadata.target : null;

		if($$.isSet(target))
			$this.attr("target", target);
		else
			$this.attr("target", "_BLANK");
		
		return true;
	});
	
	$(document).on("mouseover", ".hoverize", function() {
		$(this).addClass("ui-state-hover");
		
		return true;
	});
};

UI.prototype._installDynamicMouseOutBehaviour = function() {
	$(document).on("mouseout", ".hoverize", function() {
		$(this).removeClass("ui-state-hover");
		
		return true;
	});
};

UI.prototype._installDynamicBehaviour = function() {
	//ERROR DYNAMIC EVENTS
	$(document).on("error", "img.flag", function(event) {
		$$.Flags.handleMissingImage($(event.currentTarget).get(0));
	});	
	
	this._installDynamicResizeBehaviour();
	this._installDynamicScrollBehaviour();
	this._installDynamicClickBehaviour();
	this._installDynamicMouseOverBehaviour();
	this._installDynamicMouseOutBehaviour();
	
	$(document).on("dialogopen", ".ui-dialog-content", function(event) {
		//Handle dialog open (set hidden overflow on body?)
		
		/*
		var body = $("body");
//		var header = $("#commonHeader");
		
		body.data("overflow", body.css("overflow")); 
		body.css("overflow", "hidden");
		
		body.data("margin-right", body.css("margin-right"));
		body.css("margin-right", "16px");
//		
//		header.data("left", header.css("left")); 
//		header.css("left", "-16px");
		*/
	});
	
	$(document).on("dialogclose", ".ui-dialog-content", function(event) {
		//Handle dialog close (reset overflow on body?)
		/*
		var body = $("body");
//		var header = $("#commonHeader");
		
		body.css("overflow", $$.isSet(body.data("overflow")) ? body.data("overflow") : "auto");
		body.css("margin-right", body.data("margin-right"));
//		header.css("left", header.data("left"));
		*/
	});
};

UI.prototype._initializeExternalPlugins = function() {
	if($$.Browser.msie && $$.Browser.version < 9) 
		$$.TooltipManager.setManagerID("DEFAULT");

	$.blockUI.defaults.fadeIn = 0;
	$.blockUI.defaults.fadeOut = 0; 
	$.blockUI.defaults.theme = true;

	$.blockUI.defaults.overlayCSS.backgroundColor = '#335588';
	$.blockUI.defaults.overlayCSS.opacity = 0.2;
	$.blockUI.defaults.themedCSS.width = '40%';
	$.blockUI.defaults.themedCSS.left = '30%';
	$.blockUI.defaults.themedCSS.minHeight = '64px';
	$.blockUI.defaults.themedCSS.whiteSpace = 'nowrap';
};

UI.prototype._prepareStaticElements = function() {
	var html = $("html");
	
	html.addClass("ui-widget-content removeBackground").addClass(html.attr("dir"));

	$("fieldset").addClass("ui-widget-content");
	
	$("input[type='text'], " +
	  "input[type='password'], " +
	  "textarea").addClass("ui-corner-all");
			
	$("input[type='text']:disabled, " +
	  "input[type='password']:disabled, " +
	  "textarea:disabled").addClass("ui-state-disabled");
	
	$("legend.toggleable").
		append("<span title='" + $$.I18n.getText("vrmf.common.interface.ui.legend.toggler.tip") + "' class='status expanded right ui-helper-clearfix ui-icon ui-icon-carat-1-s'></span>").
		append("<span title='" + $$.I18n.getText("vrmf.common.interface.ui.legend.toggler.tip") + "' class='status collapsed right ui-helper-clearfix ui-icon ui-icon-carat-1-n'></span>");

	$("legend.sortable").
		prepend("<span title='" + $$.I18n.getText("vrmf.common.interface.ui.legend.handler.tip") + "' class='handle left ui-helper-clearfix ui-icon ui-icon-arrowthick-2-n-s'></span>");

	$("legend:not(.toggleable)").each(function() {
		var $legend = $(this);
		
		$legend.addClass("ui-corner-all");
		
		if(!$legend.hasClass("noHeader"))
			$legend.addClass("ui-widget-header");
	});

	$(".toRemove").remove();
	$(".buttonSet").buttonset();
};

UI.prototype._configureStaticElements = function() {
	var $ui = this;
	
	$(".datePicker").datepicker({
		dateFormat: 'yy/mm/dd',
		changeMonth: true,
		changeYear: true
	});
	
	$("input[type='text']:enabled, " +
	  "input[type='password']:enabled, " +
	  "textarea:enabled").each(function() {
		var $this = $(this);
		
		if(!$this.is(".defaultControls") && !$this.parents().is(".defaultControls")) {
			$this.addClass("ui-state-default");
			$this.focus(function() { $(this).addClass("ui-state-active"); });
			$this.blur(function() { $(this).removeClass("ui-state-active"); });
			$this.hover(
				function() { $(this).addClass("ui-state-hover"); },
				function() { $(this).removeClass("ui-state-hover"); }
			);
		}
	});
	
	$(".asButton, " +
	  "input[type='submit'], " +
	  "input[type='reset'], " +
	  "input[type='button'], " +
	  "input[type='file']").each(function() { 
		var $this = $(this);
				
		if(!$this.is(".defaultControls") && !$this.parents().is(".defaultControls"))
			$ui._buildUIButtons($this);
	});
	
	$ui._buildUIButtons($(".ui-buttonset :data(ui-button)"));
};

UI.prototype._buildUIButton = function(target) { 
	var $ui = this;
	var $target = $(target);
	
	try {
		var options = $ui._UI_BUTTONS_OPTIONS;
		
		var meta = $target.metadata();
		
		if($$.isSet(meta)) { 
			if($$.isSet(meta.buttonOptions)) {
				options = $.extend(true, {}, options, meta.buttonOptions);
			}

			if($$.isSet(meta.inline)) {
				if(!$$.isSet(options.icons)) {
					options.icons = {};
				}
			
				if(meta.inline) {
					options.icons.primary = 'ui-icon-arrowthickstop-1-s';
				} else {
					options.icons.primary = 'ui-icon-newwin';
				}
			}
		}
		
		$target = $target.button(options);
	} catch (E) {
		$$.Log.warn("Unable to parse metadata while building UI button: " + E);
	}
	
	return $target;
};

UI.prototype._buildUIButtons = function(target) {
	var $ui = this;
	
	target.each(function() {
		$ui._buildUIButton($(this));
	});
};

UI.prototype.isAutocomplete = function(target) {
	var $target = $(target);
	
	return $$.isSet($target) && 
		   $target.is(":data(ui-autocomplete)");
};

UI.prototype.isTooltipped = function(target) {
	var $target = $(target);
	
	return $$.isSet($target) && 
		   $target.is(":data(ui-tooltip)");
};

UI.prototype.isTab = function(target) {
	var $target = $(target);
	
	return $$.isSet($target) && 
		   $target.is(":data(ui-tabs)");
};

UI.prototype.isMultiselect = function(target) {
	var $target = $(target);
	
	return $$.isSet($target) && ( 
				$target.is(":data(echMultiselect)") || 
				$target.is(":data(multiselect)") 
		   );
};

UI.prototype.destroyAutocomplete = function(target) {
	var $target = $(target);
	
	if(this.isAutocomplete($target))
		$target.autocomplete("destroy");
	
	return $target;
};

UI.prototype.destroyMultiselect = function(target) {
	var $target = $(target);
	
	if(this.isMultiselect($target)) {
		var hasFilter = $target.data("echMultiselect").header.hasClass("ui-multiselect-hasfilter");
		
		if(hasFilter)
			$target.multiselectfilter("destroy");
		
		$target.multiselect("destroy");
	}
	
	return $target;
};

UI.prototype.enableTabs = function(tabs, tabIDs) {
	var disabled = tabs.tabs("option", "disabled");
	
	if(disabled == null)
		disabled = [];
	
	disabled = new $$.Set(disabled);
	
	var toEnable = [];
	
	if(typeof tabIDs === "number")
		toEnable.push(tabIDs);
	else if($.isArray(tabIDs))
		toEnable.push.apply(toEnable, tabIDs);

	for(var t=0; t<toEnable.length; t++)
		disabled.remove(toEnable[t]);
	
	$(tabs).tabs("option", "disabled", disabled.asArray());
	
	return tabs;
};

UI.prototype.disableTabs = function(tabs, tabIDs) {
	var disabled = tabs.tabs("option", "disabled");
	
	var toDisable = disabled == null ? [] : $.isArray(disabled) ? disabled : [ disabled ];
		
	if(typeof tabIDs === "number")
		toDisable.push(tabIDs);
	else if($.isArray(tabIDs))
		toDisable.push.apply(toDisable, tabIDs);
	
	var disabledSet = new $$.Set();
	disabledSet.addAll(toDisable);

	disabledSet = disabledSet.asArray();
	
	tabs.tabs("option", "disabled", disabledSet);

	return tabs;
};

$.extend(true, $$, {
	UI: new UI()
});