//Filters for displaying current data only
function typedDataFilter(currentResults, data) {
	var alreadyStored = new Array();
	
	for(var r=0; r<currentResults.length; r++) {
		alreadyStored.push(currentResults[r].typeId);
	}
	
	return alreadyStored.indexOf(data.typeId) < 0;
}

function powerTypeFilter(currentResults, data) {
	var mainEngineStore = new Array();
	var auxEnginesTypeStore = new Array();
	
	for(var r=0; r<currentResults.length; r++) {
		if(currentResults[r].mainEngine)
			mainEngineStore.push(currentResults[r].typeId);
		else
			auxEnginesTypeStore.push(currentResults[r].typeId);
	}
	
	if(data.mainEngine)
		return mainEngineStore.indexOf(data.typeId) < 0;
	
	return auxEnginesTypeStore.indexOf(data.typeId) < 0;
};

function gearTypeFilter(currentResults, data) {
	var alreadyStored = new Array();
	
	for(var r=0; r<currentResults.length; r++) {
		alreadyStored.push(currentResults[r].primaryGear);
	}
	
	return alreadyStored.indexOf(data.primaryGear) < 0;
};

function authorizationTypeFilter(currentResults, data) {
	var alreadyStored = new Array();
	
	for(var r=0; r<currentResults.length; r++) {
		alreadyStored.push(currentResults[r].typeId + "_" + currentResults[r].vesselId);
	}
	
	return alreadyStored.indexOf(data.typeId + "_" + data.vesselId) < 0;
};

function authorizationsFilter(currentResults, data) {
	var sourceSystemStore = new Array();

	for(var a=0; a<currentResults.length; a++) {
		sourceSystemStore.push(currentResults[a].sourceSystem);
	}

	return sourceSystemStore.indexOf(data.sourceSystem) < 0;
}