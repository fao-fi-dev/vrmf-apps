$$.Dep.require("vrmf.framework.externalReferences.urls",{ "vrmf.framework.externalReferences": { identifier: "$$.vrmf.ExternalReferences" } }, function() {
	$$.vrmf.ExternalReferences.URLS = {
		ID: {
			CCAMLR   : "http://www.ccamlr.org/en/node/{query}",
			CCSBT    : "http://www.ccsbt.org/site/authorised_vessels_detail.php?id={query}",
			EU       : {
				EU          : "http://ec.europa.eu/fisheries/fleet/index.cfm?method=Search.ListSearchSimpleOneVessel&COUNTRY_CODE=&CFR_CODE={query}&search_type=CFR", 
				FISHSUBSIDY : "http://fishsubsidy.org/{query}",
				ESP         : "http://www.magrama.gob.es/gl/pesca/temas/la-pesca-en-espana/censo-de-la-flota-pesquera/consulta.asp?cod={query}&cd_matr=Consultar"
			},
			FFA      : "http://www.ffa.int/node/42",
			HSVAR    : "http://www.fao.org/figis/vrmf/hsvar/display/uid/{query}",
			IATTC    : "http://www.iattc.org/VesselRegister/VesselDetails.aspx?VesNo={query}&Lang=en",
			ICCAT    : "http://www.iccat.int/en/VesselsRecordRes.asp?cajaRecord=checkbox&cajaFlag=checkbox&cajaFlag2=checkbox&cajaVessel=checkbox&cajaIRCS=checkbox&cajaRegistry=checkbox&textSN={query}&selectOrder=1&selectOrder2=6&selectInterval=25&Submit=Search",
			//IOTC     : "http://www.iotc.org/English/record/record_vessel3.php?vid={query}",
			IOTC     : "http://www.iotc.org/vessels/history/{query}",
			ISSF     : "http://iss-foundation.org/imo-database",
			SICA     : "http://www.sica.int/WSIRPAC/Paginas/Formularios/Form_Embarcaciones.aspx",
			SPRFMO   : "http://www.sprfmo.org",
			WCPFC    : "http://intra.wcpfc.int/Lists/Vessels/DispForm.aspx?ID={query}&Source=http%3A%2F%2Fintra%2Ewcpfc%2Eint%2FLists%2FVessels%2FStats%2Easpx",
			ADFG     : "http://www.adfg.alaska.gov/index.cfm?adfg=fishlicense.reporting",
			TCGC     : "http://wwwapps.tc.gc.ca/Saf-Sec-Sur/4/vrqs-srib/eng/vessel-registrations/details/{query}",
			TUVI: {
				CLAV     : "http://www.iattc.org/GVR/duplicatedvessels.aspx?IDDup={query}",
				NCLAV    : "http://www.fao.org/figis/vrmf/nclav/services/public/display/vessel/TUVI/{query}"
			},
			IMO: {
				WORLD_SHIPS       : "http://www.world-ships.com/?p=new%2F&Ship%5Bship_name%5D=&Ship%5Blast_ex_name%5D=&Ship%5Bimo%5D={query}&Ship%5Bmaritime_mobile%5D=&Ship%5Bship_type%5D=&Ship%5Bflag%5D=&Ship%5Bclass1%5D=&Ship%5Bcall_sign%5D=&Ship%5Bengine_builder%5D=&Ship%5Bbuilder%5D=&Ship%5Bowner%5D=&Ship%5Bmanager%5D=&Ship%5Bfdwt%5D=&Ship%5Bfgt%5D=&Ship%5Bfnrt%5D=&Ship%5Bfteu%5D=&Ship%5Bfloa%5D=&Ship%5Bfbeam%5D=&Ship%5Bfdraft%5D=&Ship%5Bfbuilt%5D=&Ship%5Bfengine_kw_total%5D=&Ship%5Bfengine_hp_total%5D=&Ship%5Btdwt%5D=&Ship%5Btgt%5D=&Ship%5Btnrt%5D=&Ship%5Btteu%5D=&Ship%5Btloa%5D=&Ship%5Btbeam%5D=&Ship%5Btdraft%5D=&Ship%5Btbuilt%5D=&Ship%5Btengine_kw_total%5D=&Ship%5Btengine_hp_total%5D=&Ship%5BperPage%5D=50&ajax=ship-grid",
				MARINE_TRAFFIC    : "http://www.marinetraffic.com/ais/datasheet.aspx?SHIPNAME=&IMO={query}&TYPE_SUMMARY=&DESTINATION=&menuid=&datasource=V_SHIP&app=&mode=&B1=Search",
				SHIP_SPOTTING     : "http://www.shipspotting.com/gallery/photo_search.php?query={query}",
				EQUASIS           : "http://www.equasis.org/EquasisWeb/restricted/ShipList?fs=ShipSearch&P_IMO={query}",
				WIKIMEDIA         : "http://commons.wikimedia.org/wiki/Category:IMO_{query}",
				SHIPTRACKING      : "http://www.shiptracking.eu/ais/#/vesselsearch?part={query}",
				MARITIME_CONNECTOR: "http://maritime-connector.com/ship-search/?imo={query}",
				IMO_NUMBER        : "http://www.imonumber.com/site/details?imo={query}",
				SHIP_LIST         : "http://www.shiplist.vitron.org/ship/imo/{query}",
				VT_EXPLORER       : "http://vessels.vtexplorer.com/vessel/0-IMO-{query}",
				SHIPPING_EXPLORER : "http://www.shippingexplorer.net/en/vessels/search?s={query}&Area=3"
			}
		},
		IRCS: {
			ITU               : "http://www.itu.int/online/mms/mars/ship_search.sh?lang=en&lgg=7&p=1&cgaid=0&sh_callsign={query}&sh_epirb_hex=&sh_epirb_id=&sh_mmsi=&sh_name=",
			SHIP_SPOTTING     : "http://www.shipspotting.com/gallery/extra_search.php?callsign={query}",
			SHIPPING_EXPLORER : "http://www.shippingexplorer.net/en/vessels/search?s={query}&Area=4&VesselType=15",
			WORLD_FISHING     : "http://www.worldfishingtoday.com/fishingboatinfo/default.asp?soeg={query}&mode=soeg",
			WCPFC             : "http://intra.wcpfc.int/_layouts/searchresults.aspx?k={query}&u=http%3A%2F%2Fintra%2Ewcpfc%2Eint%2FLists%2FVessels"
		},
		MMSI: {
			ITU                  : "http://www.itu.int/online/mms/mars/ship_search.sh?lang=en&lgg=7&p=1&cgaid=0&sh_callsign=&sh_epirb_hex=&sh_epirb_id=&sh_mmsi={query}&sh_name=",
			MARINE_TRAFFIC       : "http://www.marinetraffic.com/ais/datasheet.aspx?SHIPNAME=&IMO={query}&TYPE_SUMMARY=&DESTINATION=&menuid=&datasource=V_SHIP&app=&mode=&B1=Search",
			MARINE_TRAFFIC_TRACK : "http://www.marinetraffic.com/ais/datasheet.aspx?datasource=ITINERARIES&MMSI={query}",
			SHIP_SPOTTING        : "http://www.shipspotting.com/gallery/extra_search.php?mmsi={query}",
			SHIPTRACKING         : "http://www.shiptracking.eu/ais/#/getvesseldetails?mmsi={query}",
			VT_EXPLORER          : "http://vessels.vtexplorer.com/vessels/list?search_text={query}",
			SHIPPING_EXPLORER    : "http://www.shippingexplorer.net/en/vessels/search?s={query}&Area=2&VesselType=15"
		},
		REG_NO: {
			//CAN : "http://wwwapps.tc.gc.ca/Saf-Sec-Sur/4/vrqs-srib/d.aspx?lang=e&shipid={query}",
			CAN : "http://wwwapps.tc.gc.ca/Saf-Sec-Sur/4/vrqs-srib/eng/vessel-registrations/details/{query}",
			USA : "http://www.st.nmfs.noaa.gov/pls/webpls/cgv_pkg.vessel_id_list?vessel_id_in={query}",
			ESP : "http://www.magrama.gob.es/gl/pesca/temas/la-pesca-en-espana/censo-de-la-flota-pesquera/consulta.asp"
		}
	};
});