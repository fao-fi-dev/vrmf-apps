function PicturesManager(baseURL) {
	$$.Log.debug("Initializing 'PicturesManager' Object...");

	this.timeout = 25000; //25 seconds
	this.baseURL = "/services/vessels/search/";
	this.cacheRequest = false;

	if($$.isSet(baseURL))
		this.baseURL = baseURL;
	
	this.lastRequestToken = null;
};

PicturesManager.prototype.setBaseURL = function(baseURL) {
	this.baseURL = baseURL;
};

PicturesManager.prototype.setTimeout = function(timeout) {
	this.timeout = timeout;
};

PicturesManager.prototype.getTimeout = function() {
	return this.timeout;
};

PicturesManager.prototype.setCacheRequest = function(flag) {
	this.cacheRequest = flag;
};

PicturesManager.prototype.startLoading = function(options) {
	var defaults = { 
		container: $("#pictureFrame"),
		title: "<h2>" + $$.I18n.getText("vrmf.vessels.manager.pictures.wait.please") + "</h2>",
		message: "<span>" + $$.I18n.getText("vrmf.vessels.manager.pictures.loading") + "&nbsp;<span class='spinner'>&nbsp;</span></span>"
	};
	
	if($$.isSet(options))
		defaults = $.extend(defaults, options);
	
	$(defaults.container).block({ 
		title: defaults.title, 
		message: defaults.message 
	});
};

PicturesManager.prototype.stopLoading = function(options) {
	var defaults = { 
		container: $("#pictureFrame")
	};
	
	if($$.isSet(options))
		defaults = $.extend(defaults, options);
	
	$(defaults.container).unblock();
};

PicturesManager.prototype.searchPictures = function (vesselIdentification, vesselData, callback, timeoutCallback) {
	var self = this;

	var currentTimeout = this.timeout;
	var currentCacheRequest = this.cacheRequest;

	$("#vesselPicture").load(function() {
		self.stopLoading();
		
		self.updatePicturesMetadata($(this).data("thumb"), vesselData);
		
		var header = $(".vesselPictureDetailsContainer .ui-widget-header");
		var controls = $(".controls", header);
		
		if(header.hasClass("collapsed"))
			controls.click();

		$$.Log.info("Loaded picture " + $(this).attr("src"));

		var $this = $(this);
		var img = $this.get(0);

		try {
			$(".vesselPictureDetailsContainer .actualPictureSize").text(img.width + " x " + img.height + " pixels");
		} catch (E) {
			$$.Log.error("Unable to compute picture size...");
		}

		var width = $this.css("width");
		width = $$.rawTrim(width.replace(/[a-zA-Z]/g, ""));

		width = $$.isSet(width) && width != "0" ? parseInt(width, 10) : "95%";

		var detailsContainer = $(".vesselPictureDetailsContainer");
		detailsContainer.css("width", width).fadeIn();
	});
	
	var now = new Date();

	self.lastRequestToken = now.getTime();
	
	var customHeaders = [];
	customHeaders[_VRMF_SEARCH_REQUEST_TOKEN_HEADER] = self.lastRequestToken;
	
	$.ajax({
		async: true,
		cache: currentCacheRequest,
		url: this.baseURL + "pictures/" + vesselIdentification.by + "/" + vesselIdentification.id + ".json",
		data: { user: $$.UserManager.getLoggedUser().getId() },
		timeout: currentTimeout,
		headers: customHeaders, 
		success: function(result, textStatus, JQXHR) {
			var responseHeader = JQXHR.getResponseHeader(_VRMF_SEARCH_REQUEST_TOKEN_HEADER);
			
			this.lastRequestToken = responseHeader;
			
			if(responseHeader == self.lastRequestToken) {
				var pid = 0;
	
				var pictures = new Array();
				var provider = {};
	
				for(var r=0; r<result.data.length; r++) {
					provider = result.data[r];
	
					for(var i=0; i<provider.IDs.length; i++) {
						pid++;
	
						var title = $$.I18n.getText("vrmf.vessels.manager.pictures.thumb.tip", provider.sourceID, i + 1, provider.criteria);
	
						var picture = $("<img id='picture_" + pid + "' class='vesselPicture'/>");
	
						picture.load(function() {
							self.stopLoading();
							
							$$.Log.debug("Loaded content for picture " + $(this).attr('data-original'));
	
							var img = $(this).get(0);
	
							$(".pictureWidth").text(img.width);
							$(".pictureHeight").text(img.height);
	
							$$.Log.debug("Original picture width: " + img.width + ", height: " + img.height);
						});
	
						picture.error(function() {
							self.stopLoading();
							
							$(this).remove();
						});
						
						picture.attr("src", provider.fullURL + provider.IDs[i]);
						picture.attr("title", title);
	
						var thumb = $("<img id='thumb_" + i + "' class='thumbs'/>");
						thumb.attr("data-original", provider.thumbsURL + provider.IDs[i]);
						thumb.data("id", provider.IDs[i]);
						thumb.data("pid", pid);

						thumb.data("provider", provider);
	
						thumb.attr("title", picture.attr("title"));
	
						thumb.error(function() {
							$(this).remove();
						});
	
						pictures.push({ thumb: thumb, picture: picture });
					}
				} 
				
				callback(pictures, vesselData);
			} else {
				$$.Log.warn("Skipping pictures display as they refer to a previous request (current token: " + self.lastRequestToken + ", returned token: " + this.lastRequestToken + ")");
			}
		}, 
		complete: function() {
			//$$.Log.warn("Skipping pictures display as they refer to a previous request (current token: " + self.lastRequestToken + ", returned token: " + this.lastRequestToken + ")");
		},
		_onErrorCallback: timeoutCallback
	});
};

PicturesManager.prototype.applyPictureDetailsBehaviour = function(control, container) {
	var globalContainer = $(".vesselPictureDetailsContainer");
	var header = $("div.ui-widget-header", globalContainer);
	var currentControl = $$.isSet(control) ? $(control) : $(".controls", header);
	var currentContainer = $$.isSet(container) ? $(container) : $(".allDetails", globalContainer);
	
	currentControl.unbind("click").click(function() {
		var isExpanded = header.hasClass("expanded");

		header.toggleClass("expanded").toggleClass("collapsed");

		if(isExpanded)
			currentContainer.slideUp();
		else
			currentContainer.slideDown();
	});
	
	header.unbind("dblclick").dblclick(function() {
		currentControl.click();
	});
	
	globalContainer.draggable({ axis: 'y', containment: 'parent', handle: ".ui-widget-header" });
};

PicturesManager.prototype.updatePicturesMetadata = function(thumb, vesselData) {
	if($$.isSet(thumb)) {
		var provider = thumb.data("provider");
	
		var id = thumb.data("id");
		var pid = thumb.data("pid");
	
		if(!$$.isSet(provider)) {
			return $("<p class='pictureDetails error'>" + $$.I18n.getText("vrmf.vessels.manager.pictures.thumb.error.tip", id, pid) + "</p>");
		} 
	
		var container = $(".vesselPictureDetailsContainer");
		var attribution = $(".attribution", container);
		var sourceURL = $(".sourceURL a", container);
		var pictureURL = $(".pictureURL a", container);
		var criteria = $(".identificationCriteria", container);
		var searchImage = $(".searchImage", container);
	
		$(".attributionType", attribution).text($$.I18n.getText("vrmf.vessels.manager.pictures.attribution." + ( provider.isPublic ? "courtesy" : "copyright" )));
		$("a", attribution).attr("href", provider.source).text(provider.source);
	
		sourceURL.attr("href", provider.parentURL).text(provider.parentURL);
		pictureURL.attr("href", provider.fullURL + id).text(provider.fullURL + id);
	
		var criteriaMessage = $("<span>" + provider.criteria + "</span>");
	
		if(provider.criteria.indexOf("Name = ") == 0) {
			var messageText = $$.I18n.getText("vrmf.vessels.manager.pictures.warning.name");
			criteriaMessage.append("<strong class=\"hint\" " +
											$$.TooltipManager.TOOLTIP_CONFIGURATION_ATTRIBUTE + "=\"{ position: { my: 'center top', at: 'center bottom' } }\" title=\"" + messageText + "\">&#160;[&#160;" + messageText + "&#160;]</strong>");
		}
	
		criteria.empty().append(criteriaMessage);
	
		if($$.isSet(vesselData) &&
		   $$.isSet(vesselData.nameData) &&
		   $$.isSet(vesselData.nameData[0].name))
			vesselName = vesselData.nameData[0].name;
	
		var searchImageLink = "http://images.google.com/searchbyimage?hl=ensite=search&image_url=" + provider.fullURL + id + ( $$.isSet(vesselName) ? "&q=" + encodeURI(vesselName) : "");
	
		$("a", searchImage).attr("href", searchImageLink);
	
		$$.TooltipManager.apply($("[title]", container));
	}
};

$$.Dep.require("vrmf.vessels.manager.pictures", { "vrmf.tooltip.manager": { identifier: "$$.TooltipManager" } }, function() {
	$.extend($$, {
		PicturesManager: new PicturesManager()
	});	
});