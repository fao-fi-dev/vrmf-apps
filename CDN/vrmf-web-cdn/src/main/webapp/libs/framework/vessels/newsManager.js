window.newsManagerIntermediateCallback = function(contextValue, responseObject, responseStatus, errorDetails) {
	$$.NewsManager.completeRequest(contextValue, responseObject, responseStatus, errorDetails);
}; 
	
function NewsManager(searchAPIURL, version, clientIP) {
	$$.Log.debug("Initializing 'NewsManager' Object...");

	this.searchAPIURL = $$.isSet(searchAPIURL) ? searchAPIURL : "https://ajax.googleapis.com/ajax/services/search/news";
	this.version = $$.isSet(version) ? version : "1.0";
	this.clientIP = $$.isSet(clientIP) ? clientIP : null;
	
	this.data = {};
	this.numberOfTerms = 0;
	
	this.intermediateCallbackName = "newsManagerIntermediateCallback";
};

NewsManager.prototype.setSearchAPIURL = function(searchAPIURL) {
	this.searchAPIURL = searchAPIURL;
};

NewsManager.prototype.setVersion = function(version) {
	this.version = version;
};

NewsManager.prototype.setClientIP = function(clientIP) {
	this.clientIP = clientIP;
};

NewsManager.prototype.search = function(searchTerms, completionCallback) {
	var $this = this;
	
	$this.data = {};
	$this.numberOfTerms = 1; //searchTerms.length;
	$this.completionCallback = completionCallback;
	
	for(var s=0; s<searchTerms.length && s==0; s++) {
		var data = {
			v: $this.version,
			q: "Fishing vessel " + searchTerms[s]
		};
		
		if($$.isSet($this.clientIP))
			data.userip = $this.clientIP;
		
		$.ajax({
			url: this.searchAPIURL,
			data: data,
			dataType: 'jsonp',
			jsonpCallback: $this.intermediateCallbackName
		});
	}
};

/** See: https://developers.google.com/news-search/v1/jsondevguide#json_args */
NewsManager.prototype.completeRequest = function(responseObject, responseStatus, errorDetails) {
	var $this = $$.NewsManager;
	
	$this.numberOfTerms--;
	
	if(responseObject.responseStatus == 200)
		$this.data = responseObject;

	if($this.numberOfTerms == 0)
		$this.completionCallback($this.data);
};

$.extend($$, {
	NewsManager: new NewsManager()
});