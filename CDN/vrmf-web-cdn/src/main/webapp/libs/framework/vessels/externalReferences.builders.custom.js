$$.Dep.require("vrmf.framework.externalReferences.builders.custom", { "vrmf.framework.externalReferences": { identifier: "$$.vrmf.ExternalReferences" } }, function() {
	$$.vrmf.ExternalReferences.BUILDERS = $.extend(true, {}, $$.vrmf.ExternalReferences.BUILDERS, {
		REG_NO: {
			ESP: {
				linkBuilder: $$.vrmf.ExternalReferences._buildPOSTExternalLink
			}
		}
	});
});