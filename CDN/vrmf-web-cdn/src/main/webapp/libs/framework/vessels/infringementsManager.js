function InfringementsManager(baseURL) {
	$$.Log.debug("Initializing 'InfringementsManager' Object...");

	this.timeout = 25000; //25 seconds
	this.baseURL = "/services/vessels/search/";
	this.cacheRequest = false;
	this.infringements = null;
	this.lastInfoWindow = null;

	if($$.isSet(baseURL))
		this.baseURL = baseURL;

	this.lastRequestToken = null;
};

InfringementsManager.prototype.setBaseURL = function(baseURL) {
	this.baseURL = baseURL;
};

InfringementsManager.prototype.setTimeout = function(timeout) {
	this.timeout = timeout;
};

InfringementsManager.prototype.setCacheRequest = function(flag) {
	this.cacheRequest = flag;
};

InfringementsManager.prototype.getInfringements = function() {
	return this.shipTracks;
};

InfringementsManager.prototype.searchInfringements = function (vesselIdentification, vesselData, callback) {
	var currentTimeout = this.timeout;
	var currentCacheRequest = this.cacheRequest;

	var $this = this;

	var now = new Date();

	$this.lastRequestToken = now.getTime();

	var customHeaders = [];
	customHeaders[_VRMF_SEARCH_REQUEST_TOKEN_HEADER] = $this.lastRequestToken;

	$.ajax({
		async: true,
		cache: currentCacheRequest,
		url: this.baseURL + "infringements/" + vesselIdentification.by + "/" + vesselIdentification.id + ".json",
		data: { user: $$.UserManager.getLoggedUser().getId() },
		timeout: currentTimeout,
		headers: customHeaders,
		success: function(response, textStatus, JQXHR) {
			var responseHeader = JQXHR.getResponseHeader(_VRMF_SEARCH_REQUEST_TOKEN_HEADER);

			this.lastRequestToken = responseHeader;

			if(responseHeader == $this.lastRequestToken) {
				var infringements = response.data;

				var filtered = new Array();

				for(var p=0; p<infringements.length; p++) {
					if(!$$.isEmpty(infringements[p].details))
						filtered.push(infringements[p]);
				}

				$this.infringements = $$.isEmpty(filtered) ? null : filtered;

				callback($this.infringements, vesselData);
			} else
				$$.Log.warn("Skipping infringements display as they refer to a previous request (current token: " + $this.lastRequestToken + ", returned token: " + this.lastRequestToken + ")");
		},
		error: $$.Ajax._timeoutDumperErrorManager,
		complete: function() {
			//$$.Log.warn("Skipping infringements display as they refer to a previous request (current token: " + $this.lastRequestToken + ", returned token: " + this.lastRequestToken + ")");
		}
	});
};

InfringementsManager.prototype.displayInfringements = function(container, vesselData, infringements) {
	container.empty();

	var infringementsToDisplay = $$.isSet(infringements) ? infringements : this.infringements;

	for(var t=0; t<infringementsToDisplay.length; t++) {
		infringement = infringementsToDisplay[t];
		
		container.append("<h1 class='infringementsDetails ui-widget-header'><b>[ #" + ( t + 1 ) + " ] </b><b>Source: </b>" + infringement.source + " - <b>Criteria: </b>" + infringement.criteria + " - <b>URL: </b><a target='_BLANK' href='" + infringement.sourceURL + "'>" + infringement.sourceURL + "</a></h1>");
		container.append("<div class='infringementDetail'>" + infringement.details + "</div>").append("<hr/>");
	};
};

$.extend($$, {
	InfringementsManager: new InfringementsManager()
});