function ExternalReferences() {
	this.BUILDERS = {};
	this.URLS = {};
	this.PREPROCESSORS = {};
	this.POSTPROCESSORS = {};
};

var FIGIS_VESSEL_TYPE_DETAIL_URL = "http://www.fao.org/fishery/vesseltype/{query}/en";
var FIGIS_GEAR_TYPE_DETAIL_URL = "http://www.fao.org/fishery/geartype/{query}/en";
var FIGIS_PSM_DETAIL_URL = "http://www.fao.org/fi/website/FISearchAction.do?dslist[0]=staticxml.psm&refxml=false&lixsl=webapps/figis/shared/xsl/search_result.xsl&kv[1]={query}&loadMenu=PSM";
var FIGIS_SPECIES_BY_3A_URL = "http://www.fao.org/fishery/species/search/3alpha/{query}/en";
var FIGIS_SPECIES_SEARCH_URL = "http://www.fao.org/fi/website/FISearchAction.do?dslist[0]=species&refxml=true&loadMenu=FishFinder&lixsl=webapps/figis/shared/xsl/search_result.xsl&kv[0]={query}";

var EXTERNAL_SEARCH_WITHOUT_ID_SOURCES = [ "ISSF", "SICA", "CTMFM", "SPRFMO" ];

ExternalReferences.prototype.getDetailByIDURLS = function(source) {
	return $$.isSet(source) ? this.DETAILS_BY_ID_URLS[source] : this.DETAILS_BY_ID_URLS;
};

ExternalReferences.prototype.getDetailByIMOURLS = function(service) {
	return $$.isSet(service) ? this.DETAILS_BY_IMO_URLS[service] : this.DETAILS_BY_IMO_URLS;
};

ExternalReferences.prototype.getDetailByMMSIURLS = function(service) {
	return $$.isSet(service) ? this.DETAILS_BY_MMSI_URLS[service] : this.DETAILS_BY_MMSI_URLS;
};	

ExternalReferences.prototype.getDetailByIRCSURLS = function(service) {
	return $$.isSet(service) ? this.DETAILS_BY_IRCS_URLS[service] : this.DETAILS_BY_IRCS_URLS;
};	

ExternalReferences.prototype.getDetailByEUCFRURLS = function(service) {
	return $$.isSet(service) ? this.DETAILS_BY_EUCFR_URLS[service] : this.DETAILS_BY_EUCFR_URLS;
};

ExternalReferences.prototype.getDetailByREGNOURLS = function(service) {
	return $$.isSet(service) ? this.DETAILS_BY_REG_NO_URLS[service] : this.DETAILS_BY_REG_NO_URLS;
};

ExternalReferences.prototype.showExternalDetails = function (URL, id) {
	window.open(URL.replace("\{query\}", id), "_blank");
	
	return false;
};

ExternalReferences.prototype._filterData = function (data, selectors) {
	if($$.isSet(data)) {
		if(arguments.length > 1) {
			var node = data;
			for(var a=1; a<arguments.length; a++) {
				try {
					if($$.isSet(arguments[a]))
						node = node[arguments[a]];
				} catch (E) {
					
				}
			}
			
			return node;
		} else
			return data;
	}
	
	return null;
};

ExternalReferences.prototype._isMultipleBuilder = function(builder) {
	if($$.isSet(builder) && typeof builder == "object") {
		for(var key in builder) {
			if($$.isSet(builder[key]) && typeof builder[key] === "object")
				return true;
		}
	}
	
	return false;
};

ExternalReferences.prototype.getLinkBuilders = function (category, identifierType, discriminator) {
	if(!$$.isSet(category))
		return null;
	
	var normalizedType = identifierType;
	
	if($$.isSet(normalizedType)) {
		normalizedType = normalizedType.replace(/_ID$/, "").replace(/_CFR$/, "");
	}

	var filtered = this._filterData(this.BUILDERS, category, normalizedType, discriminator);
	
	var builders = [];
	
	if($$.isSet(filtered)) {
		if(this._isMultipleBuilder(filtered)) {
			var discriminated;
			for(var key in filtered) {
				discriminated = filtered[key];
				
				discriminated.category = category;
				discriminated.type = normalizedType;
				discriminated.discriminator = key;
				discriminated.preprocessor = this._filterData(this.PREPROCESSORS, category, normalizedType, key);
				discriminated.postprocessor = this._filterData(this.POSTPROCESSORS, category, normalizedType, key);
				discriminated.url = this._filterData(this.URLS, category, normalizedType, key);
				
				builders.push(discriminated);
			}
		} else {
			filtered.category = category;
			filtered.type = normalizedType;
			filtered.preprocessor = this._filterData(this.PREPROCESSORS, category, normalizedType, discriminator);
			filtered.postprocessor = this._filterData(this.POSTPROCESSORS, category, normalizedType, discriminator);
			filtered.url = this._filterData(this.URLS, category, normalizedType, discriminator);
			
			builders.push(filtered);
		}
	}
	
	return builders;
};

ExternalReferences.prototype.buildLink = function(type, configuration, data, defaultPreprocessor) {
	var _category = configuration.category;
	var _type = configuration.type;
	var _discriminator = configuration.discriminator;
	var _preprocessor = configuration.preprocessor; 
	var _postprocessor = configuration.postprocessor;
	var _url = configuration.url;
	
	var _bundlePrefix = "vrmf.vessel.external." + _category + 
												  ( $$.isSet(_type) ? "." + _type : "" ) + 
												  ( $$.isSet(_discriminator) ? "." + _discriminator : "" );
	
	var _title = $$.I18n.getText(_bundlePrefix + ".description");
	var _text = $$.I18n.getText(_bundlePrefix + ".label");

	var _linkBuilder = configuration.linkBuilder;
	
	var _preprocessedData = data;
	
	if($$.isSet(_preprocessor)) {
		_preprocessedData = _preprocessor(data);
	} else if($$.isSet(defaultPreprocessor)) {
		_preprocessedData = defaultPreprocessor(data);
	}
	
	var _target = "_" + 
				  ( $$.isSet(_discriminator) ? _discriminator + "_" : "" ) + 
				  ( $$.isSet(type) ? type + "_" : "" );
	
	if(_preprocessedData == null) {
		_target += new Date().getTime();
	} else if(typeof _preprocessedData === "string") {
		_url = _url.replace("\{query\}", _preprocessedData);

		_target += _preprocessedData.replace(/\s|\//g, "_");
	} else if(typeof _preprocessedData === "object") {
		for(var key in _preprocessedData) {
			if($$.isSet(_preprocessedData[key]) && $$.isSet(_preprocessedData[key].value)) {
				_target += _preprocessedData[key].value.replace(/\s|\//g, "_") + "_";
			}
		}
	}
	
	if($$.isSet(_postprocessor)) {
		_url = _postprocessor(_url);
	}
	
	if($$.isSet(_linkBuilder))
		return _linkBuilder(_preprocessedData, data, _url, _text, _title, _target);
	
	return this._buildExternalLink(_preprocessedData, data, _url, _text, _title, _target);
};

ExternalReferences.prototype._buildPseudoExternalLink = function(tooltip) {
	var link = $("<a href=\"#\" class=\"textLink external\" onclick=\"javascript:return false;\"/>");
	
	link.attr("title", $$.isSet(tooltip) ? tooltip : "Search the web for this data by opening an external link into a separate window");
	
	return link;
};

ExternalReferences.prototype._buildEmptyExternalLink = function(tooltip) {
	var link = $("<a href=\"#\" class=\"textLink external\" target=\"_blank\"/>");

	link.attr("title", $$.isSet(tooltip) ? tooltip : "Search the web for this data by opening an external link into a separate window");

	return link;
};

ExternalReferences.prototype._buildExternalLink = function (data, originalData, url, text, title, target) {
	if(data == null)
		return null;
	
	var link = this._buildEmptyExternalLink(title);

	link.attr("target", target);
	link.attr("href", url.replace(/\{query\}/g, data));
	link.addClass("{ target: '" + target + "' }");

	link.text(text);
	
	return link;
};

ExternalReferences.prototype._buildPOSTExternalLink = function (data, originalData, url, text, title, target) {
	if(data == null)
		return null;
	
	var link = $$.vrmf.ExternalReferences._buildPseudoExternalLink(title);
	
	var ID = "_POST_LINK_FORM_";

	if(data == null)
		ID += new Date().getTime();
	else if(typeof data === "string")
		ID += data.replace(/\s|\//g, "_");
	else if(typeof data === "object") {
		for(var key in data) {
			if($$.isSet(data[key]) && $$.isSet(data[key].value)) {
				ID += data[key].value.replace(/\s|\//g, "_") + "_";
			} 
		}
	}
	
	if("_POST_LINK_FORM_" === ID)
		ID += new Date().getTime();
	
	//Link can be effectively followed only if it POSTs data to the data owner server.
	//Hence, we need to simulate sending a form in this messy way...
	var form = $("<form class='hidden'/>");
	
	form.attr("id", ID);
	form.attr("name", ID);
	form.attr("method", "POST");
	form.attr("action", url.replace(/\{query\}/g, data));
	form.attr("target", target);
	
	if($$.isSet(data) && typeof data === "object") {
		var param, input;
		
		for(var key in data) {
			param = data[key];
			
			if($$.isSet(param.name)) {
				input = $("<input name='" + param.name + "' " +
								 "type='" + ( $$.isSet(param) && $$.isSet(param.type) ? param.type : "text" ) + "' " +
								 "value='" + ( $$.isSet(param) && $$.isSet(param.value) ? param.value : "" ) + "'/>");
			}

			form.append(input);
		}
	}

	$("body").append(form);
	link.text(text);
	link.click(function() {
		$("form#" + ID).submit();
		
		return false;
	});
	
	return link;
};

$$.Dep.require("vrmf.framework.externalReferences", { "vrmf.utils": { identifier: "$$.Utils" }}, function() { 
	$$.vrmf.registerComponent("ExternalReferences", new ExternalReferences());
});