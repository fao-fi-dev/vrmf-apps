/** Functions to be overridden by custom implementations - START */
function getDefaultStatusClasses() {
	$$.Log.warn("You must override the 'getDefaultStatusClasses' method in order to get the expected results...");

	return "";
};

function getDefaultHoverClasses() {
	$$.Log.warn("You must override the 'getDefaultHoverClasses' method in order to get the expected results...");

	return "";
};

function getDefaultDisabledClasses() {
	$$.Log.warn("You must override the 'getDefaultDisabledClasses' method in order to get the expected results...");

	return "";
};

function getDefaultActiveClasses() {
	$$.Log.warn("You must override the 'getDefaultActiveClasses' method in order to get the expected results...");

	return "";
};

function getHeadingDefaultClasses() {
	$$.Log.warn("You must override the 'getHeadingDefaultClasses' method in order to get the expected results...");

	return "";
};

function getHeadingHoverClasses() {
	$$.Log.warn("You must override the 'getHeadingHoverClasses' method in order to get the expected results...");

	return "";
};

function getHeadingDisabledClasses() {
	$$.Log.warn("You must override the 'getHeadingDisabledClasses' method in order to get the expected results...");

	return "";
};

function colorizeBySource(container) {
	$$.Log.warn("You must override the 'colorizeBySource' method in order to get the expected results...");

	return false;
};

function hideEmptyAttributes(container) {
	$$.Log.warn("You must override the 'hideEmptyAttributes' method in order to get the expected results...");

	return false;
};

/** Functions to be overridden by custom implementations - END */

function sortDatesAscending(a, b) {
	var firstRef = $$.isSet(a.referenceDate) ? a.referenceDate : 100000000000000000;
	var secondRef = $$.isSet(b.referenceDate) ? b.referenceDate : 100000000000000000;

	var firstUpd = $$.isSet(a.updateDate) ? a.updateDate : 100000000000000000;
	var secondUpd = $$.isSet(b.updateDate) ? b.updateDate : 100000000000000000;

	var dFirstRef = new Date(firstRef).formatDate();
	var dSecondRef = new Date(secondRef).formatDate();

	if(dFirstRef != dSecondRef)
		return firstRef > secondRef ? 1 : ( firstRef < secondRef ? -1 : 0 );

	return firstUpd > secondUpd ? 1 : ( firstUpd < secondUpd ? -1 : 0 );	 
};

function sortDatesDescending(a, b) {
	return sortDatesAscending(b, a);
};

function getVesselDataAsArray(vesselData) {
	var asArray = new Array();

	var attribute;
	for(var key in vesselData) {
		attribute = vesselData[key];

		if($$.isArray(attribute) && attribute.length > 0)
			asArray = asArray.concat(attribute);
	}

	return asArray;
};

function findVesselOriginalIDForSource(vesselData, sourceSystem) {
	if(vesselData == null || ( !$$.isSet(vesselData.internalIdentifiers) && !$$.isSet(vesselData.publicIdentifiers) ) )
		return null;

	var identifiers = [];

	if($$.isSet(vesselData.publicIdentifiers))
		identifiers.push(vesselData.publicIdentifiers);

	if($$.isSet(vesselData.internalIdentifiers))
		identifiers.push(vesselData.internalIdentifiers);

	for(var i=0; i<identifiers.length; i++) {
		if( identifiers[i].sourceSystem == sourceSystem && 
			( identifiers[i].typeId == sourceSystem + "_ID" || identifiers[i].typeId == sourceSystem + "_CFR" ) )
			return identifiers[i].identifier;
	}

	return null;
};

function findVesselOriginalIDForVRMFID(vesselData, vrmfId) {
	if(vesselData == null || ( !$$.isSet(vesselData.internalIdentifiers) && !$$.isSet(vesselData.publicIdentifiers) ) )
		return null;

	var sourceSystem = null;
	
	for(var c=0; c<vesselData.componentVessels.length; c++) {
		if(vrmfId == vesselData.componentVessels[c].id) {
			sourceSystem = vesselData.componentVessels[c].sourceSystem;
			break;
		}
	}
	
	var result = {
		linkText: "VRMF ID #" + vrmfId + " [&nbsp;" + $$.I18n.getText("vrmf.vessels.display.common.missing.source.id", sourceSystem) + "&nbsp;]",
		identified: false
	};
	
	var identifiers = [];

	if($$.isSet(vesselData.publicIdentifiers))
		identifiers.push.apply(identifiers, vesselData.publicIdentifiers);

	if($$.isSet(vesselData.internalIdentifiers))
		identifiers.push.apply(identifiers, vesselData.internalIdentifiers);
	
	for(var i=0; i<identifiers.length; i++) {
		if( identifiers[i].vesselId == vrmfId &&
			identifiers[i].sourceSystem == sourceSystem && 
			( identifiers[i].typeId == sourceSystem + "_ID" || identifiers[i].typeId == sourceSystem + "_CFR" ) ) {
			result.linkText = sourceSystem + " #" + identifiers[i].identifier;
			result.identified = true;
		}
	}
	
	return result;
};

function getComponentVesselsForSystem(vesselData) {
	var vesselDataAsArray = getVesselDataAsArray(vesselData);
	var data;

	var results = { };

	for(var d=0; d<vesselDataAsArray.length; d++) {
		data = vesselDataAsArray[d];

		if($$.isSet(data.sourceSystem) && $$.isSet(data.vesselId)) {
			if($$.isSet(data.typeId) && (data.typeId === "UID" || data.typeId === "VRMF_ID"))
				continue;

			if(!$$.isSet(results[data.sourceSystem])) {
				results[data.sourceSystem] = new $$.Set();
			}

			results[data.sourceSystem].add(data.vesselId);
		}
	}

	return results;
};

function isADataProviderSystem(system) {
	var result = false;

	var source = $$.Metadata.SYSTEMS_MAP[system];

	if($$.isSet(source))
		result |= source.vesselSource;

	source = $$.isSet($$.Metadata.AUTH_SYSTEMS_MAP) ? $$.Metadata.AUTH_SYSTEMS_MAP[system] : null;

	if($$.isSet(source))
		result |= source.vesselSource;

	return result;
};

function providesCrossSystemsData(system) {
	var result = false;

	var source = $$.Metadata.SYSTEMS_MAP[system];

	if($$.isSet(source))
		result |= source.crossSystemsData;

	source = $$.isSet($$.Metadata.AUTH_SYSTEMS_MAP) ? $$.Metadata.AUTH_SYSTEMS_MAP[system] : null;

	if($$.isSet(source))
		result |= source.crossSystemsData;

	return result;
};

function initializeSystempicker(systemsPicker, vesselData) {
	$("option:gt(0)", systemsPicker).remove();

	var vesselSources = new $$.Set();

	var vesselDataAsArray = getVesselDataAsArray(vesselData);
	var data;

	for(var d=0; d<vesselDataAsArray.length; d++) {
		data = vesselDataAsArray[d];

		if($$.isSet(data.sourceSystem) && isADataProviderSystem(data.sourceSystem)) {
			if($$.isSet(data.typeId) && (data.typeId === "UID" || data.typeId === "VRMF_ID"))
				continue;

			vesselSources.add(data.sourceSystem);
		}
	}

	vesselSources = vesselSources.asArray().sort();

	var option;
	for(var d=0; d<vesselSources.length; d++) {
		option = $("<option class='" + vesselSources[d] + "'>" + vesselSources[d] + "</option>");
		option.val(vesselSources[d]);

		systemsPicker.append(option);
	};

	if(vesselSources.length <= 1) 
		systemsPicker.parents(".sourceChoice").hide();
	else
		systemsPicker.parents(".sourceChoice").show();

	var container = systemsPicker.parents(".detailsContainer");
	
	systemsPicker.unbind("change").change(function() {
		displayVesselDetailsForSystem(container);
	});
	
	$$.select($("option:first", systemsPicker));
		
	return vesselSources.length;
};

function initializeVesselpicker(vesselPicker, vesselData) {
	var target = vesselPicker.parents(".detailsContainer");

	var systemToComponents = getComponentVesselsForSystem(vesselData);

	var systemFilter = getSystemFilter($(".systemPicker", target));
	var filterBySystem = $$.isSet(systemFilter);
	var identifiers = vesselData.publicIdentifiers;

	$("option:gt(0)", vesselPicker).remove();

	var IDs = new $$.Set();
	var vesselIDs = new $$.Set();

	if(filterBySystem) {
		IDs = systemToComponents[systemFilter];
	} else {
		IDs = IDs.fromArray(vesselData.ids);
	}

	var identifier;
	for(var i=0; i<identifiers.length; i++) {
		identifier = identifiers[i];
		if(IDs.contains(identifier.vesselId) && ( !filterBySystem || systemFilter.indexOf(identifier.sourceSystem) >= 0) ) {
			if(identifier.typeId.startsWith(identifier.sourceSystem + "_"))
				vesselIDs.add({ 
					id: identifier.vesselId, 
					sourceSystem: identifier.sourceSystem, 
					identifier: identifier.sourceSystem + " #" + identifier.identifier
				});
		}
	}

	var sorter = function(a, b) {
		if(a.sourceSystem < b.sourceSystem)
			return -1;
		else if(a.sourceSystem > b.sourceSystem)
			return 1;
		else if(a.identifier < b.identifier)
			return -1;
		else if(a.identifier > b.identifier)
			return 1;
		
		return 0;
		/* Was:
		return 
			a.sourceSystem < b.sourceSystem ? 
				-1 
			: 
				a.sourceSystem > b.sourceSystem ?
					1 
				:
					a.identifier < b.identifier ? 
						-1 
					:
						a.identifier > b.identifier ?
							1
						:
							0;
		*/
		
		/* But for some reasons, IE 8 refuses to accept it... */
	};
	
	/* Was: 
	 * vesselIDs = vesselIDs.asArray().sort(sorter);
	 */
	vesselIDs = vesselIDs.asArray();
	vesselIDs = vesselIDs.sort(sorter);
	/* But for some reasons, IE 8 refuses to accept it... */ 
	var counter = 1;

	var option;
	for(var d=0; d<vesselIDs.length; d++) {
		option = $("<option>" + vesselIDs[d].identifier + "</option>");
		option.val(vesselIDs[d].id);
		option.addClass(vesselIDs[d].sourceSystem);

		vesselPicker.append(option);

		counter++;
	};

	if(vesselIDs.length <= 1) {
		vesselPicker.parents(".vesselChoice").hide();
	} else {
		vesselPicker.parents(".vesselChoice").show();
	}

	var container = vesselPicker.parents(".detailsContainer");
	
	vesselPicker.unbind("change").change(function() {
		displayVesselDetailsForVessel(container);
	});
	
	$$.select($("option:first", vesselPicker));
	
	return vesselIDs.length;
};

function initializeDateSelector(dateSelector, vesselData) {
	var target = dateSelector.parents(".detailsContainer");

	var systemFilter = getSystemFilter($(".systemPicker", target));
	var vesselFilter = getVesselFilter($(".vesselPicker", target));

	var filterBySystem = $$.isSet(systemFilter);
	var filterByVessel = $$.isSet(vesselFilter);

	dateSelector.empty();

	var dates = new $$.Set();
	dates.add(new Date().formatDate());

	var isAuth = false;

	var filteredData = getVesselDataAsArray(vesselData);
	
	if(filterBySystem || filterByVessel) {
		filteredData = filterDetailsBySystem(filteredData, 
											 filterBySystem ? systemFilter : null, 
											 filterByVessel ? vesselFilter : null);
	}
	
	var vesselDataAsArray = filteredData;
	var data;
	
	for(var d=0; d<vesselDataAsArray.length; d++) {
		data = vesselDataAsArray[d];

//		if(filterBySystem && $$.isSet(data.sourceSystem)) {
//			if(data.sourceSystem != systemFilter)
//				continue;
//		}
//
//		if(filterByVessel && $$.isSet(data.vesselId)) {
//			if(data.vesselId != vesselFilter)
//				continue;
//		}

		//To include also authorization ranges in the datepicker...
		//isAuth = $$.isSet(data.validFrom);
		isAuth = false;

		//TODO: as it is now, it won't work properly, even if enabled...
		if(isAuth) {
			dates.add(new Date(data.validFrom).formatDate());

			if($$.isSet(data.validTo)) {
				dates.add(new Date(data.validTo).formatDate());
			}

			if($$.isSet(data.terminationReferenceDate)) {
				dates.add(new Date(data.terminationReferenceDate).formatDate());
			}
		} else {
			if($$.isSet(data.referenceDate)) {
				dates.add(new Date(data.referenceDate).formatDate());
			}
		}
	}

	dates = dates.asArray().sort(function(a, b) { return a < b ? 1 : ( a == b ? 0 : -1 ); } );

	var counter = 0;

	var now = new Date().formatDate();

	var option;
	for(var d=0; d<dates.length; d++) {
		if(dates[d] <= now) {
			counter++;

			option = $("<option>" + dates[d] + "</option>");
			option.val(dates[d]);
			option.addClass(counter % 2 == 0 ? "even" : "odd");

			dateSelector.append(option);
		}
	};

	var container = dateSelector.parents(".detailsContainer");
	
	dateSelector.unbind("change").change(function() {
		displayVesselDetailsAtDate(container);
		
		return true;
	});
	
	$$.select($("option:first", dateSelector));

	return dates.length;
};

function getSystemFilter(systemFilter) {
	var selected = $$.rawTrim(systemFilter.val());

	$$.Log.debug("System filter is currently set to: " + selected);

	if(selected == null)
		return [];

	return [ selected ];
};

function getVesselFilter(vesselFilter) {
	var selected = $$.rawTrim(vesselFilter.val());

	$$.Log.debug("Vessel filter is currently set to: " + selected);

	return selected;
};

function getDateFilter(dateFilter) {
	var selected = $$.rawTrim(dateFilter.val());

	$$.Log.debug("Date filter is currently set to: " + selected);

	return selected;
};

function getCurrentTypedData(dataset, typeFilter) {
	if(!$$.isSet(dataset)) {
		return null;
	}

	var result = new Array();

	for(var d=0; d<dataset.length; d++) {
		if(result.length == 0)
			result.push(dataset[d]);
		else {
			if(typeFilter(result, dataset[d])) {
				result.push(dataset[d]);
			}
		}
	}

	return result;
};

function getCurrentUntypedData(dataset) {
	if(!$$.isSet(dataset)) { return new Array(); }

	return new Array(dataset[0]);
};

function getCurrentMultipleUntypedData(dataset) {
	if(!$$.isSet(dataset)) { return new Array(); }

	var currentDate = dataset[0].referenceDate;

	var filtered = new Array();

	for(var d=0; d<dataset.length; d++)
		if(dataset[d].referenceDate == currentDate)
			filtered.push(dataset[d]);

	return filtered;
};

function filterDetailsByDate(details, date) {
	if(!$$.isSet(date) || !$$.isSet(details))
		return details;

	var filtered = new Array();

	for(var d=0;d<details.length; d++) {
		if(new Date(details[d].referenceDate).formatDate() <= date)
			filtered.push(details[d]);
	}

	return filtered;
};

function filterDetailsBySystem(details, systems, vesselId, vesselIdExtractor) {	
	if(!$$.isSet(details))
		return null;

	var currentVesselIdExtractor = $$.isSet(vesselIdExtractor) ? vesselIdExtractor : function(data) { return $$.isSet(data.vesselId) ? data.vesselId : null; };
	
	var filtered = new Array();

	var filterSources = $$.isSet(systems) && systems.length > 0;
	var filterVessels = $$.isSet(vesselId);

	var validSystems = { };

	if(filterSources)
		for(var s=0; s<systems.length; s++) {
			validSystems[systems[s]] = true;
		}

	var include = true;
	var hasVesselId = false;
	var hasSystem = false;
	var isAValidSystem = false;
	var isACrossSystemsData = false;
	var isACrossSystemsDataForVessel = false;

	var componentVesselsBySystem = getComponentVesselsForSystem(VESSEL_DATA);

	var componentVessels = [];

	if($$.isSet(systems)) {
		for(var s=0; s<systems.length; s++) {
			for(var c=0; c<componentVesselsBySystem[systems[s]].size(); c++)
				componentVessels.push(componentVesselsBySystem[systems[s]].asArray()[c]);
		}
	}

	for(var d=0; d<details.length; d++) {
		hasVesselId = $$.isSet(currentVesselIdExtractor(details[d]));
		hasSystem = $$.isSet(details[d].sourceSystem);
		isAValidSystem = hasSystem && $$.isSet(validSystems[details[d].sourceSystem]);
		isACrossSystemsData = hasSystem && providesCrossSystemsData(details[d].sourceSystem);
		isACrossSystemsDataForVessel = isACrossSystemsData && $$.isSet(systems) && componentVessels.indexOf(currentVesselIdExtractor(details[d])) >= 0;

		include = !filterVessels || 
				  ( hasVesselId &&
				    currentVesselIdExtractor(details[d]) == vesselId );

		include &= ( !filterSources || 
				   	 isAValidSystem ||
				   	 ( isACrossSystemsData && isACrossSystemsDataForVessel ) );

		if(include)
			filtered.push(details[d]);
	}

	return filtered;
};

function addAdditionalMeta(attribute, data) {
	var meta = $(".attributeMeta", attribute);

	var updateDate = new Date(data.updateDate);
	var formattedUpdateDate = updateDate.formatDate() + " @ " + updateDate.formatTime();

	var content = "<html><div>";
	content += "<div><label>" + $$.I18n.getText("vrmf.vessels.display.common.meta.update.user") + "</label> " + data.updaterId + "</div>";
	content += "<div><label>" + $$.I18n.getText("vrmf.vessels.display.common.meta.update.date") + "</label> " + formattedUpdateDate + "</div>";

	if($$.isSet(data.comment))
		content += "<div><label>" + $$.I18n.getText("vrmf.vessels.display.common.meta.update.comment") + "</label> " + data.comment + "</div>";

	content += "</div></html>";

	meta.attr("title", content);
}

function buildAttributeData(targetContainer, targetClass, source, filter, labelFormatter, valueFormatter, metaFormatter, customSorters, postProcessors) {
	$$.Log.info("Building attribute data for " + targetClass);

	var attribute = $(".attribute." + targetClass, $(targetContainer));
	attribute.removeClass("hidden");

	var noData = $(".noData", attribute);
	var data = $(".data", attribute);

	data.empty();

	var sourceData = filter(source);

	var isEmpty = !$$.isSet(sourceData) || sourceData.length == 0; 

	if(!isEmpty) {
		var entry;
		var label;
		var value;
		var meta;

		var currentData;

		var sorter = null;
		var postProcessor = null;
		
		if($$.isSet(customSorters) && $$.isSet(customSorters[targetClass])) {
			sorter = customSorters[targetClass];
		} 
		
		if($$.isSet(postProcessors) && $$.isSet(postProcessors[targetClass])) {
			postProcessor = postProcessors[targetClass];
		}

		if(!$$.isSet(sorter)) {
			if("ascending" == $$.Preferences.defaultReferenceDateSorting) {
				sorter = sortDatesAscending;
			} else {
				sorter = sortDatesDescending;
			}
		}

		sourceData = sourceData.sort(sorter);

		if($$.isSet(postProcessor)) {
			sourceData = postProcessor(sourceData);
		}
		
		var isNew, isUpdated;

		isNew = isUpdated = false;

		for(var i=0; i<sourceData.length; i++) {
			currentData = sourceData[i];

			isNew = !$$.isSet(currentData.vesselId);
			isUpdated = $$.isSet(currentData.vesselId) && $$.isSet(currentData.vesselUid) && currentData.vesselUid < 0;

			label = labelFormatter(currentData, source);

			value = valueFormatter(currentData, source);

			meta = metaFormatter(currentData, source);

			entry = $("<div class='attributeContainer" + ( isNew ? " newData" : isUpdated ? " updatedData" : "" ) + "'/>");

			if($$.isSet(currentData._depth)) {
				if(currentData._depth - 2 > 0)
					label.css("padding-left", ( ( currentData._depth - 2 ) * 1.1 ) + "em");
			}
			
			entry.append(label);
			entry.append(value);
			entry.append(meta);
			entry.data("_data", currentData);
									 
			if($$.isSet(currentData.sourceSystem)) {
				entry.addClass("sourced").addClass(currentData.sourceSystem);
			}

			entry.data("source", currentData);
			entry.data("originalSource", $.extend(true, {}, currentData));

			addAdditionalMeta(entry, currentData);

			data.append(entry);
		}

		attribute.addClass("notEmpty");
		attribute.removeClass("empty");

		data.removeClass("hidden");
		noData.addClass("hidden");

		attribute.addClass("expanded");
	} else {
		attribute.addClass("empty");
		attribute.removeClass("notEmpty");

		data.addClass("hidden");
		noData.removeClass("hidden");
	}

	if(colorizeBySource(targetContainer) && !isEmpty)
		data.addClass("removeUI");
	else
		data.removeClass("removeUI");

	if(hideEmptyAttributes(targetContainer) && isEmpty) {
		attribute.hide();
	} else {
		var forceHidden = attribute.data("forceHidden");

		if(!$$.isSet(forceHidden) || !forceHidden)
			attribute.show();
	}
};

function buildVesselDetails(container, vesselData, vesselID, systems, currentDate, customSorters, postProcessors) {
	var atDate = $$.isSet(currentDate);

	var filteredComponents = filterDetailsBySystem(vesselData.componentVessels, $$.rawTrim(systems), vesselID, function(data) { return data.id; });
		
	buildAttributeData(container, "components", 
					   filteredComponents.length > 1 ? vesselData : {}, 
					   function(source) { 
					   		return filterDetailsBySystem(source.componentVessels, $$.rawTrim(systems), vesselID, function(data) { return data.id; });
					   },
					   componentVesselsLabelFormatter,
					   componentVesselsValueFormatter,
					   sourceSystemComponentVesselsDetailsFormatter,
					   customSorters,
					   postProcessors);
	
	buildAttributeData(container, "IIDs", 
						vesselData, 
						function(source) { 
						 	return source.internalIdentifiers;
						}, 
						identifierTypeLabelFormatter,
						identifierValueFormatter, 
						sourceSystemDetailsFormatter,
						customSorters);

	buildAttributeData(container, "PIDs", 
						vesselData, 
						function(source) { 
							if(atDate)
								return getCurrentTypedData(filterDetailsByDate(filterDetailsBySystem(source.publicIdentifiers, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate)), typedDataFilter);
							
							return filterDetailsByDate(filterDetailsBySystem(source.publicIdentifiers, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate));
//							return filterDetailsBySystem(source.publicIdentifiers, $$.rawTrim(systems), vesselID);
						}, 
						identifierTypeLabelFormatter,
						identifierValueFormatter, 
						sourceSystemAndReferenceDateDetailsFormatter,
						customSorters);

	buildAttributeData(container, "names",
			   			vesselData,
			   			function(source) {
							if(atDate)
								return getCurrentUntypedData(filterDetailsByDate(filterDetailsBySystem(source.nameData, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate)));

							return filterDetailsByDate(filterDetailsBySystem(source.nameData, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate));
			   			}, 
			   			emptyLabelFormatter,
			   			nameValueFormatter,
			   			sourceSystemAndReferenceDateDetailsFormatter,
						customSorters);
	if(atDate) {
		var previousNames = new Array();

		if($$.isSet(vesselData.nameData) && vesselData.nameData.length > 1) {
			var previousDate = null;

			var nameHistory = filterDetailsByDate(filterDetailsBySystem(vesselData.nameData, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate));

			if($$.isSet(nameHistory) && nameHistory.length > 0) {
				var lastNameDate = nameHistory[0].referenceDate;

				for(var n=1; n < nameHistory.length; n++) {
					if(nameHistory[n].referenceDate < lastNameDate &&
					  (previousDate == null || nameHistory[n].referenceDate == previousDate)) {
						if(nameHistory[0].name != nameHistory[n].name) {
							previousNames.push(nameHistory[n]); 

							previousDate = nameHistory[n].referenceDate;
						}
					}
				}
			}
		}

		buildAttributeData(container, "previousNames",
				vesselData,
				function(source) {
					return getCurrentMultipleUntypedData(previousNames); //Was: return getCurrentUntypedData(previousNames);
				}, 
				emptyLabelFormatter,
				nameValueFormatter,
				sourceSystemAndReferenceDateDetailsFormatter,
				customSorters);
	} else {
		$(".previousNames", container).hide();
	}

	buildAttributeData(container, "externalMarkings",
					    vesselData,
					    function(source) {
							if(atDate)
								return getCurrentMultipleUntypedData(filterDetailsByDate(filterDetailsBySystem(source.externalMarkingsData, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate)));

							return filterDetailsByDate(filterDetailsBySystem(source.externalMarkingsData, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate));
   						}, 
   						emptyLabelFormatter,
   						externalMarkingsValueFormatter,
   						sourceSystemAndReferenceDateDetailsFormatter,
						customSorters);

	buildAttributeData(container, "shipbuilders", 
						vesselData, 
						function(source) { 
							if(atDate)
								return getCurrentUntypedData(filterDetailsByDate(filterDetailsBySystem(source.shipbuilders, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate)));

							return filterDetailsByDate(filterDetailsBySystem(source.shipbuilders, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate)); 
						}, 
						emptyLabelFormatter,
						shipbuilderValueFormatter, 
						sourceSystemAndReferenceDateDetailsFormatter,
						customSorters);

	buildAttributeData(container, "buildingYears",
   			   			vesselData,
   			   			function(source) {
							if(atDate)
								return getCurrentUntypedData(filterDetailsByDate(filterDetailsBySystem(source.buildingYearData, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate)));

				  			return filterDetailsByDate(filterDetailsBySystem(source.buildingYearData, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate));
   			   			}, 
   			   			emptyLabelFormatter,
   			   			buildingYearValueFormatter,
   			   			sourceSystemAndReferenceDateDetailsFormatter,
						customSorters);
	
	buildAttributeData(container, "hullMaterials",
			   			vesselData,
			   			function(source) {
						if(atDate)
							return getCurrentUntypedData(filterDetailsByDate(filterDetailsBySystem(source.hull, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate)));
		
			  			return filterDetailsByDate(filterDetailsBySystem(source.hull, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate));
			   			}, 
			   			emptyLabelFormatter,
			   			hullMaterialValueFormatter,
			   			sourceSystemAndReferenceDateDetailsFormatter,
			   			customSorters);

	buildAttributeData(container, "statuses",
			   			vesselData,
			   			function(source) {
							if(atDate)
								return getCurrentUntypedData(filterDetailsByDate(filterDetailsBySystem(source.status, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate)));

	  			  			return filterDetailsByDate(filterDetailsBySystem(source.status, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate));
			   			}, 
			   			emptyLabelFormatter,
			   			vesselStatusValueFormatter,
			   			sourceSystemAndReferenceDateDetailsFormatter,
						customSorters);

	buildAttributeData(container, "vesselTypes",
			   			vesselData,
			   			function(source) {
							if(atDate)
								return getCurrentUntypedData(filterDetailsByDate(filterDetailsBySystem(source.types, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate)));

				  			return filterDetailsByDate(filterDetailsBySystem(source.types, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate));
			   			}, 
			   			emptyLabelFormatter,
			   			vesselTypeValueFormatter,
			   			sourceSystemAndReferenceDateDetailsFormatter,
						customSorters);

	buildAttributeData(container, "vesselGears", 
						vesselData, 
						function(source) { 
							if(atDate)
								return getCurrentTypedData(filterDetailsByDate(filterDetailsBySystem(source.gears, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate)), gearTypeFilter);

							return filterDetailsByDate(filterDetailsBySystem(source.gears, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate)); 
			 			}, 
			 			gearTypeLabelFormatter,
			 			gearTypeValueFormatter, 
			 			sourceSystemAndReferenceDateDetailsFormatter,
						customSorters);

	buildAttributeData(container, "flags",
   			   			vesselData,
   			   			function(source) {
							if(atDate)
								 return getCurrentMultipleUntypedData(filterDetailsByDate(filterDetailsBySystem(source.flagData, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate)));

	  			   			return filterDetailsByDate(filterDetailsBySystem(source.flagData, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate));
   			   			}, 
   			   			emptyLabelFormatter,
   			   			flagValueFormatter,
   			   			sourceSystemAndReferenceDateDetailsFormatter,
						customSorters);

	if(atDate) {
		var previousFlags = new Array();

		if($$.isSet(vesselData.flagData) && vesselData.flagData.length > 1) {
			var previousDate = null;

			var flagHistory = filterDetailsByDate(filterDetailsBySystem(vesselData.flagData, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate));

			if($$.isSet(flagHistory) && flagHistory.length > 0) {
				var lastFlagDate = flagHistory[0].referenceDate;

				for(var f=1; f < flagHistory.length; f++) {
					if(flagHistory[f].referenceDate < lastFlagDate && 
					  (previousDate == null || flagHistory[f].referenceDate == previousDate)) {
						if(flagHistory[f].countryId != flagHistory[0].countryId) {
							previousFlags.push(flagHistory[f]);

							previousDate = flagHistory[f].referenceDate;
						}
					}
				}
			}
		}

		buildAttributeData(container, "previousFlags",
						   vesselData,
						   function(source) {
								return getCurrentMultipleUntypedData(previousFlags);
						   },
						   emptyLabelFormatter,
						   flagValueFormatter,
						   sourceSystemAndReferenceDateDetailsFormatter,
						   customSorters);
	} else {
		$(".previousFlags", container).hide();
	}

	buildAttributeData(container, "callsigns",
   			   			vesselData,
   			   			function(source) {
							if(atDate)
								return getCurrentMultipleUntypedData(filterDetailsByDate(filterDetailsBySystem(source.callsignData, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate)));

							return filterDetailsByDate(filterDetailsBySystem(source.callsignData, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate));
						}, 
						emptyLabelFormatter,
						callsignValueFormatter,
						sourceSystemAndReferenceDateDetailsFormatter,
						customSorters);

	buildAttributeData(container, "MMSIs",
		    			vesselData,
		    			function(source) {
							if(atDate)
								return getCurrentUntypedData(filterDetailsByDate(filterDetailsBySystem(source.MMSIData, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate)));

			   				return filterDetailsByDate(filterDetailsBySystem(source.MMSIData, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate));
						}, 
						emptyLabelFormatter,
						MMSIValueFormatter,
						sourceSystemAndReferenceDateDetailsFormatter,
						customSorters);
	
	buildAttributeData(container, "VMSs",
						vesselData,
						function(source) {
							if(atDate)
								return getCurrentUntypedData(filterDetailsByDate(filterDetailsBySystem(source.VMSData, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate)));
			
			   				return filterDetailsByDate(filterDetailsBySystem(source.VMSData, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate));
						}, 
						emptyLabelFormatter,
						VMSValueFormatter,
						sourceSystemAndReferenceDateDetailsFormatter,
						customSorters);

	buildAttributeData(container, "registrations",
   			   			vesselData,
   			   			function(source) {
							if(atDate)
								return getCurrentMultipleUntypedData(filterDetailsByDate(filterDetailsBySystem(source.registrations, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate)));

	   			   			return filterDetailsByDate(filterDetailsBySystem(source.registrations, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate));
   			   			}, 
   			   			emptyLabelFormatter,
   			   			fullRegistrationNumberValueFormatter,
   			   			sourceSystemAndReferenceDateDetailsFormatter,
						customSorters);

	buildAttributeData(container, "fishingLicenses",
			   			vesselData,
			   			function(source) {
							if(atDate)
								return getCurrentMultipleUntypedData(filterDetailsByDate(filterDetailsBySystem(source.fishingLicenseData, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate)));

			      			return filterDetailsByDate(filterDetailsBySystem(source.fishingLicenseData, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate));
			   			}, 
			   			emptyLabelFormatter,
			   			fishingLicenseValueFormatter,
			   			sourceSystemAndReferenceDateDetailsFormatter,
						customSorters);

	buildAttributeData(container, "physicalDimensions", 
			 			vesselData, 
			 			function(source) { 
							if(atDate)
								return getCurrentTypedData(filterDetailsByDate(filterDetailsBySystem(source.lengthData, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate)), typedDataFilter);

							return filterDetailsByDate(filterDetailsBySystem(source.lengthData, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate)); 
			 			}, 
			 			lengthLabelFormatter,
			 			lengthValueFormatter, 
			 			sourceSystemAndReferenceDateDetailsFormatter,
						customSorters);

	buildAttributeData(container, "tonnages", 
			 			vesselData, 
			 			function(source) { 
							if(atDate)
								return getCurrentTypedData(filterDetailsByDate(filterDetailsBySystem(source.tonnageData, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate)), typedDataFilter);

							return filterDetailsByDate(filterDetailsBySystem(source.tonnageData, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate)); 
						}, 
						tonnageLabelFormatter,
						tonnageValueFormatter, 
						sourceSystemAndReferenceDateDetailsFormatter,
						customSorters);

	buildAttributeData(container, "engines", 
			 			vesselData, 
			 			function(source) { 
							if(atDate)
								return getCurrentTypedData(filterDetailsByDate(filterDetailsBySystem(source.powerData, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate)), powerTypeFilter);

			     		 	return filterDetailsByDate(filterDetailsBySystem(source.powerData, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate)); 
			 			}, 
			 			powerLabelFormatter,
			 			powerValueFormatter, 
			 			sourceSystemAndReferenceDateDetailsFormatter,
						customSorters);

	buildAttributeData(container, "crew",
  			   			vesselData,
  			   			function(source) {
							if(atDate)
								return getCurrentUntypedData(filterDetailsByDate(filterDetailsBySystem(source.crewData, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate)));

  			      			return filterDetailsByDate(filterDetailsBySystem(source.crewData, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate));
			   			}, 
			   			emptyLabelFormatter,
			   			crewValueFormatter,
			   			sourceSystemAndReferenceDateDetailsFormatter,
						customSorters);

	buildAttributeData(container, "owners", 
						vesselData, 
						function(source) { 
							if(atDate)
								return getCurrentUntypedData(filterDetailsByDate(filterDetailsBySystem(source.owners, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate)));

							return filterDetailsByDate(filterDetailsBySystem(source.owners, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate)); 
			 			}, 
			 			emptyLabelFormatter,
			 			ownerValueFormatter, 
			 			sourceSystemAndReferenceDateDetailsFormatter,
						customSorters);

	buildAttributeData(container, "operators", 
			 			vesselData, 
			 			function(source) { 
							if(atDate)
								return getCurrentUntypedData(filterDetailsByDate(filterDetailsBySystem(source.operators, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate)));

							return filterDetailsByDate(filterDetailsBySystem(source.operators, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate)); 
			 			}, 
			 			emptyLabelFormatter,
			 			operatorValueFormatter, 
			 			sourceSystemAndReferenceDateDetailsFormatter,
						customSorters);

	buildAttributeData(container, "authorizations",
					   	vesselData,
					   	function(source) {
					   		if(atDate)
								return getCurrentTypedData(filterDetailsByDate(filterDetailsBySystem(source.authorizations, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate)), authorizationTypeFilter);
					   	   
					   		return filterDetailsByDate(filterDetailsBySystem(source.authorizations, $$.rawTrim(systems), vesselID), $$.rawTrim(currentDate));
						},
						authorizationsLabelFormatter,
						authorizationsValueFormatter,
						sourceSystemAndReferenceDateDetailsFormatter,
						customSorters);

	$(".attributeHeading", container).addClass(getHeadingDefaultClasses()).each(function() {
		$(this).removeClass(getHeadingDisabledClasses());
		$("> *", $(this)).removeClass(getHeadingDisabledClasses());
	}).on("mouseenter", function() {
		$(this).addClass(getHeadingHoverClasses());
	}).on("mouseleave", function() {
		$(this).removeClass(getHeadingHoverClasses());
	});

	$(".empty .attributeHeading", container).each(function() {
		$(this).removeClass(getHeadingHoverClasses()).unbind("hover");
		$("> *", $(this)).addClass(getHeadingDisabledClasses());
	});
};

function buildHistoricalQuickJump() {
	var counter = 0;

	$("#quickJump option:gt(0)").remove();
	
	$("#historical .attribute").each(function() {
		var $this = $(this);
		var classes = $this.attr("class").split(" ");

		var isPrivate = $this.hasClass("private");
		var isEmpty = $this.hasClass("empty");
		var isHidden = $(".data", $this).hasClass("hidden");
		
		if(!isPrivate && !isHidden) {
			var section = $(".attributeHeading label", $this).text().replace(/\:/g, "").replace(/ $/g, "");
			
			$("#quickJump").append("<option value='" + classes[0] + "' " + ( isEmpty ? "disabled='disabled' " : "") + "class='" + ( counter % 2 == 0 ? "even" : "odd" )+ "'>" + section + "</option>");
			counter++;
		}
	});
};

function jumpTo() {
	var section = $("#quickJump").val();

	var pane = $("#historical .vesselDetails");

	if($$.isSet(section)) {
		var sectionClass = $("." + section, pane);

		if(sectionClass.hasClass("notEmpty"))
			sectionClass.removeClass("collapsed").addClass("expanded");

		pane.scrollTo(sectionClass, 500, {
			onAfter: function() {
//				if(!$.browser.msie) {
				if(!$$.Browser.isMSIE) {
					sectionClass.animate({
						opacity: 0.55
					}, function() {
						sectionClass.animate({ opacity: 1.0 });
					});
				}
			}
		});
	} else {
		pane.scrollTop(0);
	} 
};

$$.Dep.require("vrmf.vessels.display.timeline", { "vrmf.framework.preferences": { identifier: "$$.Preferences" }, 
												  "vrmf.framework.externalReferences": { identifier: "$$.vrmf.ExternalReferences" } });