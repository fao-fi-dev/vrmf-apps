function getAttributeLabelContainer(cssClass) {
	return $("<div/>").addClass($$.isSet(cssClass) ? cssClass : "attributeLabel");
};

function emptyLabelFormatter(data) {
	return getAttributeLabelContainer().append($("<label/>").addClass("hidden"));
};

function identifierTypeLabelFormatter(data) {
	var identifier = $$.Metadata.VESSEL_IDENTIFIERS_MAP[data.typeId];
	return getAttributeLabelContainer().append($("<label>" + ( $$.isSet(identifier) ? identifier.description : data.typeId ) + ":</label>"));
};

function componentVesselsLabelFormatter(data) {
	var isRoot = data.mapsTo == null;
	
	return getAttributeLabelContainer().append($("<label class='ui-helper-clearfix " + (isRoot ? "root" : "component" ) + "Vessel'>" +
													( isRoot ? "" : "<span class='left ui-icon ui-icon-arrowthick-1-n'>&nbsp;</span>" ) +
													"<span " + ( isRoot ? "" : "class='left'") + ">" + 
														$$.I18n.getText("vrmf.vessels.formatters.label.component." + ( isRoot ? "main" : "component" ) ) +
													"</span>" +
												 "</label>"));
};

function gearTypeLabelFormatter(data) {
	return getAttributeLabelContainer().append("<label>" + 
												$$.I18n.getText("vrmf.vessels.formatters.label.type.gear." + ( data.primaryGear ? "primary" : "secondary" )) + ":&nbsp;" +
											  "</label>");
};

function lengthLabelFormatter(data) {
	var lengthItem = $$.Metadata.LENGTH_TYPES_MAP[data.typeId];
	
	return getAttributeLabelContainer().append($("<label class='hint'>" + lengthItem.description + ": </label>").attr("title", $$.I18n.getText("vrmf.vessels.formatters.label.code") + " " + lengthItem.id + " - " + lengthItem.comment));
};

function tonnageLabelFormatter(data) {
	var tonnageItem = $$.Metadata.TONNAGE_TYPES_MAP[data.typeId];
	
	return getAttributeLabelContainer().append($("<label class='hint'>" + tonnageItem.description + ": </label>").attr("title", $$.I18n.getText("vrmf.vessels.formatters.label.code") + " " + tonnageItem.id + " - " + tonnageItem.comment));
};

function powerLabelFormatter(data) {
	return getAttributeLabelContainer().append($("<label class='hint'>" + 
													$$.I18n.getText("vrmf.vessels.formatters.label.engine." + ( data.mainEngine ? "main" : "aux" ) ) + ": </label>")).
													attr("title", $$.I18n.getText("vrmf.vessels.formatters.label.engine." + ( data.mainEngine ? "main" : "aux" ) + ".tip") + " " + 
																  $$.I18n.getText("vrmf.vessels.formatters.label.engine.power.tip"));
};

function authorizationsLabelFormatter(data) {
	var status = getAuthorizationStatusCode(data);
		
	var label = $("<label class='authorizationIssuer " + status + "'>" + data.sourceSystem + "</label>");

	label.attr("title", $$.I18n.getText("vrmf.vessels.formatters.attribute.authorization.status." + status)); 
	
	return getAttributeLabelContainer().append(label); 	
};

$$.Dep.require("vrmf.vessels.formatters.labels", { "vrmf.metadata": { identifier: "$$.Metadata" }, 
	   											   "vrmf.i18n": { identifier: "$$.I18n" } });