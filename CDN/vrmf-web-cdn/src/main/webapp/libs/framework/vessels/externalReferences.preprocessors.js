$$.Dep.require("vrmf.framework.externalReferences.preprocessors", 
				{ "vrmf.framework.externalReferences": { identifier: "$$.vrmf.ExternalReferences" }, 
				  "vrmf.metadata": { identifier: "$$.Metadata" } }, function() {
	$$.vrmf.ExternalReferences.PREPROCESSORS = {
		ID: {
			CCSBT: function (data) {
				return data.alternateIdentifier;
			},
			EU: { 
				FISHSUBSIDY: function (data) { 
					var euCFR = data.identifier;
	
					var iso3 = $$.isSet(euCFR) && euCFR.length > 3 ? euCFR.substring(0, 3) : null;
					
					if(iso3 != null) {
						if(iso3 === "ROM") //Necessary to handle EU CFR having 'old' ISO 3 code for Romania
							iso3 = "ROU";
						
						var country = $$.Metadata.getCountryByIso3Code(iso3);
						
						if($$.isSet(country)) {
							var iso2 = country.iso2Code;
						
							return $$.isSet(iso2) ? iso2.toUpperCase() + "/vessel/" + euCFR : null;
						}
					}
					
					$$.Log.warn("Unable to determine ISO2 code for EU CFR " + data.identifier);
					
					return null;
				},
				ESP: function (data) { 
					var euCFR = data.identifier;
					
					var iso3 = $$.isSet(euCFR) && euCFR.length > 3 ? euCFR.substring(0, 3) : null;
					
					if(iso3 != null) {
						if(iso3 === "ESP") {
							return euCFR.substring(6);
						}
					}
					
					return null;
				}
			},
			IOTC: function (data) {
				return data.alternateIdentifier;
			},
			HSVAR: function (data) { 
				return data.vesselUid;
			},
			WCPFC: function (data) { 
				return $$.isSet(data.alternateIdentifier) ? data.alternateIdentifier : data.identifier;
			},
			TUVI: {
				CLAV: function (data) {
					var TUVI = data.identifier;
					
					return $$.isSet(TUVI) ? parseInt(TUVI.replace(/TUVI/g, ""), 10) : null;
				},
				NCLAV: function (data) { 
					var TUVI = data.identifier;
					
					return $$.isSet(TUVI) ? TUVI.replace(/TUVI/g, "") : null;
				}
			}
		},		
		IRCS: function (data) {
			var IRCS = data.callsignId;
			
			return $$.isSet(IRCS) ? IRCS.replace(/[^a-zA-Z0-9]/g, "").toUpperCase() : null;
		},
		MMSI: function (data) {
			var MMSI = data.mmsi;
			
			return $$.isSet(MMSI) ? MMSI.replace(/[^0-9]/g, "") : null;
		},
		REG_NO: {
			ESP: function (data) {
				if(data == null || !$$.isSet(data.registrationNumber) || data.registrationNumber.indexOf("-") < 0)
					return null;
				
				var pLISTA = "^(\\d-)?";
				var pMATRICULA = "(\\d?[a-zA-Z0-9]+-?\\d)";
				var pSEPARATOR = "-?";
				var pFOLIO = "(\\d+-?\\d+)$";
				
				var pattern = new RegExp(pLISTA + pMATRICULA + pSEPARATOR + pFOLIO);
				
				if(pattern.test(data.registrationNumber)) {
					var parts = pattern.exec(data.registrationNumber);
					
					return {
						lista: { type: 'text', value: parts[1] },
						matricula: { name: 'mat', type: 'text', value: parts[2] },
						folio: { name: 'fol', type: 'text', value: parts[3] },
						button: { name: 'bt_matr', type: 'button', value: 'Consultar' }
					};
				} else {
					return null;
				}
			}
		}
	};
});