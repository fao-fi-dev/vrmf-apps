function PositionsManager(baseURL) {
	$$.Log.debug("Initializing 'PositionsManager' Object...");

	this.timeout = 25000; //25 seconds
	this.baseURL = "/services/vessels/search/";
	this.cacheRequest = false;
	this.shipTracks = null;
	this.lastInfoWindow = null;

	if($$.isSet(baseURL))
		this.baseURL = baseURL;

	this.lastRequestToken = null;
	
	this.EQUAL = 1;
	this.UNKNOWN = 0;
	this.DIFFERENT = -1;

	this.CERTAIN = 1;
	this.UNCERTAIN = 0;
	this.UNLIKELY = -1;
};

PositionsManager.prototype.setBaseURL = function(baseURL) {
	this.baseURL = baseURL;
};

PositionsManager.prototype.setTimeout = function(timeout) {
	this.timeout = timeout;
};

PositionsManager.prototype.getTimeout = function() {
	return this.timeout;
};

PositionsManager.prototype.setCacheRequest = function(flag) {
	this.cacheRequest = flag;
};

PositionsManager.prototype.getShipTracks = function() {
	return this.shipTracks;
};


PositionsManager.prototype.buildSearchTracksRequest = function(vesselData, format) {
	return this.baseURL + 'positions.' + ( $$.isSet(format) ? format : "json" ) + "?" + $.param(this.buildTracksRequestData(vesselData));
};

PositionsManager.prototype.searchTracks = function (vesselIdentification, vesselData, callback, timeoutCallback) {
	var currentTimeout = this.timeout;
	var currentCacheRequest = this.cacheRequest;

	var $this = this;

	var now = new Date();

	$this.lastRequestToken = now.getTime();

	var customHeaders = [];
	customHeaders[_VRMF_SEARCH_REQUEST_TOKEN_HEADER] = $this.lastRequestToken;

	$.ajax({
		async: true,
		cache: currentCacheRequest,
		url: this.baseURL + "positions/" + vesselIdentification.by + "/" + vesselIdentification.id + ".json",
		data: { user: $$.UserManager.getLoggedUser().getId() },
		timeout: currentTimeout,
		headers: customHeaders,
		success: function(response, textStatus, JQXHR) {
			var responseHeader = JQXHR.getResponseHeader(_VRMF_SEARCH_REQUEST_TOKEN_HEADER);

			this.lastRequestToken = responseHeader;

			if(responseHeader == $this.lastRequestToken) {
				var tracks = response.data;

				var filtered = new Array();

				for(var p=0; p<tracks.length; p++) {
					if(!$$.isEmpty(tracks[p].positions))
						filtered.push(tracks[p]);
				}

				$this.shipTracks = $$.isEmpty(filtered) ? null : filtered;

				callback($this.shipTracks, vesselData);
			} else
				$$.Log.warn("Skipping tracks display as they refer to a previous request (current token: " + $this.lastRequestToken + ", returned token: " + this.lastRequestToken + ")");
		},
		complete: function() {
			//$$.Log.warn("Skipping tracks display as they refer to a previous request (current token: " + $this.lastRequestToken + ", returned token: " + this.lastRequestToken + ")");
		},
		_onErrorCallback: timeoutCallback
	});
};

PositionsManager.prototype.displayTracks = function(mapContainer, vesselData, tracks) {
	var self = this;

	mapContainer.empty();

	var mapOptions = {
		zoom: 4,
		mapTypeId: google.maps.MapTypeId.TERRAIN,
		overviewMapControl: true,
		overviewMapControlOptions: { opened: true }
	};

	var map = new google.maps.Map(mapContainer.get(0), $.extend({}, mapOptions, {
		center: new google.maps.LatLng(0, 0)
	}));

	var bounds = new google.maps.LatLngBounds();
	var track;
	var marker;
	var position;

	var isTrack = false;

	var tracksToDisplay = $$.isSet(tracks) ? tracks : this.shipTracks;

	for(var t=0; t<tracksToDisplay.length; t++) {
		track = tracksToDisplay[t];

		isTrack = track.positions.length > 1;

		for(var p=0; p<track.positions.length; p++) {
			position = track.positions[p];

			if(!isTrack || p == track.positions.length - 1 || p%10 == 0) {
				marker = this.addMarker(map, track, position, vesselData);
				bounds.extend(marker.getPosition());
			}
		}

		if(isTrack) {
			var points = [];

			for(var p=0; p<track.positions.length; p++) {
				position = track.positions[p];

				points.push(new google.maps.LatLng(parseFloat(position.latitude),
												   parseFloat(position.longitude)));
			}

			new google.maps.Polyline({
				geodesic: true,
				map: map,
				path: points
			});
		}
	};

	map.fitBounds(bounds);

	$("#vesselPositionTools .trackDataDump").each(function() {
		var $this = $(this);
		var format = $this.text();

		$this.attr("href", self.buildSearchTracksRequest(vesselData, format)).attr("target", "_VRMF_TRACK_DATA_" + vesselData.uid + "_" + format.toUpperCase());
	}).button($$.UI._UI_BUTTONS_OPTIONS);

	$$.UI._buildUIButtons($("#vesselPositionTools .ui-buttonset :data(button)"));
};

PositionsManager.prototype.addMarker = function(map, track, position, vesselData) {
	var timestamp =  new Date(position.timestamp);
	var point = new google.maps.LatLng(parseFloat(position.latitude),
									   parseFloat(position.longitude));

	var marker =  new google.maps.Marker({
		position: point,
		map: map,
		//title: track.source + " position data for " + track.criteria + " " + timestamp.formatDate() + ' @ ' + timestamp.formatTime(),
		icon: 'https://getfavicon.appspot.com/http://' + track.source
	});

	var criteria = track.criteria.replace(/\s?\[.+\]/g, "");
	var additionalCriteria = $$.rawTrim(track.criteria.replace(/.+\[\s/g, "").replace(/\s\]/g,""));

	var additionalCriteriaParts = additionalCriteria.split(", ");

	additionalCriteria = "";

	var check;
	var checkIcon;
	var checkData;

	for(var p=0; p<additionalCriteriaParts.length; p++) {
		check = this.checkCriteria(additionalCriteriaParts[p], vesselData);

		checkIcon = "<img src=\"" + _BASE_HREF + "../cdn/media/images/icons/default/";

		if(check == this.EQUAL)
			checkData = { icon: "greenCircle", text: $$.I18n.getText("vrmf.vessels.manager.positions.detail.attribute.matching.tip") };
		else if(check == this.UNKNOWN)
			checkData = { icon: "orangeCircle", text: $$.I18n.getText("vrmf.vessels.manager.positions.detail.attribute.missing.tip") };
		else if(check == this.DIFFERENT)
			checkData = { icon: "redCircle", text: $$.I18n.getText("vrmf.vessels.manager.positions.detail.attribute.conflicting.tip") };
		else 
			checkData = { icon: "blackCircle", text: $$.I18n.getText("vrmf.vessels.manager.positions.detail.attribute.check.error.tip") };

		checkIcon += checkData.icon + ".png\" class=\"hint trackPositionCheck\" title=\"" + checkData.text + "\"/>";

		additionalCriteria += "<div><label>" + checkIcon + "&nbsp;" + decodeURIComponent(additionalCriteriaParts[p]) + "</label></div>";
	}

	var ageInDays = Math.abs(new Date().daysBetween(timestamp));

	var infoWindow = new google.maps.InfoWindow({
		maxWidth: 640,
		content : "<div><h3>" + $$.I18n.getText("vrmf.vessels.manager.positions.detail.header", decodeURIComponent(criteria)) + "</h3>" +
				  ($$.isSet(additionalCriteria) ?
						"<div>" + additionalCriteria + "</div>" +
						"<hr/>"
					: "" ) +
				  "</div>" +
				  ( ageInDays > 0 ?
							"<div class=\"ui-state-highlight noShadow oldTrack\">" +
					  			"<img src=\"" + _BASE_HREF + "../cdn/media/images/icons/default/warning.png\"/>" +
					  			"<label>" + $$.I18n.getText("vrmf.vessels.manager.positions.detail.warning") + "</label>&nbsp;" +
					  						$$.I18n.getText("vrmf.vessels.manager.positions.detail.warning.stale", ageInDays) +
					  		"</div>" : "" ) +
				  "<div><label>" + $$.I18n.getText("vrmf.vessels.manager.positions.detail.timestamp") + "&nbsp;</label>" + timestamp.toUTCString() + "</div>" +
				  "<div><label>" + $$.I18n.getText("vrmf.vessels.manager.positions.detail.latitude") + "&nbsp;</label>" + $$.Coordinates.convertLat(position.latitude) + "</div>" +
				  "<div><label>" + $$.I18n.getText("vrmf.vessels.manager.positions.detail.longitude") + "&nbsp;</label>" + $$.Coordinates.convertLon(position.longitude) + "</div>" +
				  ($$.isSet(position.speed) ? "<div><label>" + $$.I18n.getText("vrmf.vessels.manager.positions.detail.speed") + "&nbsp;</label>" + ( position.speed / 10 ) + "&nbsp;" + $$.I18n.getText("vrmf.vessels.manager.positions.detail.speed.unit") + "</div>" : "" ) +
				  ($$.isSet(position.course) ? "<div><label>"+ $$.I18n.getText("vrmf.vessels.manager.positions.detail.course") + "&nbsp;</label>" + position.course + "°</div>" : "" ) +

				  "<div><hr/></div>" +
				  "<div><label>" +
				  	  $$.I18n.getText("vrmf.vessels.manager.positions.data.attribution", $$.I18n.getText("vrmf.vessels.manager.positions.data.attribution." + ( track.isPublic ? "courtesy" : "copyright" ))) +
				  	  "&nbsp;" +
					  "<a href=\"" + track.sourceURL + "\" class=\"textLink external\" target=\"_TRACK_\"" + criteria + "_" + track.source + "\">" +
					  	track.source +
					  "</a>" +
				  "</div></label>"
	});

	var $this = this;

	google.maps.event.addListener(marker, 'mouseover', function() {
		if($$.isSet($this.lastInfoWindow)) {
			$this.lastInfoWindow.close();
		}

		infoWindow.open(map, marker);

		$this.lastInfoWindow = infoWindow;
	});

//	google.maps.event.addListener(marker, 'mouseover', function() {
//		infowindow.open(map, this);
//	});

	return marker;
};

PositionsManager.prototype.buildTrackSelector = function(mapContainer, selector, tracks, vesselData) {
	var option;

	$("optgroup", selector).remove();
	$("option:gt(0)", selector).remove();

	var groups = {
		CERTAIN: { tracks: [] },
		UNCERTAIN: { tracks: [] },
		UNKNOWN: { tracks: [] },
		UNLIKELY: { tracks: [] }
	};

	var check;
	for(var t=0; t<tracks.length; t++) {
		check = this.checkTrack(tracks[t], vesselData);

		if(check == this.CERTAIN)
			groups.CERTAIN.tracks.push(tracks[t]);
		else if(check == this.UNCERTAIN)
			groups.UNCERTAIN.tracks.push(tracks[t]);
		else if(check == this.UNLIKELY)
			groups.UNLIKELY.tracks.push(tracks[t]);
		else
			groups.UNKNOWN.tracks.push(tracks[t]);
	}

	var optgroup;
	var track;
	for(var group in groups) {
		if(!$$.isEmpty(groups[group].tracks)) {
			optgroup = $("<optgroup/>");
			optgroup.attr("label", $$.I18n.getText("vrmf.vessels.manager.positions.data." + group));

			for(var t=0; t<groups[group].tracks.length; t++) {
				track = groups[group].tracks[t];

				option = $("<option value='" + group + "_" + t + "'>" +
								track.source + " - " + decodeURIComponent(track.criteria) + " (" + track.positions.length + " positions)" +
							"</option>");

				option.data("track", track);
				option.data("data", vesselData);

				optgroup.append(option);
			}

			selector.append(optgroup);
		}
	}

	var $this = this;

	selector.unbind("change").change(function() {
		var $select = $(this);
		var value = $$.rawTrim($select.val());

		if(value == null)
			$this.displayTracks(mapContainer, vesselData);
		else {
			var selected = $("option:selected", $select);

			$this.displayTracks(mapContainer, selected.data("data"), [ selected.data("track") ]);
		}

		return true;
	});
};

PositionsManager.prototype.getTrackReports = function(tracks, vesselData) {
	var reports = [];

	for(var t=0; t<tracks.length; t++) {
		reports.push({
			id: t,
			track: tracks[t],
			result: this.checkTrack(tracks[t], vesselData)
		});
	}

	return reports;
};

PositionsManager.prototype.checkTrack = function(track, vesselData) {
	var criteria = track.criteria.substring(0, track.criteria.indexOf(" ="));

	if("IMO" == criteria)
		return this.EQUAL;

	var additionalCriteria = $$.rawTrim(track.criteria.replace(/.+\[\s/g, "").replace(/\s\]/g,""));

	var currentCriteria = additionalCriteria.split(", ");

	var result = { };
	
	var criteriaType;
	
	for(var c=0; c<currentCriteria.length; c++) {
		criteriaType = currentCriteria[c].substring(0, currentCriteria[c].indexOf(" "));

		result[criteriaType] = this.checkCriteria(currentCriteria[c], vesselData);
	}

	var equal, different, unknown;
	
	equal = different = unknown = 0;
	
	for(var key in result) {
		if(key == "IMO") {
			if(result[key] == this.EQUAL)
				return this.CERTAIN;
			else if(result[key] == this.DIFFERENT)
				return this.UNLIKELY;
			else if(result[key] == this.UNKNOWN)
				return this.UNCERTAIN;
			return this.UNKNOWN;
		}
		
		if(result[key] == this.EQUAL)
			equal++;
		else if(result[key] == this.DIFFERENT)
			different++;
		else if(result[key] == this.UNKNOWN)
			unknown++;
	}
	
	if(different == 0) {
		if(equal >= 0)
			return this.UNCERTAIN;
		
		return this.UNKNOWN;
	} else { //DIFFERENT > 0
		if(equal >= 0)
			return this.UNKNOWN;
		
		return this.UNCERTAIN;
	}
};

PositionsManager.prototype.checkCriteria = function(criteria, vesselData) {
	var attribute = criteria.substring(0, criteria.indexOf(" "));
	var value = criteria.substring(criteria.indexOf("=") + 2);

	if("IMO" == attribute)
		return this.checkIMO(value, vesselData);
	else if("MMSI" == attribute)
		return this.checkMMSI(value, vesselData);
	else if("IRCS" == attribute)
		return this.checkCallsign(value, vesselData);
	else if("NAME" == attribute)
		return this.checkName(value, vesselData);

	return this.UNKNOWN;
};

PositionsManager.prototype.checkIMO = function(value, vesselData) {
	var result = this.UNKNOWN;

	if($$.isSet(vesselData.publicIdentifiers)) {
		var identifier;
		for(var i=0; i<vesselData.publicIdentifiers.length; i++) {
			identifier = vesselData.publicIdentifiers[i];
			if("IMO" === identifier.typeId) {
				result = identifier.identifier == value ? this.EQUAL : this.DIFFERENT;

				if(result == this.EQUAL)
					break;
			}
		}
	}

	return result;
};

PositionsManager.prototype.checkCallsign = function(value, vesselData) {
	var result = this.UNKNOWN;

	if($$.isSet(vesselData.callsignData)) {
		result = this.DIFFERENT;

		var callsign;
		for(var i=0; i<vesselData.callsignData.length; i++) {
			callsign = vesselData.callsignData[i];
			if(value.toUpperCase() == callsign.callsignId.toUpperCase() ||
			   value.toUpperCase() == callsign.callsignId.toUpperCase().replace(/[^a-zA-Z0-9]/g, "")) {
				result = this.EQUAL;
				break;
			}
		}
	}

	return result;
};

PositionsManager.prototype.checkMMSI = function(value, vesselData) {
	var result = this.UNKNOWN;

	if($$.isSet(vesselData.MMSIData)) {
		result = this.DIFFERENT;

		var MMSI;
		for(var i=0; i<vesselData.MMSIData.length; i++) {
			MMSI = vesselData.MMSIData[i];
			if(value == MMSI.mmsi) {
				result = this.EQUAL;
				break;
			}
		}
	}

	return result;
};

PositionsManager.prototype.checkName = function(value, vesselData) {
	var result = this.UNKNOWN;

	if($$.isSet(vesselData.nameData)) {
		result = this.DIFFERENT;

		var name;
		for(var i=0; i<vesselData.nameData.length; i++) {
			name = vesselData.nameData[i];
			if(value.toUpperCase() == name.name.toUpperCase() ||
			   value.toUpperCase() == name.simplifiedName.toUpperCase()) {
				result = this.EQUAL;
				break;
			}
		}
	}

	return result;
};

$$.Dep.require("vrmf.vessels.manager.positions", { "vrmf.log": { identifier: "$$.Log" } }, function() {
	$.extend($$, {
		PositionsManager: new PositionsManager()
	});	
});