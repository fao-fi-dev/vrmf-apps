var DATE_NOT_SET_COMPARISON_RESULT = 100000000000000000;

var sortTimelineAscending = function(first, second) {
	var firstDate = $$.isSet(first.referenceDate) ? first.referenceDate : DATE_NOT_SET_COMPARISON_RESULT;
	var secondDate = $$.isSet(second.referenceDate) ? second.referenceDate : DATE_NOT_SET_COMPARISON_RESULT;

	var delta = firstDate - secondDate;

	var ordinalDelta = delta != 0 ? 0 : first.ordinal - second.ordinal;
	var labelDelta = ordinalDelta != 0 ? 0 : first.labelFormatter(first.entry).text() > second.labelFormatter(second.entry).text() ? 1 : ( first.labelFormatter(first.entry).text() < second.labelFormatter(second.entry).text() ? -1 : 0 );
	var valueDelta = labelDelta != 0 ? 0 : first.valueFormatter(first.entry).text() > second.valueFormatter(second.entry).text() ? 1 : ( first.valueFormatter(first.entry).text() < second.valueFormatter(second.entry).text() ? -1 : 0 );

	return ( delta != 0 ? delta : ( ordinalDelta != 0 ? ordinalDelta : ( labelDelta != 0 ? labelDelta : valueDelta ) ) ); 
};

var sortTimelineDescending = function(first, second) { 
	var firstDate = $$.isSet(first.referenceDate) ? first.referenceDate : DATE_NOT_SET_COMPARISON_RESULT;
	var secondDate = $$.isSet(second.referenceDate) ? second.referenceDate : DATE_NOT_SET_COMPARISON_RESULT;

	var delta = firstDate - secondDate;

	var ordinalDelta = delta != 0 ? 0 : first.ordinal - second.ordinal;
//	var labelDelta = ordinalDelta != 0 ? 0 : first.labelFormatter(first.entry).text() > second.labelFormatter(second.entry).text() ? 1 : ( first.labelFormatter(first.entry).text() < second.labelFormatter(second.entry).text() ? -1 : 0 );
//	var valueDelta = labelDelta != 0 ? 0 : first.valueFormatter(first.entry).text() > second.valueFormatter(second.entry).text() ? 1 : ( first.valueFormatter(first.entry).text() < second.valueFormatter(second.entry).text() ? -1 : 0 );

//	return ( delta != 0 ? -delta : ( ordinalDelta != 0 ? ordinalDelta : ( labelDelta != 0 ? -labelDelta : -valueDelta ) ) ); 
	return ( delta != 0 ? -delta : ordinalDelta );
};
  
function convertToTimelineEntry(componentVesselsData, ordinal) {
	var converted = [];
	var componentVessel;

	var validIDs = new $$.Set();

	for(var c=0; c<componentVesselsData.length; c++)
		validIDs.add(componentVesselsData[c].id);

	for(var c=0; c<componentVesselsData.length; c++) {
		componentVessel = componentVesselsData[c];

		if($$.isSet(componentVessel.mapsTo) && validIDs.contains(componentVessel.mapsTo)) {
			converted.push({ 
				entry: componentVessel, 
				ordinal: ordinal++, 
				category: $$.I18n.getText("vrmf.vessels.display.timeline.category.matching"), 
				referenceDate: componentVessel.mappingDate, 
				labelFormatter: componentVesselsLabelFormatter, 
				valueFormatter: componentVesselsValueFormatter, 
				additionalClasses: [],
				metaFormatter: sourceSystemComponentVesselsDetailsFormatter
			});
		}
	}

	return converted;
};

function buildVesselTimeline(target, vesselData, componentVesselId, systems, atDate /* Useless? */) {
	var container = $(".vesselDetails", target);

	var timeline = new Array();

	var counter = 1;

	var includePrivateData = $$.UserManager.isUserLogged() && $$.UserManager.getLoggedUser().can("READ_I_DATA");

	var componentVessels = convertToTimelineEntry(vesselData.componentVessels, counter);

	if($$.isSet(componentVessels))
		timeline = timeline.concat(componentVessels);

	counter = timeline.length + 1;

	if(includePrivateData)
		timeline = timeline.concat(buildVesselTimelineEntries(vesselData.internalIdentifiers, counter++, $$.I18n.getText("vrmf.vessels.display.timeline.category.identifier"), identifierTypeLabelFormatter, identifierValueFormatter, [ "userDependent", "{displayIf:{can:'READ_I_DATA'}}" ]));

	timeline = timeline.concat(buildVesselTimelineEntries(vesselData.publicIdentifiers, counter++, $$.I18n.getText("vrmf.vessels.display.timeline.category.identifier"), identifierTypeLabelFormatter, identifierValueFormatter));
	timeline = timeline.concat(buildVesselTimelineEntries(vesselData.nameData, counter++, $$.I18n.getText("vrmf.vessels.display.timeline.category.name"), emptyLabelFormatter, nameValueFormatter));
	timeline = timeline.concat(buildVesselTimelineEntries(vesselData.externalMarkingsData, counter++, $$.I18n.getText("vrmf.vessels.display.timeline.category.external.marking"), emptyLabelFormatter, externalMarkingsValueFormatter));
	timeline = timeline.concat(buildVesselTimelineEntries(vesselData.flagData, counter++, $$.I18n.getText("vrmf.vessels.display.timeline.category.flag"), emptyLabelFormatter, flagValueFormatter));
	timeline = timeline.concat(buildVesselTimelineEntries(vesselData.shipbuilders, counter++, $$.I18n.getText("vrmf.vessels.display.timeline.category.shipbuilder"), emptyLabelFormatter, shipbuilderValueFormatter));
	timeline = timeline.concat(buildVesselTimelineEntries(vesselData.status, counter++, $$.I18n.getText("vrmf.vessels.display.timeline.category.status"), emptyLabelFormatter, vesselStatusValueFormatter));
	timeline = timeline.concat(buildVesselTimelineEntries(vesselData.buildingYearData, counter++, $$.I18n.getText("vrmf.vessels.display.timeline.category.building.year"), emptyLabelFormatter, buildingYearValueFormatter));
	timeline = timeline.concat(buildVesselTimelineEntries(vesselData.hull, counter++, $$.I18n.getText("vrmf.vessels.display.timeline.category.hull.material"), emptyLabelFormatter, hullMaterialValueFormatter));
	timeline = timeline.concat(buildVesselTimelineEntries(vesselData.types, counter++, $$.I18n.getText("vrmf.vessels.display.timeline.category.vessel.type"), emptyLabelFormatter, vesselTypeValueFormatter));
	timeline = timeline.concat(buildVesselTimelineEntries(vesselData.gears, counter++, $$.I18n.getText("vrmf.vessels.display.timeline.category.gear.type"), gearTypeLabelFormatter, gearTypeValueFormatter));
	timeline = timeline.concat(buildVesselTimelineEntries(vesselData.callsignData, counter++, $$.I18n.getText("vrmf.vessels.display.timeline.category.callsign"), emptyLabelFormatter, callsignValueFormatter));
	timeline = timeline.concat(buildVesselTimelineEntries(vesselData.mmsiData, counter++, $$.I18n.getText("vrmf.vessels.display.timeline.category.MMSI"), emptyLabelFormatter, MMSIValueFormatter));
	timeline = timeline.concat(buildVesselTimelineEntries(vesselData.vmsData, counter++, $$.I18n.getText("vrmf.vessels.display.timeline.category.VMS"), emptyLabelFormatter, VMSValueFormatter));
	timeline = timeline.concat(buildVesselTimelineEntries(vesselData.registrations, counter++, $$.I18n.getText("vrmf.vessels.display.timeline.category.registration"), emptyLabelFormatter, fullRegistrationNumberValueFormatter));
	timeline = timeline.concat(buildVesselTimelineEntries(vesselData.fishingLicenseData, counter++, $$.I18n.getText("vrmf.vessels.display.timeline.category.fishing.license"), emptyLabelFormatter, fishingLicenseValueFormatter));
	timeline = timeline.concat(buildVesselTimelineEntries(vesselData.lengthData, counter++, $$.I18n.getText("vrmf.vessels.display.timeline.category.physical.dimension"), lengthLabelFormatter, lengthValueFormatter));
	timeline = timeline.concat(buildVesselTimelineEntries(vesselData.tonnageData, counter++, $$.I18n.getText("vrmf.vessels.display.timeline.category.tonnage"), tonnageLabelFormatter, tonnageValueFormatter));
	timeline = timeline.concat(buildVesselTimelineEntries(vesselData.powerData, counter++, $$.I18n.getText("vrmf.vessels.display.timeline.category.power"), powerLabelFormatter, powerValueFormatter));
	timeline = timeline.concat(buildVesselTimelineEntries(vesselData.crewData, counter++, $$.I18n.getText("vrmf.vessels.display.timeline.category.crew"), emptyLabelFormatter, crewValueFormatter));
	timeline = timeline.concat(buildVesselTimelineEntries(vesselData.owners, counter++, $$.I18n.getText("vrmf.vessels.display.timeline.category.owner"), emptyLabelFormatter, ownerValueFormatter));
	timeline = timeline.concat(buildVesselTimelineEntries(vesselData.operators, counter++, $$.I18n.getText("vrmf.vessels.display.timeline.category.operator"), emptyLabelFormatter, operatorValueFormatter));
	timeline = timeline.concat(buildVesselTimelineEntries(vesselData.authorizations, counter++, $$.I18n.getText("vrmf.vessels.display.timeline.category.authorization"), authorizationsLabelFormatter, authorizationsValueFormatter));

	var filterSystems = $$.isSet(systems);
	var filterVessels = $$.isSet(componentVesselId);

	var validSystems = { };

	if(filterSystems)
		for(var s=0; s<systems.length; s++) {
			validSystems[systems[s]] = true;
		}

	var include = true;
	var hasVesselId = false;

	var hasSystem = false;
	var isAValidSystem = false;
	var isACrossSystemsData = false;
	var isACrossSystemsDataForVessel = false;

	if(filterSystems || filterVessels) {
		var componentVesselsBySystem = getComponentVesselsForSystem(VESSEL_DATA);

		var componentVessels = [];

		if(filterSystems)
			for(var s=0; s<systems.length; s++)
				for(var c=0; c<componentVesselsBySystem[systems[s]].size(); c++)
					componentVessels.push(componentVesselsBySystem[systems[s]].asArray()[c]);

		var filtered = new Array();

		for(var t=0; t<timeline.length; t++) {
			hasVesselId = $$.isSet(timeline[t].entry.vesselId);
			hasSystem = $$.isSet(timeline[t].entry.sourceSystem);
			isAValidSystem = hasSystem && $$.isSet(validSystems[timeline[t].entry.sourceSystem]);
			isACrossSystemsData = hasSystem && providesCrossSystemsData(timeline[t].entry.sourceSystem);
			isACrossSystemsDataForVessel = isACrossSystemsData && $$.isSet(systems) && componentVessels.indexOf(timeline[t].entry.vesselId) >= 0;

			include = !filterVessels || 
			  		 ( hasVesselId &&
			  				timeline[t].entry.vesselId == componentVesselId );

			include &= ( !filterSystems || 
			   	 		 isAValidSystem ||
			   	 	   ( isACrossSystemsData && isACrossSystemsDataForVessel ) );

			if(include)
				filtered.push(timeline[t]);
		}

		timeline = filtered;
	}

	timeline = timeline.sort($$.isSet($$.Preferences.defaultReferenceDateSorting) && $$.Preferences.defaultReferenceDateSorting == "descending" ? sortTimelineDescending : sortTimelineAscending );

	$(".timelineEntry", container).remove();

	var slot, slotId, slotLabel, slotData, entry, category, label, value, meta, additionalClasses;

	for(var i=0; i<timeline.length; i++) {
		var referenceDate = null;

		if($$.isSet(timeline[i].referenceDate)) {
			referenceDate = new Date();
			referenceDate.setTime(timeline[i].referenceDate);
		}

		slotId = referenceDate == null ? "NO_REF_DATE" : referenceDate.formatDate();

		slot = $("#" + slotId, container);

		if(slot == null || slot.size() == 0) {
			slot = $("<div id=\"" + slotId + "\" class=\"timelineEntry attribute notEmpty collapsed\"/>");

			slotLabel = $("<div class='attributeHeading ui-state-default'>" +
							"<label>" +
							( referenceDate == null ? "[ " + $$.I18n.getText("vrmf.vessels.display.timeline.date.not.set") + " ]" : referenceDate.formatDate() ) +
								"&nbsp;<span class=\"counter {type: 'event', countSources: true }\"/>" + 
							"</label>" +
						  "</div>");

			slotData = $("<div class='data'/>");

			slot.append(slotLabel).append(slotData);

			container.append(slot);
		} else {
			slotData = $(".data", slot);
		}

		entry = $("<div class='attributeContainer sourced " + timeline[i].entry.sourceSystem + "'/>");
		category = $("<div class=\"timelineCategory\">[&nbsp;" + timeline[i].category + "&nbsp;]</div>");

		label = timeline[i].labelFormatter(timeline[i].entry).wrap("<label/>");
		$(".ui-icon", label).remove(".ui-icon");
		$(".left", label).removeClass("left");

		value = timeline[i].valueFormatter(timeline[i].entry, vesselData);

		meta = $$.isSet(timeline[i].metaFormatter) ? timeline[i].metaFormatter(timeline[i].entry) : sourceSystemDetailsFormatter(timeline[i].entry);

		additionalClasses = null;

		if($$.isSet(timeline[i].additionalClasses)) {
			if($$.isArray(timeline[i].additionalClasses))
				additionalClasses = timeline[i].additionalClasses.join(" ");
			else
				additionalClasses = timeline[i].additionalClasses;
		}

		entry.append(category).append(label).append(value).append(meta);

		slotData.append(entry);

		if(colorizeBySource(target))
			slotData.addClass("removeUI");
		else
			slotData.removeClass("removeUI");

		if($$.isSet(additionalClasses))
			entry.addClass(additionalClasses);

		addAdditionalMeta(entry, timeline[i].entry);
	};

	$(".attributeHeading", container).on("mouseenter", function() {
		$(this).addClass(getHeadingHoverClasses());
	}).on("mouseleave", function() {
		$(this).removeClass(getHeadingHoverClasses());
	});

	$(".timelineEntry:last-child", container).addClass("lastEntry");
};

function buildVesselTimelineEntries(source, ordinal, category, labelFormatter, valueFormatter, additionalClasses) {
	var entries = new Array();

	if($$.isSet(source))
		for(var i=0; i<source.length; i++) {
			entries.push({ entry: source[i], ordinal: ordinal, category: category, referenceDate: source[i].referenceDate, labelFormatter: labelFormatter, valueFormatter: valueFormatter, additionalClasses: $$.isSet(additionalClasses) ? additionalClasses : [] });
		}

	return entries;
};

function buildTimelineQuickJump() {
	var counter = 0;

	$("#timelineQuickJump option:gt(0)").remove();

	$("#timeline .attribute").each(function() {
		var $this = $(this);
		var id = $this.attr("id");

		var isPrivate = $this.hasClass("private");
		var isEmpty = $this.hasClass("empty");
		var isHidden = $(".data", $this).hasClass("hidden");

		if(!isPrivate && !isHidden) {
			var section = $(".attributeHeading label", $this).text().replace(/\:/g, "").replace(/ $/g, "");

			$("#timelineQuickJump").append("<option value='" + id + "' " + ( isEmpty ? "disabled='disabled' " : "") + "class='" + ( counter % 2 == 0 ? "even" : "odd" )+ "'>" + section + "</option>");
			counter++;
		}
	});
};

function timelineJumpTo() {
	var section = $("#timelineQuickJump").val();

	var pane = $("#timeline .vesselDetails");

	if($$.isSet(section)) {
		var sectionId = $("#" + section, pane);

		if(sectionId.hasClass("notEmpty"))
			sectionId.removeClass("collapsed").addClass("expanded");

		pane.scrollTo(sectionId, 500, {
			onAfter: function() {
//				if(!$.browser.msie) {
				if(!$$.Browser.isMSIE) {
					sectionId.animate({
						opacity: 0.55
					}, function() {
						sectionId.animate({ opacity: 1.0 });
					});
				}
			}
		});
	} else {
		pane.scrollTop(0);
	} 
};

$$.Dep.require("vrmf.vessels.display.timeline", { "vrmf.framework.externalReferences": { identifier: "$$.vrmf.ExternalReferences" } });