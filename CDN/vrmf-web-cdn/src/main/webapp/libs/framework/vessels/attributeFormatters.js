function enableSmartLinks() {
	$$.Interface.warn("'enableSmartLinks' handler goes here");
	
	return false;
};

function IRCSSanitizer(data) {
	var IRCS = data.callsignId;
	
	return $$.isSet(IRCS) ? IRCS.replace(/[^a-zA-Z0-9]/g, "").toUpperCase() : null;
};

function MMSISanitizer(data) {
	var MMSI = data.mmsi;
	
	return $$.isSet(MMSI) ? MMSI.replace(/[^0-9]/g, "") : null;
};

function postProcessAdditionalLinks(additionalLinks, menuTitle) {
	$$.Interface.warn("'postProcessAdditionalLinks' handler goes here");
	
	return additionalLinks;
};

function displayPortCoordinates(event) {
	$$.Interface.warn("'displayPortCoordinates' handler goes here");
};

function displayAddress(event) {
	$$.Interface.warn("'displayAddress' handler goes here");
};

function displayPSMFactsheetsList(event) {
	$$.Interface.warn("'displayPSMFactsheetsList' handler goes here");
};

function wrapAdditionalLink(link) {
	var wrapped = $("<span/>");
	wrapped.append("<span class='additionalLinkWrapper'>[</span>");
	wrapped.append(link);
	wrapped.append("<span class='additionalLinkWrapper'>]</span>");
	
	return wrapped;
};

function getAuthorizationStatusCode(data) {
	var now = new Date().formatDate();
	var validFrom = $$.isSet(data.validFrom) ? new Date(data.validFrom).formatDate() : null;
	var validTo = $$.isSet(data.validTo) ? new Date(data.validTo).formatDate() : null;
	
	var isTerminated = data.terminationReferenceDate != null;
	var isInvalid = !isTerminated && validFrom != null && validTo != null && validFrom > validTo;
	var isFuture = !isInvalid && validFrom > now;
	var isValid = !isFuture && 
				  ( validFrom == null || validFrom <= now ) &&
				  ( validTo == null || validTo >= now );
	var isExpired = !isValid && 
					( validFrom == null || validFrom <= now ) &&
					( validTo == null || validTo < now );

	var authorizationStatus = "UNKNOWN";
	
	if(isTerminated) {
		authorizationStatus = "TERMINATED";
	} else if(isExpired) {
		authorizationStatus = "EXPIRED";
	} else if(isValid) {
		authorizationStatus = "AUTHORIZED";
	} else if(isFuture) {
		authorizationStatus = "TO_BE_AUTHORIZED";
	} else if(isInvalid) {
		authorizationStatus = "INVALID";
	}
	
	return authorizationStatus;
};

function getAttributeValueContainer(cssClass) {
	return $("<div/>").addClass($$.isSet(cssClass) ? cssClass : "attributeValue");
};

function componentVesselsValueFormatter(data, fullData) {
	var item = $("<span/>");

	var linkText = "&lt;UNKNOWN&gt;", title = "";

	var found = findVesselOriginalIDForVRMFID(fullData, data.id);
	
	if($$.isSet(found)) {
		linkText = found.linkText;
		
		if(found.identified) {
			title = $$.I18n.getText("vrmf.vessels.formatters.attribute.component.detach.tip");
		} else {
			title = $$.I18n.getText("vrmf.vessels.formatters.attribute.component.no.source.id.tip", data.sourceSystem);
		}
	}
	
	item.append("<code><a href=\"" + _BASE_HREF + "!/display/vessel/ID/" + ("" + data.id).padLeft("0", 9) + "\" class=\"textLink externalStatic newWin { target: '_VRMF_VESSEL_ID_" + data.id + "' }\" title=\"" + title + "\">" + linkText + "</a></code>");
	
	if(data.mapsTo != null) {
		found = findVesselOriginalIDForVRMFID(fullData, data.mapsTo);
		
		if($$.isSet(found)) {
			linkText = found.linkText;
			
			if(found.identified) {
				title = $$.I18n.getText("vrmf.vessels.formatters.attribute.component.detach.tip");
			} else {
				title = $$.I18n.getText("vrmf.vessels.formatters.attribute.component.no.source.id.tip", $$.I18n.getText("vrmf.vessels.formatters.attribute.component.source.tip"));
			}
		}
		
		item.append("<span class=\"mappingDetail mapsTo\">" + $$.I18n.getText("vrmf.vessels.formatters.attribute.component.duplicate.report") + "</span>");
		item.append("<code><a href=\"" + _BASE_HREF + "!/display/vessel/ID/" + ("" + data.mapsTo).padLeft("0", 9) + "\" class=\"textLink externalStatic newWin { target: '_VRMF_VESSEL_ID_" + data.mapsTo + "' }\" title=\"" + title + "\">" + linkText + "</a></code>");
		item.append("<span class=\"mappingDetail mappingUser\">" + $$.I18n.getText("vrmf.vessels.formatters.attribute.component.duplicate.report.by") + "</span>");
		item.append("<code title=\"User: " + data.mappingUser + "\" " +
						   $$.TooltipManager.TOOLTIP_CONFIGURATION_ATTRIBUTE + "=\"{ position: { my: 'center bottom', at: 'center top' } }\">" + ( "CLAV_IMPORTER" === data.mappingUser ? "CLAV" : "FAO" ) + "</code>");
		item.append("<span class=\"mappingDetail mappingDate\">" + $$.I18n.getText("vrmf.vessels.formatters.attribute.component.duplicate.report.on") + "</span>");
		item.append("<span>" + new Date(data.mappingDate).formatDate() + "</span>");
		item.append("<span class=\"mappingDetail mappingWeight\">" + $$.I18n.getText("vrmf.vessels.formatters.attribute.component.duplicate.report.score") + "</span>");
		item.append($("<code>" + ( Math.round(data.mappingWeight * 100 * 100) / 100 ) + "%</code>").attr("title", data.mappingComment));
	}
	
	return getAttributeValueContainer().append(item);
};

function identifierValueFormatter(data) {
	var identifierItem = $("<span class='selectableOnClick'/>");
	var additionalLinks = $("<span class=\"additionalLinks moreLeftPad\"/>");
	
	var linkBuilders = $$.vrmf.ExternalReferences.getLinkBuilders("ID", data.typeId);
	
	if($$.isSet(linkBuilders)) {
		var link = builder = null;
		
		for(var l=0; l<linkBuilders.length; l++) {
			builder = linkBuilders[l];
			
			link = $$.vrmf.ExternalReferences.buildLink(data.typeId, builder, data, /* Default preprocessor */ function(d) { return d.identifier; });
		
			if(link != null) {
				link = link.addClass("externalProvider");
				
				additionalLinks.append(wrapAdditionalLink(link));
			}
		}

		postProcessAdditionalLinks(additionalLinks, $$.I18n.getText("vrmf.vessels.formatters.attribute.external.link.sources.tip"));
	} 
	
	identifierItem.text(data.identifier);
	
	return getAttributeValueContainer().append($("<code/>").append(identifierItem)).append(additionalLinks);
};

function oldIdentifierValueFormatter(data) {
	var identifierItem = $("<span class='selectableOnClick'/>");
	var additionalLinks = $("<span class=\"additionalLinks moreLeftPad\"/>");
	
	if($$.isSet(EXTERNAL_SEARCH_BY_ID_MAP.ID[data.typeId])) {
		var link = null;
		var entry;
		var identifier;
		
		for(var key in EXTERNAL_SEARCH_BY_ID_MAP.ID[data.typeId]) {
			entry = EXTERNAL_SEARCH_BY_ID_MAP.ID[data.typeId][key];
			
			identifier = $$.isSet(entry.preprocessor) ? entry.preprocessor(data) : data.identifier;
			
			if(identifier != null) {
				link = buildExternalLink(
							entry.URL.replace("\{query\}", $$.isSet(entry.preprocessor) ? entry.preprocessor(data) : data.identifier), 
							entry.description, 
							"_" + (key === 'DEFAULT' ? "" : key + "_") + data.typeId + "_" + data.identifier).text(entry.link);
				
				link.addClass("externalProvider");
				link.attr("title", entry.description);
							
				additionalLinks.append(wrapAdditionalLink(link));
			}
		}

		postProcessAdditionalLinks(additionalLinks, $$.I18n.getText("vrmf.vessels.formatters.attribute.external.link.sources.tip"));
	} 
	
	identifierItem.text(data.identifier);
	
	return getAttributeValueContainer().append($("<code/>").append(identifierItem)).append(additionalLinks);
};

function nameValueFormatter(data) {
	var simplifiedName = data.simplifiedName.toUpperCase();
	
	var showSimplifiedName = $$.isSet(data.name) && 
	   						 $$.isSet(data.simplifiedName) &&
	   						 data.name.toUpperCase() != simplifiedName;
	
	return getAttributeValueContainer().addClass("selectableOnClick").text(data.name + ( showSimplifiedName ? " [ " + simplifiedName + " ]" : ""));
};

function externalMarkingsValueFormatter(data) {
	return getAttributeValueContainer().append("<code class='selectableOnClick'>" + data.externalMarking + "</code>");
};

function flagValueFormatter(data) {
	var country = $$.Metadata.getCountryByID(data.countryId);
	
	var value = getAttributeValueContainer().append($($$.Flags.getImage(country))).append("<span>" + country.name + "</span>").append("<span class='rightPad'>&nbsp;[&nbsp;<code>" + ( $$.isSet(country.iso2Code) ? country.iso2Code : "--" ) + " / " + ( $$.isSet(country.iso3Code) ? country.iso3Code : "---" ) + "</code>&nbsp;]</span>");
	
	if($$.isSet(country)) {
		var additionalLinks = $("<span class=\"additionalLinks\">&nbsp;</span>");
		var PSMs = $$.Metadata.getPortStateMeasuresFactsheetIDs(country);
		
		if($$.isSet(PSMs) && PSMs.length > 0) {
			var PSMLink = $("<a href=\"#\" " +
							   "class=\"textLink popup\" " +
							   "title=\"" + $$.I18n.getText("vrmf.vessels.formatters.attribute.external.link.country.psm.tip") + "\" " +
							   "onclick=\"javascript:return false;\">" +
							   $$.I18n.getText("vrmf.vessels.formatters.attribute.external.link.country.psm") + 
							"</a>");

			additionalLinks.append(wrapAdditionalLink(PSMLink));
			
			PSMLink.data("PSMCountry", country);
			PSMLink.data("PSMs", PSMs);
			
			PSMLink.click(displayPSMFactsheetsList);
		}
		
		value.append(additionalLinks);
		
		postProcessAdditionalLinks(additionalLinks, $$.I18n.getText("vrmf.vessels.formatters.attribute.external.link.flag.tip"));
		
		return value;
	}
	
	return getAttributeValueContainer();
};

function shipbuilderValueFormatter(data) {
	return $(entityDetailsFormatter($.extend({}, { referenceDate: data.referenceDate}, data.shipbuilder)));
};

function vesselStatusValueFormatter(data) {
	return getAttributeValueContainer().append("<span>" + data.vesselStatus.description + "</span>").
								   		append("<code> - " + data.vesselStatus.id + "</code>").
								   		append("<code>[ " + data.vesselStatus.sourceSystem + " ]</code>");
};

function buildingYearValueFormatter(data) {
	return getAttributeValueContainer().append("<code>" + data.value + "</code>");
};

function hullMaterialValueFormatter(data) {
	return getAttributeValueContainer().append("<span>" + data.hullMaterial.description.toUpperCase() + "</span>").
										append("<code>&nbsp;-&nbsp;" + data.hullMaterial.id + "&nbsp;[&nbsp;" + data.hullMaterial.sourceSystem + "&nbsp;]</code>");
};

function VMSValueFormatter(data) {
	return getAttributeValueContainer().append("<span><b>Has VMS indicator:&nbsp;</b></span>").
										append("<code>" + ( !$$.isSet(data.vmsIndicator) ? "NON SPECIFIED" : ( data.vmsIndicator ? "YES" : "NO" ) ) + "</code>").
										append($$.isSet(data.vmsDetails) ? ( "<span><b>Details:&nbsp;</b></span><code>" + data.vmsDetails + "</code></span>" ) : "");
};

function vesselTypeValueFormatter(data) {
	var hasCountry = $$.isSet(data.vesselType.countryId);
	var hasAbbreviation = $$.isSet(data.vesselType.standardAbbreviation);
	var hasDescription = $$.isSet(data.vesselType.description);
	
	var name = $("<span>" + data.vesselType.name + "</span>");
	
	if(hasDescription) {
		name.addClass("description");
		name.attr("title", data.vesselType.description);
	}
	
	var abbreviation = hasAbbreviation ? $("<code> - " + data.vesselType.standardAbbreviation + "</code>") : null;
	
	if(hasAbbreviation)
		name.append(abbreviation);
	
	name.append("&nbsp;");
	
	if($$.isSet(data.vesselType.isscfvCode)) {
		name.append("<code>" + ( hasAbbreviation ? "" : " ") + "- " + data.vesselType.isscfvCode + "</code>");
		name.append("&nbsp;");
	}	
	
	var buildProperLink = linkToExternalVesselTypeDataBySource(data.vesselType.sourceSystem) && $$.Metadata.getVesselTypeFactsheetLink(data.vesselType.originalVesselTypeId) != null;
	var vesselTypeLink;
	
	if(buildProperLink) {
		vesselTypeLink = buildExternalLink($$.Metadata.getVesselTypeFactsheetLink(data.vesselType.originalVesselTypeId), $$.I18n.getText("vrmf.vessels.formatters.attribute.external.link.figis.type.vessel.tip"), "_FIGIS_VESSEL_TYPE_" + data.vesselType.originalVesselTypeId);
	} else {
		vesselTypeLink = $("<span/>");
		vesselTypeLink.addClass("moreRightPad").attr("title", $$.I18n.getText("vrmf.vessels.formatters.attribute.external.link.figis.type.vessel.code.original.tip"));
	}
	
	vesselTypeLink.addClass("moreLeftPad");
		
	vesselTypeLink.append($("<b>" + data.vesselType.sourceSystem  + "</b>"));
	vesselTypeLink.append($("<span>&nbsp;/&nbsp;" + (hasCountry ? $$.Metadata.getCountryByID(data.vesselType.countryId).iso2Code + "&nbsp;/&nbsp;" : "") + data.vesselType.originalVesselTypeId + "</span>"));
		
	var codeWrapper = $("<code/>");

	codeWrapper.append(wrapAdditionalLink(vesselTypeLink));
	
	return getAttributeValueContainer().append(VRMFIDDetailsFormatter(data.vesselType.id, "right")).append(name).append(codeWrapper);
};

function gearTypeValueFormatter(data) {
	var hasAbbreviation = $$.isSet(data.gearType.standardAbbreviation);
	var hasDescription = $$.isSet(data.gearType.description);

	var name = $("<span>" + data.gearType.name + "</span>");

	if(hasDescription) {
		name.addClass("description");
		name.attr("title", data.gearType.description);
	}

	var abbreviation = hasAbbreviation ? $("<code> - " + data.gearType.standardAbbreviation + "</code>") : null;

	if(hasAbbreviation)
		name.append(abbreviation);

	name.append("&nbsp;");

	if($$.isSet(data.gearType.isscfgCode)) {
		name.append("<code>" + ( hasAbbreviation ? "" : " ") + "- " + data.gearType.isscfgCode + "</code>");
		name.append("&nbsp;");
	}
	
	var buildProperLink = linkToExternalGearTypeDataBySource(data.gearType.sourceSystem) && $$.Metadata.getGearTypeFactsheetLink(data.gearType.originalGearTypeCode) != null;
	var gearTypeLink;
	
	if(buildProperLink) {
		gearTypeLink = buildExternalLink($$.Metadata.getGearTypeFactsheetLink(data.gearType.originalGearTypeCode), $$.I18n.getText("vrmf.vessels.formatters.attribute.external.link.figis.type.gear.tip"), "_FIGIS_GEAR_TYPE_" + data.gearType.originalGearTypeCode);
	} else {
		gearTypeLink = $("<span/>").addClass("moreRightPad").attr("title", $$.I18n.getText("vrmf.vessels.formatters.attribute.external.link.figis.type.gear.code.original.tip"));
	}

	gearTypeLink.addClass("moreLeftPad");
		
	gearTypeLink.append($("<b>" + data.gearType.sourceSystem  + "</b>"));
	gearTypeLink.append($("<span>&nbsp;/&nbsp;" + data.gearType.originalGearTypeCode + "</span>"));

	var codeWrapper = $("<code/>");

	codeWrapper.append(wrapAdditionalLink(gearTypeLink));

	return getAttributeValueContainer().append(VRMFIDDetailsFormatter(data.gearType.id, "right")).append(name).append(codeWrapper);
};

function callsignValueFormatter(data) {
	var currentCallsignCountry = $$.Metadata.getCountryByID(data.countryId);

	var additionalLinks = $("<span class=\"additionalLinks leftPad\"/>");
	
	var linkBuilders = $$.vrmf.ExternalReferences.getLinkBuilders("IRCS", null);
	
	if($$.isSet(linkBuilders)) {
		var link = builder = null;
		
		for(var l=0; l<linkBuilders.length; l++) {
			builder = linkBuilders[l];
			
			link = $$.vrmf.ExternalReferences.buildLink("IRCS", builder, data, /* Default preprocessor */ function(d) { return d.callsignId; });

			if(link != null) {
				link = link.addClass("externalProvider");
				
				additionalLinks.append(wrapAdditionalLink(link));
			}
		}

		postProcessAdditionalLinks(additionalLinks, $$.I18n.getText("vrmf.vessels.formatters.attribute.external.link.ircs.tip"));
	} 
	
	return getAttributeValueContainer().append($("<code/>").append($$.Flags.getImage(currentCallsignCountry)).append("<span class='rightPad selectableOnClick'>" + data.callsignId + "</span>")).append(additionalLinks);
};

function fishingLicenseValueFormatter(data) {
	var currentFishingLicenseCountry = $$.Metadata.getCountryByID(data.countryId);

	var additionalLinks = $("<span class=\"additionalLinks leftPad\"/>");
	
	var linkBuilders = $$.vrmf.ExternalReferences.getLinkBuilders("FISHING_LICENSE", null);
	
	if($$.isSet(linkBuilders)) {
		var link = builder = null;
		
		for(var l=0; l<linkBuilders.length; l++) {
			builder = linkBuilders[l];
			
			link = $$.vrmf.ExternalReferences.buildLink("FISHING_LICENSE", builder, data, /* Default preprocessor */ function(d) { return d.licenseId; });

			if(link != null) {
				link = link.addClass("externalProvider");
				
				additionalLinks.append(wrapAdditionalLink(link));
			}
		}

		postProcessAdditionalLinks(additionalLinks, $$.I18n.getText("vrmf.vessels.formatters.attribute.external.link.fishing.license.tip"));
	} 
	
	return getAttributeValueContainer().append($("<code/>").append($$.Flags.getImage(currentFishingLicenseCountry)).append("<span class='selectableOnClick'>" + data.licenseId + "</span>")).append(additionalLinks);
};

function MMSIValueFormatter(data) {
	var additionalLinks = $("<span class=\"additionalLinks moreLeftPad\"/>");
	
	var linkBuilders = $$.vrmf.ExternalReferences.getLinkBuilders("MMSI", null);
	
	if($$.isSet(linkBuilders)) {
		var link = builder = null;
		
		for(var l=0; l<linkBuilders.length; l++) {
			builder = linkBuilders[l];
			
			link = $$.vrmf.ExternalReferences.buildLink("MMSI", builder, data, /* Default preprocessor */ function(d) { return d.mmsi; });

			if(link != null) {
				link = link.addClass("externalProvider");
				
				additionalLinks.append(wrapAdditionalLink(link));
			}
		}

		postProcessAdditionalLinks(additionalLinks, $$.I18n.getText("vrmf.vessels.formatters.attribute.external.link.mmsi.tip"));
	} 
	
	return getAttributeValueContainer().append($("<code class='selectableOnClick'/>").text(data.mmsi)).append(additionalLinks);
};

function registrationNumberValueFormatter(data) {
	return getAttributeValueContainer().append($("<code class='selectableOnClick'>" + data.registrationNumber + "</code>"));
};

function fullRegistrationNumberValueFormatter(data) {
	var detail = getAttributeValueContainer();
	var hasPort = $$.isSet(data.registrationPort);
	var hasRegCountry = hasPort || $$.isSet(data.countryId);
	
	var regCountry = hasRegCountry ? $$.Metadata.getCountryByID(data.countryId) : null;
	var regCountryFlag = hasRegCountry ? $$.Flags.getImageByCountryID(hasPort ? data.registrationPort.countryId : data.countryId) : "";
	
	var regNo = $("<code class=\"rightPad selectableOnClick" + ( hasRegCountry ? "" : " noCountry" ) + "\">" +
					"<b>" + 
					data.registrationNumber + 
					"</b>" +
				  "</code>");
	
	if(hasRegCountry)
		detail.append(regCountryFlag);
	
	detail.append(regNo);
	
	if(hasPort) {
		var hasCoordinates = $$.isSet(data.registrationPort.coordinates);
		var displayCoordinates = hasCoordinates;
		
		var portDescription = $("<span>at&nbsp;</span>");
		
		portDescription.append(VRMFIDDetailsFormatter(data.registrationPort.id, "right"));
				
		var portData = $("<a href=\"#\" " +
							"onclick=\"javascript:return false;\" " +
							"class=\"textLink portName\">" +
							"<span class=\"port\">" + 
								data.registrationPort.name + 
							"</span>" +
						"</a>");
		
		if(displayCoordinates) {
			$(".port", portData).addClass("displayOnMap").attr("title", $$.I18n.getText("vrmf.vessels.formatters.attribute.external.link.port.coordinates.tip", data.registrationPort.coordinates.length));
		}
				
		portDescription.append(portData);
		
		if(displayCoordinates) {
			var trigger = $(".port", portData);
			trigger.data("portData", data.registrationPort);
			trigger.data("portCountry", regCountry);
				
			trigger.click(displayPortCoordinates);
		} 
		
		detail.append(portDescription);
				
		var regCountry = $$.Metadata.getCountryByID(data.registrationPort.countryId);
		
		var portMeta = $("<code>[&nbsp;" + 
							"<span title=\"" + $$.I18n.getText("vrmf.vessels.formatters.attribute.registration.port.code.original.tip") + "\">" + 
								data.registrationPort.sourceSystem + " - " +
								( $$.isSet(regCountry) ? regCountry.iso2Code + " / " : "" ) + 
								data.registrationPort.originalPortId +
							"</span>" +
						 "&nbsp;]</code>");
		
		detail.append(portMeta);
	}
	
	var additionalLinks = $("<span class=\"additionalLinks\">&nbsp;</span>");

	var regCountry = $$.Metadata.getCountryByID($$.isSet(data.registrationPort) ? data.registrationPort.countryId : data.countryId); 
	
	var PSMs = $$.Metadata.getPortStateMeasuresFactsheetIDs(regCountry);
	
	if($$.isSet(PSMs) && PSMs.length > 0) {
		var PSMLink = $("<a href=\"#\" " +
						   "class=\"textLink popup\" " +
						   "title=\"" + $$.I18n.getText("vrmf.vessels.formatters.attribute.external.link.country.psm.tip") + "\" " +
						   "onclick=\"javascript:return false;\">" +
						   $$.I18n.getText("vrmf.vessels.formatters.attribute.external.link.country.psm") +
						"</a>");

		additionalLinks.append(wrapAdditionalLink(PSMLink));
		
		PSMLink.data("PSMCountry", regCountry);
		PSMLink.data("PSMs", PSMs);
		
		PSMLink.click(displayPSMFactsheetsList);
	}
	
	var searchByRegNo = $$.isSet(regCountry) && 
						$$.isSet(regCountry.iso3Code);
	
	if(searchByRegNo) {
		var linkBuilders = $$.vrmf.ExternalReferences.getLinkBuilders("REG_NO", regCountry.iso3Code);
		
		if($$.isSet(linkBuilders)) {
			var link = builder = null;
			
			for(var l=0; l<linkBuilders.length; l++) {
				builder = linkBuilders[l];
				
				link = $$.vrmf.ExternalReferences.buildLink("REG_NO", builder, data, /* Default preprocessor */ function(d) { return d.registrationNumber; });
				
				if(link != null) {
					link = link.addClass("externalProvider");
					
					additionalLinks.append(wrapAdditionalLink(link));
				}
			}
		} 
	}
	
	detail.append(additionalLinks);
	
	postProcessAdditionalLinks(additionalLinks, $$.I18n.getText("vrmf.vessels.formatters.attribute.external.link.reg.number.tip"));
		
	return detail;
};

function lengthValueFormatter(data) {
	var defaultMeasureUnit = $$.Metadata.LENGTH_TYPES_MAP[data.typeId].defaultUnit;
	var currentMeasureUnit = $$.isSet(data.unit) ? data.unit : null;;
	
	var unit = null;
	
	if($$.isSet(currentMeasureUnit))
		unit = $$.Metadata.MEASURE_UNITS_MAP[currentMeasureUnit];
	else
		unit = $$.Metadata.MEASURE_UNITS_MAP[defaultMeasureUnit];
	
	return getAttributeValueContainer().text(data.value + " " + ( $$.isSet(unit) ? unit.symbol + " (" + unit.description + ")" : "m (meters)" ));
};

function tonnageValueFormatter(data) {
	var defaultMeasureUnit = $$.Metadata.TONNAGE_TYPES_MAP[data.typeId].defaultUnit;
	var currentMeasureUnit = $$.isSet(data.unit) ? data.unit : null;;
	
	var unit = null;
	
	if($$.isSet(currentMeasureUnit))
		unit = $$.Metadata.MEASURE_UNITS_MAP[currentMeasureUnit];
	else
		unit = $$.Metadata.MEASURE_UNITS_MAP[defaultMeasureUnit];
	
	return getAttributeValueContainer().text(data.value + " " + ( $$.isSet(unit) ? unit.symbol + " (" + unit.description + ")" : "mt (metric tons)" ));
//	return getAttributeValueContainer().text(data.value + " metric tons");
};

function powerValueFormatter(data) {
	return getAttributeValueContainer().text(data.value + " " + data.typeId);
};

function crewValueFormatter(data) {
	return getAttributeValueContainer().text(data.value + " " + $$.I18n.getText("vrmf.vessels.formatters.attribute.registration.crew." + ( data.value == 1 ? "person" : "people" )));
};

function ownerValueFormatter(data) {
	return $(entityDetailsFormatter($.extend({}, { referenceDate: data.referenceDate}, data.owner)));
};

function operatorValueFormatter(data) {
	return $(entityDetailsFormatter($.extend({}, { referenceDate: data.referenceDate}, data.operator)));
};

function authorizationsValueFormatter(data) {
	var container = $("<span/>");
	
	var code = $("<code class=\"authorizationCode\">#" + data.authorizationId + "&nbsp;</code>");	
	
	var validityStart = $("<span class=\"date\"/>");
	var validityEnd = $("<span class=\"date\"/>");
	var terminationDate = $("<span class=\"date\"/>");
	var issuingCountry = data.issuingCountryId == null ? null : $$.Metadata.getCountryByID(data.issuingCountryId);
	
	var validFrom = null;
	var validTo = null;
	var terminationReferenceDate = null;
	
	if(data.validFrom != null) {
		validFrom = new Date(data.validFrom);
		validityStart.text(validFrom.formatDate());
	}
	
	if(data.validTo != null) {
		validTo = new Date(data.validTo);
		validityEnd.text(validTo.formatDate());
	}
	
	if(data.terminationReferenceDate != null) {
		terminationReferenceDate = new Date(data.terminationReferenceDate);
		terminationDate.text(terminationReferenceDate.formatDate());
	}
	
	container.append(code);
	
	if(issuingCountry != null)
		container.append($$.Flags.getImage(issuingCountry));

	container.append($("<label>&nbsp;-&nbsp;" + $$.I18n.getText("vrmf.vessels.formatters.attribute.authorization.type") + "&nbsp;</label>"));
	
	var authType = $$.Metadata.AUTHORIZATION_TYPES_MAP[data.sourceSystem + "_" + data.typeId];
	
	container.append($("<code class=\"description\">" + authType.description + "&nbsp;</code>").attr("title", authType.comment));
	
	if(validFrom != null) {
		container.append($("<label>" + $$.I18n.getText("vrmf.vessels.formatters.attribute.authorization.from") + "&nbsp;</label>"));
		container.append(validityStart);
	} /* else {
		container.append("<span>[ NOT SET ]</span>");
	} */
		
	if(validTo != null) {
		container.append($("<label>&nbsp;" + $$.I18n.getText("vrmf.vessels.formatters.attribute.authorization.to") + "&nbsp;</label>"));
		container.append(validityEnd);
	} 
	
	if(terminationReferenceDate != null) {
		container.append($("<br/><label>" + $$.I18n.getText("vrmf.vessels.formatters.attribute.authorization.terminated.on") + "&nbsp;</label>"));
		container.append(terminationDate);
		
		if($$.isSet(data.terminationReasonCode)) {
			container.append($("<label>&nbsp;-&nbsp;" + $$.I18n.getText("vrmf.vessels.formatters.attribute.authorization.terminated.reason") + "&nbsp;</label>"));
			
			var reasonCode = $("<span class=\"authorizationTerminationReasonCode description xSmallFont\"/>");
			var terminationReason = $$.Metadata.AUTHORIZATION_TERMINATION_REASONS_MAP[data.terminationReasonCode];
			
			reasonCode.append(VRMFIDDetailsFormatter(terminationReason.id));

			reasonCode.append("<span>" + terminationReason.description + "</span>");
			reasonCode.attr("title", 
						    $$.rawTrim(terminationReason.description) + 
						  ( $$.isSet(data.terminationReason) ? " [ " + $$.rawTrim(data.terminationReason) + " ]" : "" ) );

			reasonCode.css("cursor", "help");

			container.append(reasonCode);
		};		
	}
	
//	var now = new Date();
//		
//	if(terminationReferenceDate != null) {
//		code.attr("title", "This authorization has been terminated");
//		code.addClass("terminated");
//	} else {
//		if(validTo != null && data.validTo < now.getTime()) {
//			code.attr("title", "This authorization has expired but has not been terminated");
//			code.addClass("expired");
//		} else {
//			code.attr("title", "This authorization is currently enforced");
//			code.addClass("authorized");
//		}
//	}
	
	var authorizationStatus = getAuthorizationStatusCode(data);
	
	code.attr("title", $$.I18n.getText("vrmf.vessels.formatters.attribute.authorization.status." + authorizationStatus));
	code.addClass(authorizationStatus);
				
	if($$.isSet(data.details) && data.details.length > 0) {
		var detail;
		for(var d=0; d<data.details.length; d++) {
			detail = data.details[d];

			container.append("<br/>");
			
			if($$.isSet(detail.fishingZone)) {
				var zoneId = detail.fishingZone.originalId;

				if(zoneId.indexOf("FAO_" == 0)) {
					var zoneNumber = parseInt(zoneId.substring(4, 6), 10);
					
					zoneId = "<a href=\"http://www.fao.org/fishery/area/Area" + zoneNumber + "/en\" class=\"textLink static\" target=\"_FAO_FISHING_AREA_" + zoneNumber + "\" title=\"" + $$.I18n.getText("vrmf.vessels.formatters.attribute.external.link.figis.area.fao.tip") + "\">" + zoneId.substring(0, 6) + "</a>" + zoneId.substring(6);
				}

				container.append("<span class=\"smallFont\">" +
									"<label>" + $$.I18n.getText("vrmf.vessels.formatters.attribute.authorization.zone") + "&nbsp;</label>" +
									"<code>" + /* detail.fishingZone.sourceSystem + " / " + */ zoneId + 
									( $$.isSet(detail.fishingZone.description) ? " [ " + detail.fishingZone.description + " ]" : "" ) + 
									"</code>&nbsp;-&nbsp;" +
								 "</span>");
			}
			
			if($$.isSet(detail.gearType)) {
				container.append("<span class=\"smallFont\"><label>" + $$.I18n.getText("vrmf.vessels.formatters.attribute.authorization.gear") + "&nbsp;</label><code>" + detail.gearType.sourceSystem + " / " + detail.gearType.description + "</code>&nbsp;-&nbsp;</span>");
			}
			
			if($$.isSet(detail.species)) {
				var buildProperLink = $$.Metadata.getSpeciesFactsheetLink(detail.species.figisId) != null;
				var speciesLink;
				
				if(buildProperLink) {
					speciesLink = buildExternalLink($$.Metadata.getSpeciesFactsheetLink(detail.species.figisId), $$.I18n.getText("vrmf.vessels.formatters.attribute.external.link.figis.species.tip"), "_FIGIS_SPECIES_" + detail.species.figisId);
				} else {
					var has3AlphaCode = $$.isSet(detail.species.code3a);
					var hasEnglishName = $$.isSet(detail.species.name);
					var hasScientificName = $$.isSet(detail.species.scientificName);
					
					var keywords = [];
					
					if(has3AlphaCode)
						keywords.push(detail.species.code3a);
					
					if(hasEnglishName)
						keywords.push(detail.species.name);
					
					if(hasScientificName)
						keywords.push(detail.species.scientificName);
										
					if(keywords.length > 0) {
						speciesLink = buildExternalLink(FIGIS_SPECIES_SEARCH_URL.replace(/\{query\}/g, keywords.join(",")), $$.I18n.getText("vrmf.vessels.formatters.attribute.external.link.figis.species.search.tip"), "_FIGIS_SPECIES_" + keywords.join("_"));
					} else {
						speciesLink = $("<span/>");
					}
				}
				
				speciesLink.text(detail.species.code3a + " / " + detail.species.name);
				speciesLink.addClass("moreLeftPad");

				var wrapper = $("<span class=\"smallFont\"/>");
				wrapper.append("<label>" + $$.I18n.getText("vrmf.vessels.formatters.attribute.authorization.species") + "&nbsp;</label>");
				wrapper.append($("<code/>").append("[").append(speciesLink).append("]"));

				container.append(wrapper);
			}
		}
	}
	
	return getAttributeValueContainer().append(container);
};

$$.Dep.require("vrmf.vessels.formatters.attributes", { "vrmf.metadata": { identifier: "$$.Metadata" }, 
													   "vrmf.interface": { identifier: "$$.Interface" }, 
													   "vrmf.i18n": { identifier: "$$.I18n" } });