// Default or common formatters
function getMetaContainer() {
	return $("<div class='attributeMeta'/>");
};

function addMetaPrefix(meta) {
	return meta.prepend("<span>[&nbsp;</span>");
};

function addMetaSuffix(meta) {
	return meta.append("<span>&nbsp;]</span>");
};

function addMetaSeparator(meta) {
	return meta.append("<span>&nbsp;-&nbsp;</span>");
};

function sourceSystemDetailsFormatter(details, standalone, vesselIdExtractor) {
	var currentExtractor = $$.isSet(vesselIdExtractor) ? vesselIdExtractor : function(data) { return data.vesselId; };
	
	var loggedUser = $$.UserManager.getLoggedUser();

	var canDisplayID = loggedUser != null && loggedUser.can("DISPLAY_V_ID");
	var canDisplaySource = loggedUser == null || loggedUser.id === "NOT_SET" || loggedUser.can("DISPLAY_V_SRC");

	var meta = getMetaContainer();

	if(canDisplayID && $$.isSet(currentExtractor(details))) {
		meta.append("<label>VRMF ID:&nbsp;</label>");
		meta.append(currentExtractor(details));

		if(canDisplaySource) {
			addMetaSeparator(meta);
		}
	}

	if(canDisplaySource) {
		meta.append("<label>" + $$.I18n.getText("vrmf.vessels.formatters.src") + "&nbsp;</label>");
		meta.append("<span class='sourceSystem " + details.sourceSystem + "'>" + details.sourceSystem + "</span>");
	}

	if(!$$.isSet(standalone) || standalone) {
		if(canDisplayID || canDisplaySource)
			meta = addMetaPrefix(addMetaSuffix(meta));
	}

	return meta;
};

function sourceSystemComponentVesselsDetailsFormatter(details, standalone) {
	return sourceSystemDetailsFormatter(details, standalone, function(data) { return data.id; });
};

function sourceSystemAndReferenceDateDetailsFormatter(details) {
	var loggedUser = $$.UserManager.getLoggedUser();

	var canDisplayID = loggedUser == null || loggedUser.id === "NOT_SET" ||  loggedUser.can("DISPLAY_V_ID");
	var canDisplaySource = loggedUser == null || loggedUser.id === "NOT_SET" ||  loggedUser.can("DISPLAY_V_SRC");
	var canDisplayReferenceDate = loggedUser == null || loggedUser.id === "NOT_SET" ||  loggedUser.can("DISPLAY_V_REF");

	var referenceDate = null;

	if($$.isSet(details.referenceDate)) {
		referenceDate = new Date();
		referenceDate.setTime(details.referenceDate);
	}

	var meta = sourceSystemDetailsFormatter(details, false);

	if((canDisplayID || canDisplaySource) && canDisplayReferenceDate)
		addMetaSeparator(meta);

	if(canDisplayReferenceDate) {
		meta.append("<label>" + $$.I18n.getText("vrmf.vessels.formatters.reference.date") + "&nbsp;</label>");
		meta.append("<span class='referenceDate'>" + ( referenceDate == null ? "<" + $$.I18n.getText("vrmf.vessels.formatters.reference.date.unknown") + ">" : referenceDate.formatDate() ) + "</span>");
	}

	if(canDisplayID || canDisplaySource || canDisplayReferenceDate)
		meta = addMetaPrefix(addMetaSuffix(meta));

	return meta;
};

function VRMFIDDetailsFormatter(VRMFID, padding) {
	if($$.Preferences.showVRMFInternalIDs && $$.isSet(VRMFID)) {
		var noPad = !$$.isSet(padding) || "none" == padding; //Default, if no padding is provided
		var padLeft = !noPad && "left" == padding; 
		var padRight = !noPad && "right" == padding;

		var additionalClass = padLeft ? "leftPad" : ( padRight ? "rightPad" : "" );

		return $("<span class=\"idMark " + additionalClass + "\" title=\"" + $$.I18n.getText("vrmf.vessels.formatters.vrmf.id.tip") + "\">" +
				 ( noPad ? "" : ( padRight ? "" : " " ) ) +
				 "[ #" + VRMFID + " ]" +
				 //( noPad ? "" : ( padRight ? "&nbsp;>" : "" ) ) +
				 "</span>");
	} else {
		return $("");
	}
};

//Vessel attributes' formatters
function entityDetailsFormatter(entity) {
	var hasName = $$.isSet(entity.name);
	var hasAddress = $$.isSet(entity.address);
	var hasZip = $$.isSet(entity.zipCode);
	var hasCity = $$.isSet(entity.city);
	var hasCountry = $$.isSet(entity.countryId) && $$.isSet($$.Metadata.getCountryByID(entity.countryId));

	var entityDetail = [];

	var defaultTooltipMetadata = "{  my: 'TOP_CENTER', at: 'BOTTOM_CENTER' }";

	if(hasName)
		entityDetail.push("<span class=\"entityAttribute entityName\" " +
								 $$.TooltipManager.TOOLTIP_CONFIGURATION_ATTRIBUTE + "=\"" + defaultTooltipMetadata + "\" title='" + $$.I18n.getText("vrmf.vessels.formatters.entity.name.tip") + "'>" + entity.name.trim() + "</span>");

	if(hasAddress)
		entityDetail.push("<span class=\"entityAttribute entityAddress displayOnMap\" " +
								 $$.TooltipManager.TOOLTIP_CONFIGURATION_ATTRIBUTE + "=\"" + defaultTooltipMetadata + "\" title='" + $$.I18n.getText("vrmf.vessels.formatters.entity.address.map.tip") + "'>" + entity.address.trim() + "</span></a>");

	if(hasZip)
		entityDetail.push("<span class=\"entityAttribute entityZipCode\" " + 
								 $$.TooltipManager.TOOLTIP_CONFIGURATION_ATTRIBUTE + "=\"" + defaultTooltipMetadata + "\" title='" + $$.I18n.getText("vrmf.vessels.formatters.entity.zip.tip") + "'>" + entity.zipCode.trim() + "</span>");

	if(hasCity)
		entityDetail.push("<span class=\"entityAttribute entityCity\" " + 
								 $$.TooltipManager.TOOLTIP_CONFIGURATION_ATTRIBUTE + "=\"" + defaultTooltipMetadata + "\" title='" + $$.I18n.getText("vrmf.vessels.formatters.entity.city.tip") + "'>" + entity.city.trim() + "</span>");

	var serialized = entityDetail.length > 0 ? entityDetail.join(", ") : "";

	if(hasCountry)
		serialized += ( entityDetail.length > 0 ? " " : "" ) +
					  "[&nbsp;<span class=\"entityCountry entityAttribute\" title='" + $$.I18n.getText("vrmf.vessels.formatters.entity.country.tip") + "'>" + 
						  $$.Metadata.getCountryByID(entity.countryId).name.toUpperCase() + 
					    "</span>&nbsp;]";

	var detail = $("<span class=\"xSmallFont" + ( hasCountry ? "" : " noCountry") + "\" >" + serialized + "</span>");

	var toReturn = getAttributeValueContainer().addClass("selectableOnClick").
												append(VRMFIDDetailsFormatter(entity.id, "right")).
												append(hasCountry ? $$.Flags.getImageByCountryID(entity.countryId) : "").
												append(detail).
												removeClass("attributeValue").addClass("longAttributeValue");

	if(hasAddress) {
		var target = $(".entityAddress", detail);
		var link = $("<a class=\"textLink\" href=\"#\" onclick=\"javascript:return false;\"/>");
		
		target = target.wrap(link);
		target.data("entity", entity);
		
		target.click(displayAddress);
	}

	return toReturn;
};

function buildPseudoExternalLink() {
	return $("<a href=\"#\" class=\"textLink external\" onclick=\"javascript:return false;\"/>");
};

function buildEmptyExternalLink(tooltip) {
	var link = $("<a href=\"#\" class=\"textLink external\" target=\"_blank\"/>");

	link.attr("title", $$.isSet(tooltip) ? tooltip : $$.I18n.getText("vrmf.vessels.formatters.link.external.tip"));

	return link;
};

function buildExternalLink(url, tooltip, target) {
	var link = buildEmptyExternalLink(tooltip);

	link.attr("target", target);
	link.attr("href", url);
	link.addClass("{ target: '" + target + "' }");

	return link;
};

function linkToExternalVesselDataByIdentifier(identifierTypeId) {
	//THIS SUCKS!
	return $$.isSet(EXTERNAL_SEARCH_BY_ID_MAP[identifierTypeId]);
};

function linkToExternalVesselTypeDataBySource(source) {
	return "FIGIS" == source;
};

function linkToExternalGearTypeDataBySource(source) {
	return "FIGIS" == source;
};

function linkToExternalPortStateMeasure(country) {
	return $$.isSet(country.iso3Code);
};

$$.Dep.require("vrmf.vessels.formatters", { "vrmf.metadata": { identifier: "$$.Metadata" }, 
											"vrmf.i18n": { identifier: "$$.I18n" } });