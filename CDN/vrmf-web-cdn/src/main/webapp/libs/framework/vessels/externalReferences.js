var CLAV_VESSEL_DETAIL_URL  = "http://www.iattc.org/GVR/duplicatedvessels.aspx?IDDup={query}";
var NCLAV_VESSEL_DETAIL_URL = "http://www.fao.org/figis/vrmf/nclav/services/public/display/vessel/TUVI/{query}";
var EU_VESSEL_DETAIL_URL    = "http://ec.europa.eu/fisheries/fleet/index.cfm?method=Search.ListSearchSimpleOneVessel&COUNTRY_CODE=&CFR_CODE={query}&search_type=CFR";
var OLD_HSVAR_VESSEL_DETAIL_URL = "http://www.fao.org/fi/hsvar/vesselDetail.jsp?vesselID={query}";
var HSVAR_VESSEL_DETAIL_URL = "http://www.fao.org/figis/vrmf/hsvar/display/uid/{query}";
var ICCAT_VESSEL_DETAIL_URL = "http://www.iccat.int/en/VesselsRecordRes.asp?cajaRecord=checkbox&cajaFlag=checkbox&cajaFlag2=checkbox&cajaVessel=checkbox&cajaIRCS=checkbox&cajaRegistry=checkbox&textSN={query}&selectOrder=1&selectOrder2=6&selectInterval=25&Submit=Search";
//var IOTC_VESSEL_DETAIL_URL  = "http://www.iotc.org/English/record/record_vessel3.php?vid={query}";
var IOTC_VESSEL_DETAIL_URL  = "http://www.iotc.org/vessels/history/{query}";
var WCPFC_VESSEL_DETAIL_URL = "http://intra.wcpfc.int/Lists/Vessels/DispForm.aspx?ID={query}&Source=http%3A%2F%2Fintra%2Ewcpfc%2Eint%2FLists%2FVessels%2FStats%2Easpx";
var IATTC_VESSEL_DETAIL_URL = "http://www.iattc.org/VesselRegister/VesselDetails.aspx?VesNo={query}&Lang=en";
var CCSBT_VESSEL_DETAIL_URL = "http://www.ccsbt.org/site/authorised_vessels_detail.php?id={query}";
var SICA_VESSEL_DETAIL_URL  = "http://www.sica.int/WSIRPAC/Paginas/Formularios/Form_Embarcaciones.aspx";
var ISSF_VESSEL_DETAIL_URL  = "http://iss-foundation.org/imo-database";
var SPRFMO_VESSEL_DETAIL_URL= "http://www.sprfmo.org";
var FFA_VESSEL_DETAIL_URL   = "http://www.ffa.int/node/42";
var CCAMLR_VESSEL_DETAIL_URL= "http://www.ccamlr.org/en/node/{query}";

//var IMO_VESSEL_DETAIL_WORLD_SHIPS_URL = "http://www.e-ships.net/new/?View=ShipSearchResult&builder_id=&call_sign=&fbeam=&fbuilt=&fdraft=&fdwt=&fengine_hp_total=&fengine_kw_total=&fgt=&flag=-1&floa=&fnrt=&fteu=&imo={query}&last_ex_name=&manager_id=&owner_id=&ship_class=-1&ship_name=&ship_type=-1&sortby=ship_name&tbeam=&tbuilt=&tdraft=&tdwt=&tengine_hp_total=&tengine_kw_total=&tgt=&tloa=&tnrt=&tteu=";
var IMO_VESSEL_DETAIL_WORLD_SHIPS_URL 	 = "http://www.world-ships.com/?p=new%2F&Ship%5Bship_name%5D=&Ship%5Blast_ex_name%5D=&Ship%5Bimo%5D={query}&Ship%5Bmaritime_mobile%5D=&Ship%5Bship_type%5D=&Ship%5Bflag%5D=&Ship%5Bclass1%5D=&Ship%5Bcall_sign%5D=&Ship%5Bengine_builder%5D=&Ship%5Bbuilder%5D=&Ship%5Bowner%5D=&Ship%5Bmanager%5D=&Ship%5Bfdwt%5D=&Ship%5Bfgt%5D=&Ship%5Bfnrt%5D=&Ship%5Bfteu%5D=&Ship%5Bfloa%5D=&Ship%5Bfbeam%5D=&Ship%5Bfdraft%5D=&Ship%5Bfbuilt%5D=&Ship%5Bfengine_kw_total%5D=&Ship%5Bfengine_hp_total%5D=&Ship%5Btdwt%5D=&Ship%5Btgt%5D=&Ship%5Btnrt%5D=&Ship%5Btteu%5D=&Ship%5Btloa%5D=&Ship%5Btbeam%5D=&Ship%5Btdraft%5D=&Ship%5Btbuilt%5D=&Ship%5Btengine_kw_total%5D=&Ship%5Btengine_hp_total%5D=&Ship%5BperPage%5D=50&ajax=ship-grid";
var IMO_VESSEL_DETAIL_MARINE_TRAFFIC_URL = "http://www.marinetraffic.com/ais/datasheet.aspx?SHIPNAME=&IMO={query}&TYPE_SUMMARY=&DESTINATION=&menuid=&datasource=V_SHIP&app=&mode=&B1=Search";
var IMO_VESSEL_DETAIL_SHIP_SPOTTING_URL  = "http://www.shipspotting.com/gallery/photo_search.php?query={query}";
var IMO_VESSEL_DETAIL_EQUASIS_URL 		 = "http://www.equasis.org/EquasisWeb/restricted/ShipList?fs=ShipSearch&P_IMO={query}";
var IMO_VESSEL_DETAIL_WIKIMEDIA_URL		 = "http://commons.wikimedia.org/wiki/Category:IMO_{query}";
var IMO_VESSEL_DETAIL_SHIPTRACKING_URL	 = "http://www.shiptracking.eu/ais/#/vesselsearch?part={query}";
var IMO_VESSEL_DETAIL_MARITIME_CON_URL	 = "http://maritime-connector.com/ship-search/?imo={query}";
var IMO_VESSEL_DETAIL_IMO_NUMBER_URL	 = "http://www.imonumber.com/site/details?imo={query}";
var IMO_VESSEL_DETAIL_SHIPLIST_URL		 = "http://www.shiplist.vitron.org/ship/imo/{query}";
var IMO_VESSEL_DETAIL_VT_EXPLORER_URL	 = "http://vessels.vtexplorer.com/vessel/0-IMO-{query}";
var IMO_VESSEL_DETAIL_SHIPPING_EXPL_URL  = "http://www.shippingexplorer.net/en/vessels/search?s={query}&Area=3";

var MMSI_VESSEL_DETAIL_ITU_URL 			 = "http://www.itu.int/online/mms/mars/ship_search.sh?lang=en&lgg=7&p=1&cgaid=0&sh_callsign=&sh_epirb_hex=&sh_epirb_id=&sh_mmsi={query}&sh_name=";
var MMSI_VESSEL_DETAIL_MARINE_TRAFFIC_URL= IMO_VESSEL_DETAIL_MARINE_TRAFFIC_URL;
var MMSI_VESSEL_DETAIL_SHIP_SPOTTING_URL = "http://www.shipspotting.com/gallery/extra_search.php?mmsi={query}";
var MMSI_VESSEL_DETAIL_SHIPTRACKING_URL	 = "http://www.shiptracking.eu/ais/#/getvesseldetails?mmsi={query}";
//var MMSI_TRACK_VESSEL_MARINE_TRAFFIC_URL = "http://www.marinetraffic.com/ais/default.aspx?zoom=13&mmsi={query}";
var MMSI_TRACK_VESSEL_MARINE_TRAFFIC_URL = "http://www.marinetraffic.com/ais/datasheet.aspx?datasource=ITINERARIES&MMSI={query}";
var MMSI_VESSEL_DETAIL_VT_EXPLORER_URL   = "http://vessels.vtexplorer.com/vessels/list?search_text={query}";
var MMSI_VESSEL_DETAIL_SHIPPING_EXPL_URL = "http://www.shippingexplorer.net/en/vessels/search?s={query}&Area=2&VesselType=15";
	
var CALLSIGN_VESSEL_DETAIL_ITU_URL			 = "http://www.itu.int/online/mms/mars/ship_search.sh?lang=en&lgg=7&p=1&cgaid=0&sh_callsign={query}&sh_epirb_hex=&sh_epirb_id=&sh_mmsi=&sh_name=";
var CALLSIGN_VESSEL_DETAIL_SHIP_SPOTTING_URL = "http://www.shipspotting.com/gallery/extra_search.php?callsign={query}";
var CALLSIGN_VESSEL_DETAIL_SHIPPING_EXPL_URL = "http://www.shippingexplorer.net/en/vessels/search?s={query}&Area=4&VesselType=15";
var CALLSIGN_VESSEL_DETAIL_WORLDFISHING_URL  = "http://www.worldfishingtoday.com/fishingboatinfo/default.asp?soeg={query}&mode=soeg";
var CALLSIGN_VESSEL_DETAIL_WCPFC_URL		 = "http://intra.wcpfc.int/_layouts/searchresults.aspx?k={query}&u=http%3A%2F%2Fintra%2Ewcpfc%2Eint%2FLists%2FVessels";

var EU_CFR_FISHSUBSIDY_URL = "http://fishsubsidy.org/{query}";
var EU_CFR_ESP_URL		   = "http://www.magrama.gob.es/gl/pesca/temas/la-pesca-en-espana/censo-de-la-flota-pesquera/consulta.asp?cod={query}&cd_matr=Consultar";

var FIGIS_VESSEL_TYPE_DETAIL_URL = "http://www.fao.org/fishery/vesseltype/{query}/en";
var FIGIS_GEAR_TYPE_DETAIL_URL = "http://www.fao.org/fishery/geartype/{query}/en";
var FIGIS_PSM_DETAIL_URL = "http://www.fao.org/fi/website/FISearchAction.do?dslist[0]=staticxml.psm&refxml=false&lixsl=webapps/figis/shared/xsl/search_result.xsl&kv[1]={query}&loadMenu=PSM";
var FIGIS_SPECIES_BY_3A_URL = "http://www.fao.org/fishery/species/search/3alpha/{query}/en";
var FIGIS_SPECIES_SEARCH_URL = "http://www.fao.org/fi/website/FISearchAction.do?dslist[0]=species&refxml=true&loadMenu=FishFinder&lixsl=webapps/figis/shared/xsl/search_result.xsl&kv[0]={query}";

var REG_NO_VESSEL_DETAIL_CAN_URL = "http://wwwapps.tc.gc.ca/Saf-Sec-Sur/4/vrqs-srib/d.aspx?lang=e&shipid={query}";
var REG_NO_VESSEL_DETAIL_USA_URL = "http://www.st.nmfs.noaa.gov/pls/webpls/cgv_pkg.vessel_id_list?vessel_id_in={query}";
var REG_NO_VESSEL_DETAIL_ESP_URL = "http://www.magrama.gob.es/gl/pesca/temas/la-pesca-en-espana/censo-de-la-flota-pesquera/consulta.asp";

function IOTCNumberPreProcessor(data) {
	return data.alternateIdentifier;
};

function CCSBTNumberPreProcessor(data) {
	return data.alternateIdentifier;
};

function WCPFCNumberPreProcessor(data) {
	return $$.isSet(data.alternateIdentifier) ? data.alternateIdentifier : data.identifier;
};

function HSVAROldIDPreProcessor(data) {
	return data.alternateIdentifier;
};

function HSVARIDPreProcessor(data) {
	return data.vesselUid;
};

function CLAVTUVIPreProcessor(data) {
	var TUVI = data.identifier;
	
	return $$.isSet(TUVI) ? parseInt(TUVI.replace(/TUVI/g, ""), 10) : null;
};

function NCLAVTUVIPreProcessor(data) {
	var TUVI = data.identifier;
	
	return $$.isSet(TUVI) ? TUVI.replace(/TUVI/g, "") : null;
};

function EUFishSubsidyPreprocessor(data) {
	var euCFR = data.identifier;

	var iso3 = $$.isSet(euCFR) && euCFR.length > 3 ? euCFR.substring(0, 3) : null;
	
	if(iso3 != null) {
		if(iso3 === "ROM") //Necessary to handle EU CFR having 'old' ISO 3 code for Romania
			iso3 = "ROU";
		
		var country = $$.Metadata.getCountryByIso3Code(iso3);
		
		if($$.isSet(country)) {
			var iso2 = country.iso2Code;
		
			return $$.isSet(iso2) ? iso2.toUpperCase() + "/vessel/" + euCFR : null;
		}
	}
	
	$$.Log.warn("Unable to determine ISO2 code for EU CFR " + data.identifier);
	
	return null;
}; 

function EUESPPreprocessor(data) {
	var euCFR = data.identifier;
	
	var iso3 = $$.isSet(euCFR) && euCFR.length > 3 ? euCFR.substring(0, 3) : null;
	
	if(iso3 != null) {
		if(iso3 === "ESP") {
			return euCFR.substring(6);
		}
	}
	
	return null;
};

function IRCSPreProcessor(data) {
	var IRCS = data.callsignId;
	
	return $$.isSet(IRCS) ? IRCS.replace(/[^a-zA-Z0-9]/g, "").toUpperCase() : null;
};

function MMSIPreProcessor(data) {
	var MMSI = data.mmsi;
	
	return $$.isSet(MMSI) ? MMSI.replace(/[^0-9]/g, "") : null;
};

function marineTrafficMMSIPostProcessor(query) {
	var now = new Date();
	var year = now.getFullYear();
	var month = now.getMonth() + 1;
	var day = now.getDate();
	
	query += "&olddate="+ month + "/" + day + "/" + year;
	
	return query;
};

function regNo_ESP_Preprocessor(data) {
	if(data == null || !$$.isSet(data.registrationNumber) || data.registrationNumber.indexOf("-") < 0)
		return null;
	
	var pLISTA = "^(\\d-)?";
	var pMATRICULA = "(\\d?[a-zA-Z0-9]+-?\\d)";
	var pSEPARATOR = "-?";
	var pFOLIO = "(\\d+-?\\d+)$";
	
	var pattern = new RegExp(pLISTA + pMATRICULA + pSEPARATOR + pFOLIO);
	
	if(pattern.test(data.registrationNumber)) {
		var parts = pattern.exec(data.registrationNumber);
		
		return {
			lista: parts[1],
			matricula: parts[2],
			folio: parts[3]
		};
	} else {
		return null;
	}
};

function regNo_ESP_LinkBuilder(data, preprocessedData, URL, tooltip, target) {
	var formID = "_REG_NO_ESP_" + ( data == null || data == "" ? "" + new Date().getTime() : data.replace(/\b/g, "_"));
	
	//Link can be effectively followed only if it POSTs data to the data owner server.
	//Hence, we need to simulate sending a form in this messy way...
	var form = $("<form class='hidden'/>");
	form.attr("id", formID);
	form.attr("name", formID);
	form.attr("method", "POST");
	form.attr("action", URL);
	form.attr("target", target);
	
	form.append("<input name='mat' type='text' value='" + preprocessedData.matricula + "'/>");
	form.append("<input name='fol' type='text' value='" + preprocessedData.folio + "'/>");
	form.append("<input name='bt_matr' type='button' value='Consultar'/>");

	$("body").append(form);
	
	var link = buildPseudoExternalLink();//buildExternalLink("#", tooltip, null);
	link.click(function() {
		form.submit();
		return false;
	});
	
	return link;
};

var EXTERNAL_SEARCH_WITHOUT_ID_SOURCES = [ "ISSF", "SICA", "CTMFM", "SPRFMO" ];

var EXTERNAL_SEARCH_BY_ID_MAP = {
	ID: {
		TUVI: {
			CLAV: {
				link: "CLAV",
				description: "Search vessel details by CLAV TUVI number on the legacy CLAV portal",
				URL: CLAV_VESSEL_DETAIL_URL,
				preprocessor: CLAVTUVIPreProcessor
			},
			NCLAV: {
				link: "nCLAV",
				description: "Search vessel details by CLAV TUVI number on the new CLAV portal",
				URL: NCLAV_VESSEL_DETAIL_URL,
				preprocessor: NCLAVTUVIPreProcessor
			}
		},
		EU_CFR: {
			DEFAULT: {
				link: "EU fleet registry",
				description: "Display vessel details by EU CFR on 'ec.europa.eu/fisheries/fleet'",
				URL: EU_VESSEL_DETAIL_URL
			},
			FISH_SUBSIDY: {
				link: "Fish Subsidy",
				description: "Display vessel details and granted subsidies by EU CFR on 'fishsubsidy.org'",
				URL: EU_CFR_FISHSUBSIDY_URL,
				preprocessor: EUFishSubsidyPreprocessor
			},
			ESP: {
				link: "Spanish Ministry of Agriculture",
				description: "Display vessel details by spanish EU CFR  on 'magrama.gob.es'",
				URL: EU_CFR_ESP_URL,
				preprocessor: EUESPPreprocessor
			}
		},
		HSVAR_ID: {
			DEFAULT: {
				link: "FAO / HSVAR",
				description: "Display vessel details by HSVAR identifier on 'fao.org/fi/hsvar'",
				URL: HSVAR_VESSEL_DETAIL_URL,
				preprocessor: HSVARIDPreProcessor
			}
		},
		CCSBT_ID: {
			DEFAULT: {
				link: "CCSBT registry",
				description: "Display vessel details by CCSBT identifier on 'ccsbt.org'",
				URL: CCSBT_VESSEL_DETAIL_URL,
				preprocessor: CCSBTNumberPreProcessor
			}
		},
		IATTC_ID: {
			DEFAULT: {
				link: "IATTC registry",
				description: "Display vessel details by IATTC identifier on 'iattc.org'",
				URL: IATTC_VESSEL_DETAIL_URL
			}
		},
		ICCAT_ID: {
			DEFAULT: {
				link: "ICCAT registry",
				description: "Display vessel details by ICCAT identifier on 'iccat.int'",
				URL: ICCAT_VESSEL_DETAIL_URL
			}
		},
		IOTC_ID: {
			DEFAULT: {
				link: "IOTC registry",
				description: "Display vessel details by IOTC identifier on 'iotc.org'",
				URL: IOTC_VESSEL_DETAIL_URL,
				preprocessor: IOTCNumberPreProcessor
			}
		},
		WCPFC_ID: {
			DEFAULT: {
				link: "WCPFC registry",
				description: "Display vessel details by WCPFC identifier on 'wcpfc.int' (requires login - see http://www.wcpfc.int/record-fishing-vessel-database)",
				URL: WCPFC_VESSEL_DETAIL_URL,
				preprocessor: WCPFCNumberPreProcessor
			}
		},
		ISSF_ID: {
			DEFAULT: {
				link: "ISSF IMO database",
				description: "Display vessel details as reported by 'iss-foundation.org'",
				URL: ISSF_VESSEL_DETAIL_URL
			}
		},
		SICA_ID: {
			DEFAULT: {
				link: "SICA / OSPESCA WSIRPAC database",
				description: "Display vessel details as reported by 'sica.int'",
				URL: SICA_VESSEL_DETAIL_URL
			}
		},
		SPRFMO_ID: {
			DEFAULT: {
				link: "SPRFMO database",
				description: "Display vessel details as reported by 'sprfmo.org'",
				URL: SPRFMO_VESSEL_DETAIL_URL
			}
		},
		FFA_ID: {
			DEFAULT: {
				link: "FFA Vessel Register Reports",
				description: "Display vessel details as reported by 'ffa.int'",
				URL: FFA_VESSEL_DETAIL_URL
			}
		},
		CCAMLR_ID : {
			DEFAULT: { 
				link: "CCAMLR Licensed Vessels database",
				description: "Display vessel details as reported by 'ccamlr.org'",
				URL: CCAMLR_VESSEL_DETAIL_URL
			}
		},
		IMO: {
			WIKIMEDIA: {
				link: "wikimedia",
				description: "Search vessel details by IMO number on 'commons.wikimedia.org'",
				URL: IMO_VESSEL_DETAIL_WIKIMEDIA_URL
			},
			MARINE_TRAFFIC: {
				link: "marinetraffic",
				description: "Search vessel details by IMO number on 'marinetraffic.com'",
				URL: IMO_VESSEL_DETAIL_MARINE_TRAFFIC_URL
			},
			SHIP_SPOTTING: {
				link: "shipspotting",
				description: "Search vessel details by IMO number on 'shipspotting.com'",
				URL: IMO_VESSEL_DETAIL_SHIP_SPOTTING_URL
			},
			SHIPTRACKING: {
				link: "shiptracking",
				description: "Search vessel details by IMO number on 'shiptracking.eu'",
				URL: IMO_VESSEL_DETAIL_SHIPTRACKING_URL
			},
			MARITIME_CONNECTOR: {
				link: "maritime-connector",
				description: "Search vessel details by IMO number on 'maritime-connector.com'",
				URL: IMO_VESSEL_DETAIL_MARITIME_CON_URL
			},
			E_SHIPS: {
				link: "world-ships",
				description: "Search vessel details by IMO number on 'world-ships.com'",
				URL: IMO_VESSEL_DETAIL_WORLD_SHIPS_URL
			},
			EQUASIS: {
				link: "equasis",
				description: "Search vessel details by IMO number on 'equasis.org' (requires registration)",
				URL: IMO_VESSEL_DETAIL_EQUASIS_URL
			},
			IMO_NUMBER: {
				link: "imo-number",
				description: "Search vessel details by IMO number on 'imonumber.com'",
				URL: IMO_VESSEL_DETAIL_IMO_NUMBER_URL
			},
			SHIP_LIST: {
				link: "ship-list",
				description: "Search vessel details by IMO number on 'shiplist.vitron.org'",
				URL: IMO_VESSEL_DETAIL_SHIPLIST_URL
			},
			VT_EXPLORER: {
				link: "vt-explorer",
				description: "Search vessel details by IMO number on 'vessels.vtexplorer.com'",
				URL: IMO_VESSEL_DETAIL_VT_EXPLORER_URL
			},
			SHIPPING_EXPLORER: {
				link: "shipping-explorer",
				description: "Search vessel details by IMO number on 'shippingexplorer.net'",
				URL: IMO_VESSEL_DETAIL_SHIPPING_EXPL_URL
			}
		}
	},
	MMSI: {
		ITU: {
			link: "ITU",
			description: "Search vessel details by MMSI on ITU's web site",
			URL: MMSI_VESSEL_DETAIL_ITU_URL,
			preprocessor: MMSIPreProcessor
		},
		MARINE_TRAFFIC: {
			link: "marinetraffic.com",
			description: "Search vessel details by MMSI on 'marinetraffic.com'",
			URL: MMSI_VESSEL_DETAIL_MARINE_TRAFFIC_URL,
			preprocessor: MMSIPreProcessor
		},
		MARINE_TRAFFIC_TRACK: {
			description: "Track vessel position by MMSI on 'marinetraffic.com'",
			link: "marinetraffic.com (track)",
			URL: MMSI_TRACK_VESSEL_MARINE_TRAFFIC_URL,
			preprocessor: MMSIPreProcessor,
			postprocessor: marineTrafficMMSIPostProcessor
		},
		SHIPTRACKING: {
			link: "shiptracking.eu",
			description: "Search vessel details by MMSI on 'shiptracking.eu'",
			URL: MMSI_VESSEL_DETAIL_SHIPTRACKING_URL,
			preprocessor: MMSIPreProcessor
		},
		SHIP_SPOTTING: {
			link: "shipspotting.com",
			description: "Search vessel details by MMSI on 'shipspotting.com'",
			URL: MMSI_VESSEL_DETAIL_SHIP_SPOTTING_URL,
			preprocessor: MMSIPreProcessor
		},
		VT_EXPLORER: {
			link: "vt-explorer",
			description: "Search vessel details by MMSI on 'vessels.vtexplorer.com'",
			URL: MMSI_VESSEL_DETAIL_VT_EXPLORER_URL,
			preprocessor: MMSIPreProcessor
		},
		SHIPPING_EXPLORER: {
			link: "shipping-explorer",
			description: "Search vessel details by MMSI on 'shippingexplorer.net'",
			URL: MMSI_VESSEL_DETAIL_SHIPPING_EXPL_URL,
			preprocessor: MMSIPreProcessor
		}
	},
	IRCS: {
		ITU: {
			link: "ITU",
			description: "Search vessel details by IRCS on ITU's web site",
			URL: CALLSIGN_VESSEL_DETAIL_ITU_URL,
			preprocessor: IRCSPreProcessor
		},
		SHIP_SPOTTING: {
			link: "shipspotting.com",
			description: "Search vessel details by IRCS on 'shipspotting.com'",
			URL: CALLSIGN_VESSEL_DETAIL_SHIP_SPOTTING_URL,
			preprocessor: IRCSPreProcessor
		},
		SHIPPING_EXPLORER: {
			link: "shipping-explorer",
			description: "Search vessel details by IRCS on 'shippingexplorer.net'",
			URL: CALLSIGN_VESSEL_DETAIL_SHIPPING_EXPL_URL,
			preprocessor: IRCSPreProcessor
		},
		WORLD_FISHING: {
			link: "world-fishing-today",
			description: "Search vessel details by IRCS on 'worldfishingtoday.com'",
			URL: CALLSIGN_VESSEL_DETAIL_WORLDFISHING_URL,
			preprocessor: IRCSPreProcessor
		}
	},
	REG_NO: {
		CAN: {
			link: "tc.gc.ca",
			description: "External link to the Transport Canada Vessel Registration Query System (tc.gc.ca) for ships with this registration number",
			URL: REG_NO_VESSEL_DETAIL_CAN_URL
		},
		USA: {
			link: "noaa.gov",
			description: "External link to the USA Coast Guard Vessel Documentation search services (noaa.org) for ships with this registration number",
			URL: REG_NO_VESSEL_DETAIL_USA_URL
		},
		ESP: {
			link: "magrama.gob.es",
			description: "External link to the Ministerio de Agricultura, Alimentación y Medio Ambiente search services (magrama.gob.es) for ships with this registration number",
			URL: REG_NO_VESSEL_DETAIL_ESP_URL,
			preprocessor: regNo_ESP_Preprocessor,
			linkBuilder: regNo_ESP_LinkBuilder
		}
	}
};

function showExternalDetails(URL, id) {
	window.open(URL.replace("\{query\}", id), "_blank");
	
	return false;
};

function linkToExternalDetailsByID(identifier) {
	var service = EXTERNAL_SEARCH_BY_ID_MAP.ID[identifier.typeId];
	
	if(!isSet(service))
		return identifier.identifier;
	
	var link = $("<a class=\"textLink\" href=\"#\">" + id + "</a>");
	link.click(function() {
		var currentService = service[0];
		
		var ID = identifier.identifier;
		
		if(isSet(currentService.preprocessor))
			ID = currentService.preprocessor(identifier);
		
		showExternalDetails(service[0].URL, ID);
	});
	
	return link;
};