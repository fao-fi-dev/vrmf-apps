$$.Dep.require("vrmf.framework.externalReferences.postprocessors", { "vrmf.framework.externalReferences": { identifier: "$$.vrmf.ExternalReferences" } }, function() {
	$$.vrmf.ExternalReferences.POSTPROCESSORS = {
		MMSI: {
			MARINE_TRAFFIC: function (query) { 
				var now = new Date();
				var year = now.getFullYear();
				var month = now.getMonth() + 1;
				var day = now.getDate();
				
				query += "&olddate="+ month + "/" + day + "/" + year;
				
				return query;
			},
			MARINE_TRAFFIC_TRACK: function (data) { 
				return $$.vrmf.ExternalReferences.POSTPROCESSORS.MMSI.MARINE_TRAFFIC(data);
			}
		}
	};
});