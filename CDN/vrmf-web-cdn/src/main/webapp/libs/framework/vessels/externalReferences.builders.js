$$.Dep.require("vrmf.framework.externalReferences.builders", { "vrmf.framework.externalReferences": { identifier: "$$.vrmf.ExternalReferences" } }, function() {
	$$.vrmf.ExternalReferences.BUILDERS = {
		ID: {
			EU: {
				EU: {
				},
				FISHSUBSIDY: { 
				},
				ESP: {
				}
			},
			CCAMLR: {
			},
			CCSBT: { 
			},
			FFA: {
			},
			HSVAR: {
			},
			IATTC: { 
			},
			ICCAT: { 
			},
			IOTC: { 
			},
			ISSF: { 
			},
			SICA: { 
			},
			SPRFMO: { 
			},
			WCPFC: {
			},
			ADFG: {
				
			},
			TCGC: {
				
			},
			TUVI: {
				CLAV: {
				},
				NCLAV: {
				}
			},
			IMO: {
				WIKIMEDIA: {
				},
				MARINE_TRAFFIC: {
				},
				SHIP_SPOTTING: {
				},
				SHIPTRACKING: {
				},
				MARITIME_CONNECTOR: {
				},
				WORLD_SHIPS: {
				},
				EQUASIS: {
				},
				IMO_NUMBER: {
				},
				SHIP_LIST: {
				},
				VT_EXPLORER: {
				},
				SHIPPING_EXPLORER: {
				}
			}
		},
		IRCS: { 
			ITU: {
			},
			SHIP_SPOTTING: {
			},
			SHIPPING_EXPLORER: {
			},
			WORLD_FISHING: {
			}
		},
		MMSI: {
			ITU: {
			},
			MARINE_TRAFFIC: {
			},
			MARINE_TRAFFIC_TRACK: {
			},
			SHIPTRACKING: {
			},
			SHIP_SPOTTING: {
			},
			VT_EXPLORER: {
			},
			SHIPPING_EXPLORER: {
			}
		},
		REG_NO: {
			CAN: {
			},
			USA: {
			},
			ESP: {
			}
		}
	};
});