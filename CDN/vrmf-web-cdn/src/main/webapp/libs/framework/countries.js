//Countries Manager

function CountriesManager() {
	this.COUNTRY_GROUPS = new Object();
	
//	this.initializeDefaultGroups();
};

CountriesManager.prototype.initializeGroups = function(groups) {
	this.COUNTRY_GROUPS = {};
	
	for(var g=0; g<groups.length; g++)
		this.COUNTRY_GROUPS[groups[g].id] = new CountryGroup(groups[g].id, groups[g].description, groups[g].countries);
};

CountriesManager.prototype.isGroupAvailable = function(groupID) {
	return $$.isSet(this.COUNTRY_GROUPS[groupID]);
};

CountriesManager.prototype.getGroup = function(groupID) {
	return this.isGroupAvailable(groupID) ? this.COUNTRY_GROUPS[groupID] : null;
};

CountriesManager.prototype.addGroup = function(countryGroup) {
	this.COUNTRY_GROUPS[countryGroup.groupID] = countryGroup;
};

CountriesManager.prototype.countryBelongsTo = function(countryID, groupID) {
	return this.isGroupAvailable(groupID) ? this.COUNTRY_GROUPS[groupID].contains(countryID) : false;
};

CountriesManager.prototype.getGroups = function(countryID) {
	var groups = new Array();
	
	for(var key in this.COUNTRY_GROUPS) {
		if(this.countryBelongsTo(countryID, key)) {
			$$.Log.debug("Country with ID " + countryID + " belongs to group '" + key + "'");
			groups.push(this.COUNTRY_GROUPS[key]);
		}
	}
	
	return groups;
};

CountriesManager.prototype.isMapped = function(countryID) {
	for(var key in this.COUNTRY_GROUPS)
		if(this.countryBelongsTo(countryID, key))
			return true;
	
	return false;
};

CountriesManager.prototype.listGroups = function() {
	var groupIDs = new Array();
	
	for(var key in this.COUNTRY_GROUPS)
		groupIDs.push(key);
	
	return groupIDs;
};

//TO BE REMOVED
//CountriesManager.prototype.initializeDefaultGroups = function() {
//	var EUR = new CountryGroup("EUR", "European Union");
//	var countries = new Array();
//	countries.push(249); //BELGIUM
//	countries.push(27);  //BULGARIA
//	countries.push(50);  //CYPRUS
//	countries.push(54);  //DENMARK
//	countries.push(63);  //ESTONIA
//	countries.push(67);  //FINLAND
//	countries.push(68);  //FRANCE
//	countries.push(79);  //GERMANY
//	countries.push(84);  //GREECE
//	countries.push(104); //IRELAND
//	countries.push(106); //ITALY
//	countries.push(119); //LATVIA
//	countries.push(126); //LITHUANIA
//	countries.push(134); //MALTA
//	countries.push(150); //NETHERLANDS
//	countries.push(173); //POLAND
//	countries.push(174); //PORTUGAL
//	countries.push(183); //ROMANIA
//	countries.push(198); //SLOVENIA
//	countries.push(203); //SPAIN
//	countries.push(209); //SWEDEN
//	countries.push(228); //UNITED KINGDOM
//	
//	EUR.setCountries(countries);
//	
//	this.addGroup(EUR);
//};

//Country Group

function CountryGroup(groupID, groupDescription, countries) {
	this.groupID = $$.isSet(groupID) ? groupID : "NOT_SET";
	this.groupDescription = $$.isSet(groupDescription) ? groupDescription : "Not set";
	this.countries = $$.isSet(countries) && $$.isArray(countries) ? countries : new Array();
};

CountryGroup.prototype.setGroupID = function(groupID) {
	this.groupID = groupID;
};

CountryGroup.prototype.getGroupID = function() {
	return this.groupID;
};

CountryGroup.prototype.setGroupDescription = function(groupDescription) {
	this.groupDescription = groupDescription;
};

CountryGroup.prototype.getGroupDescription = function() {
	return this.groupDescription;
};

CountryGroup.prototype.setCountries = function(countries) {
	this.countries = countries;
};

CountryGroup.prototype.getCountries = function() {
	return this.countries;
};

CountryGroup.prototype.contains = function(countryID) {
	for(var c=0; c<this.countries.length; c++) {
		if(this.countries[c].id == countryID)
			return true;
	}
	
	return false;
};

$$.Dep.require("vrmf.countries", { "vrmf.log": { identifier: "$$.Log" } }, function() {
	$.extend($$, {
		CountryGroup: new CountryGroup(),
		CountriesManager: new CountriesManager()
	});
});