function Preferences() {
	this.showVRMFInternalIDs = false;
	this.defaultReferenceDateSorting = "descending";
};

$$.Dep.require("vrmf.framework.preferences", { "vrmf.framework": { identifier: "$$" } }, function() {
	$.extend($$, {
		Preferences: new Preferences()
	});	
});
