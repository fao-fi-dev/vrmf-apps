function Sources() {
	this.SOURCES = [];
	this.SOURCES_MAP = {};
	
	this._initializeSources();
};

Sources.prototype._initializeSources = function() {
	//PUBLIC SOURCES
	
	var FAO = {
		id: 'FAO',
		URL: 'http://www.fao.org'
	};
	
	var VRMF = {
		id: 'VRMF',
		URL: 'http://www.fao.org/figis/vrmf',
		icon: 'http://www.fao.org/favicon.ico'
	};
	
	var UNGA61 = {
		id: 'UNGA61',
		URL: 'http://www.fao.org/figis/vrmf/dsvar',
		icon: 'http://www.unece.org/fileadmin/templates/icons/favicon.ico'
	};

	var GFCM = {
		id: 'GFCM',
		URL: 'http://www.gfcm.org',
		icon: '../cdn/media/images/decorations/sources/logo/small/GFCM.png'
	};

	var EU = {
		id: 'EU',
		URL: 'http://ec.europa.eu',
		icon: 'http://ec.europa.eu/favicon.ico'
	};
	
	var CLAV = {
		id: 'CLAV',
		URL: 'http://www.tuna-org.org'
	};
	
	var CCSBT = {
		id: 'CCSBT',
		URL: 'http://www.ccsbt.org'
	};
	
	var IATTC = {
		id: 'IATTC',
		URL: 'http://www.iattc.org',
		icon: '../cdn/media/images/decorations/sources/logo/small/IATTC.png'
	};

	var ICCAT = {
		id: 'ICCAT',
		URL: 'http://www.iccat.org',
		icon: '../cdn/media/images/decorations/sources/logo/small/ICCAT.png'
	};
	
	var IOTC = {
		id: 'IOTC', 
		URL: 'http://www.iotc.org'
	};
	
	var WCPFC = {
		id: 'WCPFC',
		URL: 'http://www.wcpfc.int',
		icon: 'http://www.wcpfc.int/sites/default/themes/wcpfc/images/favicon.ico'
	};
	
	var SICA = {
		id: 'SICA',
		URL: 'http://www.sica.int'
	};
	
	var CTMFM = {
		id: 'CTMFM',
		URL: 'http://ctmfm.org',
		icon: 'http://ctmfm.org/wp-content/themes/ctmfm/library/media/images/favicon.ico'
	};
	
	var ISSF = {
		id: 'ISSF',
		URL: 'http://www.iss-foundation.org',
		icon: '../cdn/media/images/decorations/sources/logo/small/ISSF.png'
	};
	
	var SPRFMO = {
		id: 'SPRFMO',
		URL: 'http://www.southpacificrfmo.org/',
		icon: 'http://www.southpacificrfmo.org/favicon.ico'
	};
	
	var FFA = {
		id: 'FFA',
		URL: 'http://www.ffa.int/',
		icon: 'http://www.ffa.int/favicon.ico'
	};
	
	var CCAMLR = {
		id: 'CCAMLR',
		URL: 'http://www.ccamlr.org/',
		icon: 'http://www.ccamlr.org/sites/ccamlr.org/themes/ccamlr/images/favicon.ico'
	};
	
	var TCGC = {
		id: 'TCGC',
		URL: 'http://www.tc.gc.ca/',
		icon: 'http://www.tc.gc.ca/WET-BOEW-V3/dist/theme-gcwu-fegc/images/favicon.ico'
	};
	
	var ADFG = {
		id: 'ADFG',
		URL: 'http://www.adfg.alaska.gov/',
		icon: 'http://www.adfg.alaska.gov/favicon.ico'
	};
	
	var UN = { //UN LOCODEs
		id: 'UN',
		URL: 'http://www.un.org',
		icon: 'http://www.unece.org/favicon.ico'
	};
		
	var WPI = { //World Port Index
		id: 'WPI',
		URL: 'http://msi.nga.mil'
	};

	//PRIVATE OR PROTECTED SOURCES
	
	var HSVAR = {
		id: 'HSVAR',
		URL: 'http://www.fao.org/figis/vrmf/hsvar',
		icon: '../cdn/media/images/decorations/sources/logo/small/HSVAR.png'
	};
	
	var LLOYD = {
		id: 'LLOYD',
		URL: 'http://www.ihs.com',
		//icon: 'http://www.ihs.com/static/images/favicon.ico'
		icon: '../cdn/media/images/decorations/sources/logo/small/LLOYD.png'
	};
	
	this.SOURCES = [
		FAO, 
		VRMF, 
		UNGA61, 
		GFCM, 
		EU, 
		CLAV, 
		CCSBT, 
		IATTC, 
		ICCAT, 
		IOTC, 
		WCPFC, 
		SICA, 
		CTMFM, 
		ISSF, 
		SPRFMO, 
		FFA,
		CCAMLR,
		UN, 
		WPI, 
		HSVAR, 
		LLOYD,
		ADFG,
		TCGC
	];
	
	for(var s=0; s<this.SOURCES.length; s++)
		this.SOURCES_MAP[this.SOURCES[s].id] = this.SOURCES[s];
};

Sources.prototype.getSourceIconByID = function(sourceID) {
	var source = this.SOURCES_MAP[sourceID];
	
	if($$.isSet(source) && $$.isSet(source.URL)) {
		var hasExplicitIcon = $$.isSet(source.icon);
		
		var sourceDomain = source.URL.replace("http\:\/\/", "");
		var iconResource = hasExplicitIcon ? source.icon : "http://www.google.com/s2/favicons?domain=" + sourceDomain;
		
		return "<img src='" + iconResource + "' class='sourceIcon' alt='" + sourceID + "' " /* REMOVED */ /* title='" + sourceID + " default icon'*/ + "/>";
	} else
		return "<img src='empty' class='sourceIcon' alt='" + sourceID + "' " /* REMOVED */ /* title='" + sourceID + " default icon'*/ + "/>";	
};

$$.Dep.require("vrmf.sources", { "vrmf.utils": { identifier: "$$.Utils" } }, function() { 
	$.extend($$, {
		Sources: new Sources()
	});
});