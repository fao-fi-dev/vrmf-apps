function I18n() {
	this.BINDINGS = {};
	this.PLACEHOLDER = "{}";
	this.NULL_SUBSTITUE = "<NULL>";
};

I18n.prototype.initialize = function(bindings) {
	this.BINDINGS = bindings;
	
	return this.BINDINGS;
};

I18n.prototype.extend = function(bindings) {
	this.BINDINGS = $.extend(true, {}, this.BINDINGS, bindings);
	
	return this.BINDINGS;
};

I18n.prototype._getText = function(labelId) {
	if($$.isSet(this.BINDINGS[labelId]))
		return this.BINDINGS[labelId];
	
	return this._missing(labelId);
};

I18n.prototype._missing = function(labelId) {
	return "$$ " + labelId + " $$";
};

/** This is a varargs function. 
 *  It can be invoked by providing a label ID and an optional, 
 *  unlimited set of arguments that will be replaced positionally, 
 *  in the resulting text, by the order in which they appear in
 *  the argument list */
I18n.prototype.getText = function(labelId) {
	var text = this._getText(labelId);

	if(text == null || "" === text)
		return text;

	var numPlaceholders = 0;
	var index = 0;
	
	var actualParams = [];
	
	if(arguments.length > 1)
		for(var a=1; a < arguments.length; a++) {
			actualParams.push(arguments[a]);
		}
	
	var numParams = actualParams.length;
	
	while((index = text.indexOf(this.PLACEHOLDER, index)) != -1) {
		numPlaceholders++;
		index += this.PLACEHOLDER.length;
	}

	if(numPlaceholders > numParams)
		$$.Log.warn("Label [ " + labelId + " ] : " + numPlaceholders + " placeholders vs. " + numParams + " provided parameters: some placeholder will remain as it is...");
	else if(numPlaceholders < numParams)
		$$.Log.warn("Label [ " + labelId + " ] : " + numPlaceholders + " placeholders vs. " + numParams + " provided parameters: some parameter will not be used...");

	index = 0;

	if($$.isSet(actualParams)) {
		for(var p=0; p<actualParams.length; p++) {
			index = text.indexOf(this.PLACEHOLDER);

			if(index >= 0)
				text =
					text.substring(0, index) +
						( actualParams[p] == null ? this.NULL_SUBSTITUTE : actualParams[p] ) +
					text.substring(index + this.PLACEHOLDER.length);			
		}
	}

	return text;	
};

$$.Dep.require("vrmf.i18n", { "vrmf.log": { identifier: "$$.Log" } }, function() {
	$.extend($$, {
		I18n: new I18n()
	});
});