function FormsManager() {
	$$.Log.debug("Initializing FormsManager...");
};

FormsManager.prototype.serialize = function(form, alsoDisabled) {
	var manager = this;
	var serialized = {};
	
	var id;
	var name;
	var value;
	
	$(":input" + ($$.isSet(alsoDisabled) && alsoDisabled ? "" : ":enabled") + ", :input[type='hidden']", form).each(function() {
		var isText;
		var isRadio;
		var isCheckbox;
		var isSelect;
		var isMultiselect;
		var sendAsArray;
		
		isText = isRadio = isCheckbox = isSelect = isMultiselect = false;
		
		var $input = $(this);
				
		if(!$input.hasClass("inputModifier") &&
		   !$input.hasClass("dontSerialize")) {
			value = null;
			id = $input.attr("id");
			name = $input.attr("name");
			sendAsArray = $input.hasClass("sendAsArray");
			
			isText = $input.is("input[type='text']") ||
			   		 $input.is("input[type='hidden']") ||
			   		 $input.is("input[type='password']");
			
			isRadio = $input.is("input[type='radio']");
			isCheckbox = $input.is("input[type='checkbox']");
			isMultiselect = $input.is("select") && ( $input.hasClass("multiselect") || $input.attr("multiple") );
			isSelect = !isMultiselect && $input.is("select");
			
			if(isText)
				value = manager._getTextInputValue($input);
			else if(isCheckbox)
				value = $$.isSet(name) ? manager._getCheckboxValues($input) : manager._getCheckboxValue($input);
			else if(isRadio)
				value = manager._getRadioValue($input);
			else if(isSelect)
				value = manager._getSelectValue($input);
			else if(isMultiselect)
				value = manager._getMultiselectValue($input);
					
			if(sendAsArray) {
				value = [ value ];
			}
			
			if(value != null) {
				if($$.isSet(name)) {
					manager._augment(serialized, name, value);
				} else if($$.isSet(id)) {				
					manager._augment(serialized, id, value); 
				} else {
					$$.Log.error("Cannot serialize a form input without name / id");
				}
			}
		}
	});
	
	return serialized;
};

FormsManager.prototype._augment = function(map, key, value) {
	if($$.isSet(value)) {
		var entry = map[key];
		
		if($$.isSet(entry)) {
			if($.isArray(entry)) {
				if($.isArray(value)) {
					entry = entry.concat(value);
				} else {
					entry.push(value);
				}
			} else {
				entry = value;
			}
		} else {
			entry = value;
		}
		
		map[key] = entry;
	}
		
	return map;
};
 
FormsManager.prototype._encodeValue = function(input) {
	var trimmed = $$.rawTrim($(input).val());
	
	if(trimmed != null && $$.isEnabled($(input)))
		if($(input).hasClass("encodeOnSend")) {
			trimmed = encodeURIComponent(trimmed);
			trimmed = trimmed.replace("+", "%2B");
			trimmed = trimmed.replace("/", "%2F");
		}
	
	return trimmed;
};

FormsManager.prototype._getTextInputValue = function(input) {
	return this._encodeValue(input);
};

FormsManager.prototype._getSelectValue = function(input) {
	return this._encodeValue($("option:selected", $(input)));
};

FormsManager.prototype._getMultiselectValue = function(input) {
	var manager = this;
	var selected = new $$.Set();
	
	var trimmed;
	$("option:selected", $(input)).each(function() {
		trimmed = manager._encodeValue($(this));
		
		if(trimmed != null)
			selected.add(trimmed);
	});
	
	return selected.asArray();
};

FormsManager.prototype._getRadioValue = function(input) {
	if($$.isChecked($(input))) {
		var value = this._encodeValue(input);
		
		if($$.isSet(value))
			return value;
		
		return true;
	}
	
	return null;
};

FormsManager.prototype._getCheckboxValue = function(input) {	
	return this._getRadioValue(input);
};

FormsManager.prototype._getCheckboxValues = function(input) {	
	var value = null;

	if($$.isChecked($(input))) {
		value = this._encodeValue(input);
		
		if($$.isSet(value))
			value = new Array(value);
		else 
			value = new Array($$.isChecked(input));
	} else
		value = new Array(false);

	return value;
};

$$.Dep.require("vrmf.forms", { "vrmf.log": { identifier: "$$.Log" } }, function() {
	$.extend(true, $$, { FormsManager: new FormsManager() });
});