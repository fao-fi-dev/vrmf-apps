$.widget("ech.multiselect", $.extend({}, $.ech.multiselect.prototype, {
	refresh: function(init) {
	      var el = this.element;
	      var o = this.options;
	      var menu = this.menu;
	      var checkboxContainer = this.checkboxContainer;
	      var optgroups = [];
	      var html = "";
	      var id = el.attr('id') || multiselectID++; // unique ID for the label & option tags

	      // build items
	      el.find('option').each(function(i) {
	        var $this = $(this);
	        var parent = this.parentNode;
	        var description = this.innerHTML;
	        var title = this.title;
	        var value = this.value;
	        var inputID = 'ui-multiselect-' + (this.id || id + '-option-' + i);
	        var isDisabled = this.disabled;
	        var isSelected = this.selected;
	        var labelClasses = [ 'ui-corner-all' ];
	        var liClasses = (isDisabled ? 'ui-multiselect-disabled ' : ' ') + this.className;
	        var optLabel;

	        // is this an optgroup?
	        if(parent.tagName === 'OPTGROUP') {
	          optLabel = parent.getAttribute('label');

	          // has this optgroup been added already?
	          if($.inArray(optLabel, optgroups) === -1) {
	            html += '<li class="ui-multiselect-optgroup-label ' + parent.className + '"><a href="#">' + optLabel + '</a></li>';
	            optgroups.push(optLabel);
	          }
	        }

	        if(isDisabled) {
	          labelClasses.push('ui-state-disabled');
	        }

	        // browsers automatically select the first option
	        // by default with single selects
	        if(isSelected && !o.multiple) {
	          labelClasses.push('ui-state-active');
	        }

	        html += '<li class="' + liClasses + '">';

	        // create the label

	        // create the label
	        html += '<label for="' + inputID + '"' + ( $$.isSet(title) ? ' title="' + title + '"' : '' ) + ' class="' + labelClasses.join(' ') + '">';
	        html += '<input id="' + inputID + '" name="multiselect_' + id + '" type="' + (o.multiple ? "checkbox" : "radio") + '" value="' + value + '"' + ( $$.isSet(title) ? ' title="' + title + '"' : '' ) + '"';
        
	        // pre-selected?
	        if(isSelected) {
	          html += ' checked="checked"';
	          html += ' aria-selected="true"';
	        }

	        // disabled?
	        if(isDisabled) {
	          html += ' disabled="disabled"';
	          html += ' aria-disabled="true"';
	        }

	        // add the title and close everything off
	        html += ' /><span>' + description + '</span></label></li>';
	      });

	      // insert into the DOM
	      checkboxContainer.html(html);

	      // cache some moar useful elements
	      this.labels = menu.find('label');
	      this.inputs = this.labels.children('input');

	      // set widths
	      this._setButtonWidth();
	      this._setMenuWidth();

	      // remember default value
	      this.button[0].defaultValue = this.update();

	      // broadcast refresh event; useful for widgets
	      if(!init) {
	        this._trigger('refresh');
	      }
	    },
	    _setButtonValue: function(value) {
	    	if(typeof value === "string")
	    		this.buttonlabel.text(value);
	    	else
	    		this.buttonlabel.empty().append(value);
	    }
	})
);