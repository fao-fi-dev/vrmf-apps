/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder-business)
 */
package org.fao.fi.vrmf.web.apps.finder.business.dao.generic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Named;

import org.fao.fi.vrmf.business.core.dao.ibatis.impl.AbstractSetterInjectionIBATISDAO;
import org.fao.fi.vrmf.business.core.utilities.orm.ibatis2.annotations.IBATISNamespace;
import org.fao.fi.vrmf.common.models.generated.SCountries;
import org.fao.fi.vrmf.common.models.generated.SGearTypes;
import org.fao.fi.vrmf.common.models.generated.SVesselTypes;
import org.fao.fi.vrmf.web.apps.finder.business.dao.MetadataDAO;
import org.fao.fi.vrmf.web.apps.finder.common.models.extended.UIDBySourceIdentifier;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 20 Jun 2011   faoadmin     Creation.
 *
 * @version 1.0
 * @since 20 Jun 2011
 */
@Named("dao.finder.metadata")
@IBATISNamespace(defaultNamespacePrefix="finderMeta")
public class MetadataDAOImpl extends AbstractSetterInjectionIBATISDAO implements MetadataDAO {
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.web.apps.finder.business.dao.MetadataDAO#getCurrentReportingCountries()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<SCountries> getReportingCountries(boolean currentOnly) throws Throwable {
		return (List<SCountries>)this.sqlMapClient.queryForList(this.getIBATISNamespace() + "." + "getAll" + ( currentOnly ? "Current" : "" ) + "ReportingCountries");
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.web.apps.finder.business.dao.MetadataDAO#getAllCountries(boolean)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<SCountries> getFlags(boolean currentOnly) throws Throwable {
		return (List<SCountries>)this.sqlMapClient.queryForList(this.getIBATISNamespace() + "." + "getAll" + ( currentOnly ? "Current" : "" ) + "Countries");
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.web.apps.finder.business.dao.MetadataDAO#getAllGearTypes(boolean)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<SGearTypes> getGearTypes(boolean currentOnly) throws Throwable {
		return (List<SGearTypes>)this.sqlMapClient.queryForList(this.getIBATISNamespace() + "." + "getAll" + ( currentOnly ? "Current" : "" ) + "Gears");
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.web.apps.finder.business.dao.MetadataDAO#getAllVesselTypes(boolean)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<SVesselTypes> getVesselTypes(boolean currentOnly) throws Throwable {
		return (List<SVesselTypes>)this.sqlMapClient.queryForList(this.getIBATISNamespace() + "." + "getAll" + ( currentOnly ? "Current" : "" ) + "Types");
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.web.apps.finder.business.dao.MetadataDAO#getIDsForUID(java.lang.Integer)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> getIDsForUID(Integer UID, String[] sourceSystems, String table) throws Throwable {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("UID", UID);
		params.put("sourceSystems", sourceSystems);
		params.put("vesselsTable", table);
		return (List<Integer>)this.sqlMapClient.queryForList(this.getIBATISNamespace() + "." + "getIDsForUID", params);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.web.apps.finder.business.dao.MetadataDAO#getTUVIBySourceIdentifier(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<UIDBySourceIdentifier> getUIDBySourceIdentifier(String identifierTypeId, String identifier, String sourceSystem, String vesselsTable) throws Throwable {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("vesselsTable", vesselsTable);
		params.put("typeId", identifierTypeId);
		params.put("identifier", identifier == null ? null : identifier.replaceAll("\\*", "%"));
		params.put("sourceSystem", sourceSystem);
		params.put("identifierLike", identifier != null && ( identifier.replaceAll("\\*", "%").indexOf("%") >=0 || identifier.indexOf("_") >= 0 ));
		
		return (List<UIDBySourceIdentifier>)this.sqlMapClient.queryForList(this.getIBATISNamespace() + "." + "getUIDBySourceID", params);
	}
}