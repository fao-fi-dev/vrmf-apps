/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder-business)
 */
package org.fao.fi.vrmf.web.apps.finder.business.dao.generic;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.inject.Named;

import org.fao.fi.vrmf.business.core.dao.ibatis.impl.AbstractSetterInjectionIBATISDAO;
import org.fao.fi.vrmf.business.core.utilities.orm.ibatis2.annotations.IBATISNamespace;
import org.fao.fi.vrmf.common.models.stats.FlagsStatsData;
import org.fao.fi.vrmf.common.models.stats.LengthsStatsData;
import org.fao.fi.vrmf.common.models.stats.TypesStatsData;
import org.fao.fi.vrmf.web.apps.finder.business.dao.StatsDAO;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Aug 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Aug 2012
 *
 */
@Named("dao.finder.stats")
@IBATISNamespace(defaultNamespacePrefix="finderStats")
public class StatsDAOImpl extends AbstractSetterInjectionIBATISDAO implements StatsDAO {
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.web.apps.finder.business.dao.StatsDAO#getStatsByFlag(java.lang.String[])
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<FlagsStatsData> getStatsByFlag(String[] sourceSystems) throws Throwable {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sourceSystems", sourceSystems);
		params.put("includeNoFlags", Boolean.TRUE);
		
		return (List<FlagsStatsData>)this.sqlMapClient.queryForList(this.getQueryIDForCurrentNamespace("statsByFlag"), params);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.web.apps.finder.business.dao.StatsDAO#getStatsByType(java.lang.String, java.lang.Integer[])
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<TypesStatsData> getStatsByType(String[] sourceSystems, Integer[] countryIDs) throws Throwable {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sourceSystems", sourceSystems);
		
		boolean filterFlags = countryIDs != null && countryIDs.length > 0;
		boolean filterNoFlags = false;
		
		if(countryIDs != null && countryIDs.length > 0) {
			Set<Integer> IDs = new TreeSet<Integer>(Arrays.asList(countryIDs));
			
			filterNoFlags = IDs.contains(0);
			IDs.remove(0);
			
			params.put("flags", IDs.toArray(new Integer[IDs.size()]));
		}
		
		params.put("filterFlags", filterFlags);
		params.put("filterNoFlags", filterNoFlags);
		
		return (List<TypesStatsData>)this.sqlMapClient.queryForList(this.getQueryIDForCurrentNamespace("statsByType"), params);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.web.apps.finder.business.dao.StatsDAO#getStatsByLength(java.lang.String, java.lang.Integer[], java.lang.Integer[])
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<LengthsStatsData> getStatsByLength(String[] sourceSystems, Integer[] countryIDs, Integer[] typeIDs) throws Throwable {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("sourceSystems", sourceSystems);
		
		boolean filterFlags = countryIDs != null && countryIDs.length > 0;
		boolean filterNoFlags = false;
		
		boolean filterTypes = typeIDs != null && typeIDs.length > 0;
		boolean filterNoTypes = !filterTypes;
		
		if(countryIDs != null && countryIDs.length > 0) {
			Set<Integer> IDs = new TreeSet<Integer>(Arrays.asList(countryIDs));
			
			filterNoFlags = IDs.contains(0);
			IDs.remove(0);
			
			params.put("flags", IDs.toArray(new Integer[IDs.size()]));
		}
		
		if(typeIDs != null && typeIDs.length > 0) {
			Set<Integer> IDs = new TreeSet<Integer>(Arrays.asList(typeIDs));
			
			filterNoTypes = IDs.contains(0);
			IDs.remove(0);
			
			params.put("types", IDs.toArray(new Integer[IDs.size()]));
		}
		
		params.put("filterFlags", filterFlags);
		params.put("filterNoFlags", filterNoFlags);
		
		params.put("filterTypes", filterTypes);
		params.put("filterNoTypes", filterNoTypes);
		
		return (List<LengthsStatsData>)this.sqlMapClient.queryForList(this.getQueryIDForCurrentNamespace("statsByLength"), params);
	}
}