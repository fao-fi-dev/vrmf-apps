/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder-business)
 */
package org.fao.fi.vrmf.web.apps.finder.business.dao;

import java.util.List;

import org.fao.fi.vrmf.common.models.stats.FlagsStatsData;
import org.fao.fi.vrmf.common.models.stats.LengthsStatsData;
import org.fao.fi.vrmf.common.models.stats.TypesStatsData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Aug 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Aug 2012
 */
public interface StatsDAO {
	List<FlagsStatsData> getStatsByFlag(String[] sourceSystems) throws Throwable;
	List<TypesStatsData> getStatsByType(String[] sourceSystems, Integer[] countryIDs) throws Throwable;
	List<LengthsStatsData> getStatsByLength(String[] sourceSystems, Integer[] countryIDs, Integer[] typeIDs) throws Throwable;
}
