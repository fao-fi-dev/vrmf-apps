/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder-business)
 */
package org.fao.fi.vrmf.web.apps.finder.business.dao;

import java.util.List;

import org.fao.fi.vrmf.common.models.extended.ExportedVesselData;
import org.fao.fi.vrmf.web.apps.finder.common.models.extended.TimeframedAuthorizedVessel;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Jun 2011   faoadmin     Creation.
 *
 * @version 1.0
 * @since 24 Jun 2011
 */
public interface ExportVesselsDAO {
	List<ExportedVesselData> oldGetExportedVesselsData(List<Integer> UIDs) throws Throwable;
	List<TimeframedAuthorizedVessel> getExportedVesselsData(List<Integer> UIDs, String[] sourceSystems) throws Throwable;
	List<TimeframedAuthorizedVessel> getExportedVesselsData(List<Integer> UIDs, String[] sourceSystems, String vesselsTable) throws Throwable;
}
