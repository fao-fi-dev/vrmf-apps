/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder-business)
 */
package org.fao.fi.vrmf.web.apps.finder.business.helpers;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.fao.fi.sh.utility.core.AbstractLoggingAwareClient;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.sh.utility.model.extensions.collections.SerializableList;
import org.fao.fi.sh.utility.model.extensions.collections.impl.SerializableArrayList;
import org.fao.fi.vrmf.business.dao.CommonMetadataDAO;
import org.fao.fi.vrmf.common.models.stats.FlagsStatsData;
import org.fao.fi.vrmf.common.models.stats.LengthsStatsData;
import org.fao.fi.vrmf.common.models.stats.StatusReportBySource;
import org.fao.fi.vrmf.common.models.stats.TypesStatsData;
import org.fao.fi.vrmf.web.apps.finder.business.dao.StatsDAO;
import org.springframework.cache.annotation.Cacheable;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 10 Sep 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 10 Sep 2012
 */
@Named
public class LiveStatsRetriever extends AbstractLoggingAwareClient {
	static final private String DEFAULT_METHODS_INVOCATION_CACHE_ID = "vrmf.finder.business.live.stats";

	@Inject private StatsDAO _statsDAO;
	@Inject private CommonMetadataDAO _metadataDAO;

	/**
	 * Class constructor
	 *
	 */
	public LiveStatsRetriever() {
		super();
	}

	/**
	 * @return the 'statsDAO' value
	 */
	public StatsDAO getStatsDAO() {
		return this._statsDAO;
	}

	/**
	 * @param statsDAO the 'statsDAO' value to set
	 */
	public void setStatsDAO(StatsDAO statsDAO) {
		this._statsDAO = statsDAO;
	}

	/**
	 * @return the 'metadataDAO' value
	 */
	public CommonMetadataDAO getMetadataDAO() {
		return this._metadataDAO;
	}

	/**
	 * @param metadataDAO the 'metadataDAO' value to set
	 */
	public void setMetadataDAO(CommonMetadataDAO metadataDAO) {
		this._metadataDAO = metadataDAO;
	}

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + " +
			  												  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#sources) + " +
															  "#table")
	public SerializableList<StatusReportBySource> retrieveSourcesData(String[] sources, String table) throws Throwable {
		return new SerializableArrayList<StatusReportBySource>(this._metadataDAO.getStatusReportBySources(sources, table));
	}

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#sources) + " +
															  "#table")
	public List<FlagsStatsData> retrieveStatsByFlag(String[] sources, String table) throws Throwable {
		this._log.info("Retrieving stats by flag for sources {} and table {}", CollectionsHelper.serializeArray(sources), table);

		return this._statsDAO.getStatsByFlag(sources);
	}

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#sources) + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#countryIDs) + " +
															  "#table")
	public List<TypesStatsData> retrieveStatsByType(String[] sources, Integer[] countryIDs, String table) throws Throwable {
		this._log.info("Retrieving stats by type for sources {}, country IDs {} and table {}", 
						CollectionsHelper.serializeArray(sources), 
						CollectionsHelper.serializeArray(countryIDs),
						table);

		return this._statsDAO.getStatsByType(sources, countryIDs);
	}

	@Cacheable(value=DEFAULT_METHODS_INVOCATION_CACHE_ID, key="#root.method.name + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#sources) + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#countryIDs) + " +
															  "T(org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper).serializeArray(#typeIDs) + " +
															  "#table")
	public List<LengthsStatsData> retrieveStatsByLength(String[] sources, Integer[] countryIDs, Integer[] typeIDs, String table) throws Throwable {
		this._log.info("Retrieving stats by length for sources {}, country IDs {}, type IDs {} and table {}",
						CollectionsHelper.serializeArray(sources),
						CollectionsHelper.serializeArray(countryIDs),
						CollectionsHelper.serializeArray(typeIDs),
						table);

		return this._statsDAO.getStatsByLength(sources, countryIDs, typeIDs);
	}
}