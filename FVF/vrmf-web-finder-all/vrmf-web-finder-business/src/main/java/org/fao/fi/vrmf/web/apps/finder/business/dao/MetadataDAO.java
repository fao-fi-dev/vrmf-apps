/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder-business)
 */
package org.fao.fi.vrmf.web.apps.finder.business.dao;

import java.util.List;

import org.fao.fi.vrmf.common.models.generated.SCountries;
import org.fao.fi.vrmf.common.models.generated.SGearTypes;
import org.fao.fi.vrmf.common.models.generated.SVesselTypes;
import org.fao.fi.vrmf.web.apps.finder.common.models.extended.UIDBySourceIdentifier;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 20 Jun 2011   faoadmin     Creation.
 *
 * @version 1.0
 * @since 20 Jun 2011
 */
public interface MetadataDAO {
	List<SCountries> getReportingCountries(boolean currentOnly) throws Throwable;
	List<SCountries> getFlags(boolean currentOnly) throws Throwable;
	List<SGearTypes> getGearTypes(boolean currentOnly) throws Throwable;
	List<SVesselTypes> getVesselTypes(boolean currentOnly) throws Throwable;
	
	List<Integer> getIDsForUID(Integer TUVI, String[] sourceSystems, String vesselsTable) throws Throwable;
	
	List<UIDBySourceIdentifier> getUIDBySourceIdentifier(String identifierTypeId, String identifier, String sourceSystem, String vesselsTable) throws Throwable;
}
