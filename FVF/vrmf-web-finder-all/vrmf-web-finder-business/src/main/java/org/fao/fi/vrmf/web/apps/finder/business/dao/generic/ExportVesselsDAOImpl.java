/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder-business)
 */
package org.fao.fi.vrmf.web.apps.finder.business.dao.generic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Named;

import org.fao.fi.vrmf.business.core.dao.ibatis.impl.AbstractSetterInjectionIBATISDAO;
import org.fao.fi.vrmf.business.core.utilities.orm.ibatis2.annotations.IBATISNamespace;
import org.fao.fi.vrmf.common.models.extended.ExportedVesselData;
import org.fao.fi.vrmf.web.apps.finder.business.dao.ExportVesselsDAO;
import org.fao.fi.vrmf.web.apps.finder.common.models.extended.TimeframedAuthorizedVessel;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Jun 2011   faoadmin     Creation.
 *
 * @version 1.0
 * @since 24 Jun 2011
 */
@Named("dao.finder.export")
@IBATISNamespace(defaultNamespacePrefix="finderExport")
public class ExportVesselsDAOImpl extends AbstractSetterInjectionIBATISDAO implements ExportVesselsDAO {
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.web.apps.finder.business.dao.ExportVesselsDAO#getExportedVesselsData(java.util.List)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<ExportedVesselData> oldGetExportedVesselsData(List<Integer> UIDs) throws Throwable {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("IDs", UIDs);		
		return (List<ExportedVesselData>)this.sqlMapClient.queryForList(this.getIBATISNamespace() + ".getExportedVesselsData", params);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.web.apps.finder.business.dao.ExportVesselsDAO#getExportedVesselsData(java.util.List)
	 */
	@Override
	public List<TimeframedAuthorizedVessel> getExportedVesselsData(List<Integer> UIDs, String[] sourceSystems) throws Throwable {
		return this.getExportedVesselsData(UIDs, sourceSystems, "CLAV_VESSELS");
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.web.apps.finder.business.dao.ExportVesselsDAO#getExportedVesselsData(java.util.List, java.lang.String[], java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<TimeframedAuthorizedVessel> getExportedVesselsData(List<Integer> UIDs, String[] sourceSystems, String vesselsTable) throws Throwable {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("IDs", UIDs);
		params.put("sourceSystems", sourceSystems);
		params.put("vesselsTable", vesselsTable);
		return (List<TimeframedAuthorizedVessel>)this.sqlMapClient.queryForList(this.getIBATISNamespace() + ".getExportedVesselsData", params);
	}
}