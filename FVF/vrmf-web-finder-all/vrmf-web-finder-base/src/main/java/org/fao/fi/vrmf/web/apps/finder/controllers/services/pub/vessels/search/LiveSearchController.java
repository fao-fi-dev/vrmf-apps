/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder)
 */
package org.fao.fi.vrmf.web.apps.finder.controllers.services.pub.vessels.search;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;

import org.fao.fi.sh.utility.core.helpers.singletons.io.SerializationHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.business.dao.VesselExternalMarkingsAutocompletionDAO;
import org.fao.fi.vrmf.business.dao.VesselIRCSAutocompletionDAO;
import org.fao.fi.vrmf.business.dao.VesselIdentifierAutocompletionDAO;
import org.fao.fi.vrmf.business.dao.VesselNameFuzzyAutocompletionDAO;
import org.fao.fi.vrmf.business.dao.VesselRecordSearchDAO;
import org.fao.fi.vrmf.business.dao.VesselRegNoAutocompletionDAO;
import org.fao.fi.vrmf.business.dao.VesselUIDAutocompletionDAO;
import org.fao.fi.vrmf.business.dao.generated.SCountriesDAO;
import org.fao.fi.vrmf.common.data.exchange.core.emitters.impl.JSONDatasetEmitter;
import org.fao.fi.vrmf.common.data.exchange.core.models.JSONResponse;
import org.fao.fi.vrmf.common.j2ee.RequestAttributesConstants;
import org.fao.fi.vrmf.common.models.generated.SCountries;
import org.fao.fi.vrmf.common.models.generated.SCountriesExample;
import org.fao.fi.vrmf.common.models.search.autocompletion.FuzzySmartNamedSearchResult;
import org.fao.fi.vrmf.common.models.search.autocompletion.typed.ExternalMarkingSearchResult;
import org.fao.fi.vrmf.common.models.search.autocompletion.typed.IdentifierSearchResult;
import org.fao.fi.vrmf.common.models.search.autocompletion.typed.RegistrationNumberSearchResult;
import org.fao.fi.vrmf.common.search.dsl.impl.common.meta.CommonClauses;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.VesselRecordSearchFilter;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.VesselRecordSelector;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.VesselRecordWhere;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses;
import org.fao.fi.vrmf.common.search.dsl.support.models.vessels.record.VesselRecord;
import org.fao.fi.vrmf.common.search.dsl.support.services.response.vessels.VesselRecordSearchResponse;
import org.fao.fi.vrmf.common.web.controllers.AbstractController;
import org.fao.fi.vrmf.web.apps.finder.controllers.services.pub.PublicServicesConstants;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import lombok.Getter;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 7 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 7 May 2012
 */
@Named @Singleton @Controller
@RequestMapping(PublicServicesConstants.VESSELS_SEARCH_SERVICES_PREFIX + "live/*")
public class LiveSearchController extends AbstractSearchControllerBase {
	final protected double COLLISION_FACTOR = 1.5D;
	final protected int DEFAULT_MAX_RESULTS = 30;
	
	@Inject protected @Getter @Setter SCountriesDAO countriesDAO;
	@Inject protected @Getter @Setter VesselRecordSearchDAO searchDAO;
	@Inject protected @Getter @Setter VesselUIDAutocompletionDAO uidAutocompletionDAO;
	@Inject protected @Getter @Setter VesselIdentifierAutocompletionDAO identifierAutocompletionDAO;
	@Inject protected @Getter @Setter VesselNameFuzzyAutocompletionDAO nameAutocompletionDAO;
	@Inject protected @Getter @Setter VesselExternalMarkingsAutocompletionDAO extMarkAutocompletionDAO;
	@Inject protected @Getter @Setter VesselIRCSAutocompletionDAO IRCSAutocompletionDAO;
	@Inject protected @Getter @Setter VesselRegNoAutocompletionDAO regNoAutocompletionDAO;
	
	protected VesselRecordSearchFilter getCommonFilter(HttpServletRequest request, String sources, String countries, Integer maxEntries, Date atDate, String[] sortingCriteria) throws Throwable {
		String[] sourceSystems = sources == null ? null : sources.split("\\,");
		String[] availableSourceSystems = this.getAvailableSources(request, false);
		String[] effectiveSourceSystems = AbstractController.intersectSourceSystems(sourceSystems, availableSourceSystems);
		
		String lang = StringsHelper.trim((String)request.getAttribute(RequestAttributesConstants.CURRENT_LANGUAGE_REQUEST_ATTRIBUTE));//request.getParameter(SearchServiceConstants.LANGUAGE_PARAMETER));
		
		if(lang == null)
			lang = "en";
		
		if(effectiveSourceSystems == null || effectiveSourceSystems.length == 0) {
			throw new RuntimeException("No valid sources selected (available: "  + CollectionsHelper.serializeArray(availableSourceSystems) + ", requested: " + CollectionsHelper.serializeArray(sourceSystems));
		}
			
		String[] countryISOs = countries == null || "all".equals(countries) ? null : countries.split("\\,");
		
		List<Integer> countryIDs = null;
		
		if(countryISOs != null) {
			SCountriesExample filter = new SCountriesExample();
			filter.createCriteria().andIso2CodeIn(Arrays.asList(countryISOs));
			
			countryIDs = new ListSet<Integer>();
			
			for(SCountries country : this.countriesDAO.selectByExample(filter)) {
				if(country != null)
					countryIDs.add(country.getId());
			}
		}
		
		VesselRecordSearchFilter filter = new VesselRecordSearchFilter();
		VesselRecordSelector columns = filter.columns();
		
		columns.
			include(
				dataCustomizer.getDataColumns(request)
			);
		
		filter.
			SELECT(columns).
			FROM(dataCustomizer.getTargetTable(request)).
			LIMIT(maxEntries).
			OFFSET(0).
			AMONG(availableSourceSystems).
			SOURCED_BY(sourceSystems).
			lang(lang);

		if(atDate != null)
			filter.AT(this.truncate(atDate));
		
		filter.linkToDate().linkToSearch();
		
		if(countryIDs != null && !countryIDs.isEmpty()) {
			filter.WHERE(filter.clauses().and(VesselRecordClauses.FLAG, (Object[])countryIDs.toArray(new Integer[countryIDs.size()])));
		}

		if(sortingCriteria != null && sortingCriteria.length > 0) {
			filter = this.setOrderClauses(filter, sortingCriteria);
			
			//UNNECESSARY?
			filter.ORDER_BY(filter.getSortableColumns());
		}
		
		return filter;
	}

	@RequestMapping(value="{sources}/{historical}/reportedBy/{countries}/uid/{uid}.{format}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> searchByUID(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String countries, @PathVariable String uid, @PathVariable String format, @RequestParam(value="s_c[]", required=false) String[] sortingCriteria) {
		return this.searchByUID(request, sources, historical, countries, DEFAULT_MAX_RESULTS, uid, format, sortingCriteria);
	}
	
	@RequestMapping(value="{sources}/{historical}/reportedBy/{countries}/imo/{imo}.{format}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> searchByIMO(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String countries, @PathVariable String imo, @PathVariable String format, @RequestParam(value="s_c[]", required=false) String[] sortingCriteria) {
		return this.searchByIMO(request, sources, historical, countries, DEFAULT_MAX_RESULTS, imo, format, sortingCriteria);
	}
	
	@RequestMapping(value="{sources}/{historical}/reportedBy/{countries}/eucfr/{eucfr}.{format}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> searchByEUCFR(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String countries, @PathVariable String eucfr, @PathVariable String format, @RequestParam(value="s_c[]", required=false) String[] sortingCriteria) {
		return this.searchByEUCFR(request, sources, historical, countries, DEFAULT_MAX_RESULTS, eucfr, format, sortingCriteria);
	}
	
	@RequestMapping(value="{sources}/{historical}/reportedBy/{countries}/name/{name}.{format}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> searchByName(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String countries,  @PathVariable String name, @PathVariable String format, @RequestParam(value="s_c[]", required=false) String[] sortingCriteria) {
		return this.searchByName(request, sources, historical, countries, DEFAULT_MAX_RESULTS, name, format, sortingCriteria);
	}
	
	@RequestMapping(value="{sources}/{historical}/reportedBy/{countries}/extMark/{extMark}.{format}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> searchByExternalMarkings(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String countries,  @PathVariable String externalMarkings, @PathVariable String format, @RequestParam(value="s_c[]", required=false) String[] sortingCriteria) {
		return this.searchByExternalMarkings(request, sources, historical, countries, DEFAULT_MAX_RESULTS, externalMarkings, format, sortingCriteria);
	}
	
	@RequestMapping(value="{sources}/{historical}/reportedBy/{countries}/ircs/{ircs}.{format}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> searchByIRCS(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String countries, @PathVariable String ircs, @PathVariable String format, @RequestParam(value="s_c[]", required=false) String[] sortingCriteria) {
		return this.searchByIRCS(request, sources, historical, countries, DEFAULT_MAX_RESULTS, ircs, format, sortingCriteria);
	}
	
	@RequestMapping(value="{sources}/{historical}/reportedBy/{countries}/regNo/{regNo}.{format}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> searchByRegNo(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String countries, @PathVariable String regNo, @PathVariable String format, @RequestParam(value="s_c[]", required=false) String[] sortingCriteria) {
		return this.searchByRegNo(request, sources, historical, countries, DEFAULT_MAX_RESULTS, regNo, format, sortingCriteria);
	}
		
	@SuppressWarnings("unused")
	@RequestMapping(value="{sources}/{historical}/reportedBy/{countries}/max/{maxEntries}/uid/{uid}.{format}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> searchByUID(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String countries, @PathVariable Integer maxEntries, @PathVariable String uid, @PathVariable String format, @RequestParam(value="s_c[]", required=false) String[] sortingCriteria) {
		long end, start = System.currentTimeMillis();
			
		this._log.info("Live searching vessels by {} UID {} from {}", historical, uid, sources);
		
		try {
			Date atDate = "current".equals(historical) ? new Date() : null;
			Boolean groupByUID = Boolean.TRUE;
			Boolean authorizedOnly = Boolean.FALSE;
			String authorizationIssuer = null;

			VesselRecordSearchFilter filter = this.getCommonFilter(request, sources, countries, maxEntries, atDate, sortingCriteria);

			Collection<IdentifierSearchResult> vesselUIDData = this.uidAutocompletionDAO.searchVesselUID(null, uid, filter.getSourceSystems(), maxEntries);
						
			List<Integer> UIDs = new ListSet<Integer>();
			for(IdentifierSearchResult foundUID : vesselUIDData) {
				UIDs.add(Integer.valueOf(foundUID.getIdentifier()));
			}
			
			VesselRecordSearchResponse<VesselRecord> vessels = null;
			
			if(!UIDs.isEmpty()) {
				filter.WHERE(filter.clauses().and(CommonClauses.VRMF_UIDS, (Object[])UIDs.toArray(new Integer[UIDs.size()])));
			
				String[] availableSources = this.getAvailableSources(request, false);
				
				vessels = this.searchDAO.searchVessels(filter.GROUP_DUPLICATES().retrieveAdditionalMetadata());
			}
			
			if(vessels == null)
				vessels = new VesselRecordSearchResponse<VesselRecord>();

			vessels.setPageSize(vessels.getCurrentResultPageSize());
			
			end = System.currentTimeMillis();
			
			JSONResponse<VesselRecordSearchResponse<VesselRecord>> response = new JSONResponse<VesselRecordSearchResponse<VesselRecord>>();
			response.setError(false);
			response.setElapsed(end - start);
			response.setData(vessels);
					
			byte[] content = response.JSONify().getBytes();
			
			return this.commonResponse(content, "json", request);
		} catch (Throwable t) {
			this._log.error("Unable to find vessels by UID", t);
			
			return this.manageError(t);
		}			
	}
	
	@RequestMapping(value="{sources}/{historical}/reportedBy/{countries}/max/{maxEntries}/imo/{imo}.{format}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> searchByIMO(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String countries, @PathVariable Integer maxEntries, @PathVariable String imo, @PathVariable String format, @RequestParam(value="s_c[]", required=false) String[] sortingCriteria) {
		this._log.info("Live searching vessels by " + historical + " IMO " + imo + " from " + sources);
		
		try {
			return this.doSearchByIdentifier(request, "IMO", imo, sources, historical, countries, maxEntries, sortingCriteria);
		} catch (Throwable t) {
			this._log.error("Unable to find vessels by IMO", t);
			
			return this.manageError(t);
		}	
	}
	
	@RequestMapping(value="{sources}/{historical}/reportedBy/{countries}/max/{maxEntries}/eucfr/{eucfr}.{format}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> searchByEUCFR(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String countries, @PathVariable Integer maxEntries, @PathVariable String eucfr, @PathVariable String format, @RequestParam(value="s_c[]", required=false) String[] sortingCriteria) {
		this._log.info("Live searching vessels by " + historical + " EU CFR " + eucfr + " from " + sources);
		
		try {		
			return this.doSearchByIdentifier(request, "EU_CFR", eucfr, sources, historical, countries, maxEntries, sortingCriteria);
		} catch (Throwable t) {
			this._log.error("Unable to find vessels by EU CFR", t);
			
			return this.manageError(t);
		}	
	}
	
	protected ResponseEntity<byte[]> doSearchByIdentifier(HttpServletRequest request, String identifierType, String identifier, String sources, String historical, String countries, Integer maxEntries, String sortingCriteria[]) throws Throwable {
		Date atDate = "current".equals(historical) ? new Date() : null;
		
		@SuppressWarnings("unused")
		Boolean groupByUID = Boolean.TRUE;
		
		@SuppressWarnings("unused")
		Boolean authorizedOnly = Boolean.FALSE;
		
		@SuppressWarnings("unused")
		String authorizationIssuer = null;

		VesselRecordSearchFilter filter = this.getCommonFilter(request, sources, countries, maxEntries, atDate, sortingCriteria);

		VesselRecordSearchResponse<VesselRecord> vessels = null;
		
		filter.WHERE(
			filter.clauses().
				and(VesselRecordClauses.IDENTIFIER_TYPE, identifierType).
				and(VesselRecordClauses.IDENTIFIERS, new Object[] { "%" + identifier + "%" }).
				and(VesselRecordClauses.IDENTIFIER_LIKE, Boolean.TRUE)
		);
				
		long end, start = System.currentTimeMillis();
		
		vessels = this.searchDAO.searchVessels(filter.GROUP_DUPLICATES().retrieveAdditionalMetadata());
		
		if(vessels == null)
			vessels = new VesselRecordSearchResponse<VesselRecord>();

		vessels.setPageSize(vessels.getCurrentResultPageSize());
		
		end = System.currentTimeMillis();
		
		JSONResponse<VesselRecordSearchResponse<VesselRecord>> response = new JSONResponse<VesselRecordSearchResponse<VesselRecord>>();
		response.setError(false);
		response.setElapsed(end - start);
		response.setData(vessels);
				
		byte[] content = response.JSONify().getBytes();
		
		return this.commonResponse(content, "json", request);
	}
	
	@RequestMapping(value="{sources}/{historical}/reportedBy/{countries}/max/{maxEntries}/name/{name}.{format}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> searchByName(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String countries, @PathVariable Integer maxEntries, @PathVariable String name, @PathVariable String format, @RequestParam(value="s_c[]", required=false) String[] sortingCriteria) {
		long end, start = System.currentTimeMillis();
		
		this._log.info("Live searching vessels by {} name {} from {}", historical, name, sources);
		
		try {		
			Date atDate = this.truncate("current".equals(historical) ? new Date() : null);
			Boolean groupByUID = Boolean.TRUE;

			VesselRecordSearchFilter filter = this.getCommonFilter(request, sources, countries, maxEntries, atDate, sortingCriteria);
			filter.GROUP_DUPLICATES();
			
			Collection<FuzzySmartNamedSearchResult> vesselNameData = this.nameAutocompletionDAO.searchVesselNames(name, atDate, (int)Math.round(maxEntries * COLLISION_FACTOR), filter.getSourceSystems(), groupByUID);
						
			List<byte[]> names = new ListSet<byte[]>();
			for(FuzzySmartNamedSearchResult foundName : vesselNameData) {
				names.add(foundName.getName().getBytes(Charset.forName("UTF-8")));
			}
			
			List<VesselRecord> vesselsList = new ArrayList<VesselRecord>();
			Set<Integer> vesselIDsList = new TreeSet<Integer>();
			VesselRecordSearchResponse<VesselRecord> vessels = null;
			
			VesselRecordSearchFilter nameFilter;
			
			//Does a vessel search for each name, checking that the total (growing) number of found vessels
			//doesn't exceed the set threshold. This is not performing at best, but is necessary to preserve 
			//the resultset ordering by most similar name. Also, as a side effect, when searching for historical data
			//it doesn't returns the name the vessel might have had (a-lá 'link metadata to search criteria') because
			//it's a fuzzy search and results cannot be further filtered...
			if(!names.isEmpty()) {
				VesselRecordWhere clauses;
				cycle: for(byte[] currentName : names) {
					nameFilter = SerializationHelper.clone(filter);
					
					clauses = nameFilter.clauses();
					
					clauses.and(VesselRecordClauses.NAMES, new Object[] { new String(currentName, "UTF-8") });
					nameFilter.WHERE(clauses);
					nameFilter.retrieveAdditionalMetadata();
					
					vessels = this.searchDAO.searchVessels(nameFilter);
					
					if(vessels != null && vessels.getDataSet() != null)
						for(VesselRecord currentVessel : vessels.getDataSet()) {
							if(!vesselIDsList.contains(currentVessel.getUid())) {
								vesselsList.add(currentVessel);
								vesselIDsList.add(currentVessel.getUid());
							}
							
							if(vesselsList.size() >= maxEntries)
								break cycle;
						}
				}
			}

			if(vesselsList.isEmpty())
				vessels = new VesselRecordSearchResponse<VesselRecord>();
			else
				vessels = new VesselRecordSearchResponse<VesselRecord>(0, vesselsList.size(), vesselsList.size(), vesselsList);
			
			end = System.currentTimeMillis();
			
			JSONResponse<VesselRecordSearchResponse<VesselRecord>> response = new JSONResponse<VesselRecordSearchResponse<VesselRecord>>();
			response.setError(false);
			response.setElapsed(end - start);
			response.setData(vessels);
					
			byte[] content = response.JSONify().getBytes();
			
			return this.commonResponse(content, "json", request);
		} catch (Throwable t) {
			this._log.error("Unable to find vessels by name", t);
			
			return this.manageError(t);
		}
	}
	
	@RequestMapping(value="{sources}/{historical}/reportedBy/{countries}/max/{maxEntries}/extMark/{extMark}.{format}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> searchByExternalMarkings(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String countries, @PathVariable Integer maxEntries, @PathVariable String extMark, @PathVariable String format, @RequestParam(value="s_c[]", required=false) String[] sortingCriteria) {
		long end, start = System.currentTimeMillis();
		
		this._log.info("Live searching vessels by {} external markings {} from {}", historical, extMark, sources);
		
		try {		
			Date atDate = "current".equals(historical) ? new Date() : null;
			Boolean groupByUID = Boolean.TRUE;

			VesselRecordSearchFilter filter = this.getCommonFilter(request, sources, countries, maxEntries, atDate, sortingCriteria);

			Collection<ExternalMarkingSearchResult> extMarkData = this.extMarkAutocompletionDAO.searchVesselExternalMarkings(extMark, atDate, (int)Math.round(maxEntries * COLLISION_FACTOR), filter.getSourceSystems(), groupByUID);
						
			List<byte[]> externalMarkings = new ListSet<byte[]>();
			for(ExternalMarkingSearchResult foundExternalMarking : extMarkData) {
				externalMarkings.add(foundExternalMarking.getExternalMarking().getBytes(Charset.forName("UTF-8")));
			}
			
			VesselRecordSearchResponse<VesselRecord> vessels = null;
			
			if(!externalMarkings.isEmpty()) {
				filter.WHERE(filter.clauses().and(VesselRecordClauses.EXTERNAL_MARKINGS, (Object[])externalMarkings.toArray(new byte[externalMarkings.size()][])));
				
				vessels = this.searchDAO.searchVessels(filter.GROUP_DUPLICATES().retrieveAdditionalMetadata());
			}
			
			if(vessels == null)
				vessels = new VesselRecordSearchResponse<VesselRecord>();

			vessels.setPageSize(vessels.getCurrentResultPageSize());
			
			end = System.currentTimeMillis();
			
			JSONResponse<VesselRecordSearchResponse<VesselRecord>> response = new JSONResponse<VesselRecordSearchResponse<VesselRecord>>();
			response.setError(false);
			response.setElapsed(end - start);
			response.setData(vessels);
					
			byte[] content = response.JSONify().getBytes();
			
			return this.commonResponse(content, "json", request);
		} catch (Throwable t) {
			this._log.error("Unable to find vessels by external markings", t);
			
			return this.manageError(t);
		}
	}
	
	@RequestMapping(value="{sources}/{historical}/reportedBy/{countries}/max/{maxEntries}/ircs/{ircs}.{format}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> searchByIRCS(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String countries, @PathVariable Integer maxEntries, @PathVariable String ircs, @PathVariable String format, @RequestParam(value="s_c[]", required=false) String[] sortingCriteria) {
		long end, start = System.currentTimeMillis();
		
		this._log.info("Live searching vessels by {} IRCS {} from {}", historical, ircs, sources);
		
		try {		
			Date atDate = "current".equals(historical) ? new Date() : null;
			
			@SuppressWarnings("unused")
			Boolean groupByUID = Boolean.TRUE;
			
			@SuppressWarnings("unused")
			Boolean authorizedOnly = Boolean.FALSE;
			
			@SuppressWarnings("unused")
			String authorizationIssuer = null;

			VesselRecordSearchFilter filter = this.getCommonFilter(request, sources, countries, maxEntries, atDate, sortingCriteria);

			VesselRecordSearchResponse<VesselRecord> vessels = null;
			filter.WHERE(
				filter.clauses().
					and(VesselRecordClauses.CALLSIGNS, new Object[] { "%" + ircs + "%" }).
					and(VesselRecordClauses.CALLSIGNS_LIKE, Boolean.TRUE));
			
			vessels = this.searchDAO.searchVessels(filter.GROUP_DUPLICATES().retrieveAdditionalMetadata());
			
			if(vessels == null)
				vessels = new VesselRecordSearchResponse<VesselRecord>();

			vessels.setPageSize(vessels.getCurrentResultPageSize());
			
			end = System.currentTimeMillis();
			
			JSONResponse<VesselRecordSearchResponse<VesselRecord>> response = new JSONResponse<VesselRecordSearchResponse<VesselRecord>>();
			response.setError(false);
			response.setElapsed(end - start);
			response.setData(vessels);
					
			byte[] content = response.JSONify().getBytes();
			
			return this.commonResponse(content, "json", request);

		} catch (Throwable t) {
			this._log.error("Unable to find vessels by IRCS", t);
			
			return this.manageError(t);
		}
	}
	
	@RequestMapping(value="{sources}/{historical}/reportedBy/{countries}/max/{maxEntries}/regNo/{regNo}.{format}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> searchByRegNo(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String countries, @PathVariable Integer maxEntries, @PathVariable String regNo, @PathVariable String format, @RequestParam(value="s_c[]", required=false) String[] sortingCriteria) {
		long end, start = System.currentTimeMillis();

		this._log.info("Live searching vessels by {} reg. no. {} from {}", historical, regNo, sources);
		
		try {		
			Date atDate = "current".equals(historical) ? new Date() : null;
			Boolean groupByUID = Boolean.TRUE;

			VesselRecordSearchFilter filter = this.getCommonFilter(request, sources, countries, maxEntries, atDate, sortingCriteria);

			Collection<RegistrationNumberSearchResult> vesselRegNoData = this.regNoAutocompletionDAO.searchVesselRegNo("REG_NO_SEARCH", null, null, regNo, atDate, (int)Math.round(maxEntries * COLLISION_FACTOR), filter.getSourceSystems(), groupByUID);
						
			List<byte[]> regNumbers = new ListSet<byte[]>();
			for(RegistrationNumberSearchResult foundIRCS : vesselRegNoData) {
				regNumbers.add(foundIRCS.getRegistrationNumber().getBytes(Charset.forName("UTF-8")));
			}

			VesselRecordSearchResponse<VesselRecord> vessels = null;
			
			if(!regNumbers.isEmpty()) {
				filter.WHERE(
					filter.clauses().
						and(VesselRecordClauses.REGISTRATION_NUMBERS, (Object[])regNumbers.toArray(new byte[regNumbers.size()][]))
					);
							
				vessels = this.searchDAO.searchVessels(filter.GROUP_DUPLICATES().retrieveAdditionalMetadata());
			}
			
			if(vessels == null)
				vessels = new VesselRecordSearchResponse<VesselRecord>();

			vessels.setPageSize(vessels.getCurrentResultPageSize());
			
			end = System.currentTimeMillis();
			
			JSONResponse<VesselRecordSearchResponse<VesselRecord>> response = new JSONResponse<VesselRecordSearchResponse<VesselRecord>>();
			response.setError(false);
			response.setElapsed(end - start);
			response.setData(vessels);
					
			byte[] content = response.JSONify().getBytes();
			
			return this.commonResponse(content, "json", request);
		} catch (Throwable t) {
			this._log.error("Unable to find vessels by reg. no.", t);
			
			return this.manageError(t);
		}
	}
	
	protected ResponseEntity<byte[]> commonResponse(byte[] body, String format, HttpServletRequest request) {
		HttpHeaders contentTypeHeaders = this.getContentTypeDispositionAndLengthHeaders(null, format, body == null ? 0 : body.length);
		HttpHeaders cachingHeaders = this.getCachingHeaders(3600);
		
		return new ResponseEntity<byte[]>(body, this.mergeHeaders(contentTypeHeaders, cachingHeaders), HttpStatus.OK);
	}
	
	protected Properties commonProperties(long elapsed, String[] inclusionPatterns) {
		Properties props = new Properties();
		
		props.put(JSONDatasetEmitter.EXCLUSION_PATTERNS_PROPERTY, new String[] { "*" });
		props.put("elapsed", elapsed);
		
		if(inclusionPatterns != null && inclusionPatterns.length > 0)
			props.put(JSONDatasetEmitter.INCLUSION_PATTERNS_PROPERTY, inclusionPatterns);
		
		return props;
	}
}