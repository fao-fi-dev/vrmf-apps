/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder)
 */
package org.fao.fi.vrmf.web.apps.finder;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 20 Jun 2011   faoadmin     Creation.
 *
 * @version 1.0
 * @since 20 Jun 2011
 */
public interface SearchServiceConstants {
	String DEFAULT_QUERY_PARAMETER = "q";
	
	String GROUP_DUPLICATES_PARAMETER 	    = "gd";	
	String SEARCH_DUPLICATES_ONLY_PARAMETER = "sd";

	String LINK_METADATA_TO_SEARCH_CRITERIA_PARAMETER = "mts";
	String TIME_RELATED_METADATA_PARAMETER 	  		  = "mtf";
	String SYSTEMS_RELATED_METADATA_PARAMETER 		  = "msf";
	
	String ADDITIONAL_METADATA_PARAMETER   = "am";
	
	String SOURCE_SYSTEM_PARAMETER 		   = "s";
	String LIMIT_TO_CURRENT_DATA_PARAMETER = "c";	
	String TIME_FRAME_MIN_PARAMETER	   	   = "tfm";
	String TIME_FRAME_MAX_PARAMETER	   	   = "tfM";
	String AT_DATE_PARAMETER			   = "ad";
	
	String RESULT_PAGE_SIZE_PARAMETER	   = "ps";
	String RESULT_OFFSET_PARAMETER	   	   = "o";
	
	String SORTING_CRITERIA_PARAMETER 	   = "sc";
	
	String LANGUAGE_PARAMETER			   = "lang";
}
