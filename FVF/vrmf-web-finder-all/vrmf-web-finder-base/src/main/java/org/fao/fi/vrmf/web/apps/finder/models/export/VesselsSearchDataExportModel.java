/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder)
 */
package org.fao.fi.vrmf.web.apps.finder.models.export;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportFieldModelType;
import org.fao.fi.vrmf.common.data.exchange.core.models.DataExportModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.typed.DataExportFloatFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.typed.DataExportIntegerFieldModel;
import org.fao.fi.vrmf.common.data.exchange.core.models.typed.DataExportStringFieldModel;
import org.fao.fi.vrmf.common.data.exchange.domain.countries.transformers.CountryIDToISO2DataModelTransformer;
import org.fao.fi.vrmf.common.data.exchange.domain.countries.transformers.CountryIDToNameDataModelTransformer;
import org.fao.fi.vrmf.common.models.generated.SCountries;
import org.fao.fi.vrmf.web.apps.finder.common.models.extended.TimeframedAuthorizedVessel;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 29 Nov 2011   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 29 Nov 2011
 */
public class VesselsSearchDataExportModel extends DataExportModel<TimeframedAuthorizedVessel> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 2438021770677217633L;
	
	/**
	 * Class constructor
	 */
	public VesselsSearchDataExportModel(Map<Integer, SCountries> countriesByIDMap, Map<String, SCountries> countriesByISO2Map) {
		super();
	
		List<DataExportFieldModel<TimeframedAuthorizedVessel>> model = new ArrayList<DataExportFieldModel<TimeframedAuthorizedVessel>>();
		model.add(new DataExportIntegerFieldModel<TimeframedAuthorizedVessel>("uid", "UID"));
		model.add(new DataExportStringFieldModel<TimeframedAuthorizedVessel>(DataExportFieldModelType.OPTIONAL, "imo", "IMO"));
		model.add(new DataExportStringFieldModel<TimeframedAuthorizedVessel>("currentName", "Name"));	
		model.add(new DataExportStringFieldModel<TimeframedAuthorizedVessel>("currentLengthType", "Current length (code)"));
		model.add(new DataExportFloatFieldModel<TimeframedAuthorizedVessel>("currentLengthValue", "Current length"));
		model.add(new DataExportStringFieldModel<TimeframedAuthorizedVessel>("currentTonnageType", "Current tonnage (code)"));
		model.add(new DataExportFloatFieldModel<TimeframedAuthorizedVessel>("currentTonnageValue", "Current tonnage"));		
		model.add(new DataExportStringFieldModel<TimeframedAuthorizedVessel>(DataExportFieldModelType.OPTIONAL, "currentCallsign", "Callsign"));
		model.add(new DataExportStringFieldModel<TimeframedAuthorizedVessel>("currentRegCountry", "Country of registry (code)", new CountryIDToISO2DataModelTransformer<TimeframedAuthorizedVessel>(countriesByIDMap)));
		model.add(new DataExportStringFieldModel<TimeframedAuthorizedVessel>("currentRegCountry", "Country of registry", new CountryIDToNameDataModelTransformer<TimeframedAuthorizedVessel>(countriesByIDMap, countriesByISO2Map)));
		model.add(new DataExportStringFieldModel<TimeframedAuthorizedVessel>("currentRegPort", "Port of registry"));
		model.add(new DataExportStringFieldModel<TimeframedAuthorizedVessel>("currentRegNo", "Registration number"));
		model.add(new DataExportStringFieldModel<TimeframedAuthorizedVessel>("authIssuingCountry", "Reporting flag (code)", new CountryIDToISO2DataModelTransformer<TimeframedAuthorizedVessel>(countriesByIDMap)));
		model.add(new DataExportStringFieldModel<TimeframedAuthorizedVessel>("authIssuingCountry", "Reporting flag", new CountryIDToNameDataModelTransformer<TimeframedAuthorizedVessel>(countriesByIDMap, countriesByISO2Map)));		
//		model.add(new DataExportStringFieldModel<TimeframedAuthorizedVessel>("Authorization status", new BasicVesselAuthorizationRangeToAuthorizationStatusTransformer<TimeframedAuthorizedVessel>()));
//		model.add(new DataExportDateFieldModel<TimeframedAuthorizedVessel>("authFrom", "Authorization start"));
//		model.add(new DataExportDateFieldModel<TimeframedAuthorizedVessel>("authTo", "Authorization end"));

		this.setFieldsModel(model);
	}	
}
