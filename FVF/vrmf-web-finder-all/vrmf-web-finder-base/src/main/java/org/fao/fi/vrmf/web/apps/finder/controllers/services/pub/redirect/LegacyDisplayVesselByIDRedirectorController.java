/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder)
 */
package org.fao.fi.vrmf.web.apps.finder.controllers.services.pub.redirect;

import javax.inject.Singleton;

import org.fao.fi.vrmf.web.apps.finder.controllers.services.pub.PublicServicesConstants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 21 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 21 May 2012
 */
@Singleton @Controller
@RequestMapping(PublicServicesConstants.MAPPING_PREFIX + "display/" + "*")
public class LegacyDisplayVesselByIDRedirectorController extends DisplayVesselByIDRedirectorController{
}