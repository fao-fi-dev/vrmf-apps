/**
 * (c) 2015 FAO / UN (project: vrmf-web-finder)
 */
package org.fao.fi.vrmf.web.apps.finder.support;

import java.util.ArrayList;
import java.util.Collection;

import javax.inject.Named;
import javax.inject.Singleton;

import org.fao.fi.vrmf.web.apps.finder.controllers.services.pub.meta.CacheManagementController;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;

/**
 * A NOP implementation of the {@link org.springframework.cache.CacheManager} interface.
 * 
 * This is needed in order to let the {@link CacheManagementController} work when caches are disabled.
 * 
 * If this NOP cache manager is not instantiated in the Spring context, under those circumstances, the {@link CacheManagementController}
 * bean cannot be instantiated as well and the context startup process halts.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Nov 4, 2015   Fabio     Creation.
 *
 * @version 1.0
 * @since Nov 4, 2015
 */
@Named("vrmf.finder.nop.cache.manager") @Singleton
public class NopCacheManager implements CacheManager {

	/* (non-Javadoc)
	 * @see org.springframework.cache.CacheManager#getCache(java.lang.String)
	 */
	@Override
	public Cache getCache(String name) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.springframework.cache.CacheManager#getCacheNames()
	 */
	@Override
	public Collection<String> getCacheNames() {
		return new ArrayList<String>();
	}
}
