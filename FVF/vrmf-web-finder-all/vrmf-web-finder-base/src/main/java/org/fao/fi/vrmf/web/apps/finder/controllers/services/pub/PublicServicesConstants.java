/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder)
 */
package org.fao.fi.vrmf.web.apps.finder.controllers.services.pub;

import org.fao.fi.vrmf.common.web.controllers.services.CommonServicesConstants;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 14 May 2012
 */
public interface PublicServicesConstants {
	String MAPPING_PREFIX = CommonServicesConstants.MAPPING_PREFIX + "public/";
	
	String USER_SERVICES_PREFIX = MAPPING_PREFIX + "user/";
	String INDEXING_SERVICES_PREFIX = MAPPING_PREFIX + "indexing/";
	
	String META_SERVICES_PREFIX 	   		= MAPPING_PREFIX + "meta/";
	
	String VESSELS_SERVICES_PREFIX 	   		= MAPPING_PREFIX + "vessels/";
	String VESSELS_RETRIEVE_SERVICES_PREFIX = VESSELS_SERVICES_PREFIX + "retrieve/";
	String VESSELS_SEARCH_SERVICES_PREFIX 	= VESSELS_SERVICES_PREFIX + "search/";
}
