/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder)
 */
package org.fao.fi.vrmf.web.apps.finder.controllers.services.pub.redirect;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 25 Oct 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 25 Oct 2012
 */
public interface DisplayVesselByIDRedirectorConstants {
	String DISPLAY_PAGE = "displayVessel.jsp";
	String REDIRECT_TO_NEW_UID_PAGE = "redirectToNewUID.jsp";
	
	String ID_REQUEST_PARAMETER 	= "ID";
	String RELOAD_REQUEST_PARAMETER = "reload";
	
	String VRMF_VESSELS_REDIRECT_BY_ID_GROUPING_REQUEST_ATTRIBUTE = "VRMF_VESSELS_REDIRECT_BY_ID_GROUPING";
	String VRMF_VESSELS_REDIRECT_BY_ID_CRITERIA_REQUEST_ATTRIBUTE = "VRMF_VESSELS_REDIRECT_BY_ID_CRITERIA";
	String VRMF_MULTIPLE_VESSELS_DATA_REQUEST_ATTRIBUTE	 		  = "VRMF_MULTIPLE_VESSELS_DATA";
	
	String VRMF_STALE_UID_REQUEST_ATTRIBUTE = "VRMF_STALE_UID";
	String VRMF_NEW_UID_REQUEST_ATTRIBUTE   = "VRMF_NEW_UID";
	
	String VRMF_VESSEL_DATA_REQUEST_ATTRIBUTE 			 = "VRMF_VESSEL_DATA";
	String VRMF_SERIALIZED_VESSEL_DATA_REQUEST_ATTRIBUTE = "VRMF_SERIALIZED_VESSEL_DATA";
	String VRMF_GROUP_BY_UID_REQUEST_ATTRIBUTE 			 = "VRMF_GROUP_BY_UID";
}