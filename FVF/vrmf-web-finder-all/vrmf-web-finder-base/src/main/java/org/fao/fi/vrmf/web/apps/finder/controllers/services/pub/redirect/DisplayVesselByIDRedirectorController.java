/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder)
 */
package org.fao.fi.vrmf.web.apps.finder.controllers.services.pub.redirect;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.sh.utility.model.NameValuePair;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.business.dao.VesselRecordSearchDAO;
import org.fao.fi.vrmf.common.j2ee.RequestAttributesConstants;
import org.fao.fi.vrmf.common.j2ee.exceptions.authentication.NoLoggedUserException;
import org.fao.fi.vrmf.common.j2ee.utilities.ServletsHelper;
import org.fao.fi.vrmf.common.models.extended.FullVessel;
import org.fao.fi.vrmf.common.models.generated.SCountries;
import org.fao.fi.vrmf.common.models.generated.SCountriesExample;
import org.fao.fi.vrmf.common.models.generated.Vessels;
import org.fao.fi.vrmf.common.models.generated.VesselsHistory;
import org.fao.fi.vrmf.common.models.generated.VesselsHistoryExample;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.VesselRecordSearchFilter;
import org.fao.fi.vrmf.common.search.dsl.support.models.vessels.record.VesselRecord;
import org.fao.fi.vrmf.common.web.controllers.services.pub.vessels.retrieve.CommonRetrieveVesselDataController;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import flexjson.JSONSerializer;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 21 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 21 May 2012
 */
@Singleton @Controller
@RequestMapping(RedirectorsConstants.DISPLAY_MAPPING_PREFIX + "*")
public class DisplayVesselByIDRedirectorController extends CommonRetrieveVesselDataController<VesselRecord, VesselRecordSearchFilter> implements InitializingBean {
	final protected static Boolean RELOAD 	 = Boolean.TRUE;
	final protected static String NOT_ID_OR_UID_OR_IMO = "^(?:(?!U?ID$|IMO$)).*$";
	final protected static Boolean DONT_RELOAD = Boolean.FALSE;

	@Inject protected VesselRecordSearchDAO _searchVesselsDAO;

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		this._countriesMap = new HashMap<Integer, SCountries>();

		for(SCountries country : this.countriesDAO.selectByExample(new SCountriesExample()))
			this._countriesMap.put(country.getId(), country);
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.web.controllers.services.pub.vessels.retrieve.AbstractRetrieveVesselDataController#getEmptySearchFilter()
	 */
	@Override
	protected VesselRecordSearchFilter getEmptySearchFilter() {
		return new VesselRecordSearchFilter();
	}

	/**
	 * @return the 'searchVesselsDAO' value
	 */
	public VesselRecordSearchDAO getSearchVesselsDAO() {
		return this._searchVesselsDAO;
	}

	/**
	 * @param searchVesselsDAO the 'searchVesselsDAO' value to set
	 */
	public void setSearchVesselsDAO(VesselRecordSearchDAO searchVesselsDAO) {
		this._searchVesselsDAO = searchVesselsDAO;
	}

	@RequestMapping(value="vessel/UID/{UID:[0-9]{9}}/reload", method=RequestMethod.GET)
	public void redirectByUID(HttpServletRequest request, HttpServletResponse response, @PathVariable Integer UID) {
		this.doRedirect(request, response, BY_UID, UID, RELOAD);
	}

	@RequestMapping(value="vessel/UID/{UID:[0-9]{9}}", method=RequestMethod.GET)
	public void redirectByUID(HttpServletRequest request, HttpServletResponse response, @PathVariable Integer UID, @RequestParam(required=false) Boolean reload) {
		this.doRedirect(request, response, BY_UID, UID, reload);
	}

	@RequestMapping(value="vessel/ID/{ID:[0-9]+}/reload", method=RequestMethod.GET)
	public void redirectByID(HttpServletRequest request, HttpServletResponse response, @PathVariable Integer ID) {
		this.doRedirect(request, response, BY_ID, ID, RELOAD);
	}

	@RequestMapping(value="vessel/ID/{ID:[0-9]+}", method=RequestMethod.GET)
	public void redirectByID(HttpServletRequest request, HttpServletResponse response, @PathVariable Integer ID, @RequestParam(required=false) Boolean reload) {
		this.doRedirect(request, response, BY_ID, ID, reload);
	}

	@RequestMapping(value="vessel/IMO/{IMO:[0-9]{6,7}}", method=RequestMethod.GET)
	public void redirectByIMO(HttpServletRequest request, HttpServletResponse response, @PathVariable Integer IMO) {
		this.doRedirectByIMO(request, response, BY_UID, IMO, null);
	}

	@RequestMapping(value="vessel/{groupBy:ID|UID}/IMO/{IMO:[0-9]{7}}", method=RequestMethod.GET)
	public void redirectByIMO(HttpServletRequest request, HttpServletResponse response, @PathVariable String groupBy, @PathVariable Integer IMO) {
		this.doRedirectByIMO(request, response, "ID".equals(groupBy) ? BY_ID : BY_UID, IMO, null);
	}

	@RequestMapping(value="vessel/IMO/{IMO:[0-9]{6,7}}/{index:[1-9][0-9]*}", method=RequestMethod.GET)
	public void redirectByIMO(HttpServletRequest request, HttpServletResponse response, @PathVariable Integer IMO, @PathVariable Integer index) {
		this.doRedirectByIMO(request, response, BY_UID, IMO, index);
	}

	@RequestMapping(value="vessel/{groupBy:ID|UID}/IMO/{IMO:[0-9]{6,7}}/{index:[1-9][0-9]*}", method=RequestMethod.GET)
	public void redirectByIdentifier(HttpServletRequest request, HttpServletResponse response, @PathVariable String groupBy, @PathVariable Integer IMO,  @PathVariable Integer index) {
		this.doRedirectByIMO(request, response, "ID".equals(groupBy) ? BY_ID : BY_UID, IMO, null);
	}

	@RequestMapping(value="vessel/{groupBy:ID|UID}/{identifierType:" + NOT_ID_OR_UID_OR_IMO + "}/{identifier}/{index:[1-9][0-9]*}", method=RequestMethod.GET)
	public void redirectByIdentifier(HttpServletRequest request, HttpServletResponse response, @PathVariable String groupBy, @PathVariable String identifierType, @PathVariable String identifier, @PathVariable Integer index) {
		this.doRedirectByIdentifier(request, response, "ID".equals(groupBy) ? BY_ID : BY_UID, identifierType, identifier, index);
	}

	@RequestMapping(value="vessel/{identifierType:" + NOT_ID_OR_UID_OR_IMO + "}/{identifier}", method=RequestMethod.GET)
	public void redirectByIdentifier(HttpServletRequest request, HttpServletResponse response, @PathVariable String identifierType, @PathVariable String identifier) {
		this.doRedirectByIdentifier(request, response, BY_UID, identifierType, identifier, null);
	}

	@RequestMapping(value="vessel/{groupBy:ID|UID}/{identifierType:" + NOT_ID_OR_UID_OR_IMO + "}/{identifier}", method=RequestMethod.GET)
	public void redirectByIdentifier(HttpServletRequest request, HttpServletResponse response, @PathVariable String groupBy, @PathVariable String identifierType, @PathVariable String identifier) {
		this.doRedirectByIdentifier(request, response, "ID".equals(groupBy) ? BY_ID : BY_UID, identifierType, identifier, null);
	}

	@RequestMapping(value="vessel/{identifierType:" + NOT_ID_OR_UID_OR_IMO + "}/{identifier}/{index:[1-9][0-9]*}", method=RequestMethod.GET)
	public void redirectByIdentifier(HttpServletRequest request, HttpServletResponse response, @PathVariable String identifierType, @PathVariable String identifier, @PathVariable Integer index) {
		this.doRedirectByIdentifier(request, response, BY_UID, identifierType, identifier, index);
	}

	protected String padID(int ID) {
		return this.padID(ID, 9);
	}

	protected String padID(int ID, int length) {
		String padded = String.valueOf(ID);

		while(padded.length() < length) {
			padded = "0" + padded;
		}

		return padded;
	}

	protected void doRedirectByIMO(HttpServletRequest request, HttpServletResponse response, boolean byUID, Integer IMO, Integer index) {
		try {
			AbstractRedirectorController.propagateOriginalURL(request);

			request.setAttribute(DisplayVesselByIDRedirectorConstants.VRMF_VESSELS_REDIRECT_BY_ID_GROUPING_REQUEST_ATTRIBUTE, byUID ? "UID" : "ID");
			request.setAttribute(DisplayVesselByIDRedirectorConstants.VRMF_VESSELS_REDIRECT_BY_ID_CRITERIA_REQUEST_ATTRIBUTE, new NameValuePair("IMO", IMO));

			String[] availableSources = this.getAvailableSources(ServletsHelper.getLoggedUser(request), request, DONT_INCLUDE_OTHER_SOURCES_BY_DEFAULT);
			String[] effectiveSources = this.getSources(ServletsHelper.getLoggedUser(request), request, NO_REQUESTED_SOURCES, INCLUDE_OTHER_SOURCES_BY_DEFAULT);

			List<VesselRecord> vessels = this.doRetrieveVesselsByIdentifier(this._searchVesselsDAO, dataCustomizer.getTargetTable(request), byUID, "IMO", String.valueOf(IMO), effectiveSources);

			if(vessels != null && !vessels.isEmpty()) {
				this.doRedirectToVesselByIdentifier(request, response, byUID, "IMO", String.valueOf(IMO), vessels, effectiveSources, index);
			} else {
				this.sendResponseError(response, HttpStatus.NOT_FOUND, "No data available for vessel with IMO " + IMO + " grouped by " + ( byUID ? "UID" : "ID" ) + " ( available sources: " + CollectionsHelper.serializeArray(availableSources) + " )");
			}
		} catch(NoLoggedUserException NLe) {
			this.sendResponseError(response, HttpStatus.FORBIDDEN, NLe);
		} catch(Throwable t) {
			this.sendResponseError(response, HttpStatus.INTERNAL_SERVER_ERROR, t);
		}
	}

	protected void doRedirectByIdentifier(HttpServletRequest request, HttpServletResponse response, boolean byUID, String identifierType, String identifier, Integer index) {
		try {
			AbstractRedirectorController.propagateOriginalURL(request);

			this.checkRequestValidity(request, identifier);

			request.setAttribute(DisplayVesselByIDRedirectorConstants.VRMF_VESSELS_REDIRECT_BY_ID_GROUPING_REQUEST_ATTRIBUTE, byUID ? "UID" : "ID");
			request.setAttribute(DisplayVesselByIDRedirectorConstants.VRMF_VESSELS_REDIRECT_BY_ID_CRITERIA_REQUEST_ATTRIBUTE, new NameValuePair(identifierType, identifier));

			String[] availableSources = this.getAvailableSources(ServletsHelper.getLoggedUser(request), request, DONT_INCLUDE_OTHER_SOURCES_BY_DEFAULT);
			String[] effectiveSources = this.getSources(ServletsHelper.getLoggedUser(request), request, NO_REQUESTED_SOURCES, INCLUDE_OTHER_SOURCES_BY_DEFAULT);

			List<VesselRecord> vessels = this.doRetrieveVesselsByIdentifier(this._searchVesselsDAO, dataCustomizer.getTargetTable(request), byUID, identifierType, identifier, effectiveSources);
 
			if(vessels != null && !vessels.isEmpty()) {
				this.doRedirectToVesselByIdentifier(request, response, byUID, identifierType, identifier, vessels, effectiveSources, index);
			} else {
				this.sendResponseError(response, HttpStatus.NOT_FOUND, "No data available for vessel with " + identifierType + " " + identifier + " grouped by " + ( byUID ? "UID" : "ID" ) + " ( available sources: " + CollectionsHelper.serializeArray(availableSources) + " )");
			}
		} catch(NoLoggedUserException NLe) {
			this.sendResponseError(response, HttpStatus.FORBIDDEN, NLe);
		} catch(Throwable t) {
			this.sendResponseError(response, HttpStatus.INTERNAL_SERVER_ERROR, t);
		}
	}

	protected void doRedirect(HttpServletRequest request, HttpServletResponse response, boolean byUID, Integer ID, Boolean reload) {
		String[] effectiveSources = null;
		String[] availableSources = null;

		AbstractRedirectorController.propagateOriginalURL(request);

		try {
			availableSources = this.getAvailableSources(ServletsHelper.getLoggedUser(request), request, DONT_INCLUDE_OTHER_SOURCES_BY_DEFAULT);
			effectiveSources = this.getSources(ServletsHelper.getLoggedUser(request), request, NO_REQUESTED_SOURCES, INCLUDE_OTHER_SOURCES_BY_DEFAULT);

			FullVessel vessel = this.fullVesselsDAO.localize(this.doRetrieveVesselByID(dataCustomizer.getTargetTable(request), byUID, ID, effectiveSources), (String)request.getAttribute(RequestAttributesConstants.CURRENT_LANGUAGE_REQUEST_ATTRIBUTE));

			if(vessel != null) {
				if(!RELOAD.equals(reload)) {
					String vesselAsJSON = ( vessel == null ? null : new JSONSerializer().exclude(BASIC_JSON_EXCLUSION_PATTERNS).deepSerialize(vessel) );

					request.setAttribute(DisplayVesselByIDRedirectorConstants.VRMF_VESSEL_DATA_REQUEST_ATTRIBUTE, vessel);
					request.setAttribute(DisplayVesselByIDRedirectorConstants.VRMF_SERIALIZED_VESSEL_DATA_REQUEST_ATTRIBUTE, vesselAsJSON);
				}

				request.setAttribute(DisplayVesselByIDRedirectorConstants.VRMF_GROUP_BY_UID_REQUEST_ATTRIBUTE, byUID);

				request.getRequestDispatcher("/" + DisplayVesselByIDRedirectorConstants.DISPLAY_PAGE + "?" + DisplayVesselByIDRedirectorConstants.ID_REQUEST_PARAMETER + "=" + ID).forward(request, response);
			} else {
				if(byUID) {
					VesselsHistoryExample filter = new VesselsHistoryExample();
					VesselsHistoryExample.Criteria criteria = filter.createCriteria();
					criteria.andVesselUidEqualTo(ID);

					if(effectiveSources != null)
						criteria.andSourceSystemIn(Arrays.asList(effectiveSources));

					filter.setOrderByClause("UTIME DESC");

					List<VesselsHistory> previousVessels = this.vesselsHistoryDAO.selectByExample(filter);

					if(previousVessels != null && !previousVessels.isEmpty()) {
						ListSet<Integer> IDs = new ListSet<Integer>();

						for(VesselsHistory history : previousVessels)
							IDs.add(history.getVesselId());

						//Multiple vessels can be identified as having the provided (stale) UID in his history...
						//Redirecting to the most recent new UID...
						final int numIDs = IDs.size();
						if(numIDs >= 1) {
							Integer vesselID = IDs.get(0);

							Vessels currentVessel = this.vesselsDAO.selectByPrimaryKey(vesselID);

							request.setAttribute(DisplayVesselByIDRedirectorConstants.VRMF_STALE_UID_REQUEST_ATTRIBUTE, this.padID(ID));
							request.setAttribute(DisplayVesselByIDRedirectorConstants.VRMF_NEW_UID_REQUEST_ATTRIBUTE, this.padID(currentVessel.getUid()));

							response.setStatus(HttpStatus.MOVED_PERMANENTLY.value());
							request.getRequestDispatcher("/" + DisplayVesselByIDRedirectorConstants.REDIRECT_TO_NEW_UID_PAGE).forward(request, response);

							if(numIDs > 1)
								this._log.warn(numIDs + " historical records were identified for UID #" + ID + ". Identified vessel IDs are: " + IDs + ". Redirection will be performed to the UID for the first (most recent) ID (" + vesselID + " that is UID #" + currentVessel.getUid() + ")...");

							return;
						}
						//What to do if (ever) a given, stale UID maps onto multiple vessels?
					}
				}

				this.sendResponseError(response, HttpStatus.NOT_FOUND, "No data available for vessel with " + ( byUID ? "UID" : "ID" ) + " #" + this.padID(ID) + " ( available sources: " + CollectionsHelper.serializeArray(availableSources) + " )");
			}
		} catch(NoLoggedUserException NLe) {
			this.sendResponseError(response, HttpStatus.FORBIDDEN, NLe);
		} catch(Throwable t) {
			this.sendResponseError(response, HttpStatus.INTERNAL_SERVER_ERROR, t);
		}
	}

	private void doRedirectToVesselByIdentifier(HttpServletRequest request, HttpServletResponse response, boolean byUID, String identifierType, String identifier, List<VesselRecord> vessels, String[] sources, Integer index) throws Throwable {
		AbstractRedirectorController.propagateOriginalURL(request);

		if(index != null && index > vessels.size()) {
			this.sendResponseError(response, HttpStatus.BAD_REQUEST);
		} else if(vessels.size() == 1 || index != null) {
			int vesselIndex = vessels.size() == 1 ? 0 : index - 1;

			Integer ID = byUID ? vessels.get(vesselIndex).getUid() : vessels.get(0).getId();

			FullVessel vessel = this.fullVesselsDAO.localize(this.doRetrieveVesselByID(dataCustomizer.getTargetTable(request), byUID, ID, sources), (String)request.getAttribute(RequestAttributesConstants.CURRENT_LANGUAGE_REQUEST_ATTRIBUTE));

			String vesselAsJSON = ( vessel == null ? null : new JSONSerializer().exclude(BASIC_JSON_EXCLUSION_PATTERNS).deepSerialize(vessel) );

			request.setAttribute(DisplayVesselByIDRedirectorConstants.VRMF_VESSEL_DATA_REQUEST_ATTRIBUTE, vessel);
			request.setAttribute(DisplayVesselByIDRedirectorConstants.VRMF_SERIALIZED_VESSEL_DATA_REQUEST_ATTRIBUTE, vesselAsJSON);

			request.setAttribute(DisplayVesselByIDRedirectorConstants.VRMF_GROUP_BY_UID_REQUEST_ATTRIBUTE, byUID);

			request.getRequestDispatcher("/" + DisplayVesselByIDRedirectorConstants.DISPLAY_PAGE + "?" + DisplayVesselByIDRedirectorConstants.ID_REQUEST_PARAMETER + "=" + ID).forward(request, response);
		} else {
			Collection<NameValuePair> alternatives = new ArrayList<NameValuePair>();
			Collection<String> alternates = new ArrayList<String>();

			int counter = 1;
			for(VesselRecord vessel : vessels) {
				alternatives.add(new NameValuePair(this.doGetBaseURL(request, DONT_INCLUDE_SERVER_AND_PROTOCOL_IN_BASE_URL, INCLUDE_CONTEXT_IN_BASE_URL) + "/!/display/vessel/" + ( byUID ? "" : "ID/" ) + identifierType + "/" + URLEncoder.encode(identifier, "UTF-8") + "/" + counter++, vessel));
			}

			request.setAttribute(DisplayVesselByIDRedirectorConstants.VRMF_MULTIPLE_VESSELS_DATA_REQUEST_ATTRIBUTE, alternatives);

			for(NameValuePair alternateURL : alternatives) {
				alternates.add("{\"" + alternateURL.getName() + "\" 1.0 {type text/html}}");
			}

			String alternatesHeader = CollectionsHelper.join(alternates, ",");
			alternatesHeader = ( alternatesHeader == null ? null : alternatesHeader.substring(0, Math.min(alternatesHeader.length(), 6 * 1024))); //Truncates header value to 6KB (to stay under common HTTP server limitations)

			response.setHeader("TCN", "list");
			response.setHeader("Alternates", alternatesHeader);
			response.setHeader("Vary", "negotiate, accept");

			this.sendResponseError(response, HttpStatus.MULTIPLE_CHOICES);
		}
	}

	private void checkRequestValidity(HttpServletRequest request, String identifier) throws Throwable {
		if(identifier != null && ( identifier.indexOf("%") >= 0 || identifier.indexOf("_") >= 0))
			this.requireLoggedUser(request);
	}
}