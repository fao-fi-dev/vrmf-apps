/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder)
 */
package org.fao.fi.vrmf.web.apps.finder.filters;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.fao.fi.vrmf.business.dao.UserManagementDAO;
import org.fao.fi.vrmf.common.j2ee.RequestAttributesConstants;
import org.fao.fi.vrmf.common.j2ee.filters.AbstractFilter;
import org.fao.fi.vrmf.common.j2ee.utilities.ServletsHelper;
import org.fao.fi.vrmf.common.models.security.VRMFUser;
import org.fao.fi.vrmf.web.apps.finder.controllers.services.pub.redirect.AbstractRedirectorController;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 Mar 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 Mar 2012
 */
@Named
@Singleton
public class GuestUserLoginFilter extends AbstractFilter {
	private String _username = null;
	private String _password = null;

	@Inject private UserManagementDAO _userManagementDAO;

	/**
	 * @return the 'username' value
	 */
	public String getUsername() {
		return this._username;
	}

	/**
	 * @param username the 'username' value to set
	 */
	public void setUsername(String username) {
		this._username = username;
	}

	/**
	 * @return the 'password' value
	 */
	public String getPassword() {
		return this._password;
	}

	/**
	 * @param password the 'password' value to set
	 */
	public void setPassword(String password) {
		this._password = password;
	}

	/**
	 * @return the 'userManagementDAO' value
	 */
	public UserManagementDAO getUserManagementDAO() {
		return this._userManagementDAO;
	}

	/**
	 * @param userManagementDAO the 'userManagementDAO' value to set
	 */
	public void setUserManagementDAO(UserManagementDAO userManagementDAO) {
		this._userManagementDAO = userManagementDAO;
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		super.init(filterConfig);

		if(this._username == null)
			this._username = this.getConfigurationParameter("USER_NAME");

		if(this._password == null)
			this._password = this.getConfigurationParameter("PASSWORD");
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void internalDoFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		this._log.info("Executing " + this.getFilterName() + " internalDoFilter...");

//		super.doFilter(request, response, chain);

		HttpServletRequest hRequest = (HttpServletRequest)request;

		AbstractRedirectorController.propagateOriginalURL(hRequest);

		HttpSession session = hRequest.getSession(false);

		this._log.info("Session ID: " + ( session == null ? "NULL" : session.getId() ));

		this._log.debug("Intercepted URI: " + hRequest.getRequestURI());
		this._log.debug("Intercepted URL: " + hRequest.getRequestURL());
		this._log.debug("Context: " + hRequest.getContextPath());

		boolean alreadyProcessedByAutomaticSourceLoginFilter = hRequest.getAttribute(RequestAttributesConstants.SOURCE_SPECIFIC_SOURCE_REQUEST_ATTRIBUTE) != null;

		VRMFUser loggedUser = ServletsHelper.getLoggedUser(((HttpServletRequest)request).getSession(true));

		if(loggedUser == null && !Boolean.TRUE.equals(alreadyProcessedByAutomaticSourceLoginFilter)) {
			ServletsHelper.setLoggedUser((HttpServletRequest)request, this.guestLogin());
		}

		chain.doFilter(request, response);

		this._log.debug("Request has been filtered via " + this.getClass().getSimpleName());
	}

	private VRMFUser guestLogin() {
		VRMFUser guest = null;

		try {
			this._log.debug("Logging in guest user with credentials " + this._username + " / " + this._password);

			guest = this._userManagementDAO.login(this._username, this._password);

			this._log.debug("Successfully logged in guest user with credentials " + this._username + " / " + this._password);
		} catch(Throwable t) {
			this._log.error("Unable to perform guest login", t);
		}

		return guest;
	}
}