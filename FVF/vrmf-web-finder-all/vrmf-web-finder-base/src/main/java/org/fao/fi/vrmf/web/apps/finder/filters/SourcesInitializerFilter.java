/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder)
 */
package org.fao.fi.vrmf.web.apps.finder.filters;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.vrmf.business.dao.CommonMetadataDAO;
import org.fao.fi.vrmf.business.dao.UserManagementDAO;
import org.fao.fi.vrmf.business.dao.generated.SSystemsDAO;
import org.fao.fi.vrmf.common.j2ee.RequestAttributesConstants;
import org.fao.fi.vrmf.common.j2ee.utilities.ServletsHelper;
import org.fao.fi.vrmf.common.models.generated.SSystems;
import org.fao.fi.vrmf.common.models.generated.SSystemsExample;
import org.fao.fi.vrmf.common.models.security.BasicUser;
import org.fao.fi.vrmf.web.apps.finder.controllers.services.pub.redirect.AbstractRedirectorController;
import org.springframework.beans.factory.InitializingBean;

import edu.emory.mathcs.backport.java.util.Collections;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 Mar 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 Mar 2012
 */
@Named
@Singleton
public class SourcesInitializerFilter extends AbstractNavigationFilter implements InitializingBean {
	@Inject private SSystemsDAO _systemsDAO;
	@Inject private CommonMetadataDAO _metadataDAO;
	@Inject private UserManagementDAO _userManagementDAO;

	private final Map<String, SSystems> _dataSources = new HashMap<String, SSystems>();
	private final Map<String, SSystems> _otherSources = new HashMap<String, SSystems>();

	private final Map<String, Pattern> _interceptorPatterns = new HashMap<String, Pattern>();

	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		this._log.info("Initializing public sources list...");

		SSystemsExample filter = new SSystemsExample();
		filter.createCriteria();

		for(SSystems system : this._systemsDAO.selectByExample(filter)) {
			if(system.getVesselSource())
				this._dataSources.put(system.getId(), system);
			else
				this._otherSources.put(system.getId(), system);
		}

		this._log.info(this._dataSources.size() + " data sources and " + this._otherSources.size() + " other sources have been added to the public sources list");
	}

	protected Pattern getInterceptorPatternForSources(String... sources) {
		if(sources == null || sources.length == 0)
			return null;

		List<String> sanitizedSources = new ListSet<String>();
		String sanitized;
		for(String source : sources) {
			if((sanitized = StringsHelper.trim(source)) != null)
				if(this._dataSources.containsKey(sanitized))
					sanitizedSources.add(sanitized);
		}

		if(sanitizedSources.isEmpty())
			return null;

		Collections.sort(sanitizedSources);
		String key = CollectionsHelper.join(sanitizedSources, "|");

		if(this._interceptorPatterns.containsKey(key))
			return this._interceptorPatterns.get(key);

		Pattern pattern = Pattern.compile("^(/?)(" + key + ")(/?.*)$");

		this._interceptorPatterns.put(key,  pattern);

		return pattern;
	}

	/**
	 * @return the 'systemsDAO' value
	 */
	public SSystemsDAO getSystemsDAO() {
		return this._systemsDAO;
	}

	/**
	 * @param systemsDAO the 'systemsDAO' value to set
	 */
	public void setSystemsDAO(SSystemsDAO systemsDAO) {
		this._systemsDAO = systemsDAO;
	}

	/**
	 * @return the 'metadataDAO' value
	 */
	public CommonMetadataDAO getMetadataDAO() {
		return this._metadataDAO;
	}

	/**
	 * @param metadataDAO the 'metadataDAO' value to set
	 */
	public void setMetadataDAO(CommonMetadataDAO metadataDAO) {
		this._metadataDAO = metadataDAO;
	}

	/**
	 * @return the 'userManagementDAO' value
	 */
	public UserManagementDAO getUserManagementDAO() {
		return this._userManagementDAO;
	}

	/**
	 * @param userManagementDAO the 'userManagementDAO' value to set
	 */
	public void setUserManagementDAO(UserManagementDAO userManagementDAO) {
		this._userManagementDAO = userManagementDAO;
	}

	protected Matcher getURLParser(String[] sources, String URL) {
		String sanitized = StringsHelper.trim(URL);
		Pattern pattern = this.getInterceptorPatternForSources(sources);

		return sanitized == null || pattern == null ? null : pattern.matcher(sanitized);
	}

	protected BasicUser filterBySource(HttpServletRequest request, String sourceID, BasicUser loggedUser) throws Throwable {
		long end, start = System.currentTimeMillis();

		this._log.info(this.getFilterName() + " filter by source...");

		SSystems source = this._dataSources.get(sourceID);

		Boolean isPublic = source.getIsPublic();

		String sourceSpecificUserID = ( isPublic ? "PUBLIC" : "PRIVATE" ) + "_" + sourceID;

		BasicUser currentUser = null;

		if(isPublic) {
			currentUser = this._userManagementDAO.getAllUserDetails(sourceSpecificUserID);
		} else if(loggedUser != null) {
			Set<String> availableSources = new HashSet<String>();
			availableSources.addAll(Arrays.asList(ServletsHelper.getAvailableDataSources(loggedUser)));

			if(availableSources.contains(sourceID))
				currentUser = this._userManagementDAO.getAllUserDetails(sourceSpecificUserID);
		}

		boolean userIdentified = currentUser != null;

		request.setAttribute(RequestAttributesConstants.SOURCE_SPECIFIC_AUTO_LOGIN_REQUEST_ATTRIBUTE, userIdentified);
		request.setAttribute(RequestAttributesConstants.SOURCE_SPECIFIC_SOURCE_REQUEST_ATTRIBUTE, userIdentified ? source : null);
		request.setAttribute(RequestAttributesConstants.SOURCE_SPECIFIC_SOURCE_ID_REQUEST_ATTRIBUTE, userIdentified ? source.getId() : null);
		request.setAttribute(RequestAttributesConstants.AVAILABLE_DATA_SOURCES_REQUEST_ATTRIBUTE, userIdentified ? ServletsHelper.getAvailableDataSources(currentUser) : new String[0]);
		request.setAttribute(RequestAttributesConstants.AVAILABLE_DATA_SOURCES_STATS_REQUEST_ATTRIBUTE, this._metadataDAO.getStatusReportBySources(userIdentified ? ServletsHelper.getAvailableDataSources(currentUser) : null, "VESSELS"));
		request.setAttribute(RequestAttributesConstants.AVAILABLE_OTHER_SOURCES_REQUEST_ATTRIBUTE, userIdentified ? ServletsHelper.getAvailableOtherSources(currentUser) : new String[0]);
		request.setAttribute(RequestAttributesConstants.DATA_SOURCES_REQUEST_ATTRIBUTE, this.getDataSources(userIdentified ? ServletsHelper.getAvailableDataSources(currentUser) : new String[0]));

		end = System.currentTimeMillis();

		this._log.info(this.getClass().getSimpleName() + " filter by source took " + ( end - start ) + " mSec. to execute");

		return currentUser;
	}

	protected BasicUser filterByNoSource(HttpServletRequest request) throws Throwable {
		long end, start = System.currentTimeMillis();

		this._log.info(this.getClass().getSimpleName() + " filter by no source...");

		BasicUser publicGuest = this._userManagementDAO.getAllUserDetails("PUBLIC_GUEST");
		BasicUser loggedUser = ServletsHelper.getLoggedUser(request);

		BasicUser toKeep = loggedUser == null ? publicGuest : loggedUser;

		request.setAttribute(RequestAttributesConstants.SOURCE_SPECIFIC_AUTO_LOGIN_REQUEST_ATTRIBUTE, Boolean.FALSE);
		request.setAttribute(RequestAttributesConstants.SOURCE_SPECIFIC_SOURCE_REQUEST_ATTRIBUTE, null);
		request.setAttribute(RequestAttributesConstants.SOURCE_SPECIFIC_SOURCE_ID_REQUEST_ATTRIBUTE, null);
		request.setAttribute(RequestAttributesConstants.AVAILABLE_DATA_SOURCES_REQUEST_ATTRIBUTE, ServletsHelper.getAvailableDataSources(toKeep));
		request.setAttribute(RequestAttributesConstants.AVAILABLE_DATA_SOURCES_STATS_REQUEST_ATTRIBUTE, this._metadataDAO.getStatusReportBySources(ServletsHelper.getAvailableDataSources(toKeep), "VESSELS"));
		request.setAttribute(RequestAttributesConstants.AVAILABLE_OTHER_SOURCES_REQUEST_ATTRIBUTE, ServletsHelper.getAvailableOtherSources(toKeep));
		request.setAttribute(RequestAttributesConstants.DATA_SOURCES_REQUEST_ATTRIBUTE, this.getDataSources(ServletsHelper.getAvailableDataSources(toKeep)));

		end = System.currentTimeMillis();

		this._log.info(this.getClass().getSimpleName() + " filter by no source took " + ( end - start ) + " mSec. to execute");

		return publicGuest;
	}

	private Collection<SSystems> getDataSources(String[] availableDataSources) {
		Collection<SSystems> available = new ListSet<SSystems>();

		Collection<String> sourceIDs = new TreeSet<String>(Arrays.asList(availableDataSources));

		for(String source : sourceIDs) {
			if(this._dataSources.containsKey(source))
				available.add(this._dataSources.get(source));
		}

		return available;
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	protected void internalDoFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		try {
			this._log.info("Executing " + this.getFilterName() + " internalDoFilter...");

			long end, start = System.currentTimeMillis();

			HttpServletRequest hRequest = (HttpServletRequest)request;

			AbstractRedirectorController.propagateOriginalURL(hRequest);

			HttpSession session = hRequest.getSession(true);

			BasicUser sourceSpecificUser = null;
			BasicUser loggedUser = ServletsHelper.getLoggedUser(hRequest);

			if(loggedUser == null)
				loggedUser = this._userManagementDAO.getAllUserDetails("PUBLIC_GUEST");

			String context = hRequest.getContextPath() + "/";

			String URL = hRequest.getRequestURL().toString();
			String URI = this.stripContext(context, hRequest.getRequestURI());

			this._log.debug("Logged user: " + ( loggedUser == null ? "NULL" : loggedUser.getId() ));
			this._log.debug("Session ID : " + ( session == null ? "NULL" : session.getId() ));

			this._log.debug("Intercepted URI: " + hRequest.getRequestURI());
			this._log.debug("Intercepted URL: " + URL);

			this._log.debug("Context: " + hRequest.getContextPath());

			String interceptedSource = null;
	//		SSystems currentSource;

	//		for(String source : this._dataSources.keySet()) {
	//			currentSource = this._dataSources.get(source);
	//
	//			if(Boolean.TRUE.equals(currentSource.getVesselSource()))
	//				if(URL.indexOf(context + source) >= 0) {
	//					selectedSource = source;
	//
	//					updatedURL = URL.replaceFirst(source + "\\/?", "").substring(URL.indexOf(context) + context.length());
	//
	//					break;
	//				}
	//		}

			Matcher parser = this.getURLParser(ServletsHelper.getAvailableDataSources(loggedUser), URI);

			if(parser != null && parser.matches()) {
				interceptedSource = parser.group(2);

				URI = parser.replaceAll("$1$3");

				if(URI.startsWith("/"))
					URI = URI.substring(1);
			}

			boolean isASourceEntryPage = false;
			boolean isSourceAccessibleForUser = false;

			if(interceptedSource != null) {
				isSourceAccessibleForUser = true;
			}

			isASourceEntryPage = isSourceAccessibleForUser && ( "".equals(URI) || SEARCH_PAGE.equals(URI));

			if(isASourceEntryPage) {
				this._log.debug("Setting source specific entrance page attribute for " + interceptedSource);

				hRequest.setAttribute(RequestAttributesConstants.SOURCE_SPECIFIC_ENTRANCE_PAGE_REQUEST_ATTRIBUTE, Boolean.TRUE);
			}

			if(isSourceAccessibleForUser) {
				this._log.debug("Requesting filtering by source " + interceptedSource);
			}

			try {
				sourceSpecificUser = interceptedSource == null ? this.filterByNoSource(hRequest) : this.filterBySource(hRequest, interceptedSource, loggedUser);
			} catch (Throwable t) {
				this._log.error("Unable to retrieve details for source-specific user", t);
			}

			if(sourceSpecificUser != null && isSourceAccessibleForUser) {
				this._log.debug("Forwarding request for " + URL + " to /" + URI);

				//hRequest.setAttribute(RequestAttributesConstants.ORIGINAL_URL_REQUEST_ATTRIBUTE, URL);
				hRequest.getRequestDispatcher("/" + URI).forward(hRequest, response);
			} else {
				this._log.debug("Propagating filter " + this.getClass().getSimpleName() + " through filter chain...");

				chain.doFilter(hRequest, response);
			}

			this._log.debug("Request has been filtered via " + this.getClass().getSimpleName());

			end = System.currentTimeMillis();

			this._log.debug(this.getClass().getSimpleName() + " took " + ( end - start ) + " mSec. to execute");
		} catch (Throwable t) {
			this._log.error("Unable to complete " + this.getFilterName() + " internalDoFilter execution", t);

			chain.doFilter(request, response);
		}
	}
}