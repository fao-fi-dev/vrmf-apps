/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder)
 */
package org.fao.fi.vrmf.web.apps.finder.controllers.services.pub.redirect;

import org.fao.fi.vrmf.common.web.controllers.services.CommonServicesConstants;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 29 Oct 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 29 Oct 2012
 */
public interface RedirectorsConstants {
	String MAPPING_PREFIX = CommonServicesConstants.MAPPING_PREFIX;
	String DISPLAY_MAPPING_PREFIX = MAPPING_PREFIX + "display/";

	String REQUESTED_URI_REQUEST_ATTRIBUTE_CONSTANT = "VRMF_REQUESTED_URI";
}
