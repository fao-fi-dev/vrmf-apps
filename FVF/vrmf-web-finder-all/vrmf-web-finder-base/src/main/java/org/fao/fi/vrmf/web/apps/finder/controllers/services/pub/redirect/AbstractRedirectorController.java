/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder)
 */
package org.fao.fi.vrmf.web.apps.finder.controllers.services.pub.redirect;

import javax.servlet.http.HttpServletRequest;

import org.fao.fi.vrmf.common.web.controllers.AbstractController;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 09/apr/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 09/apr/2013
 */
abstract public class AbstractRedirectorController extends AbstractController {
	static public void propagateOriginalURL(HttpServletRequest request) {
		Object current = request.getAttribute(RedirectorsConstants.REQUESTED_URI_REQUEST_ATTRIBUTE_CONSTANT);

		if(current == null) {
			request.setAttribute(RedirectorsConstants.REQUESTED_URI_REQUEST_ATTRIBUTE_CONSTANT,
								 request.getRequestURI());
		}
	}
}
