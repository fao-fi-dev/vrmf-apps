/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder)
 */
package org.fao.fi.vrmf.web.apps.finder.controllers.services.pub.redirect;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.fao.fi.vrmf.business.dao.UserManagementDAO;
import org.fao.fi.vrmf.business.dao.generated.SSystemsDAO;
import org.fao.fi.vrmf.common.j2ee.RequestAttributesConstants;
import org.fao.fi.vrmf.common.j2ee.utilities.ServletsHelper;
import org.fao.fi.vrmf.common.models.generated.SSystems;
import org.fao.fi.vrmf.common.models.generated.SSystemsExample;
import org.fao.fi.vrmf.common.models.security.BasicUser;
import org.fao.fi.vrmf.common.web.controllers.AbstractController;
import org.fao.fi.vrmf.web.apps.finder.filters.SourcesInitializerFilter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 Mar 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 Mar 2012
 * @deprecated see: {@link SourcesInitializerFilter}
 */
public class AutomaticSourceLoginController extends AbstractController implements InitializingBean {
	static final String INDEX_PAGE = "index.jsp";
	static final String SEARCH_PATH = "search";
	static final String SEARCH_PAGE = SEARCH_PATH + ".jsp";
	
	@Inject
	private SSystemsDAO _systemsDAO ;
	
	@Inject
	private UserManagementDAO _userManagementDAO;

	private Set<String> _publicSources = new HashSet<String>();
	
	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		this._log.info("Initializing public sources list...");
		
		SSystemsExample filter = new SSystemsExample();
		filter.createCriteria().andVesselSourceEqualTo(Boolean.TRUE).andIsPublicEqualTo(Boolean.TRUE);
		
		for(SSystems system : this._systemsDAO.selectByExample(filter))
			this._publicSources.add(system.getId());
		
		this._log.info(this._publicSources.size() + " sources have been added to the public sources list");
	}

	/**
	 * @return the 'userManagementDAO' value
	 */
	public UserManagementDAO getUserManagementDAO() {
		return this._userManagementDAO;
	}

	/**
	 * @param userManagementDAO the 'userManagementDAO' value to set
	 */
	public void setUserManagementDAO(UserManagementDAO userManagementDAO) {
		this._userManagementDAO = userManagementDAO;
	}

	@RequestMapping("{source:^(?!" + SEARCH_PATH + ")[A-Z0-9]{,16}}")
	public void genericFilter(@PathVariable String source, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		this.doFilter(source, null, INDEX_PAGE, request, response);
	}

	@RequestMapping("{source}/{URL}")
	public void genericFilter(@PathVariable String source, @PathVariable String URL, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		this.doFilter(source, URL, SEARCH_PATH.equals(URL) ? SEARCH_PATH : null, request, response);
	}

	private void doFilter(@PathVariable String source, @PathVariable String URL, String page, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		this._log.info("Filtering request via " + this.getClass().getSimpleName());
		
		this._log.info("Intercepted URI: " + request.getRequestURI());
		this._log.info("Intercepted URL: " + request.getRequestURL());
		this._log.info("Context: " + request.getContextPath());
				
		String selectedSource = null;
		
		for(String publicSource : this._publicSources) {
			if(publicSource.equals(source)) {
				selectedSource = publicSource;
				
				break;
			}
		}
		
		if(URL == null || 
		   INDEX_PAGE.equals(URL) ||
		   SEARCH_PAGE.equals(URL) ||
		   SEARCH_PATH.equals(URL)) {
			this._log.info("Setting source specific entrance page attribute for " + selectedSource);
			
			request.setAttribute(RequestAttributesConstants.SOURCE_SPECIFIC_ENTRANCE_PAGE_REQUEST_ATTRIBUTE, Boolean.TRUE);
		}
		
		BasicUser loggedUser = ServletsHelper.getLoggedUser(request);
		
		if(selectedSource != null) {
			this._log.info("Requesting filtering by source " + selectedSource);
			
			request.setAttribute(RequestAttributesConstants.SOURCE_SPECIFIC_SOURCE_REQUEST_ATTRIBUTE, selectedSource);
			
			try {
				String sourceSpecificUserID = "PUBLIC_" + selectedSource;
				
				if(loggedUser == null || !sourceSpecificUserID.equals(loggedUser.getId())) {
					ServletsHelper.setLoggedUser(request, this._userManagementDAO.getAllUserDetails("PUBLIC_" + selectedSource));
				}
				
				request.setAttribute(RequestAttributesConstants.SOURCE_SPECIFIC_AUTO_LOGIN_REQUEST_ATTRIBUTE, Boolean.TRUE);
			} catch (Throwable t) {
				this._log.error("Unable to log-in specific user for source " + selectedSource, t);
			}
			
			request.getRequestDispatcher("/" + ( URL == null ? page : URL )).forward(request, response);
//			hResponse.sendRedirect(updatedURL);
		} else {
			request.setAttribute(RequestAttributesConstants.SOURCE_SPECIFIC_AUTO_LOGIN_REQUEST_ATTRIBUTE, Boolean.FALSE);
		}
		
		this._log.info("Request has been filtered via " + this.getClass().getSimpleName());
	}
}