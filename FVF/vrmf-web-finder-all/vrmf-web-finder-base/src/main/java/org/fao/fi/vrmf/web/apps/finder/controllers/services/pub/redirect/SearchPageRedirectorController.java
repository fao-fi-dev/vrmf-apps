/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder)
 */
package org.fao.fi.vrmf.web.apps.finder.controllers.services.pub.redirect;

import java.io.IOException;

import javax.inject.Singleton;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.fao.fi.vrmf.common.web.controllers.AbstractController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 Mar 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 Mar 2012
 */
@Singleton @Controller
@RequestMapping(RedirectorsConstants.MAPPING_PREFIX)
public class SearchPageRedirectorController extends AbstractController {
	static final String SEARCH_PATH = "search";
	static final String SEARCH_PAGE = SEARCH_PATH + ".jsp";

	public void genericFilter(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		try {
			String URL = this.doGetBaseURL(request) + "/" + SEARCH_PAGE;
			this._log.info("Redirecting " + request.getRequestURI() + " to " + URL);
			response.sendRedirect(URL);
		} catch (Throwable t) {
			this.sendResponseError(response, HttpStatus.INTERNAL_SERVER_ERROR, t);
		}
	}
}