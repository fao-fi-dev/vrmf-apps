/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder)
 */
package org.fao.fi.vrmf.web.apps.finder.controllers.services.pub.vessels.stats;

import java.net.URI;
import java.net.URL;
import java.security.acl.NotOwnerException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;

import org.fao.fi.sh.utility.common.helpers.beans.text.XMLPrettyPrinter;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.sh.utility.model.NameValuePair;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.sh.utility.model.extensions.collections.SerializableList;
import org.fao.fi.sh.utility.model.extensions.collections.impl.SerializableArrayList;
import org.fao.fi.sh.utility.model.extensions.maps.SerializableMap;
import org.fao.fi.sh.utility.model.extensions.maps.impl.SerializableHashMap;
import org.fao.fi.vrmf.business.dao.generated.SCountriesDAO;
import org.fao.fi.vrmf.business.dao.generated.SVesselTypesDAO;
import org.fao.fi.vrmf.common.data.exchange.core.models.JSONResponse;
import org.fao.fi.vrmf.common.models.generated.SCountries;
import org.fao.fi.vrmf.common.models.generated.SCountriesExample;
import org.fao.fi.vrmf.common.models.generated.SVesselTypes;
import org.fao.fi.vrmf.common.models.generated.SVesselTypesExample;
import org.fao.fi.vrmf.common.models.stats.BasicVesselStatsReport;
import org.fao.fi.vrmf.common.models.stats.FlagsStatsData;
import org.fao.fi.vrmf.common.models.stats.LengthsStatsData;
import org.fao.fi.vrmf.common.models.stats.StatusReportBySource;
import org.fao.fi.vrmf.common.models.stats.TypesStatsData;
import org.fao.fi.vrmf.common.web.DataCustomizer;
import org.fao.fi.vrmf.common.web.controllers.AbstractController;
import org.fao.fi.vrmf.web.apps.finder.business.helpers.LiveStatsRetriever;
import org.fao.fi.vrmf.web.apps.finder.controllers.services.pub.PublicServicesConstants;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 24 Aug 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 24 Aug 2012
 */
@Named @Singleton @Controller
@RequestMapping(PublicServicesConstants.VESSELS_SEARCH_SERVICES_PREFIX + "stats/*")
public class LiveStatsController extends AbstractController implements InitializingBean {
	static protected Integer[] ALL_COUNTRY_IDS = null;
	static protected Integer[] ALL_VESSEL_TYPE_IDS = null;
	
	@Inject private DataCustomizer _dataCustomizer;
	@Inject private SCountriesDAO _countriesDAO;
	@Inject private SVesselTypesDAO _typesDAO;
	@Inject private LiveStatsRetriever _retriever;
	
	private Map<Integer, SCountries> _countriesMap = new HashMap<Integer, SCountries>();
	private Map<Integer, SVesselTypes> _typesMap = new HashMap<Integer, SVesselTypes>();
	
	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		for(SCountries country : this._countriesDAO.selectByExample(new SCountriesExample())) {
			this._countriesMap.put(country.getId(), country);
		}
		
		for(SVesselTypes type : this._typesDAO.selectByExample(new SVesselTypesExample())) {
			this._typesMap.put(type.getId(), type);
		}
	}

	/**
	 * @return the 'countriesDAO' value
	 */
	public SCountriesDAO getCountriesDAO() {
		return this._countriesDAO;
	}

	/**
	 * @param countriesDAO the 'countriesDAO' value to set
	 */
	public void setCountriesDAO(SCountriesDAO countriesDAO) {
		this._countriesDAO = countriesDAO;
	}

	/**
	 * @return the 'typesDAO' value
	 */
	public SVesselTypesDAO getTypesDAO() {
		return this._typesDAO;
	}

	/**
	 * @param typesDAO the 'typesDAO' value to set
	 */
	public void setTypesDAO(SVesselTypesDAO typesDAO) {
		this._typesDAO = typesDAO;
	}

	/**
	 * @return the 'retriever' value
	 */
	public LiveStatsRetriever getRetriever() {
		return this._retriever;
	}

	/**
	 * @param retriever the 'retriever' value to set
	 */
	public void setRetriever(LiveStatsRetriever retriever) {
		this._retriever = retriever;
	}

	private ResponseEntity<byte[]> commonResponse(byte[] body, String format) {
		HttpHeaders contentTypeHeaders = this.getContentTypeDispositionAndLengthHeaders(null, format, body == null ? 0 : body.length);
		HttpHeaders cachingHeaders = this.getCachingHeaders(3600);
				
		return new ResponseEntity<byte[]>(body, this.mergeHeaders(contentTypeHeaders, cachingHeaders), HttpStatus.OK);
	}
	
	private Integer[] getCountryIDs(String countries) throws Throwable {
		List<Integer> countryIDs = new ListSet<Integer>();
		List<String> countryIso2Codes = new ListSet<String>();
		
		if(countries != null) {
			for(String part : countries.split("\\,")) {
				part = StringsHelper.trim(part);
				
				if(part != null) {
					try {
						countryIDs.add(Integer.parseInt(part));
					} catch (NumberFormatException NFe) {
						countryIso2Codes.add(part);
					}
				}
			}

			if(!countryIso2Codes.isEmpty()) {
				SCountriesExample filter = new SCountriesExample();
				filter.createCriteria().andIso2CodeIn(countryIso2Codes);
				
				for(SCountries country : this._countriesDAO.selectByExample(filter)) {
					countryIDs.add(country.getId());
				}
			}
		}
		
		if(countryIDs.isEmpty())
			return null;
		
		return countryIDs.toArray(new Integer[countryIDs.size()]);
	}
	
	private Integer[] getTypeIDs(String types) throws Throwable {
		List<Integer> typeIDs = null;
		
		if(types != null) {
			typeIDs = new ListSet<Integer>();
			
			for(String part : types.split("\\,")) {
				typeIDs.add(Integer.parseInt(part));
			}
		}
		
		if(typeIDs == null)
			return null;
		
		return typeIDs.toArray(new Integer[typeIDs.size()]);
	}

	@RequestMapping(value="sources/{sources}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> getStats(HttpServletRequest request, @PathVariable String sources) {
		return this.doGetStats(request, sources, "json");
	}

	
	@RequestMapping(value="all.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> getAllStats(HttpServletRequest request, @PathVariable String format) {
		return this.doGetStats(request, null, format);
	}
	
	private ResponseEntity<byte[]> doGetStats(HttpServletRequest request, String sources, String format) {
		try {
			long end, start = System.currentTimeMillis();
			
			String[] availableSources = this.getAvailableSources(request);
			String[] requestedSources = sources == null ? null : sources.split("\\,");
			
			String[] effectiveSources = requestedSources == null ? availableSources : AbstractController.intersectSourceSystems(requestedSources, availableSources);
			
			if(effectiveSources.length == 0)
				throw new NotOwnerException();
			
			String effectiveFormat = format == null ? "json" : format;
			
			SerializableList<StatusReportBySource> sourceStats = this._retriever.retrieveSourcesData(effectiveSources, _dataCustomizer.getTargetTable(request));
			SerializableList<FlagsStatsData> flagStats = new SerializableArrayList<FlagsStatsData>(this._retriever.retrieveStatsByFlag(effectiveSources, _dataCustomizer.getTargetTable(request)));
			SerializableList<TypesStatsData> typeStats = new SerializableArrayList<TypesStatsData>(this._retriever.retrieveStatsByType(effectiveSources, ALL_COUNTRY_IDS, _dataCustomizer.getTargetTable(request)));
			SerializableList<LengthsStatsData> lengthStats = new SerializableArrayList<LengthsStatsData>(this._retriever.retrieveStatsByLength(effectiveSources, ALL_COUNTRY_IDS, ALL_VESSEL_TYPE_IDS, _dataCustomizer.getTargetTable(request)));
			
			end = System.currentTimeMillis();
			
			byte[] content = null;
			
			if("json".equals(format)) {
				SerializableMap<String, SerializableList<? extends BasicVesselStatsReport>> data = new SerializableHashMap<String, SerializableList<? extends BasicVesselStatsReport>>();
				data.put("sources", this._retriever.retrieveSourcesData(effectiveSources, _dataCustomizer.getTargetTable(request)));
				data.put("flags", flagStats);
				data.put("types", typeStats);
				data.put("lengths", lengthStats);
				
				JSONResponse<SerializableMap<String, SerializableList<? extends BasicVesselStatsReport>>> response = new JSONResponse<SerializableMap<String, SerializableList<? extends BasicVesselStatsReport>>>();
				response.setError(false);
				response.setElapsed(end - start);
				response.setData(data);
						
				content = response.JSONify().getBytes("UTF-8");
			} else {
				int totalVesselsBySource = 0;
				int totalVesselsByFlag = 0;
				int totalVesselsByType = 0;
				int totalVesselsByLength = 0;
				
				for(StatusReportBySource source : sourceStats)
					totalVesselsBySource += source.getGroupedVessels();
				
				for(FlagsStatsData flag : flagStats)
					totalVesselsByFlag += flag.getVessels();
				
				for(TypesStatsData type : typeStats)
					totalVesselsByType += type.getVessels();
				
				for(LengthsStatsData length : lengthStats)
					totalVesselsByLength += length.getVessels();
				
				XMLPrettyPrinter xml = new XMLPrettyPrinter();
				xml.setNamespace("vrmf");
				xml.setNamespaceURI(new URI("http://www.fao.org/fi/vrmf"));
				xml.setSchemaLocation(new URL("http://www.fao.org/figis/vrmf/static/xsd/stats.xsd"));
				xml.open("response");
					xml.open("sources",
							 new NameValuePair("num", sourceStats.size()),
							 new NameValuePair("totalVessels", totalVesselsBySource));
					int counter = 1;
					for(StatusReportBySource source : sourceStats) {
						xml.empty("source", 
								  new NameValuePair("item", counter++),
								  new NameValuePair("id", source.getSourceSystem()),
								  new NameValuePair("vessels", source.getGroupedVessels())
								 );
					}
					xml.close();
				
					xml.open("flags",
							 new NameValuePair("num", flagStats.size()),
							 new NameValuePair("totalVessels", totalVesselsByFlag));
					SCountries country;
					counter = 1;
					for(FlagsStatsData flag : flagStats) {
						country = this._countriesMap.get(flag.getCountryId());
						xml.empty("flag", 
								  new NameValuePair("item", counter++),
								  new NameValuePair("id", flag == null ? null : flag.getCountryId()),
								  new NameValuePair("figisId", flag == null ? null : flag.getCountryId()),
								  new NameValuePair("iso2Code", country == null ? null : country.getIso2Code()),
								  new NameValuePair("iso3Code", country == null ? null : country.getIso3Code()),
								  new NameValuePair("vessels", flag.getVessels())
								 );
					}
					xml.close();
					
					xml.open("types",
							 new NameValuePair("num", typeStats.size()),
							 new NameValuePair("totalVessels", totalVesselsByType));
					SVesselTypes vesselType;
					counter = 1;
					for(TypesStatsData type : typeStats) {
						vesselType = this._typesMap.get(type.getTypeId());
						xml.empty("type", 
								  new NameValuePair("item", counter++),
								  new NameValuePair("vrmfId", vesselType == null ? null : type.getTypeId()),
								  new NameValuePair("sourceSystem", vesselType == null ? null : vesselType.getSourceSystem()),
								  new NameValuePair("originalId", vesselType == null ? null : ( vesselType.getCountryId() == null ? "" : this._countriesMap.get(vesselType.getCountryId()).getIso2Code() + "_" ) + vesselType.getOriginalVesselTypeId()),
								  new NameValuePair("vessels", type.getVessels())
								 );
					}
					xml.close();
					
					xml.open("lengths",
							 new NameValuePair("num", lengthStats.size()),
							 new NameValuePair("totalVessels", totalVesselsByLength));
					counter = 1;
					for(LengthsStatsData length : lengthStats) {
						xml.empty("length",
								  new NameValuePair("item", counter++),
								  new NameValuePair("type", "loa"),
								  new NameValuePair("from", length == null ? null : length.getRangeFrom()),
								  new NameValuePair("to", length == null || length.getRangeTo() == null || length.getRangeTo() >= 100000 ? null : length.getRangeTo()),
								  new NameValuePair("vessels", length.getVessels())
								 );
					}
					xml.close();
					
				xml.close();
				
				content = xml.toString().getBytes("UTF-8");
			}
			
			return this.commonResponse(content, effectiveFormat);
		} catch (Throwable t) {
			return this.manageError(t);
		}
	}
	
	@RequestMapping(value="/sources/{sources}/flag/{flags}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> getStatsByFlag(HttpServletRequest request, @PathVariable String sources, @PathVariable String flags) {
		try {
			long end, start = System.currentTimeMillis();
			
			String[] requestedSources = sources.split("\\,");
			
			String[] effectiveSources = AbstractController.intersectSourceSystems(requestedSources, this.getAvailableSources(request));
			
			if(effectiveSources.length == 0)
				throw new NotOwnerException();
			
			SerializableArrayList<FlagsStatsData> flagStats = new SerializableArrayList<FlagsStatsData>(this._retriever.retrieveStatsByFlag(effectiveSources, _dataCustomizer.getTargetTable(request)));
			SerializableArrayList<TypesStatsData> typeStats = new SerializableArrayList<TypesStatsData>(this._retriever.retrieveStatsByType(effectiveSources, this.getCountryIDs(flags), _dataCustomizer.getTargetTable(request)));
			SerializableArrayList<LengthsStatsData> lengthStats = new SerializableArrayList<LengthsStatsData>(this._retriever.retrieveStatsByLength(effectiveSources, this.getCountryIDs(flags), ALL_VESSEL_TYPE_IDS, _dataCustomizer.getTargetTable(request)));
			
			end = System.currentTimeMillis();
			
			SerializableMap<String, SerializableList<? extends BasicVesselStatsReport>> data = new SerializableHashMap<String, SerializableList<? extends BasicVesselStatsReport>>();
			data.put("sources", this._retriever.retrieveSourcesData(effectiveSources, _dataCustomizer.getTargetTable(request)));
			data.put("flags", flagStats);
			data.put("types", typeStats);
			data.put("lengths", lengthStats);
			
			JSONResponse<SerializableMap<String, SerializableList<? extends BasicVesselStatsReport>>> response = new JSONResponse<SerializableMap<String, SerializableList<? extends BasicVesselStatsReport>>>();
			response.setError(false);
			response.setElapsed(end - start);
			response.setData(data);
					
			byte[] content = response.JSONify().getBytes();
			
			return this.commonResponse(content, "json");
		} catch (Throwable t) {
			return this.manageError(t);
		}
	}

	@RequestMapping(value="/sources/{sources}/type/{types}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> getStatsByType(HttpServletRequest request, @PathVariable String sources, @PathVariable String types) {
		try {
			long end, start = System.currentTimeMillis();
			
			String[] requestedSources = sources.split("\\,");
			
			String[] effectiveSources = AbstractController.intersectSourceSystems(requestedSources, this.getAvailableSources(request));
			
			if(effectiveSources.length == 0)
				throw new NotOwnerException();
			
			SerializableArrayList<FlagsStatsData> flagStats = new SerializableArrayList<FlagsStatsData>(this._retriever.retrieveStatsByFlag(effectiveSources, _dataCustomizer.getTargetTable(request)));
			SerializableArrayList<TypesStatsData> typeStats = new SerializableArrayList<TypesStatsData>(this._retriever.retrieveStatsByType(effectiveSources, ALL_COUNTRY_IDS, _dataCustomizer.getTargetTable(request)));
			SerializableArrayList<LengthsStatsData> lengthStats = new SerializableArrayList<LengthsStatsData>(this._retriever.retrieveStatsByLength(effectiveSources, ALL_COUNTRY_IDS, this.getTypeIDs(types), _dataCustomizer.getTargetTable(request)));
			
			end = System.currentTimeMillis();
			
			SerializableMap<String, SerializableList<? extends BasicVesselStatsReport>> data = new SerializableHashMap<String, SerializableList<? extends BasicVesselStatsReport>>();
			data.put("sources", this._retriever.retrieveSourcesData(effectiveSources, _dataCustomizer.getTargetTable(request)));
			data.put("flags", flagStats);
			data.put("types", typeStats);
			data.put("lengths", lengthStats);
			
			JSONResponse<SerializableMap<String, SerializableList<? extends BasicVesselStatsReport>>> response = new JSONResponse<SerializableMap<String, SerializableList<? extends BasicVesselStatsReport>>>();
			response.setError(false);
			response.setElapsed(end - start);
			response.setData(data);
					
			byte[] content = response.JSONify().getBytes();
			
			return this.commonResponse(content, "json");
		} catch (Throwable t) {
			return this.manageError(t);
		}
	}
	
	@RequestMapping(value="/sources/{sources}/flag/{flags}/type/{types}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> getStatsByFlagAndType(HttpServletRequest request, @PathVariable String sources, @PathVariable String flags, @PathVariable String types) {
		try {
			long end, start = System.currentTimeMillis();
			
			String[] requestedSources = sources.split("\\,");
			
			String[] effectiveSources = AbstractController.intersectSourceSystems(requestedSources, this.getAvailableSources(request));
			
			if(effectiveSources.length == 0)
				throw new NotOwnerException();
			
			SerializableArrayList<FlagsStatsData> flagStats = new SerializableArrayList<FlagsStatsData>(this._retriever.retrieveStatsByFlag(effectiveSources, _dataCustomizer.getTargetTable(request)));
			SerializableArrayList<TypesStatsData> typeStats = new SerializableArrayList<TypesStatsData>(this._retriever.retrieveStatsByType(effectiveSources, this.getCountryIDs(flags), _dataCustomizer.getTargetTable(request)));
			SerializableArrayList<LengthsStatsData> lengthStats = new SerializableArrayList<LengthsStatsData>(this._retriever.retrieveStatsByLength(effectiveSources, this.getCountryIDs(flags), this.getTypeIDs(types), _dataCustomizer.getTargetTable(request)));
			
			end = System.currentTimeMillis();
			
			SerializableMap<String, SerializableList<? extends BasicVesselStatsReport>> data = new SerializableHashMap<String, SerializableList<? extends BasicVesselStatsReport>>();
			data.put("sources", this._retriever.retrieveSourcesData(effectiveSources, _dataCustomizer.getTargetTable(request)));
			data.put("flags", flagStats);
			data.put("types", typeStats);
			data.put("lengths", lengthStats);
			
			JSONResponse<SerializableMap<String, SerializableList<? extends BasicVesselStatsReport>>> response = new JSONResponse<SerializableMap<String, SerializableList<? extends BasicVesselStatsReport>>>();
			response.setError(false);
			response.setElapsed(end - start);
			response.setData(data);
					
			byte[] content = response.JSONify().getBytes();
			
			return this.commonResponse(content, "json");
		} catch (Throwable t) {
			return this.manageError(t);
		}
	}

	@RequestMapping(value="/flags/sources/{sources}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> getFlagStats(HttpServletRequest request, @PathVariable String sources) {
		try {
			long end, start = System.currentTimeMillis();
			
			String[] requestedSources = sources.split("\\,");
			
			String[] effectiveSources = AbstractController.intersectSourceSystems(requestedSources, this.getAvailableSources(request));
			
			if(effectiveSources.length == 0)
				throw new NotOwnerException();			
			
			List<FlagsStatsData> stats = this._retriever.retrieveStatsByFlag(effectiveSources, _dataCustomizer.getTargetTable(request));
			
			end = System.currentTimeMillis();
			
			JSONResponse<SerializableList<FlagsStatsData>> response = new JSONResponse<SerializableList<FlagsStatsData>>();
			response.setError(false);
			response.setElapsed(end - start);
			response.setData(new SerializableArrayList<FlagsStatsData>(stats));
					
			byte[] content = response.JSONify().getBytes();
			
			return this.commonResponse(content, "json");
		} catch (Throwable t) {
			return this.manageError(t);
		}
	}
	
	@RequestMapping(value="types/sources/{sources}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> getTypeStats(HttpServletRequest request, @PathVariable String sources) {
		return this.doGetTypeStats(request, sources.split("\\,"), null);
	}
	
	@RequestMapping(value="types/sources/{sources}/flag/{flags}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> getTypeStatsByFlags(HttpServletRequest request, @PathVariable String sources, @PathVariable String flags) {
		try {
			return this.doGetTypeStats(request, sources.split("\\,"), this.getCountryIDs(flags));
		} catch (Throwable t) {
			return this.manageError(t);
		}
	}
	
	@RequestMapping(value="lengths/sources/{sources}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> getLengthStats(HttpServletRequest request, @PathVariable String sources) {
		return this.doGetLengthStats(request, sources.split("\\,"), null, null);
	}
	
	@RequestMapping(value="lengths/sources/{sources}/flag/{flags}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> getLengthStatsByFlags(HttpServletRequest request, @PathVariable String sources, @PathVariable String flags) {
		try {
			return this.doGetLengthStats(request, sources.split("\\,"), this.getCountryIDs(flags), null);
		} catch (Throwable t) { 
			return this.manageError(t);
		}
	}

	@RequestMapping(value="lengths/sources/{sources}/flag/{flags}/type/{types}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> getLengthStatsByFlagsAndTypes(HttpServletRequest request, @PathVariable String sources, @PathVariable String flags, @PathVariable String types) {
		try {
			return this.doGetLengthStats(request, sources.split("\\,"), this.getCountryIDs(flags), this.getTypeIDs(types));
		} catch (Throwable t) { 
			return this.manageError(t);
		}
	}

	@RequestMapping(value="lengths/sources/{sources}/type/{types}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> getLengthStatsByTypes(HttpServletRequest request, @PathVariable String sources, @PathVariable String types) {
		try {
			return this.doGetLengthStats(request, sources.split("\\,"), null, this.getTypeIDs(types));
		} catch (Throwable t) { 
			return this.manageError(t);
		}
	}
	
	private ResponseEntity<byte[]> doGetTypeStats(HttpServletRequest request, String[] sources, Integer[] countryIDs) {
		try {
			long end, start = System.currentTimeMillis();
			
			String[] effectiveSources = AbstractController.intersectSourceSystems(sources, this.getAvailableSources(request));
			
			if(effectiveSources.length == 0)
				throw new NotOwnerException();
			
			List<TypesStatsData> stats = this._retriever.retrieveStatsByType(sources, countryIDs, _dataCustomizer.getTargetTable(request));
			
			end = System.currentTimeMillis();
			
			JSONResponse<SerializableList<TypesStatsData>> response = new JSONResponse<SerializableList<TypesStatsData>>();
			response.setError(false);
			response.setElapsed(end - start);
			response.setData(new SerializableArrayList<TypesStatsData>(stats));
					
			byte[] content = response.JSONify().getBytes();
			
			return this.commonResponse(content, "json");
		} catch (Throwable t) {
			return this.manageError(t);
		}
	}
	
	private ResponseEntity<byte[]> doGetLengthStats(HttpServletRequest request, String[] sources, Integer[] countryIDs, Integer[] typeIDs) {
		try {
			long end, start = System.currentTimeMillis();
			
			String[] effectiveSources = AbstractController.intersectSourceSystems(sources, this.getAvailableSources(request));
			
			if(effectiveSources.length == 0)
				throw new NotOwnerException();
			
			List<LengthsStatsData> stats = this._retriever.retrieveStatsByLength(sources, countryIDs, typeIDs, _dataCustomizer.getTargetTable(request));
			
			end = System.currentTimeMillis();
			
			JSONResponse<SerializableList<LengthsStatsData>> response = new JSONResponse<SerializableList<LengthsStatsData>>();
			response.setError(false);
			response.setElapsed(end - start);
			response.setData(new SerializableArrayList<LengthsStatsData>(stats));
					
			byte[] content = response.JSONify().getBytes();
			
			return this.commonResponse(content, "json");
		} catch (Throwable t) {
			return this.manageError(t);
		}
	}
}