/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder)
 */
package org.fao.fi.vrmf.web.apps.finder.filters;

import java.io.IOException;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.fao.fi.vrmf.web.apps.finder.controllers.services.pub.redirect.AbstractRedirectorController;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 13 Nov 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 13 Nov 2012
 */
@Named
@Singleton
public class CommonRedirectorFilter extends AbstractNavigationFilter {
	/* (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void internalDoFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
//		super.doFilter(request, response, chain);

		HttpServletRequest servletRequest = (HttpServletRequest)request;
		HttpServletResponse servletResponse = (HttpServletResponse)response;

		AbstractRedirectorController.propagateOriginalURL(servletRequest);

		String requestedURI = servletRequest.getRequestURI();

		if(requestedURI != null) {
			if(requestedURI.endsWith("/"))
				requestedURI = requestedURI.substring(0, requestedURI.length() - 1);

			if(requestedURI.endsWith(SEARCH_PATH)) {
				requestedURI = SEARCH_PAGE;

				try {
					servletRequest.getRequestDispatcher(requestedURI).forward(servletRequest, servletResponse);

					return;
				} catch (Throwable t) {
					throw new ServletException(t);
				}
			}
		}

		chain.doFilter(request, response);
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() {
	}

}
