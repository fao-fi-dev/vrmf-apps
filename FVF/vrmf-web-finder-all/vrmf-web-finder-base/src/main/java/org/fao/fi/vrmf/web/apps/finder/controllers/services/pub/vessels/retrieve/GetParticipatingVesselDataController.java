/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder)
 */
package org.fao.fi.vrmf.web.apps.finder.controllers.services.pub.vessels.retrieve;

import java.nio.charset.Charset;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;

import org.fao.fi.sh.utility.model.extensions.collections.ListSet;
import org.fao.fi.sh.utility.model.extensions.collections.impl.SerializableArrayList;
import org.fao.fi.vrmf.business.dao.FullVesselsDAO;
import org.fao.fi.vrmf.common.data.exchange.core.models.JSONResponse;
import org.fao.fi.vrmf.common.j2ee.utilities.ServletsHelper;
import org.fao.fi.vrmf.common.models.extended.FullVessel;
import org.fao.fi.vrmf.common.services.search.response.SearchResponse;
import org.fao.fi.vrmf.common.web.DataCustomizer;
import org.fao.fi.vrmf.common.web.controllers.AbstractController;
import org.fao.fi.vrmf.web.apps.finder.business.dao.MetadataDAO;
import org.fao.fi.vrmf.web.apps.finder.controllers.services.pub.PublicServicesConstants;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import lombok.Getter;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 14 May 2012
 */
@Named @Singleton @Controller
@RequestMapping(PublicServicesConstants.VESSELS_SERVICES_PREFIX + "get/participants/*")
public class GetParticipatingVesselDataController extends AbstractController {
	static final protected String[] JSON_RESPONSE_INCLUSION_PATTERNS = { 
		"data.pageSize", 
		"data.currentResultPageSize",
		"data.fullResultsSize",
		"data.dataSet.id", 
		"data.dataSet.uid", 
		"data.dataSet.flagData.countryId",
		"data.dataSet.nameData.name",
		"data.dataSet.nameData.simplifiedName",
		"data.dataSet.callsignData.callsignId",
		"data.dataSet.callsignData.countryId",
		"data.dataSet.registrations.countryId",
		"data.dataSet.registrations.registrationNumber",
		"data.dataSet.registrations.registrationPort.name",
		"data.dataSet.lengthData.typeId",
		"data.dataSet.lengthData.value",
		"data.dataSet.tonnageData.typeId",
		"data.dataSet.tonnageData.value",
		"data.dataSet.types.vesselType.name",
		"data.dataSet.types.vesselType.id",
		"data.dataSet.gears.primaryGear",
		"data.dataSet.gears.gearType.name",
		"data.dataSet.gears.gearType.id",
		"data.dataSet.identifiers.typeId",
		"data.dataSet.identifiers.identifier",
		"data.dataSet.identifiers.alternateIdentifier",
		"data.dataSet.authorizationsData.sourceSystem",
		"data.dataSet.authorizationsData.validFrom",
		"data.dataSet.authorizationsData.validTo",
		"data.dataSet.sourceSystem",
		"data.dataSet.updateDate"
	};
	
	@Inject private @Getter @Setter DataCustomizer dataCustomizer;

	@Inject private @Getter @Setter MetadataDAO metadataDAO;
	@Inject private @Getter @Setter FullVesselsDAO fullVesselsDAO;
	
	@RequestMapping(value="UID/{UID:[0-9]+}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> getParticipatingByUID(HttpServletRequest request, @PathVariable Integer UID) {
		try {
			String[] sourceSystems = this.getAvailableSources(ServletsHelper.getLoggedUser(request), request, ServletsHelper.INCLUDE_OTHER_SOURCES);

			this._log.info("Getting participating vessels by UID '" + UID + "'...");
			
			long end, start = System.currentTimeMillis();
			
			List<Integer> IDs = this.metadataDAO.getIDsForUID(UID, sourceSystems, dataCustomizer.getTargetTable(request));

			SearchResponse<FullVessel> searchResults = null;
			
			if(IDs != null && !IDs.isEmpty()) {
				List<FullVessel> vessels = new ListSet<FullVessel>();
				
				for(Integer ID : IDs) {
					vessels.add(this.fullVesselsDAO.selectFullByPrimaryKeyAndDataSources(ID, sourceSystems));
				}
				
				searchResults = new SearchResponse<FullVessel>(new SerializableArrayList<FullVessel>(vessels));
			}
			
			end = System.currentTimeMillis();
			
			JSONResponse<SearchResponse<FullVessel>> response = new JSONResponse<SearchResponse<FullVessel>>(searchResults);
			response.setElapsed(end-start);
			
			String content = response.JSONifyOnly(JSON_RESPONSE_INCLUSION_PATTERNS);
			
			byte[] data = content == null ? new byte[0] : content.getBytes(Charset.forName("UTF-8"));
			
			HttpHeaders headers = this.getContentTypeDispositionAndLengthHeaders("Participating_vessels_" + UID, "json", data.length);
			
			return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
		} catch (Throwable t) {
			return this.manageError(t);
		}
	}
}