/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder)
 */
package org.fao.fi.vrmf.web.apps.finder.filters;

import org.fao.fi.vrmf.common.j2ee.filters.AbstractOncePerRequestFilter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 13 Nov 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 13 Nov 2012
 */
abstract public class AbstractNavigationFilter extends AbstractOncePerRequestFilter {
	static final String INDEX_PAGE = "index.jsp";
	static final String SEARCH_PATH = "search";
	static final String SEARCH_PAGE = SEARCH_PATH + ".jsp";
}
