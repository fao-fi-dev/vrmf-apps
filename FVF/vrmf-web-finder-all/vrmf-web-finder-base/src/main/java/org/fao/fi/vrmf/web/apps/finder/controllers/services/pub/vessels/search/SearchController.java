/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder)
 */
package org.fao.fi.vrmf.web.apps.finder.controllers.services.pub.vessels.search;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;

import org.fao.fi.vrmf.common.data.exchange.core.models.JSONResponse;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.VesselRecordSearchFilter;
import org.fao.fi.vrmf.common.search.dsl.support.models.vessels.record.VesselRecord;
import org.fao.fi.vrmf.common.search.dsl.support.services.response.vessels.VesselRecordSearchResponse;
import org.fao.fi.vrmf.web.apps.finder.controllers.services.pub.PublicServicesConstants;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 7 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 7 May 2012
 */
@Named @Singleton @Controller
@RequestMapping(PublicServicesConstants.VESSELS_SERVICES_PREFIX)
public class SearchController extends AbstractSearchControllerBase {
	@RequestMapping(value="search", method=RequestMethod.GET)
	public ResponseEntity<byte[]> search(HttpServletRequest request) {
		this._log.debug("Executing {} search...", this.getClass().getSimpleName());
		
		long overallEnd, overallStart = System.currentTimeMillis();

		try {
			long end, start = System.currentTimeMillis();
			
			VesselRecordSearchFilter filter = this.buildFilter(request);
			
			VesselRecordSearchResponse<VesselRecord> vessels = this.searchVesselsDAO.searchVessels(filter);
			
			end = System.currentTimeMillis();

			this._log.debug("Vessels search took {} mSec.", end - start);
			
			JSONResponse<VesselRecordSearchResponse<VesselRecord>> response = new JSONResponse<VesselRecordSearchResponse<VesselRecord>>(vessels);
			response.setElapsed(end - start);
			
			start = System.currentTimeMillis();
			
			byte[] content = response.JSONify(JSON_RESPONSE_INCLUSION_PATTERNS, new String[] { "*.class" }).getBytes("UTF-8");
			
			end = System.currentTimeMillis();
			
			this._log.debug("JSON-ification took {} mSec.", end - start);
			
			HttpHeaders contentHeaders = this.getContentTypeDispositionAndLengthHeaders("VesselsSearch_" + start, "json", content.length);
			HttpHeaders cacheHeaders = this.getCachingHeaders(3600);
			
			return new ResponseEntity<byte[]>(content, this.mergeHeaders(contentHeaders, cacheHeaders), HttpStatus.OK);
		} catch (Throwable t) {
			this._log.error("Unable to search vessels", t);
			
			return this.manageError(t);
		} finally {
			overallEnd = System.currentTimeMillis();
			
			this._log.debug("Overall search request management took {} mSec.", overallEnd - overallStart);
		}
	}
}