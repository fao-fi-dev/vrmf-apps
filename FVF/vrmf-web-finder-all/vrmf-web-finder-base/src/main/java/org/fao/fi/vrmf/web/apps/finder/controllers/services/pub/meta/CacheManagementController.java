/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder)
 */
package org.fao.fi.vrmf.web.apps.finder.controllers.services.pub.meta;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.fao.fi.vrmf.web.apps.finder.controllers.services.pub.PublicServicesConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 10 Jun 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 10 Jun 2014
 */
@Singleton @Controller
@RequestMapping(PublicServicesConstants.META_SERVICES_PREFIX)
public class CacheManagementController {
	private Logger _log = LoggerFactory.getLogger(this.getClass());
	
	@Inject protected List<CacheManager> _cacheManagers;

	@RequestMapping(value="caches/clear", method=RequestMethod.DELETE, produces="text/plain")
	public ResponseEntity<byte[]> clearCaches() {
		StringBuilder message = new StringBuilder();
		
		this._log.info("Clearing caches:");
		
		Cache cache;
		for(CacheManager manager : this._cacheManagers) {
			this._log.info("Processing cache manager {}...", manager);
			message.append("Processing cache manager ").append(manager).append("...\n");
			
			for(String cacheName : manager.getCacheNames()) {
				cache = manager.getCache(cacheName);
				
				if(cache != null) {
					this._log.info(" * Clearing cache {}", cacheName);
					message.append(" * Clearing cache ").append(cacheName).append("\n");
					
					cache.clear();
				}
			}
		}
		
		return new ResponseEntity<byte[]>(message.toString().getBytes(), HttpStatus.OK);
	}
}
