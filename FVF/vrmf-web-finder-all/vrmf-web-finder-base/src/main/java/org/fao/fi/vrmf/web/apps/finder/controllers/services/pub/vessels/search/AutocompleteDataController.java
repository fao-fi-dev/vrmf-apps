/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder)
 */
package org.fao.fi.vrmf.web.apps.finder.controllers.services.pub.vessels.search;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;

import org.fao.fi.vrmf.common.web.controllers.services.pub.CommonPublicServicesConstants;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 15 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 15 May 2012
 */
@Named @Singleton @Controller
@RequestMapping(CommonPublicServicesConstants.VESSELS_AUTOCOMPLETE_PREFIX + "*")
public class AutocompleteDataController extends org.fao.fi.vrmf.common.web.controllers.services.pub.vessels.search.autocompletion.AutocompleteDataController {
	/** Vessel type **/
	
	@RequestMapping(value="sources/{sources}/{historical}/max/{maxEntries}/grouped/vessel/type/{vesselType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteVesselTypeHistoricalGrouped(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String vesselType, @PathVariable String format) {
		return this.doAutocompleteVesselType(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), GROUP_BY_UID, maxEntries, vesselType, GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="sources/{sources}/{historical}/max/{maxEntries}/vessel/type/{vesselType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteVesselTypeHistorical(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String vesselType, @PathVariable String format) {
		return this.doAutocompleteVesselType(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), GROUP_BY_UID, maxEntries, vesselType, DONT_GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="sources/{sources}/{historical}/grouped/vessel/type/{vesselType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteVesselTypeHistoricalGrouped(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String vesselType, @PathVariable String format) {
		return this.doAutocompleteVesselType(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, vesselType, GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="sources/{sources}/{historical}/vessel/type/{vesselType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteVesselTypeHistorical(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String vesselType, @PathVariable String format) {
		return this.doAutocompleteVesselType(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, vesselType, DONT_GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="sources/{sources}/at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/grouped/vessel/type/{vesselType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteVesselTypeAtDateGrouped(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String vesselType, @PathVariable String format) {
		return this.doAutocompleteVesselType(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), GROUP_BY_UID, maxEntries, vesselType, GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="sources/{sources}/at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/vessel/type/{vesselType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteVesselTypeAtDate(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String vesselType, @PathVariable String format) {
		return this.doAutocompleteVesselType(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), GROUP_BY_UID, maxEntries, vesselType, DONT_GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="sources/{sources}/at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/vessel/type/{vesselType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteVesselTypeAtDate(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable String vesselType, @PathVariable String format) {
		return this.doAutocompleteVesselType(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, vesselType, DONT_GROUP_MAPPED_TYPES, format);
	}
	
	@RequestMapping(value="sources/{sources}/at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/grouped/vessel/type/{vesselType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteVesselTypeAtDateGrouped(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable String vesselType, @PathVariable String format) {
		return this.doAutocompleteVesselType(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, vesselType, GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="{historical}/max/{maxEntries}/vessel/type/{vesselType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteVesselTypeHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String vesselType, @PathVariable String format) {
		return this.doAutocompleteVesselType(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, maxEntries, vesselType, DONT_GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="{historical}/max/{maxEntries}/grouped/vessel/type/{vesselType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteVesselTypeHistoricalGrouped(HttpServletRequest request, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String vesselType, @PathVariable String format) {
		return this.doAutocompleteVesselType(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, maxEntries, vesselType, GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="{historical}/vessel/type/{vesselType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteVesselTypeHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable String vesselType, @PathVariable String format) {
		return this.doAutocompleteVesselType(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, vesselType, DONT_GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="{historical}/grouped/vessel/type/{vesselType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteVesselTypeHistoricalGrouped(HttpServletRequest request, @PathVariable String historical, @PathVariable String vesselType, @PathVariable String format) {
		return this.doAutocompleteVesselType(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, vesselType, GROUP_MAPPED_TYPES, format);
	}

	
	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/vessel/type/{vesselType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteVesselTypeAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String vesselType, @PathVariable String format) {
		return this.doAutocompleteVesselType(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, maxEntries, vesselType, DONT_GROUP_MAPPED_TYPES, format);
	}
	
	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/grouped/vessel/type/{vesselType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteVesselTypeAtDateGrouped(HttpServletRequest request, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String vesselType, @PathVariable String format) {
		return this.doAutocompleteVesselType(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, maxEntries, vesselType, GROUP_MAPPED_TYPES, format);
	}
	
	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/vessel/type/{vesselType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteVesselTypeAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable String vesselType, @PathVariable String format) {
		return this.doAutocompleteVesselType(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, vesselType, DONT_GROUP_MAPPED_TYPES, format);
	}
	
	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/grouped/vessel/type/{vesselType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteVesselTypeAtDateGrouped(HttpServletRequest request, @PathVariable String atDate, @PathVariable String vesselType, @PathVariable String format) {
		return this.doAutocompleteVesselType(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, vesselType, GROUP_MAPPED_TYPES, format);
	}
	
	/** Gear type **/
	
	@RequestMapping(value="sources/{sources}/{historical}/max/{maxEntries}/gear/type/{gearType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteGearTypeHistorical(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String gearType, @PathVariable String format) {
		return this.doAutocompleteGearType(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), GROUP_BY_UID, maxEntries, gearType, DONT_GROUP_MAPPED_TYPES, format);
	}
	
	@RequestMapping(value="sources/{sources}/{historical}/max/{maxEntries}/grouped/gear/type/{gearType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteGearTypeHistoricalGrouped(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String gearType, @PathVariable String format) {
		return this.doAutocompleteGearType(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), GROUP_BY_UID, maxEntries, gearType, GROUP_MAPPED_TYPES, format);
	}
	
	@RequestMapping(value="sources/{sources}/{historical}/gear/type/{gearType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteGearTypeHistorical(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String gearType, @PathVariable String format) {
		return this.doAutocompleteGearType(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, gearType, DONT_GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="sources/{sources}/{historical}/grouped/gear/type/{gearType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteGearTypeHistoricalGrouped(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String gearType, @PathVariable String format) {
		return this.doAutocompleteGearType(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, gearType, GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="sources/{sources}/at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/gear/type/{gearType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteGearTypeAtDate(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String gearType, @PathVariable String format) {
		return this.doAutocompleteGearType(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), GROUP_BY_UID, maxEntries, gearType, DONT_GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="sources/{sources}/at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/grouped/gear/type/{gearType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteGearTypeAtDateGrouped(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String gearType, @PathVariable String format) {
		return this.doAutocompleteGearType(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), GROUP_BY_UID, maxEntries, gearType, GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="sources/{sources}/at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/gear/type/{gearType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteGearTypeAtDate(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable String gearType, @PathVariable String format) {
		return this.doAutocompleteGearType(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, gearType, DONT_GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="sources/{sources}/at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/grouped/gear/type/{gearType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteGearTypeAtDateGrouped(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable String gearType, @PathVariable String format) {
		return this.doAutocompleteGearType(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, gearType, GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="{historical}/max/{maxEntries}/gear/type/{gearType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteGearTypeHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String gearType, @PathVariable String format) {
		return this.doAutocompleteGearType(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, maxEntries, gearType, DONT_GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="{historical}/max/{maxEntries}/grouped/gear/type/{gearType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteGearTypeHistoricalGrouped(HttpServletRequest request, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String gearType, @PathVariable String format) {
		return this.doAutocompleteGearType(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, maxEntries, gearType, GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="{historical}/gear/type/{gearType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteGearTypeHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable String gearType, @PathVariable String format) {
		return this.doAutocompleteGearType(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, gearType, DONT_GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="{historical}/grouped/gear/type/{gearType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteGearTypeHistoricalGrouped(HttpServletRequest request, @PathVariable String historical, @PathVariable String gearType, @PathVariable String format) {
		return this.doAutocompleteGearType(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, gearType, GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/gear/type/{gearType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteGearTypeAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String gearType, @PathVariable String format) {
		return this.doAutocompleteGearType(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, maxEntries, gearType, DONT_GROUP_MAPPED_TYPES, format);
	}
	
	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/grouped/gear/type/{gearType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteGearTypeAtDateGrouped(HttpServletRequest request, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String gearType, @PathVariable String format) {
		return this.doAutocompleteGearType(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, maxEntries, gearType, GROUP_MAPPED_TYPES, format);
	}
	
	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/gear/type/{gearType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteGearTypeAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable String gearType, @PathVariable String format) {
		return this.doAutocompleteGearType(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, gearType, DONT_GROUP_MAPPED_TYPES, format);
	}
	
	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/grouped/gear/type/{gearType}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteGearTypeAtDateGrouped(HttpServletRequest request, @PathVariable String atDate, @PathVariable String gearType, @PathVariable String format) {
		return this.doAutocompleteGearType(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, gearType, GROUP_MAPPED_TYPES, format);
	}
	
	/** IRCS **/
	
	@RequestMapping(value="sources/{sources}/{historical}/max/{maxEntries}/ircs/{ircs}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteIRCSHistorical(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String ircs, @PathVariable String format) {
		return this.doAutocompleteIRCS(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), GROUP_BY_UID, maxEntries, ircs, format);
	}
	
	@RequestMapping(value="sources/{sources}/{historical}/ircs/{ircs}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteIRCSHistorical(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String ircs, @PathVariable String format) {
		return this.doAutocompleteIRCS(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, ircs, format);
	}
	
	@RequestMapping(value="sources/{sources}/at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/ircs/{ircs}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteIRCSAtDate(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String ircs, @PathVariable String format) {
		return this.doAutocompleteIRCS(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), GROUP_BY_UID, maxEntries, ircs, format);
	}

	@RequestMapping(value="sources/{sources}/at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/ircs/{ircs}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteIRCSAtDate(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable String ircs, @PathVariable String format) {
		return this.doAutocompleteIRCS(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, ircs, format);
	}

	@RequestMapping(value="{historical}/max/{maxEntries}/ircs/{ircs}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteIRCSHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String ircs, @PathVariable String format) {
		return this.doAutocompleteIRCS(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, maxEntries, ircs, format);
	}
	
	@RequestMapping(value="{historical}/ircs/{ircs}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteIRCSHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable String ircs, @PathVariable String format) {
		return this.doAutocompleteIRCS(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, ircs, format);
	}
	
	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/ircs/{ircs}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteIRCSAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String ircs, @PathVariable String format) {
		return this.doAutocompleteIRCS(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, maxEntries, ircs, format);
	}
	
	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/ircs/{ircs}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteIRCSAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable String ircs, @PathVariable String format) {
		return this.doAutocompleteIRCS(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, ircs, format);
	}
		
	/** MMSI */
	
	@RequestMapping(value="sources/{sources}/{historical}/max/{maxEntries}/mmsi/{mmsi}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteMMSIHistorical(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String mmsi, @PathVariable String format) {
		return this.doAutocompleteMMSI(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), GROUP_BY_UID, maxEntries, mmsi, format);
	}
	
	@RequestMapping(value="sources/{sources}/{historical}/mmsi/{mmsi}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteMMSIHistorical(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String mmsi, @PathVariable String format) {
		return this.doAutocompleteMMSI(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, mmsi, format);
	}
	
	@RequestMapping(value="sources/{sources}/at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/mmsi/{mmsi}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteMMSIAtDate(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String mmsi, @PathVariable String format) {
		return this.doAutocompleteMMSI(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), GROUP_BY_UID, maxEntries, mmsi, format);
	}

	@RequestMapping(value="sources/{sources}/at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/mmsi/{mmsi}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteMMSIAtDate(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable String mmsi, @PathVariable String format) {
		return this.doAutocompleteMMSI(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, mmsi, format);
	}

	@RequestMapping(value="{historical}/max/{maxEntries}/mmsi/{mmsi}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteMMSIHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String mmsi, @PathVariable String format) {
		return this.doAutocompleteMMSI(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, maxEntries, mmsi, format);
	}
	
	@RequestMapping(value="{historical}/mmsi/{mmsi}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteMMSIHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable String mmsi, @PathVariable String format) {
		return this.doAutocompleteMMSI(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, mmsi, format);
	}

	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/mmsi/{mmsi}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteMMSIAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String mmsi, @PathVariable String format) {
		return this.doAutocompleteMMSI(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, maxEntries, mmsi, format);
	}
	
	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/mmsi/{mmsi}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteMMSIAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable String mmsi, @PathVariable String format) {
		return this.doAutocompleteMMSI(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, mmsi, format);
	}

	/** NAME */
	
	@RequestMapping(value="sources/{sources}/{historical}/max/{maxEntries}/name/{name}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteNameHistorical(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String name, @PathVariable String format) {
		return this.doAutocompleteName(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), GROUP_BY_UID, maxEntries, name, format);
	}
	
	@RequestMapping(value="sources/{sources}/{historical}/name/{name}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteNameHistorical(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String name, @PathVariable String format) {
		return this.doAutocompleteName(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, name, format);
	}
	
	@RequestMapping(value="sources/{sources}/at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/name/{name}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteNameAtDate(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String name, @PathVariable String format) {
		return this.doAutocompleteName(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), GROUP_BY_UID, maxEntries, name, format);
	}
	
	@RequestMapping(value="sources/{sources}/at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/name/{name}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteNameAtDate(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable String name, @PathVariable String format) {
		return this.doAutocompleteName(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, name, format);
	}
		
	@RequestMapping(value="{historical}/max/{maxEntries}/name/{name}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteNameHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String name, @PathVariable String format) {
		return this.doAutocompleteName(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, maxEntries, name, format);
	}
	
	@RequestMapping(value="{historical}/name/{name}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteNameHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable String name, @PathVariable String format) {
		return this.doAutocompleteName(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, name, format);
	}

	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/name/{name}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteNameAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String name, @PathVariable String format) {
		return this.doAutocompleteName(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, maxEntries, name, format);
	}
	
	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/name/{name}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteNameAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable String name, @PathVariable String format) {
		return this.doAutocompleteName(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, name, format);
	}

	/** EXT. MARKINGS */
	
	@RequestMapping(value="sources/{sources}/{historical}/max/{maxEntries}/externalMarkings/{externalMarkings}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteExternalMarkingsHistorical(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String externalMarkings, @PathVariable String format) {
		return this.doAutocompleteExternalMarkings(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), GROUP_BY_UID, maxEntries, externalMarkings, format);
	}
	
	@RequestMapping(value="sources/{sources}/{historical}/externalMarkings/{externalMarkings}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteExternalMarkingsHistorical(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String externalMarkings, @PathVariable String format) {
		return this.doAutocompleteExternalMarkings(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, externalMarkings, format);
	}
	
	@RequestMapping(value="sources/{sources}/at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/externalMarkings/{externalMarkings}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteExternalMarkingsAtDate(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String externalMarkings, @PathVariable String format) {
		return this.doAutocompleteExternalMarkings(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), GROUP_BY_UID, maxEntries, externalMarkings, format);
	}

	@RequestMapping(value="sources/{sources}/at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/externalMarkings/{externalMarkings}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteExternalMarkingsAtDate(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable String externalMarkings, @PathVariable String format) {
		return this.doAutocompleteExternalMarkings(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, externalMarkings, format);
	}

	@RequestMapping(value="{historical}/max/{maxEntries}/externalMarkings/{externalMarkings}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteExternalMarkingsHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String externalMarkings, @PathVariable String format) {
		return this.doAutocompleteExternalMarkings(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, maxEntries, externalMarkings, format);
	}
	
	@RequestMapping(value="{historical}/externalMarkings/{externalMarkings}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteExternalMarkingsHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable String externalMarkings, @PathVariable String format) {
		return this.doAutocompleteExternalMarkings(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, externalMarkings, format);
	}
	
	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/externalMarkings/{externalMarkings}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteExternalMarkingsAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String externalMarkings, @PathVariable String format) {
		return this.doAutocompleteExternalMarkings(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, maxEntries, externalMarkings, format);
	}

	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/externalMarkings/{externalMarkings}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteExternalMarkingsAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable String externalMarkings, @PathVariable String format) {
		return this.doAutocompleteExternalMarkings(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, externalMarkings, format);
	}
	
	/** REG. PORT */
	@RequestMapping(value="sources/{sources}/{historical}/max/{maxEntries}/registration/countries/{regCountries}/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortHistoricalBySource(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String regCountries, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), GROUP_BY_UID, maxEntries, regCountries, port, DONT_GROUP_MAPPED_TYPES, format);
	}
	
	@RequestMapping(value="sources/{sources}/{historical}/max/{maxEntries}/registration/countries/{regCountries}/grouped/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortHistoricalBySourceGrouped(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String regCountries, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), GROUP_BY_UID, maxEntries, regCountries, port, GROUP_MAPPED_TYPES, format);
	}
	
	@RequestMapping(value="sources/{sources}/{historical}/registration/countries/{regCountries}/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortHistoricalBySourceUnlimited(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String regCountries, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, regCountries, port, DONT_GROUP_MAPPED_TYPES, format);
	}
	
	@RequestMapping(value="sources/{sources}/{historical}/registration/countries/{regCountries}/grouped/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortHistoricalBySourceUnlimitedGrouped(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String regCountries, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, regCountries, port, GROUP_MAPPED_TYPES, format);
	}
	
	@RequestMapping(value="sources/{sources}/at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/registration/countries/{regCountries}/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortAtDateBySource(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String regCountries, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), GROUP_BY_UID, maxEntries, regCountries, port, DONT_GROUP_MAPPED_TYPES, format);
	}
	
	@RequestMapping(value="sources/{sources}/at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/registration/countries/{regCountries}/grouped/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortAtDateBySourceGrouped(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String regCountries, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), GROUP_BY_UID, maxEntries, regCountries, port, GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="sources/{sources}/at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/registration/countries/{regCountries}/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortAtDateBySourceUnlimited(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable String regCountries, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, regCountries, port, DONT_GROUP_MAPPED_TYPES, format);
	}
	
	@RequestMapping(value="sources/{sources}/at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/registration/countries/{regCountries}/grouped/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortAtDateBySourceUnlimitedGrouped(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable String regCountries, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, regCountries, port, GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="{historical}/max/{maxEntries}/registration/countries/{regCountries}/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String regCountries, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, maxEntries, regCountries, port, DONT_GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="{historical}/max/{maxEntries}/registration/countries/{regCountries}/grouped/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortHistoricalGrouped(HttpServletRequest request, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String regCountries, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, maxEntries, regCountries, port, GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="{historical}/registration/countries/{regCountries}/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortHistoricalUnlimited(HttpServletRequest request, @PathVariable String historical, @PathVariable String regCountries, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, regCountries, port, DONT_GROUP_MAPPED_TYPES, format);
	}
	
	@RequestMapping(value="{historical}/registration/countries/{regCountries}/grouped/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortHistoricalUnlimitedGrouped(HttpServletRequest request, @PathVariable String historical, @PathVariable String regCountries, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, regCountries, port, GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/registration/countries/{regCountries}/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String regCountries, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, maxEntries, regCountries, port, DONT_GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/registration/countries/{regCountries}/grouped/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortAtDateGrouped(HttpServletRequest request, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String regCountries, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, maxEntries, regCountries, port, GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/registration/countries/{regCountries}/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortAtDateUnlimited(HttpServletRequest request, @PathVariable String atDate, @PathVariable String regCountries, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, regCountries, port, DONT_GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/registration/countries/{regCountries}/grouped/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortAtDateUnlimitedGrouped(HttpServletRequest request, @PathVariable String atDate, @PathVariable String regCountries, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, regCountries, port, GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="sources/{sources}/{historical}/max/{maxEntries}/registration/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortHistoricalBySource(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), GROUP_BY_UID, maxEntries, NO_REGISTRATION_COUNTRY, port, DONT_GROUP_MAPPED_TYPES, format);
	}
	
	@RequestMapping(value="sources/{sources}/{historical}/max/{maxEntries}/registration/grouped/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortHistoricalBySourceGrouped(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), GROUP_BY_UID, maxEntries, NO_REGISTRATION_COUNTRY, port, GROUP_MAPPED_TYPES, format);
	}
	
	@RequestMapping(value="sources/{sources}/{historical}/registration/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortHistoricalBySourceUnlimited(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, NO_REGISTRATION_COUNTRY, port, DONT_GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="sources/{sources}/{historical}/registration/grouped/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortHistoricalBySourceUnlimitedGrouped(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, NO_REGISTRATION_COUNTRY, port, GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="sources/{sources}/at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/registration/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortAtDateBySource(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), GROUP_BY_UID, maxEntries, NO_REGISTRATION_COUNTRY , port, DONT_GROUP_MAPPED_TYPES, format);
	}
	
	@RequestMapping(value="sources/{sources}/at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/registration/grouped/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortAtDateBySourceGrouped(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), GROUP_BY_UID, maxEntries, NO_REGISTRATION_COUNTRY , port, GROUP_MAPPED_TYPES, format);
	}
	
	@RequestMapping(value="sources/{sources}/at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/registration/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortAtDateBySourceUnlimited(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, NO_REGISTRATION_COUNTRY , port, DONT_GROUP_MAPPED_TYPES, format);
	}
	
	@RequestMapping(value="sources/{sources}/at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/registration/grouped/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortAtDateBySourceUnlimitedGrouped(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, NO_REGISTRATION_COUNTRY , port, GROUP_MAPPED_TYPES, format);
	}
	
	@RequestMapping(value="{historical}/max/{maxEntries}/registration/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, maxEntries, NO_REGISTRATION_COUNTRY, port, DONT_GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="{historical}/max/{maxEntries}/registration/grouped/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortHistoricalGrouped(HttpServletRequest request, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, maxEntries, NO_REGISTRATION_COUNTRY, port, GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="{historical}/registration/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortHistoricalUnlimited(HttpServletRequest request, @PathVariable String historical, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, NO_REGISTRATION_COUNTRY, port, DONT_GROUP_MAPPED_TYPES, format);
	}
	
	@RequestMapping(value="{historical}/registration/grouped/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortHistoricalUnlimitedGrouped(HttpServletRequest request, @PathVariable String historical, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, NO_REGISTRATION_COUNTRY, port, GROUP_MAPPED_TYPES, format);
	}
	
	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/registration/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, maxEntries, NO_REGISTRATION_COUNTRY, port, DONT_GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/registration/grouped/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortAtDateGrouped(HttpServletRequest request, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, maxEntries, NO_REGISTRATION_COUNTRY, port, GROUP_MAPPED_TYPES, format);
	}

	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/registration/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortAtDateUnlimited(HttpServletRequest request, @PathVariable String atDate, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, NO_REGISTRATION_COUNTRY, port, DONT_GROUP_MAPPED_TYPES, format);
	}
	
	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/registration/grouped/port/{port}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegPortAtDateUnlimitedGrouped(HttpServletRequest request, @PathVariable String atDate, @PathVariable String port, @PathVariable String format) {
		return this.doAutocompleteRegistrationPort(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, NO_REGISTRATION_COUNTRY, port, GROUP_MAPPED_TYPES,format);
	}
	
	/** REG. NO. */

	@RequestMapping(value="sources/{sources}/{historical}/max/{maxEntries}/registration/number/{regNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegNoHistorical(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String regNo, @PathVariable String format) {
		return this.doAutocompleteRegistrationNumber(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), GROUP_BY_UID, maxEntries, NO_REGISTRATION_COUNTRY, NO_REGISTRATION_PORT, regNo, format);
	}
	
	@RequestMapping(value="sources/{sources}/{historical}/max/{maxEntries}/registration/countries/{countries}/number/{regNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegNoAndCountriesHistorical(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String countries, @PathVariable String regNo, @PathVariable String format) {
		return this.doAutocompleteRegistrationNumber(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), GROUP_BY_UID, maxEntries, countries, NO_REGISTRATION_PORT, regNo, format);
	}
	
	@RequestMapping(value="sources/{sources}/{historical}/max/{maxEntries}/registration/port/{port}/number/{regNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegNoAndPortHistorical(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String port, @PathVariable String regNo, @PathVariable String format) {
		return this.doAutocompleteRegistrationNumber(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), GROUP_BY_UID, maxEntries, NO_REGISTRATION_COUNTRY, port, regNo, format);
	}
	
	@RequestMapping(value="sources/{sources}/at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/registration/number/{regNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegNoAtDate(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String regNo, @PathVariable String format) {
		return this.doAutocompleteRegistrationNumber(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), GROUP_BY_UID, maxEntries, NO_REGISTRATION_COUNTRY, NO_REGISTRATION_PORT, regNo, format);
	}
	
	@RequestMapping(value="sources/{sources}/at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/registration/countries/{countries}/number/{regNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegNoAndCountriesAtDate(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String countries, @PathVariable String regNo, @PathVariable String format) {
		return this.doAutocompleteRegistrationNumber(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), GROUP_BY_UID, maxEntries, countries, NO_REGISTRATION_PORT, regNo, format);
	}

	@RequestMapping(value="sources/{sources}/at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/registration/port/{port}/number/{regNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegNoAndPortAtDate(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String port, @PathVariable String regNo, @PathVariable String format) {
		return this.doAutocompleteRegistrationNumber(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), GROUP_BY_UID, maxEntries, NO_REGISTRATION_COUNTRY, port, regNo, format);
	}

	@RequestMapping(value="{historical}/max/{maxEntries}/registration/number/{regNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegNoHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String regNo, @PathVariable String format) {
		return this.doAutocompleteRegistrationNumber(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, maxEntries, NO_REGISTRATION_COUNTRY, NO_REGISTRATION_PORT, regNo, format);
	}
	
	@RequestMapping(value="{historical}/max/{maxEntries}/registration/countries/{countries}/number/{regNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegNoAndCountriesHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String countries, @PathVariable String regNo, @PathVariable String format) {
		return this.doAutocompleteRegistrationNumber(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, maxEntries, countries, NO_REGISTRATION_PORT, regNo, format);
	}

	@RequestMapping(value="{historical}/max/{maxEntries}/registration/port/{port}/number/{regNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegNoAndPortHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String port, @PathVariable String regNo, @PathVariable String format) {
		return this.doAutocompleteRegistrationNumber(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, maxEntries, NO_REGISTRATION_COUNTRY, port, regNo, format);
	}

	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/registration/number/{regNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegNoAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String regNo, @PathVariable String format) {
		return this.doAutocompleteRegistrationNumber(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, maxEntries, NO_REGISTRATION_COUNTRY, NO_REGISTRATION_PORT, regNo, format);
	}
	
	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/registration/countries/{countries}/number/{regNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegNoAndCountriesAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String countries, @PathVariable String regNo, @PathVariable String format) {
		return this.doAutocompleteRegistrationNumber(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, maxEntries, countries, NO_REGISTRATION_PORT, regNo, format);
	}
	
	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/registration/port/{port}/number/{regNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegNoAndPortAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String port, @PathVariable String regNo, @PathVariable String format) {
		return this.doAutocompleteRegistrationNumber(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, maxEntries, NO_REGISTRATION_COUNTRY, port, regNo, format);
	}
	
	@RequestMapping(value="{historical}/registration/number/{regNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegNoHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable String regNo, @PathVariable String format) {
		return this.doAutocompleteRegistrationNumber(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, NO_REGISTRATION_COUNTRY, NO_REGISTRATION_PORT, regNo, format);
	}
	
	@RequestMapping(value="{historical}/registration/countries/{countries}/number/{regNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegNoAndCountriesHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable String countries, @PathVariable String regNo, @PathVariable String format) {
		return this.doAutocompleteRegistrationNumber(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, countries, NO_REGISTRATION_PORT, regNo, format);
	}

	@RequestMapping(value="{historical}/registration/port/{port}/number/{regNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegNoAndPortHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable String port, @PathVariable String regNo, @PathVariable String format) {
		return this.doAutocompleteRegistrationNumber(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, NO_REGISTRATION_COUNTRY, port, regNo, format);
	}
	
	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/registration/number/{regNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegNoAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable String regNo, @PathVariable String format) {
		return this.doAutocompleteRegistrationNumber(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, NO_REGISTRATION_COUNTRY, NO_REGISTRATION_PORT, regNo, format);
	}
	
	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/registration/countries/{countries}/number/{regNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegNoAndCountriesAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable String countries, @PathVariable String regNo, @PathVariable String format) {
		return this.doAutocompleteRegistrationNumber(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, countries, NO_REGISTRATION_PORT, regNo, format);
	}
	
	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/registration/port/{port}/number/{regNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteRegNoAndPortAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable String port, @PathVariable String regNo, @PathVariable String format) {
		return this.doAutocompleteRegistrationNumber(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, NO_REGISTRATION_COUNTRY, port, regNo, format);
	}
	
	/** Fishing license */
	
	@RequestMapping(value="sources/{sources}/{historical}/max/{maxEntries}/fishingLicense/number/{fishingLicenseNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteFishingLicenseHistorical(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String fishingLicenseNo, @PathVariable String format) {
		return this.doAutocompleteFishingLicense(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), GROUP_BY_UID, maxEntries, NO_REGISTRATION_COUNTRY, fishingLicenseNo, format);
	}
	
	@RequestMapping(value="sources/{sources}/{historical}/max/{maxEntries}/fishingLicense/countries/{countries}/number/{fishingLicenseNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteFishingLicenseAndCountriesHistorical(HttpServletRequest request, @PathVariable String sources, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String countries, @PathVariable String fishingLicenseNo, @PathVariable String format) {
		return this.doAutocompleteFishingLicense(request, DEFAULT_PROCEDURE, sources, this.getDate(historical), GROUP_BY_UID, maxEntries, countries, fishingLicenseNo, format);
	}
		
	@RequestMapping(value="sources/{sources}/at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/fishingLicense/number/{fishingLicenseNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteFishingLicenseAtDate(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String fishingLicenseNo, @PathVariable String format) {
		return this.doAutocompleteFishingLicense(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), GROUP_BY_UID, maxEntries, NO_REGISTRATION_COUNTRY, fishingLicenseNo, format);
	}
	
	@RequestMapping(value="sources/{sources}/at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/fishingLicense/countries/{countries}/number/{fishingLicenseNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteFishingLicenseAndCountriesAtDate(HttpServletRequest request, @PathVariable String sources, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String countries, @PathVariable String fishingLicenseNo, @PathVariable String format) {
		return this.doAutocompleteFishingLicense(request, DEFAULT_PROCEDURE, sources, this.atDate(atDate), GROUP_BY_UID, maxEntries, countries, fishingLicenseNo, format);
	}

	@RequestMapping(value="{historical}/max/{maxEntries}/fishingLicense/number/{fishingLicenseNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteFishingLicenseHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String fishingLicenseNo, @PathVariable String format) {
		return this.doAutocompleteFishingLicense(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, maxEntries, NO_REGISTRATION_COUNTRY, fishingLicenseNo, format);
	}
	
	@RequestMapping(value="{historical}/max/{maxEntries}/fishingLicense/countries/{countries}/number/{fishingLicenseNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteFishingLicenseAndCountriesHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable Integer maxEntries, @PathVariable String countries, @PathVariable String fishingLicenseNo, @PathVariable String format) {
		return this.doAutocompleteFishingLicense(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, maxEntries, countries, fishingLicenseNo, format);
	}

	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/fishingLicense/number/{fishingLicenseNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteFishingLicenseAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String fishingLicenseNo, @PathVariable String format) {
		return this.doAutocompleteFishingLicense(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, maxEntries, NO_REGISTRATION_COUNTRY, fishingLicenseNo, format);
	}
	
	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/max/{maxEntries}/fishingLicense/countries/{countries}/number/{fishingLicenseNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteFishingLicenseAndCountriesAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable Integer maxEntries, @PathVariable String countries, @PathVariable String fishingLicenseNo, @PathVariable String format) {
		return this.doAutocompleteFishingLicense(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, maxEntries, countries, fishingLicenseNo, format);
	}
	
	@RequestMapping(value="{historical}/fishingLicense/number/{fishingLicenseNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteFishingLicenseHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable String fishingLicenseNo, @PathVariable String format) {
		return this.doAutocompleteFishingLicense(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, NO_REGISTRATION_COUNTRY, fishingLicenseNo, format);
	}
	
	@RequestMapping(value="{historical}/fishingLicense/countries/{countries}/number/{fishingLicenseNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteFishingLicenseAndCountriesHistorical(HttpServletRequest request, @PathVariable String historical, @PathVariable String countries, @PathVariable String fishingLicenseNo, @PathVariable String format) {
		return this.doAutocompleteFishingLicense(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.getDate(historical), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, countries, fishingLicenseNo, format);
	}
	
	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/fishingLicense/number/{fishingLicenseNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteFishingLicenseAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable String fishingLicenseNo, @PathVariable String format) {
		return this.doAutocompleteFishingLicense(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, NO_REGISTRATION_COUNTRY, fishingLicenseNo, format);
	}
	
	@RequestMapping(value="at/{atDate:[0-9]{4}-[0-9]{2}-[0-9]{2}}/fishingLicense/countries/{countries}/number/{fishingLicenseNo}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteFishingLicenseAndCountriesAtDate(HttpServletRequest request, @PathVariable String atDate, @PathVariable String countries, @PathVariable String fishingLicenseNo, @PathVariable String format) {
		return this.doAutocompleteFishingLicense(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, this.atDate(atDate), GROUP_BY_UID, DEFAULT_MAX_ENTRIES, countries, fishingLicenseNo, format);
	}
	
	/** UID */
	
	@RequestMapping(value="sources/{sources}/max/{maxEntries}/UID/{uid}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteUID(HttpServletRequest request, @PathVariable String sources, @PathVariable Integer maxEntries, @PathVariable String uid, @PathVariable String format) {
		return this.doAutocompleteUID(request, DEFAULT_PROCEDURE, sources, GROUP_BY_UID, maxEntries, uid, format);
	}
	
	@RequestMapping(value="max/{maxEntries}/UID/{uid}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteUID(HttpServletRequest request, @PathVariable Integer maxEntries, @PathVariable String uid, @PathVariable String format) {
		return this.doAutocompleteUID(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, GROUP_BY_UID, maxEntries, uid, format);
	}
	
	@RequestMapping(value="UID/{uid}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteUID(HttpServletRequest request, @PathVariable String uid, @PathVariable String format) {
		return this.doAutocompleteUID(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, GROUP_BY_UID, DEFAULT_MAX_ENTRIES, uid, format);
	}
	
	@RequestMapping(value="sources/{sources}/max/{maxEntries}/TUVI/{tuvi}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteTUVI(HttpServletRequest request, @PathVariable String sources, @PathVariable Integer maxEntries, @PathVariable String tuvi, @PathVariable String format) {
		return this.doAutocompleteTUVI(request, DEFAULT_PROCEDURE, sources, GROUP_BY_UID, maxEntries, tuvi, format);
	}
	
	@RequestMapping(value="max/{maxEntries}/TUVI/{tuvi}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteTUVI(HttpServletRequest request, @PathVariable Integer maxEntries, @PathVariable String tuvi, @PathVariable String format) {
		return this.doAutocompleteTUVI(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, GROUP_BY_UID, maxEntries, tuvi, format);
	}
	
	@RequestMapping(value="TUVI/{tuvi}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteTUVI(HttpServletRequest request, @PathVariable String tuvi, @PathVariable String format) {
		return this.doAutocompleteTUVI(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, GROUP_BY_UID, DEFAULT_MAX_ENTRIES, tuvi, format);
	}
	
	/** IDENTIFIER */
	
	@RequestMapping(value="sources/{sources}/max/{maxEntries}/identifier/{identifierType}/{identifier}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteIdentifier(HttpServletRequest request, @PathVariable String sources, @PathVariable String identifierType, @PathVariable Integer maxEntries, @PathVariable String identifier, @PathVariable String format) {
		return this.doAutocompleteIdentifier(request, DEFAULT_PROCEDURE, sources, identifierType, GROUP_BY_UID, maxEntries, identifier, format);
	}
	
	@RequestMapping(value="max/{maxEntries}/identifier/{identifierType}/{identifier}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteIdentifier(HttpServletRequest request,  @PathVariable String identifierType, @PathVariable Integer maxEntries, @PathVariable String identifier, @PathVariable String format) {
		return this.doAutocompleteIdentifier(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, identifierType, GROUP_BY_UID, maxEntries, identifier, format);
	}
	
	@RequestMapping(value="identifier/{identifierType}/{identifier}.{format:json|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> autocompleteIdentifier(HttpServletRequest request, @PathVariable String identifierType, @PathVariable String identifier, @PathVariable String format) {
		return this.doAutocompleteIdentifier(request, DEFAULT_PROCEDURE, NO_SPECIFIC_SOURCE, identifierType, GROUP_BY_UID, DEFAULT_MAX_ENTRIES, identifier, format);
	}
}