/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder)
 */
package org.fao.fi.vrmf.web.apps.finder.controllers.services.pub.vessels.retrieve;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;

import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.vrmf.common.j2ee.utilities.ServletsHelper;
import org.fao.fi.vrmf.common.models.security.VRMFUser;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.VesselRecordSearchFilter;
import org.fao.fi.vrmf.common.search.dsl.support.models.vessels.record.VesselRecord;
import org.fao.fi.vrmf.common.web.controllers.services.pub.vessels.retrieve.CommonRetrieveVesselDataController;
import org.fao.fi.vrmf.web.apps.finder.controllers.services.pub.PublicServicesConstants;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 11 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 11 May 2012
 */
@Named @Singleton @Controller
@RequestMapping(PublicServicesConstants.VESSELS_SERVICES_PREFIX + "get/*")
public class GetVesselDataByIDAndUIDController extends CommonRetrieveVesselDataController<VesselRecord, VesselRecordSearchFilter> {
	static final protected String[] DEFAULT_EXCLUSION_PATTERNS = {
		"*.checksum",
		"*.currentFlag", 
		"*.currentFlagId",
		"*.currentName",
		"*.currentLength",
		"*.currentTonnage",
//		"*.mappingComment",
//		"*.mappingDate",
//		"*.mappingUser",
//		"*.mappingWeight",
//		"*.mapsTo",
		"*.empty",
		"*.allIdentifiers",
		"*.identifiers",
		"*.flags",
		"*.shipbuildersData",
		"*.hullMaterialData",
		"*.statusData",
		"*.typeData",
		"*.gearData",
		"*.callsigns",
		"*.registrationData",
		"*.fishingLicenses",
		"*.lengths",
		"*.tonnages",
		"*.powers",
		"*.beneficialOwnersData",
		"*.ownersData",
		"*.operatorsData",
		"*.mastersData",
		"*.fishingMastersData",
		"*.authorizationsData",
		"*.comment",
		"*.keywords"
	};

	@RequestMapping(value="UID/{UID:[0-9]+$}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> getByUID(HttpServletRequest request, @PathVariable Integer UID) {
		return this.getByUID(request, UID, "json", null); //Defaults to JSON if no file suffix is provided in the URL
	}
	
	@RequestMapping(value="UID/{UID:[0-9]+}.{format:json|jsonp|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> getByUID(HttpServletRequest request, @PathVariable Integer UID, @PathVariable String format, @RequestParam(required=false, value="jsonpcallback") String jsonpCallback) {
		try {
			VRMFUser loggedUser = ServletsHelper.getLoggedUser(request);
			
			return this.doRetrieveByUID(request, 
										INCLUDE_OTHER_SOURCES_BY_DEFAULT, //Limit just to the logged user sources 
										null, //no specific sources
										UID, 
										format, 
										jsonpCallback,
										CollectionsHelper.append(DEFAULT_EXCLUSION_PATTERNS, loggedUser == null || !loggedUser.can("READ_I_DATA") ? new String[] { "*.internalIdentifiers" } : new String[0]), 
										NO_REQUIRED_ROLES, 
										NO_REQUIRED_CAPABILITIES);
		} catch (Throwable t) {
			return this.manageError(t);
		}
	}
	
	@RequestMapping(value="ID/{ID:[0-9]+$}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> getByID(HttpServletRequest request, @PathVariable Integer ID) {
		return this.getByID(request, ID, "json", null); //Defaults to JSON if no file suffix is provided in the URL
	}
	
	@RequestMapping(value="ID/{ID:[0-9]+}.{format:json|jsonp|xml}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> getByID(HttpServletRequest request, @PathVariable Integer ID, @PathVariable String format, @RequestParam(required=false, value="jsonpcallback") String jsonpCallback) {
		try {
			VRMFUser loggedUser = ServletsHelper.getLoggedUser(request);
			
			return this.doRetrieveByID(request, 
									   INCLUDE_OTHER_SOURCES_BY_DEFAULT, //Limit just to the logged user sources 
									   null, //no specific sources
									   ID, 
									   format, 
									   jsonpCallback,
									   CollectionsHelper.append(DEFAULT_EXCLUSION_PATTERNS, loggedUser == null || !loggedUser.can("READ_I_DATA") ? new String[] { "*.internalIdentifiers" } : new String[0]), 
									   NO_REQUIRED_ROLES, 
									   NO_REQUIRED_CAPABILITIES);
		} catch (Throwable t) {
			return this.manageError(t);
		}
	}

	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.web.controllers.services.pub.vessels.retrieve.AbstractRetrieveVesselDataController#getEmptySearchFilter()
	 */
	@Override
	protected VesselRecordSearchFilter getEmptySearchFilter() {
		return new VesselRecordSearchFilter();
	}
}