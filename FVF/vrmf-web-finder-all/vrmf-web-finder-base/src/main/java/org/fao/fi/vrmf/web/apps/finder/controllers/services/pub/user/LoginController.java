/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder)
 */
package org.fao.fi.vrmf.web.apps.finder.controllers.services.pub.user;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;

import org.fao.fi.vrmf.business.dao.UserManagementDAO;
import org.fao.fi.vrmf.common.j2ee.utilities.ServletsHelper;
import org.fao.fi.vrmf.common.models.security.VRMFUser;
import org.fao.fi.vrmf.common.web.controllers.AbstractController;
import org.fao.fi.vrmf.web.apps.finder.controllers.services.pub.PublicServicesConstants;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 25 Apr 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 25 Apr 2012
 */
@Singleton @Controller
@RequestMapping(PublicServicesConstants.USER_SERVICES_PREFIX + "*")
public class LoginController extends AbstractController {
	@Inject
	private UserManagementDAO _userManagementDAO;
	
	/**
	 * @return the 'userManagementDAO' value
	 */
	public UserManagementDAO getUserManagementDAO() {
		return this._userManagementDAO;
	}

	/**
	 * @param userManagementDAO the 'userManagementDAO' value to set
	 */
	public void setUserManagementDAO(UserManagementDAO userManagementDAO) {
		this._userManagementDAO = userManagementDAO;
	}

	@RequestMapping(value="login", method=RequestMethod.GET)
	public ResponseEntity<byte[]> login(HttpServletRequest servletRequest, @RequestParam String username, @RequestParam String password) {
		try {
			VRMFUser user = this._userManagementDAO.login(username, password);
			
			if(user == null)
				return new ResponseEntity<byte[]>(HttpStatus.UNAUTHORIZED);
			
			if(user.is("BROWSER_USER")) {
				ServletsHelper.setLoggedUser(servletRequest, user);
				
				return new ResponseEntity<byte[]>(HttpStatus.OK);
			}
			
			return new ResponseEntity<byte[]>(HttpStatus.FORBIDDEN);
		} catch (Throwable t) {
			return this.manageError(t);
		}
	}
	
	@RequestMapping(value="logout", method=RequestMethod.GET)
	public ResponseEntity<byte[]> logout(HttpServletRequest servletRequest) {
		try {
			ServletsHelper.setLoggedUser(servletRequest, null);
			
			return new ResponseEntity<byte[]>(HttpStatus.OK);
		} catch (Throwable t) {
			return this.manageError(t);
		}
	}
}