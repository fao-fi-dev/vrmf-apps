/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder)
 */
package org.fao.fi.vrmf.web.apps.finder.controllers.services.pub.vessels.search;

import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.AUTHORIZATION_AT_DATE;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.AUTHORIZATION_ISSUERS;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.AUTHORIZATION_ISSUING_COUNTRIES;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.AUTHORIZATION_STATUS;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.AUTHORIZATION_TYPES;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.AUX_ENGINE_POWER_RANGES;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.AUX_ENGINE_POWER_TYPES;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.BUILDING_YEAR_MAX;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.BUILDING_YEAR_MIN;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.CALLSIGNS;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.CALLSIGNS_LIKE;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.EXTERNAL_MARKINGS;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.EXTERNAL_MARKING_LIKE;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.FISHING_LICENSE_ISSUING_COUNTRIES;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.FISHING_LICENSE_NUMBERS;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.FISHING_LICENSE_NUMBER_LIKE;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.IDENTIFIERS;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.IDENTIFIER_LIKE;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.IDENTIFIER_TYPE;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.LENGTH_RANGES;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.LENGTH_TYPES;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.MAIN_ENGINE_POWER_RANGES;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.MAIN_ENGINE_POWER_TYPES;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.MMSIS;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.MMSI_LIKE;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.NAMES;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.NAME_LIKE;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.NO_FLAG;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.NO_LENGTH;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.NO_PARENT_TYPE;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.NO_TYPE;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.PARENT_TYPE;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.PRIMARY_GEAR_TYPE;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.REGISTRATION_COUNTRIES;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.REGISTRATION_NUMBERS;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.REGISTRATION_NUMBER_LIKE;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.REGISTRATION_PORTS;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.SECONDARY_GEAR_TYPE;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.STATUS;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.TONNAGE_RANGES;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.TONNAGE_TYPES;
import static org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses.TYPE;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.vrmf.business.dao.VesselRecordSearchDAO;
import org.fao.fi.vrmf.common.j2ee.RequestAttributesConstants;
import org.fao.fi.vrmf.common.j2ee.utilities.ServletsHelper;
import org.fao.fi.vrmf.common.models.search.DoubleRange;
import org.fao.fi.vrmf.common.search.dsl.ColumnRegistry;
import org.fao.fi.vrmf.common.search.dsl.impl.Column;
import org.fao.fi.vrmf.common.search.dsl.impl.common.meta.CommonDataColumns;
import org.fao.fi.vrmf.common.search.dsl.impl.meta.GroupableDataColumns;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.VesselRecordSearchFilter;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.VesselRecordSelector;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.VesselRecordSorter;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.VesselRecordWhere;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.authorized.support.AuthorizationStatus;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordClauses;
import org.fao.fi.vrmf.common.web.DataCustomizer;
import org.fao.fi.vrmf.common.web.constants.services.vessels.CommonSearchVesselsServiceConstants;
import org.fao.fi.vrmf.common.web.controllers.AbstractController;
import org.fao.fi.vrmf.web.apps.finder.SearchServiceConstants;

import lombok.Getter;
import lombok.Setter;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 4 Mar 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 4 Mar 2013
 */
abstract public class AbstractSearchControllerBase extends AbstractController {
	@Inject @Getter @Setter protected DataCustomizer dataCustomizer;
	@Inject @Getter @Setter protected ColumnRegistry columnRegistry;
	@Inject @Getter @Setter protected VesselRecordSearchDAO searchVesselsDAO;
	
	@Getter @Setter protected Integer defaultPageSize = 30;
	@Getter @Setter protected Integer maxPageSize = 200;
	
	static protected String[] JSON_RESPONSE_INCLUSION_PATTERNS = { 
		"error", 
		"errorMessage", 
		"additionalErrorMessage", 
		"trace", 
		"elapsed",
		"data.pageSize", 
		"data.currentResultPageSize",
		"data.fullResultsSize",
		"data.dataSet.*"
	};
	
	protected Column findColumnByMnemonic(String mnemonic) { 
		Column column = columnRegistry.byMnemonic(mnemonic);
		
		if(column != null)
			return column;
		
		return CommonDataColumns.NOP;
	}
	
	protected VesselRecordSearchFilter setOrderClauses(VesselRecordSearchFilter filter, String[] pSortingCriteria) {
		VesselRecordSorter sortableColumns = filter.sortableColumns();
		
		if(pSortingCriteria != null && pSortingCriteria.length > 0) {
			String mnemonic, order;
			
			for(String sortCriteria : pSortingCriteria) {
				mnemonic = sortCriteria.split("_")[0];
				order = sortCriteria.split("_")[1];
				
				sortableColumns.sort(this.findColumnByMnemonic(mnemonic), "a".equalsIgnoreCase(order));
			}
		}
		
		return filter;
	}

	protected VesselRecordSearchFilter buildFilter(HttpServletRequest request) throws Throwable {
		Integer pOffset = ServletsHelper.getIntParameter(SearchServiceConstants.RESULT_OFFSET_PARAMETER, request);
		Integer pPageSize = ServletsHelper.getIntParameter(SearchServiceConstants.RESULT_PAGE_SIZE_PARAMETER, request);
	
		if(pPageSize == null || pPageSize > this.maxPageSize)
			pPageSize = this.defaultPageSize;
		
		String pLang = StringsHelper.trim((String)request.getAttribute(RequestAttributesConstants.CURRENT_LANGUAGE_REQUEST_ATTRIBUTE));
		
		if(pLang == null)
			pLang = "en";
	
		Boolean pLimitToCurrentData = Boolean.parseBoolean(ServletsHelper.getParameter(SearchServiceConstants.LIMIT_TO_CURRENT_DATA_PARAMETER, request));
		Long pAtDate = ServletsHelper.getLongParameter(SearchServiceConstants.AT_DATE_PARAMETER, request);
	
		Boolean pGroupDuplicates = ServletsHelper.getBooleanParameter(SearchServiceConstants.GROUP_DUPLICATES_PARAMETER, Boolean.TRUE, request);
		Boolean pSearchDuplicatesOnly = ServletsHelper.getBooleanParameter(SearchServiceConstants.SEARCH_DUPLICATES_ONLY_PARAMETER, Boolean.FALSE, request);
	
		String[] pSourceSystems = ServletsHelper.getMultipleParameter(SearchServiceConstants.SOURCE_SYSTEM_PARAMETER, request);
	
		Integer pNumSourcesFrom = ServletsHelper.getIntParameter(CommonSearchVesselsServiceConstants.NUM_SOURCES_FROM, request);
		Integer pNumSourcesTo   = ServletsHelper.getIntParameter(CommonSearchVesselsServiceConstants.NUM_SOURCES_TO, request);
	
		Long pUpdatedFrom = ServletsHelper.getLongParameter(CommonSearchVesselsServiceConstants.UPDATED_FROM, request);
		Long pUpdatedTo = ServletsHelper.getLongParameter(CommonSearchVesselsServiceConstants.UPDATED_TO, request);
	
		String pIdentifierType = ServletsHelper.getParameter(CommonSearchVesselsServiceConstants.IDENTIFIER_TYPE_PARAMETER, request);
		String pIdentifier = ServletsHelper.getParameter(CommonSearchVesselsServiceConstants.IDENTIFIER_VALUE_PARAMETER, request);
	
		String[] pNames = ServletsHelper.getMultipleEscapedStringParameter(CommonSearchVesselsServiceConstants.VESSEL_NAME_PARAMETER, request);
		
		Object pExternalMarking = ServletsHelper.getEscapedParameter(CommonSearchVesselsServiceConstants.VESSEL_EXT_MARKING_PARAMETER, request);
		pExternalMarking = pExternalMarking == null ? null : new String((byte[])pExternalMarking, "UTF-8");		
	
		Boolean pNoFlag = ServletsHelper.getBooleanParameter(CommonSearchVesselsServiceConstants.NO_FLAG, Boolean.FALSE, request);
		Integer[] pFlags = ServletsHelper.getMultipleIntParameter(CommonSearchVesselsServiceConstants.VESSEL_FLAG_PARAMETER, request);
	
		String[] pStatuses = ServletsHelper.getMultipleParameter(CommonSearchVesselsServiceConstants.VESSEL_STATUS_PARAMETER, request);
	
		Boolean pNoType = ServletsHelper.getBooleanParameter(CommonSearchVesselsServiceConstants.NO_TYPE, Boolean.FALSE, request);
		String pTypes = ServletsHelper.getParameter(CommonSearchVesselsServiceConstants.VESSEL_TYPE_PARAMETER, request);
	
		Boolean pNoParentType = ServletsHelper.getBooleanParameter(CommonSearchVesselsServiceConstants.NO_PARENT_TYPE, Boolean.FALSE, request);
		String pParentTypes = ServletsHelper.getParameter(CommonSearchVesselsServiceConstants.PARENT_VESSEL_TYPE_PARAMETER, request);
	
		String pPrimaryGearTypes = ServletsHelper.getParameter(CommonSearchVesselsServiceConstants.VESSEL_PRIMARY_GEAR_TYPE_PARAMETER, request);
		String pSecondaryGearTypes = ServletsHelper.getParameter(CommonSearchVesselsServiceConstants.VESSEL_SECONDARY_GEAR_TYPE_PARAMETER, request);
	
		String pIRCS = ServletsHelper.getParameter(CommonSearchVesselsServiceConstants.VESSEL_IRCS_PARAMETER, request);
		String pMMSI = ServletsHelper.getParameter(CommonSearchVesselsServiceConstants.VESSEL_MMSI_PARAMETER, request);
	
		Integer[] pRegCountries = ServletsHelper.getMultipleIntParameter(CommonSearchVesselsServiceConstants.VESSEL_REG_COUNTRY_PARAMETER, request);
		Integer[] pRegPorts = ServletsHelper.getMultipleIntParameter(CommonSearchVesselsServiceConstants.VESSEL_REG_PORT_PARAMETER, request);
		
		Object pRegNo = ServletsHelper.getEscapedParameter(CommonSearchVesselsServiceConstants.VESSEL_REG_NO_PARAMETER, request);
		pRegNo = pRegNo == null ? null : new String((byte[])pRegNo, "UTF-8");
	
		Integer[] pFishingLicenseCountries = ServletsHelper.getMultipleIntParameter(CommonSearchVesselsServiceConstants.VESSEL_F_LIC_COUNTRY_PARAMETER, request);
		
		Object pFishingLicense = ServletsHelper.getEscapedParameter(CommonSearchVesselsServiceConstants.VESSEL_F_LIC_NUMBER_PARAMETER, request);
		pFishingLicense = pFishingLicense == null ? null : new String((byte[])pFishingLicense, "UTF-8");
	
		Boolean pNoLength = ServletsHelper.getBooleanParameter(CommonSearchVesselsServiceConstants.NO_LENGTH, Boolean.FALSE, request);
		String[] pLengthTypes = ServletsHelper.getMultipleParameter(CommonSearchVesselsServiceConstants.VESSEL_LENGTH_TYPES, request);
		String[] pLengthRanges = ServletsHelper.getMultipleParameter(CommonSearchVesselsServiceConstants.VESSEL_LENGTHS, request);
	
		Integer pAgeMin = ServletsHelper.getIntParameter(CommonSearchVesselsServiceConstants.VESSEL_BUILDING_YEAR_MAX_PARAMETER, request);
		Integer pAgeMax = ServletsHelper.getIntParameter(CommonSearchVesselsServiceConstants.VESSEL_BUILDING_YEAR_MIN_PARAMETER, request);
	
		String[] pTonnageTypes = ServletsHelper.getMultipleParameter(CommonSearchVesselsServiceConstants.VESSEL_TONNAGE_TYPES, request);
		Double pTonnageMin = ServletsHelper.getDoubleParameter(CommonSearchVesselsServiceConstants.VESSEL_TONNAGE_MIN, request);
		Double pTonnageMax = ServletsHelper.getDoubleParameter(CommonSearchVesselsServiceConstants.VESSEL_TONNAGE_MAX, request);
	
		String[] pMainEngineTypes = ServletsHelper.getMultipleParameter(CommonSearchVesselsServiceConstants.VESSEL_MAIN_ENGINE_TYPES, request);
		Double pMainEngineMin = ServletsHelper.getDoubleParameter(CommonSearchVesselsServiceConstants.VESSEL_MAIN_ENGINE_MIN, request);
		Double pMainEngineMax = ServletsHelper.getDoubleParameter(CommonSearchVesselsServiceConstants.VESSEL_MAIN_ENGINE_MAX, request);
	
		String[] pAuxEngineTypes = ServletsHelper.getMultipleParameter(CommonSearchVesselsServiceConstants.VESSEL_AUXILIARY_ENGINE_TYPES, request);
		Double pAuxEngineMin = ServletsHelper.getDoubleParameter(CommonSearchVesselsServiceConstants.VESSEL_AUXILIARY_ENGINE_MIN, request);
		Double pAuxEngineMax = ServletsHelper.getDoubleParameter(CommonSearchVesselsServiceConstants.VESSEL_AUXILIARY_ENGINE_MAX, request);
	
		String[] pAuthIssuingAuthorities = ServletsHelper.getMultipleParameter(CommonSearchVesselsServiceConstants.VESSEL_AUTH_ISSUING_AUTHORITY, request);
		Integer[] pAuthIssuingCountries = ServletsHelper.getMultipleIntParameter(CommonSearchVesselsServiceConstants.VESSEL_AUTH_ISSUING_COUNTRY, request);
		String[] pAuthTypes = ServletsHelper.getMultipleParameter(CommonSearchVesselsServiceConstants.VESSEL_AUTH_TYPE, request);
	
		String pAuthStatus = ServletsHelper.getParameter(CommonSearchVesselsServiceConstants.VESSEL_AUTH_STATUS, request);
		Long pAuthAtDate = ServletsHelper.getLongParameter(CommonSearchVesselsServiceConstants.VESSEL_AUTH_STATUS_AT_DATE, request);
	
		String[] pSortingCriteria = ServletsHelper.getMultipleParameter(SearchServiceConstants.SORTING_CRITERIA_PARAMETER, request);
		
		String[] availableSourceSystems = this.getAvailableSources(request);
		String[] selectedSourceSystems = pSourceSystems == null ? this.getAvailableSources(request) : AbstractController.intersectSourceSystems(pSourceSystems, availableSourceSystems);
		
		Date atDate = pLimitToCurrentData ? new Date() : pAtDate != null ? new Date(pAtDate) : null;
		atDate = atDate == null ? null : this.truncate(atDate);
			
		boolean linkMetadataToSearchCriteria = pGroupDuplicates && ( pLimitToCurrentData || pAtDate != null );
		
		List<Integer> parentVesselTypeCodes = null;
		List<Integer> vesselTypeCodes = null;
		List<Integer> primaryGearTypeCodes = null;
		List<Integer> secondaryGearTypeCodes = null;
		
		if(StringsHelper.trim(pParentTypes) != null) {
			parentVesselTypeCodes = new ArrayList<Integer>();
			
			for(String part : StringsHelper.trim(pParentTypes).split("\\,"))
				parentVesselTypeCodes.add(Integer.valueOf(part));
		}
		
		if(StringsHelper.trim(pTypes) != null) {
			vesselTypeCodes = new ArrayList<Integer>();
			
			for(String part : StringsHelper.trim(pTypes).split("\\,"))
				vesselTypeCodes.add(Integer.valueOf(part));
		}
		
		if(StringsHelper.trim(pPrimaryGearTypes) != null){
			primaryGearTypeCodes = new ArrayList<Integer>();
	
			for(String part : pPrimaryGearTypes.split("\\,"))
				primaryGearTypeCodes.add(Integer.valueOf(part));
		}
		
		if(StringsHelper.trim(pSecondaryGearTypes) != null){
			secondaryGearTypeCodes = new ArrayList<Integer>();
	
			for(String part : pSecondaryGearTypes.split("\\,"))
				secondaryGearTypeCodes.add(Integer.valueOf(part));
		}
		
		List<DoubleRange> lengthRanges = null;
	
		if(pLengthRanges != null && pLengthRanges.length > 0) {
			lengthRanges = new ArrayList<DoubleRange>();
		
			DoubleRange lengthRange;
			for(String range : pLengthRanges) {
				lengthRange = new DoubleRange();
	
				try {
					lengthRange.setMin(Double.parseDouble(range.substring(0, range.indexOf("_"))));
				} catch(Throwable t) {
					this._log.warn("Unparseable length bottom range for " + range + ": assuming bottom range is not set...");
				}
	
				try {
					lengthRange.setMax(Double.parseDouble(range.substring(range.indexOf("_") + 1)));
				} catch(Throwable t) {
					this._log.warn("Unparseable length top range for " + range + ": assuming top range is not set...");
				}
	
				if(lengthRange.getMin() != null || lengthRange.getMax() != null)
					lengthRanges.add(lengthRange);
			}
		}
	
		VesselRecordSearchFilter filter = new VesselRecordSearchFilter().FROM(dataCustomizer.getTargetTable(request));
		
		VesselRecordSelector columns = filter.columns();
		
		columns.
			include(
				dataCustomizer.getDataColumns(request)
			);
		
		VesselRecordWhere conditions = filter.clauses();
	
		conditions.
			and(IDENTIFIER_TYPE, pIdentifierType).
			and(IDENTIFIERS, pIdentifier != null ? new String[] { pIdentifier } : null).
			and(IDENTIFIER_LIKE, pIdentifier == null ? null : this.useLike(pIdentifier)).
			and(NAMES, (Object[])pNames).
			and(NAME_LIKE, pNames == null || pNames.length == 0 ? null : this.useLike(pNames)).
			and(EXTERNAL_MARKINGS, pExternalMarking == null ? null : new String[] { (String)pExternalMarking }).
			and(EXTERNAL_MARKING_LIKE, pExternalMarking == null ? null : this.useLike((String)pExternalMarking)).
			and(NO_FLAG, pNoFlag).
			and(VesselRecordClauses.FLAG, (Object[])pFlags).
			and(STATUS, (Object[])pStatuses).
			and(BUILDING_YEAR_MIN, pAgeMin != null && pAgeMin >= 0 ? Calendar.getInstance().get(Calendar.YEAR) - pAgeMin : null).
			and(BUILDING_YEAR_MAX, pAgeMax != null && pAgeMax >= 0 ? Calendar.getInstance().get(Calendar.YEAR) - pAgeMax : null).
			and(NO_TYPE, Boolean.FALSE.equals(pNoType) ? null : pNoType).
			and(TYPE, vesselTypeCodes != null && !vesselTypeCodes.isEmpty() ? vesselTypeCodes.toArray(new Integer[vesselTypeCodes.size()]) : null).
			and(NO_PARENT_TYPE, Boolean.FALSE.equals(pNoParentType) ? null : pNoParentType).
			and(PARENT_TYPE, parentVesselTypeCodes != null && !parentVesselTypeCodes.isEmpty() ? parentVesselTypeCodes.toArray(new Integer[parentVesselTypeCodes.size()]) : null).
			and(PRIMARY_GEAR_TYPE, primaryGearTypeCodes != null && !primaryGearTypeCodes.isEmpty() ? primaryGearTypeCodes.toArray(new Integer[primaryGearTypeCodes.size()]) : null).
			and(SECONDARY_GEAR_TYPE, secondaryGearTypeCodes != null && !secondaryGearTypeCodes.isEmpty() ? secondaryGearTypeCodes.toArray(new Integer[secondaryGearTypeCodes.size()]) : null).
			and(CALLSIGNS, pIRCS != null ? new String[] { StringsHelper.trim(pIRCS) } : null).
			and(CALLSIGNS_LIKE, pIRCS == null ? null : this.useLike(pIRCS)).
			and(MMSIS, StringsHelper.trim(pMMSI) != null ? new String[] { StringsHelper.trim(pMMSI) } : null).
			and(MMSI_LIKE, pMMSI == null ? null : this.useLike(pMMSI)).
			and(REGISTRATION_COUNTRIES, pRegCountries != null && pRegCountries.length > 0 ? pRegCountries : null).
			and(REGISTRATION_PORTS, pRegPorts != null && pRegPorts.length > 0 ? pRegPorts : null).
			and(REGISTRATION_NUMBERS, pRegNo != null ? new String[] { (String)pRegNo } : null).
			and(REGISTRATION_NUMBER_LIKE, pRegNo == null ? null : this.useLike((String)pRegNo)).
			and(FISHING_LICENSE_ISSUING_COUNTRIES, (Object[])pFishingLicenseCountries).
			and(FISHING_LICENSE_NUMBERS, pFishingLicense != null ? new String[] { (String)pFishingLicense } : null).
			and(FISHING_LICENSE_NUMBER_LIKE, pFishingLicense == null ? null : this.useLike((String)pFishingLicense)).
			and(NO_LENGTH, Boolean.FALSE.equals(pNoLength) ? null : pNoLength).
			and(LENGTH_TYPES, (Object[])pLengthTypes).
			and(LENGTH_RANGES, lengthRanges != null && !lengthRanges.isEmpty() ? lengthRanges.toArray(new DoubleRange[lengthRanges.size()]) : null ).
			and(TONNAGE_TYPES, (Object[])pTonnageTypes).
			and(TONNAGE_RANGES, pTonnageMin != null || pTonnageMax != null ? new DoubleRange[] { new DoubleRange(pTonnageMin, pTonnageMax) } : null ).
			and(MAIN_ENGINE_POWER_TYPES, (Object[])pMainEngineTypes).
			and(MAIN_ENGINE_POWER_RANGES, pMainEngineMin != null || pMainEngineMax != null ? new DoubleRange[] { new DoubleRange(pMainEngineMin, pMainEngineMax) }  : null ).
			and(AUX_ENGINE_POWER_TYPES, (Object[])pAuxEngineTypes).
			and(AUX_ENGINE_POWER_RANGES, pAuxEngineMin != null || pAuxEngineMax != null ? new DoubleRange[] { new DoubleRange(pAuxEngineMin, pAuxEngineMax) }  : null ).
			and(AUTHORIZATION_ISSUERS, (Object[])pAuthIssuingAuthorities).
			and(AUTHORIZATION_TYPES, (Object[])pAuthTypes).
			and(AUTHORIZATION_ISSUING_COUNTRIES, (Object[])pAuthIssuingCountries).
			and(AUTHORIZATION_STATUS, pAuthStatus == null ? null : AuthorizationStatus.fromStatus(pAuthStatus)).
			and(AUTHORIZATION_AT_DATE, pAuthAtDate == null ? null : this.truncate(new Date(pAuthAtDate))).
			nop(); //LEFT TO ALLOW EASY INSERTION OF ADDITIONAL CRITERIA BEFORE THIS
	
		
		filter = this.setOrderClauses(filter, pSortingCriteria);
		
		VesselRecordSorter sortableColumns = filter.sortableColumns();
		
		filter.
			AMONG(availableSourceSystems).
			SOURCED_BY(selectedSourceSystems).
			updatedBetween(this.truncate(pUpdatedFrom == null ? null : new Date(pUpdatedFrom)), this.truncate(pUpdatedTo == null ? null : new Date(pUpdatedTo))).
			AT(atDate).
			retrieveAdditionalMetadata().
		SELECT(columns).
		WHERE(conditions).
		ORDER_BY(sortableColumns).
		GROUP_BY(Boolean.TRUE.equals(pGroupDuplicates) ? GroupableDataColumns.UID : GroupableDataColumns.ID).
		OFFSET(pOffset).
		LIMIT(pPageSize).
		lang(pLang);
	
		if(pSearchDuplicatesOnly)
			filter.duplicatesOnly();
		
		if(linkMetadataToSearchCriteria)
			filter.linkToSearch();
		
		if(pNumSourcesFrom != null)
			filter.numberOfSourcesFrom(pNumSourcesFrom);
		
		if(pNumSourcesTo != null)
			filter.numberOfSourcesTo(pNumSourcesTo);
		
		return filter;
	}

	protected boolean useLike(byte[] value) {
		try {
			return this.useLike(value == null ? null : new String(value, "UTF-8"));
		} catch(UnsupportedEncodingException UEe) {
			return this.useLike(new String(value));
		}
	}

	protected boolean useLike(byte[]... values) {
		if(values == null || values.length == 0)
			return false;
		
		for(byte[] value : values) {
			if(this.useLike(value))
				return true;
		}
		
		return false; 
	}

	protected boolean useLike(String value) {
		String trimmed = StringsHelper.trim(value);
		
		return trimmed != null && ( trimmed.indexOf("%") >= 0 || trimmed.indexOf("_") >= 0); 
	}

	protected boolean useLike(String... values) {
		if(values == null || values.length == 0)
			return false;
		
		for(String value : values) {
			if(this.useLike(value))
				return true;
		}
		
		return false; 
	}
}
