/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder-common)
 */
package org.fao.fi.vrmf.web.apps.finder.common.models.extended.upload;

import java.util.Date;

import org.fao.fi.sh.utility.common.helpers.beans.text.XMLPrettyPrinter;
import org.fao.fi.sh.utility.common.helpers.singletons.lang.BeansHelper;
import org.fao.fi.vrmf.common.core.model.GenericData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 May 2012
 */
public class ImportRecord extends GenericData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -7118446049377027808L;
	
	private Integer _VRMFID;
	
	private String _source;
	private String _typeUpdate;
	private Date _dateUpdated;
	private String _vesselId;
	private Integer _tuvi;
	private String _vesselName;
	private String _flagCode;
	private String _vesselTypeCode;
	private String _gearTypeCode;
	private Double _length;
	private String _lengthTypeCode;
	private String _ircs;
	private String _nrn;
	private String _imo;
	private Double _tonnage;
	private String _tonnageTypeCode;
	private String _previousName;
	private String _previousFlagCode;
	private Date _dateAutStart;
	private Date _dateAutEnd;
	private String _urlid;
	private Date _timeStamp;
	
	/**
	 * Class constructor
	 *
	 */
	public ImportRecord() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param source
	 * @param typeUpdate
	 * @param dateUpdated
	 * @param vesselID
	 * @param tUVI
	 * @param vesselName
	 * @param flagCode
	 * @param vesselTypeCode
	 * @param gearTypeCode
	 * @param length
	 * @param lengthTypeCode
	 * @param iRCS
	 * @param nRN
	 * @param iMO
	 * @param tonnage
	 * @param tonnageTypeCode
	 * @param previousName
	 * @param previousFlagCode
	 * @param dateAutStart
	 * @param dateAutEnd
	 * @param uRLID
	 * @param timestamp
	 */
	public ImportRecord(Integer VRMFID, String source, String typeUpdate, Date dateUpdated, String vesselID, Integer tUVI, String vesselName, String flagCode, String vesselTypeCode, String gearTypeCode, Double length, String lengthTypeCode, String iRCS, String nRN,
			String iMO, Double tonnage, String tonnageTypeCode, String previousName, String previousFlagCode, Date dateAutStart, Date dateAutEnd, String uRLID, Date timestamp) {
		super();
		this._VRMFID = VRMFID;
		this._source = source;
		this._typeUpdate = typeUpdate;
		this._dateUpdated = dateUpdated;
		this._vesselId = vesselID;
		this._tuvi = tUVI;
		this._vesselName = vesselName;
		this._flagCode = flagCode;
		this._vesselTypeCode = vesselTypeCode;
		this._gearTypeCode = gearTypeCode;
		this._length = length;
		this._lengthTypeCode = lengthTypeCode;
		this._ircs = iRCS;
		this._nrn = nRN;
		this._imo = iMO;
		this._tonnage = tonnage;
		this._tonnageTypeCode = tonnageTypeCode;
		this._previousName = previousName;
		this._previousFlagCode = previousFlagCode;
		this._dateAutStart = dateAutStart;
		this._dateAutEnd = dateAutEnd;
		this._urlid = uRLID;
		this._timeStamp = timestamp;
	}
	
	/**
	 * Class constructor
	 *
	 */
	public ImportRecord(ImportRecord data) {
		super();
		
		try {
			BeansHelper.transferBean(data, this);
		} catch (Throwable t) {
			throw new RuntimeException(t);
		}
	}

	/**
	 * @return the 'vRMFID' value
	 */
	public Integer getVRMFID() {
		return this._VRMFID;
	}

	/**
	 * @param vRMFID the 'vRMFID' value to set
	 */
	public void setVRMFID(Integer vRMFID) {
		this._VRMFID = vRMFID;
	}

	/**
	 * @return the 'source' value
	 */
	public String getSource() {
		return this._source;
	}
	/**
	 * @param source the 'source' value to set
	 */
	public void setSource(String source) {
		this._source = source;
	}
	/**
	 * @return the 'typeUpdate' value
	 */
	public String getTypeUpdate() {
		return this._typeUpdate;
	}
	/**
	 * @param typeUpdate the 'typeUpdate' value to set
	 */
	public void setTypeUpdate(String typeUpdate) {
		this._typeUpdate = typeUpdate;
	}
	/**
	 * @return the 'dateUpdated' value
	 */
	public Date getDateUpdated() {
		return this._dateUpdated;
	}
	/**
	 * @param dateUpdated the 'dateUpdated' value to set
	 */
	public void setDateUpdated(Date dateUpdated) {
		this._dateUpdated = dateUpdated;
	}
	/**
	 * @return the 'vesselID' value
	 */
	public String getVesselId() {
		return this._vesselId;
	}
	/**
	 * @param vesselID the 'vesselID' value to set
	 */
	public void setVesselId(String vesselID) {
		if(vesselID != null)
			vesselID = vesselID.replaceAll("\\.\\d+$", "");
		
		this._vesselId = vesselID;
	}
	/**
	 * @return the 'tUVI' value
	 */
	public Integer getTuvi() {
		return this._tuvi;
	}
	/**
	 * @param tUVI the 'tUVI' value to set
	 */
	public void setTuvi(Integer tUVI) {
		this._tuvi = tUVI;
	}
	/**
	 * @return the 'vesselName' value
	 */
	public String getVesselName() {
		return this._vesselName;
	}
	/**
	 * @param vesselName the 'vesselName' value to set
	 */
	public void setVesselName(String vesselName) {
		this._vesselName = vesselName;
	}
	/**
	 * @return the 'flagCode' value
	 */
	public String getFlagCode() {
		return this._flagCode;
	}
	/**
	 * @param flagCode the 'flagCode' value to set
	 */
	public void setFlagCode(String flagCode) {
		this._flagCode = flagCode;
	}
	/**
	 * @return the 'vesselTypeCode' value
	 */
	public String getVesselTypeCode() {
		return this._vesselTypeCode;
	}
	/**
	 * @param vesselTypeCode the 'vesselTypeCode' value to set
	 */
	public void setVesselTypeCode(String vesselTypeCode) {
		this._vesselTypeCode = vesselTypeCode;
	}
	/**
	 * @return the 'gearTypeCode' value
	 */
	public String getGearTypeCode() {
		return this._gearTypeCode;
	}
	/**
	 * @param gearTypeCode the 'gearTypeCode' value to set
	 */
	public void setGearTypeCode(String gearTypeCode) {
		this._gearTypeCode = gearTypeCode;
	}
	/**
	 * @return the 'length' value
	 */
	public Double getLength() {
		return this._length;
	}
	/**
	 * @param length the 'length' value to set
	 */
	public void setLength(Double length) {
		if(length != null)
			length = Math.round(length * 100D) * 0.01D;
		
		this._length = length;
	}
	/**
	 * @return the 'lengthTypeCode' value
	 */
	public String getLengthTypeCode() {
		return this._lengthTypeCode;
	}
	/**
	 * @param lengthTypeCode the 'lengthTypeCode' value to set
	 */
	public void setLengthTypeCode(String lengthTypeCode) {
		this._lengthTypeCode = lengthTypeCode;
	}
	/**
	 * @return the 'iRCS' value
	 */
	public String getIrcs() {
		return this._ircs;
	}
	/**
	 * @param iRCS the 'iRCS' value to set
	 */
	public void setIrcs(String iRCS) {
		this._ircs = iRCS;
	}
	/**
	 * @return the 'nRN' value
	 */
	public String getNrn() {
		return this._nrn;
	}
	/**
	 * @param nRN the 'nRN' value to set
	 */
	public void setNrn(String nRN) {
		this._nrn = nRN;
	}
	/**
	 * @return the 'iMO' value
	 */
	public String getImo() {
		return this._imo;
	}
	/**
	 * @param iMO the 'iMO' value to set
	 */
	public void setImo(String iMO) {
		if(iMO != null) {
			iMO = iMO.replaceAll("\\.\\d+$", "");
		}
		
		this._imo = iMO;
	}
	/**
	 * @return the 'tonnage' value
	 */
	public Double getTonnage() {
		return this._tonnage;
	}
	/**
	 * @param tonnage the 'tonnage' value to set
	 */
	public void setTonnage(Double tonnage) {
		if(tonnage != null)
			tonnage = Math.round(tonnage * 100D) * 0.01D;
		
		this._tonnage = tonnage;
	}
	/**
	 * @return the 'tonnageTypeCode' value
	 */
	public String getTonnageTypeCode() {
		return this._tonnageTypeCode;
	}
	/**
	 * @param tonnageTypeCode the 'tonnageTypeCode' value to set
	 */
	public void setTonnageTypeCode(String tonnageTypeCode) {
		this._tonnageTypeCode = tonnageTypeCode;
	}
	/**
	 * @return the 'previousName' value
	 */
	public String getPreviousName() {
		return this._previousName;
	}
	/**
	 * @param previousName the 'previousName' value to set
	 */
	public void setPreviousName(String previousName) {
		this._previousName = previousName;
	}
	/**
	 * @return the 'previousFlagCode' value
	 */
	public String getPreviousFlagCode() {
		return this._previousFlagCode;
	}
	/**
	 * @param previousFlagCode the 'previousFlagCode' value to set
	 */
	public void setPreviousFlagCode(String previousFlagCode) {
		this._previousFlagCode = previousFlagCode;
	}
	/**
	 * @return the 'dateAutStart' value
	 */
	public Date getDateAutStart() {
		return this._dateAutStart;
	}
	/**
	 * @param dateAutStart the 'dateAutStart' value to set
	 */
	public void setDateAutStart(Date dateAutStart) {
		this._dateAutStart = dateAutStart;
	}
	/**
	 * @return the 'dateAutEnd' value
	 */
	public Date getDateAutEnd() {
		return this._dateAutEnd;
	}
	/**
	 * @param dateAutEnd the 'dateAutEnd' value to set
	 */
	public void setDateAutEnd(Date dateAutEnd) {
		this._dateAutEnd = dateAutEnd;
	}
	/**
	 * @return the 'uRLID' value
	 */
	public String getUrlid() {
		return this._urlid;
	}
	/**
	 * @param uRLID the 'uRLID' value to set
	 */
	public void setUrlid(String uRLID) {
		this._urlid = uRLID;
	}
	/**
	 * @return the 'timestamp' value
	 */
	public Date getTimeStamp() {
		return this._timeStamp;
	}
	/**
	 * @param timestamp the 'timestamp' value to set
	 */
	public void setTimeStamp(Date timestamp) {
		this._timeStamp = timestamp;
	}
	
	public void toXML(XMLPrettyPrinter emitter) {
		//TODO: complete!
	}
}
