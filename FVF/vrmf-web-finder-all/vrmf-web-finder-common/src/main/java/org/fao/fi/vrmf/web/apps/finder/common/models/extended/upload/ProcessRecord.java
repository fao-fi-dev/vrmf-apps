/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder-common)
 */
package org.fao.fi.vrmf.web.apps.finder.common.models.extended.upload;

import java.util.Date;

import org.fao.fi.sh.utility.common.helpers.singletons.lang.BeansHelper;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 May 2012
 */
public class ProcessRecord extends ImportRecord {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -6474435027408054027L;
	
	private String _status;
	private Date _uploadDate;
	private String _uploaderID;
	private String _uploadComment;
	private Date _updateDate;
	private String _updaterID;
	private String _uploadID;

	/**
	 * Class constructor
	 *
	 */
	public ProcessRecord() {
		super();
	}
	
	
	
	/**
	 * Class constructor
	 *
	 * @param status
	 * @param uploadDate
	 * @param uploaderID
	 * @param uploadComment
	 * @param updateDate
	 * @param updaterID
	 * @param uploadID
	 */
	public ProcessRecord(ImportRecord data, String status, Date uploadDate, String uploaderID, String uploadComment, Date updateDate, String updaterID, String uploadID) {
		super(data);
		this._status = status;
		this._uploadDate = uploadDate;
		this._uploaderID = uploaderID;
		this._uploadComment = uploadComment;
		this._updateDate = updateDate;
		this._updaterID = updaterID;
		this._uploadID = uploadID;
	}
	
	public ProcessRecord(ProcessRecord data) {
		super();
		
		try {
			BeansHelper.transferBean(data, this);
		} catch (Throwable t) {
			throw new RuntimeException(t);
		}
	}

	/**
	 * @return the 'status' value
	 */
	public String getStatus() {
		return this._status;
	}
	/**
	 * @param status the 'status' value to set
	 */
	public void setStatus(String status) {
		this._status = status;
	}
	/**
	 * @return the 'uploadDate' value
	 */
	public Date getUploadDate() {
		return this._uploadDate;
	}
	/**
	 * @param uploadDate the 'uploadDate' value to set
	 */
	public void setUploadDate(Date uploadDate) {
		this._uploadDate = uploadDate;
	}
	/**
	 * @return the 'uploaderID' value
	 */
	public String getUploaderID() {
		return this._uploaderID;
	}
	/**
	 * @param uploaderID the 'uploaderID' value to set
	 */
	public void setUploaderID(String uploaderID) {
		this._uploaderID = uploaderID;
	}
	/**
	 * @return the 'uploadComment' value
	 */
	public String getUploadComment() {
		return this._uploadComment;
	}
	/**
	 * @param uploadComment the 'uploadComment' value to set
	 */
	public void setUploadComment(String uploadComment) {
		this._uploadComment = uploadComment;
	}
	/**
	 * @return the 'updateDate' value
	 */
	public Date getUpdateDate() {
		return this._updateDate;
	}
	/**
	 * @param updateDate the 'updateDate' value to set
	 */
	public void setUpdateDate(Date updateDate) {
		this._updateDate = updateDate;
	}
	/**
	 * @return the 'updaterID' value
	 */
	public String getUpdaterID() {
		return this._updaterID;
	}
	/**
	 * @param updaterID the 'updaterID' value to set
	 */
	public void setUpdaterID(String updaterID) {
		this._updaterID = updaterID;
	}
	/**
	 * @return the 'uploadID' value
	 */
	public String getUploadID() {
		return this._uploadID;
	}
	/**
	 * @param uploadID the 'uploadID' value to set
	 */
	public void setUploadID(String uploadID) {
		this._uploadID = uploadID;
	}
}
