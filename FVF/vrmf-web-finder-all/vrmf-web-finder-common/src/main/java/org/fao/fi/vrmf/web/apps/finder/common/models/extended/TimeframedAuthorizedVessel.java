/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder-common)
 */
package org.fao.fi.vrmf.web.apps.finder.common.models.extended;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.fao.fi.vrmf.common.models.extended.BasicTimeframedAuthorizedVessel;
import org.fao.fi.vrmf.common.models.generated.VesselsToIdentifiers;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 29 Mar 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 29 Mar 2012
 */
@XmlRootElement(name="vessel")
@XmlAccessorType(XmlAccessType.FIELD)
public class TimeframedAuthorizedVessel extends BasicTimeframedAuthorizedVessel {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -2225252894367413915L;

	@XmlElement(name="previousName")
	private String _previousName;
	
	@XmlElement(name="previousCountryId")
	private Integer _previousFlagId;
	
	@XmlElement(name="vesselTypeCode")
	private Integer _vesselTypeCode;
	
	@XmlElement(name="vesselType")
	private String _vesselType;
	
	@XmlElement(name="primaryGearTypeCode")
	private Integer _primaryGearTypeCode;
	
	@XmlElement(name="primaryGearType")
	private String _primaryGearType;
	
	@XmlTransient
	private List<VesselsToIdentifiers> _identifiers;
	
	private String _originalIdentifier;
	private String _alternateOriginalIdentifier;
	
	/**
	 * @return the 'previousName' value
	 */
	public String getPreviousName() {
		return this._previousName;
	}

	/**
	 * @param previousName the 'previousName' value to set
	 */
	public void setPreviousName(String previousName) {
		this._previousName = previousName;
	}

	/**
	 * @return the 'previousCountryId' value
	 */
	public Integer getPreviousFlagId() {
		return this._previousFlagId;
	}

	/**
	 * @param previousCountryId the 'previousCountryId' value to set
	 */
	public void setPreviousFlagId(Integer previousCountryId) {
		this._previousFlagId = previousCountryId;
	}

	/**
	 * @return the 'alternateOriginalIdentifier' value
	 */
	public String getAlternateOriginalIdentifier() {
		return this._alternateOriginalIdentifier;
	}

	/**
	 * @param alternateOriginalIdentifier the 'alternateOriginalIdentifier' value to set
	 */
	public void setAlternateOriginalIdentifier(String alternateOriginalIdentifier) {
		this._alternateOriginalIdentifier = alternateOriginalIdentifier;
	}

	/**
	 * @return the 'originalIdentifier' value
	 */
	public String getOriginalIdentifier() {
		return this._originalIdentifier;
	}

	/**
	 * @param originalIdentifier the 'originalIdentifier' value to set
	 */
	public void setOriginalIdentifier(String originalIdentifier) {
		this._originalIdentifier = originalIdentifier;
	}

	/**
	 * @return the 'identifiers' value
	 */
	public List<VesselsToIdentifiers> getIdentifiers() {
		return this._identifiers;
	}

	/**
	 * @param identifiers the 'identifiers' value to set
	 */
	public void setIdentifiers(List<VesselsToIdentifiers> identifiers) {
		this._identifiers = identifiers;
	}

	/**
	 * @return the 'vesselTypeCode' value
	 */
	public Integer getVesselTypeCode() {
		return this._vesselTypeCode;
	}

	/**
	 * @param vesselTypeCode the 'vesselTypeCode' value to set
	 */
	public void setVesselTypeCode(Integer vesselTypeCode) {
		this._vesselTypeCode = vesselTypeCode;
	}

	/**
	 * @return the 'vesselType' value
	 */
	public String getVesselType() {
		return this._vesselType;
	}

	/**
	 * @param vesselType the 'vesselType' value to set
	 */
	public void setVesselType(String vesselType) {
		this._vesselType = vesselType;
	}

	/**
	 * @return the 'primaryGearTypeCode' value
	 */
	public Integer getPrimaryGearTypeCode() {
		return this._primaryGearTypeCode;
	}

	/**
	 * @param primaryGearTypeCode the 'primaryGearTypeCode' value to set
	 */
	public void setPrimaryGearTypeCode(Integer primaryGearTypeCode) {
		this._primaryGearTypeCode = primaryGearTypeCode;
	}

	/**
	 * @return the 'primaryGearType' value
	 */
	public String getPrimaryGearType() {
		return this._primaryGearType;
	}

	/**
	 * @param primaryGearType the 'primaryGearType' value to set
	 */
	public void setPrimaryGearType(String primaryGearType) {
		this._primaryGearType = primaryGearType;
	}
}