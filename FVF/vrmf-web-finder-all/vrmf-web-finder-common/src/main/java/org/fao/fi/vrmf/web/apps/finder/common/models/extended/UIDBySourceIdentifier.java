/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder-common)
 */
package org.fao.fi.vrmf.web.apps.finder.common.models.extended;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.fao.fi.vrmf.common.core.model.GenericData;
import org.fao.fi.vrmf.common.models.generated.Vessels;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 11 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 11 May 2012
 */
@XmlRootElement(name="TUVIBySourceIdentifier")
@XmlAccessorType(XmlAccessType.FIELD)
public class UIDBySourceIdentifier extends GenericData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 1353365978630573019L;
	
	@XmlAttribute(name="tuvi")
	private Integer _tuvi;

	@XmlAttribute(name="identifier")
	private String _identifier;
	
	@XmlAttribute(name="identifierSourceSystem")
	private String _identifierSourceSystem;
	
	@XmlElement(name="vrmfId")
	private Integer _vrmfId;
	
	@XmlTransient
	private Integer _mapsTo;
	
	@XmlTransient
	private Date _mappingDate;
	
	@XmlTransient
	private String _mappingUser;
	
	@XmlTransient
	private String _mappingComment;
	
	@XmlTransient
	private Double _mappingWeight;
	
	@XmlElement(name="sourceSystem")
	private String _sourceSystem;
	
	@XmlElement(name="updateDate")
	private Date _updateDate;
	
	@XmlElement(name="updaterId")
	private String _updaterId;
	
	@XmlElement(name="comment")
	private String _comment;
	
	/**
	 * Class constructor
	 *
	 */
	public UIDBySourceIdentifier() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param vrmfId
	 * @param tuvi
	 * @param mapsTo
	 * @param mappingDate
	 * @param mappingUser
	 * @param mappingComment
	 * @param mappingWeight
	 * @param sourceSystem
	 * @param updateDate
	 * @param updaterId
	 * @param comment
	 */
	public UIDBySourceIdentifier(Integer tuvi, Integer vrmfId, String identifier, String identifierSourceSystem, Integer mapsTo, Date mappingDate, String mappingUser, String mappingComment, Double mappingWeight, String sourceSystem, Date updateDate, String updaterId, String comment) {
		super();
		this._tuvi = tuvi;
		this._vrmfId = vrmfId;
		this._identifier = identifier;
		this._identifierSourceSystem = identifierSourceSystem;
		this._mapsTo = mapsTo;
		this._mappingDate = mappingDate;
		this._mappingUser = mappingUser;
		this._mappingComment = mappingComment;
		this._mappingWeight = mappingWeight;
		this._sourceSystem = sourceSystem;
		this._updateDate = updateDate;
		this._updaterId = updaterId;
		this._comment = comment;
	}
	
	static public UIDBySourceIdentifier fromVessel(Vessels vessel) {
		return new UIDBySourceIdentifier(vessel.getUid(),
											  vessel.getId(),
											  null,
											  null,
											  vessel.getMapsTo(),
											  vessel.getMappingDate(),
											  vessel.getMappingUser(),
											  vessel.getMappingComment(),
											  vessel.getMappingWeight(),
											  vessel.getSourceSystem(),
											  vessel.getUpdateDate(),
											  vessel.getUpdaterId(),
											  vessel.getComment());	
	}
	
	/**
	 * @return the 'identifier' value
	 */
	public String getIdentifier() {
		return this._identifier;
	}

	/**
	 * @param identifier the 'identifier' value to set
	 */
	public void setIdentifier(String identifier) {
		this._identifier = identifier;
	}

	/**
	 * @return the 'identifierSourceSystem' value
	 */
	public String getIdentifierSourceSystem() {
		return this._identifierSourceSystem;
	}

	/**
	 * @param identifierSourceSystem the 'identifierSourceSystem' value to set
	 */
	public void setIdentifierSourceSystem(String identifierSourceSystem) {
		this._identifierSourceSystem = identifierSourceSystem;
	}

	/**
	 * @return the 'vrmfId' value
	 */
	public Integer getVrmfId() {
		return this._vrmfId;
	}

	/**
	 * @param vrmfId the 'vrmfId' value to set
	 */
	public void setVrmfId(Integer vrmfId) {
		this._vrmfId = vrmfId;
	}

	/**
	 * @return the 'tuvi' value
	 */
	public Integer getTuvi() {
		return this._tuvi;
	}

	/**
	 * @param tuvi the 'tuvi' value to set
	 */
	public void setTuvi(Integer tuvi) {
		this._tuvi = tuvi;
	}

	/**
	 * @return the 'mapsTo' value
	 */
	public Integer getMapsTo() {
		return this._mapsTo;
	}

	/**
	 * @param mapsTo the 'mapsTo' value to set
	 */
	public void setMapsTo(Integer mapsTo) {
		this._mapsTo = mapsTo;
	}

	/**
	 * @return the 'mappingDate' value
	 */
	public Date getMappingDate() {
		return this._mappingDate;
	}

	/**
	 * @param mappingDate the 'mappingDate' value to set
	 */
	public void setMappingDate(Date mappingDate) {
		this._mappingDate = mappingDate;
	}

	/**
	 * @return the 'mappingUser' value
	 */
	public String getMappingUser() {
		return this._mappingUser;
	}

	/**
	 * @param mappingUser the 'mappingUser' value to set
	 */
	public void setMappingUser(String mappingUser) {
		this._mappingUser = mappingUser;
	}

	/**
	 * @return the 'mappingComment' value
	 */
	public String getMappingComment() {
		return this._mappingComment;
	}

	/**
	 * @param mappingComment the 'mappingComment' value to set
	 */
	public void setMappingComment(String mappingComment) {
		this._mappingComment = mappingComment;
	}

	/**
	 * @return the 'mappingWeight' value
	 */
	public Double getMappingWeight() {
		return this._mappingWeight;
	}

	/**
	 * @param mappingWeight the 'mappingWeight' value to set
	 */
	public void setMappingWeight(Double mappingWeight) {
		this._mappingWeight = mappingWeight;
	}

	/**
	 * @return the 'sourceSystem' value
	 */
	public String getSourceSystem() {
		return this._sourceSystem;
	}

	/**
	 * @param sourceSystem the 'sourceSystem' value to set
	 */
	public void setSourceSystem(String sourceSystem) {
		this._sourceSystem = sourceSystem;
	}

	/**
	 * @return the 'updateDate' value
	 */
	public Date getUpdateDate() {
		return this._updateDate;
	}

	/**
	 * @param updateDate the 'updateDate' value to set
	 */
	public void setUpdateDate(Date updateDate) {
		this._updateDate = updateDate;
	}

	/**
	 * @return the 'updaterId' value
	 */
	public String getUpdaterId() {
		return this._updaterId;
	}

	/**
	 * @param updaterId the 'updaterId' value to set
	 */
	public void setUpdaterId(String updaterId) {
		this._updaterId = updaterId;
	}

	/**
	 * @return the 'comment' value
	 */
	public String getComment() {
		return this._comment;
	}

	/**
	 * @param comment the 'comment' value to set
	 */
	public void setComment(String comment) {
		this._comment = comment;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this._comment == null) ? 0 : this._comment.hashCode());
		result = prime * result + ((this._vrmfId == null) ? 0 : this._vrmfId.hashCode());
		result = prime * result + ((this._mappingComment == null) ? 0 : this._mappingComment.hashCode());
		result = prime * result + ((this._mappingDate == null) ? 0 : this._mappingDate.hashCode());
		result = prime * result + ((this._mappingUser == null) ? 0 : this._mappingUser.hashCode());
		result = prime * result + ((this._mappingWeight == null) ? 0 : this._mappingWeight.hashCode());
		result = prime * result + ((this._mapsTo == null) ? 0 : this._mapsTo.hashCode());
		result = prime * result + ((this._sourceSystem == null) ? 0 : this._sourceSystem.hashCode());
		result = prime * result + ((this._tuvi == null) ? 0 : this._tuvi.hashCode());
		result = prime * result + ((this._updateDate == null) ? 0 : this._updateDate.hashCode());
		result = prime * result + ((this._updaterId == null) ? 0 : this._updaterId.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		UIDBySourceIdentifier other = (UIDBySourceIdentifier) obj;
		if (this._comment == null) {
			if (other._comment != null)
				return false;
		} else if (!this._comment.equals(other._comment))
			return false;
		if (this._vrmfId == null) {
			if (other._vrmfId != null)
				return false;
		} else if (!this._vrmfId.equals(other._vrmfId))
			return false;
		if (this._mappingComment == null) {
			if (other._mappingComment != null)
				return false;
		} else if (!this._mappingComment.equals(other._mappingComment))
			return false;
		if (this._mappingDate == null) {
			if (other._mappingDate != null)
				return false;
		} else if (!this._mappingDate.equals(other._mappingDate))
			return false;
		if (this._mappingUser == null) {
			if (other._mappingUser != null)
				return false;
		} else if (!this._mappingUser.equals(other._mappingUser))
			return false;
		if (this._mappingWeight == null) {
			if (other._mappingWeight != null)
				return false;
		} else if (!this._mappingWeight.equals(other._mappingWeight))
			return false;
		if (this._mapsTo == null) {
			if (other._mapsTo != null)
				return false;
		} else if (!this._mapsTo.equals(other._mapsTo))
			return false;
		if (this._sourceSystem == null) {
			if (other._sourceSystem != null)
				return false;
		} else if (!this._sourceSystem.equals(other._sourceSystem))
			return false;
		if (this._tuvi == null) {
			if (other._tuvi != null)
				return false;
		} else if (!this._tuvi.equals(other._tuvi))
			return false;
		if (this._updateDate == null) {
			if (other._updateDate != null)
				return false;
		} else if (!this._updateDate.equals(other._updateDate))
			return false;
		if (this._updaterId == null) {
			if (other._updaterId != null)
				return false;
		} else if (!this._updaterId.equals(other._updaterId))
			return false;
		return true;
	}
}