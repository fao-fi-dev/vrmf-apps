/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder-common)
 */
package org.fao.fi.vrmf.web.apps.finder.common.models.extended;

import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.vrmf.common.core.model.GenericData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 11 May 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 11 May 2012
 */
@XmlRootElement(name="TUVIBySourceIdentifierResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class UIDBySourceIdentifierResponse extends GenericData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 2146199116488138599L;
	
	@XmlElement(name="source")
	private String _source;
	
	@XmlElement(name="identifierType")
	private String _identifierType;
	
	@XmlElement(name="identifier")
	private String _identifier;
	
	@XmlElement(name="items")
	private Collection<UIDBySourceIdentifier> _items;

	/**
	 * @return the 'source' value
	 */
	public String getSource() {
		return this._source;
	}

	/**
	 * @param source the 'source' value to set
	 */
	public void setSource(String source) {
		this._source = source;
	}

	/**
	 * @return the 'identifierType' value
	 */
	public String getIdentifierType() {
		return this._identifierType;
	}

	/**
	 * @param identifierType the 'identifierType' value to set
	 */
	public void setIdentifierType(String identifierType) {
		this._identifierType = identifierType;
	}

	/**
	 * @return the 'identifier' value
	 */
	public String getIdentifier() {
		return this._identifier;
	}

	/**
	 * @param identifier the 'identifier' value to set
	 */
	public void setIdentifier(String identifier) {
		this._identifier = identifier;
	}

	/**
	 * @return the 'items' value
	 */
	public Collection<UIDBySourceIdentifier> getItems() {
		return this._items;
	}

	/**
	 * @param items the 'items' value to set
	 */
	public void setItems(Collection<UIDBySourceIdentifier> items) {
		this._items = items;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this._items == null) ? 0 : this._items.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		UIDBySourceIdentifierResponse other = (UIDBySourceIdentifierResponse) obj;
		if (this._items == null) {
			if (other._items != null)
				return false;
		} else if (!this._items.equals(other._items))
			return false;
		return true;
	}
	
	
}