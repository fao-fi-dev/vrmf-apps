<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page import="java.lang.reflect.Field"%>
<%@page import="java.util.Collection"%>
<%@page import="org.fao.fi.sh.utility.core.helpers.singletons.lang.ObjectsHelper"%>
<%@page import="org.fao.fi.vrmf.common.j2ee.utilities.ServletsHelper.RequestInfo"%>
<%@page import="org.fao.fi.vrmf.common.j2ee.utilities.ServletsHelper"%>
<%@page contentType="text/html; charset=UTF-8" isErrorPage="false"%>
<%
	RequestInfo ri = ServletsHelper.getRequestInfo(request);
%>

<html>
	<head>
		<title>Testing page: to be removed</title>
	</head>
	<body>
		<h1>Request:</h1>
		<div>
		<label>Local name: </label> <code><%=request.getLocalName()%></code>
		<label>Local address: </label> <code><%=request.getLocalAddr()%>:<%=request.getLocalPort()%></code>
		
		<label>Remote host: </label> <code><%=request.getRemoteHost()%></code>
		<label>Remote address: </label> <code><%=request.getRemoteAddr()%>:<%=request.getRemotePort()%></code>
		
		<label>Server name: </label> <code><%=request.getServerName()%>:<%=request.getServerPort()%></code>
		
		<label>Request URI:</label> <code><%=request.getRequestURI()%></code>
		<label>Context path:</label> <code><%=request.getContextPath()%></code>
		<hr/>
		
		<h2>Request info:</h2>
		
<%
			Collection<Field> fields = ObjectsHelper.listAllFields(ri);

			for(Field field : fields) {
				field.setAccessible(true);
				out.println("<label>" + field.getName() + "</label> <code>" + field.get(ri) + "</code>");
			}
		%>
<%
 out.println("<ul>");

 java.util.Enumeration<?> names = request.getHeaderNames();
 while (names.hasMoreElements()) {
   String name = (String) names.nextElement();
   String value = request.getHeader(name);
   out.println(" <li>     <b>" + name + "=</b>" + value +"</li>");
 }
 out.println("</ul>");
 %>
 		</div>
 	</body>
</html>