<%@page contentType="text/html; charset=UTF-8" isErrorPage="true" trimDirectiveWhitespaces="true"%>
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page import="java.io.Serializable"%>
<%@page import="java.io.Writer"%>
<%@page import="java.io.StringWriter"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="org.fao.fi.vrmf.common.search.dsl.support.models.vessels.record.VesselRecord"%>
<%@page import="org.fao.fi.vrmf.web.apps.finder.common.models.extended.TimeframedAuthorizedVessel"%>
<%@include file="components/commonInitialization.jspf"%>
<%
	CustomBundleManager $MULTIPLE = new CustomBundleManager("i18n/finder/web/server", new Locale(languageWithFallback), new String[] {
		"other/multipleChoices"
	});

	String grouping = (String)request.getAttribute(DisplayVesselByIDRedirectorConstants.VRMF_VESSELS_REDIRECT_BY_ID_GROUPING_REQUEST_ATTRIBUTE);
	NameValuePair redirectionCriteria = (NameValuePair)request.getAttribute(DisplayVesselByIDRedirectorConstants.VRMF_VESSELS_REDIRECT_BY_ID_CRITERIA_REQUEST_ATTRIBUTE);

	String identifierType = redirectionCriteria.getName();
	Serializable identifier = (Serializable)redirectionCriteria.getValue();

	Collection<NameValuePair> choices = (Collection<NameValuePair>)request.getAttribute(DisplayVesselByIDRedirectorConstants.VRMF_MULTIPLE_VESSELS_DATA_REQUEST_ATTRIBUTE);
	String choicesHeader = $MULTIPLE.getText("vrmf.other.multiple.header", choices.size(), identifierType, identifier);

	breadcrumbs.add(new BasicPair<String, Pair<String, String>>($MULTIPLE.getText("vrmf.other.multiple.breadcrumb", identifierType, identifier), new BasicPair<String, String>(requestedURI, $MULTIPLE.getText("vrmf.other.multiple.breadcrumb.tip", identifierType, identifier ))));
%>
<html xmlns="http://www.w3.org/1999/xhtml" lang="<%=languageWithFallback%>" xml:lang="<%=languageWithFallback%>" dir="<%=isRTL ? "rtl" : "ltr"%>" class="<%=isRTL ? "rtl" : "ltr"%>">
	<head> 
		<base href="<%=baseHREF%>"/>

		<title><%=$MULTIPLE.getText("vrmf.other.multiple.title", choicesHeader, defaultTitle)%></title>

		<meta name="description" content="<%=defaultDescription%>"/>
		<meta name="keywords" content="<%=CollectionsHelper.join(keywords, ", ")%>"/>
		<meta name="author" content="<%=metaAuthor%>"/>
		<meta name="generator" content="FAO VRMF engine<%=specificSource == null ? "" : " for " + specificSourceID + " vessels"%>"/>
		<meta http-equiv="content-language" content="<%=languageWithFallback%>"/>

		<%@include file="components/commonStyles.jspf"%>
		<%@include file="components/commonLibraries.jspf"%>
		<%@include file="components/commonScripts.jspf"%>

		<link type="image/png" rel="icon" href="../cdn/media/images/icons/default/warning.png"/>

		<link id="baseCSS" class="additionalStyles" rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css"/>
		<link id="themeCSS" rel="stylesheet" type="text/css" href="<%=URLHelper.unescape(themeCSS)%>"/>
		
		<link rel="stylesheet" type="text/css" href="../cdn/styles/base/all.css"/>
		<link rel="stylesheet" type="text/css" href="styles/default.css|login.css|multipleChoices.css"/>

		<!-- Damn' that IE thing... -->
		<link rel="stylesheet" type="text/css" href="../cdn/styles/default.ie.css"/>
		<link rel="stylesheet" type="text/css" href="styles/default.ie.css"/>
	</head>
	<body class="ui-helper-clearfix">

		<!-- Header -->
		<%@include file="components/header.jspf"%>

		<div id="padder" class="ui-widget-content borderless removeBackground">&#160;</div>

		<!-- Main content -->
		<div id="mainContainer" class="hidden ui-helper-clearfix ui-widget-content">
			<div id="multipleChoices">
				<h1 class="ui-state-highlight"><%=choicesHeader%></h1>
				<div class="choicesContainer">
					<ul>
<%
	int counter = 1;
	VesselRecord vessel;

	String href, target, title, description;
	for(NameValuePair choice : choices) {
		vessel = (VesselRecord)choice.getValue();
		href = choice.getName();
		target = "_VRMF_VESSEL_BY_" + identifierType + "_" + identifier + "_" + counter;
		title = $MULTIPLE.getText("vrmf.other.multiple.choice.tip", counter++, redirectionCriteria.getName(), redirectionCriteria.getValue(), grouping.toUpperCase());

		description = ( vessel.getFlagID() != null ? "<span class=\"vesselFlag\" rel=\"#" + vessel.getFlagID() + "\"></span>" : "" ) + 
						"<span class=\"vesselName\">" + ( vessel.getName() == null ? "&lt;" + $MULTIPLE.getText("vrmf.other.multiple.choice.name.missing") + "&gt;" : vessel.getName() ) + "</span>";

		out.println("<li>" +
						"<a href=\"" + href + "\" class=\"pre externalStatic default { target: '" + target + "' }\">" +
							"<span title=\"" + title + "\">" + description + "</span>" +
						"</a>" +
						 " [ <span class=\"vesselSource\">" + vessel.getSourceSystem() + "</span> ]" +  
					"</li>");
	}
%>
					</ul>
				</div>
			</div>
		</div>

		<!-- Login -->
		<div id="loginDialog" class="hidden">
			<%@include file="components/login.jspf" %>
		</div>

		<!-- Footer -->
		<%@include file="components/footer.jspf"%>

		<!-- Credits -->
		<div id="applicationCredits" class="hidden">
			<%@include file="components/credits.jspf"%>
		</div>
		
		<!-- Browser list -->
		<%@include file="components/browserList.jspf"%>

		<%@include file="components/externalLibraries.jspf"%>

		<script type="text/javascript" src="../cdn/libs/other/jquery/ui/effects/jquery.effects.core.js|jquery.effects.drop.js|jquery.effects.blind.js|jquery.effects.slide.js|jquery.effects.fade.js"></script>
		<script type="text/javascript" src="libs/<%if(trackRequest){%>tracking.js|<%}%>defaults.js|
																					   login.js|
																					   interface.common.js|
																					   _multipleChoices.js">
		</script>
	</body>
</html>