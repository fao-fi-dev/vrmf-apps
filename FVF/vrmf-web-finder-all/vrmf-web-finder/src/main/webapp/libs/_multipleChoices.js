$(function() {
	$$.Flags.setBaseURL("../cdn/media/images/flags/famfamfam/");
	$$.Flags.setEmptyImgURL("../cdn/media/images/flags/empty.png");
	$$.Flags.setCase("lower");
	$$.Flags.setImgType("png");

	$$.Metadata.setBaseURL(_WRAP_URL("common/services/public/metadata/reference"));
	$$.Metadata.setTimeout(VRMF_FINDER_CONFIGURATION.timeouts.metadata);
	
	$$.Metadata.loadCountries(updateInterface);	
});

function updateInterface() {
	$("span.vesselFlag").each(function() {
		var $this = $(this);
		var countryId = parseInt($this.attr("rel").replace(/\#/g, ""), 10);
		
		$this.append($$.Flags.getImageByCountryID(countryId));
	});

	initializeCommonInterface();
};