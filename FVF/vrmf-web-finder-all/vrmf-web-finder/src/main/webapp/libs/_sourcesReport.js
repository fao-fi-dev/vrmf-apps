function afterMetadataInitialization() {
	buildSources(false);
	
	completeInitialization(function() {
		$$.TooltipManager.apply($("#actions [title], #text [title]"));
		
		$.unblockUI();
		$("#mainContainer").removeClass("hidden");
	});
};

/** Currently unused */
function addDescriptions() {
	$("#text *").each(function() {
		var $this = $(this);
		
		if($this.children().size() == 0) {
			var text = $this.html();
			
			for(var key in _DESCRIPTIONS) {
				text = text.replace(new RegExp(key, "g"), 
									"<cite class=\"description\" " + 
										   $$.TooltipManager.TOOLTIP_CONFIGURATION_ATTRIBUTE + "=\"{ position: { my: 'center bottom', at: 'center top' } }\" " +
										  "title=\"" + _DESCRIPTIONS[key].replace(/\"/g, "\\\"") + "\">" + 
										  key + 
									"</cite>"); 
			}
			
			$this.html(text);
		}
	});
};

$(function() {
	$$.Metadata.setBaseURL("common/services/public/metadata/reference");
	$$.Metadata.setTimeout(VRMF_FINDER_CONFIGURATION.timeouts.metadata);
	
	$("#mainContainer .sectionContainer h3").addClass("ui-helper-clearfix")
	.hover(function() {
		$(this).addClass("ui-state-hover");
	}, function() {
		$(this).removeClass("ui-state-hover");
	})
	.prepend("<span class='left ui-icon ui-icon-carat-1-n hidden ui-helper-clearfix'></span>")
	.prepend("<span class='left ui-icon ui-icon-carat-1-s ui-helper-clearfix'></span>")
	.click(function() {
		var $this = $(this);

		var section = $this.next(".section");
		var collapsed = $this.is(".collapsed");
		
		if(collapsed)
			$this.toggleClass("collapsed");
		
		section.slideToggle(function() {
			if(!collapsed)
				$this.toggleClass("collapsed");
			
			if($$.UI.nicescrollEnabled)
				$(this).trigger("resize.ui");
		});
		
		$(".ui-icon", $this).toggleClass("hidden");
	});
	
	$(".availableDataSources li").hover(function() {
		$(this).removeClass("noBorder");
		$(this).css({ "border-color": "initial" });
	}, function() {
		$(this).addClass("noBorder");
	});
	
	$$.Metadata._load(['{systems}'], {}, $$.UserManager.getLoggedUser(), true, function() {
		getLoggedUserDetails(afterMetadataInitialization);
	});
});