var BASE_URLS = {
	EU:    "http://ec.europa.eu/fisheries/fleet/index.cfm?method=Search.ListSearchSimpleOneVessel&COUNTRY_CODE=&CFR_CODE={query}&search_type=CFR",
	CCSBT: "http://www.ccsbt.org/site/authorised_vessels_detail.php?id={query}",
	HSVAR: "http://www.fao.org/figis/vrmf/hsvar/display/uid/{query}#historical",
	IATTC: "http://www.iattc.org/VesselRegister/VesselDetails.aspx?VesNo={query}&Lang=en",
	ICCAT: "http://www.iccat.int/en/VesselsRecordRes.asp?cajaRecord=checkbox&cajaFlag=checkbox&cajaFlag2=checkbox&cajaVessel=checkbox&cajaIRCS=checkbox&cajaRegistry=checkbox&textSN={query}&selectOrder=1&selectOrder2=6&selectInterval=25&Submit=Search",
//	IOTC:  "http://www.iotc.org/English/record/record_vessel3.php?vid={query}",
	IOTC:  "http://www.iotc.org/vessels/history/{query}",
	WCPFC: "http://intra.wcpfc.int/Lists/Vessels/DispForm.aspx?ID={query}&Source=http%3A%2F%2Fintra%2Ewcpfc%2Eint%2FLists%2FVessels%2FStats%2Easpx"
};

var URL_BUILDERS = {
	EU: { builder: buildEUVessel, baseURL: BASE_URLS.EU },
	CCSBT: { builder: buildCCSBTVessel, baseURL: BASE_URLS.CCSBT },
	HSVAR: { builder: buildHSVARVessel, baseURL: BASE_URLS.HSVAR },
	IATTC: { builder: buildIATTCVessel, baseURL: BASE_URLS.IATTC },
	ICCAT: { builder: buildICCATVessel, baseURL: BASE_URLS.ICCAT },
	IOTC: { builder: buildIOTCVessel, baseURL: BASE_URLS.IOTC },
	WCPFC: { builder: buildWCPFCVessel, baseURL: BASE_URLS.WCPFC }
};

function buildEUVessel(data, baseURL) {
	var query = "";
	
	for(var i=0; i<data.identifiers.length; i++) {
		if(data.identifiers[i].typeId == "EU_CFR")
			query = data.identifiers[i].identifier;
	}
	
	return baseURL.replace("\{query\}", query);
};

function buildHSVARVessel(data, baseURL) {
	var query = null;
	
	if($$.isSet(data.authorizationsData)) 
		for(var i=0; i<data.authorizationsData.length; i++) {
			if(data.authorizationsData[i].sourceSystem == "HSVAR") {
				query = data.uid;
				
				break;
			}
		}
	
	if(query != null)
		return baseURL.replace("\{query\}", query);
	
	return null;
};

function buildCCSBTVessel(data, baseURL) {
	var query = "";
	
	for(var i=0; i<data.identifiers.length; i++) {
		if(data.identifiers[i].typeId == "CCSBT_ID")
			query = data.identifiers[i].alternateIdentifier;
	}
	
	return baseURL.replace("\{query\}", query);
};

function buildIATTCVessel(data, baseURL) {
	var query = "";
	
	for(var i=0; i<data.identifiers.length; i++) {
		if(data.identifiers[i].typeId == "IATTC_ID")
			query = data.identifiers[i].identifier;
	}
	
	return baseURL.replace("\{query\}", query);
};

function buildICCATVessel(data, baseURL) {
	var query = "";
	
	for(var i=0; i<data.identifiers.length; i++) {
		if(data.identifiers[i].typeId == "ICCAT_ID")
			query = data.identifiers[i].identifier;
	}
	
	return baseURL.replace("\{query\}", query);
};

function buildIOTCVessel(data, baseURL) {
	var query = "";
	
	for(var i=0; i<data.identifiers.length; i++) {
		if(data.identifiers[i].typeId == "IOTC_ID")
			query = data.identifiers[i].alternateIdentifier;
	}
	
	return baseURL.replace("\{query\}", query);
};

function buildWCPFCVessel(data, baseURL) {
	var query = "";
	
	for(var i=0; i<data.identifiers.length; i++) {
		if(data.identifiers[i].typeId == "WCPFC_ID")
			query = data.identifiers[i].identifier;
	}
	
	return baseURL.replace("\{query\}", query);
};