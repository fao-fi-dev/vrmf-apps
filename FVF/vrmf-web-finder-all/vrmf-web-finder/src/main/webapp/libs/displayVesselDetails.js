var VESSEL_DATA = {};

var _ASCENDING = -1;
var _DESCENDING = 1;

var _NUMBER_SORTER = function(a, b) {
	return a < b ? _ASCENDING : a > b ? _DESCENDING : 0;
};

var _ALPHA_SORTER = function(a, b) {
	return a < b ? _ASCENDING : a > b ? _DESCENDING : 0;
};

var _PICTURES_NOTIFICATION;
var _TRACKS_NOTIFICATION;
var _INFRINGEMENTS_NOTIFICATION;

var TABS = {
	current: 0,
	historical: 1,
	timeline: 2,
	pictures: 3,
	tracks: 4,
	infringements: 5
};

var _CUSTOM_SORTERS = {
	components: function(a, b) {
		var firstMapsTo = a.mapsTo;
		var secondMapsTo = b.mapsTo;
		
		if(firstMapsTo == null)
			return -1;
		else if(secondMapsTo == null)
			return 1;
		else
			return _NUMBER_SORTER(b.mappingDate, a.mappingDate);
	},
	PIDs: function(a, b) {
		var firstType = $$.Metadata.VESSEL_IDENTIFIERS_MAP[a.typeId];
		var secondType = $$.Metadata.VESSEL_IDENTIFIERS_MAP[b.typeId];

		var priority = _NUMBER_SORTER(firstType.priority, secondType.priority);

		if(priority != 0)
			return priority;

		priority = _ALPHA_SORTER(a.sourceSystem, b.sourceSystem);

		if(priority != 0)
			return priority;

		return _ALPHA_SORTER(a.identifier, b.identifier);
	},
	vesselGears: function(a, b) {
		return a.primaryGear && !b.primaryGear ? _ASCENDING :
				!a.primaryGear && b.primaryGear ? _DESCENDING :
					0;
	},
	engines: function(a, b) {
		return a.mainEngine && !b.mainEngine ? _ASCENDING :
				!a.mainEngine && b.mainEngine ? _DESCENDING :
					_ALPHA_SORTER(a.typeId, b.typeId);
	},
	physicalDimensions: function(a, b) {
		return _ALPHA_SORTER(a.typeId, b.typeId);
	},
	tonnages: function(a, b) {
		return _ALPHA_SORTER(a.typeId, b.typeId);
	},
	authorizations: function(a, b) {
		var aDate = new Date(a.referenceDate).formatDate();
		var bDate = new Date(b.referenceDate).formatDate();

		var aSource = a.sourceSystem;
		var bSource = b.sourceSystem;

		if(aDate === bDate)
			return aSource == "CLAV" ? _ASCENDING : bSource == "CLAV" ? _DESCENDING : aSource < bSource ? _DESCENDING : aSource > bSource ? _ASCENDING : 0;

		return aDate < bDate ? _DESCENDING : aDate > bDate ? _ASCENDING : 0;
	}/*,
	authorizations: function(a, b) {
		var aIsTerm = $$.isSet(a.terminationReferenceDate);
		var bIsTerm = $$.isSet(b.terminationReferenceDate);

		var aDate = aIsTerm ? a.terminationReferenceDate : a.referenceDate;
		var bDate = bIsTerm ? b.terminationReferenceDate : b.referenceDate;

		return aDate < bDate ? _DESCENDING : aDate > bDate ? _ASCENDING : 0;
	}*/
};

var _CURRENT_DATA_CUSTOM_SORTERS = _CUSTOM_SORTERS;
var _HISTORICAL_DATA_CUSTOM_SORTERS = {
	components: _CUSTOM_SORTERS.components,
	PIDs: _CUSTOM_SORTERS.PIDs,
	authorizations: _CUSTOM_SORTERS.authorizations
};

var _POST_PROCESSORS = {
	components: function(data) {
		if($$.isSet(data)) {
			var tree = new Arboreal();
			
			var parent = tree;
			var toProcess = new $$.Set();
			
			for(var d=0; d<data.length; d++)
				toProcess.add(data[d].id);
			
			//This is a rather 'dirty' way to build the vessel mappings tree (less complexity...)
			while(!toProcess.isEmpty()) {
				for(var d=0; d<data.length; d++) {
					if(toProcess.contains(data[d].id)) {
						try {
							if(data[d].mapsTo != null) {
								parent = tree.find(function(node) {
									return node.data.id === data[d].mapsTo;
								});
							} else 
								parent = tree;
							
							if(parent == null)
								parent = tree;
							
							parent.appendChild(data[d], data[d].id);
							
							toProcess.remove(data[d].id);
						} catch (E) {
							;//Suffocate... ( required to not add extra complexity into implementing the proper sorting order on mapped vessels )
						}
					}
				}
			}
			
			var processedData = [];
			
			function iterator(node) {
				if($$.isSet(node.data.id)) {
					var originalData = node.data;
					originalData._depth = node.depth;
					
					processedData.push(originalData);
				}
			}
			
			tree.traverseDown(iterator);
			
			return processedData;
		} else
			return data;
	}
};

var _CURRENT_DATA_POST_PROCESSORS = _POST_PROCESSORS;
var _HISTORICAL_DATA_POST_PROCESSORS = _POST_PROCESSORS;

function showVesselDetails(byUID, ID, afterDisplayCallback) {
	var formattedID = formatID(ID, _ID_MAX_LENGTH);

	$(".exportVesselDataLink").each(function() {
		var $this = $(this);

		var href = $this.data("href");

		if(!$$.isSet(href)) {
			$this.data("href", $this.attr("href"));
		} else {
			$this.attr("href", href);
		}
	});

	var vesselDataAreEmbedded = false;

	try {
		eval("VRMF_VESSEL_DATA");

		vesselDataAreEmbedded = true;
	} catch (E) {

	}

	var groupBy = byUID ? "UID" : "ID";

	if(vesselDataAreEmbedded) {
		disableInputs($$.I18n.getText("vessels.display.message.blocking.combined.displaying", groupBy, formattedID));

		doShowVesselDetails(byUID, formattedID, VRMF_VESSEL_DATA, afterDisplayCallback);

		enableInputs();
	} else {
		disableInputs($$.I18n.getText("vessels.display.message.blocking.combined.retrieving", groupBy, formattedID));

		$.ajax({
			cache: false,
			url: _WRAP_URL(_VRMF_BROWSER_SERVICES_VESSELS_GET_URL_PREFIX) + groupBy + "/" + formattedID,
			type: 'get',
			dataType: 'json',
			success: function(results) {
				if(results.error) {
					$$.Interface.error([ $$.I18n.getText("vessels.display.error.combined.retrieving", groupBy, formattedID), results.errorMessage]);
				} else {
					if($$.isSet(results.data)) {
						doShowVesselDetails(byUID, formattedID, results.data, afterDisplayCallback);
					} else {
						$$.Interface.error( [ $$.I18n.getText("vessels.display.error.combined.displaying", groupBy, formattedID), "No data" ]);
					}
				}
			},
			complete: function() {
				enableInputs();
			}
		});
	}
};

function doShowVesselDetails(byUID, formattedID, data, afterDisplayCallback) {
	$(".exportVesselDataLink").each(function() {
		var $this = $(this);

		var currentHref = $this.data("href");

		if(!$$.isSet(currentHref))
			$this.data("href", $this.attr("href"));

		currentHref = currentHref.replace(/\{idType\}/g, byUID ? "UID" : "ID").replace(/\{id\}/g, formattedID);

		$this.attr("href", currentHref);
	}); 
	
	if(typeof detach == 'function')
		detach(byUID, formattedID, $(".detach"));

	displayVesselDetails(byUID, parseInt(formattedID, 10), data, false, 0, 0, afterDisplayCallback);
	
	var href = window.location.href;
	var hashIndex = href.indexOf("#", 0);
	var hash = null;
	
	if(hashIndex >= 0) 
		hash = href.substring(hashIndex);
	
	if(hash != null)
		$("#tabs a[href='" + hash + "']").click();
	
	showVesselsDataPanel();
};

function displayVesselDetails(byUID, ID, data, cached, queryTime, totalTime, afterDisplayCallback) {
	VESSEL_DATA = data;

	buildVesselDetails($("#current"), data, null, [], new Date().formatDate(), _CURRENT_DATA_CUSTOM_SORTERS, _CURRENT_DATA_POST_PROCESSORS);
	buildVesselDetails($("#historical"), data, null, [], null, _HISTORICAL_DATA_CUSTOM_SORTERS, _CURRENT_DATA_POST_PROCESSORS);
	buildVesselTimeline($("#timeline"), data, null, [], null);

	installVesselDisplayTooltips();
	
	updateCounters($("#dataContainer"));
	
	buildHistoricalQuickJump();
	buildTimelineQuickJump();

	$(".systemPicker").each(function() {
		initializeSystempicker($(this), data);
	});

	$(".vesselPicker").each(function() {
		initializeVesselpicker($(this), data);
	});

	initializeDateSelector($("#currentDateSelector"), data);
	
	$(".colorizer input[type='radio']").unbind("change").change(function() {
		var target = $(this).parents(".detailsContainer");
		
		applyColorBySource(target);
	});

	$(".emptyAttributesToggler input[type='radio']").unbind("change").change(function() {
		var target = $(this).parents(".detailsContainer");
		
		displayHiddenAttributes(target);
	});

	var showSystemsChoice = true;	 //This should be user-dependent
	var showComponentsChoice = true; //This should be user-dependent

	if(!showSystemsChoice) {
		$(".sourceChoice").hide();
	} else {
		$(".sourceChoice").each(function() {
			if($("option", $(this)).size() > 2)
				$(this).show();
			else
				$(this).hide();
		});
	}

	if(!showComponentsChoice) {
		$(".vesselChoice").hide();
	} else {
		$(".vesselChoice").each(function() {
			if($("option", $(this)).size() > 2)
				$(this).show();
			else
				$(this).hide();
		});
	}

	var systems = $("#historical .sourceChoice option").size() - 1;
	var components = $("#historical .vesselChoice option").size() - 1;

	if(systems <= 1 && components <= 1) {
		$("#historicalDetailsDisplayContainer, #timelineDisplayContainer").hide();
	} else {
		$("#historicalDetailsDisplayContainer, #timelineDisplayContainer").show();
	}

	var legend = $("#selectedVesselsDetails legend");

	var vesselName = "<" + $$.I18n.getText("vessels.display.data.title.no.name").toUpperCase() + ">";

	if($$.isSet(data.nameData) && data.nameData.length > 0)
		vesselName = data.nameData[0].name;

	var tabs = $("#tabsContainer").removeClass("ui-corner-all").tabs({
		//Prevents Ajax loading of tab content (due to misinterpretation of anchor vs current URL)
		beforeLoad: function(event, ui) {
			$$.Log.warn("Prevented loading URL '" + ui.ajaxSettings.url + "' to fix jQuery UI tabs misbehaviour (sort of). Any following logged errors should be wholeheartedly forgotten...");
			return false;
		}
	});
	
	$$.UI.disableTabs(tabs, [ TABS.pictures, TABS.tracks, TABS.infringements ]);
	
	tabs.tabs({ active: TABS.historical });
	
	tabs.tabs("option", "activate", function(event, ui) {
		var index = ui.newTab.index();
		
		if(index === TABS.pictures) {
			if(ui.newTab.context.hash == '#pictures') {
				scrollThumbs(0, 0);
				
				displayFirstPicture();
			}
		} else if(index === TABS.tracks) {
			var selected = $("#trackSelector option:selected");
			
			if(selected.val() == null || selected.data("track") == null)
				$$.PositionsManager.displayTracks($("#positionsMap"), data);
			else
				$$.PositionsManager.displayTracks($("#positionsMap"), data, [ selected.data("track") ]);
		}
	});
	
	var formattedID = formatID(ID, _ID_MAX_LENGTH);

	$(".currentVesselName", legend).text(vesselName);

	var groupBy = byUID ? "UID" : "ID";

	$("#groupBy").text(groupBy);

	var vesselUID = $(".currentVesselUID", legend);
	vesselUID.text(formattedID);

	var vesselIdentification = { by: groupBy, id: formattedID }; 
	
	getPictures(vesselIdentification, data);
	getTracks(vesselIdentification, data);
	getInfringements(vesselIdentification, data);
	/** Temporarily (?) disabled
	 
	 getNews(data);
	 */
	
	$("#pictures [title]").each(function() {
		var $this = $(this);

		$$.TooltipManager.apply($this, TOOLTIP_DISPLAY_DEFAULTS);
	});

	if($$.Browser.msie)
		$(".vesselDetails .ui-state-disabled").removeClass("ui-state-disabled");

	if($$.isSet(afterDisplayCallback))
		afterDisplayCallback(data);
};

function getPictures(vesselIdentification, vesselData) {
	$("#thumbs").empty();
	 
	var t = $("#tabsContainer").tabs();

	$$.UI.disableTabs(t, TABS.pictures);
	
	$("#numberOfPictures").text("");

	_PICTURES_NOTIFICATION = $$.Notification.info("<span>" + $$.I18n.getText("vessels.display.pictures.searching") + "&#160;<span class=\"spinner\">&#160;</span></span>", null, { delay: VRMF_FINDER_CONFIGURATION.timeouts.picturesManager, hide: false });
	
	$$.PicturesManager.setBaseURL(( $$.isSet(_SPECIFIC_SOURCE) ? _SPECIFIC_SOURCE + "/" : "" ) + _COMMON_SERVICES_VESSELS_SEARCH_URL_PREFIX);
	$$.PicturesManager.setTimeout(VRMF_FINDER_CONFIGURATION.timeouts.picturesManager);
	$$.PicturesManager.searchPictures(vesselIdentification, vesselData, displayPictures, picturesNotFound);
};

function picturesNotFound() {
	$$.Notification.change(_PICTURES_NOTIFICATION, 'error', $$.I18n.getText("vessels.display.data.pictures.not.found"), null);
};

function getTracks(vesselIdentification, vesselData) {
	var t = $("#tabsContainer").tabs();
	
	$$.UI.disableTabs(t, TABS.tracks);
	
	$("#numberOfPositions").text("");

	_TRACKS_NOTIFICATION = $$.Notification.info("<span>" + $$.I18n.getText("vessels.display.tracks.searching") + "&#160;<span class=\"spinner\">&#160;</span></span>", null, { delay: VRMF_FINDER_CONFIGURATION.timeouts.positionsManager, hide: false });
	
	$$.PositionsManager.setBaseURL(( $$.isSet(_SPECIFIC_SOURCE) ? _SPECIFIC_SOURCE + "/" : "" ) + _COMMON_SERVICES_VESSELS_SEARCH_URL_PREFIX);
	$$.PositionsManager.setTimeout(VRMF_FINDER_CONFIGURATION.timeouts.positionsManager);
	$$.PositionsManager.searchTracks(vesselIdentification, vesselData, displayTracks, tracksNotFound);
};

function tracksNotFound() {
	$$.Notification.change(_TRACKS_NOTIFICATION, 'error', $$.I18n.getText("vessels.display.data.tracks.not.found"), null);
};

function getInfringements(vesselIdentification, vesselData) {
	var t = $("#tabsContainer").tabs();
	
	$$.UI.disableTabs(t, TABS.infringements);
	
	$("#numberOfInfringements").text("");

	_INFRINGEMENTS_NOTIFICATION = $$.Notification.info("<span>" + $$.I18n.getText("vessels.display.infringements.searching") + "&#160;<span class=\"spinner\">&#160;</span></span>", null, { delay: VRMF_FINDER_CONFIGURATION.timeouts.infringementsManager, hide: false });
	
	$$.InfringementsManager.setBaseURL(( $$.isSet(_SPECIFIC_SOURCE) ? _SPECIFIC_SOURCE + "/" : "" ) + _COMMON_SERVICES_VESSELS_SEARCH_URL_PREFIX);
	$$.InfringementsManager.setTimeout(VRMF_FINDER_CONFIGURATION.timeouts.infringementsManager);
	$$.InfringementsManager.searchInfringements(vesselIdentification, vesselData, displayInfringements, infringementsNotFound);
};

function infringementsNotFound() {
	$$.Notification.change(_INFRINGEMENTS_NOTIFICATION, 'error', $$.I18n.getText("vessels.display.data.infringements.not.found"), null);
};

function getNews(vesselIdentification, vesselData) {
	$$.UI.disableTabs($("#tabsContainer"), TABS.news);
	
	if($$.isSet(vesselData) && $$.isSet(vesselData.nameData)) {
		var names = new $$.Set();
		
		for(var n=0; n<vesselData.nameData.length; n++)
			names.add(vesselData.nameData[n].name);
		
		names = names.asArray();
		
		if($$.isSet(names)) {
			$$.NewsManager.setClientIP(_CLIENT_IP);
			
			$$.NewsManager.search(names, displayNews);
		}
	}
};

function displayNews(data) {
	if($$.isSet(data)) {
		$("#newsResults").empty();
		
		var news = data.responseData.results;
		
		var ul = $("<ul/>");
		var li;
		for(var n=0; n<news.length; n++) {
			li = $("<li/>");
			li.append("<span class='publisher'>[ " + news[n].publisher + " ]</span>");
			li.append("<strong class='title'>" + news[n].titleNoFormatting + "</strong>");
			li.append("<em class='body'>" + news[n].content + "</em>");
			ul.append(li);
		}
		
		$("#newsResults").append(ul);
		
		$$.UI.enableTabs($("#tabsContainer").tabs(), TABS.news);
	}
};

function displayTracks(shipTracksData, vesselData) {
	var map = $("#positionsMap");
	var trackSelector = $("#trackSelector");
			
	if($$.isSet(shipTracksData)) {
		var numTracks = shipTracksData.length;
		
		var sources = new $$.Set();
					
		for(var p=0; p<shipTracksData.length; p++)
			sources.add(shipTracksData[p].source);
		
		var numSources = sources.asArray().length;

		var typeLabel = $$.I18n.getText("vessels.display.data.tracks.label.track" + ( numTracks > 1 ? ".multiple" : "" ));
		var sourceLabel = $$.I18n.getText("vessels.display.data.label.source" + ( numTracks > 1 ? ".multiple" : "" ));

		var tooltip = $$.I18n.getText("vessels.display.data.external.report", numTracks, typeLabel, numSources, sourceLabel);
		
		$$.Notification.change(_TRACKS_NOTIFICATION, "success", tooltip, null);

		$$.PositionsManager.buildTrackSelector(map, trackSelector, shipTracksData, vesselData);
		
		var placeholder = $("#numberOfPositions"); 
		
		placeholder.text(" [" + numTracks + " / " + numSources + "]").attr("title", tooltip);
		
		$$.TooltipManager.destroy(placeholder);
		$$.TooltipManager.apply(placeholder);
		
		$$.UI.enableTabs($("#tabsContainer").tabs(), TABS.tracks);
	} else { 
		tracksNotFound();
	}
};

function displayInfringements(shipInfringementsData, vesselData) {
	var container = $("#infringementsReport");
			
	if($$.isSet(shipInfringementsData)) {  
		var numInfringements = shipInfringementsData.length;
		
		var sources = new $$.Set();
					
		for(var p=0; p<shipInfringementsData.length; p++)
			sources.add(shipInfringementsData[p].source);
		
		var numSources = sources.asArray().length;

		var typeLabel = $$.I18n.getText("vessels.display.data.infringements.label.infringement" + ( numInfringements > 1 ? ".multiple" : "" ));
		var sourceLabel = $$.I18n.getText("vessels.display.data.label.source" + ( numInfringements > 1 ? ".multiple" : "" ));
		
		var tooltip = $$.I18n.getText("vessels.display.data.external.report", numInfringements, typeLabel, numSources, sourceLabel);
		
		$$.Notification.change(_INFRINGEMENTS_NOTIFICATION, "success", tooltip, null);

		$$.InfringementsManager.displayInfringements(container, vesselData, shipInfringementsData);
		
		var placeholder = $("#numberOfInfringements"); 
		
		placeholder.text(" [" + numInfringements + " / " + numSources + "]").attr("title", tooltip);
		$$.TooltipManager.destroy(placeholder);
		$$.TooltipManager.apply(placeholder);
		
		$("th, h4", container).addClass("ui-state-highlight");
		
		$$.UI.enableTabs($("#tabsContainer").tabs(), TABS.infringements);
	} else { 
		infringementsNotFound();
	}
};

function displayPictures(picturesData, vesselData) {
	var time = $$.Browser.msie ? 0 : 500;
	
	var thumb;
	var picture;

	var sources = new $$.Set();

	for(var p=0; p<picturesData.length; p++) {
		thumb = picturesData[p].thumb;
		picture = picturesData[p].picture;

		var provider = thumb.data("provider");
		thumb.data("provider", provider);
		thumb.data("picture", picture);

		sources.add(provider.source);

		thumb.unbind("click").click(function() {
			var target = $("#vesselPicture");
			
			scrollThumbs($(this), time);

			var currentPid = $(this).data("pid");

			$("#thumbs img").removeClass("current").removeClass(getDefaultActiveClasses());
			$(this).addClass("current").addClass(getDefaultActiveClasses());

			var linkedPicture = $(this).data("picture");
			
			if(target.attr("src") != linkedPicture.attr("src")) {
				$$.PicturesManager.startLoading();
	
				target.attr("src", linkedPicture.attr("src"));
				target.data("thumb", $(this));
				
				var details = $(".vesselPictureDetailsContainer .ui-widget-header");
				
				if(details.hasClass("collapsed"))
					details.click();
	
				$(".pictureWidth", $("#vesselPicture").get(0).width);
				$(".pictureHeight", $("#vesselPicture").get(0).height);
	
				$$.Log.debug("Displaying picture #" + currentPid);
			}
		});

		$("#thumbs").append(thumb);
	}
	
	if(picturesData.length > 0) {
		var picture = $("#vesselPicture"); 
		picture.attr("src", picturesData[0].picture.attr("src"));
		
		$$.PicturesManager.updatePicturesMetadata(picturesData[0].thumb, vesselData);
	}

	var imageThumbs = $("#thumbs img"); 

	if(imageThumbs.length > 0) {
		var numPictures = imageThumbs.length;
		var typeLabel = $$.I18n.getText("vessels.display.data.pictures.label.picture" + ( numPictures > 1 ? ".multiple" : "" ));
		var numSources = sources.size();
		var sourceLabel = $$.I18n.getText("vessels.display.data.label.source" + ( numSources > 1 ? ".multiple" : "" ));
		
		var tooltip = $$.I18n.getText("vessels.display.data.external.report", numPictures, typeLabel, numSources, sourceLabel);
		
		$$.Notification.change(_PICTURES_NOTIFICATION, "success", tooltip, null);

		var placeholder = $("#numberOfPictures"); 
		
		placeholder.text(" [" + imageThumbs.length + " / " + numSources + "]").attr("title", tooltip);
		$$.TooltipManager.destroy(placeholder);
		$$.TooltipManager.apply(placeholder);

		applyPicturesBehaviour();
		
		$$.UI.enableTabs($("#tabsContainer").tabs(), TABS.pictures);
	} else
//		$$.Notification.change(_PICTURES_NOTIFICATION, "error", $$.I18n.getText("vessels.display.data.pictures.not.found"));
		picturesNotFound();//$$.Notification.error($$.I18n.getText("vessels.display.data.pictures.not.found"), null, { after_open: function() { if($$.isSet(_PICTURES_NOTIFICATION)) _PICTURES_NOTIFICATION.pnotify_remove(); } });

	var currentThumbs = $(".thumbs[title]");
	
	if(currentThumbs.size() > 0) {
		$$.PicturesManager.applyPictureDetailsBehaviour();
		
		$$.TooltipManager.apply($(".thumbs[title]"), TOOLTIP_THUMBS_DEFAULTS);

		$$.TooltipManager.apply($("#vesselPicture"), TOOLTIP_SEARCH_RESULTS_DEFAULTS);

		$(".thumbs").addClass(getDefaultStatusClasses());
	}
};

function scrollThumbs(target, time, callback) {
	$("#thumbs").scrollTo(target, time, function() {
		$("#thumbs img.thumbs").lazyload();
		
		if($$.isSet(callback))
			callback();
	});
};

function firstPicture(show) {
	var first = $("#thumbs img:first");

	if(show && first.length != 0) {
		scrollThumbsTo(first);
	}

	return first;
};

function nextPicture(show) {
	var next = $("#thumbs img.current").next("img");

	if(show && next.length != 0) {
		scrollThumbsTo(next);
	}

	return next;
};

function previousPicture(show) {
	var previous = $("#thumbs img.current").prev("img");

	if(show && previous.length != 0) {
		scrollThumbsTo(previous);
	}

	return previous;
};

function lastPicture(show) {
	var last = $("#thumbs img:last");

	if(show && last.length != 0) {
		scrollThumbsTo(last);
	}

	return last;
};

function scrollThumbsTo(picture) {
	var scrollTime = $$.Browser.msie ? 0 : 500;

	picture.click();

	$("#currentPictures").mouseover();
	
	scrollThumbs(picture, scrollTime);
};

function displayFirstPicture() {
	if($("#thumbs img").length > 0)
		$("#thumbs img:first").click();
};

function applyPicturesBehaviour() {
	$("#currentPictures").unbind("hover").hover(function() {
		var showPrevious = previousPicture(false).length != 0;
		var showNext = nextPicture(false).length != 0;
		
		var fadeTime = $$.Browser.msie  ? 0 : 100;

		if(showNext)
			$("#nextPicture .pictureNavigation").fadeIn(fadeTime);
		else
			$("#nextPicture .pictureNavigation").fadeOut(fadeTime);

		if(showPrevious)
			$("#previousPicture .pictureNavigation").fadeIn(fadeTime);
		else
			$("#previousPicture .pictureNavigation").fadeOut(fadeTime);
	}, function() {
		var fadeTime = $$.Browser.msie  ? 0 : 100;

		$("#previousPicture .pictureNavigation, #nextPicture .pictureNavigation").fadeOut(fadeTime);
	});

	$("#scrollFirst, #scrollLeft, #scrollRight, #scrollLast").unbind("click").click(function() {
		var first = $(this).attr("id").toLowerCase().indexOf("first") >= 0;
		var left = $(this).attr("id").toLowerCase().indexOf("left") >= 0;
		var right = $(this).attr("id").toLowerCase().indexOf("right") >= 0;

		var time = $$.Browser.msie  ? 0 : 500;

		$$.Log.debug("Scrolling thumbnails to " + ( first ? "first" : ( left ? "left" : ( right ? "right" : "last" ) ) ) + "...");

		var current = $("#thumbs img.current");
		var target = null;

		if(current.length == 0)
			target = $("#thumbs img:first");
		else {
			target = first ? $("#thumbs img:first") : ( left ? current.prev() : ( right ? current.next() : $("#thumbs img:last") ) );
		}

		scrollThumbs(target, time);

		target.click();
	});
};

function updateVesselDetails(target) {
	var targetId = target.attr("id");

	buildVesselDetails(target, VESSEL_DATA, getVesselFilter($(".vesselPicker", target)), getSystemFilter($(".systemPicker", target)), targetId == 'current' ? getDateFilter($(".dateSelector", target)) : null, _CUSTOM_SORTERS);

	updateCounters(target);
	
	buildHistoricalQuickJump();

	return false;
};

function updateTimeline(target) {
	buildVesselTimeline(target, VESSEL_DATA, getVesselFilter($(".vesselPicker", target)), getSystemFilter($(".systemPicker", target)), null, _CUSTOM_SORTERS);

	updateCounters(target);

	buildTimelineQuickJump();
	
	return false;
};

function displayVesselDetailsForSystem(target) {
	initializeVesselpicker($(".vesselPicker", target), VESSEL_DATA);
	initializeDateSelector($(".dateSelector", target), VESSEL_DATA);
	
	updateVesselDetails(target);
};

function displayVesselDetailsForVessel(target) {
	initializeDateSelector($(".dateSelector", target), VESSEL_DATA);

	updateVesselDetails(target);
};

function displayVesselDetailsAtDate(target) {
	updateVesselDetails(target);
};

function displayTimelineForSystem(target) {
	initializeVesselpicker($(".vesselPicker", target), VESSEL_DATA);

	updateTimeline(target);
};

function displayTimelineForVessel(target) {
	updateTimeline(target);
};

function applyColorBySource(targetContainer) {
	if(colorizeBySource(targetContainer)) {
		targetContainer.addClass('colorizeBySource');
		$('.attribute:not(.empty) .data', targetContainer).addClass('removeUI');
	} else {
		targetContainer.removeClass('colorizeBySource');
		$('.attribute .data', targetContainer).removeClass('removeUI');
	}

	return true;
};

function displayHiddenAttributes(targetContainer) {
	if(hideEmptyAttributes(targetContainer)) {
		$(".attribute.empty:not(.private)", targetContainer).hide();
	} else {
		$(".attribute.empty:not(.private)", targetContainer).each(function() {
			var $this = $(this);
			var forceHidden = $this.data("forceHidden");

			if(!$$.isSet(forceHidden) || !forceHidden)
				$this.show();
		});
	}

	return true;
};

function rollDate(current, up) {
	var dateSelector = $("#" + ( $$.isSet(current) && current ? "current" : "historical" ) + "DateSelector");
	var current = $("option:selected", dateSelector);
	
	if($$.isSet(current) && current.size() > 1)
		current = $(":first", current);

	var rolled = up ? current.prev("option") : current.next("option");

	if(rolled.length > 0 && rolled.text != current.text()) {
		$$.select(rolled);

		dateSelector.change();
	} else {
		$$.Interface.warn($$.I18n.getText("vessels.display.data.current.timeline." + ( up ? "end" : "start" )));
	}

	return false;
};

function rollUp(current) {
	return rollDate(current, true);
};

function rollDown(current) {
	return rollDate(current, false);
};

function updateCounters(target) {
	$(".counter", target).text("");

	$(".detailsContainer .attributeHeading label").each(function() {
		var heading = $(this).parents(".attributeHeading");
		var container = $(this).parents(".attribute");
		var dataContainer = $(".data", container);
		var data = $("div.attributeContainer", dataContainer);
		var sources = new $$.Set();
		
		data.each(function() {
			sources.add($(".attributeMeta .sourceSystem", $(this)).text());
		});
		
		var numSources = sources.asArray().length;
		
		$(this).unbind("click");

		if(data.length > 0) {
			container.removeClass("expanded").removeClass("collapsed").addClass("expanded");

			var counter = $(".counter", $(this));

			var metadata = counter.metadata();
			
			var counterType = $$.isSet(metadata.type) ? metadata.type : "record";
			var countSources = true || $$.isSet(metadata.countSources) && metadata.countSources;

			var _type = $$.I18n.getText("vrmf.vessels.display.common.entry.type." + counterType + ( data.length > 1 ? ".multiple" : "" ));
			var _sources = $$.I18n.getText("vrmf.vessels.display.common.entry.source" + ( numSources > 1 ? ".multiple" : "" ));
			
			counter.text("[ " + 
							data.length + " " + _type +
						  ( countSources && numSources > 0 ? " / " + numSources + " " + _sources : "" ) +
						" ]");

			heading.unbind("click").click(function() { 
				container.toggleClass("expanded");
				container.toggleClass("collapsed");
			});
		}  
		
		if(container.hasClass("startCollapsed"))
			container.addClass("collapsed").removeClass("expanded");
	});
};

function expandNodes(container) {
	$(".attribute", container).removeClass("collapsed").addClass("expanded");
};

function collapseNodes(container) {
	$(".attribute", container).removeClass("expanded").addClass("collapsed");
};

function toggleNodes(container) {
	$(".attribute", container).toggleClass("expanded").toggleClass("collapsed");
};