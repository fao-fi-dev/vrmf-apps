var _LAST_V_POS = 0;
var _LAST_H_POS = 0;

var _SCROLL_HANDLER = function(event) {
	$$.Log.debug("Handling scroll / resize event");
	
	$$.TooltipManager.close($$.isSet(event) && $$.isSet(event.target) ? $(event.target) : $(this)); //Was: .close($(this))
	
	try {
		var header = $("#commonHeader");
		var filters = $("#searchCriteria");
		var searchResults = $("#searchResults");

		var resultsPinned = $("#results").is(".pinned");
		var resultsExpanded = $("#results").is(".expanded");
		var resultsOverflows = $("#results").height() > $(window).height();
		
		var VPosition = $(window).scrollTop();
		var HPosition = $(window).scrollLeft();

		var sticky = resultsPinned && !resultsOverflows;
		
		if(sticky) {
			var onTop  = VPosition == 0;
			var atLeft = HPosition == 0;

			var hScroll = _LAST_H_POS != HPosition;
			var vScroll = _LAST_V_POS != VPosition;
			
			_LAST_V_POS = VPosition;
			_LAST_H_POS = HPosition;
			
			var left = filters.outerWidth() + filters.offset().left + 12 - HPosition;
			var top = header.outerHeight() + 8;

			var css = {};
			
			var reset = onTop && atLeft;
			
			if(reset) {
				css.position = 'relative';
				css.top = 0;
				css.left = 0;
			} else {
				css.position = hScroll ? 'relative' : 'fixed';
				css.top = hScroll ? VPosition : top;
				css.left = hScroll ? 0 : left;
			}
			
			searchResults.css(css);
			
			if($$.UI.nicescrollEnabled)
				_NICESCROLL_UPDATER();
		} else {
			searchResults.css({
				position: 'relative',
				top: 0,
				left: 0
			});
		}
	} catch (E) {
		;
	}
	
	if($$.UI.nicescrollEnabled)
		_NICESCROLL_UPDATER();
	
	if($$.isSet(event))
		event.stopImmediatePropagation();
};

var _UNLOAD_HANDLER = function() {
	if(!$("#vesselsData").hasClass("hidden")) {
		return $$.I18n.getText("common.interface.back.warning");
	}
};

var _DEFAULT_MULTISELECT_OPTIONS = {
	open: function(event, ui) {
		if($$.UI.nicescrollEnabled) {
			$(".ui-multiselect-checkboxes", ui.delegateTarget).addClass("scrollable nicescroll").niceScroll({
				zindex: 10,
				cursorwidth: 10,
				scrollspeed: 0,
				autohidemode: false,
				grabcursorenabled: true
			});
			
			_NICESCROLL_COLOR_UPDATER();
		}
	},
	close: function(event, ui) {
		if($$.UI.nicescrollEnabled) 
			$(".ui-multiselect-checkboxes", ui.delegateTarget).addClass("scrollable").niceScroll("hide");
	}	
};

function setStatus(text) {
	setStatusMessage(text, false);
};

function setError(text) {
	setStatusMessage(text, true);
};

function setStatusMessage(text, error) {
	var element = $("#statusText");

	element.stop().clearQueue().fadeOut(300, function() { 
		var now = new Date();
		$(this).text(now.formatTime() + ": " + text).css("opacity", "1");

		if(!error)
			$(this).removeClass("errorMessage");
		else
			$(this).addClass("errorMessage");

		$(this).show().fadeOut(10000); 
	}); 
};

function detach(byUID, ID, detachLinkElement) {
	var targetAttr = $(detachLinkElement).is("button") ? "formtarget" : "target";
	var targetHref = $(detachLinkElement).is("button") ? "formaction" : "href";
	var URL = ( $$.isSet(_SPECIFIC_SOURCE) ? _SPECIFIC_SOURCE + "/" : "" ) + ( byUID ? _VRMF_BROWSER_DISPLAY_VESSEL_BY_UID_URL_PREFIX : _VRMF_BROWSER_DISPLAY_VESSEL_BY_ID_URL_PREFIX ) + ID;
	
	URL += _CURRENT_LANG === _DEFAULT_LANG ? "" : "/" + _CURRENT_LANG;
	
	detachLinkElement.attr(targetAttr, "_VRMF_BROWSER_" + ( byUID ? "UID" : "ID" ) + "_" + ID);
	detachLinkElement.attr(targetHref, URL);
};

function showVesselDetailsPanel() {
	hideAllPanels();

	$("#vesselDetails").removeClass("hidden");
};

function showSearchResultsPanel() {
	$$.Notification.clear();

	hideAllPanels();
	
	var toShow = $("#searchCriteria, #searchResults");
	toShow.removeClass("hidden");
	
	if($$.UI.nicescrollEnabled)
		toShow.trigger("resize.ui");

	try {
		//updateExportResultsLink();
	} catch (E) {
		;
	}
	
	$(window).resize();
	
	return false;
};

function initializeInterface(callback) {
	updateInterfaceAccordingToUserGrants($$.UserManager.getLoggedUser());
	
	updateSourceImages();
	
	configureTooltips([ 
	                   { selector: "#searchCriteria .fieldAutocompletes", config: TOOLTIP_SEARCH_FILTER_AUTOCOMPLETE_DEFAULTS },
	                   { selector: "#searchCriteria [title], " +
	                   			   "#vesselDetails [title]", config: TOOLTIP_SEARCH_FILTER_DEFAULTS },
	                   { selector: "#selectedVesselsDetails [title]", config: TOOLTIP_DEFAULTS },
	                   { selector: ".searchResultsHeader div", config: TOOLTIP_SEARCH_RESULTS_HEADER_DEFAULTS }  
	                  ]);
	
	$(window).scroll(_SCROLL_HANDLER).
			  resize(_SCROLL_HANDLER).
			  on("beforeunload", _UNLOAD_HANDLER); 
		
	if($$.isSet(callback))
		callback();
};