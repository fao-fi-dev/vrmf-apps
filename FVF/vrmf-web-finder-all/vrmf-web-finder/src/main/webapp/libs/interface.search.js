var _SORTERS = { };
var _SEQUENTIAL_SORTERS = [];

function tweakInterface() {
	$(".fieldAutocompletes").each(function() {
		$(this).attr("title", $$.I18n.getText("vrmf.search.controls.autocompletion.tip")).after($("<span class='autocompletionMarker ui-icon ui-icon-search' title='" + $$.I18n.getText("vrmf.search.controls.autocompletion.tip") + "'></span>"));
	});

	$(".searchResultsHeader").each(function() {
		var firstColumn = $("div:first", $(this));
		var width = parseInt(firstColumn.css("width").replace(/px/g, ""), 10);
		firstColumn.attr("style", "width: " + (width - 1) + "px !important");
	});

	$("#formContainer").scroll(function(event) {
		var $this = $(this);

		$(".datePicker", $this).datepicker("hide");
		$(".fieldAutocompletes", $this).each(function() {
			if($$.UI.isAutocomplete($(this)))
				$(this).autocomplete("close");
		});
		
		$("select", $this).each(function() {
			if($$.UI.isMultiselect($(this)) && $(this).multiselect("isOpen"))
				$(this).multiselect("close");
		});
	});
};

function installAdditionalBehaviours() {
	installTabsBehaviour();
	installFieldsetsBehaviour();
	installSortingControlsBehaviour();
	installSpecialControlsBehaviour();	
};

function installTabsBehaviour() {
	$("#tabsContainer").tabs();
	
	$("#tabs").removeClass("ui-corner-all");
};

function installFieldsetsBehaviour() {
	$("#searchForm fieldset, #fastSearchForm fieldset").hover(function() {
		$("legend", $(this)).addClass("ui-state-active");
	}, function() {
		$("legend", $(this)).removeClass("ui-state-active");
	});	
};

function installSpecialControlsBehaviour() {
	var searchTypes = $("#searchCriteria .actionsTop");
	
	$("input[name='searchType']", searchTypes).change(function() {
		var $this = $(this);
		
		var selectedType = $this.val();

		if(!$$.isSet(selectedType))
			selectedType = "stats";
			
		var location = window.location.href.replace(/\#.*/g, "");
		window.location.href = location + "#" + selectedType.toLowerCase();
		
		if("quick" === selectedType)
			enableQuickSearch();
		else if("basic" === selectedType)
			enableBasicSearch();
		else if("advanced" === selectedType)
			enableAdvancedSearch();
		else 
			enableStatsSearch();
			
		return true;
	});

	var searchCriteria = $("#searchCriteria");
	
	$("input[name='historicalSearch']", searchCriteria).change(function() {
		updateSearchFilters(enableInputs);
	});

	$("input[name='groupByUID']", searchCriteria).change(function() {
		var resetAll = function() { clearSearchResults(); updateSearchFilters(enableInputs); };
		var hasResults = parseInt($("#currentResults").text(), 10) > 0;

		var currentSelected = $("[name='groupByUID']:checked", searchCriteria).val();

		if(hasResults) {
			confirm($$.I18n.getText("search.controls.change.grouping"),
					resetAll,
					function() { 
						$("[name='groupByUID']", searchCriteria).each(function() {
							if($(this).val() != currentSelected) {
								$$.Utils.check($(this));
							}
						});

						$("#groupingButtonset").buttonset("refresh");
					});
		} else
			resetAll();
	});
	
	$("input[name='authStatus']", searchCriteria).change(function() {
		var $this = $(this);

		if($$.isSet($this.val()) && $this.val() != '') {
			$(".hideIfAuthStatusNotSelected", searchCriteria).removeClass("hidden");

			var authStatusTiming = $("[name='authStatusTiming']:checked", searchCriteria).val();

			if(authStatusTiming === "atDate" )
				$(".hideIfNow", searchCriteria).removeClass("hidden");
			else
				$(".hideIfNow", searchCriteria).addClass("hidden");
		} else {
			$(".hideIfAuthStatusNotSelected").addClass("hidden");
		}
	});

	$("input[name='authStatusTiming']", searchCriteria).change(function() {
		var authStatusTiming = $("[name='authStatusTiming']:checked", searchCriteria).val();

		if(authStatusTiming === "atDate" )
			$(".hideIfNow", searchCriteria).removeClass("hidden");
		else
			$(".hideIfNow", searchCriteria).addClass("hidden");
	});
};

function installSortingControlsBehaviour() {
	$(".searchResultsHeader div").each(function() {
		if($(this).hasClass("sortable")) {
			$(this).addClass("ui-state-default");

			$(this).append("<span class='arrowDown'>↑</span>").append("<span class='arrowUp'>↓</span>");
			$(this).attr("title", $(this).attr("title") + ". " + $$.I18n.getText("search.controls.results.header.additional.sorting.tip"));

			$(this).each(function() {
				$(this).hover(function() {
					$(this).addClass("ui-state-hover");
				}, function() {
					if(!$(this).hasClass("asc") && !$(this).hasClass("desc"))
						$(this).removeClass("ui-state-hover");
				});
			});

			$(this).click(function(event) {
				if(isQuickSearch()) {
					$$.Interface.warn($$.I18n.getText("search.quick.controls.sort.disabled"));
					
					return false;
				}

				var id = $(this).attr("id").substring(5);

				var sequential = event.shiftKey;

				if(!sequential) {
					_SEQUENTIAL_SORTERS = [ id ];
				} 

				if($$.isSet(_SORTERS[id])) {
					var order = _SORTERS[id];

					if(order == 'a')
						_SORTERS[id] = 'd';
					else
						_SORTERS[id] = sequential ? 'a' : null;
				} else {
					_SORTERS[id] = 'a';
				}

				if(sequential)
					$(this).removeClass("asc desc ui-state-active");
				else 
					$(".searchResultsHeader div.sortable").removeClass("asc desc ui-state-active");

				if(_SORTERS[id] != null)
					$(this).addClass(_SORTERS[id] == 'a' ? 'asc' : 'desc');

				if(sequential && (_SEQUENTIAL_SORTERS.length == 0 || _SEQUENTIAL_SORTERS.indexOf(id) < 0)) {
					_SEQUENTIAL_SORTERS.push(id);
				}

				if(_SEQUENTIAL_SORTERS.length > 0)
					var params = _LAST_SEARCH;
					params.sc = getSortingParameters();
				
					return doSearch(params);
			});
		}

		var width = parseInt($(this).css("width").replace(/px/g, ""), 10);
		$(this).attr("style", "width: " + (width - 1) + "px !important");
	});
};

function initializeSearchControls() {
	buildIdentifierTypes();

	buildStatuses();
	buildAgeClass();
	
	buildFlags();
	buildRegistrationCountries();
	buildFishingLicenseCountries();
	buildAuthorizationReportingCountries();
	
	buildLengthTypes();
	buildTonnageTypes();
	
	buildMainEnginePowerTypes();
	buildAuxEnginesPowerTypes();
	
	buildVesselSourcesGroup("#source", $$.Metadata.SYSTEMS_GROUPS);
	buildAuthorizationSourcesGroup("#issuingAuthority", $$.Metadata.AUTH_SYSTEMS_GROUPS);
	
	if(countAvailableVesselSources() == 1) {
		$("#liveStatsSourcesFieldset, " +
		  "#sourcesSearch, " +
		  "#liveSources, " +
		  "#liveSourcesHeading").hide();
		
	}
	
	buildAuthorizationTypes();
	
	addAutocompletion();
	
	initializeSliders();
	initializeMultiselects($("#formContainer select"));
};

function makeSearchInterfaceConfigurable() {
	var panelPositions = 0;
	$("#searchForm > ul > li").each(function() {
		$(this).attr("_original_position", panelPositions++);
	});
	
	$("#searchForm > ul").sortable({
		axis: 'y',
		placeholder: 'ui-state-highlight', 
		forcePlaceholderSize: true,
		handle: 'legend .handle',
		opacity: .6,
		start: function(event, ui) {
			$$.Log.debug("Started!");
			
			$$.TooltipManager.close($("legend .handle", ui.item));
		},
		stop: function() {
			storeInterfaceStatus();
		}
	});	
};

function storeInterfaceStatus() {
	var sequence = [];

	var counter = 0;
	$("#searchForm > ul > li").each(function() {
		sequence.push({ id: $("div:first", $(this)).attr("id"), position: counter++, collapsed: $("legend", $(this)).hasClass("collapsed") });
	});

	//$.cookie("vrmf.browser.interface", JSON.stringify(sequence), { path: '/' });
	localStorage["vrmf.browser.interface"] = JSON.stringify(sequence);
};

function loadInterfaceStatus() {
	//var status = $.cookie("vrmf.browser.interface");
	var status = localStorage["vrmf.browser.interface"];

	if($$.isSet(status)) {
		try {
			var configuration = $.parseJSON(status);

			var filter;
			for(var c=0; c<configuration.length; c++) {
				filter = $("#" + configuration[c].id);
				filter.parent("li").addClass("sortedElement").attr("position", configuration[c].position);

				if(configuration[c].collapsed) {
					$("legend", filter).addClass("collapsed").next("ul, div").addClass("hidden");
				}
			}

			$(".sortedElement").sortElements(function(a, b) {
				var aOrder = parseInt($(a).attr("position"), 10);
				var bOrder = parseInt($(b).attr("position"), 10);
				return aOrder < bOrder ? -1 : aOrder > bOrder ? 1 : 0;
			});

		} catch ( E ) {
			$$.Interface.error("Unable to restore interface status: " + E);
		}
	}
};

function resetInterfaceStatus() {
	$("#searchForm legend").removeClass("collapsed").next("ul, div").removeClass("hidden");
	$("#searchForm > ul > li").sortElements(function(a, b) {
		var aOrder = parseInt($(a).attr("_original_position"), 10);
		var bOrder = parseInt($(b).attr("_original_position"), 10);
		
		return aOrder < bOrder ? -1 : aOrder > bOrder ? 1 : 0;
	});	
	
	localStorage.removeItem("vrmf.browser.interface");
};

function resetCriteria() {
	disableInputs($$.I18n.getText("vrmf.common.interface.resetting"), $$.I18n.getText("vrmf.common.interface.please.wait")); 

	_SORTERS = {};
	_SEQUENTIAL_SORTERS = [];
	
	document.forms[0].reset();

	initializeSearchControls();
	
	//Fixes for issue #FVF-98: See: http://193.43.36.238:9090/browse/FVF-98 
	$("#lengthClass, #tonnageClass, #mainEnginePowerClass, #auxEnginePowerClass").multiselect("disable");
	$("#authorizationsOptions [name='authStatus']:checked").change();
	//Fixes for issue #FVF-98: END
	
	$(".sortable").removeClass("asc desc");

	$("input[type='hidden']").val("");
	$(".emptyOnReset").text("").attr("title", null);
	$$.TooltipManager.destroy($(".emptyOnReset"));
	
	clearSearchResults();

	if(isNormalSearch())
		enableBasicSearch();
	else
		enableAdvancedSearch();

	//$.cookie("vrmf.browser.interface", null, { path: '/' });
	
	resetInterfaceStatus();
	
	enableInputs();

	return false;
};

function route() {
	var searchType = "unknown";
	
	var anchor = null;
	var params = null;
	
	var anchorIndex = window.location.href.indexOf("#");
	
	if(anchorIndex > 0) {
		anchor = window.location.href.substring(anchorIndex + 1);
	
		var colonIndex = anchor.indexOf(":");
		
		if(colonIndex > 0) {
			params = anchor.substring(colonIndex + 1);
			anchor = anchor.substring(0, colonIndex);
		}
	}
	
	switch(anchor) {
		case "stats":
		case "quick":
		case "basic":
		case "advanced":
			searchType = anchor;
			break;
		default:
			searchType = "stats";
	}
	
	var mappings = {
		stats: { 
			tab: 'stats',
			init: enableStatsSearch
		},
		quick: {
			tab: 'quick',
			init: enableQuickSearch
		},
		basic: {
			tab: 'basic',
			init: enableBasicSearch
		},
		advanced: {
			tab: 'advanced',
			init: enableAdvancedSearch
		}
	};
			
	$("#searchCriteria .actionsTop input[name='searchType']").each(function() {
		if($(this).val() === mappings[searchType].tab)
			$$.Utils.check($(this));
		
		$("#searchCriteria .actionsTop input[name='searchType']").button("refresh");
	});

	mappings[searchType].init(params);
}

function initializeSearchInterface(callback) {
	$$.Log.debug("Initializing search interface...");
	
	makeSearchInterfaceConfigurable();
	
	loadInterfaceStatus();
	
	initializeSearchControls();
	installAdditionalBehaviours();
	
	tweakInterface();
	
	route();
	
	$$.Log.debug("Search interface has been initialized");
	
	if($$.isSet(callback)) {
		$$.Log.debug("Invoking search interface post-initialization callback...");
		
		callback();
		
		$$.Log.debug("Search interface post-initialization callback has been invoked");
	}
};