function initializeMultiselects(target) {
	var elements = $$.isSet(target) ? $(target) : $("select");

	elements.each(function() {
		initializeMultiselect(this);
	});
 
	return elements;
};

function initializeMultiselect(target) {
	$$.Log.debug("main :: initializeMultiselect " + $(target).attr("id"));

	var end, start = new Date().getTime();

	var $this = $(target);
	if(!$this.hasClass("simple")) {
		if($this.is("select[multiple='multiple']")) {
			doInitializeMultiSelect($this);

			if($this.is(":disabled"))
				$this.multiselect("disable");

		} else {
			doInitializeSingleSelect($this);

			if($this.is(":disabled"))
				$this.multiselect("disable"); 
		}
	}

	end = new Date().getTime();

  	$$.Log.debug("main :: initializeMultiselect for " + ( $(target).attr("id") ) + " took " + ( end - start ) + " mSec");

	return $this;
};

function doInitializeSingleSelect(element) {
	var end, start = new Date().getTime();

	var select = $(element);

	$$.Log.debug("Initializing singleselect #" + select.attr("id"));

	var isOHidden = select.hasClass("oHidden");
	var isHAuto = select.hasClass("hAuto");
	var isWAuto = select.hasClass("wAuto");
	var isNoWrap = select.hasClass("noWrap");

	var classes = [ ];

	if(!isOHidden)
		classes.push("oAuto");

	if(isWAuto)
		classes.push("wAuto");

	if(select.hasClass("small"))
		classes.push("small");

	if(select.hasClass("medium"))
		classes.push("medium");

	if(select.hasClass("large"))
		classes.push("large");

	if(isNoWrap)
		classes.push("noWrap");

	var hasDefaults = $("option.default", select).size() > 0;

	if(hasDefaults)
		$$.select($("option.default", select));
	else
		$$.select($("option:first", select));

	var SINGLESELECT_OPTIONS = $.extend(true, {}, _DEFAULT_MULTISELECT_OPTIONS, { 
		position: MULTISELECT_POSITION,
		multiple: false,
		selectedList: 1,
		classes: classes.join(" "),
		height: isHAuto ? "auto" : "195px",
		header: false,
		click: function(event, ui){
			select.multiselect("close");

			return true;
		}
	});

	var customOptions = OPTIONS_BY_SELECT_SINGLE[select.attr("id")]; 

	if($$.isSet(customOptions)) {
		SINGLESELECT_OPTIONS = $.extend(true, {}, SINGLESELECT_OPTIONS, customOptions);
	}

	$$.UI.destroyMultiselect(select).multiselect(SINGLESELECT_OPTIONS);

	if($$.isSet(customOptions) && $$.isSet(customOptions.afterInitialization))
		customOptions.afterInitialization();

	end = new Date().getTime();

	$$.Log.debug("Initializing singleselect " + ( element.attr("id") ) + " took " + ( end - start ) + " mSec.");
};

function doInitializeMultiSelect(element) { 
	var end, start = new Date().getTime();

	var select = $(element);

	$$.Log.debug("Initializing multiselect #" + select.attr("id"));

	var isHAuto = select.hasClass("hAuto");
	var isWAuto = select.hasClass("wAuto");
	var isNoWrap = select.hasClass("noWrap");

	var classes = [ "oAuto" ];

	if(!isWAuto) {
		if(select.hasClass("small"))
			classes.push("small");

		if(select.hasClass("medium"))
			classes.push("medium");
		 
		if(select.hasClass("large"))
			classes.push("large");
	} 

	if(isNoWrap)
		classes.push("noWrap");

	var MULTISELECT_OPTIONS = $.extend(true, {}, _DEFAULT_MULTISELECT_OPTIONS, { 
		header: true,
		position: MULTISELECT_POSITION,
		classes: classes.join(" "),
		height: isHAuto ? "auto" : "195px"
	});

	if(isWAuto)
		MULTISELECT_OPTIONS.minWidth = "auto";

	if($$.isSet(OPTIONS_BY_SELECT_MULTIPLE[select.attr("id")])) {
		MULTISELECT_OPTIONS = $.extend(true, {}, MULTISELECT_OPTIONS, OPTIONS_BY_SELECT_MULTIPLE[select.attr("id")]);
	}
	
	if($$.isSet(MULTISELECT_OPTIONS.noneSelectedTextLabel)) {
		MULTISELECT_OPTIONS.noneSelectedText = $$.I18n.getText(MULTISELECT_OPTIONS.noneSelectedTextLabel);
		
		delete MULTISELECT_OPTIONS.noneSelectedTextLabel;
	}
	 
	var multiselect = $$.UI.destroyMultiselect(select).multiselect(MULTISELECT_OPTIONS); 
	multiselect.multiselectfilter();
	
	if($$.isSet(MULTISELECT_OPTIONS.bindings)) {
		MULTISELECT_OPTIONS.bindings(select, multiselect);
	};
	
	end = new Date().getTime();
	 
	$$.Log.debug("Initializing multiselect " + ( element.attr("id") ) + " took " + ( end - start ) + " mSec.");
};

function getMultiselectedValues(select) {
	return select.multiselect("getChecked").map(function(){
		   return this.value;
	}).get();
};

function setMultiselectedValues(select, values) {
	var widget = select.multiselect("widget");
	select.multiselect("enable");

	for(var v=0; v<values.length; v++) {
		$(":checkbox[value='" + values[v] + "']", widget).click();
	}
};