function toggleMultipleSourcesReport() {
	$("#multipleSourcesReport").fadeOut(function() { 
		var $this = $(this);
		
		$this.toggleClass("down up");
		
		if($this.is(".down")) {
			$("#keywords").before($this);
		} else {
			$("#tabsContainer").before($this);
		}
		
		$this.fadeIn();
	});
	
	return false;
};

function updateVesselPageTitle(groupBy, formattedID) {
	$$.Interface.appendToTitle(" - Details for vessel with " + groupBy + " #" + formattedID + " / " + $(".currentVesselName").text());
};

function initializeDisplayInterface(callback) { 
	var customInitializeDisplayInterface = function() {
		$(".embeddedLinks").remove();
		
		updateSourceImages();
		
		if($$.isSet(callback))
			callback();
	};
	
	initializeCommonInterface(customInitializeDisplayInterface);
};
