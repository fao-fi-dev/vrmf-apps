var _STATS_SEARCH_GEO_CHARTS_CONTINENTS_DATA = {
	//See: https://developers.google.com/chart/interactive/docs/gallery/geochart#Configuration_Options
	"vrmf.geo.africa": {
		code: '002',
		subContinents: {
			"vrmf.geo.africa.north": {
				code: '015'		
			},
			"vrmf.geo.africa.west": {
				code: '011'		
			},
			"vrmf.geo.africa.middle": {
				code: '017'		
			},
			"vrmf.geo.africa.east": {
				code: '014'		
			},
			"vrmf.geo.africa.south": {
				code: '018'		
			}
		}
	},
	"vrmf.geo.europe": {
		code: '150',
		subContinents: {
			"vrmf.geo.europe.north": {
				code: '154'		
			},
			"vrmf.geo.europe.west": {
				code: '155'		
			},
			"vrmf.geo.europe.east": {
				code: '151'		
			},
			"vrmf.geo.europe.south": {
				code: '039'		
			}
		}
	},
	"vrmf.geo.americas": {
		code: '019',
		subContinents: {
			"vrmf.geo.americas.north": {
				code: '021'		
			},
			"vrmf.geo.americas.caribbean": {
				code: '029'		
			},
			"vrmf.geo.americas.central": {
				code: '013'		
			},
			"vrmf.geo.americas.south": {
				code: '005'		
			}
		}
	},
	"vrmf.geo.asia": {
		code: '142',
		subContinents: {
			"vrmf.geo.asia.central": {
				code: '143'		
			},
			"vrmf.geo.asia.east": {
				code: '030'		
			},
			"vrmf.geo.asia.south": {
				code: '034'		
			},
			"vrmf.geo.asia.south.east": {
				code: '035'		
			},
			"vrmf.geo.asia.west": {
				code: '145'		
			}
		}
	},
	"vrmf.geo.oceania": {
		code: '009',
		subContinents: {
			"vrmf.geo.oceania.australia.newzealand": {
				code: '053'		
			},
			"vrmf.geo.oceania.melanesia": {
				code: '054'		
			},
			"vrmf.geo.oceania.micronesia": {
				code: '057'		
			},
			"vrmf.geo.oceania.polynesia": {
				code: '061'		
			}
		}
	}
};

var _STATS_BASE_DEFAULT_BLOCK_UI_PARAMS = { 
	theme: false, 
	baseZ: 10,
	css: { 
		backgroundColor: 'transparent', 
		border: 0
	}
};

var _STATS_SEARCH_DEFAULT_BLOCK_UI_PARAMS = $.extend(true, {}, _STATS_BASE_DEFAULT_BLOCK_UI_PARAMS, {
	message: "<div class='spinner' style='margin-left: 130px; height: 106px;'>&nbsp;</div>"
});

var _STATS_UPDATE_DEFAULT_BLOCK_UI_PARAMS = $.extend(true, {}, _STATS_BASE_DEFAULT_BLOCK_UI_PARAMS, {
	message: "<div class='spinner'>&nbsp;</div>"
});

function blockStatsPanels(options) {
	$("#liveStatsSourcesContainer, " +
	  "#liveStatsFlagsContainer, " +
	  "#liveStatsTypesContainer, " +
	  "#liveStatsLengthsContainer").block($$.isSet(options) ? options : _STATS_BASE_DEFAULT_BLOCK_UI_PARAMS);
};

function unblockStatsPanels() {
	$("#liveStatsSourcesContainer, " +
	  "#liveStatsFlagsContainer, " +
	  "#liveStatsTypesContainer, " +
	  "#liveStatsLengthsContainer").unblock();
};

function enableStatsSearch(params) {
	$("#formContainer, #liveSearchFormContainer, .actionsBottom").addClass("hidden");

	enableCommonSearch();

	$("#liveStatsFormContainer").removeClass("hidden");

	liveStats(params, liveStatsSearch);

	return false;
};

function getCountryISO2ForID(countryID) {
	var country = $$.Metadata.getCountryByID(countryID);

	if($$.isSet(country.iso2Code))
		return country.iso2Code;

	return countryID;
};

function disableCounters() {
	$(".counter", $("#liveStatsFormContainer")).addClass("hidden");
	$(".placeholder", $("#liveStatsFormContainer")).removeClass("hidden");
};

function enableCounters() {
	updateAllStatsCounters();
	
	$(".counter", $("#liveStatsFormContainer")).removeClass("hidden");
	$(".placeholder", $("#liveStatsFormContainer")).addClass("hidden");
};

function toggleCounters() {
	$(".counter, .placeholder", $("#liveStatsFormContainer")).toggleClass("hidden");
};

function updateStatsCounters(fieldset) {
	var numSelected = $(".counter .numSelected", fieldset);
	var numAvailable = $(".counter .numAvailable", fieldset);
	var numVessels = $(".counter .numVessels", fieldset);
	
	var selected = $(".liveStatsLabel.selected", fieldset).size();
	var available = $(".liveStatsEntry", fieldset).size();
	var vessels = 0;
	
	$(".liveStatsEntry", fieldset).each(function() {
		var $this = $(this);
		var include = selected == 0 || $(".liveStatsLabel.selected", $this).size() > 0;
		
		include = include && !$this.hasClass("rangeAggregation");

		if(include)
			vessels += parseInt($(".liveStatsVesselsReportNum", $this).text(), 10);
	});
	
	if(selected == 0)
		selected = available;
	
	numSelected.text(selected);
	numAvailable.text(available);
	numVessels.text(vessels + " " + $$.I18n.getText("search.stats.vessel" + ( vessels == 1 ? "" : "s" )));
};

function updateAllStatsCounters() {
	updateStatsCounters($("#liveStatsSourcesFieldset"));
	updateStatsCounters($("#liveStatsFlagsFieldset"));
	updateStatsCounters($("#liveStatsTypesFieldset"));
	updateStatsCounters($("#liveStatsLengthsFieldset"));
};

function liveStats(params, callback) {
	disableCounters();
	
	blockStatsPanels(_STATS_SEARCH_DEFAULT_BLOCK_UI_PARAMS);

	var sources = [];

	if($$.isSet(_SPECIFIC_SOURCE)) {
		$("#liveStatsFormContainer").addClass("singleSource");

		sources.push(_SPECIFIC_SOURCE);
	} else {
		$("#liveStatsFormContainer").removeClass("singleSource");

		var systems = $$.Metadata.SYSTEMS;

		for(var s=0; s<systems.length; s++) {
			if(systems[s].vesselSource)
				sources.push(systems[s].id);
		}
	}

	var _success = false;
	
	var URL = _VRMF_BROWSER_SERVICES_VESSELS_LIVE_STATS_VESSELS_URL_PREFIX + "sources/" + sources.join(",");
	
	var selectedCountries = [];
	
	if($$.isSet(params)) {
		var iso3s = params.split(",");
		var iso2s = [];
		var validIso3s = [];
		var wrongIso3s = [];
		
		for(var i=0; i<iso3s.length; i++) {
			country = $$.Metadata.getCountryByIso3Code(iso3s[i]);
			
			if($$.isSet(country) && $$.isSet(country.iso2Code)) {
				selectedCountries.push(country);
				validIso3s.push(iso3s[i]);
			} else
				wrongIso3s.push(iso3s[i]);
		}
		
		if(selectedCountries.length > 0) {
			for(var c=0; c<selectedCountries.length; c++)
				iso2s.push(selectedCountries[c].iso2Code);
			
			URL += "/flag/" + iso2s.join(",");
		} 
		if(wrongIso3s.length > 0) {
			$$.Interface.warn($$.I18n.getText("search.stats.warning.iso3.unknown", wrongIso3s.join(", ")) + " " + 
				( 
					iso2s.length > 0 ? 
						$$.I18n.getText("search.stats.warning.iso3.valid", validIso3s.join(", ")) 
					:
						$$.I18n.getText("search.stats.warning.iso3.all")
				)
			);
		}
	}
	
	$.ajax({
		timeout: VRMF_FINDER_CONFIGURATION.timeouts.retrieveStats,
		cache: true,
		async: true,
		url: _WRAP_URL(URL),
		type: 'get',
		dataType: 'json',
		success: function(response) {
			var data = response.data;

			buildLiveSourcesStats(data.sources);
			buildLiveFlagStats(data.flags, selectedCountries);
			buildLiveTypeStats(data.types);
			buildLiveLengthStats(data.lengths);

			updateFilters();
			
			_success = true;
		},
		complete: function() {
			unblockStatsPanels();

			enableCounters();
			
			if(_success && $$.isSet(callback)) 
				callback();
		}
	});
};

function buildLiveSourcesStats(sources) {
	if($$.isSet(sources)) {
		$("#liveStatsSources .noLiveStats").addClass("hidden");
	} else {
		$("#liveStatsSources .noLiveStats").removeClass("hidden");
	}

	var target = $("#liveStatsSources .liveStatsEntries");
	target.empty();

	sources = sources.sort(function(a, b) {
		return a.groupedVessels >= b.groupedVessels ? -1 : 1;
	});
	
	var totals = 0;

	for(var s=0; s<sources.length; s++) {
		totals += sources[s].groupedVessels;
	}

	var html = "";

	for(var s=0; s<sources.length; s++) {
		html += buildLiveStatsEntry("liveStatsSource", sources[s].sourceSystem, $$.Sources.getSourceIconByID(sources[s].sourceSystem) + sources[s].sourceSystem, sources[s].groupedVessels, totals, $$.Metadata.SYSTEMS_MAP[sources[s].sourceSystem].name);
	}

	var selectedSources = $("#liveStatsSources .selected").size();
	selectedSources = selectedSources == 0 ? sources.length : selectedSources;

	target.append(html);
};

function buildLiveFlagStats(flags, selectedFlags) {
	if($$.isSet(flags)) {
		$("#liveStatsFlags .noLiveStats").addClass("hidden");
	} else {
		$("#liveStatsFlags .noLiveStats").removeClass("hidden");
	}

	var target = $("#liveStatsFlags .liveStatsEntries");
	target.empty();

	flags = flags.sort(function(a, b) {
		return a.vessels > b.vessels ? -1 : a.vessels < b.vessels ? 1 : 0;
	});
	
	var totals = 0;

	for(var f=0; f<flags.length; f++) {
		totals += flags[f].vessels;
	}

	var html = "";

	for(var f=0; f<flags.length; f++) {
		if(flags[f].countryId == null)
			html += buildLiveStatsEntry("liveStatsFlag", "0", $$.I18n.getText("search.stats.entry.label.not.set"), flags[f].vessels, totals);
		else
			html += buildLiveStatsEntry("liveStatsFlag", flags[f].countryId, $$.Flags.getImageByCountryID(flags[f].countryId) + $$.Metadata.getCountryByID(flags[f].countryId).name, flags[f].vessels, totals, $$.Metadata.getCountryByID(flags[f].countryId).name);
	}
	
//	var selectedFlags = $("#liveStatsFlags .selected").size();
//	selectedFlags = selectedFlags == 0 ? flags.length : selectedFlags;

	html = $(html);
	
	if($$.isSet(selectedFlags)) {
		for(var f=0; f<selectedFlags.length; f++) {
			selectLabel(html, selectedFlags[f].id);
		}
	}

	$("img.flag", html).removeAttr("title");

	target.append(html);

	$(".liveStatsLabel", target).addClass("ui-state-default");
};

function buildLiveTypeStats(types) {
	if($$.isSet(types)) {
		$("#liveStatsTypes .noLiveStats").addClass("hidden");
	} else {
		$("#liveStatsTypes .noLiveStats").removeClass("hidden");
	}

	var target = $("#liveStatsTypes .liveStatsEntries");
	target.empty();

	types = types.sort(function(a, b) {
		return a.vessels > b.vessels ? -1 : a.vessels < b.vessels ? 1 : 0;
	});

	var totals = 0;

	for(var f=0; f<types.length; f++) {
		totals += types[f].vessels;
	}

	var html = "";

	var vesselType, label, title;
	for(var f=0; f<types.length; f++) {
		if(types[f].typeId == null)
			html += buildLiveStatsEntry("liveStatsType", "0", $$.I18n.getText("search.stats.entry.label.not.set"), types[f].vessels, totals, null, [ "countryIndependent" ]);
		else {
			title = label = "";

			vesselType = $$.Metadata.VESSEL_TYPES_MAP[types[f].typeId];

			if($$.isSet(vesselType.countryId)) {
				label +=  $$.Flags.getImageByCountryID(vesselType.countryId) + " " + vesselType.originalVesselTypeId + " - ";
				title += $$.Metadata.getCountryByID(vesselType.countryId).iso2Code + " " + vesselType.originalVesselTypeId + " - ";
			}

			title += $$.isSet(vesselType.isscfvCode) ? vesselType.isscfvCode + " - " : "";

			if($$.isSet(vesselType.standardAbbreviation)) {
				title += vesselType.standardAbbreviation + " - ";
				label += vesselType.standardAbbreviation + " - ";
			}

			label += vesselType.name;
			title += vesselType.name + " [ " + vesselType.sourceSystem + " #" + vesselType.originalVesselTypeId + " ]";

			html += buildLiveStatsEntry("liveStatsType", types[f].typeId, label, types[f].vessels, totals, title, $$.isSet(vesselType.countryId) ? null : [ "countryIndependent" ] );
		}
	}
	
	var selectedTypes = $("#liveStatsTypes .selected").size();
	selectedTypes = selectedTypes == 0 ? types.length : selectedTypes;

	target.append(html);

	$(".liveStatsLabel", target).addClass("ui-state-default");
};

function statsLengthSorter(a, b) {
	var rangeFromA = $$.isSet(a.rangeFrom) ? a.rangeFrom : null;
	var rangeFromB = $$.isSet(b.rangeFrom) ? b.rangeFrom : null;

	var rangeToA = $$.isSet(a.rangeTo) && a.rangeTo < _UNLIMITED_RANGE_TO ? a.rangeTo : null;
	var rangeToB = $$.isSet(b.rangeTo) && b.rangeTo < _UNLIMITED_RANGE_TO ? b.rangeTo : null;

	if(rangeFromA == null && rangeToA == null)
		return -1;

	if(rangeFromB == null && rangeToB == null)
		return 1;

	if(rangeToA == null && rangeToB == null)
		return rangeFromA <= rangeFromB ? -1 : 1;

	if(rangeToA == null)
		return 1;

	if(rangeToB == null)
		return -1;

	if(rangeFromA != null) {
		if(rangeFromB != null)
			return rangeFromA <= rangeFromB ? -1 : 1;
		else
			return rangeToB > rangeFromA ? -1 : 1; 
	}

	if(rangeFromB != null)
		return rangeToA <= rangeFromB ? -1 : 1;

	return rangeToA <= rangeToB ? -1 : 1;
};

function statsLengthLabelFormatter(data) {
	var label = 
		$$.isSet(data.rangeFrom) && $$.isSet(data.rangeTo) && data.rangeTo < _UNLIMITED_RANGE_TO ? 
			$$.I18n.getText("search.stats.loa.range", data.rangeFrom, data.rangeTo) : 
				$$.isSet(data.rangeFrom) && data.rangeTo >= _UNLIMITED_RANGE_TO ? $$.I18n.getText("search.stats.loa.range.unlimited.up", data.rangeFrom) : 
					$$.isSet(data.rangeTo) && data.rangeTo < _UNLIMITED_RANGE_TO ? $$.I18n.getText("search.stats.loa.range.unlimited.down", data.rangeTo) : 
						$$.I18n.getText("search.stats.entry.not.set");

	return label;
};

function isARangeAggregation(range) {
	return range.rangeFrom == null && range.rangeTo != null || 
		   range.rangeTo == null  && range.rangeFrom != null || 
		   range.rangeTo >= _UNLIMITED_RANGE_TO;
};

function buildLiveLengthStats(lengths) {
	if($$.isSet(lengths)) {
		$("#liveStatsLengths .noLiveStats").addClass("hidden");
	} else {
		$("#liveStatsLengths .noLiveStats").removeClass("hidden");
	}

	var target = $("#liveStatsLengths .liveStatsEntries");
	target.empty();

	var totals = 0;

	lengths = lengths.sort(_RANGE_SORTER);

	for(var f=0; f<lengths.length; f++) {
		if(!isARangeAggregation(lengths[f]))
			totals += lengths[f].vessels;
	}

	lengths = lengths.sort(statsLengthSorter);

	var html = "";

	var label, title;
	label = title = null;

	var additionalClasses = [];

	for(var f=0; f<lengths.length; f++) {
		if(isARangeAggregation(lengths[f]))
			additionalClasses = [ "rangeAggregation" ];
		else
			additionalClasses = [];

		if(lengths[f].rangeFrom == null && lengths[f].rangeTo == null)
			html += buildLiveStatsEntry("liveStatsLength", "_", $$.I18n.getText("search.stats.entry.label.not.set"), lengths[f].vessels, totals, null, additionalClasses);
		else {
			label = statsLengthLabelFormatter(lengths[f]);
			title = label + ( isARangeAggregation(lengths[f]) ? " " + $$.I18n.getText("search.stats.range.aggregated") : "");

			html += buildLiveStatsEntry("liveStatsLength", lengths[f].rangeFrom + "_" + lengths[f].rangeTo, 
										 statsLengthLabelFormatter(lengths[f]), lengths[f].vessels, totals, title, additionalClasses);
		}
	}
	
	var selectedLengths = $("#liveStatsLengths .selected").size();
	selectedLengths = selectedLengths == 0 ? lengths.length : selectedLengths;

	target.append(html);

	$(".liveStatsLabel", target).addClass("ui-state-default");
};

function buildLiveStatsEntry(name, value, label, vessels, totals, title, additionalClasses) {
	var entryID = name + "_" + value;
	var percentage = vessels / totals * 100.0;
	percentage = "" + percentage.toFixed(1);

	while(percentage.length < 4)
		percentage = " " + percentage;

	percentage = percentage.replace(/\s/g, '&nbsp;&nbsp;');

	return "<div class='ui-helper-clearfix liveStatsEntry" + ( $$.isSet(additionalClasses) ? " " + additionalClasses.join(" ") : "" ) + "'>" +
				"<div class='liveStatsLabel left ui-state-default ui-corner-all'>" +
					"<div class='liveStatsEntryStatus left ui-helper-clearfix'>" +
						"<span class='ui-icon ui-icon-check'>&nbsp;</span>" +
						"<span class='unchecked'>&nbsp;</span>" +
					"</div>" +
					"<input id='" + entryID + "' type='checkbox' name='" + name + "' value='" + value + "' class='hidden'></input>" +
					"<label for='" + entryID + "' title=\"" + ( $$.isSet(title) ? title : label ) + "\">" + label + "</label>" +
				"</div>" +
				"<div class='liveStatsVesselsReport right rightPad ui-helper-clearfix'>" + 
					"<span class='liveStatsVesselsReportNum'>" + vessels + "</span>" + 
					" [ " + percentage + "% ]" + 
				"</div>" +
			"</div>";
};

function getSelectedSources() {
	return getSelectedItem($("#liveStatsSources"));
};

function getSelectedFlags() {
	return getSelectedItem($("#liveStatsFlags"));
};

function getSelectedTypes() {
	return getSelectedItem($("#liveStatsTypes"));
};

function getSelectedLengths() {
	return getSelectedItem($("#liveStatsLengths"));
};

function getSelectedItem(container) {
	var values = [];

	$(".liveStatsLabel.selected input[type='checkbox']:checked", container).each(function() {
		values.push($(this).val());
	});

	return values;
};

function selectLabel(container, value) {
	$(".liveStatsLabel", container).each(function() {
		var $this = $(this);

		var input = $("input[type='checkbox']", $this);

		if(input.val() == value) {
			$$.Utils.check(input);
			$this.addClass("selected");
		}
	});
};

function deselectAllLabels(container) {
	$(".liveStatsLabel", container).each(function() {
		var $this = $(this);

		var input = $("input[type='checkbox']", $this);

		$$.Utils.uncheck(input);
		$this.removeClass("ui-state-active");
	});
};

function updateLiveStats(originator, selectedSources, selectedFlags, selectedTypes, selectedLengths) {
	disableCounters();
	
	blockStatsPanels(_STATS_UPDATE_DEFAULT_BLOCK_UI_PARAMS);

	var effectiveSources = ($$.isSet(selectedSources) ? selectedSources : getSources()).join(",");

	var flagsISO2Codes = [];
	if($$.isSet(selectedFlags))
		for(var f=0; f<selectedFlags.length; f++)
			flagsISO2Codes.push(getCountryISO2ForID(selectedFlags[f]));

	$.ajax({
		timeout: VRMF_FINDER_CONFIGURATION.timeouts.retrieveStats,
		cache: true,
		async: true,
		url: _WRAP_URL(_VRMF_BROWSER_SERVICES_VESSELS_LIVE_STATS_VESSELS_URL_PREFIX + "sources/" + effectiveSources + 
				( $$.isSet(flagsISO2Codes) ? 
						"/flag/" + flagsISO2Codes.join(",") : "" ) + 
						( $$.isSet(selectedTypes) ? 
			 				( "/type/" + selectedTypes.join(",") ) : "" )),
		type: 'get',
		dataType: 'json',
		success: function(response) {
			var data = response.data;

//			buildLiveSourcesStats(data.sources);
			buildLiveFlagStats(data.flags);
			buildLiveTypeStats(data.types);
			buildLiveLengthStats(data.lengths);

			for(var s=0; s<selectedSources.length; s++) {
				selectLabel($("#liveStatsSources"), selectedSources[s]);
			}

			for(var f=0; f<selectedFlags.length; f++) {
				selectLabel($("#liveStatsFlags"), selectedFlags[f]);
			}

			for(var t=0; t<selectedTypes.length; t++) {
				selectLabel($("#liveStatsTypes"), selectedTypes[t]);
			}

			for(var l=0; l<selectedLengths.length; l++) {
				selectLabel($("#liveStatsLengths"), selectedLengths[l]);
			}
						
			liveStatsSearch();
		},
		complete: function() {
			enableCounters();

			unblockStatsPanels();

			$(originator).parents("fieldset").nextAll("fieldset").each(function() {
				$(".liveStats", $(this)).scrollTo("0%", 0);
			});
			
			if($$.UI.nicescrollEnabled)
				_NICESCROLL_UPDATER();
		}
	});
};

function liveStatsSearch(callback) {
	updateFilters();

	var sources = getSelectedSources();
	var flags = getSelectedFlags();
	var types = getSelectedTypes(); 
	var lengths = getSelectedLengths();

	var includeNoFlags = false;
	var includeNoTypes = false;
	var includeNoLengths = false;

	for(var f=0; f<flags.length && !includeNoFlags; f++) {
		includeNoFlags = flags[f] == 0;
	}

	for(var t=0; t<types.length && !includeNoTypes; t++) {
		includeNoTypes = types[t] == 0;
	}

	for(var l=0; l<lengths.length && !includeNoLengths; l++) {
		includeNoLengths = lengths[l] === "_";
	}

	flags = flags.sort();
	types = types.sort();
	lengths = lengths.sort();

	var params = {
		c: true,
		gd: true,
		nof: includeNoFlags,
		not: includeNoTypes,
		nol: includeNoLengths
	};

	if(sources.length > 0)
		params.s = sources;

	if(flags.length > 0)
		params.f = flags;

	if(types.length > 0)
		params.t = types.join(",");

	if(lengths.length > 0  || includeNoLengths) {
		params.lt = [ 'LOA' ];

		if(lengths.length > 0)
			params.ls = lengths; 
	}

	params = $.extend({}, params, getDisplayOptionsParameters());
	params.o = 0;

	doSearch(params, callback, { timeout: VRMF_FINDER_CONFIGURATION.timeouts.searchStats });
};

function displayLiveStatsSources() {
	var dialog = $("<div class='removeUI'><span class='waitPlease'>" + $$.I18n.getText("search.stats.loading") + "<span class='spinner'>&nbsp;</span></span></div>");

	dialog.dialog({
		title: $$.I18n.getText("search.stats.sources.title"),
		width: 700,
		height: 720,
		dialogClass: 'statsPopup',
		modal: true,
		resizable: false
	});

	var sources = [];

	$("#liveStatsSources [name='liveStatsSource']").each(function() {
		sources.push($(this).val());
	});

	sources = sources.join(",");

	$.ajax({
		timeout: VRMF_FINDER_CONFIGURATION.timeouts.retrieveStats,
		cache: true,
		async: true,
		url: _WRAP_URL(_VRMF_BROWSER_SERVICES_VESSELS_LIVE_STATS_VESSELS_URL_PREFIX + "sources/" + sources),
		type: 'get',
		dataType: 'json',
		success: function(response) {
			var data = response.data.sources;
			var rawDataArray = [[ $$.I18n.getText("search.stats.sources.label"), 
			                      $$.I18n.getText("search.stats.vessels.number.grouped") ]];

			for(var d=0; d<data.length; d++)
				rawDataArray.push([ data[d].sourceSystem + " [ " + data[d].groupedVessels + " " + $$.I18n.getText("search.stats.vessels") + " ]", data[d].groupedVessels ]);

			rawDataArray = rawDataArray.sort(function(a, b) {
				return a[1] - b[1];
			});
			
			var options = { 
				'title': $$.I18n.getText("search.stats.sources.vessels.number"),
				'width': 675,
				'height': 630,
				'fontSize': 10
			};

			var chart = new google.visualization.PieChart(dialog.get(0));
			chart.draw(google.visualization.arrayToDataTable(rawDataArray), options);
		}
	});
};

function displayOtherLiveStatsSources() {
	var dialog = $("<div class='removeUI'><span class='waitPlease'>" + $$.I18n.getText("search.stats.loading") + "<span class='spinner'>&nbsp;</span></span></div>");

	dialog.dialog({
		title: $$.I18n.getText("search.stats.sources.title"),
		width: 700,
		height: 720,
		dialogClass: 'statsPopup',
		modal: true,
		resizable: false
	});

	var sources = [];

	$("#liveStatsSources [name='liveStatsSource']").each(function() {
		sources.push($(this).val());
	});

	sources = sources.join(",");

	$.ajax({
		timeout: VRMF_FINDER_CONFIGURATION.timeouts.retrieveStats,
		cache: true,
		async: true,
		url: _WRAP_URL(_VRMF_BROWSER_SERVICES_VESSELS_LIVE_STATS_VESSELS_URL_PREFIX + "sources/" + sources),
		type: 'get',
		dataType: 'json',
		success: function(response) {
			var data = response.data.sources;
			var rawDataArray = [[ $$.I18n.getText("search.stats.sources.label"), 
			                      $$.I18n.getText("search.stats.vessels.number.grouped") ]];

			for(var d=0; d<data.length; d++)
				rawDataArray.push([ data[d].sourceSystem, data[d].vessels ]);

			var options = { 
				title: $$.I18n.getText("search.stats.sources.vessels.number"),
				width: 675,
				height: 630,
				fontSize: 10
			};

			var chart = new google.visualization.BarChart(dialog.get(0));
			chart.draw(google.visualization.arrayToDataTable(rawDataArray), options);
		}
	});
};

function displayOtherLiveStatsTabularSources() {
	var dialog = $("<div class='removeUI'><span class='waitPlease'>" + $$.I18n.getText("search.stats.loading") + "<span class='spinner'>&nbsp;</span></span></div>");

	dialog.dialog({
		title: $$.I18n.getText("search.stats.sources.vessels.number"),
		width: 700,
		height: 720,
		dialogClass: 'statsPopup',
		modal: true,
		resizable: false
	});

	var sources = [];

	$("#liveStatsSources [name='liveStatsSource']").each(function() {
		sources.push($(this).val());
	});

	sources = sources.join(",");

	$.ajax({
		timeout: VRMF_FINDER_CONFIGURATION.timeouts.retrieveStats,
		cache: true,
		async: true,
		url: _WRAP_URL(_VRMF_BROWSER_SERVICES_VESSELS_LIVE_STATS_VESSELS_URL_PREFIX + "sources/" + sources),
		type: 'get',
		dataType: 'json',
		success: function(response) {
			var data = response.data.sources;
			var rawDataArray = [[ $$.I18n.getText("search.stats.sources.id.label"), 
			                      $$.I18n.getText("search.stats.sources.label"), 
			                      $$.I18n.getText("search.stats.vessels.number.grouped") ]];

			for(var d=0; d<data.length; d++)
				rawDataArray.push([ 
					data[d].sourceSystem, 
					$$.Metadata.SYSTEMS_MAP[data[d].sourceSystem].name,
					data[d].vessels
				]);

			var options = { 
				width: 675,
				height: 630,
				showRowNumber: true
			};

			var chart = new google.visualization.Table(dialog.get(0));
			chart.draw(google.visualization.arrayToDataTable(rawDataArray), options);
		}
	});
};

function displayLiveStatsFlags() {
	var dialog = $("<div class='removeUI'><span class='waitPlease'>" + $$.I18n.getText("search.stats.loading") + "<span class='spinner'>&nbsp;</span></span></div>");

	dialog.dialog({
		title: $$.I18n.getText("search.stats.flag.title"),
		width: 700,
		height: 720,
		dialogClass: 'statsPopup',
		modal: true,
		resizable: false
	});

	var sources = getSources();
	var allSources = sources.length == getAllSources().length;

	var serializedSources = sources.join(",");

	$.ajax({
		timeout: VRMF_FINDER_CONFIGURATION.timeouts.retrieveStats,
		cache: true,
		async: true,
		url: _WRAP_URL(_VRMF_BROWSER_SERVICES_VESSELS_LIVE_STATS_VESSELS_URL_PREFIX + "flags/sources/" + serializedSources),
		type: 'get',
		dataType: 'json',
		success: function(response) {
			var data = response.data;
			var rawDataArray = [[ $$.I18n.getText("search.stats.flag"), 
			                      $$.I18n.getText("search.stats.vessels.number") ]];

			for(var d=0; d<data.length; d++)
				if(data[d].countryId != null)
					rawDataArray.push([ $$.Metadata.getCountryByID(data[d].countryId).name + " [ " + data[d].vessels + " " + $$.I18n.getText("search.stats.vessels") + " ]", data[d].vessels ]);
				else
					rawDataArray.push([ $$.I18n.getText("search.stats.chart.label.not.set"), data[d].vessels ]);

			rawDataArray = rawDataArray.sort(function(a, b) {
				return a[1] - b[1];
			});
			
			var hTitle = ( !allSources && $$.isSet(sources) ? " [ " + $$.I18n.getText("search.stats.sources.title.detail", sources.join(", ")) + " ]" : "" );

			var options = { 
				'title': $$.I18n.getText("search.stats.flag.vessels.number") + ( $$.isSet(hTitle) ? hTitle : "" ),
				'width': 675,
				'height': 630,
				'fontSize': 10
			};

			var chart = new google.visualization.PieChart(dialog.get(0));
			chart.draw(google.visualization.arrayToDataTable(rawDataArray), options);
		}
	});
};

function displayLiveStatsGeoFlags() {
	var dialog = $("<div>" +
				   	"<span class='waitPlease'>" + $$.I18n.getText("search.stats.loading") + "<span class='spinner'>&nbsp;</span></span>" +
				   	"<div class='geoChart removeUI'/>" +
				   	"<div class='geoChartControls hidden ui-state-highlight'>" +
				   		"<label>" + $$.I18n.getText("vrmf.geo.zoom.on") + "&nbsp;</label>" +
				   		"<select class='geoChartControlsCountrySelector'>" +
				   			"<option value='world'>" + $$.I18n.getText("vrmf.geo.world") + "</option>" +
				   		"</select>" +
				   	"</div>" +
				  "</div>");

	var sources = getSources();
	var allSources = sources.length == getAllSources().length;

	var serializedSources = sources.join(",");

	var hTitle = ( !allSources && $$.isSet(sources) ? " [ " + $$.I18n.getText("search.stats.sources.title.detail", sources.join(", ")) + " ]" : "" );
	
	dialog.dialog({
		title: $$.I18n.getText("search.stats.flag.geo.title") + hTitle,
		width: 700,
		height: 720,
		dialogClass: 'statsPopup',
		modal: true,
		resizable: false
	});
	
	$.ajax({
		timeout: VRMF_FINDER_CONFIGURATION.timeouts.retrieveStats,
		cache: true,
		async: true,
		url: _WRAP_URL(_VRMF_BROWSER_SERVICES_VESSELS_LIVE_STATS_VESSELS_URL_PREFIX + "flags/sources/" + serializedSources),
		type: 'get',
		dataType: 'json',
		success: function(response) {
			var data = response.data;
			var rawDataArray = [[ $$.I18n.getText("search.stats.flag.iso2.label"),  
			                      $$.I18n.getText("search.stats.vessels.number") ]];

			for(var d=0; d<data.length; d++)
				if(data[d].countryId != null) {
					var country = $$.Metadata.getCountryByID(data[d].countryId);
					
					if(country.iso2Code != null)
						rawDataArray.push([ country.iso2Code, data[d].vessels ]);
				}
			
			var options = { 
				'width': 655,
				'height': 600,
				'fontSize': 10,
				'colorAxis': {
					minValue: 0,  
					colors: ['#660000', '#0000FF']
				},
				'datalessRegionColor': 'F5F5F5'
			};

			var dataTable = google.visualization.arrayToDataTable(rawDataArray);
			
			var chart = new google.visualization.GeoChart($(".geoChart", dialog).get(0));
			chart.draw(dataTable, options);
			
			$(".waitPlease", dialog).remove();
			$(".geoChart", dialog).css("border", "1px dotted");

			var selector = $(".geoChartControlsCountrySelector", dialog);
			
			selector.data("chart", chart);
			selector.data("chartOptions", options);
			selector.data("dataTable", dataTable);

			for(var c in _STATS_SEARCH_GEO_CHARTS_CONTINENTS_DATA) {
				var continent = _STATS_SEARCH_GEO_CHARTS_CONTINENTS_DATA[c];
				var group = $("<optgroup label='" + $$.I18n.getText(c) + "'/>");
				
				group.append($("<option value='" + continent.code + "'>" + $$.I18n.getText(c) + " " + $$.I18n.getText("vrmf.geo.overall") + "</option>"));
				
				for(var s in continent.subContinents) {
					var subContinent = continent.subContinents[s];
					group.append($("<option value='" + subContinent.code + "'>" + $$.I18n.getText(s) + "</option>"));
				}
				
				selector.append(group);
			}
						
			selector.change(function() {
				var $this = $(this);
				
				var _chart = $this.data("chart");
				var _options = $this.data("chartOptions");
				var _data = $this.data("dataTable");
				
				var value = $this.val();
				
				_chart.draw(_data, $.extend(true, {}, _options, { region: value }));
			});
			
			$(".geoChartControls").removeClass("hidden");
		}
	});
};

function displayLiveStatsTabularFlags() {
	var dialog = $("<div class='removeUI'>" +
				   	"<span class='waitPlease'>" + $$.I18n.getText("search.stats.loading") + "<span class='spinner'>&nbsp;</span></span>" +
				  "</div>");

	var sources = getSources();
	var allSources = sources.length == getAllSources().length;

	var serializedSources = sources.join(",");

	var hTitle = ( !allSources && $$.isSet(sources) ? " [ " + $$.I18n.getText("search.stats.sources.title.detail", sources.join(", ")) + " ]" : "" );
	
	dialog.dialog({
		title: $$.I18n.getText("search.stats.flag.title") + hTitle,
		width: 700,
		height: 720,
		dialogClass: 'statsPopup',
		modal: true,
		resizable: false
	});
	
	$.ajax({
		timeout: VRMF_FINDER_CONFIGURATION.timeouts.retrieveStats,
		cache: true,
		async: true,
		url: _WRAP_URL(_VRMF_BROWSER_SERVICES_VESSELS_LIVE_STATS_VESSELS_URL_PREFIX + "flags/sources/" + serializedSources),
		type: 'get',
		dataType: 'json',
		success: function(response) {
			var data = response.data;
			var rawDataArray = [[ $$.I18n.getText("search.stats.flag.label"), 
			                      $$.I18n.getText("search.stats.flag.iso2.label"), 
			                      $$.I18n.getText("search.stats.vessels.number") ]];

			for(var d=0; d<data.length; d++)
				if(data[d].countryId != null) {
					var country = $$.Metadata.getCountryByID(data[d].countryId);
					
					rawDataArray.push([ 
						country.name, 
						$$.isSet(country.iso2Code) ? country.iso2Code : null, 
						data[d].vessels 
					]);
				}
			
			var options = { 
				'width': 655,
				'height': 600,
				showRowNumber: true
			};

			var dataTable = google.visualization.arrayToDataTable(rawDataArray);
			
			var chart = new google.visualization.Table(dialog.get(0));
			chart.draw(dataTable, options);
		}
	});
};

function displayLiveStatsTypes() {
	var dialog = $("<div class='removeUI'><span class='waitPlease'>" + $$.I18n.getText("search.stats.loading") + "<span class='spinner'>&nbsp;</span></span></div>");

	dialog.dialog({
		title: $$.I18n.getText("search.stats.type.title"),
		width: 700,
		height: 720,
		dialogClass: 'statsPopup',
		modal: true,
		resizable: false
	});

	var flags = getSelectedFlags();
	var flagsISO2Codes = [];
	var countries = [];

	var value = null;

	if($$.isSet(flags))
		for(var f=0; f<flags.length; f++) {
			value = flags[f];

			var id = parseInt(value, 10);
			var country = $$.Metadata.getCountryByID(id);

			if(value == "0")
				countries.push($$.I18n.getText("search.stats.entry.not.set"));
			else {
				countries.push(country.name);

				if($$.isSet(country.iso2Code))
					flagsISO2Codes.push(country.iso2Code);
				else
					flagsISO2Codes.push(country.id);
			}
		};

	var sources = getSources();
	var allSources = sources.length == getAllSources().length;

	var serializedSources = sources.join(",");

	$.ajax({
		timeout: VRMF_FINDER_CONFIGURATION.timeouts.retrieveStats,
		cache: true,
		async: true,
		url: _WRAP_URL(_VRMF_BROWSER_SERVICES_VESSELS_LIVE_STATS_VESSELS_URL_PREFIX + "sources/" + serializedSources + 
				( $$.isSet(flagsISO2Codes) ? 
						"/flag/" + flagsISO2Codes.join(",") : "" )), 
		type: 'get',
		dataType: 'json',
		success: function(response) {
			var data = response.data.types;
			var rawDataArray = [[ $$.I18n.getText("search.stats.type"), 
			                      $$.I18n.getText("search.stats.vessels.number") ]];

			var vesselType = null;

			for(var d=0; d<data.length; d++) {
				vesselType = $$.isSet(data[d].typeId) ? $$.Metadata.VESSEL_TYPES_MAP[data[d].typeId] : null;

				rawDataArray.push([ 
				                   ( $$.isSet(vesselType) ? (  
				                		$$.isSet(vesselType.countryId) ?
				                			$$.Metadata.getCountryByID(vesselType.countryId).iso2Code + " - " 
				                		:
				                			""
				                		) + vesselType.name
				                	: 
				                		$$.I18n.getText("search.stats.entry.not.set") ) + " [ " + data[d].vessels + " " + $$.I18n.getText("search.stats.vessels") + " ]", 
				                   data[d].vessels 
				                  ]);
			}
			
			rawDataArray = rawDataArray.sort(function(a, b) {
				return a[1] - b[1];
			});

			var hTitle = ( !allSources && $$.isSet(sources) ? " [ " + $$.I18n.getText("search.stats.sources.title.detail", sources.join(", ")) + " ]" : "" ) +
			 		 	 ( $$.isSet(countries) ? " [ " + $$.I18n.getText("search.stats.flag.title.detail", countries.join(", ")) + " ]" : "" );

			var options = { 
				'title': $$.I18n.getText("search.stats.type.regional.title") + ( $$.isSet(hTitle) ? hTitle : "" ),
				'width': 675,
				'height': 630,
				'fontSize': 10
			};

			var chart = new google.visualization.PieChart(dialog.get(0));
			chart.draw(google.visualization.arrayToDataTable(rawDataArray), options);
		}
	});
};

function displayLiveStatsTabularTypes() {
	var dialog = $("<div class='removeUI'><span class='waitPlease'>" + $$.I18n.getText("search.stats.loading") + "<span class='spinner'>&nbsp;</span></span></div>");

	var flags = getSelectedFlags();
	var flagsISO2Codes = [];
	var countries = [];

	var value = null;

	if($$.isSet(flags))
		for(var f=0; f<flags.length; f++) {
			value = flags[f];

			var id = parseInt(value, 10);
			var country = $$.Metadata.getCountryByID(id);

			if(value == "0")
				countries.push($$.I18n.getText("search.stats.entry.not.set"));
			else {
				countries.push(country.name);

				if($$.isSet(country.iso2Code))
					flagsISO2Codes.push(country.iso2Code);
				else
					flagsISO2Codes.push(country.id);
			}
		};

	var sources = getSources();
	var allSources = sources.length == getAllSources().length;

	var hTitle = ( !allSources && $$.isSet(sources) ? " [ " + $$.I18n.getText("search.stats.sources.title.detail", sources.join(", ")) + " ]" : "" ) +
				 ( $$.isSet(countries) ? " [ " + $$.I18n.getText("search.stats.flag.title.detail", countries.join(", ")) + " ]" : "" );
	
	dialog.dialog({
		title: $$.I18n.getText("search.stats.type.title") + ( $$.isSet(hTitle) ? " " + hTitle : "" ),
		width: 1200,
		height: 720,
		dialogClass: 'statsPopup',
		modal: true,
		resizable: false
	});
	
	var serializedSources = sources.join(",");

	$.ajax({
		timeout: VRMF_FINDER_CONFIGURATION.timeouts.retrieveStats,
		cache: true,
		async: true,
		url: _WRAP_URL(_VRMF_BROWSER_SERVICES_VESSELS_LIVE_STATS_VESSELS_URL_PREFIX + "sources/" + serializedSources + 
				( $$.isSet(flagsISO2Codes) ? 
						"/flag/" + flagsISO2Codes.join(",") : "" )), 
		type: 'get',
		dataType: 'json',
		success: function(response) {
			var data = response.data.types;
			var rawDataArray = [[ $$.I18n.getText("search.stats.sources.label"), 
			                      $$.I18n.getText("search.stats.type.code.original"),
			                      $$.I18n.getText("search.stats.type.country.dependent"),
			                      $$.I18n.getText("search.stats.type.country"), 
			                      $$.I18n.getText("search.stats.type.abbreviation"),
			                      $$.I18n.getText("search.stats.type.code.isscfv"),
			                      $$.I18n.getText("search.stats.type"),
			                      $$.I18n.getText("search.stats.vessels.number") ]];

			var vesselType = null;

			for(var d=0; d<data.length; d++) {
				vesselType = $$.isSet(data[d].typeId) ? $$.Metadata.VESSEL_TYPES_MAP[data[d].typeId] : null;

				if($$.isSet(vesselType)) {
					rawDataArray.push([
					                   $$.isSet(vesselType.sourceSystem) ? vesselType.sourceSystem : "---",
					                   $$.isSet(vesselType.originalVesselTypeId) ? vesselType.originalVesselTypeId : "---",
					                   $$.isSet(vesselType.countryId),
					                   $$.isSet(vesselType.countryId) ? $$.Metadata.getCountryByID(vesselType.countryId).iso2Code : "---",
					                   $$.isSet(vesselType.standardAbbreviation) ? vesselType.standardAbbreviation : "---",
					                   $$.isSet(vesselType.isscfvCode) ? vesselType.isscfvCode : "---",
					                   $$.isSet(vesselType.name) ? vesselType.name : "---",
					                   data[d].vessels
					                  ]);
				} else {
					rawDataArray.push([
					                   "---",
					                   "---",
					                   false,
					                   "---",
					                   "---",
					                   "---",
					                   $$.I18n.getText("search.stats.entry.not.set"),
					                   data[d].vessels
					                  ]);
				}
			}

			var options = {
				showRowNumber: true
			};

			var chart = new google.visualization.Table(dialog.get(0));
			chart.draw(google.visualization.arrayToDataTable(rawDataArray), options);
		}
	});
};

function displayLiveStatsLengths() {
	var dialog = $("<div class='removeUI'><span class='waitPlease'>" + $$.I18n.getText("search.stats.loading") + "<span class='spinner'>&nbsp;</span></span></div>");

	dialog.dialog({
		title: $$.I18n.getText("search.stats.loa.title"),
		width: 700,
		height: 720,
		dialogClass: 'statsPopup',
		modal: true,
		resizable: false
	});

	var sources = getSources();
	var allSources = sources.length == getAllSources().length;

	var serializedSources = sources.join(",");

	var flags = getSelectedFlags();
	var flagsISO2Codes = [];
	var countries = [];

	var value = null;

	if($$.isSet(flags))
		for(var f=0; f<flags.length; f++) {
			value = flags[f];

			var id = parseInt(value, 10);
			var country = $$.Metadata.getCountryByID(id);

			if(value == "0")
				countries.push($$.I18n.getText("search.stats.entry.not.set"));
			else {
				countries.push(country.name);

				if($$.isSet(country.iso2Code))
					flagsISO2Codes.push(country.iso2Code);
				else
					flagsISO2Codes.push(country.id);
			}
		};

	var types = getSelectedTypes();
	var vesselTypes = [];

	$("#liveStatsTypes [name='liveStatsType']:checked").each(function() {
		value = $(this).val();

		types.push(value);

		if(value == "0")
			vesselTypes.push($$.I18n.getText("search.stats.entry.not.set"));
		else
			vesselTypes.push($$.Metadata.VESSEL_TYPES_MAP[parseInt(value, 10)].name);
	});

	$.ajax({
		timeout: VRMF_FINDER_CONFIGURATION.timeouts.retrieveStats,
		cache: true,
		async: true,
		url: _WRAP_URL(_VRMF_BROWSER_SERVICES_VESSELS_LIVE_STATS_VESSELS_URL_PREFIX + "sources/" + serializedSources + 
				( $$.isSet(flagsISO2Codes) ? 
						"/flag/" + flagsISO2Codes.join(",") : "" ) +  
						( $$.isSet(types) ? 
				 				( "/type/" + types.join(",") ) : "" )),
		type: 'get',
		dataType: 'json',
		success: function(response) {
			var data = response.data.lengths;
			var rawDataArray = [[ $$.I18n.getText("search.stats.loa.range.class"), 
			                      $$.I18n.getText("search.stats.vessels.number"), 
			                      $$.I18n.getText("search.stats.loa.aggregated") ]];

			data = data.sort(statsLengthSorter);

			var isRange = false;
			var total = 0;
			var rollingTotal = 0;
			
			for(var d=0; d<data.length; d++) {
				isRange = data[d].rangeFrom != null && data[d].rangeTo != null && data[d].rangeTo < _UNLIMITED_RANGE_TO;
				
				if(isRange)
					total += data[d].vessels;
			}
			
			for(var d=0; d<data.length; d++) {
				isRange = data[d].rangeFrom != null && data[d].rangeTo != null && data[d].rangeTo < _UNLIMITED_RANGE_TO;
				
				if(isRange)
					rollingTotal += data[d].vessels;
				
				rawDataArray.push([ 
				                   statsLengthLabelFormatter(data[d]), 
				                   data[d].vessels,
				                   isRange && total > 0 ? Math.round(rollingTotal * 100.0 * 100.0 / total ) / 100.0 : null
				                  ]);
			}

			var hTitle = null;

			if($$.isSet(countries) || $$.isSet(vesselTypes)) {
				hTitle = ( !allSources && $$.isSet(sources) ? " [ " + $$.I18n.getText("search.stats.sources.title.detail", sources.join(", ")) + " ]" : "" ) +
						 ( $$.isSet(countries) ? " [ " + $$.I18n.getText("search.stats.flag.title.detail", countries.join(", ")) + " ]" : "" ) +
						 ( $$.isSet(vesselTypes) ? " [ " + $$.I18n.getText("search.stats.type.title.detail", vesselTypes.join(", ")) + " ]" : "" );
			}

			var options = { 
				'title': $$.I18n.getText("search.stats.loa.regional.title") + ($$.isSet(hTitle) ? " " + hTitle : "" ),
				'width': 675,
				'height': 630,
				'fontSize': 10,
				seriesType: "bars",
				series: {
					0: {
						targetAxisIndex: 0
					},
					1: {
						type: "line",
						targetAxisIndex: 1
					}
				},
				vAxes: {
					0: {
						minValue: 0
					},
					1: {
						maxValue: 100,
						minValue: 0
					}
				}
			};

			var chart = new google.visualization.ComboChart(dialog.get(0));
			chart.draw(google.visualization.arrayToDataTable(rawDataArray), options);
		}
	});
};

function displayLiveStatsTabularLengths() {
	var dialog = $("<div class='removeUI'><span class='waitPlease'>" + $$.I18n.getText("search.stats.loading") + "<span class='spinner'>&nbsp;</span></span></div>");

	var sources = getSources();
	var allSources = sources.length == getAllSources().length;

	var serializedSources = sources.join(",");

	var flags = getSelectedFlags();
	var flagsISO2Codes = [];
	var countries = [];

	var value = null;

	if($$.isSet(flags))
		for(var f=0; f<flags.length; f++) {
			value = flags[f];

			var id = parseInt(value, 10);
			var country = $$.Metadata.getCountryByID(id);

			if(value == "0")
				countries.push($$.I18n.getText("search.stats.entry.not.set"));
			else {
				countries.push(country.name);

				if($$.isSet(country.iso2Code))
					flagsISO2Codes.push(country.iso2Code);
				else
					flagsISO2Codes.push(country.id);
			}
		};

	var types = getSelectedTypes();
	var vesselTypes = [];

	$("#liveStatsTypes [name='liveStatsType']:checked").each(function() {
		value = $(this).val();

		types.push(value);

		if(value == "0")
			vesselTypes.push($$.I18n.getText("search.stats.entry.not.set"));
		else
			vesselTypes.push($$.Metadata.VESSEL_TYPES_MAP[parseInt(value, 10)].name);
	});

	var hTitle = null;

	if($$.isSet(countries) || $$.isSet(vesselTypes)) {
		hTitle = ( !allSources && $$.isSet(sources) ? " [ " + $$.I18n.getText("search.stats.sources.title.detail", sources.join(", ")) + " ]" : "" ) +
				 ( $$.isSet(countries) ? " [ " + $$.I18n.getText("search.stats.flag.title.detail", countries.join(", ")) + " ]" : "" ) +
				 ( $$.isSet(vesselTypes) ? " [ " + $$.I18n.getText("search.stats.type.title.detail", vesselTypes.join(", ")) + " ]" : "" );
	}
	
	dialog.dialog({
		title: $$.I18n.getText("search.stats.loa.title") + ( $$.isSet(hTitle) ? " " + hTitle : "" ),
		width: 700,
		height: 400,
		modal: true,
		resizable: false
	});
	
	$.ajax({
		timeout: VRMF_FINDER_CONFIGURATION.timeouts.retrieveStats,
		cache: true,
		async: true,
		url: _WRAP_URL(_VRMF_BROWSER_SERVICES_VESSELS_LIVE_STATS_VESSELS_URL_PREFIX + "sources/" + serializedSources + 
				( $$.isSet(flagsISO2Codes) ? 
						"/flag/" + flagsISO2Codes.join(",") : "" ) +  
						( $$.isSet(types) ? 
				 				( "/type/" + types.join(",") ) : "" )),
		type: 'get',
		dataType: 'json',
		success: function(response) {
			var data = response.data.lengths;
			var rawDataArray = [[ $$.I18n.getText("search.stats.loa.range.type"), 
			                      $$.I18n.getText("search.stats.loa.range.class"), 
			                      $$.I18n.getText("search.stats.loa.range.from"), 
			                      $$.I18n.getText("search.stats.loa.range.to"), 
			                      $$.I18n.getText("search.stats.vessels.number") ]];

			data = data.sort(statsLengthSorter);

			var isRange = false;
			var total = 0;
			var rollingTotal = 0;
			
			for(var d=0; d<data.length; d++) {
				isRange = data[d].rangeFrom != null && data[d].rangeTo != null && data[d].rangeTo < _UNLIMITED_RANGE_TO;
				
				if(isRange)
					total += data[d].vessels;
			}
			
			for(var d=0; d<data.length; d++) {
				isRange = data[d].rangeFrom != null && data[d].rangeTo != null && data[d].rangeTo < _UNLIMITED_RANGE_TO;
				
				if(isRange)
					rollingTotal += data[d].vessels;
				
				rawDataArray.push([ 
				                   $$.I18n.getText("search.stats.loa"),
				                   statsLengthLabelFormatter(data[d]),
				                   data[d].rangeFrom != null ? data[d].rangeFrom : null,
				                   data[d].rangeTo != null && data[d].rangeTo < _UNLIMITED_RANGE_TO ? data[d].rangeTo : null,
				                   data[d].vessels
				                  ]);
			}

			var options = { 
				showRowNumber: true
			};

			var chart = new google.visualization.Table(dialog.get(0));
			chart.draw(google.visualization.arrayToDataTable(rawDataArray), options);
		}
	});
};

function getSources() {
	if($$.isSet(_SPECIFIC_SOURCE))
		return [ _SPECIFIC_SOURCE ];

	var sources = getSelectedSources();

	if(sources.length == 0) {
		return getAllSources();
	}

	return sources;
};

function getAllSources() {
	if($$.isSet(_SPECIFIC_SOURCE))
		return [ _SPECIFIC_SOURCE ];

	var sources = [];

	$("#liveStatsSources [name='liveStatsSource']").each(function() {
		sources.push($(this).val());
	});

	return sources;
};

function updateFilters(value) {
	var filter = $(".filter input[type='text']");

	if(typeof value !== 'undefined')
		filter.val(value);

	filter.trigger("keyup");

	if($$.UI.nicescrollEnabled)
		$(".scrollable").trigger("resize.ui");
};


function installFilterFieldsBehaviour() {
	$("#liveStatsForm fieldset .filter input").keyup(function(event) {
		if (event.which == 13) {
			event.preventDefault();
		}

		var $this = $(this);
		var value = $$.rawTrim($(event.target).val());
		var entries = $(".liveStatsEntry", $this.parent("div").prev(".liveStats"));

		var total, shown;

		total = shown = 0;

		if($$.isSet(value)) {
			value = value.toLowerCase();

			entries.each(function() {
				total++;

				var $entry = $(this);
				var checkbox = $("input[type='checkbox']", $entry);

				var text = $("label", $entry).text().toLowerCase();

				if(!$$.isChecked(checkbox) && text.indexOf(value) < 0) {
					$entry.addClass("hidden");
				} else {
					shown++;
					$entry.removeClass("hidden");
				}
			});

			if(total > 0 && shown == 0) {
				$(".noFilteredStats", $this.parent("div").prev(".liveStats")).removeClass("hidden");
			} else {
				$(".noFilteredStats", $this.parent("div").prev(".liveStats")).addClass("hidden");
			}
		} else {
			total = shown = entries.size();

			$(".noFilteredStats", $this.parent("div").prev(".liveStats")).addClass("hidden");

			entries.removeClass("hidden");
		}

		$(".filterReport", $this.parents(".filter")).text(shown + " / " + total);

		if($$.UI.nicescrollEnabled)
			$this.parent(".scrollable").trigger("resize.ui");

		return true;
	});
	
	$("#liveStatsForm .resetFilter").click(function() {
		updateFilters(null);
	});
	
	$("#liveStatsForm .removeSelection").click(function() {
		var $this = $(this);
		$(".liveStatsLabel.selected", $this.parents("fieldset")).each(function() {
			var $entry = $(this);
			$entry.removeClass("selected");
			$$.Utils.uncheck($("input[type='checkbox']", $entry));
		});

		var sources = getSelectedSources();
		var flags = getSelectedFlags();
		var types = getSelectedTypes();
		var lengths = getSelectedLengths();

		updateLiveStats(null, sources, flags, types, lengths);
		
		updateFilters();
	});
};

function installStatsSearchDynamicBehaviour() {
	//CLICK DYNAMIC EVENTS
	$("#liveStatsSources").on("click", ".liveStatsLabel label", function(event) {
		event.preventDefault();

		var container = $(this).parent(".liveStatsLabel");
		container.toggleClass("selected");

		$$.Utils.toggleCheck($("input[type='checkbox']", container));

		var sources = getSelectedSources();

		updateLiveStats($("#liveStatsSources"), sources, [], [], []);
	});

	$("#liveStatsFlags").on("click", ".liveStatsLabel label", function(event) {
		event.preventDefault();

		var container = $(this).parent(".liveStatsLabel");
		container.toggleClass("selected");

		$$.Utils.toggleCheck($("input[type='checkbox']", container));

		var sources = getSelectedSources();
		var flags = getSelectedFlags();

		updateLiveStats($("#liveStatsFlags"), sources, flags, [], []);
	});

	$("#liveStatsTypes").on("click", ".liveStatsLabel label", function(event) {
		event.preventDefault();

		var container = $(this).parent(".liveStatsLabel");
		container.toggleClass("selected");

		$$.Utils.toggleCheck($("input[type='checkbox']", container));

		var sources = getSelectedSources();
		var flags = getSelectedFlags();
		var types = getSelectedTypes();

		updateLiveStats($("#liveStatsTypes"), sources, flags, types, []);
	});

	$("#liveStatsLengths").on("click", ".liveStatsLabel label",  function(event) {
		event.preventDefault();

		var container = $(this).parent(".liveStatsLabel");
		container.toggleClass("selected");

		$$.Utils.toggleCheck($("input[type='checkbox']", container));

		var sources = getSelectedSources();
		var flags = getSelectedFlags();
		var types = getSelectedTypes();
		var lengths = getSelectedLengths();

		updateLiveStats($("#liveStatsLengths"), sources, flags, types, lengths);
	});
	
	//MOUSEOVER DYNAMIC EVENTS
	$(".liveStats").on("mouseover", ".liveStatsLabel", function() {
		$(this).addClass("ui-state-hover");
	});

	//MOUSEOUT DYNAMIC EVENTS
	$(".liveStats").on("mouseout", ".liveStatsLabel", function() {
		$(this).removeClass("ui-state-hover");
	});
};

function initializeStatsSearchInterface(callback) {
	$$.Log.debug("Initializing stats search interface...");

	installStatsSearchDynamicBehaviour();
	installFilterFieldsBehaviour();
		
	$$.Log.debug("Stats search interface has been initialized");
	
	if($$.isSet(callback)) {
		$$.Log.debug("Invoking stats search interface post-initialization callback...");
		
		callback();
		
		$$.Log.debug("Search stats interface post-initialization callback has been invoked");
	}
};