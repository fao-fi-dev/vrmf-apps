$(function() {
	var mailLink = $(".social.email a");
	var title = document.title;
	var url = window.location.href;
	var description = $("meta[name='description']").attr("content");
	
	if($$.isSet(mailLink) && mailLink.size() > 0)
		mailLink.attr('href', mailLink.attr('href').
										replace(/\{subject\}/g, escape(title) ).
										replace(/\{body\}/g, escape(description + "\n\nSee: " + url) ));
	
	var SHARE_SCRIPTS = {
		googlePlus: {
			id: "google-plus-wjs",
			url: "https://apis.google.com/js/plusone.js",
			callback: function() {
				$("#share .googlePlus").removeClass("hidden");
			}
		},
		twitter: {
			id: "twitter-wjs",
			url: "http://platform.twitter.com/widgets.js",
			enable: function() { return !$$.Browser.msie || $$.Browser.version >= 8; },
			callback: function() {
				window.twttr = {
					_e : [],
					ready : function(f) {
						window.twttr._e.push(f);
					}
				};
					
				window.twttr.ready(function(twttr) {
					//Google Analytics subscription to Twitter events ( tweet )
					//See: https://developers.google.com/analytics/devguides/collection/gajs/gaTrackingSocial#twitter
					window._share_extractParamFromUri = function(uri, paramName) {
						if (!uri) {
							return;
						}
						
						var regex = new RegExp('[\\?&#]' + paramName + '=([^&#]*)');
						var params = regex.exec(uri);
						if (params != null) {
							return unescape(params[1]);
						}
						return;
					};
					
					window._share_trackTwitter = function(intent_event) {
						try {
							if($$.isSet(_gaq)) {
								if (intent_event) {
									var opt_target;
									
									if (intent_event.target && intent_event.target.nodeName == 'IFRAME') {
										opt_target = window._share_extractParamFromUri(intent_event.target.src, 'url');
										
										_gaq.push([ '_trackSocial', 'twitter', 'tweet', opt_target ]);
									}
								}
							}
						} catch (E) {
							$$.Log.warn(E);
						}
					};
					
					twttr.events.bind('tweet', _share_trackTwitter);
					
					$("#share .twitter").removeClass("hidden");
				});
			}
		},
		facebook: {
			id: "facebook-jssdk",
			url: "http://connect.facebook.net/en_US/all.js#xfbml=1",
			callback: function() {
				window.fbAsyncInit = function() {
				    // init the FB JS SDK
				    FB.init({
				      status     : true, // check the login status upon init?
				      cookie     : true, // set sessions cookies to allow your server to access the session?
				      xfbml      : true  // parse XFBML tags on this page?
				    });

				    //Google Analytics subscription to Facebook events ( like / unlike / send )
					//See: https://developers.google.com/analytics/devguides/collection/gajs/gaTrackingSocial#facebook
				    FB.Event.subscribe('edge.create', function(targetUrl) {
				    	try {
							if($$.isSet(_gaq)) {
								_gaq.push(['_trackSocial', 'facebook', 'like', targetUrl]);
							}
				    	} catch (E) {
				    		$$.Log.warn(E);
				    	}
					});
					
					FB.Event.subscribe('edge.remove', function(targetUrl) {
						try {
							if($$.isSet(_gaq)) {
								_gaq.push(['_trackSocial', 'facebook', 'unlike', targetUrl]);
							}
						} catch (E) {
							$$.Log.warn(E);
						}
					});
					
					FB.Event.subscribe('message.send', function(targetUrl) {
						try {
							if($$.isSet(_gaq)) {
								_gaq.push(['_trackSocial', 'facebook', 'send', targetUrl]);
							}
						} catch (E) {
							$$.Log.warn(E);
						}
					});	
					
					$("#share .facebook").removeClass("hidden");
				};
			}
		},
		pinterest: {
			url: "//assets.pinterest.com/js/pinit.js",
			enable: function() { return false; }, //Currently disabled...
			callback: function() {
				$("#share .pinterest").removeClass("hidden");
			}
		},
		tumblr: {
			url: "http://platform.tumblr.com/v1/share.js",
			callback: function() {
				$("#share .tumblr").removeClass("hidden");
			}
		},
		stumbleUpon: {
			url: "//platform.stumbleupon.com/1/widgets.js",
			callback: function() {
				$("#share .stumbleupon").removeClass("hidden");
			}
		}
	};

	var SCRIPT;
	for(var key in SHARE_SCRIPTS) {
		SCRIPT = SHARE_SCRIPTS[key];
		
		if(!$$.isSet(SCRIPT.enable) || SCRIPT.enable())
			$$.ScriptManager.include(SCRIPT.id, SCRIPT.url, SCRIPT.callback);
	}
});