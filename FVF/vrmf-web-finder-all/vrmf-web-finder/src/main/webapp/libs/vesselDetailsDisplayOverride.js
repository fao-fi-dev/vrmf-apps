/** Provides current (i.e. specialized) functions for the generic 'vesselDetailsDisplay.js' library */
var _MIN_NUMBER_OF_EXTERNAL_LINKS = 1;

function enableSmartLinks() { 
	return true;
};

function postProcessAdditionalLinks(additionalLinks, menuTitle) {
	if($$.isSet(additionalLinks)) {
		var updateLinkUI = function(additionalLinks, link, icon) {
			link.addClass("asButton xSmallButtons thinButtons uCase widthMin65");
			
			link.removeClass("external textLink");
			
			link.attr($$.TooltipManager.TOOLTIP_CONFIGURATION_ATTRIBUTE, "{ position: { my: 'left bottom', at: 'left top' } }");
			
			additionalLinks.append(link);

			if($$.isSet(icon))
				$$.UI._buildUIButton(link).button("option", "icons", { secondary: icon });
			
			return link;
		};
		
		if(enableSmartLinks()) {
			var popupLinks = $("a.popup", additionalLinks).clone(true);
			$("a.popup", additionalLinks).remove();
			
			popupLinks.each(function() {
				additionalLinks.prepend(updateLinkUI(additionalLinks, $(this), "ui-icon-newwin"));
			});
			
			var links = $("a.external, " +
						  "a.externalStatic", additionalLinks).clone(true);
			var numAdditionalLinks = links.length;
	
			$("a.external, " +
			  "a.externalStatic", additionalLinks).remove();
	
			if(numAdditionalLinks > _MIN_NUMBER_OF_EXTERNAL_LINKS) {
				var menuContainer = $("<span class='dropDownMenu'/>");
				
				var menuTrigger = $("<span class='asButton xSmallButtons thinButtons uCase widthMin125 { " +
										"buttonOptions: { " +
											"icons: { " +
												"secondary: \"ui-icon-info\" " +
											"} " +
										"} " +
									"}'>" +
										$$.I18n.getText("vessels.display.external.links.menu", numAdditionalLinks) + "&nbsp;" + 
									"</span>");
				
				menuTrigger.attr("title", $$.isSet(menuTitle) ? menuTitle : $$.I18n.getText("vessels.display.external.links.menu.tip", numAdditionalLinks));
				
				var menu = $("<ul class=\"menu clean ui-tooltip-shadow hidden\"/>");
				
				links.each(function() {
					var $link = $(this);
					$link.removeClass("external textLink");
					
					var text = $link.text();
					$link.empty().
						  append("<span class='ui-icon ui-icon-search'/>").
						  append("<span>" + text + "</span>");
					
					var li = $("<li/>");
					li.append($link);
					
					menu.append(li);
				});
				
				menuContainer.append(menuTrigger);
				menuContainer.append(menu);
				
				additionalLinks.append(menuContainer);
				
				$$.UI._buildUIButtons(menuTrigger);
			} else {
				var $updater = updateLinkUI;
				
				links.each(function() {
					var $link = $(this);
					
					additionalLinks.append($updater(additionalLinks, $link, 'ui-icon-extlink')).
										   append("<span>&nbsp;</span>");
				});
			}
		}
	}
	
	$(".additionalLinkWrapper", additionalLinks).remove();
	
	return additionalLinks;
};

function getDefaultStatusClasses() {
	return "ui-state-default";
};

function getDefaultHoverClasses() {
	return "ui-state-hover";
};

function getDefaultDisabledClasses() {
	return "ui-state-disabled";
};

function getDefaultActiveClasses() {
	return "ui-state-active";
};

function getHeadingDefaultClasses() {
	return "ui-state-default";
};

function getHeadingHoverClasses() {
	return getDefaultHoverClasses();
};

function getHeadingDisabledClasses() {
	return getDefaultDisabledClasses();
};

function colorizeBySource(container) {
	var colorizer = $(".colorizer input[type='radio']:checked", container);
	
	return $$.isSet(colorizer) && colorizer.size() > 0 && "true" === colorizer.val();
};

function hideEmptyAttributes(container) {
	var toggler = $(".emptyAttributesToggler input[type='radio']:checked", container);
	
	return $$.isSet(toggler) && toggler.size() > 0 && "true" === toggler.val();
};

function displayPortCoordinates(event) {
	var $trigger = $(event.currentTarget);
	
	var data = $trigger.data("portData");
	
	var coords = data.coordinates;
	
	var containerIDs = [];
	
	for(var c=0; c<coords.length; c++) {
		var coordinates = coords[c];
		
		var latitude = coordinates.latitude;
		var longitude = coordinates.longitude;
		
		var latS = latitude.indexOf("S") > 0;
		var lonW = longitude.indexOf("W") > 0;

		var dLat = parseFloat(latitude.replace("N", "").replace("S", ""));
		var dLon = parseFloat(longitude.replace("E", "").replace("W", ""));

		if(latS)
			dLat = -dLat;

		if(lonW)
			dLon = -dLon;
		
		dLat = $$.Coordinates.convertToDecimalCoordinates(dLat);
		dLon = $$.Coordinates.convertToDecimalCoordinates(dLon);
		
		containerIDs.push({
			id: "gmap_" + c + "_" + new Date().getTime(),
			position: new google.maps.LatLng(dLat, dLon),
			symbolic: {
				latitude: latitude,
				longitude: longitude
			},
			source: coordinates.sourceSystem
		});
	}
	
	var content = "<div class=\"mapWrapperSmall" + (containerIDs.length > 1 ? " multipleMaps" : "") + "\">";
	for(var c=0; c<containerIDs.length; c++) {
		content += "<div>" +
				   	"<div id=\"" + containerIDs[c].id + "\" class=\"mapContainerSmall\"/>" +
			   		"<p>" +
			   			"<label>" + $$.I18n.getText("vessels.display.data.port.coordinates.latitude") + "</label> " + containerIDs[c].symbolic.latitude + " " + 
			   			"<label>" + $$.I18n.getText("vessels.display.data.port.coordinates.longitude") + "</label> " + containerIDs[c].symbolic.longitude + 
			   		"</p>" +
			   		"<p class=\"ui-state-highlight\">" +
			   			"<label>" + $$.I18n.getText("vessels.display.data.port.coordinates.source") + "</label> " + $$.Metadata.OTHER_SYSTEMS_MAP[containerIDs[c].source].name + 
			   		"</p>" +
				   "</div>";
	}
	
	content += "</div>";
	
	var wrapper = $("<div/>");
	wrapper.append(content);

	wrapper.dialog({
		title: $$.I18n.getText("vessels.display.data.port.coordinates.dialog.title", containerIDs.length, data.name, $trigger.data("portCountry").name),
		autoOpen: true,
		modal: true,
		resizable: false,
		width: 500,
		maxHeight: 350,
		open: function(event, ui) {
			var alreadyInitialized = $$.isSet($trigger.data("initialized")) && $trigger.data("initialized");
			
			if(!alreadyInitialized) {
				var mapOptions = {
					zoom : 8,
					mapTypeId : google.maps.MapTypeId.HYBRID
				};

				var map;
				for ( var c=0; c<containerIDs.length; c++) {
					map = new google.maps.Map($("#" + containerIDs[c].id).get(0), $.extend({
						center: containerIDs[c].position
					}, mapOptions));

					new google.maps.Marker({
						position: containerIDs[c].position,
						map: map,
						title: data.name
					});
				};
				
				$trigger.data("alreadyInitialized", true);
			} 
		}
	});

	return false;
};

function displayAddress(event) {
	var $trigger = $(event.currentTarget);
	var entity = $trigger.data("entity");
	
	var searchTerms = [];

	searchTerms.push(entity.address);

	if($$.isSet(entity.zipCode))
		searchTerms.push(entity.zipCode);

	if($$.isSet(entity.city))
		searchTerms.push(entity.city);

	if($$.isSet(entity.countryId))
		searchTerms.push($$.Metadata.getCountryByID(entity.countryId).name);

	var searchTerm = searchTerms.join(", ");

	var mapContainerId = "entity_" + new Date().getTime();

	var content = "<div class='mapWrapperSmall'>";
	content += "<div>" +
				   "<div id='" + mapContainerId + "' class='mapContainerSmall'/>" +
				   "<p><h3>" + $$.I18n.getText("vessels.display.data.entity.address.header") + "</h3></p>" + 
				   "<p>" +
				   		"<a target='_GMAPS_" + searchTerm.replace(/\'/g, "\\\"") + "' class='externalStatic bottomDotted' href='http://maps.google.com/maps?f=q&source=s_q&output=html&q=" + escape(searchTerms) + "'>" + searchTerm + "</a>" +
				   "</p>" +
			   "</div>";

	content += "</div>";

	var wrapper = $("<div/>");
	wrapper.append(content);

	$trigger.data("searchTerm", searchTerm);
	$trigger.data("mapContainerId", mapContainerId);
	
	wrapper.dialog({
		title: $$.I18n.getText("vessels.display.data.entity.address.dialog.title"),
		autoOpen: true,
		modal: true,
		resizable: false,
		width: 500,
		maxHeight: 350,
		open: function(event, ui) {
			var alreadyInitialized = $$.isSet($trigger.data("initialized")) && $trigger.data("initialized");
			
			if(!alreadyInitialized) {
				var geocoder = new google.maps.Geocoder();

			    var latlng = new google.maps.LatLng(0, 0);
			    var mapOptions = {
			      zoom: 16,
			      center: latlng,
			      mapTypeId: google.maps.MapTypeId.ROADMAP
			    };

			    var $mapContainerId = $trigger.data("mapContainerId");
				var $searchTerm = $trigger.data("searchTerm");

				var map = new google.maps.Map($("#" + $mapContainerId).get(0), mapOptions);

				geocoder.geocode({ 'address' : $searchTerm },
								 function(results, status) {
								 	if (status == google.maps.GeocoderStatus.OK) {
								 		map.setCenter(results[0].geometry.location);
								 		new google.maps.Marker({
											map : map,
											position : results[0].geometry.location,
											title : $searchTerm
										});
								 	} else {
								 		$$.Interface.warn($$.I18n.getText("vessels.display.data.warning.message.address.geocoding", status));
								 	}
								});
				
				$trigger.data("initialized", true);
			}
		}
	});
	
	return false;
};

function displayPSMFactsheetsList(event) {
	var $trigger = $(event.currentTarget);
	var PSMCountry = $trigger.data("PSMCountry");

	var PSMIDPrefixes = $$.Metadata.getPortStateMeasuresFactsheetIDsPrefix(PSMCountry);
	
	if($$.isSet(PSMIDPrefixes)) {
		var buttonClasses = [ "asButton", "smallButtons", "thinButtons", "uCase", "widthMin65" ];
		
		var PSMLinksContainer = "";
		PSMLinksContainer += "<div class=\"PSMLinksContainer fsContainer\">";

			PSMLinksContainer += "<div class=\"fsLinksList\">";
				PSMLinksContainer += "<span class='spinner'>&nbsp;</span>";
				PSMLinksContainer += "<span>" + $$.I18n.getText("vessels.display.data.psm.retrieving") + "</span>";
			PSMLinksContainer += "</div>";
		
			PSMLinksContainer += "<div class=\"fsLinksControl ui-state-highlight hidden\">";
				PSMLinksContainer += "<span class='left'>";
					PSMLinksContainer += "<label for=\"PSMLinksFilter\">&nbsp;" + $$.I18n.getText("vessels.display.data.psm.filter") + "&nbsp;</label>";
					PSMLinksContainer += "<input id=\"PSMLinksFilter\" size=\"32\" name=\"PSMLinksFilter\" type=\"text\" title=\"" + $$.I18n.getText("vessels.display.data.psm.filter.tip") + "\"></input>";
				PSMLinksContainer += "</span>";
				
				PSMLinksContainer += "<span class='left fsLinksControlButtons'>";
					PSMLinksContainer += "<span class='resetFS " + buttonClasses.join(" ") + "' title='" + $$.I18n.getText("vessels.display.data.psm.reset.tip") + "'>" + $$.I18n.getText("vessels.display.data.psm.reset") + "</span>";
				PSMLinksContainer += "</span>";
				
				PSMLinksContainer += "<span class='right rightPad fsLinksControlButtons'>";
					PSMLinksContainer += "<span class='statusFS'>" +
										 	"<label>" +
										 		$$.I18n.getText("vessels.display.data.psm.report", 
										 						"<span class='fsShown'>#</span>",
										 						"<span class='fsTotal'>#</span>") +
										 	"</label>";
				PSMLinksContainer += "</span>";

			PSMLinksContainer += "</div>";
		
		PSMLinksContainer += "</div>";
	
		PSMLinksContainer = $(PSMLinksContainer);
				
		PSMLinksContainer.dialog({
			title: $$.I18n.getText("vessels.display.data.psm.dialog.title", PSMCountry.name),
			modal: true,
			width: 700,
			maxHeight: 600,
			resizable: false,
			autoOpen: true,
			buttons: [ { 
							text: $$.I18n.getText("vessels.display.data.psm.dialog.button.ok"), 
							click: function() { 
								$(this).dialog("destroy"); 
							}
					   } ],
			open: function(event, ui) {
				var $dialog = $(this);
				
				$.ajax({
					cache: false,
					timeout: 5000,
					url: _COMMON_SERVICES_FS_PREFIX + 'psm/get/id/prefixes/' + PSMIDPrefixes.join(",") + '.json',
					type: 'get',
					dataType: 'json',
					error: function() {
						$(".fsLinksList", $dialog).empty().append("<h3>" + $$.I18n.getText("vessels.display.data.psm.timeout") + "</h3>");
					},
					success: function(result) { 
						var sorter = function(a, b) {
							var idA = a.factsheetID;
							var idB = b.factsheetID;
							var prefixA = idA.substring(0, idA.indexOf("_"));
							var suffixA = parseInt(idA.substring(idA.indexOf("_") + 1), 10);
							
							var prefixB = idB.substring(0, idB.indexOf("_"));
							var suffixB = parseInt(idB.substring(idB.indexOf("_") + 1), 10);
							
							return prefixA < prefixB ? -1 : ( prefixA > prefixB ? 1 : ( suffixA < suffixB ? -1 : 1 ) ) ;
						};

						var factsheets = result.data;
						var sortedFactsheets = factsheets.sort(sorter);
						
						var PSMFactsheetsList = "";
						
						var PSMFS;
						var PSMFSID;
						var PSMFSURL;
						var PSMFSTitle;
						
						for(var f=0; f<sortedFactsheets.length; f++) {
							PSMFS = sortedFactsheets[f];
							PSMFSID = PSMFS.factsheetID;
							PSMFSURL = PSMFS.factsheetURL;
							PSMFSTitle = PSMFS.factsheetTitle;
							
							PSMFSTitle = $$.isSet(PSMFSTitle) ? PSMFSTitle : " [ TITLE UNKNOWN ] ";
							
							PSMFactsheetsList += "<div class=\"fsEntry\">" +
													"<a " +
														"id=\"PSM_" + PSMFSID + "\" " + 
														"href=\"" + PSMFSURL + "\" " +
														"class=\"externalStatic default { position: { my: 'left bottom', at: 'left top' } }\" " +
														"target=\"_PSM_" + PSMFSID + "\" " +
														"title=\"" + $$.I18n.getText("vessels.display.data.psm.fs.tip", PSMFSTitle) + "\">" + 
														"<span class='fsID'>" + PSMFSID + "</span>" +
														"<span class='fsSeparator'>&nbsp;-&nbsp;</span>" +
														"<span class='fsTitle'>" + PSMFSTitle + "</span>" +
													"</a>" +
												  "</div>";	
						}
						
						$(".fsLinksList", $dialog).empty().append(PSMFactsheetsList);
						$(".fsShown, .fsTotal", $dialog).text(sortedFactsheets.length);
						
						var reset = $("span.resetFS", $dialog);
						reset.click(function() {
							$("input[name='PSMLinksFilter']", $dialog).val('').keyup();
						});
				
						reset.button();
						
						$("input[name='PSMLinksFilter']", $dialog).keyup(function(event) {
							if(event.which == 13) {
								event.preventDefault();
							}
							
							var value = $$.rawTrim($(event.currentTarget).val());
							var totalFS = $("div.fsEntry", $dialog).size();
							var shownFS = 0;
							
							if(value == null) {
								$("div.fsEntry", $dialog).show();
								totalFS = shownFS = sortedFactsheets.length;
							} else {
								value = value.toLowerCase();
								$(".fsTitle", $dialog).each(function() {
									var $entry = $(this);
									var text = $entry.text().toLowerCase();
									
									
									if(text.indexOf(value) < 0) {
										$entry.parents("div.fsEntry").hide();
									} else {
										shownFS++;
										$entry.parents("div.fsEntry").show();
									}
								});
							}
							
							$(".fsShown").text(shownFS);
							$(".fsTotal").text(totalFS);
						});
						
						$(".fsLinksControl").removeClass("hidden");
					}
				});
			}
		});
	} else {
		$$.Interface.warn($$.I18n.getText("vessels.display.data.psm.fs.unavailable"));
	}
	
	enableInputs();
	
	return false;
};