var UI_AUTOCOMPLETE_DEFAULTS = {
	html: true,
	delay: VRMF_FINDER_CONFIGURATION.timeouts.autocomplete,
	minLength: 2,
	position: {
		collision: "flipfit flip" 
	},
	search: function(event, ui) {
		return true; 
		//Always return TRUE: now synchronization is done via the 'requestToken' header
		/* Uncomment to block request if previous autocompletion has yet to terminate */
		//return !$(event.target).hasClass("ui-autocomplete-loading");			
	},
	open: function(event, ui) {
		if($$.UI.nicescrollEnabled) {
			$("ul.ui-autocomplete:visible").addClass("scrollable nicescroll").niceScroll({
				zindex: 10,
				cursorwidth: 10,
				scrollspeed: 0,
				autohidemode: false,
				grabcursorenabled: true
			});
			
			_NICESCROLL_COLOR_UPDATER();
		}
	},
	close: function(event, ui) {
		if($$.UI.nicescrollEnabled) 
			$("ul.ui-autocomplete:visible", ui.delegateTarget).addClass("scrollable").niceScroll("hide");
	},
	response: function(event, ui) {
		$$.TooltipManager.close($(ui.target));
	}
};

var UI_AJAX_AUTOCOMPLETE_DEFAULTS = $.extend(true, {}, AJAX_COMMON_SEARCH_DEFAULTS, {
	timeout: VRMF_FINDER_CONFIGURATION.timeouts.autocomplete,
	error: function(jqXHR, textStatus, errorThrown) { 
		var parsed = $$.Ajax.parseError(jqXHR, textStatus, errorThrown);
		
		var msg = "[ " + parsed.code + " : " + parsed.error + " ] " + ( $$.isSet(parsed.message) && $$.rawTrim(parsed.message) != null ? " : " + parsed.message : "" );
		
		var fullMsg = $$.I18n.getText("vrmf.common.ajax.communication.error", msg);
		
		var reportMessage = "<code class='selectable ajaxError asyncMessage'>";
		reportMessage += "<span class='debug'>" + this.type + " " + this.url + "</span>";
		reportMessage += "<br/>";
		reportMessage += "<br/>";

		if($$.isSet(this._lastRequestToken)) {
			var lastRequestToken = this._lastRequestToken();
			var requestToken = this.requestToken;

			displayMessage = !$$.isSet(requestToken) || requestToken === lastRequestToken;
		} else
			displayMessage = true;
		
		reportMessage += "</code>";
		
		if(displayMessage) {
			$$.Notification.severe(fullMsg, reportMessage);
		} else {
			$$.Log.error(reportMessage);
			$$.Log.error(fullMsg);
		}
		
		return false;
		//$$.Notification.severe("An Ajax " + this.type + " request to " + unescape(this.url) + " has failed: " + errorThrown + "...");
	},
	cache: false //Otherwise, cached requests will be interpreted as 'previous' live search requests...
});

var _AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE = null;

function haltAutocompletion(input) {
	$(input).removeClass("ui-autocomplete-loading");
};

function resetMultipleCodes(sourceInput, codesHolder) {
	sourceInput.val("");
	codesHolder.empty();
};

function getCommonAutocompletionParameters() {
	var params = {
		gd: true, //Group by UID,
		l: 50 //Limit to 50 items only
	};

	var sources = getMultiselectedValues($("#source"));

	if($$.isSet(sources) && sources.length > 0)
		params.s = sources;

	params.c = !isHistorical(); 

	if(isAtDate())
		params.ad = getAtDate();

	return params;
};

function commonAutocompletionSelectionHandler(toDisplay, codesHolder) {
	var del = $("<a href='#' title='" + $$.I18n.getText("search.controls.autocompletion.result.code.remove") + "' class='asButton smallButtons thinButtons'>&#160;</a>");
	del.button({
        icons: {
            primary: "ui-icon-close"
        },
        text: false
	});

	del.click(function() { 
		$(this).parent("li").slideUp(function() {
			$(this).remove();
		});

		return false;
	});

	var about = $("<a href='#' title='" + $$.I18n.getText("search.controls.autocompletion.result.code.about") + "' class='asButton smallButtons thinButtons'>&#160;</a>");
	about.button({
        icons: {
            primary: "ui-icon-lightbulb"
        },
        text: false
	});

	about.click(function() { 
		var aboutDialog = $("<div/>");
		aboutDialog.append($(this).next(".codesHolder").data("mappedCodes"));

		$(".dimmed", aboutDialog).removeClass("dimmed");

		aboutDialog.dialog({
			title: $$.I18n.getText("search.controls.autocompletion.result.code.detail"),
			modal: true,
			resizable: false,
			autoOpen: true
		});

		return false;
	});

	/* REMOVED */ //$$.TooltipManager.apply(about);

	var entry = $("<li class='hidden'/>").append(del).append(about).append(toDisplay);

	$(".codeName", toDisplay).click(function() {
		about.click();
	});

	codesHolder.append(entry);
	entry.slideDown();
};

function formatOccurrenciesReport(item) {
	return "<span class='autocompleteMeta'>" +
				"[ " +
					"<span title='" + $$.I18n.getText("search.controls.autocompletion.result.occurrencies.total") + "'>T:</span> <b>" + item.totalOccurrencies + "</b>, " +
					//"<span title='Number of distinct vessels with this attribute'>V:</span> <b>" + item.occurrencies + "</b>, " +
					"<span title='" + $$.I18n.getText("search.controls.autocompletion.result.occurrencies.distinct") + "'>V:</span> <b>" + item.groupedOccurrencies + "</b>" +
					($$.isSet(item.score) ? 
							", <span title='" + $$.I18n.getText("search.controls.autocompletion.result.score") + "'>&sigma;:</span> " + ( new Number(item.score).toFixed(2) ) + "%" 
					: 
					"") +
				" ]" +
		   "</span>"; 
};

function formatMappedDataReport(item, originalIdExtractor) {
	var extractor = $$.isSet(originalIdExtractor) ? originalIdExtractor : function(data) { return data.originalId; };
	return "<span class='autocompleteMeta'>" +
				"[ " +
					"<span title='"+ $$.I18n.getText("search.controls.autocompletion.result.vrmf.id") + "'>ID: <b>" + item.id + "</b>, </span>" +
					"<span title='"+ $$.I18n.getText("search.controls.autocompletion.result.source") + "'>S: <b>" + item.sourceSystem + "</b>, </span>" +
					"<span title='"+ $$.I18n.getText("search.controls.autocompletion.result.source.id") + "'>SID: <b>" + extractor(item) + "</b></span>" +
					($$.isSet(item.score) && item.score > 0 ? 
							", <span title='" + $$.I18n.getText("search.controls.autocompletion.result.score") + "'>&sigma;: " + ( new Number(item.score).toFixed(2) ) + "%" + "</span>" 
					: 
					"") +
				" ]" +
		   "</span>"; 
};

function formatVesselIdentifierItem(vesselIdentifier, requestTerm, postprocessor) {
	var formatted = "<div class='autocompletionContainer'>"; 
	var value = $$.isSet(postprocessor) ? postprocessor(vesselIdentifier.identifier) : vesselIdentifier.identifier;
	var identifier = !$$.isSet(requestTerm) ? value : value.emphasize($.ui.autocomplete.escapeRegex(requestTerm));

	formatted += identifier + " [ <span title='" + $$.I18n.getText("search.controls.autocompletion.result.identifier.type") + " (" + $$.Metadata.VESSEL_IDENTIFIERS_MAP[vesselIdentifier.identifierType].description + ")'>" + vesselIdentifier.identifierType + "</span> ]" +
				 formatOccurrenciesReport(vesselIdentifier); 
		     
	formatted += "</div>";

	return formatted;
};

function formatVesselNameItem(vesselName, requestTerm) {
	var formatted = "<div class='autocompletionContainer'>";
	var name = !$$.isSet(requestTerm) ? vesselName.name : vesselName.name.emphasize($.ui.autocomplete.escapeRegex(requestTerm));

	formatted += ( vesselName.name == vesselName.simplifiedName ? name :  "<span title='" + vesselName.name + " [ " + vesselName.simplifiedName + " ]'>" + name + "</span>" ) +
				 formatOccurrenciesReport(vesselName);
		     
	formatted += "</div>";

	return formatted;
};

function formatVesselTypeItem(vesselType, requestTerm) {
	var countryDependent = vesselType.countryId != null && vesselType.countryId != "";
	var hasAbbreviation = $$.isSet(vesselType.standardAbbreviation);

	var country = $$.Metadata.getCountryByID(vesselType.countryId);

	var formatted = "<div class='autocompletionContainer'>";
	var name = !$$.isSet(requestTerm) ? vesselType.name : vesselType.name.emphasize($.ui.autocomplete.escapeRegex(requestTerm));
	var abbreviation = hasAbbreviation ? ( !$$.isSet(requestTerm) ? vesselType.standardAbbreviation : vesselType.standardAbbreviation.emphasize($.ui.autocomplete.escapeRegex(requestTerm)) ) : null;

	formatted += "<div>" + ( countryDependent ? $$.Flags.getImage(country) : "" ) + 
						   ( hasAbbreviation ? abbreviation + " - " : "" ) + name + 
						   " (" + ( $$.isSet(requestTerm) ? vesselType.originalVesselTypeId.emphasize($.ui.autocomplete.escapeRegex(requestTerm)) : vesselType.originalVesselTypeId ) + ")" + 
				 "</div>" + 
				 formatMappedDataReport(vesselType, function(data) { return data.originalVesselTypeId; });


		formatted += "<div><dl class='clean dimmed'>";
		if(groupCodes() && vesselType.mappedTypes != null && vesselType.mappedTypes != "" && vesselType.mappedTypes.length > 0) {

		var mappedType;

		for(var m=0; m<vesselType.mappedTypes.length; m++) {
			mappedType = vesselType.mappedTypes[m];

			formatted += "<dt>" + formatVesselTypeItem(mappedType, requestTerm) + "</dt>";
		}

		formatted += "</dl></div>";
	}
		     
	formatted += "</div>";

	return formatted;
};

function formatGearTypeItem(gearType, requestTerm) {
	var hasAbbreviation = $$.isSet(gearType.standardAbbreviation);

	var formatted = "<div class='autocompletionContainer'>";
	var name = !$$.isSet(requestTerm) ? gearType.name : gearType.name.emphasize($.ui.autocomplete.escapeRegex(requestTerm));
	var abbreviation = hasAbbreviation ? ( !$$.isSet(requestTerm) ? gearType.standardAbbreviation : gearType.standardAbbreviation.emphasize($.ui.autocomplete.escapeRegex(requestTerm)) ) : null;

	formatted += "<div>" + ( hasAbbreviation ? abbreviation + " - " : "" ) + name + 
						   " (" + ( $$.isSet(requestTerm) ? gearType.originalGearTypeCode.emphasize($.ui.autocomplete.escapeRegex(requestTerm)) : gearType.originalGearTypeCode ) + ")" + 
				 "</div>" + 
				 formatMappedDataReport(gearType, function(data) { return data.originalGearTypeCode; });

	if(groupCodes() && gearType.mappedTypes != null && gearType.mappedTypes != "" && gearType.mappedTypes.length > 0) {
		formatted += "<div><dl class='clean dimmed'>";

		var mappedType;

		for(var m=0; m<gearType.mappedTypes.length; m++) {
			mappedType = gearType.mappedTypes[m];

			formatted += "<dt>" + formatGearTypeItem(mappedType, requestTerm) + "</dt>";
		}

		formatted += "</dl></div>";
	}
		     
	formatted += "</div>";

	return formatted;
};

function formatVesselExternalMarkingItem(vesselExtMark, requestTerm) {
	var formatted = "<div class='autocompletionContainer'>";
	var extMark = !$$.isSet(requestTerm) ? vesselExtMark.externalMarking : vesselExtMark.externalMarking.emphasize($.ui.autocomplete.escapeRegex(requestTerm));

	formatted += "<div>" + 
					extMark +
					formatOccurrenciesReport(vesselExtMark) +
				 "</div>";

	return formatted;
};

function formatVesselIRCSItem(vesselIRCS, requestTerm) {
	var formatted = "<div class='autocompletionContainer'>";
	var IRCS = !$$.isSet(requestTerm) ? vesselIRCS.IRCS : vesselIRCS.IRCS.emphasize($.ui.autocomplete.escapeRegex(requestTerm));

	formatted += "<div>" + 
					IRCS +
					formatOccurrenciesReport(vesselIRCS) +
				 "</div>";

	return formatted;
};

function formatVesselMMSIItem(vesselMMSI, requestTerm) {
	var formatted = "<div class='autocompletionContainer'>";
	var MMSI = !$$.isSet(requestTerm) ? vesselMMSI.MMSI : vesselMMSI.MMSI.emphasize($.ui.autocomplete.escapeRegex(requestTerm));

	formatted += "<div>" + 
					MMSI +
					formatOccurrenciesReport(vesselMMSI) +
				 "</div>";

	return formatted;
};

function zformatVesselRegistrationPortItem(port, requestTerm, inner) {
	var country = $$.Metadata.getCountryByID(port.countryId);

	var formatted = $$.isSet(inner) && inner ? "" : "<li><a><div class='autocompletionContainer'>";
	var name = !$$.isSet(requestTerm) ? port.name : port.name.emphasize($.ui.autocomplete.escapeRegex(requestTerm));

	formatted += "<div>"+ $$.Flags.getImage(country) + name + " - " + country.iso2Code + "</div>" +
				 formatMappedDataReport(port);

	if(groupCodes() && port.mappedPorts != null && port.mappedPorts != "" && port.mappedPorts.length > 0) {
		formatted += "<div><dl class='clean dimmed'>";

		var mappedPort;

		for(var m=0; m<port.mappedPorts.length; m++) {
			mappedPort = port.mappedPorts[m];

			formatted += "<dt>" + formatVesselRegistrationPortItem(mappedPort, requestTerm, true) + "</dt>";
		}

		formatted +="</dl></div>";
	}

	formatted += $$.isSet(inner) && inner ? "" : "</div></a></li>";

	return formatted;
};

function formatVesselRegistrationPortItem(port, requestTerm, inner) {
	var country = $$.Metadata.getCountryByID(port.countryId);

	var formatted = "<div class='autocompletionContainer'>";
	var name = !$$.isSet(requestTerm) ? port.name : port.name.emphasize($.ui.autocomplete.escapeRegex(requestTerm));

	formatted += "<div>"+ $$.Flags.getImage(country) + name + " - " + country.iso2Code + "</div>" +
				 formatMappedDataReport(port);

	if(groupCodes() && port.mappedPorts != null && port.mappedPorts != "" && port.mappedPorts.length > 0) {
		formatted += "<div><dl class='clean dimmed'>";

		var mappedPort;

		for(var m=0; m<port.mappedPorts.length; m++) {
			mappedPort = port.mappedPorts[m];

			formatted += "<dt>" + formatVesselRegistrationPortItem(mappedPort, requestTerm, true) + "</dt>";
		}

		formatted +="</dl></div>";
	}

	formatted += "</div>";

	return formatted;
};


function formatVesselRegistrationNumberItem(vesselRegNo, requestTerm) {
	var formatted = "<div class='autocompletionContainer'>";
	var regNo = !$$.isSet(requestTerm) ? vesselRegNo.registrationNumber : vesselRegNo.registrationNumber.emphasize($.ui.autocomplete.escapeRegex(requestTerm));

	formatted += "<div>" + 
					regNo + 
					formatOccurrenciesReport(vesselRegNo) +
				 "</div>";

	return formatted;
};

function formatVesselFishingLicenseItem(vesselFishingLicense, requestTerm) {
	var formatted = "<div class='autocompletionContainer'>";
	var fishingLicense = !$$.isSet(requestTerm) ? vesselFishingLicense.licenseId : vesselFishingLicense.licenseId.emphasize($.ui.autocomplete.escapeRegex(requestTerm));

	formatted += "<div>" + 
					fishingLicense + 
					formatOccurrenciesReport(vesselFishingLicense) +
				 "</div>";

	return formatted;
};