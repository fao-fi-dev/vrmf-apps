var VRMF_FINDER_CONFIGURATION = {
	timeouts: {
		generic: 6000,
		login: 10000,
		metadata: 10000,
		retrieveStats: 30000,
		searchStats: 30000,
		searchLive: 30000,
		search: 40000,
		autocomplete: 5000,
		picturesManager: 25000,
		positionsManager: 25000,
		infringementsManager: 25000,
		polling: 20000
	}
};