function expandSearchForm() {
	$("#searchCriteria").addClass("expanded");
};

function collapseSearchForm() {
	$(window).scrollTo(0);

	$("#searchCriteria").removeClass("expanded");
};

function updateSearchType() {
	var $this = $("#searchCriteria .actionsTop [name='searchType']:checked");

	$$.Notification.clear();
	
	if("quick" === $this.val())
		return enableQuickSearch();
	else if("basic" === $this.val())
		return enableBasicSearch();
	else if("advanced" === $this.val())
		return enableAdvancedSearch();
	else
		return enableStatsSearch();
};

function updateSearchFilters(callback) {
	if(isHistorical()) {
		$(".hideIfNotHistorical").removeClass("hidden");
	} else {
		$(".hideIfNotHistorical").addClass("hidden");
	}

	if(byUID()) {
		$(".hideIfNotGroupingByUID").removeClass("hidden");
		$(".hideIfNotGroupingByID").addClass("hidden");
	} else {
		$(".hideIfNotGroupingByUID").addClass("hidden");
		$(".hideIfNotGroupingByID").removeClass("hidden");
	}
	 
	if($$.isSet(callback))
		callback();
};


function isStatsSearch() {
	return _isSearch("stats");
};

function isQuickSearch() {
	return _isSearch("quick");
};

function isNormalSearch() {
	return _isSearch("normal");
};

function isAdvancedSearch() {
	return _isSearch("advanced");
};

function _isSearch(type) {
	return $("#searchCriteria .actionsTop [name='searchType']:checked").val() === type;
};

function enableCommonSearch() {
	addAutocompletion();

	$("#liveSearchFormContainer, #liveStatsFormContainer").addClass("hidden");

	$("#results").removeClass("force");

	$("#results .resultsHeader .vesselAttribute").addClass("sortable").removeClass("ui-state-disabled");

	clearSearchResults();

	return true;
};

function enableBasicSearch() {
	updateSearchFilters();

	$("#formContainer, .actionsBottom").removeClass("hidden");

	$(".advancedSearch").addClass("hidden");

	return enableCommonSearch();
};

function enableAdvancedSearch() {
	updateSearchFilters();

	$("#formContainer, .actionsBottom").removeClass("hidden");

	$(".advancedSearch").removeClass("hidden");

	return enableCommonSearch();
};

function initializeSliders() {
	var numAvailableSources = getAvailableVesselSources();
	
	$("#numSourcesTo").val(numAvailableSources.length);

	$(".numSources.slider").slider({
		max: parseInt($("#numSourcesTo").val(), 10),
		values: [ parseInt($("#numSourcesFrom").val(), 10), parseInt($("#numSourcesTo").val(), 10) ]
	});
	
	$(".numSources.slider").slider({
		range: true,
		min: parseInt($("#numSourcesFrom").val(), 10),
		max: parseInt($("#numSourcesTo").val(), 10),
		step: 1,
		values: [ parseInt($("#numSourcesFrom").val(), 10), parseInt($("#numSourcesTo").val(), 10) ],
		slide: function( event, ui ) {
			$("#numSourcesFrom").val(ui.values[ 0 ]);
			$("#numSourcesTo").val(ui.values[ 1 ]);
		}
	});
};

function buildVesselSourcesGroup(component, sourcesGroups) {
	buildSourcesGroup(component, sourcesGroups, function(source) { return $$.isSet(source) && source.vesselSource; });
};

function buildAuthorizationSourcesGroup(component, sourcesGroups) {
	buildSourcesGroup(component, sourcesGroups, function(source) { return $$.isSet(source) && source.authSource; });
};

function buildSourcesGroup(component, sourcesGroups, filter) {
	var target = $(component).empty();

	var optgroup = null;
	var system = null;
	var group = null;

	for(var g=0; g<sourcesGroups.length; g++) {
		group = sourcesGroups[g];

		optgroup = "<optgroup label='" + group.description + "'>";

		for(var s=0; s<group.systems.length; s++) {
			system = group.systems[s];

			if(filter(system)) {
				optgroup += "<option value='" + system.id + "'>";

				if($$.Browser.msie )
					optgroup += system.id + " - " + system.name;

				optgroup += "</option>";
			} else {
				$$.Log.debug("Excluding '" + system.id + "' from " + $(component).attr("id") + " according to current sources' filter...");
			}
		}

		optgroup += "</optgroup>";

		if(group.systems.length > 0)
			target.append(optgroup);
	}

	if(!$$.Browser.msie ) {
		$("option", target).each(function() {
			var $this = $(this);
			var currentSystem = $$.Metadata.SYSTEMS_MAP[$this.val()];
			
			if(!$$.isSet(currentSystem)) {
				currentSystem = $$.Metadata.AUTH_SYSTEMS_MAP[$this.val()];
			}
			
			if(!$$.isSet(currentSystem)) {
				$$.Log.warn("Unable to find system data by ID '" + $this.val() + "'");
			}
			
			$this.append("<span>" + 
							($$.isSet(currentSystem) ? 
								$$.Sources.getSourceIconByID(currentSystem.id) + currentSystem.id + " - " + currentSystem.name
							 :
								$this.val) +  
						 "</span>");
		});
	}
};

function buildCountriesGroup(target, group) {
	var countriesForGroup = null;
	var country = null;

	var optgroup = "<optgroup label='" + group.description + "'>";
	
	countriesForGroup = group.countries;

	countriesForGroup = countriesForGroup.sort(function(a, b) {
		return a.name < b.name ? -1 : a.name > b.name ? 1 : 0;
	});

	var label = null;
	var hasISO2;
	for(var s=0; s<countriesForGroup.length; s++) {
		country = countriesForGroup[s];
		hasISO2 = $$.isSet(country.iso2Code);
		
		label = ( hasISO2 ? country.iso2Code + " - " : "" ) + country.name;
		
		optgroup += "<option value='" + country.id + "' " /* REMOVED */ /* + title='" + ( hasISO2 ? country.iso2Code + " - " : "" ) + country.name + "' */ + ">";
		optgroup += label;
		optgroup += "</option>";
	}
	
	optgroup += "</optgroup>";

	optgroup = $(optgroup);
	
	if(!$$.Browser.msie ) {
		var hasISO2;
		$("option", optgroup).each(function() {
			var $this = $(this);
			var country = $$.Metadata.getCountryByID($this.val());
			hasISO2 = $$.isSet(country.iso2Code);
			$this.empty().append("<span>" + 
									$$.Flags.lazyGetImageByCountryID(country.id) + 
									"<code>" + 
										( hasISO2 ? country.iso2Code + "&#160;-&#160;" : "&#160;&#160;&#160;&#160;&#160;" ) + 
									"</code>" +
									country.name +
								 "</span>");
		});
	}
	
	if(countriesForGroup.length > 0)
		target.append(optgroup);
};

function buildCountriesSelector(target, data) {
	var end = start = new Date();
	
	var allCountriesGroup = {
		id: 'ALL',
		description: $$.I18n.getText("search.controls.countries.all"),
		countries: $$.Metadata.COUNTRIES
	};

	buildCountriesGroup(target, allCountriesGroup);

	var group = null;

	for(var g=0; g<$$.Metadata.COUNTRIES_GROUPS.length; g++) {
		group = $$.Metadata.COUNTRIES_GROUPS[g];

		buildCountriesGroup(target, group);
	}
	
	end = new Date();
	
	$$.Log.debug("Building countries select for " + target.attr("id") + " took " + ( end.getTime() - start.getTime() ) + " mSec.");
};

function buildCountries(component) {
	var countries = $$.Metadata.COUNTRIES;

	countries = countries.sort(function(a, b) { 
		return a.name < b.name ? -1 : ( a.name > b.name ? 1 : 0 ); 
	});

	var target = $(component).empty();

	buildCountriesSelector(target, countries);
};

function buildFlags() {
	buildCountries("#flag");
};

function buildRegistrationCountries() {
	buildCountries("#regCountry");
};

function buildFishingLicenseCountries() {
	buildCountries("#fishingLicenseCountry");
};

function buildAuthorizationReportingCountries() {
	buildCountries("#reportingCountry");
};

function buildCodedSelection(component, data) {
	var target = $(component);
	
	$("option:not([value='none'])", target).remove();
	
	for(var d=0; d<data.length; d++) {
		target.append("<option value='" + data[d].id + "'>" + data[d].id + " - " + data[d].description + "</option>");
	}
};

function buildRangeSelection(component, ranges, selectedType, labelFormatter) {
	var target = $(component);
	
	$$.UI.destroyMultiselect(target);

	$("option:not([value='any'])", target).remove();
	
	if($$.isSet(selectedType) && selectedType !== "none") {
		if($$.isSet(ranges) && ranges.length > 0) {
			ranges = ranges.sort(_RANGE_SORTER);
			
			for(var r=0; r<ranges.length; r++) {
				target.append("<option value='" + ranges[r].rangeFrom + "_" + ranges[r].rangeTo + "'>" + 
								( $$.isSet(labelFormatter) ? labelFormatter(ranges[r]) : ranges[r].description ) + 
							  "</option>");
			}

			component.removeAttr("disabled");
//			initializeMultiselect(component).multiselect("enable");
		} else {
			component.attr("disabled", "disabled");
//			initializeMultiselect(component).multiselect("disable");
		}
	} else {
		component.attr("disabled", "disabled");
//		initializeMultiselect(component).multiselect("disable");
	}

	return true;
};

function buildIdentifierTypes() {
	var identifierType = $("#identifierType");

	$("option:gt(0)", identifierType).remove();

	for(var i=0; i<$$.Metadata.VESSEL_IDENTIFIERS.length; i++) {
		var identifier = $$.Metadata.VESSEL_IDENTIFIERS[i];

		if(identifier.isPublic && identifier.id != 'UID')
			identifierType.append("<option value='" + identifier.id + "'" + ( identifier.id == 'IMO' ? " class='default'" : "" ) + ">" + identifier.description + "</option>");
	};

	return identifierType;
};

function buildStatuses() {
	var optgroups = {
		IN_SERVICE: 				 $("<optgroup label=\"" + $$.I18n.getText("search.controls.status.group.inservice") + "\" class=\"main\"/>"),
		TEMPORARILY_NOT_IN_SERVICE:	 $("<optgroup label=\"" + $$.I18n.getText("search.controls.status.group.temporarily.not.inservice") + "\" class=\"main\"/>"),
		NOT_IN_SERVICE:  			 $("<optgroup label=\"" + $$.I18n.getText("search.controls.status.group.not.inservice") + "\" class=\"other\"/>"),
		PERMANENTLY_NOT_IN_SERVICE:  $("<optgroup label=\"" + $$.I18n.getText("search.controls.status.group.permanently.not.inservice") + "\" class=\"other\"/>")
	};
	
	var status;
	var option;

	var statusSelect = $("#status");

	if($$.Metadata.VESSEL_STATUS.length > 0) {
		statusSelect = statusSelect.empty();

		for(var i=0; i<$$.Metadata.VESSEL_STATUS.length; i++) {
			status = $$.Metadata.VESSEL_STATUS[i];
			option = $("<option/>");
			option.attr("value", status.id);
			option.text(status.id + " - " + status.description + " [" + status.sourceSystem + "]");

			if(status.inService) {
				option.addClass("main");
				optgroups.IN_SERVICE.append(option.clone());
			} 

			if(status.temporarilyNotInService) {
				option.addClass("other");
				optgroups.TEMPORARILY_NOT_IN_SERVICE.append(option.clone());
			}

			if(status.notInService) {
				option.addClass("other");
				optgroups.NOT_IN_SERVICE.append(option.clone());
			} 

			if(status.permanentlyNotInService) {
				option.addClass("other");
				optgroups.PERMANENTLY_NOT_IN_SERVICE.append(option.clone());
			}
		}

		if($("option", optgroups.IN_SERVICE).length > 0)
			statusSelect.append(optgroups.IN_SERVICE);

		if($("option", optgroups.TEMPORARILY_NOT_IN_SERVICE).length > 0)
			statusSelect.append(optgroups.TEMPORARILY_NOT_IN_SERVICE);

		if($("option", optgroups.NOT_IN_SERVICE).length > 0)
			statusSelect.append(optgroups.NOT_IN_SERVICE);

		if($("option", optgroups.PERMANENTLY_NOT_IN_SERVICE).length > 0)
			statusSelect.append(optgroups.PERMANENTLY_NOT_IN_SERVICE);
	}
};

function buildAgeClass() {
	var ageClass = $("#ageClass");

	$$.UI.destroyMultiselect(ageClass);

	var ranges = $$.Metadata.AGE_RANGES;

	if($$.isSet(ranges) && ranges.length > 0) {
		for(var r=0; r<ranges.length; r++) {
			ageClass.append("<option value='" + ranges[r].rangeFrom + "_" + ranges[r].rangeTo + "'>" + ranges[r].description + "</option>");
		}

		ageClass.removeAttr("disabled");
	} else {
		ageClass.attr("disabled", "disabled");
	}

	return true;
};

function buildLengthTypes() {
	var lengthRangeUpdater = function() {
		var selectedValue = $("#lengthType").multiselect("getChecked");
		selectedValue = $(selectedValue[0]).val();
		
		buildRangeSelection("#lengthClass", $$.Metadata.LENGTH_RANGES_MAP[selectedValue], selectedValue);
	};
	
	buildCodedSelection("#lengthType", $$.Metadata.LENGTH_TYPES, lengthRangeUpdater);
};

function buildTonnageTypes() {
	var tonnageRangeUpdater = function() {
		var selectedValue = $("#tonnageType").multiselect("getChecked");
		selectedValue = $(selectedValue[0]).val();
		
		buildRangeSelection("#tonnageClass", $$.Metadata.TONNAGE_RANGES_MAP[selectedValue], selectedValue);
	};
	
	buildCodedSelection("#tonnageType", $$.Metadata.TONNAGE_TYPES, tonnageRangeUpdater);
};

function buildMainEnginePowerTypes() {
	var mainEngineRangeUpdater = function() {
		var selectedValue = $("#mainEnginePowerType").multiselect("getChecked");
		selectedValue = $(selectedValue[0]).val();
		
		buildRangeSelection("#mainEnginePowerClass", $$.Metadata.POWER_RANGES_MAP[selectedValue], selectedValue);
	};
	
	buildCodedSelection("#mainEnginePowerType", $$.Metadata.POWER_TYPES, mainEngineRangeUpdater);
};

function buildAuxEnginesPowerTypes() {
	var auxEnginesRangeUpdater = function() {
		var selectedValue = $("#auxEnginePowerType").multiselect("getChecked");
		selectedValue = $(selectedValue[0]).val();
		
		buildRangeSelection("#auxEnginePowerClass", $$.Metadata.POWER_RANGES_MAP[selectedValue], selectedValue);
	};
	
	buildCodedSelection("#auxEnginePowerType", $$.Metadata.POWER_TYPES, auxEnginesRangeUpdater);
}

function updateAuthorizationTypes() {
	var issuingAuthorities = getMultiselectedValues($("#issuingAuthority")); 
	var authorizationTypes = $("#authorizationType");
	
	$$.UI.destroyMultiselect(authorizationTypes);
	
	buildAuthorizationTypes(issuingAuthorities);
	
	initializeMultiselect(authorizationTypes);
};

function updateCodedValuesClasses(typeSelector, classSelector, dataMap) {
	var type = $(typeSelector).multiselect("getChecked");
	type = $(type[0]).val();
	
	var typeClass = $(classSelector);
	
	$$.UI.destroyMultiselect(typeClass);
	
	buildRangeSelection(typeClass, dataMap[type], type);
	
	initializeMultiselect(typeClass);
};

function updateLengthClasses() {
	updateCodedValuesClasses("#lengthType", "#lengthClass", $$.Metadata.LENGTH_RANGES_MAP);
};

function updateTonnageClasses() {
	updateCodedValuesClasses("#tonnageType", "#tonnageClass", $$.Metadata.TONNAGE_RANGES_MAP);
};

function updateMainEnginePowerClasses() {
	updateCodedValuesClasses("#mainEnginePowerType", "#mainEnginePowerClass", $$.Metadata.POWER_RANGES_MAP);
};

function updateAuxEnginePowerClasses() {
	updateCodedValuesClasses("#auxEnginePowerType", "#auxEnginePowerClass", $$.Metadata.POWER_RANGES_MAP);
};

function buildAuthorizationTypes(authIssuers) {
	var target = $("#authorizationType");
	
	$("optgroup, option:not([value='any'])", target).remove();

	var allTypes = $$.Metadata.AUTHORIZATION_TYPES;
	var filteredTypes = [];

	var filterByType = $$.isSet(authIssuers) && ( authIssuers.length > 1 || authIssuers[0] != "" );
	for(var t=0; t<allTypes.length; t++) {
		if(!filterByType || authIssuers.indexOf(allTypes[t].sourceSystem) >= 0)
			filteredTypes.push(allTypes[t]);
	}

	var groups = { };

	for(var t=0; t<filteredTypes.length; t++) {
		if(!$$.isSet(groups[filteredTypes[t].sourceSystem]))
			groups[filteredTypes[t].sourceSystem] = [];

		groups[filteredTypes[t].sourceSystem].push(filteredTypes[t]);
	}

	for(var groupID in groups) {
		var optgroup = null;
		var option = null;

		optgroup = $("<optgroup label='" + $$.I18n.getText("search.controls.authorization.group", groupID) + "'/>");

		for(var at=0; at<groups[groupID].length; at++) {
			option = $("<option value='" + groups[groupID][at].id + "'>" + groups[groupID][at].description + "</option>");

			optgroup.append(option);
		}

		target.append(optgroup);
	}
};