var _LAST_SEARCH = {};

function computeCurrentPage(offset, resultsPerPage) {
	if(!$$.isSet(offset))
		return 1;

	return 1 + Math.floor( (offset + 1) / resultsPerPage );
};

function computeMaxPages(resultSize, resultsPerPage) {
	if(resultSize == 0)
		return 0;

	var numPages = resultSize / resultsPerPage;
	numPages = Math.floor(numPages);

	if((numPages * resultsPerPage) < resultSize)
		numPages++;

	return numPages;
}; 

function firstPage() {
	var currentPage = parseInt($("#currentPage").val(), 10);

	if(currentPage <= 1) {
		$$.Interface.warn($$.I18n.getText("search.common.warning.first.results.page"));
	} else {
		showPage(1);
	}
};

function rewindPage() {
	var currentPage = parseInt($("#currentPage").val(), 10);

	if(currentPage > 1) {
		var rewind = currentPage - 10;

		if(rewind < 1)
			rewind = 1;

		showPage(rewind);
	} else {
		$$.Interface.warn($$.I18n.getText("search.common.warning.first.results.page"));
	}
};

function previousPage() {
	var currentPage = parseInt($("#currentPage").val(), 10);

	if(currentPage > 1)	{
		showPage(currentPage - 1);
	} else {
		$$.Interface.warn($$.I18n.getText("search.common.warning.first.results.page"));
	}
};

function nextPage() {
	var currentPage = parseInt($("#currentPage").val(), 10);
	var lastPage = parseInt($("#maxPages").text(), 10);

	if(currentPage < lastPage) {
		showPage(currentPage + 1);
	} else {
		$$.Interface.warn($$.I18n.getText("search.common.warning.last.results.page"));
	}
};

function fastForwardPage() {
	var currentPage = parseInt($("#currentPage").val(), 10);
	var lastPage = parseInt($("#maxPages").text(), 10);

	if(currentPage < lastPage) {
		var forward = currentPage + 10;

		if(forward >= lastPage)
			forward = lastPage;

		showPage(forward);
	} else {
		$$.Interface.warn($$.I18n.getText("search.common.warning.last.results.page"));
	}
};

function lastPage() {
	var currentPage = parseInt($("#currentPage").val(), 10);
	var lastPage = parseInt($("#maxPages").text(), 10);

	if(lastPage > currentPage) {
		showPage(lastPage);
	} else {
		$$.Interface.warn($$.I18n.getText("search.common.warning.last.results.page"));
	}
};

function showPage(currentPage) {
	var lastPage = parseInt($("#maxPages").text(), 10);

	if(currentPage > lastPage || currentPage <= 0 || isNaN(currentPage))
		$$.Interface.warn($$.I18n.getText("search.common.warning.invalid.results.page", Math.min(lastPage, 1), lastPage));
	else {
		$("#currentPage").val(currentPage);

		var params = _LAST_SEARCH;
		params.o = getResultsPerPage() * (currentPage - 1);

		return doSearch(params);
	}

	return false;
};

function goToSelectedPage() {
	return showPage(parseInt($("#currentPage").val()), 10);
};

function byUID() {
	return isNormalSearch() || $("[name='groupByUID']:checked").val() === "true";
};

function isHistorical() {
	return !isNormalSearch() && $("[name='historicalSearch']:checked").val() === "true";
};

function groupCodes() {
	return isNormalSearch() || $("[name='groupCodes']:checked").val() === "true";
};

function isAtDate() {
	return isHistorical() && getAtDate() != null;
};

function getAtDate() {
	if(!isHistorical())
		return null;

	var date = $("#atDate").datepicker("getDate");

	if(date == null)
		return null;

	return new Date(date).formatDate();
};

function getResultsPerPage() {
	return parseInt(getMultiselectedValues($("#vesselsPerPage"))[0], 10);
};

function getTimingSearchParameters() {
	var normalSearch = isNormalSearch();

	var params = {
		c: normalSearch || $("[name='historicalSearch']:checked").val() === "false"
	};

	var atDate = normalSearch ? null : $("#atDate").datepicker("getDate");

	if(!params.c && atDate != null)
		params.ad = atDate.getTime();

	return params;
};

function getSourcesSearchParameters() {
	var advancedSearch = isAdvancedSearch();

	var groupByUID = byUID();
	var sources = getMultiselectedValues($("#source"));
	var duplicates = $("[name='duplicates']:checked").val() === "true";

	var params = {
	};

	if($$.isSet(sources))
		params.s = sources;

	if(groupByUID) {
		params.sd = advancedSearch ? duplicates : false;

		if(advancedSearch && countAvailableVesselSources() > 1) {
			params.sf = $("#numSourcesFrom").val();
			params.st = $("#numSourcesTo").val();
		}
		
		if(params.sf == 1 && params.st > params.sf)
			delete params.sf;
		
		if(!$$.isSet(params.sf) && params.st == countAvailableVesselSources())
			delete params.st;
	}
	
	return params;
};

function getBasicSearchParameters() {
	var normalSearch = isNormalSearch();
	var advancedSearch = isAdvancedSearch();

	var params = {
		gd: normalSearch ? true : byUID()
	};

	var name = $("#name").val();

	if(name.trim() != "") {
		var nameLike = getMultiselectedValues($("#nameLike"))[0];

		if("startsWith" == nameLike) {
			name += encodeURI("%");
		} else if("contains" == nameLike) {
			name = encodeURI("%") + name + encodeURI("%");
		} else
			name = encodeURI(name);
	}

	if($$.isSet(name) && name.trim() != "")
		params.n = [ name ];

	var flags = getMultiselectedValues($("#flag"));

	if($$.isSet(flags)) {
		params.f = flags;
	}

	var identifierType = getMultiselectedValues($("#identifierType"))[0];
	var identifier 	   = $("#identifier").val();

	if($$.isSet(identifier)) {
		if($$.isSet(identifierType)) {
			if("TUVI" == identifierType || "UID" == identifierType)
				params.it = "VRMF_UID";
			else
				params.it = identifierType;
		}

		if($$.isSet(params.it) && "VRMF_UID" == params.it) {
			var identifierAsNumber = parseInt(identifier, 10);

			if(!isNaN(identifierAsNumber))
				params.id = "" + identifierAsNumber;
		} else
			params.id = identifier;
	}

	if(advancedSearch) {
		var extMarking = $("#extMark").val();
		
		var status = normalSearch ? null : getMultiselectedValues($("#status"));

		var primaryGears = normalSearch ? null : new $$.Set();

		$("#primaryGearTypeCodes li span.codesHolder").each(function() {
			var codes = $(this).data("codes");

			var codesAsArray = codes.split(",");

			for(var c=0; c<codesAsArray.length; c++) {
				primaryGears.add(parseInt(codesAsArray[c], 10));
			}
		});

		if($$.isSet(primaryGears))
			primaryGears = primaryGears.toArray();

		var secondaryGears = normalSearch ? null : new $$.Set();

		$("#secondaryGearTypeCodes li span.codesHolder").each(function() {
			var codes = $(this).data("codes");

			var codesAsArray = codes.split(",");

			for(var c=0; c<codesAsArray.length; c++) {
				secondaryGears.add(parseInt(codesAsArray[c], 10));
			}
		});

		if($$.isSet(secondaryGears))
			secondaryGears = secondaryGears.toArray();

		var vesselTypes = normalSearch ? null : new $$.Set();

		$("#vesselTypeCodes li span.codesHolder").each(function() {
			var codes = $(this).data("codes");

			var codesAsArray = codes.split(",");

			for(var c=0; c<codesAsArray.length; c++) {
				vesselTypes.add(parseInt(codesAsArray[c], 10));
			}
		});

		if($$.isSet(vesselTypes))
			vesselTypes = vesselTypes.toArray();

		if($$.isSet(status)) {
			params.u = status;
		}

		if($$.isSet(vesselTypes)) {
			params.t = vesselTypes.join(",");
		}

		if($$.isSet(primaryGears)) {
			params.pg = primaryGears.join(",");
		}

		if($$.isSet(secondaryGears)) {
			params.sg = secondaryGears.join(",");
		}

		if($$.isSet(extMarking) && extMarking.trim() != "")
			params.e = encodeURI(extMarking);
	}

	return params;
};

function getAdvancedSearchParameters() {
	var normalSearch = isNormalSearch();
	var advancedSearch = isAdvancedSearch();

	var params = {};

	var ircs = $("#ircs").val();

	if($$.isSet(ircs)) {
		params.i = ircs;
	}

	var regCountry = getMultiselectedValues($("#regCountry"));

	if($$.isSet(regCountry)) {
		params.pc = regCountry;
	}

	var regNo = $("#regNo").val();

	if($$.isSet(regNo)) {
		params.r = regNo;
	}

	if(advancedSearch) {
		var mmsi = $("#mmsi").val();

		var regPorts = new $$.Set();

		$("#regPortCodes li span.codesHolder").each(function() {
			var codes = $(this).data("codes");

			var codesAsArray = codes.split(",");

			for(var c=0; c<codesAsArray.length; c++) {
				regPorts.add(parseInt(codesAsArray[c], 10));
			}
		});

		regPorts = regPorts.asArray();

		if($$.isSet(regPorts)) 
			params.p = regPorts;

		var fishingLicenseCountry = normalSearch ? null : getMultiselectedValues($("#fishingLicenseCountry"));
		var fishingLicense = normalSearch ? null : $("#fishingLicense").val();

		var ageClass = normalSearch ? null : getMultiselectedValues($("#ageClass"));

		var lengthType = normalSearch ? null : getMultiselectedValues($("#lengthType"));
		var lengthClass = normalSearch ? null : getMultiselectedValues($("#lengthClass"));

		var tonnageType = normalSearch ? null : getMultiselectedValues($("#tonnageType"));
		var tonnageClass = normalSearch ? null : getMultiselectedValues($("#tonnageClass"));

		var mainEngineType = normalSearch ? null : getMultiselectedValues($("#mainEnginePowerType"));
		var mainEngineClass = normalSearch ? null : getMultiselectedValues($("#mainEnginePowerClass"));

		var auxEngineType = normalSearch ? null : getMultiselectedValues($("#auxEnginePowerType"));
		var auxEngineClass = normalSearch ? null : getMultiselectedValues($("#auxEnginePowerClass"));

		var updatedFrom = normalSearch ? null : $("#updatedFrom").datepicker("getDate");
		var updatedTo =   normalSearch ? null : $("#updatedTo").datepicker("getDate");

		if($$.isSet(mmsi)) {
			params.m = mmsi;
		}

		if($$.isSet(fishingLicenseCountry)) {
			params.flc = fishingLicenseCountry;
		}

		if($$.isSet(fishingLicense)) {
			params.fln = fishingLicense;
		}

		var hasAgeClass = $$.isSet(ageClass) && ageClass[0].toLowerCase() != "any";
		var hasLengthType = $$.isSet(lengthType) && lengthType[0].toLowerCase() != "none";
		var hasLengthClass = $$.isSet(lengthClass) && lengthClass[0].toLowerCase() != "any";

		var hasTonnageType = $$.isSet(tonnageType) && tonnageType[0].toLowerCase() != "none";
		var hasTonnageClass = $$.isSet(tonnageClass) && tonnageClass[0].toLowerCase() != "any";

		var hasMainEngineType = $$.isSet(mainEngineType) && mainEngineType[0].toLowerCase() != "none";
		var hasMainEngineClass = $$.isSet(mainEngineClass) && mainEngineClass[0].toLowerCase() != "any";

		var hasAuxEngineType = $$.isSet(auxEngineType) && auxEngineType[0].toLowerCase() != "none";
		var hasAuxEngineClass = $$.isSet(auxEngineClass) && auxEngineClass[0].toLowerCase() != "any";

		if(hasLengthType) {
			params.lt = lengthType;
		}

		if(hasTonnageType) {
			params.tt = tonnageType;
		}

		if(hasMainEngineType) {
			params.met = mainEngineType;
		}

		if(hasAuxEngineType) {
			params.aet = auxEngineType;
		}

		if(hasAgeClass) {
			var entry = ageClass[0];

			var min = entry.substring(0, entry.indexOf("_"));
			var max = entry.substring(entry.indexOf("_") + 1);

			if($$.isSet(min))
				params.bym = min;

			if($$.isSet(max) && parseInt(max, 10) < _UNLIMITED_RANGE_TO)
				params.byM = max;
		}

		if(hasLengthClass) {
			var entry = lengthClass[0];

			var min = entry.substring(0, entry.indexOf("_"));
			var max = entry.substring(entry.indexOf("_") + 1);

			var range = ( $$.isSet(min) ? min : "" ) + "_" +
						( $$.isSet(max) ? max : "" );

			params.ls = [ range ];

		} else if(hasLengthType)
			params.ls = [ "0_" ];

		if(hasTonnageClass) {
			var entry = tonnageClass[0];

			var min = entry.substring(0, entry.indexOf("_"));
			var max = entry.substring(entry.indexOf("_") + 1);

			if($$.isSet(min))
				params.tm = min;

			if($$.isSet(max) && parseInt(max, 10) < _UNLIMITED_RANGE_TO)
				params.tM = max;
		} else if(hasTonnageType)
			params.tm = 0;

		if(hasMainEngineClass) {
			var entry = mainEngineClass[0];

			var min = entry.substring(0, entry.indexOf("_"));
			var max = entry.substring(entry.indexOf("_") + 1);

			if($$.isSet(min))
				params.mem = min;

			if($$.isSet(max) && parseInt(max, 10) < _UNLIMITED_RANGE_TO)
				params.meM = max;
		} else if(hasMainEngineType)
			params.mem = 0;

		if(hasAuxEngineClass) {
			var entry = auxEngineClass[0];

			var min = entry.substring(0, entry.indexOf("_"));
			var max = entry.substring(entry.indexOf("_") + 1);

			if($$.isSet(min))
				params.aem = min;

			if($$.isSet(max) && parseInt(max, 10) < _UNLIMITED_RANGE_TO)
				params.aeM = max;
		} else if(hasAuxEngineType)
			params.aem = 0;

		if($$.isSet(updatedFrom))
			params.uf = updatedFrom.getTime();

		if($$.isSet(updatedTo))
			params.ut = updatedTo.getTime();
	}

	return params;
};

function getAuthSearchParameters() {
	var normalSearch = isNormalSearch();

	var params = {};

	if(normalSearch)
		return params;

	var authIssuers = getMultiselectedValues($("#issuingAuthority"));
	var authCountries = getMultiselectedValues($("#reportingCountry"));
	var authTypes = getMultiselectedValues($("#authorizationType"));

	var authStatus = $("[name='authStatus']:checked").val();
	authStatus = '' === authStatus ? null : authStatus;

	var authStatusTiming = $$.isSet(authStatus) ? $("[name='authStatusTiming']:checked").val() : null;
	var authStatusAtDate = null;

	if("historical" !== authStatusTiming) {
		authStatusAtDate = "now" === authStatusTiming ? new Date() : $("#authAtDate").datepicker("getDate");
	}

	if($$.isSet(authIssuers)) { 
		params.aia = authIssuers;
	}

	if($$.isSet(authCountries)) { 
		params.aic = authCountries;
	}

	if($$.isSet(authTypes)) {
		params.at = authTypes;
	}

	if($$.isSet(authStatus)) {
		params.as = authStatus;
	}

	if($$.isSet(authStatusAtDate)) {
		params.asad = authStatusAtDate.getTime();
	}

	return params;
};

function getDisplayOptionsParameters() {
	var resultsPerPage = isNormalSearch() ? 30 : getResultsPerPage();
	var offset = resultsPerPage * parseInt($("#currentPage").val(), 10);

	var sortingCriteria = getSortingParameters();

	var params = { 
		ps: resultsPerPage,
		o: offset
	};

	if($$.isSet(sortingCriteria))
		params.sc = sortingCriteria;

	return params;
};

function getSortingParameters() {
	var sortingCriteria = [];

	if(_SEQUENTIAL_SORTERS.length > 0) {
		var current;
		for(var r=0; r<_SEQUENTIAL_SORTERS.length; r++) {

			current = _SEQUENTIAL_SORTERS[r];

			if($$.isSet(_SORTERS[current]))
				sortingCriteria.push(current + "_" + _SORTERS[current]);
		}
	}

	return sortingCriteria;
};

function checkParams(params) {
	var ok = false;

	for(var key in params) {
		ok |= $$.isSet(params[key]);
	}

	return ok;
};

function search(offset, callback, customOptions, quiet) {
	var params = {};

	if(isAdvancedSearch())
		$.extend(params, getTimingSearchParameters());

	$.extend(params, getSourcesSearchParameters());
	$.extend(params, getBasicSearchParameters());

	$.extend(params, getAdvancedSearchParameters());

	if(isAdvancedSearch()) {
		$.extend(params, getAuthSearchParameters());
		$.extend(params, getDisplayOptionsParameters());
	}

	if($$.isSet(offset))
		params.o = offset;
	else
		params.o = 0;

	doSearch(params, callback, customOptions, quiet);

	return false;
};

function doSearch(params, callback, customOptions, quiet) {
	if(!checkParams(params)) {
		$$.Interface.error($$.I18n.getText("search.common.error.no.criteria"));

		return;
	}
	
	params.user = $$.UserManager.getLoggedUser().getId();
	
	if(!$$.isSet(customOptions))
		customOptions = {};

	setStatus($$.I18n.getText("search.common.status.searching"));

	if(!$$.isSet(quiet) || !quiet)
		disableInputs($$.I18n.getText("search.common.blocking.message.searching"));

	var now = new Date();
	var customHeaders = [];
	customHeaders[_VRMF_SEARCH_REQUEST_TOKEN_HEADER] = now.getTime();

	$.ajax($.extend(true, {}, AJAX_SEARCH_DEFAULTS, {
		_requestStart: now, //Custom property
		_lastRequestTokenHolder: "_VRMF_SEARCH_REQUEST_TOKEN_HEADER", //Custom property
		url: _WRAP_URL(_VRMF_BROWSER_SERVICES_VESSELS_SEARCH_URL), 
		data: params,
		timeout: VRMF_FINDER_CONFIGURATION.timeouts.search,
		headers: customHeaders, 
		success: function(result, textStatus, JQXHR) {
			$$.Notification.clear();
			
			var _requestEnd = new Date();

			$$.Log.info("...Successfully completed search via Ajax " + this.type + " request to " + unescape(this.url));

			if(result.error) {
				$$.Interface.error($$.I18n.getText("search.common.error", result.errorMessage));
			} else {
				var responseHeader = JQXHR.getResponseHeader(_VRMF_SEARCH_REQUEST_TOKEN_HEADER);

				result.requestStart = this._requestStart;
				result.requestEnd = _requestEnd;

				if(responseHeader < this.headers[_VRMF_SEARCH_REQUEST_TOKEN_HEADER]) {
					//Search request was cached! 
					result.elapsed = 0;
				}

				buildSearchResults(result, params.o);
			}
		},
		complete: function() {
			_LAST_SEARCH = params;

			if(!$$.isSet(quiet) || !quiet)
				enableInputs();

			if($$.Browser.msie )
				$("#searchResults .ui-state-disabled").removeClass("ui-state-disabled");

			if($$.isSet(callback))
				callback();
		}
	}, customOptions));
};

function clearSearchResults() {
	$("#searchResults").addClass("empty");

	$("#searchResultsContainer").empty();

	$("#currentPage").val(0);
	$("#maxPages, #currentResults").text(0);

	$("#paginationReport").text("");

	$("#results .searchResultsHeader").addClass("hidden");

	//updateExportResultsLink();
};

function buildSearchResults(results, offset) {
	var target = $("#searchResultsContainer");
	target.empty();

	var start = new Date().getTime();
	
	if($$.isSet(results.data.dataSet)) {
		var html = "";

		var groupByUID = byUID();

		for(var row=0; row<results.data.dataSet.length; row++)
			html += buildCombinedVesselData(results.data.dataSet[row], groupByUID);

		target.append(html);

		var counter = 0;

		target = $("#searchResultsContainer");

		var groupByUID = byUID();

		var displayStart = new Date();

		$(".vesselResultEntry", target).each(function() {
			var $this = $(this);

			var id = parseInt($(groupByUID ? ".vesselUid" : ".vesselId", $this).text(), 10);  

			$$.Log.debug("Processing row " + counter + " ( id is: " + id + " )");
			 
			var even = counter % 2 == 0;
			$this.addClass(even ? _EVEN_ROW_CLASS : _ODD_ROW_CLASS).data("originalClass", even ? _EVEN_ROW_CLASS : _ODD_ROW_CLASS);

			if(groupByUID)
				$(".vesselUid", $this).click(function() {
					showParticipatingVesselDetails(id);
					return false;
				});

			$(".vesselName", $this).click(function() {
				showVesselDetails(groupByUID, id);
				return false;
			});

			counter++;
		});

		$("#currentPage").val(computeCurrentPage(offset, results.data.pageSize));
		$("#maxPages").text(computeMaxPages(results.data.fullResultsSize, results.data.pageSize));

		configureTooltips([ { selector: "#searchResultsContainer .vesselResultEntry [title]", config: TOOLTIP_SEARCH_RESULTS_DEFAULTS} ]);
		
		$("#searchResultsContainer .vesselResultEntry").hover(function() {
			$(this).removeClass(_EVEN_ROW_CLASS).
					removeClass(_ODD_ROW_CLASS).
					addClass(_HOVER_ROW_CLASS);
		}, function() {
			var originalClass = $(this).data("originalClass");
			$(this).removeClass(_EVEN_ROW_CLASS).
					removeClass(_ODD_ROW_CLASS).
					removeClass(_HOVER_ROW_CLASS).
					addClass(originalClass);
		});

		var displayEnd = new Date();

		var searchTime = results.elapsed;
		var searchAndRetrievmentTime = results.requestEnd.getTime() - results.requestStart.getTime();

		var retrievmentTime = searchAndRetrievmentTime - searchTime; 
		var displayTime = displayEnd.getTime() - displayStart.getTime();
		var overallTime = searchAndRetrievmentTime + displayTime; 

		setStatus($$.I18n.getText("search.common.status.completed", 
								  searchTime == 0 ? " 0 ( CACHED )" : searchTime, 
								  retrievmentTime, 
								  displayTime, 
								  overallTime));

		$("#results .searchResultsHeader").removeClass("hidden");

		if(results.data.fullResultsSize > 1)
			$("#paginationReport").text($$.I18n.getText("search.common.report.pagination", 
														results.data.dataSet.length,
														offset + 1,
														offset + results.data.dataSet.length,
														results.data.fullResultsSize));
		else
			$("#paginationReport").text("");

		$("#searchResults").removeClass("empty");
	} else {
		setError($$.I18n.getText("search.common.status.no.results"));

		clearSearchResults();
	}

	var end = new Date().getTime();

	$$.Log.info("Building the search results element took " + ( end - start ) + " mSec.");

	$("#currentResults").text(results.data.fullResultsSize);

	showSearchResultsPanel();
}

$(function() {
	var initializeSearchInterfaces = function() {
		initializeStatsSearchInterface();
		initializeLiveSearchInterface();
		initializeSearchInterface();
		initializeInterface();
		
		setStatusMessage($$.I18n.getText("search.common.status.ready"));
		$$.Utils.enable($("#startSearch"));
	};
	initializeCommonInterface(initializeSearchInterfaces);
});