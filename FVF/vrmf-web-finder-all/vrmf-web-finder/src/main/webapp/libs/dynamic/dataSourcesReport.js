<%@page contentType="text/javascript; charset=UTF-8" trimDirectiveWhitespaces="true" isErrorPage="false"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@page import="org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper"%>
<%@page import="org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper"%>
<%@page import="org.fao.fi.vrmf.common.j2ee.utilities.ServletsHelper"%>
<%@page import="org.fao.fi.vrmf.common.j2ee.RequestAttributesConstants"%>

<%@page import="org.fao.fi.vrmf.common.core.models.stats.StatusReportBySource"%>
<%@page import="org.fao.fi.vrmf.common.core.models.generated.SSystems"%>
<%
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

	String selectedLanguage = (String)request.getAttribute(RequestAttributesConstants.CURRENT_LANGUAGE_REQUEST_ATTRIBUTE);

	String requestedURL = (String)request.getAttribute(RequestAttributesConstants.ORIGINAL_URL_REQUEST_ATTRIBUTE);
	if(requestedURL == null)
		requestedURL = request.getRequestURL().toString();
%>
	/*****************************/
	/** VRMF DATA SOURCES REPORT */
	/*****************************/

	//Generated on     : <%=formatter.format(new Date())%>
	//Invocation URL   : <%=requestedURL%>
	//Selected language: <%=selectedLanguage%> 
	var VRMF_DATA_SOURCES     = new Array();
	var VRMF_DATA_SOURCES_MAP = new Object();
<%
	ServletsHelper.RequestInfo requestInfo = ServletsHelper.getRequestInfo(request); 
	
	String baseURI = StringsHelper.trim(requestInfo.getForwarderBaseURI());
	String base = ( baseURI != null ? baseURI.substring(0, baseURI.lastIndexOf("/")) : "" ) + requestInfo.getContextPath();
	boolean addPort = ( "http".equals(request.getScheme()) && request.getServerPort() != 80 ) ||
					  ( "https".equals(request.getScheme()) && request.getServerPort() != 443 );

	String baseHREF = requestInfo.getScheme() + "://" + 
					  request.getServerName() + 
					( addPort ? ":" + request.getServerPort() : "" ) + 
					  base + "/";
	
	if(StringsHelper.trim(request.getParameter("baseURL")) != null)
		baseHREF = StringsHelper.trim(request.getParameter("baseURL"));

	String callback = StringsHelper.trim(request.getParameter("callback"));
	String placeholder = StringsHelper.trim(request.getParameter("placeholder"));
	
	String defaultSearchType = StringsHelper.trim(request.getParameter("defaultSearchType"));
	if(defaultSearchType == null)
		defaultSearchType = "stats";
	
	Collection<SSystems> availableSourceSystems = (Collection<SSystems>)request.getAttribute(RequestAttributesConstants.DATA_SOURCES_REQUEST_ATTRIBUTE);
	Collection<StatusReportBySource> availableSourceSystemsStats = (List<StatusReportBySource>)request.getAttribute(RequestAttributesConstants.AVAILABLE_DATA_SOURCES_STATS_REQUEST_ATTRIBUTE);
	
	Map<String, SSystems> systemsMap = new HashMap<String, SSystems>();
	for(SSystems system : availableSourceSystems) {
		systemsMap.put(system.getId(), system);
	}
	
	int counter, totalVessels, totalGroupedVessels;
	counter = totalVessels = totalGroupedVessels = 0;
	
	String systemID, systemName, systemWebsite;
		
	for(StatusReportBySource stats : availableSourceSystemsStats) {
		systemID = stats.getSourceSystem();
		systemName = systemsMap.get(systemID).getName();
		systemWebsite = systemsMap.get(systemID).getWebsite();
		
		totalVessels += stats.getVessels();
		totalGroupedVessels += stats.getGroupedVessels();
		
		out.println();
		out.println("\t/* " + systemID + " current source data */");
		out.println("\tVRMF_DATA_SOURCES.push({" + 
			"id: '" + systemID + "', " + 
			"name: '" + systemName + "', " +
			"website: '" + systemWebsite + "', " +
			"icon: '" + baseHREF + "../cdn/media/images/decorations/sources/logo/small/" + systemID + ".png', " + 
			"totalVessels: " + stats.getVessels() + ", " + 
			"groupedVessels: " + stats.getGroupedVessels() + ", " +
			"lastUpdate: '" + formatter.format(stats.getLastUpdate()) + "', " + 
			"lastUpdateTimestamp: " + stats.getLastUpdate().getTime() + ", " +
			"searchURL: '" + baseHREF + systemID + "/search/#" + defaultSearchType + "'" +
		"});");
		
		out.println("\tVRMF_DATA_SOURCES_MAP['" + systemID + "'] = VRMF_DATA_SOURCES[" + counter + "];");
		
		counter++;
	}
%>
	var VRMF_DATA_SOURCES_TOTAL_VESSELS   = <%=totalVessels%>;
	var VRMF_DATA_SOURCES_GROUPED_VESSELS = <%=totalGroupedVessels%>;
	var VRMF_BASE_URL = '<%=baseHREF%>';
	
	var VRMF_DATA_SOURCES_DATA = {
		VRMF_BASE_URL: VRMF_BASE_URL,
		VRMF_NUMBER_OF_DATA_SOURCES: VRMF_DATA_SOURCES.length,
		VRMF_DATA_SOURCES: VRMF_DATA_SOURCES,
		VRMF_DATA_SOURCES_MAP: VRMF_DATA_SOURCES_MAP,
		VRMF_DATA_SOURCES_TOTALS: {
			ungrouped: VRMF_DATA_SOURCES_TOTAL_VESSELS,
			grouped: VRMF_DATA_SOURCES_GROUPED_VESSELS
		}
	};
	
	<%if(callback != null) {%>
	/* Check callback existance */	
	if(typeof <%=callback%> !== 'function') {
		/* Callback is not defined as a function */
		alert("Cannot embed VRMF sources data: '<%=callback%>' is not a function!");
	} else {
		/* Invoke callback */
		try {
			<%=callback%>(VRMF_DATA_SOURCES_DATA);
		} catch (E) { /* Catch and report callback invocation errors */
			alert("Unable to embed VRMF sources data: " + E);
		}
	}
	<%} else if(placeholder != null) {%>
		var target = document.getElementById('<%=placeholder%>');
		
		if(typeof target === 'undefined' || target == null) {
			alert("Unable to find target element by id '<%=placeholder%>'");
		} else {
			var data = VRMF_DATA_SOURCES_DATA.VRMF_DATA_SOURCES;
			var html = "";
			
			html += "<table class='vrmfDataSourcesReport'>\n";
				html += "<thead>\n";
					//html += "<tr><th colspan='4'>Available data sources</th></tr>\n";
					html += "<tr>\n";
						html += "<th class='vrmfDataSourceId'>Acronym</th>";
						html += "<th class='vrmfDataSourceName'>Name</th>";
						html += "<th class='vrmfDataSourceLastUpdate'>Last update</th>";
						html += "<th class='vrmfDataSourceNumberOfVessels'>Vessels</th>";
					html += "</tr>\n";
					
					html += "<tbody>\n";
			
			for(var d=0; d<data.length; d++) {
						html += "<tr class='vrmfDataSourceRow " + ( d%2 == 0 ? "even" : "odd" ) + "'>\n";
							html += "<td class='vrmfDataSourceId'>" + data[d].id + "</td>\n";
							html += "<td class='vrmfDataSourceName'>" +
										"<a href='" + data[d].website + "' target='_" + data[d].id + "' title='" + data[d].name + ". Click to open " + data[d].id + " homepage in a new window'>" + data[d].name + "</a>" + 
									"</td>\n";
						
							html += "<td class='vrmfDataSourceLastUpdate'>" + data[d].lastUpdate + "</a></td>";
							html += "<td class='vrmfDataSourceNumberOfVessels'>" +
										"<a href='" + VRMF_BASE_URL + data[d].id + "/search/#<%=defaultSearchType%>' target='_VRMF_FVF_" + data[d].id + "' title='Click to enter the Fishing Vessels Finder search panel for " + data[d].id + " vessels'>" +
											data[d].groupedVessels +
										"</a>" +
									"</td>\n";
						html += "</tr>\n";
			}	
					html += "</tbody>\n";
					
					html += "<tfoot>\n";
						html += "<tr>\n";
							html += "<th colspan='2'>\n" +
										"Number of data providers: " + data.length + 
									"</th>\n";
							html += "<th colspan='2'>\n" + 	
										"Records: " + VRMF_DATA_SOURCES_DATA.VRMF_DATA_SOURCES_TOTALS.grouped + "\n" +
									"</th>\n";
						html += "</tr>\n";
					html += "</tfoot>\n";
					
				html += "</thead>\n";
			html += "</table>\n";
			
			target.innerHTML = html;
		}
	<%}%>