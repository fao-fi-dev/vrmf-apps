<%@page import="java.util.Locale"%>
<%@page import="org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper"%>
<%@page import="org.fao.fi.vrmf.common.web.helpers.beans.text.i18n.CustomBundleManager"%>
<%@page contentType="text/javascript; charset=UTF-8" trimDirectiveWhitespaces="true" isErrorPage="false"%>
<%
	String callback = StringsHelper.trim(request.getParameter("callback"));
	String lang = StringsHelper.trim(request.getParameter("lang"));
	String bundles = StringsHelper.trim(request.getParameter("bundles"));
	
	if(lang == null)
		lang = "en";
		
	String[] realBundles = bundles == null ? new String[0] : bundles.split("\\,");
		
	for(int b=0; b<realBundles.length; b++)
		realBundles[b] = StringsHelper.trim(realBundles[b]);
	
	String bundlesPath = "i18n/finder/web/client";
	
	try {
		CustomBundleManager BM = new CustomBundleManager(bundlesPath, new Locale(lang, ""), realBundles);
%>
$$.Dep.require("vrmf.fvf.bundles", { "vrmf.i18n": { identifier: "$$.I18n" } });

var localizedMessages = <%=BM.toJSON()%>;
		
<%
		if(callback != null) {
%>
<%=callback%>(localizedMessages);
<%
		} else {
%>
$$.I18n.extend(localizedMessages);
<%
		}
	} catch (Throwable t) {
%>
$$.Interface.warn("Unable to load bundles <%=bundles%> for language \"<%=lang%>\" from path '<%=bundlesPath%>': <%=t.getClass().getSimpleName()%> [ <%=t.getMessage()%> ]");
<%		
		t.printStackTrace();
	}
%>