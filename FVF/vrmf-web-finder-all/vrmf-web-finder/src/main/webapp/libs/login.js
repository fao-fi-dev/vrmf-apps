function changeUser() {
	$("#loginDialog input").val("");
	$("#loginDialog .loginResult").removeClass("error").text("").hide();
	$("#loginButton").removeClass("ui-state-active");
	
	$("#loginForm input").unbind("keypress").keypress(function(event) {
		if(event.which == 13){
			$("#loginButton").addClass("ui-state-active");
			$("#loginForm").submit();
		}		
	});
	
	$("#loginDialog" ).dialog({
		title: $$.I18n.getText("vrmf.common.interface.user.login.title"),
		modal: true,
		draggable: false,
		resizable: false,
		width: 350,
		position: 'middle',
		show: { effect: 'fade' },
		hide: { effect: 'fade' },
		buttons: [
		    {
		    	id: "loginButton",
		    	text: $$.I18n.getText("vrmf.common.interface.user.login.login.button"),
		    	click: function() { $("#loginForm").submit(); }
		    },
			{ 
		    	id: "cancelLoginButton",
		    	text: $$.I18n.getText("vrmf.common.interface.user.login.cancel.button"),
		    	click: function() { $(this).dialog( "close" ); }
			}
		],
		open: function(event, ui){
			$('.ui-widget-overlay').css('width', $(window).width());
			$('.ui-widget-overlay').css('height', $(window).height());
		}, 
	    close: function(event, ui){$('body').css('overflow-x','auto'); } 
	});
};

function logout() {
	window.confirm($$.I18n.getText("vrmf.common.interface.user.login.logout.confirm"),
			function() {
				doLogout();
			},
			function() {
				
			}
	);
};

function doLogout(hardLogout) {
	$.ajax({
		url: _VRMF_BROWSER_SERVICES_LOGOUT_URL,
		type: 'get',
		success: function() {
			if(!$$.isSet(hardLogout) || !hardLogout)
				$$.Interface.warn($$.I18n.getText("vrmf.common.interface.user.login.logout.confirm.message.title"), 
								  $$.I18n.getText("vrmf.common.interface.user.login.logout.confirm.message"));
		},
		complete: function() {
			if(!$$.isSet(hardLogout) || !hardLogout)
				window.setTimeout(function() { window.location.reload(); }, 1500);
			else
				window.location.href = window.location.href.substring(0, window.location.href.lastIndexOf("/finder") + "/finder".length);
//			getLoggedUserDetails(function() {
//				 loggedIn();
//			});
		}
	});
}

function login() {
	$("#loginDialog .loginResult").removeClass("error").text("").hide();
	
	var message = null;
	var isLoggedIn = false;
	var isError = true;
	
	var username = $$.rawTrim($("#username").val());
	var password = $("#password").val();

	if(username == null || password == null) {
		$("#loginDialog .loginResult").text($$.I18n.getText("vrmf.common.interface.user.login.missing.credentials")).addClass("error").show();
		$("#username").focus();
	} else {
		$("#loginForm input").addClass("ui-state-disabled").attr("disabled", "disabled");
		$("#loginButton, #cancelLoginButton").addClass("ui-state-disabled");
		$("#loginDialog .waitPlease").fadeIn();

		$.ajax({
			timeout: VRMF_FINDER_CONFIGURATION.timeouts.login,
			url: _VRMF_BROWSER_SERVICES_LOGIN_URL,
			data: { 
				username: username,
				password: password
			},
			type: 'get',
			success: function() {
				
			},
			statusCode: {
				//For some reasons, success + statusCode doesn't seem to work for 200 (OK) responses (and jQuery 1.9.x). 
				//'Success' handling was consequently moved here... 
				200: function() {
					message = $$.I18n.getText("vrmf.common.interface.user.login.successful", username);
					isError = false;
					isLoggedIn = true;
				},
				/* Unauthorized (i.e. wrong credentials) */
				401: function() {
					message = $$.I18n.getText("vrmf.common.interface.user.login.invalid");
				}, 
				/* Forbidden (i.e. right credentials, but user not a public browser user */
				403: function() {
					message = $$.I18n.getText("vrmf.common.interface.user.login.unauthorized");
				}
			},
			complete: function() {
				$("#loginDialog .waitPlease").fadeOut(function() {
					if(message != null) {
						if(isError) {
							$("#loginDialog .loginResult").addClass("error");
						}
						
						$("#loginDialog .loginResult").text(message).fadeIn(function() {
							if(isLoggedIn) {		
								window.location.reload();
							}
						});
					}
					
					$("#loginForm input").removeAttr("disabled");
					$("#loginForm input, #loginButton, #cancelLoginButton").removeClass("ui-state-disabled");
				});
			}
		});
	}
};

function updateDefaultsForLoggedUser() {
	$("#dumpDialog [name='dumpSource']").each(function() {
		var $this = $(this);
		
		var managedSources = $$.UserManager.getLoggedUser().getManagedSourcesBySystem("FINDER");

		if($$.isSet(managedSources)) {
			if(managedSources.indexOf($this.val()) >= 0) {
				$$.Utils.check($this);
			} else
				$$.Utils.uncheck($this);
			
			$this.button("refresh");
		}
	});	
};

function loggedIn(additionalCallback) {
	var callback = function(loggedUser) {
		$("#currentUser .ui-button-text").text(" " + loggedUser.id);
		
		updateDefaultsForLoggedUser();
		
		if($$.isSet(additionalCallback))
			additionalCallback();
	};
	
	getLoggedUserDetails(callback); 
};

function getLoggedUserDetails(callback) {
	if(_IS_USER_LOGGED) {
		$.ajax({
			async: true,
			cache: false,		
			url: ( $$.isSet(_SPECIFIC_SOURCE) ? _SPECIFIC_SOURCE + "/" : "" ) + _COMMON_SERVICES_USER_DETAILS_URL_PREFIX + "/FINDER",
			timeout: VRMF_FINDER_CONFIGURATION.timeouts.login,
			success: function(result) {
				var loggedUser = null;
				
				if($$.isSet(result)) {
					if(result.error) {
						$$.Interface.error(result.additionalErrorMessage);
					} else {
						loggedUser = $$.UserManager.logIn(result.data);
											
						$$.Log.setup();
						
						var enableLogToggler = $$.isSet(loggedUser) && 
											   ( loggedUser.is("*_ADMIN") || loggedUser.is("ADMINISTRATOR") );
						
						if(!enableLogToggler)
							$$.Log.hide();
						else
							$$.Log.show();	
					}
				}
				
				updateInterfaceAccordingToUserGrants(loggedUser);
	//			updateExportResultsLink();
				callback(loggedUser);
			}
		});
	} else {
		updateInterfaceAccordingToUserGrants(null);
		callback(null);
	}
};

function updateInterfaceAccordingToUserGrants(loggedUser) {
	$(".userDependent").each(function() {
		var $this = $(this);
		
		var meta = $this.metadata();
		
		if($$.isSet(loggedUser)) {
			if($$.isSet(meta)) {			
				var id = $$.isSet($this.attr('id')) ? $this.attr('id') : meta.id;
				
				var displayIfNot = meta.displayIfNot;
				var displayIf = meta.displayIf;
				var enableIfNot = meta.enableIfNot;
				var enableIf = meta.enableIf;
				
				if($$.isSet(displayIf)) {
					var can = displayIf.can;
					var is = displayIf.is;
					
					var show  = !$$.isSet(can) && !$$.isSet(is);
					
					if($$.isSet(can))
						if($$.isArray(can)) {
							for(var c=0; c<can.length; c++)
								show |= loggedUser.can(can[c]);
						} else {
							show |= loggedUser.can(can);
						}
					
					if($$.isSet(is)) {
						if($$.isArray(is)) {
							for(var i=0; i<is.length; i++)
								show |= loggedUser.is(is[i]);
						} else {
							show |= loggedUser.is(is);
						}
					}

					if(!$this.hasClass("forceHidden")) {
						$$.Log.debug((show ? "Showing" : "Hiding") + " element " + ( $$.isSet(id) ? id + " " : "" ) + "according to currently logged user capabilities and element metadata");
						
						if(show) {
							$this.data("forceHidden", false);
							$this.show();
						} else {
							$this.data("forceHidden", true);
							$this.hide();
						}
					}
				}
				
				if($$.isSet(enableIf)) {
					var can = enableIf.can;
					var is = enableIf.is;
					
					var enable  = !$$.isSet(can) && !$$.isSet(is);
					
					if($$.isSet(can))
						if($$.isArray(can)) {
							for(var c=0; c<can.length; c++)
								enable |= loggedUser.can(can[c]);
						} else {
							enable |= loggedUser.can(can);
						}
					
					if($$.isSet(is)) {
						if($$.isArray(is)) {
							for(var i=0; i<is.length; i++)
								enable |= loggedUser.is(is[i]);
						} else {
							enable |= loggedUser.is(is);
						}
					}
					
//					var enable = $$.isSet(can) && loggedUser.can(can);
//					enable |= $$.isSet(is) && loggedUser.is(is);
					
					$$.Log.debug((enable ? "Enabling" : "Disabling") + " element " + ( $$.isSet(id) ? id + " " : "" ) + "according to currently logged user capabilities and element metadata");
					
					if(enable) {
						$this.data("forceDisabled", false);
						$$.Utils.enable($this);
					} else {
						$this.data("forceDisabled", true);
						$$.Utils.disable($this);
					}
				}
				
				if($$.isSet(displayIfNot)) {
					var can = displayIfNot.can;
					var is = displayIfNot.is;
					
					var show  = !$$.isSet(can) && !$$.isSet(is);
//					var show  = $$.isSet(can) && !loggedUser.can(can);
//						show |= $$.isSet(is) && !loggedUser.is(is);
					
					if($$.isSet(can))
						if($$.isArray(can)) {
							for(var c=0; c<can.length; c++)
								show |= !loggedUser.can(can[c]);
						} else {
							show |= !loggedUser.can(can);
						}
					
					if($$.isSet(is)) {
						if($$.isArray(is)) {
							for(var i=0; i<is.length; i++)
								show |= !loggedUser.is(is[i]);
						} else {
							show |= !loggedUser.is(is);
						}
					}
					
					if(!$this.hasClass("forceHidden")) {
						$$.Log.debug((show ? "Showing" : "Hiding") + " element " + ( $$.isSet(id) ? id + " " : "" ) + "according to currently logged user capabilities and element metadata");
						
						if(show) {
							$this.data("forceHidden", false);
							$this.show();
						} else {
							$this.data("forceHidden", true);
							$this.hide();
						}
					}
				}
				
				if($$.isSet(enableIfNot)) {
					var can = enableIfNot.can;
					var is = enableIfNot.is;
					
//					var enable = $$.isSet(can) && !loggedUser.can(can);
//					enable |= $$.isSet(is) && !loggedUser.is(is);
					
					var enable  = !$$.isSet(can) && !$$.isSet(is);
					
					if($$.isSet(can))
						if($$.isArray(can)) {
							for(var c=0; c<can.length; c++)
								enable |= !loggedUser.can(can[c]);
						} else {
							enable |= !loggedUser.can(can);
						}
					
					if($$.isSet(is)) {
						if($$.isArray(is)) {
							for(var i=0; i<is.length; i++)
								enable |= !loggedUser.is(is[i]);
						} else {
							enable |= !loggedUser.is(is);
						}
					}
					
					$$.Log.debug((enable ? "Enabling" : "Disabling") + " element " + ( $$.isSet(id) ? id + " " : "" ) + "according to currently logged user capabilities and element metadata");
					
					if(enable) {
						$this.data("forceDisabled", false);
						$$.Utils.enable($this);
					} else {
						$this.data("forceDisabled", true);
						$$.Utils.disable($this);
					}
				}
			}
		} else
			$this.hide();
	});
};