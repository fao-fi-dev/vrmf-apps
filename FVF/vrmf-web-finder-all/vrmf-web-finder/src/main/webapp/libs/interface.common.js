var _UI_THEMES = {
	VRMF: {
		description: "common.interface.themes.fvf",
		urlBase: /*_BASE_HREF + */ "../cdn/styles/themes/jquery/ui/vrmf/",
		themes: [ { id: "delta", description: "FVF default (Delta)" },
		          { id: "finder", description: "FVF blue (Aristo)" }, 
		          { id: "grey", description: "FVF grey (Aristo)" } ],
		themeResource: "jquery-ui.css",
		themeResources: {
			hsvar: 'jquery-ui-1.8.18.custom.css'
		}
	},
	UI: {
		description: "common.interface.themes.ui",
		urlBase: "http://code.jquery.com/ui/1.10.2/themes/",
		themes: [ { id: "black-tie", description: "UI Black-tie" }, 
		          { id: "blitzer", description: "UI Blitzer" }, 
		          { id: "cupertino", description: "UI Cupertino" }, 
		          { id: "dark-hive", description: "UI Dark-hive" }, 
		          { id: "dot-luv", description: "UI Dot-luv" }, 
		          { id: "eggplant", description: "UI Eggplant" }, 
		          { id: "excite-bike", description: "UI Excite-bike" }, 
		          { id: "flick", description: "UI Flick" },
		          { id: "hot-sneaks", description: "UI Hot-sneaks" }, 
		          { id: "humanity", description: "UI Humanity" }, 
		          { id: "le-frog", description: "UI Le-frog" }, 
		          { id: "mint-choc", description: "UI Mint-choc" },
		          { id: "overcast", description: "UI Overcast" },
		          { id: "pepper-grinder", description: "UI Pepper-grinder" }, 
		          { id: "redmond", description: "UI Redmond" }, 
		          { id: "smoothness", description: "UI Smoothness" },
		          { id: "south-street", description: "UI South-street" },
		          { id: "start", description: "UI Start" },
		          { id: "sunny", description: "UI Sunny" },
		          { id: "swanky-purse", description: "UI Swanky-purse" },
		          { id: "trontastic", description: "UI Trontastic" }, 
		          { id: "ui-darkness", description: "UI Darkness" },
		          { id: "ui-lightness", description: "UI Lightness" }, 
		          { id: "vader", description: "UI Vader" } ],
		themeResource: "jquery-ui.css"
	}
}; 

var _UI_THEMES_NOT_REQUIRING_BASE_CSS = [ ];//[ "finder", "grey" ];

var _DELAY_EXECUTION = function(after, fn) {
	var timer = null;
	return function() {
		if($$.isSet(timer)) 
			clearTimeout(timer);
		
		timer = setTimeout(fn, after);
	};
};

var _UNLIMITED_RANGE_TO = 10000000;

var _RANGE_SORTER = function(a, b) {
	var aFrom, aTo, bFrom, bTo;
	
	aFrom = parseInt(a.rangeFrom, 10);
	aTo = parseInt(a.rangeTo, 10);
	
	if(aTo >= _UNLIMITED_RANGE_TO)
		aTo = null;
	
	bFrom = parseInt(b.rangeFrom, 10);
	bTo = parseInt(b.rangeTo, 10);
	
	if(bTo >= _UNLIMITED_RANGE_TO)
		bTo = null;
	
	if($$.isSet(aTo) && $$.isSet(bTo))
		return aFrom < bFrom ? -1 : aFrom > bFrom ? 1 : ( aTo < bTo ? -1 : 1 );
		
	if($$.isSet(aTo) && !$$.isSet(bTo))
		return -1;
	
	if($$.isSet(bTo) && !$$.isSet(aTo))
		return 1;
	
	return aFrom < bFrom ? -1 : 1;
};

var _AVAILABLE_VESSEL_SOURCES = [];

function _NICESCROLL_COLOR_UPDATER() {
	if($$.UI.nicescrollEnabled) {
		var scrollbars = $("html, .scrollable").getNiceScroll();
		
		var bgColor = $("div.ui-widget-header:first").css("backgroundColor");
		var borderColor = $("div.ui-widget-content:first").css("borderLeftColor");
		
		for(var s=0; s<scrollbars.length; s++) {
			var $this = scrollbars[s];
			$($this.cursor).css("backgroundColor", bgColor);
			$($this.cursor).css("borderColor", borderColor);
			
			$($this.cursor).css("marginRight", "2px");
			$($this.cursor).unbind("mouseover").mouseover(function() {
				$(this).css("cursor", "url('http://www.google.com/intl/en_ALL/mapfiles/openhand.cur'),n-resize");
			});
		};
	}
};

var _NICESCROLL_UPDATER = function(event) {
	$("html, .scrollable").getNiceScroll().resize();
};

function loadMetadataAndUserDetails(callback) {
	$$.Flags.setBaseURL("../cdn/media/images/flags/famfamfam/");
	$$.Flags.setEmptyImgURL("../cdn/media/images/flags/empty.png");
	$$.Flags.setCase("lower");
	$$.Flags.setImgType("png");

//	$$.Flags.setBaseURL("http://www.fao.org/countryprofiles/flags/");
//	$$.Flags.setCase("upper");
//	$$.Flags.setImgType("gif");

	$$.Metadata.setBaseURL(_WRAP_URL("common/services/public/metadata/reference"));
	$$.Metadata.setTimeout(VRMF_FINDER_CONFIGURATION.timeouts.metadata);
	$$.Metadata.setCacheResponse(true);
	
	var toLoad = ['{systems}',
	              '{authorizationSystems}',
	              '{countries}',
	              '{ranges}',
	              '{factsheets}',
	              'identifiers',
	              'hullMaterials',
	              'vesselStatus',
	              'vesselTypes',
	              'gearTypes',
	              'measureUnits',
	              'lengthTypes', 
	              'tonnageTypes', 
	              'powerTypes', 
	              'authorizationTypes',
	              'authorizationTerminationReasons',
	              'authorityRoles',
	              'authorizationHolders',
	              'inspectionReportTypes',
	              'inspectionOutcomeTypes',
	              'inspectionInfringementTypes',
	              'rfmoIuuLists'];

	var user = $$.UserManager.getLoggedUser();
	
	if(user.getId() === 'NOT_SET') {
		user = new $$.UserModel();
		user.setId($$.isSet(_SPECIFIC_SOURCE) ? _SPECIFIC_SOURCE + "_USER" : "PUBLIC_USER");
	}	
	
	$$.Metadata._load(toLoad, {}, user, true, function() {
		$$.CountriesManager.initializeGroups($$.Metadata.COUNTRIES_GROUPS);

		_AVAILABLE_VESSEL_SOURCES = getAvailableVesselSources();
				
		getLoggedUserDetails(callback);
	});
};

function disableInputs(message, title) {
	$.blockUI({ 
		title: "<div class='ui-helper-clearfix'><div class='ui-dialog-title'><h3>" + ( $$.isSet(title) ? title : $$.I18n.getText("vrmf.common.interface.please.wait") ) + "</h3></div></div>",
		message: "<h3>" + message + "&nbsp;<span class='spinner'>&nbsp;</span></h3>",
		blockMsgClass: "ui-dialog"
	});
};

function enableInputs(callback) {
	if($$.UI.nicescrollEnabled)
		$(":nicescroll").getNiceScroll().resize();
	
	if($$.isSet(callback))
		$.unblockUI({ onUnblock: callback });
	else
		$.unblockUI();
};

function displayUserDetails() {
	var loggedUser = $$.UserManager.getLoggedUser();

	var userRoles = loggedUser.getRoles();
	var userCapabilities = loggedUser.getCapabilities();
	var managedCountries = loggedUser.getManagedCountriesBySystem("VRMF");
	var managedSources = loggedUser.getManagedSourcesBySystem("VRMF");

	var container = $("<div class='userDetails xSmallFont'/>");
	container.append("<div style='margin: 1em auto'><label>" + $$.I18n.getText("vrmf.common.interface.user.details.role") + ":</label></div>");

	for(var r=0; r<userRoles.size(); r++)
		container.append("<div class='moreLeftPad'><code>" + userRoles.get(r).role + "</code></div>");

	container.append("<div style='margin: 1em auto'><label>" + $$.I18n.getText("vrmf.common.interface.user.details.capabilities") + ":</label></div>");

	for(var c=0; c<userCapabilities.size(); c++)
		container.append("<div class='moreLeftPad'><code>" + userCapabilities.get(c).capabilityName + ( $$.isSet(userCapabilities.get(c).capabilityParameters) ? " { " + userCapabilities.get(c).capabilityParameters + " } " : "" ) + "</code></div>");

	if($$.isSet(managedCountries)) {
		container.append("<div style='margin-top: 1em'><label>" + $$.I18n.getText("vrmf.common.interface.user.details.manager.for") + ":</label></div>");

		var country = null;

		var countries = [];
		for(var m=0; m<managedCountries.length; m++) {
			country = $$.Metadata.getCountryByID(managedCountries[m]);

			if($$.isSet(country))
				countries.push(country);
		}

		countries = countries.sort(function(a, b) {
			return a.name > b.name ? 1 : -1;
		});

		for(var c=0; c<countries.length; c++) {
			country = countries[c];

			container.append("<div class='moreLeftPad managedCountry'>" + 
								 $$.Flags.getImage(country) +
								 "&nbsp;" +
								 "<span class='countryName'>" + country.name + "</span>" +
								 "&nbsp;" +
								 "(<code class='countryIso2'>" + country.iso2Code + "</code>)" +
							 "</div>");
		}
	}

	if($$.isSet(managedSources)) {
		container.append("<div style='margin: 1em auto'><label>" + $$.I18n.getText("vrmf.common.interface.user.details.manager.for") + ":</label></div>");

		var source = null;

		var sources = [];
		for(var m=0; m<managedSources.length; m++) {
			source = managedSources[m];

			sources.push(source);
		}

		sources = sources.sort(function(a, b) {
			return a.name > b.name ? 1 : -1;
		});

		for(var c=0; c<sources.length; c++) {
			source = sources[c];

			container.append("<div class='moreLeftPad managedSource'>" + 
					 			$$.Sources.getSourceIconByID(source) +
					 			"&nbsp;" +
								 "<span class='sourceName'>" + source + "</span>" +
								 "<span class='sourceDescription'>&#160;[&#160;<em>" + $$.Metadata.AUTH_SYSTEMS_MAP[source].name + "</em>&#160;]</span>" +
							 "</div>");
		}
	}

	$$.Interface.showDialog($$.I18n.getText("vrmf.common.interface.user.details.title"), container, { dialogClass: 'user' });
};

function isBrowserCompatible() {
	var browserCheck = $$.Browser.checkCompatibility({
		msie: {
			description: "Microsoft Internet Explorer",
			version: 8
		},
		opera: {
			description: "Opera",
			version: 10
		}
	});
		
	if(!browserCheck.compatible) {
		$("#mainContainer").empty();
		$("#shareContainer").remove();
		
		var browserList = $("#browserList");
		
		$(".browserDescription", browserList).text(browserCheck.description);
		$(".browserVersionCurrent", browserList).text(browserCheck.current);
		$(".browserVersionRequired", browserList).text(browserCheck.required);
		
		var readMore = $(".browserListReadMore .hidden." + browserCheck.browser, browserList);
		
		if(readMore.size() > 0) {
			readMore.removeClass("hidden");
			
			$(".browserListReadMore").removeClass("hidden");
		}
		
		$(browserList).dialog({
			dialogClass: "noClose",
			title: $$.I18n.getText("common.interface.browser.non.compatible"),
			minWidth: 640,
			autoopen: true,
			modal: true,
			resizable: false,
			open: function(event, ui) {
				$("a:first", this).blur();
				$("#currentUser, #switchUserLink, #logoutLink").hide();
				$("#footer select").hide().after("<span>[ default ]</span>");//attr("disabled", "disabled");
				$("img.browserIcon", this).lazyload();
			}
		});
		
		throw $$.I18n.getText("common.interface.browser.non.compatible");
	}
	
	return browserCheck.compatible;
};

function installThemeSelector() {
	var themeSelector = $("#theme");
	var themeData = null;
	var optgroup = null;
	var themeResource = null;
	
	var label, text;
	
	for(var themeCategory in _UI_THEMES) {
		optgroup = $("<optgroup label='" + $$.I18n.getText(_UI_THEMES[themeCategory].description) + "'/>");
		
		for(var t=0; t<_UI_THEMES[themeCategory].themes.length; t++) {
			themeData = _UI_THEMES[themeCategory].themes[t];
			
			themeResource = _UI_THEMES[themeCategory].themeResource;

			if($$.isSet(_UI_THEMES[themeCategory].themeResources) &&
			   $$.isSet(_UI_THEMES[themeCategory].themeResources[themeData.id]))
				themeResource = _UI_THEMES[themeCategory].themeResources[themeData.id];

			label = ( $$.isSet(themeData.description) ? themeData.description : themeData.id );
			text = themeData.id;
			
			optgroup.append("<option label='" + label + "' value='" + _UI_THEMES[themeCategory].urlBase + themeData.id + '/' + themeResource + "'>" + 
								text + 
							"</option>");
		}

		themeSelector.append(optgroup);
	}
	
	themeSelector.change(changeTheme);
};

function applyStoredTheme() {
	var storedTheme = $$.rawTrim($.cookie("vrmf.browser.theme.CSS"));
	var currentTheme = $$.rawTrim($("#themeCSS").attr("href"));
	
	var theme = $$.isSet(storedTheme) ? storedTheme : $$.isSet(currentTheme) ? currentTheme : null;

	if($$.isSet(storedTheme) && 
	   storedTheme != currentTheme) {
		$("#themeCSS").attr("href", storedTheme);
	}

	$$.select($("#theme option[value='" + theme + "']"));
	
	furtherConfigureTheme();
};

function changeTheme() {
	var theme = $("#theme option:selected");
	var themeURL = theme.val();

	$("#themeCSS").attr("href", themeURL).ready(function() {
		_DELAY_EXECUTION(250, _NICESCROLL_COLOR_UPDATER);
	});

	$.cookie("vrmf.browser.theme.CSS", $("#themeCSS").attr("href"), { path: '/' });

	furtherConfigureTheme();
};

function furtherConfigureTheme(selectedTheme) {
	var theme = ($$.isSet(selectedTheme) ? selectedTheme : $("#theme option:selected"));
	var themeID = theme.text();
	
	var baseCSS = $("#baseCSS");
	var baseCSSHREF = baseCSS.data("href");

	if(!$$.isSet(baseCSSHREF))
		baseCSS.data("href", baseCSS.attr("href"));

	if(_UI_THEMES_NOT_REQUIRING_BASE_CSS.indexOf(themeID) >= 0)
		baseCSS.attr("href", null);
	else
		baseCSS.attr("href", baseCSSHREF);
	
	var classes = $("body").attr("class").split(" ");
	
	var updatedClasses = [];
	for(var c=0; c<classes.length; c++) {
		if(!classes[c].startsWith("_theme_"))
			updatedClasses.push(classes[c]);
	}
	
	updatedClasses.push("_theme_" + themeID);
	
	$("body").attr("class", updatedClasses.join(" "));
};

function getAvailableVesselSources() {
	var system = null;
	var group = null;

	var uniqueVesselSources = new $$.Set();

	for(var g=0; g<$$.Metadata.SYSTEMS_GROUPS.length; g++) {
		group = $$.Metadata.SYSTEMS_GROUPS[g];

		for(var s=0; s<group.systems.length; s++) {
			system = group.systems[s];

			if(system.vesselSource)
				uniqueVesselSources.add(system);
		}
	}
		
	return uniqueVesselSources.asArray();
};

function countAvailableVesselSources() {
	return getAvailableVesselSources().length;
};

function checkEngine() {
	$.ajax({
		async: true,
		cache: false,
		type: 'get',
		dataType: 'json',
		url: _COMMON_SERVICES_SYSTEM_URL_PREFIX + "check/matchingEngine/status",
		success: function(results) {
			if(results.data.running) {
				var startDate = new Date(results.data.startTime);
				var content = "<div><h2 class='centered'>The matching engine is currently running!</h2>" +
							  "<p>The process started on <code>" + startDate.formatDate() + "</code> at <code>" + startDate.formatTime() + "</code> and will be completed in:</p>" + 
							  "<p class='centered'><code>" + results.data.ETA + "</code> (estimate)</p>" +
							  "<p>We do apologize for any sluggishness you will experience when using the VRMF Vessel Browser during this time period.</p>" + 
							  "<p style='text-align: right; padding-right: 2em'><em>The VRMF Vessel Browser administrators</em></p></div>";

				$$.Interface.warn($(content), "Important notice");
			}
		}
	});
};

/** USELESS? */
function updateHeaderAccordingToEntryPoint(updateTitle) {
	if($$.isSet(_SPECIFIC_SOURCE)) {
		//Do nothing...
	} else {
		$("#sourceLogo [title], #sourceVersion").attr("title", null).empty();
		$("#sourcesSearch, #liveSources, #liveSourcesHeading").show();
	}
}; 

function installFooterWidget() {
	$("#footer").dblclick(function() {
		$$.TooltipManager.close();
		
		var $this = $(this);
		var expanded = $this.is(".expanded");
		
		$this.toggleClass("expanded");
		$(".state .ui-icon", $this).toggleClass("hidden");
		
		if(expanded) 
			$this.animate({ height: $this.css("min-height") });
		else 
			$this.animate({ height: $("#footerContent").height() + 2 });
	});
};

function installShareWidget() {
	$("#shareContainer").draggable({ 
		axis: 'x',
		cursor: 'move', 
		handle: '.shareHandle',
		containment: 'parent',
		drag: function() {
			$$.TooltipManager.close($(".shareHandle .vrmf-tooltipped"));
		}
	});

	$(".shareHandle .controls").click(function() {
		$$.TooltipManager.close($(".vrmf-tooltipped", $(this)));
		$('.shareHandle .controls .ui-icon').toggleClass('hidden');	
		$('#share').slideToggle();
	});
};
	
function installToggleableLegendWidget() {
	$("legend.toggleable").addClass("ui-state-default ui-corner-all").hover(function() {
		$(this).addClass("ui-state-hover");
	}, function() {
		$(this).removeClass("ui-state-hover");
	}).dblclick(function() {
		var $this = $(this);
		$this.next("ul, div").toggleClass("hidden");
		$this.toggleClass("collapsed");

		if($$.UI.nicescrollEnabled)
			$this.trigger("resize.ui");
		
		storeInterfaceStatus();
	});
};

function installExpandableLegendWidget() {
	$("legend.applyHeightToggler").each(function() {
		var expander = $("<span title='" + $$.I18n.getText("vrmf.common.interface.ui.expander.tip") + "' class='expander heightToggler left ui-icon ui-icon-carat-1-n'/>");
		var collapser = $("<span title='" + $$.I18n.getText("vrmf.common.interface.ui.collapser.tip") + "' class='collapser heightToggler left ui-icon ui-icon-carat-1-s'/>");

		expander.click(function() {
			$(this).parents("fieldset").addClass("expanded");
			
			_DELAY_EXECUTION(250, _NICESCROLL_COLOR_UPDATER);
		});

		collapser.click(function() {
			$(this).parents("fieldset").removeClass("expanded");
		});

		$(this).prepend(expander).prepend(collapser);
	});
};
	
function installPinnableLegendWidget() {
	$("legend.pinnable").each(function() {
		var pinner = $("<span title='" + $$.I18n.getText("vrmf.common.interface.ui.pinner.tip") + "' class='pinner heightToggler right ui-icon ui-icon-pin-w'/>");
		var unpinner = $("<span title='" + $$.I18n.getText("vrmf.common.interface.ui.unpinner.tip") + "' class='unpinner heightToggler right ui-icon ui-icon-pin-s'/>");

		pinner.click(function() {
			$(this).parents("fieldset").addClass("pinned");
			
			_DELAY_EXECUTION(250, _NICESCROLL_COLOR_UPDATER);
		});

		unpinner.click(function() {
			$(this).parents("fieldset").removeClass("pinned");
			
			var searchResults = $("#searchResults");
			var header = $("#commonHeader");
			var position = searchResults.css("position");
			var vScroll = $(window).scrollTop();
			
			var top = position == 'fixed' ? header.outerHeight() + 8 - vScroll : 0;
			
			searchResults.animate({
				top: top
			}, 2000, function() {
				searchResults.css({
					position: 'relative',
					top: 0,
					left: 0
				});
			});
		});

		$(this).prepend(pinner).prepend(unpinner);
	});
};


function installLegendWidget() {
	installToggleableLegendWidget();
	installExpandableLegendWidget();
	installPinnableLegendWidget();
};

function installWidgets() {
	installThemeSelector();
	installFooterWidget();
//	installShareWidget();
	installLegendWidget();
};

function installNicescroll() {
	if($$.UI.nicescrollEnabled) {
		var defaultNSOptions = { 
			cursorWidth: 10,
			scrollspeed: 0,
			grabcursorenabled: true
		};
		
		$(".scrollable").niceScroll($.extend(true, {}, defaultNSOptions, {
			zindex: 1,
			autohidemode: false
		}));
		
		$("html").addClass("nicescrollEnabled").niceScroll($.extend(true, {}, defaultNSOptions, {
			zindex: 10
		}));
	
		$("html, .scrollable").bind("resize", _NICESCROLL_UPDATER).
							   bind("resize.ui", _NICESCROLL_UPDATER);
	}
};

function installPNotify() {
	if($$.UI.pnotifyEnabled) {
		$.pnotify.defaults.styling = "jqueryui";
		$.pnotify.defaults.history = false;
		$.pnotify.defaults.sticker = false;
		$.pnotify.defaults.mouseReset = false;
		$.pnotify.defaults.maxonscreen = 5;
	}
};

function installExtensions() {
	installNicescroll();
	installPNotify();
	
	//As we have redefined the error pages, this must override the default Tomcat Error Pages parser
	$$.Ajax.setErrorParser({
		name: 'Null parser',
		parse: function() { return null; }
	});
};

function configureTooltips(configuration) {
	var tConf;
	var target;
	for(var t=0; t<configuration.length; t++) {
		tConf = configuration[t];
		
		target = $(tConf.selector);

		$$.Log.debug("Configuring tooltips for " + tConf.selector + " with " + JSON.stringify(tConf.config));

		target.each(function() {
			var $this = $(this);
			
			if(!$$.isSet($this.attr($$.TooltipManager.TOOLTIP_CONFIGURATION_ATTRIBUTE)))
				$this.attr($$.TooltipManager.TOOLTIP_CONFIGURATION_ATTRIBUTE, JSON.stringify(tConf.config));
		});
	}
};

function sanitizeEMailLinks() {
	$("a[href^='mailto:']").each(function() {
		$$.Utils.sanitizeEMailLink($(this));
	});
};

function showCredits() {
	var creditsSourcesListContainer = $("#creditsSourcesList");

	if(!creditsSourcesListContainer.hasClass("initialized")) {
		var sourceItem;
		var currentSource;

		var availableSources = getAvailableVesselSources();
		
		for(var u=0; u<availableSources.length; u++) {
			currentSource = availableSources[u];

			sourceItem = $("<li/>");
			sourceItem.append("<a class=\"external { target:'_" + currentSource.id + "' }\">" +
								"<img src=\"../cdn/media/images/decorations/sources/logo/" + currentSource.id + ".png\" class=\"sourceLogo\" alt=\"" + currentSource.id + "\"/>" +
							  "</a>");

			$("img", sourceItem).attr('title', currentSource.id + " - " + $$.Metadata.SYSTEMS_MAP[currentSource.id].name);
			$("a", sourceItem).attr('href', $$.Metadata.SYSTEMS_MAP[currentSource.id].website);

			creditsSourcesListContainer.append(sourceItem);
		}

		creditsSourcesListContainer.addClass("initialized");
	}
	
	$("#applicationCredits").dialog({ 
		show: { effect: "fade", duration: 200 },
		modal: true, 
		maxHeight: 700,
		width: 640,
		resizable: false, 
		title: "Credits"
	});

	return false;
};

function showFAQs() {
	var text = $("<div><p>We are currently working to provide a detailed set of FAQs. Please, check again in the next few days.</p></div>");
	$$.Interface.warn(text);
	return false;
};

function hideAllPanels(callback) {
	$$.TooltipManager.close();
		
	$(".fieldAutocompletes").each(function() {
		var $this = $(this);
		
		if($$.UI.isAutocomplete($this))
			$this.autocomplete("close");
	});

	$(".panel").addClass("hidden");
};

function showVesselsDataPanel() {
	hideAllPanels();

	$("#searchCriteria").addClass("hidden");
	$("#vesselsData").removeClass("hidden");

	return false;
};

function updateSourceImages() {
	$("img.sourceIcon.fill").each(function() {
		var $this = $(this);
		var name = $this.attr("name");
		
		$this.attr("src", $($$.Sources.getSourceIconByID(name)).attr("src"));
	});
};

function showMainContainer() {
	$("#mainContainer").fadeIn($$.Browser.msie  ? 0 : "fast", function() {
		$(this).removeClass("hidden");
	});
};

function initializeCommonInterface(callback) {
	var onComplete = function() {
		configureTooltips([ 
							{ selector: "#FAOFisheryHeader [title]", config: TOOLTIP_HEADER_DEFAULTS },
							{ selector: "#pageHeader [title]", config: TOOLTIP_DEFAULTS },
							{ selector: "#footer", config: $.extend(true, {}, TOOLTIP_FOOTER_DEFAULTS, $$.TooltipManager.MY_CENTER_BOTTOM_DEFAULTS, $$.TooltipManager.AT_CENTER_TOP_DEFAULTS) },
							{ selector: "#footer [title]", config: $.extend(true, {}, TOOLTIP_FOOTER_DEFAULTS, { show: { solo: true } }) },
							{ selector: "#applicationCredits [title]", config: TOOLTIP_CREDITS_DEFAULTS }
						  ]);	
		
		showMainContainer();
		
		enableInputs();
	};
	
	try {
		$$.UI.install();
		
		$(".displayOnInitialization").fadeIn(500);

		if(isBrowserCompatible()) {
			var lockOnStartup = $("body").hasClass("lockOnStartup");
			
			if(lockOnStartup) {
				disableInputs($$.I18n.getText("vrmf.common.interface.initializing"));
			}

			installExtensions();
			installWidgets();

			applyStoredTheme();

			sanitizeEMailLinks();

			var wrappedCallback = function() {
				if($$.isSet(callback))
					callback();
				
				onComplete();
			};
			
			loadMetadataAndUserDetails(wrappedCallback);
		}
	} catch (E) {
		$$.Log.error(E);
		
		onComplete();
	}
};