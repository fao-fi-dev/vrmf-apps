function addAutocompletion() {
	addVesselTypeAutocompletion();
	addGearTypeAutocompletion();

	addVesselIdentifierAutocompletion();

	addVesselNameAutocompletion();

	addVesselExternalMarkingsAutocompletion();

	addVesselIRCSAutocompletion();
	addVesselMMSIAutocompletion();

	addVesselRegistrationPortAutocompletion();
	addVesselRegistrationNumberAutocompletion();

	addVesselFishingLicenseAutocompletion();
		
	$("span").filter("[role='status']").remove();
};

function postProcessAutocompletionURL(URL) {
	if($$.isSet(URL)) {
		var loggedUser = $$.UserManager.getLoggedUser();
		
		URL += (URL.endsWith("?") ? "" : "?" ) + "uname=" + ( $$.isSet(loggedUser) ? loggedUser.getId() : "_NOT_SET" );
	}
	
	return URL;	
};

function addVesselIdentifierAutocompletion() {
	var identifier = $("#identifier");
	
	$$.UI.destroyAutocomplete(identifier);

	identifier.unbind("dblclick").dblclick(function() {
		identifier.autocomplete("search");
	});

	identifier.autocomplete($.extend({}, UI_AUTOCOMPLETE_DEFAULTS, {
		minLength: 1,
		delay: 50,
		source: function( request, response ) {
			var queryData = $.extend({}, getCommonAutocompletionParameters(), {
				q: encodeURI(request.term.replace(/\./g, '_'))
			});

			var identifierType = getMultiselectedValues($("#identifierType"))[0];

			if($$.isSet(identifierType))
				queryData.it = identifierType;

			var isUID = $$.isSet(queryData.it) && "UID" == queryData.it;
			var isTUVI = $$.isSet(queryData.it) && "TUVI" == queryData.it;
			var isOtherID = !isUID & !isTUVI;

			if($$.isSet(queryData.it)) {
				if(!isOtherID) { 
					var IDAsNumber = parseInt(request.term, 10);

					if(!isNaN(IDAsNumber))
						queryData.q = encodeURI("" + IDAsNumber);
					else
						queryData.q = null;
				}
			}

			if(!$$.isSet(request.term) || request.term.hasWildcards()) {
				haltAutocompletion($(this));
				identifier.removeClass("ui-autocomplete-loading");
				response([]);
			} else {
				var token = new Date().getTime();
				_AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE = token;

				var customHeaders = {};
				customHeaders[_VRMF_SEARCH_REQUEST_TOKEN_HEADER] = token;

				$.ajax($.extend({ headers: customHeaders }, UI_AJAX_AUTOCOMPLETE_DEFAULTS, {
					url: postProcessAutocompletionURL(
							_WRAP_URL(_VRMF_BROWSER_AUTOCOMPLETION_SERVICES_URL_PREFIX) +
							( $$.isSet(queryData.s) ? "sources/" + queryData.s.join(",") + "/" : "" ) +
							( $$.isSet(queryData.l) ? "max/" + queryData.l + "/" : "" ) + 
							( isOtherID ? "identifier/" : "" ) + queryData.it + "/" + 
							( queryData.q + ".json" )
					),
					success: function(results, textStatus, jqXHR) {
						var returnedToken = jqXHR.getResponseHeader(_VRMF_SEARCH_REQUEST_TOKEN_HEADER);

						if(returnedToken == null || returnedToken != _AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE) {
							$$.Log.warn("Skipping autocompletion result display as it was related to a previous request (response token header: " + returnedToken + ", last request token: " + _AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE);
						} else {
							$(this).removeClass("selected");

							var filteredData = [];

							if($$.isSet(results.data)) { 
								for(var d=0; d<results.data.length; d++) {
									filteredData.push(results.data[d]);
								}
							}

							response($.map(filteredData, function(item) {
								var postprocessor = function(term) { return term; };

								if("TUVI" === item.identifierType)
									postprocessor = function(term) { 
										while(term.length < 7)
											term = "0" + term;

										return term;
									};

								var label = formatVesselIdentifierItem(item, request.term, postprocessor);

								return {
									label: label,
									value: postprocessor(item.identifier),
									identifier: item
								};
							}));
						}
					}
				}));
			}
		}
	}));

	identifier.autocomplete("enable");
};

function addVesselNameAutocompletion() {
	var name = $("#name");
	
	$$.UI.destroyAutocomplete(name);

	var enabled = $("option:selected", $("#nameLike")).hasClass("addAutocompletion");

	if(enabled) {
		name.attr("title", $$.I18n.getText("vrmf.search.controls.autocompletion.tip")).addClass("fieldAutocompletes");
	} else {
		name.attr("title", $$.I18n.getText("vrmf.search.controls.no.autocompletion.tip"));

		$$.TooltipManager.destroy(name);
	}

	name.unbind("dblclick").dblclick(function() {
		if($("option:selected", $("#nameLike")).hasClass("addAutocompletion"))
			name.autocomplete("search");
	});

	name.autocomplete($.extend({}, UI_AUTOCOMPLETE_DEFAULTS, {
		delay: 200,
		minLength: 3,
		source: function( request, response ) {
			var queryData = $.extend({}, getCommonAutocompletionParameters(), {
				q: encodeURI(request.term.replace(/\./g, '_'))
			});

			if(!$$.isSet(request.term) || request.term.hasWildcards()) {
				haltAutocompletion($(this));
				name.removeClass("ui-autocomplete-loading");
				response([]);
			} else {
				var token = new Date().getTime();
				_AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE = token;

				var customHeaders = {};
				customHeaders[_VRMF_SEARCH_REQUEST_TOKEN_HEADER] = token;

				$.ajax($.extend({ headers: customHeaders }, UI_AJAX_AUTOCOMPLETE_DEFAULTS, {
					url: postProcessAutocompletionURL(
							_WRAP_URL(_VRMF_BROWSER_AUTOCOMPLETION_SERVICES_URL_PREFIX) + 
							( $$.isSet(queryData.s) ? "sources/" + queryData.s.join(",") + "/" : "" ) + 
							( queryData.c ? "current" : 
								$$.isSet(queryData.ad) ? "at/" + queryData.ad : 
														 "historical" ) + "/" +  
							( $$.isSet(queryData.l) ? "max/" + queryData.l + "/" : "" ) +
							( "name/" ) +
							( queryData.q + ".json")
					),
					success: function(results, textStatus, jqXHR) {
						var returnedToken = jqXHR.getResponseHeader(_VRMF_SEARCH_REQUEST_TOKEN_HEADER);

						if(returnedToken == null || returnedToken != _AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE) {
							$$.Log.warn("Skipping autocompletion result display as it was related to a previous request (response token header: " + returnedToken + ", last request token: " + _AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE);
						} else {
							$(this).removeClass("selected");

							response($.map(results.data, function(item) {
								var label = formatVesselNameItem(item, request.term);

								return {
									label: label,
									value: item.name,
									name: item
								};
							}));
						}
					}
				}));
			}
		}
	}));

	name.autocomplete(enabled ? "enable" : "disable");
};

function addVesselExternalMarkingsAutocompletion() {
	var extMark = $("#extMark");
	
	$$.UI.destroyAutocomplete(extMark);

	extMark.unbind("dblclick").dblclick(function() {
		extMark.autocomplete("search");
	});

	extMark.autocomplete($.extend({}, UI_AUTOCOMPLETE_DEFAULTS, {
		delay: 50,
		minLength: 1,
		source: function( request, response ) {
			var queryData = $.extend({}, getCommonAutocompletionParameters(), {
				q: encodeURI(request.term.replace(/\./g, '_'))
			});

			if(!$$.isSet(request.term) || request.term.hasWildcards()) {
				haltAutocompletion($(this));
				extMark.removeClass("ui-autocomplete-loading");
				response([]);
			} else {
				var token = new Date().getTime();
				_AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE = token;

				var customHeaders = {};
				customHeaders[_VRMF_SEARCH_REQUEST_TOKEN_HEADER] = token;

				$.ajax($.extend({ headers: customHeaders }, UI_AJAX_AUTOCOMPLETE_DEFAULTS, {
					url: postProcessAutocompletionURL(
							_WRAP_URL(_VRMF_BROWSER_AUTOCOMPLETION_SERVICES_URL_PREFIX) + 
							( $$.isSet(queryData.s) ? "sources/" + queryData.s.join(",") + "/" : "" ) + 
							( queryData.c ? "current" : 
								$$.isSet(queryData.ad) ? "at/" + queryData.ad : 
														 "historical" ) + "/" + 
							( $$.isSet(queryData.l) ? "max/" + queryData.l + "/" : "" ) +
							( "externalMarkings/" ) +
							( queryData.q + ".json")
					),
					success: function(results, textStatus, jqXHR) {
						var returnedToken = jqXHR.getResponseHeader(_VRMF_SEARCH_REQUEST_TOKEN_HEADER);

						if(returnedToken == null || returnedToken != _AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE) {
							$$.Log.warn("Skipping autocompletion result display as it was related to a previous request (response token header: " + returnedToken + ", last request token: " + _AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE);
						} else {
							$(this).removeClass("selected");

							response($.map(results.data, function(item) {
								var label = formatVesselExternalMarkingItem(item, request.term);

								return {
									label: label,
									value: item.externalMarking,
									externalMarking: item
								};
							}));
						}
					}
				}));
			}
		}
	}));
};

function addVesselIRCSAutocompletion() {
	var IRCS = $("#ircs");
	
	$$.UI.destroyAutocomplete(IRCS);

	IRCS.unbind("dblclick").dblclick(function() {
		IRCS.autocomplete("search");
	});

	IRCS.autocomplete($.extend({}, UI_AUTOCOMPLETE_DEFAULTS, {
		delay: 50,
		minLength: 1,
		source: function( request, response ) {
			var queryData = $.extend({}, getCommonAutocompletionParameters(), {
				q: encodeURI(request.term.replace(/\./g, '_'))
			});

			if(!$$.isSet(request.term) || request.term.hasWildcards()) {
				haltAutocompletion($(this));
				IRCS.removeClass("ui-autocomplete-loading");
				response([]);
			} else {
				var token = new Date().getTime();
				_AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE = token;

				var customHeaders = {};
				customHeaders[_VRMF_SEARCH_REQUEST_TOKEN_HEADER] = token;

				$.ajax($.extend({ headers: customHeaders }, UI_AJAX_AUTOCOMPLETE_DEFAULTS, {
					url: postProcessAutocompletionURL(
							_WRAP_URL(_VRMF_BROWSER_AUTOCOMPLETION_SERVICES_URL_PREFIX) + 
							( $$.isSet(queryData.s) ? "sources/" + queryData.s.join(",") + "/" : "" ) + 
							( queryData.c ? "current" : 
								$$.isSet(queryData.ad) ? "at/" + queryData.ad : 
														 "historical" ) + "/" + 
									( $$.isSet(queryData.l) ? "max/" + queryData.l + "/" : "" ) +
									( "ircs/" ) +
						( queryData.q + ".json")
					),
					success: function(results, textStatus, jqXHR) {
						var returnedToken = jqXHR.getResponseHeader(_VRMF_SEARCH_REQUEST_TOKEN_HEADER);

						if(returnedToken == null || returnedToken != _AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE) {
							$$.Log.warn("Skipping autocompletion result display as it was related to a previous request (response token header: " + returnedToken + ", last request token: " + _AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE);
						} else {
							$(this).removeClass("selected");

							response($.map(results.data, function(item) {
								var label = formatVesselIRCSItem(item, request.term);

								return {
									label: label,
									value: item.IRCS,
									IRCS: item
								};
							}));
						}
					}
				}));
			}
		}
	}));
};

function addVesselMMSIAutocompletion() {
	var MMSI = $("#mmsi");

	$$.UI.destroyAutocomplete(MMSI);
	
	MMSI.unbind("dblclick").dblclick(function() {
		MMSI.autocomplete("search");
	});

	MMSI.autocomplete($.extend({}, UI_AUTOCOMPLETE_DEFAULTS, {
		delay: 50,
		minLength: 1,
		source: function( request, response ) {
			var queryData = $.extend({}, getCommonAutocompletionParameters(), {
				q: encodeURI(request.term.replace(/\./g, '_'))
			});

			if(!$$.isSet(request.term) || request.term.hasWildcards()) {
				haltAutocompletion($(this));
				MMSI.removeClass("ui-autocomplete-loading");
				response([]);
			} else {
				var token = new Date().getTime();
				_AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE = token;

				var customHeaders = {};
				customHeaders[_VRMF_SEARCH_REQUEST_TOKEN_HEADER] = token;

				$.ajax($.extend({ headers: customHeaders }, UI_AJAX_AUTOCOMPLETE_DEFAULTS, {
					url: postProcessAutocompletionURL(
							_WRAP_URL(_VRMF_BROWSER_AUTOCOMPLETION_SERVICES_URL_PREFIX) + 
							( $$.isSet(queryData.s) ? "sources/" + queryData.s.join(",") + "/" : "" ) + 
							( queryData.c ? "current" : 
								$$.isSet(queryData.ad) ? "at/" + queryData.ad : 
														 "historical" ) + "/" + 
							( $$.isSet(queryData.l) ? "max/" + queryData.l + "/" : "" ) +
							( "mmsi/" ) +
							( queryData.q + ".json")
					),
					success: function(results, textStatus, jqXHR) {
						var returnedToken = jqXHR.getResponseHeader(_VRMF_SEARCH_REQUEST_TOKEN_HEADER);

						if(returnedToken == null || returnedToken != _AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE) {
							$$.Log.warn("Skipping autocompletion result display as it was related to a previous request (response token header: " + returnedToken + ", last request token: " + _AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE);
						} else {
							$(this).removeClass("selected");

							response($.map(results.data, function(item) {
								var label = formatVesselMMSIItem(item, request.term);

								return {
									label: label,
									value: item.MMSI,
									MMSI: item
								};
							}));
						}
					}
				}));
			}
		}
	}));
};

function addVesselFishingLicenseAutocompletion() {
	var fishingLicense = $("#fishingLicense");

	$$.UI.destroyAutocomplete(fishingLicense);

	fishingLicense.unbind("dblclick").dblclick(function() {
		fishingLicense.autocomplete("search");
	});

	fishingLicense.autocomplete($.extend({}, UI_AUTOCOMPLETE_DEFAULTS, {
		delay: 50,
		minLength: 1,
		source: function( request, response ) {
			var queryData = $.extend({}, getCommonAutocompletionParameters(), {
				q: encodeURI(request.term.replace(/\./g, '_'))
			});

			var fishingLicenseCountries = getMultiselectedValues($("#fishingLicenseCountry"));

			if($$.isSet(fishingLicenseCountries) && fishingLicenseCountries.length > 0) {
				queryData.flc = new Array();

				var country = null;
				for(var c=0; c<fishingLicenseCountries.length; c++) {
					country = $$.Metadata.getCountryByID(fishingLicenseCountries[c]);

					if(country != null && $$.isSet(country.iso2Code))
						queryData.flc.push(country.iso2Code);
				}

				if($$.isSet(queryData.flc) && queryData.flc.length == 0)
					delete queryData.flc;
			}

			if(!$$.isSet(request.term) || request.term.hasWildcards()) {
				haltAutocompletion($(this));
				fishingLicense.removeClass("ui-autocomplete-loading");
				response([]);
			} else {
				var token = new Date().getTime();
				_AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE = token;

				var customHeaders = {};
				customHeaders[_VRMF_SEARCH_REQUEST_TOKEN_HEADER] = token;

				$.ajax($.extend({ headers: customHeaders }, UI_AJAX_AUTOCOMPLETE_DEFAULTS, {
					url: postProcessAutocompletionURL(
							_WRAP_URL(_VRMF_BROWSER_AUTOCOMPLETION_SERVICES_URL_PREFIX) + 
							( $$.isSet(queryData.s) ? "sources/" + queryData.s.join(",") + "/" : "" ) + 
							( queryData.c ? "current" : 
								$$.isSet(queryData.ad) ? "at/" + queryData.ad : 
														 "historical" ) + "/" + 
							( $$.isSet(queryData.l) ? "max/" + queryData.l + "/" : "" ) +
							( "fishingLicense/" ) +
							( $$.isSet(queryData.flc) ? "countries/" + queryData.flc.join(",") + "/" : "" ) + 
							( "number/" ) +
							( queryData.q + ".json")
					),
					success: function(results, textStatus, jqXHR) {
						var returnedToken = jqXHR.getResponseHeader(_VRMF_SEARCH_REQUEST_TOKEN_HEADER);

						if(returnedToken == null || returnedToken != _AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE) {
							$$.Log.warn("Skipping autocompletion result display as it was related to a previous request (response token header: " + returnedToken + ", last request token: " + _AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE);
						} else {
							response($.map(results.data, function(item) {
								var label = formatVesselFishingLicenseItem(item, request.term);

								return {
									label: label,
									value: item.licenseId,
									regNo: item
								};
							}));
						}
					}
				}));
			}
		}
	}));
};

function addVesselRegistrationNumberAutocompletion() {
	var normalSearch = isNormalSearch();
	var regNo = $("#regNo");

	$$.UI.destroyAutocomplete(regNo);

	regNo.unbind("dblclick").dblclick(function() {
		regNo.autocomplete("search");
	});

	regNo.autocomplete($.extend({}, UI_AUTOCOMPLETE_DEFAULTS, {
		delay: 50,
		minLength: 1,
		source: function( request, response ) {
			var queryData = $.extend({}, getCommonAutocompletionParameters(), {
				q: encodeURI(request.term.replace(/\./g, '_'))
			});

			var regCountries = getMultiselectedValues($("#regCountry"));

			if($$.isSet(regCountries) && regCountries.length > 0) {
				queryData.pc = new Array();

				var country = null;
				for(var c=0; c<regCountries.length; c++) {
					country = $$.Metadata.getCountryByID(regCountries[c]);

					if(country != null && $$.isSet(country.iso2Code))
						queryData.pc.push(country.iso2Code);
				}

				if($$.isSet(queryData.pc) && queryData.pc.length == 0)
					delete queryData.pc;
			}

			var regPorts = normalSearch ? null : new $$.Set();

			if(!normalSearch) {
				$("#regPortCodes li span.codesHolder").each(function() {
					var codes = $(this).data("codes");

					var codesAsArray = codes.split(",");

					for(var c=0; c<codesAsArray.length; c++) {
						regPorts.add(parseInt(codesAsArray[c], 10));
					}
				});

				regPorts = regPorts.asArray();
			}

			if($$.isSet(regPorts))
				queryData.p = regPorts.join(",");

			if(!$$.isSet(request.term) || request.term.hasWildcards()) {
				haltAutocompletion($(this));
				regNo.removeClass("ui-autocomplete-loading");
				response([]);
			} else {
				var token = new Date().getTime();
				_AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE = token;

				var customHeaders = {};
				customHeaders[_VRMF_SEARCH_REQUEST_TOKEN_HEADER] = token;

				$.ajax($.extend({ headers: customHeaders }, UI_AJAX_AUTOCOMPLETE_DEFAULTS, {
					url: postProcessAutocompletionURL(
							_WRAP_URL(_VRMF_BROWSER_AUTOCOMPLETION_SERVICES_URL_PREFIX) + 
							( $$.isSet(queryData.s) ? "sources/" + queryData.s.join(",") + "/" : "" ) + 
							( queryData.c ? "current" : 
								$$.isSet(queryData.ad) ? "at/" + queryData.ad : 
														 "historical" ) + "/" + 
							( $$.isSet(queryData.l) ? "max/" + queryData.l + "/" : "" ) +
							( "registration/" ) +
							( $$.isSet(queryData.p) ? "port/" + queryData.p + "/" : 
								( $$.isSet(queryData.pc) ? "countries/" + queryData.pc.join(",") + "/" : "" ) ) + 
							( "number/" ) +
							( queryData.q + ".json")
					),
					success: function(results, textStatus, jqXHR) {
						var returnedToken = jqXHR.getResponseHeader(_VRMF_SEARCH_REQUEST_TOKEN_HEADER);

						if(returnedToken == null || returnedToken != _AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE) {
							$$.Log.warn("Skipping autocompletion result display as it was related to a previous request (response token header: " + returnedToken + ", last request token: " + _AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE);
						} else {
							response($.map(results.data, function(item) {
								var label = formatVesselRegistrationNumberItem(item, request.term);

								return {
									label: label,
									value: item.registrationNumber,
									regNo: item
								};
							}));
						}
					}
				}));
			}
		}
	}));
};

function addGearTypeAutocompletion() {
	doAddGearTypeAutocompletion($("#primaryGear"), $("#primaryGearTypeLabel"), $("#primaryGearTypeCodes"));
	doAddGearTypeAutocompletion($("#secondaryGear"), $("#secondaryGearTypeLabel"), $("#secondaryGearTypeCodes"));
}

function doAddGearTypeAutocompletion(target, label, codesHolder) {
	var gearType = target;

	$$.UI.destroyAutocomplete(gearType);
	
	gearType.attr("title", $$.I18n.getText("vrmf.search.controls.autocompletion.tip")).addClass("fieldAutocompletes");

	gearType.unbind("dblclick").dblclick(function() {
		gearType.autocomplete("search");
	});

	gearType.autocomplete($.extend({}, UI_AUTOCOMPLETE_DEFAULTS, {
		delay: 100,
		minLength: 2,
		source: function( request, response ) {
			var queryData = $.extend({}, getCommonAutocompletionParameters(), {
				q: encodeURI(request.term.replace(/\./g, '_'))
			});

			if(!$$.isSet(request.term) || request.term.hasWildcards()) {
				haltAutocompletion($(this));
				gearType.removeClass("ui-autocomplete-loading");
				response([]);
			} else {
				var token = new Date().getTime();
				_AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE = token;

				var customHeaders = {};
				customHeaders[_VRMF_SEARCH_REQUEST_TOKEN_HEADER] = token;

				$.ajax($.extend({ headers: customHeaders }, UI_AJAX_AUTOCOMPLETE_DEFAULTS, {
					url: postProcessAutocompletionURL(
							_WRAP_URL(_VRMF_BROWSER_AUTOCOMPLETION_SERVICES_URL_PREFIX) + 
							( $$.isSet(queryData.s) ? "sources/" + queryData.s.join(",") + "/" : "" ) + 
							( queryData.c ? "current" : 
								$$.isSet(queryData.ad) ? "at/" + queryData.ad : 
														 "historical" ) + "/" +  
							( $$.isSet(queryData.l) ? "max/" + queryData.l + "/" : "" ) +
							( groupCodes() ? "grouped/" : "" ) + "gear/type/" +
							( queryData.q + ".json")
					),
					success: function(results, textStatus, jqXHR) {
						var returnedToken = jqXHR.getResponseHeader(_VRMF_SEARCH_REQUEST_TOKEN_HEADER);

						if(returnedToken == null || returnedToken != _AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE) {
							$$.Log.warn("Skipping autocompletion result display as it was related to a previous request (response token header: " + returnedToken + ", last request token: " + _AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE);
						} else {
							$(this).removeClass("selected");

							response($.map(results.data, function(item) {
								var label = formatGearTypeItem(item, request.term);

								return {
									label: label,
									value: null, //item.name,
									type: item
								};
							}));
						}
					}
				}));
			}
		},
		select: function( event, ui ) {
			if(ui.item) {
				var type = ui.item.type;

				var separator = "<span> > </span>";

				var display = $("<span class='codesHolder'/>");
				display.append("<span class=\"codeSource\" title=\"" + $$.I18n.getText("search.controls.autocompletion.result.source.tip") + "\">" + type.sourceSystem + "</span>");
				display.append(separator);

				if($$.isSet(type.originalGearTypeCode)) {
					display.append("<span class=\"codeOriginal\" title=\"" + $$.I18n.getText("search.controls.autocompletion.result.code.original.tip") + "\">" + type.originalGearTypeCode + "</span>");
					display.append(separator);
				}

				var title = "";

				display.append("<span class='codeName'>" + decodeURI(type.name) + "</span>");

				var currentTitle = decodeURI(type.name);

				title += currentTitle;

				if($$.isSet(type.mappedTypes)) {
					currentTitle = $$.I18n.getText("search.controls.autocompletion.result.code." + ( type.mappedTypes.length > 1 ? "others" : "other"), type.mappedTypes.length);

					title += currentTitle;

					display.append("<span class='codeMapped'>" + currentTitle + "</span>");
				}

				$(".codeName", display).attr("title", title);

				var typeCodes = [ type.id ];

				if(groupCodes() && $$.isSet(type.mappedTypes))
					for(var t=0; t<type.mappedTypes.length; t++)
						typeCodes.push(type.mappedTypes[t].id);

				var selectedCodes = typeCodes.join(",");
				var currentCodes = null;

				var alreadySelected = false;

				$("li", codesHolder).each(function() {
					var $this = $(this);
					var data = $("span.codesHolder", $this);

					currentCodes = data.data("codes");

					if($$.isSet(currentCodes) && currentCodes === selectedCodes)
						alreadySelected = true;
				});

				if(!alreadySelected) {
					display.data("codes", selectedCodes);
					display.data("mappedCodes", formatGearTypeItem(type));

					commonAutocompletionSelectionHandler(display, codesHolder);
				}

				target.blur();
			} else {
				resetMultipleCodes(gearType, $(codesHolder));
			}
		},
		change: function( event, ui ) {
			if($(target).val() == "") {
				resetMultipleCodes(gearType, $(codesHolder));
			}
		}
	}));
};

function addVesselTypeAutocompletion() {
	var vesselType = $("#type");
	
	$$.UI.destroyAutocomplete(vesselType);
	
	vesselType.attr("title", $$.I18n.getText("vrmf.search.controls.autocompletion.tip")).addClass("fieldAutocompletes");

	vesselType.unbind("dblclick").dblclick(function() {
		vesselType.autocomplete("search");
	});

	vesselType.autocomplete($.extend({}, UI_AUTOCOMPLETE_DEFAULTS, {
		delay: 100,
		minLength: 2,
		source: function( request, response ) {
			var queryData = $.extend({}, getCommonAutocompletionParameters(), {
				q: encodeURI(request.term.replace(/\./g, '_'))
			});

			if(!$$.isSet(request.term) || request.term.hasWildcards()) {
				haltAutocompletion($(this));
				vesselType.removeClass("ui-autocomplete-loading");
				response([]);
			} else {
				var token = new Date().getTime();
				_AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE = token;

				var customHeaders = {};
				customHeaders[_VRMF_SEARCH_REQUEST_TOKEN_HEADER] = token;

				$.ajax($.extend({ headers: customHeaders }, UI_AJAX_AUTOCOMPLETE_DEFAULTS, {
					_lastRequestToken: function() { return _AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE; },
					url: postProcessAutocompletionURL(
							_WRAP_URL(_VRMF_BROWSER_AUTOCOMPLETION_SERVICES_URL_PREFIX) + 
							( $$.isSet(queryData.s) ? "sources/" + queryData.s.join(",") + "/" : "" ) + 
							( queryData.c ? "current" : 
								$$.isSet(queryData.ad) ? "at/" + queryData.ad : 
														 "historical" ) + "/" +  
							( $$.isSet(queryData.l) ? "max/" + queryData.l + "/" : "" ) +
							( groupCodes() ? "grouped/" : "" ) + "vessel/type/" +
							( queryData.q + ".json")
					),
					success: function(results, textStatus, jqXHR) {
						var returnedToken = jqXHR.getResponseHeader(_VRMF_SEARCH_REQUEST_TOKEN_HEADER);

						if(returnedToken == null || returnedToken != _AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE) {
							$$.Log.warn("Skipping autocompletion result display as it was related to a previous request (response token header: " + returnedToken + ", last request token: " + _AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE);
						} else {
							$(this).removeClass("selected");

							response($.map(results.data, function(item) {
								var label = formatVesselTypeItem(item, request.term);

								return {
									label: label,
									value: null, //item.name,
									type: item
								};
							}));
						}
					}
				}));
			}
		},
		select: function( event, ui ) {
			if(ui.item) {
				var type = ui.item.type;

				var countryDependant = $$.isSet(type.countryId);

				var country = countryDependant ? $$.Metadata.getCountryByID(type.countryId) : null;

				var separator = "<span> > </span>";

				var display = $("<span class='codesHolder'/>");
				display.append("<span class=\"codeSource\" title=\"" + $$.I18n.getText("search.controls.autocompletion.result.source.tip") + "\">" + type.sourceSystem + "</span>");
				display.append(separator);

				if($$.isSet(country)) {
					display.append("<span class='codeCountry'>" + $$.Flags.getImage(country) + "</span>");
					display.append(separator);
				}

				if($$.isSet(type.originalVesselTypeId)) {
					display.append("<span class=\"codeOriginal\" title=\"" + $$.I18n.getText("search.controls.autocompletion.result.code.original.tip") + "\">" + type.originalVesselTypeId + "</span>");
					display.append(separator);
				}

				var title = "";

				display.append("<span class='codeName'>" + decodeURI(type.name) + "</span>");

				var currentTitle = decodeURI(type.name);

				title += currentTitle;

				if($$.isSet(type.mappedTypes)) {
					currentTitle = $$.I18n.getText("search.controls.autocompletion.result.code." + ( type.mappedTypes.length > 1 ? "others" : "other"), type.mappedTypes.length);

					title += currentTitle;

					display.append("<span class='codeMapped'>" + currentTitle + "</span>");
				}

				$(".codeName", display).attr("title", title);

				var typeCodes = [ type.id ];

				if(groupCodes() && $$.isSet(type.mappedTypes))
					for(var t=0; t<type.mappedTypes.length; t++)
						typeCodes.push(type.mappedTypes[t].id);

				var selectedCodes = typeCodes.join(",");
				var currentCodes = null;

				var alreadySelected = false;

				$("#vesselTypeCodes li").each(function() {
					var $this = $(this);
					var data = $("span.codesHolder", $this);

					currentCodes = data.data("codes");

					if($$.isSet(currentCodes) && currentCodes === selectedCodes)
						alreadySelected = true;
				});

				if(!alreadySelected) {
					var codesHolder = $("#vesselTypeCodes");

					display.data("codes", selectedCodes);
					display.data("mappedCodes", formatVesselTypeItem(type));

					commonAutocompletionSelectionHandler(display, codesHolder);
				}

				$("#type").blur();
			}
		}
	}));
};

function addVesselRegistrationPortAutocompletion() {
	var port = $("#regPort");
	
	$$.UI.destroyAutocomplete(port);

	port.unbind("dblclick").dblclick(function() {
		port.autocomplete("search");
	});

	port.autocomplete($.extend({}, UI_AUTOCOMPLETE_DEFAULTS, {
		delay: 50,
		minLength: 2,
		source: function( request, response ) {
			var queryData = $.extend({}, getCommonAutocompletionParameters(), {
				q: encodeURI(request.term.replace(/\./g, '_'))
			});

			var regCountries = getMultiselectedValues($("#regCountry"));

			if($$.isSet(regCountries) && regCountries.length > 0) {
				queryData.rc = new Array();

				var country = null;
				for(var c=0; c<regCountries.length; c++) {
					country = $$.Metadata.getCountryByID(regCountries[c]);

					if(country != null && $$.isSet(country.iso2Code))
						queryData.rc.push(country.iso2Code);
				}

				if($$.isSet(queryData.rc) && queryData.rc.length == 0)
					delete queryData.rc;
			}

			if(!$$.isSet(request.term) || request.term.hasWildcards()) {
				haltAutocompletion($(this));
				port.removeClass("ui-autocomplete-loading");
				response([]);
			} else {
				var token = new Date().getTime();
				_AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE = token;

				var customHeaders = {};
				customHeaders[_VRMF_SEARCH_REQUEST_TOKEN_HEADER] = token;

				$.ajax($.extend({ headers: customHeaders }, UI_AJAX_AUTOCOMPLETE_DEFAULTS, {
					url: postProcessAutocompletionURL(
							_WRAP_URL(_VRMF_BROWSER_AUTOCOMPLETION_SERVICES_URL_PREFIX) + 
							( $$.isSet(queryData.s) ? "sources/" + queryData.s.join(",") + "/" : "" ) + 
							( queryData.c ? "current" : 
								$$.isSet(queryData.ad) ? "at/" + queryData.ad : 
														 "historical" ) + "/" + 
							( $$.isSet(queryData.l) ? "max/" + queryData.l + "/" : "" ) +
							( "registration/" ) +
							( $$.isSet(queryData.rc) ? "countries/" + queryData.rc.join(",") + "/" : "" ) + 
							( groupCodes() ? "grouped/" : "" ) + 
							( "port/" ) +
							( queryData.q + ".json")
					),
					success: function(results, textStatus, jqXHR) {
						var returnedToken = jqXHR.getResponseHeader(_VRMF_SEARCH_REQUEST_TOKEN_HEADER);

						if(returnedToken == null || returnedToken != _AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE) {
							$$.Log.warn("Skipping autocompletion result display as it was related to a previous request (response token header: " + returnedToken + ", last request token: " + _AUTOCOMPLETE_LAST_REQUEST_TOKEN_VALUE);
						} else {
							var mapped = $.map(results.data, function(item) {
								return {
									label: formatVesselRegistrationPortItem(item, request.term),
									value: null, //item.name,
									port: item
								};
							});
							
							response(mapped);
						}
					}
				}));
			}
		},
		select: function( event, ui ) {
			if(ui.item) {
				var regPort = ui.item.port;

				var country = $$.Metadata.getCountryByID(regPort.countryId);

				var separator = "<span> > </span>";

				var display = $("<span class='codesHolder'/>");

				display.append("<span class=\"codeSource\" title=\"" + $$.I18n.getText("search.controls.autocompletion.result.source.tip") + "\">" + regPort.sourceSystem + "</span>");
				display.append(separator);

				if($$.isSet(country)) {
					display.append("<span class='codeCountry'>" + $$.Flags.getImage(country) + "</span>");
					display.append(separator);
				}

				if($$.isSet(regPort.originalId)) {
					display.append("<span class=\"codeOriginal\" title=\"" + $$.I18n.getText("search.controls.autocompletion.result.code.original.tip") + "\">" + regPort.originalId + "</span>");
					display.append(separator);
				}

				var title = "";

				display.append("<span class='codeName'>" + decodeURI(regPort.name) + "</span>");

				var currentTitle = decodeURI(regPort.name);

				title += currentTitle;

				if($$.isSet(regPort.mappedPorts)) {
					currentTitle = " + " + regPort.mappedPorts.length + " other" + ( regPort.mappedPorts.length > 1 ? "s" : "" );

					title += currentTitle;

					display.append("<span class='codeMapped'>" + currentTitle + "</span>");
				}

				$(".codeName", display).attr("title", title);

				var portCodes = [ regPort.id ];

				if(groupCodes() && $$.isSet(regPort.mappedPorts))
					for(var t=0; t<regPort.mappedPorts.length; t++)
						portCodes.push(regPort.mappedPorts[t].id);

				var selectedCodes = portCodes.join(",");
				var currentCodes = null;

				var alreadySelected = false;

				$("#regPortCodes li").each(function() {
					var $this = $(this);
					var data = $("span.codesHolder", $this);

					currentCodes = data.data("codes");

					if($$.isSet(currentCodes) && currentCodes === selectedCodes)
						alreadySelected = true;
				});

				if(!alreadySelected) {
					var codesHolder = $("#regPortCodes");

					display.data("codes", selectedCodes);
					display.data("mappedCodes", formatVesselRegistrationPortItem(regPort));

					commonAutocompletionSelectionHandler(display, codesHolder);
				}

				$("#regPort").blur();
			} else {
				resetMultipleCodes(port, $("#regPortCodes"));
			}
		}
	}));
};