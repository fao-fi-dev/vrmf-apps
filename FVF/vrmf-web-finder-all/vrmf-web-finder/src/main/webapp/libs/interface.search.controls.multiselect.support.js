var valueSelectionDisplay = function(numChecked, numTotal, checked) {
	var selected = $(checked[0]).val();
	
	if("none" != selected)
		return selected;
	
	return $(checked[0]).next("span").text();
};

var labelSelectionDisplay = function(numChecked, numTotal, checked) {
	return $(checked[0]).next("span").text();
};

var titleSelectionDisplay = function(numChecked, numTotal, checked) {
	return $(checked[0]).attr("title");
};

var _multipleSelectionDisplay = function(maxChecked, numChecked, numTotal, checked, maxLengthPerSelectedEntry, extractor) {
	var lengthPerEntry = $$.isSet(maxLengthPerSelectedEntry) ? maxLengthPerSelectedEntry : 5;
	
	if(numChecked > maxChecked)
		return $$.I18n.getText("vrmf.common.interface.ui.multiselect.num.selected", numChecked);
	
	var text = [];
	
	for(var c=0; c<checked.length; c++) {
		text.push($$.isSet(extractor) ? extractor($(checked[c])) : $(checked[c]).next("span").text());
	}
	
	var toReturn = text.join(", ");
	
	if(toReturn.length > maxChecked * lengthPerEntry)
		return toReturn.substring(0, maxChecked * lengthPerEntry) + "...";
	
	return toReturn;
};

var _multipleSelectionDisplayByValue = function(maxChecked, numChecked, numTotal, checked, maxLengthPerSelectedEntry) {
	var lengthPerEntry = $$.isSet(maxLengthPerSelectedEntry) ? maxLengthPerSelectedEntry : 5;
	
	if(numChecked > maxChecked)
		return $$.I18n.getText("vrmf.common.interface.ui.multiselect.num.selected", numChecked);
	
	var text = [];
	
	for(var c=0; c<checked.length; c++) {
		text.push($(checked[c]).val());
	}
	
	var toReturn = text.join(", ");
	
	if(toReturn.length > maxChecked * lengthPerEntry)
		return toReturn.substring(0, maxChecked * lengthPerEntry) + "...";
	
	return toReturn;
};

var defaultMultipleSelectionDisplay = function(numChecked, numTotal, checked) {
	return _multipleSelectionDisplay(2, numChecked, numTotal, checked);
};

var sourcesMultipleSelectionDisplay = function(numChecked, numTotal, checked) {
	return _multipleSelectionDisplay(3, numChecked, numTotal, checked, 6, function(checked) { return $(checked).val(); });
};

var statusMultipleSelectionDisplay = function(numChecked, numTotal, checked) {
	return _multipleSelectionDisplayByValue(5, numChecked, numTotal, checked);
};

var authTypesMultipleSelectionDisplay = function(numChecked, numTotal, checked) {
	return _multipleSelectionDisplayByValue(2, numChecked, numTotal, checked, 9);
};

var lazyLoadFlags = function(event, ui) {
	var $widget = $(event.target).multiselect("widget");
	
	$("img.flag", $widget).lazyload();	
};

var flagsSelectionDisplay = function(numChecked, numTotal, checked) {
	if(numChecked > 4)
		return $$.I18n.getText("vrmf.common.interface.ui.multiselect.num.selected", numChecked);
	
	var toReturn = [];
	
	var country = null;
	for(var c=0; c<checked.length; c++) {
		country = $$.Metadata.getCountryByID($(checked[c]).val());
		
		if($$.isSet(country)) {
			if($$.isSet(country.iso2Code))
				toReturn.push("&nbsp;" + $$.Flags.getImage(country) + "<code>" + country.iso2Code + "</code>");
			else
				return $$.I18n.getText("vrmf.common.interface.ui.multiselect.num.selected", numChecked);
		}
	}
	
	return $("<span>" + toReturn.join(", ") + "</span>");
};

var singleFlagSelectionDisplay = function(numChecked, numTotal, checked) {
	var toReturn = [];
	
	var country = null;
	for(var c=0; c<checked.length; c++) {
		country = $$.Metadata.getCountryByID($(checked[c]).val());
		
		if($$.isSet(country))
			toReturn.push($$.Flags.getImage(country) + "<code>" + country.iso2Code + " - " + country.name + "</code>");
	}
	
	return $("<span>" + toReturn.join(", ") + "</span>");
};

var MULTISELECT_POSITION = {
		my: 'left top',
		at: 'left bottom'
	};

var COMMON_FLAGS_LAZYLOADING_BINDINGS = function(element, multiselect) {
	var $widget = multiselect.multiselect("widget"); 
	
	$("ul", $widget).bind("scroll", function(event) {
		$("img.flag", $(this)).lazyload();
	});
	
	$("input[type='search']", $widget).bind("keyup", function(event) {
		$("img.flag", $widget).lazyload();
	});
};

var OPTIONS_BY_SELECT_MULTIPLE = {
	source: {
		classes: 'sourceMultiselect',
		noneSelectedTextLabel: "search.controls.ui.multiselect.all.sources",
		selectedText: sourcesMultipleSelectionDisplay
	},			
	flag: {
		classes: 'flagMultiselect',
		noneSelectedTextLabel: "search.controls.ui.multiselect.all.flags",
		selectedText: flagsSelectionDisplay,
		open: lazyLoadFlags,
		bindings: COMMON_FLAGS_LAZYLOADING_BINDINGS
	},
	status: {
		noneSelectedTextLabel: "search.controls.ui.multiselect.any",
		selectedText: statusMultipleSelectionDisplay
	},
	regCountry: {
		classes: 'flagMultiselect',
		noneSelectedTextLabel: "search.controls.ui.multiselect.all.countries",
		selectedText: flagsSelectionDisplay,
		open: lazyLoadFlags,
		bindings: COMMON_FLAGS_LAZYLOADING_BINDINGS
	},
	fishingLicenseCountry: {
		classes: 'flagMultiselect',
		noneSelectedTextLabel: "search.controls.ui.multiselect.all.countries",
		selectedText: flagsSelectionDisplay,
		open: lazyLoadFlags,
		bindings: COMMON_FLAGS_LAZYLOADING_BINDINGS
	},
	issuingAuthority: {
		classes: 'sourceMultiselect',
		noneSelectedTextLabel: "search.controls.ui.multiselect.please.select",
		selectedText: sourcesMultipleSelectionDisplay,
		close: updateAuthorizationTypes
	},
	authorizationType: {
		classes: 'authTypeMultiselect',
		noneSelectedTextLabel: "search.controls.ui.multiselect.please.select",
		selectedText: authTypesMultipleSelectionDisplay
	},
	reportingCountry: {
		classes: 'flagMultiselect',
		noneSelectedTextLabel: "search.controls.ui.multiselect.all.countries",
		selectedText: flagsSelectionDisplay,
		open: lazyLoadFlags,
		bindings: COMMON_FLAGS_LAZYLOADING_BINDINGS
	}/*,
	statsGearType: {
		noneSelectedTextLabel: 'All types',
		selectedText: defaultMultipleSelectionDisplay
	},
	statsCountry: {
		noneSelectedTextLabel: "search.controls.ui.multiselect.all.countries",
		selectedText: flagsSelectionDisplay
	},
	statsVesselType: {
		noneSelectedTextLabel: 'All types',
		selectedText: defaultMultipleSelectionDisplay
	} */	
};

var OPTIONS_BY_SELECT_SINGLE = {
	identifierType: {
		selectedText: valueSelectionDisplay
	},
	nameLike: {
		click: function(event, ui){
			if("is" != ui.value) {
				$("#name").removeClass("fieldAutocompletes").attr("title", $$.I18n.getText("vrmf.search.controls.no.autocompletion.tip")).autocomplete("disable");
				$$.TooltipManager.destroy($("#name"));
			} else {
				$("#name").addClass("fieldAutocompletes").attr("title", $$.I18n.getText("vrmf.search.controls.autocompletion.tip")).autocomplete("enable");
				$$.TooltipManager.apply($("#name"), TOOLTIP_SEARCH_FILTER_DEFAULTS);
			}
			
			$(this).multiselect("close");
		}
	},
	ageClass: {
		selectedText: labelSelectionDisplay
	},
	lengthType: {
		selectedText: valueSelectionDisplay,
		afterInitialization: null, 
		click: updateLengthClasses
	},
	tonnageType: {
		selectedText: valueSelectionDisplay,
		afterInitialization: null,
		click: updateTonnageClasses
	},
	mainEnginePowerType: {
		selectedText: valueSelectionDisplay,
		afterInitialization: null,
		close: updateMainEnginePowerClasses
	},
	auxEnginePowerType: {
		selectedText: valueSelectionDisplay,
		afterInitialization: null,
		close: updateAuxEnginePowerClasses
	},
	lengthClass: {
		selectedText: labelSelectionDisplay
	},
	tonnageClass: {
		selectedText: labelSelectionDisplay
	},
	mainEnginePowerClass: {
		selectedText: labelSelectionDisplay
	},
	auxEnginePowerClass: {
		selectedText: labelSelectionDisplay
	}/*,
	dumpCountries: {
		classes: 'dumpCountries',
		selectedText: singleFlagSelectionDisplay
	} */
};