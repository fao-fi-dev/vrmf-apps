var _LIVE_SEARCH_LAST_REQUEST_TOKEN_VALUE = null;
var _LIVE_SEARCH_RESULTS = 30;
var _LIVE_SEARCH_AUTOCOMPLETION_DELAY = $$.Browser.msie  ? 700 : 300;
var _LIVE_SEARCH_AUTOCOMPLETION_MIN_CHARS = 2;

var _LIVE_SEARCH_REPORTING_COUNTRIES = null;

var _LIVE_SEARCH_POST_PROCESSORS = {
	uid: {
		afterSearch: function(term) {
			$("#searchResultsList .vesselUid").each(function() {
				$(this).html($(this).html().emphasize(term.trim(), "u"));
			});
		}
	},
	imo: {
		afterSearch: function(term) {
			$("#searchResultsList .vesselIMO").each(function() {
				$(this).html($(this).html().emphasize(term.trim(), "u"));
			});
		}
	},
	eucfr: {
		afterSearch: function(term) {
			$("#searchResultsList .vesselEUCFR").each(function() {
				$(this).html($(this).html().emphasize(term.trim(), "u"));
			});
		}
	},
	name: {
		afterSearch: function(term) {
			$("#searchResultsList .vesselName").each(function() {
				$(this).html($(this).html().emphasize(term.trim(), "u"));
			});
		}
	},
	ircs: {
		afterSearch: function(term) {
			$("#searchResultsList .vesselIRCS").each(function() {
				$(this).html($(this).html().emphasize(term.trim(), "u"));
			});
		}
	},
	regNo: {
		afterSearch: function(term) {
			$("#searchResultsList .vesselRegNo").each(function() {
				$(this).html($(this).html().emphasize(term.trim(), "u")); 
			});
		}
	}
};

function enableQuickSearch() {
	$("#formContainer, #liveStatsFormContainer, .actionsBottom").addClass("hidden");
	$("#liveSearchFormContainer").removeClass("hidden");

	$("#results").addClass("force");

	$("#live").val(null);
	
	_SORTERS = {};
	_SEQUENTIAL_SORTERS = [];

	$("#results .resultsHeader .vesselAttribute.sortable").removeClass("sortable asc desc").addClass("ui-state-disabled");

	clearSearchResults();

	$("#live").focus();

	return false;
};

function changeAutocompletionOptions(type) {
	var $input = $(type);
	var metadata = $input.metadata();
	
	if($$.isSet(metadata)) {
		if($$.isSet(metadata.delay) && !$$.Browser.msie ) {
			$$.Log.debug("Setting autocomplete delay to: " + parseInt(metadata.delay, 10));
			$("#live").autocomplete("option", "delay", parseInt(metadata.delay, 10));
		}

		if($$.isSet(metadata.minLength)) {
			$$.Log.debug("Setting autocomplete min length to: " + parseInt(metadata.minLength, 10));
			$("#live").autocomplete("option", "minLength", parseInt(metadata.minLength, 10));
		}
	}
};

function addLiveAutocompletion() {
	var field = $("#live");
	
	field.autocomplete($.extend({}, UI_AUTOCOMPLETE_DEFAULTS, {
		search: function(event, ui) {
			return true;
		},
		source: function( request, response ) {
			$$.Log.debug("Delay: " + this.options.delay);
			$$.Log.debug("MinLength: " + this.options.minLength);
			
			if(!$$.isSet(request.term) || request.term.hasWildcards()) {
				haltAutocompletion($(this));
				response([]);
			} else {
				var sortingCriteria = getDisplayOptionsParameters().sc;

				var availableSources = getAvailableVesselSources(); 
				var allSources = [];
				
				for(var s=0; s<availableSources.length; s++)
					allSources.push(availableSources[s].id);
				
				var sources = []; 
				var reportingCountries = [];

				$("#liveSearchForm [name='liveSearchSource']:checked").each(function() {
					sources.push($(this).val());
				});

				$("#liveSearchForm [name='reportingCountries']:checked").each(function() {
					reportingCountries.push($(this).val());
				});

				if(sources.length == 0) {
					sources = allSources;
				}

				var historical = $("#liveSearchForm [name='liveHistorical']:checked").val();
				var byCountry = "true" === $("#liveSearchForm [name='liveIncludeReportingCountry']:checked").val();
				var attribute = $("#liveSearchForm [name='liveSearchCriteria']:checked").val();
				var maxEntries = $("#liveSearchForm [name='liveSearchLimit']:checked").val();

				setStatus($$.I18n.getText("search.quick.status.searching", attribute, request.term));

				var token = new Date().getTime();
				_LIVE_SEARCH_LAST_REQUEST_TOKEN_VALUE = token;

				var customHeaders = {};
				customHeaders[_VRMF_SEARCH_REQUEST_TOKEN_HEADER] = token;
				
				$.ajax($.extend({}, UI_AJAX_AUTOCOMPLETE_DEFAULTS, {
					timeout: VRMF_FINDER_CONFIGURATION.timeouts.searchLive,
					_lastRequestToken: function() { return _LIVE_SEARCH_LAST_REQUEST_TOKEN_VALUE; },
					_requestStart: new Date(), //Custom property
					url: _WRAP_URL(_VRMF_BROWSER_SERVICES_VESSELS_LIVE_SEARCH_VESSELS_URL_PREFIX + 
															  sources.join(",") + '/' + 
															  historical + '/' +
															  'reportedBy/' + ( !byCountry || reportingCountries.length == 0 ? "all" : reportingCountries.join(",")) + '/' + 
															  'max/' + maxEntries + '/' + 
															  attribute + '/' + 
															  encodeURI(request.term.trim().replace(/\./g, '_')) + '.json'),
					data: {
						s_c: $$.isSet(sortingCriteria) ? sortingCriteria : []
					},
					headers: customHeaders,
					success: function(results, textStatus, JQXHR) {
						var _requestEnd = new Date();

						var returnedToken = JQXHR.getResponseHeader(_VRMF_SEARCH_REQUEST_TOKEN_HEADER);

						if(returnedToken != null && returnedToken == _LIVE_SEARCH_LAST_REQUEST_TOKEN_VALUE) {
							if($$.isSet(results) && $$.isSet(results.data)) {
								results.data.fullResultsSize = results.data.pageSize;
							}

							results.requestStart = this._requestStart;
							results.requestEnd = _requestEnd;

							buildSearchResults(results, 0);

							if($$.isSet(_LIVE_SEARCH_POST_PROCESSORS[attribute]))
								_LIVE_SEARCH_POST_PROCESSORS[attribute].afterSearch(request.term);
						} else
							$$.Log.warn("Skipping result display as it was related to a previous request (response token header: " + returnedToken + ", last request token: " + _LIVE_SEARCH_LAST_REQUEST_TOKEN_VALUE);
					},
					complete: function() {
						response([]);
					}
				}));
			}
		}
	}));
	
	$("#live").autocomplete("option", "delay", _LIVE_SEARCH_AUTOCOMPLETION_DELAY);
	$("#live").autocomplete("option", "minLength", _LIVE_SEARCH_AUTOCOMPLETION_MIN_CHARS);
};

function buildReportingSourcesControl() {
	var liveSources = $("#liveSources");
	liveSources.addClass("displaySourceLogos");

	var html = "";
	var source;
	
	var availableSources = getAvailableVesselSources();
	
	for(var s=0; s<availableSources.length; s++) {
		source = availableSources[s];

		html += "<label for=\"liveSearchSource" + source.id + "\" " +
					   "title=\"" + $$.I18n.getText("search.quick.controls.source.select.tip", source.name, source.id) + "\" " +
					   "class=\"widthMin85\" " +
					    $$.TooltipManager.TOOLTIP_CONFIGURATION_ATTRIBUTE + "=\"{ position: { my: 'center bottom', at: 'center top' } }\">" + 
					"<div class='liveSourceIcon'>" + $$.Sources.getSourceIconByID(source.id) + "</div>" + 
					"<div class='liveSourceLabel'>" + source.id + "</div>" + 
				 "</label>";
		
		html += "<input id='liveSearchSource" + source.id + "' type='checkbox' name='liveSearchSource' value='" + source.id + "'/>";
	}
	
	html = $(html);
	
	liveSources.append(html);
};

function buildReportingCountriesControl() {
	var reportingCountries = $$.Metadata.COUNTRIES;

	reportingCountries = reportingCountries.sort(function(a, b) {
		return a.name < b.name ? -1 : ( a.name > b.name ? 1 : 0 );
	});

	var reportingCountriesSelector = $("#liveReportingCountries");
	var countryISO2s = new $$.Set();

	var html = "";
	var country, inputId, image;

	for(var r=0; r<reportingCountries.length; r++) {
		country = reportingCountries[r];

		if($$.isSet(country.iso2Code)) {
			if(!countryISO2s.contains(country.iso2Code)) {
				countryISO2s.add(country.iso2Code);

				inputId = "liveReportingCountries" + country.iso2Code;
				image = $$.Flags.getImage(country);

				html += "<label for='" + inputId + "' title='" + country.name + "'><code>" + country.iso2Code + "&nbsp;</code>" + image + "</label>";
				html += "<input id='" + inputId + "' type='checkbox' name='reportingCountries' value='" + country.iso2Code + "'/>";
			}
		}
	}
	
	html = $(html);
	$("img.flag[title]", html).removeAttr("title");
	
	reportingCountriesSelector.append(html);
};

function installLiveSearchControlsBehaviour() {
	$("#live").unbind("dblclick").dblclick(function() {
		$(this).autocomplete("search");
	});
	
//	$("#liveSearchFormContainer input[type='radio']").click(function() {
//		changeAutocompletionOptions($(this));
//	});
	
	$("#liveSearchFormContainer input[name='liveIncludeReportingCountry']").change(function() {
		var countries = $("#liveReportingCountries");
		var initialized = countries.data("initialized");

		if(!$$.isSet(initialized)) {
			disableInputs($$.I18n.getText("search.quick.blocking.message.interface.updating"));
			
			buildReportingCountriesControl();
			
			countries.addClass("buttonSet");
			countries.data("initialized", true);
			
			installLiveSearchButtonsBehaviour(countries);
			
			enableInputs();
		}
		
		$('#liveReportingCountriesSelector, ' + 
		  '#liveReportingCountries').toggleClass('hidden');
	});
	
	$("input[type='radio'], input[type='checkbox']", $("#liveSearchFormContainer")).change(function() {
//		if($$.Utils.isChecked($(this)))
			changeAutocompletionOptions($(this));
	});
};

function installLiveSearchButtonsBehaviour(target) {
	if($$.isSet(target) && target.is(".buttonSet")) {
		$(target).buttonset();
	} else
		$("#liveSearchFormContainer .buttonSet").buttonset();
};

function installLiveSearchDynamicBehaviour() {
	//CLICK DYNAMIC EVENTS
	$(document).on("change", "#liveSearchFormContainer input[type='radio'], " +
							 "#liveSearchFormContainer input[type='checkbox']", function() {
		$("#live").autocomplete("search");
	});
	
	$(document).on("change", "#liveReportingCountries input[type='checkbox']", function() {
		var selected = $("#liveReportingCountries input[type='checkbox']:checked").size();
		
		$("#liveReportingCountriesSelector .selectedCountries").text(selected);
	});
};

function initializeLiveSearchInterface(callback) {
	$$.Log.debug("Initializing live search interface...");

	buildReportingSourcesControl();
	
	installLiveSearchButtonsBehaviour();
	installLiveSearchDynamicBehaviour();
	installLiveSearchControlsBehaviour();
		
	addLiveAutocompletion();
	
	$$.Log.debug("Live search interface has been initialized");
	
	if($$.isSet(callback)) {
		$$.Log.debug("Invoking live search interface post-initialization callback...");
		
		callback();
		
		$$.Log.debug("Search live interface post-initialization callback has been invoked");
	}
};