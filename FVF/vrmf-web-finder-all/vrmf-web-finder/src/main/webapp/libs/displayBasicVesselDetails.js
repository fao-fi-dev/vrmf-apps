function convertVesselData(row) {
	var IMO = null;
	var EUCFR = null;
	
	var flag = null;
	var flagID = null;
	
	var name = null;
	var simplifiedName = null;
	
	var callsign = null;
	var callsignCountry = null;
	var callsignCountryID = null;
	
	var registrationCountry = null;
	var registrationCountryID = null;
	var registrationNumber = null;
	var registrationPort = null;
	var registrationPortID = null;
	
	var LOA = null;
	var currentLengthType = null;
	var currentLengthValue = null;
	
	var GT = null;
	var currentTonnageType = null;
	var currentTonnageValue = null;
	
	var vesselType = null;
	var vesselTypeID = null;
	
	var gearType = null;
	var gearTypeID = null;

	var identifiers = null;	
	var authorizationsData = null;
		
	if($$.isSet(row.flagData)) {
		flagID = row.flagData[0].countryId;
	}
	
	if($$.isSet(flagID)) {
		flag = $$.Metadata.getCountryByID(flagID).name;
	}

	if($$.isSet(row.nameData)) {
		name = row.nameData[0].name;
		simplifiedName = row.nameData[0].simplifiedName;
	}

	if($$.isSet(row.callsignData)) {
		callsign = row.callsignData[0].callsignId;
		callsignCountryID = row.callsignData[0].countryId;
	}
	
	if($$.isSet(callsignCountryID)) {
		callsignCountry = $$.Metadata.getCountryByID(callsignCountryID).name;
	}

	if($$.isSet(row.registrations)) {
		registrationCountryID = row.registrations[0].countryId;
		registrationNumber = row.registrations[0].registrationNumber;
		registrationPortID = $$.isSet(row.registrations[0].registrationPort) ? row.registrations[0].registrationPort.id : null;
		registrationPort = $$.isSet(row.registrations[0].registrationPort) ? row.registrations[0].registrationPort.name : null;
	}
	
	if($$.isSet(registrationCountryID)) {
		registrationCountry = $$.Metadata.getCountryByID(registrationCountryID).name;
	}

	if($$.isSet(row.lengthData)) {
		currentLengthType = row.lengthData[0].typeId;
		currentLengthValue = row.lengthData[0].value;
		
		for(var l=0; l<row.lengthData.length; l++) {
			if(row.lengthData[l].typeId == "LOA") {
				LOA = row.lengthData[l].value;

				break;
			}
		}
	}

	if($$.isSet(row.tonnageData)) {
		currentTonnageType = row.tonnageData[0].typeId;
		currentTonnageValue = row.tonnageData[0].value;
		
		for(var t=0; t<row.tonnageData.length; t++) {
			if(row.tonnageData[t].typeId == "GT") {
				GT = row.tonnageData[t].value;

				break;
			}
		}
	}

	if($$.isSet(row.authorizationsData)) {
		authorizationsData = row.authorizationsData;
	}

	if($$.isSet(row.types)) {
		vesselType = row.types[0].vesselType.name;
		vesselTypeID = row.types[0].vesselType.id;
	}

	if($$.isSet(row.gears)) {
		for(var g=0; g<row.gears.length; g++)
			if(row.gears[g].primaryGear) {
				gearType = row.gears[g].gearType.name;
				gearTypeID = row.gears[g].gearType.id;

				break;
			}
	}

	IMO = findIMO(row);
	EUCFR = findEUCFR(row);

	if($$.isSet(row.identifiers))
		identifiers = row.identifiers;

	return {
		id: row.id,
		uid: row.uid,
		identifiers: identifiers,
		IMO: IMO,
		EUCFR: EUCFR,
		flag: flag,
		flagID: flagID,
		
		name: name,
		simplifiedName: simplifiedName,
		
		callsign: callsign,
		callsignCountry: callsignCountry,
		callsignCountryID: callsignCountryID,
		
		registrationCountry: registrationCountry,
		registrationCountryID: registrationCountryID,
		registrationNumber: registrationNumber,
		registrationPort: registrationPort,
		registrationPortID: registrationPortID,
		
		LOA: LOA,
		currentLengthType: currentLengthType,
		currentLengthValue: currentLengthValue,
		
		GT: GT,
		currentTonnageType: currentTonnageType,
		currentTonnageValue: currentTonnageValue,
		
		vesselType: vesselType,
		vesselTypeID: vesselTypeID,
		
		gearType: gearType,
		gearTypeID: gearTypeID,

		identifiers: identifiers,	
		authorizationsData: authorizationsData,

		updateDate: row.updateDate,
		
		sourceSystem: row.sourceSystem,
		authorizationsData: authorizationsData
	};
};

function findIMO(data) {
	return findIdentifier("IMO", data);
};

function findEUCFR(data) {
	return findIdentifier("EU_CFR", data);
};

function findIdentifier(typeId, data) {
	if(!$$.isSet(data)) 
		return null;

	if(!$$.isSet(data.identifiers))
		return null;

	for(var i=0; i<data.identifiers.length; i++)
		if(data.identifiers[i].typeId == typeId)
			return data.identifiers[i].identifier;

	return null;
};

function formatUID(uid) {
	return formatID(uid, _ID_MAX_LENGTH);
};

function formatID(id, length) {
	var ID = "" + id;

	return ID.padLeft("0", $$.isSet(length) ? length: _ID_MAX_LENGTH);
};

function openOriginalSourceData(data) {
	window.open(URL_BUILDERS[data.sourceSystem].builder(data, URL_BUILDERS[data.sourceSystem].baseURL), "SRC_VESSEL_" + data.id);

	return false;
};

function buildParticipatingVesselData(data) {
	return this.buildResult(data, false, true, false);
};

function buildCombinedVesselData(data, groupByUID) {
	return this.buildResult(data, groupByUID, false, true);
};

function isSourceID(identifier, source) {
	return identifier.typeId === source + "_ID" ||
		   identifier.typeId === source + "_CFR";
};

function buildResult(data, groupByUID, displayInternalID, displayAllSources, showVesselDetailsCallback, showParticipatingVesselsCallback) {
	var UID = "<div class='vesselUid hideIfNotGroupingByUID vesselAttribute" + ( groupByUID ? "" : " hidden" ) + "' title='" + $$.I18n.getText("vessels.display.results.uid.tip") + "'>" + 
			  	formatID(data.uid) + 
			  "</div>\n";

	var IID = "<div class='vesselId hideIfNotGroupingByID vesselAttribute" + ( groupByUID ? " hidden" : "" ) + "'>" + 
				formatID(data.id) + 
			  "</div>";

	var flag = "<div class='vesselFlag vesselAttribute'>\n" + $$.Flags.getImageByCountryID($$.isSet(data.flagID) ? data.flagID : null) + "\n</div>\n";

	var hasName = $$.isSet(data.name);
	var vesselName = hasName ? data.name : "-------";
	
	var vesselSimplifiedName = hasName && data.name.toUpperCase() != data.simplifiedName ? data.simplifiedName : null;
	var hasSimplifiedName = vesselSimplifiedName != null;
	var showSourceLink = displayInternalID && $$.isSet(URL_BUILDERS[data.sourceSystem]);

	var nameTitle = ( hasName ? data.name.replace(/\"/g, "''") + ( hasSimplifiedName ? " [ " + data.simplifiedName.replace(/\"/g, "''") + " ]" : "" ) : "[ " + $$.I18n.getText("vessels.display.results.name.missing.tip") + " ]" ) + " - " + ( showSourceLink ? $$.I18n.getText("vessels.display.results.id.details.source.tip") : ( !displayInternalID ? $$.I18n.getText("vessels.display.results.id.details.aggregated.tip") : $$.I18n.getText("vessels.display.results.id.details.link.missing.tip") ) );
	
	var name = "<div class='vesselName vesselAttribute expandOnHover' title=\"" + nameTitle + "\">" +
			   	( showSourceLink ? "<a href='" + URL_BUILDERS[data.sourceSystem].builder(data, URL_BUILDERS[data.sourceSystem].baseURL) + "' target='SRC_VESSEL_" + data.id + "' class='externalStatic newWin'>" : "" ) + 
			   		"<span class='currentVesselName'>" +
			   		vesselName + 
			   		"</span>" +
			   	( showSourceLink ? "</a>" : "" ) +
			   "</div>\n";

	var hasIMO = $$.isSet(data.IMO);
	var imoNumber = hasIMO ? data.IMO : "-------";
	var imo = "<div class='vesselIMO vesselAttribute" + ( hasIMO ? " selectableOnClick" : " empty") + "'>" +
			  	imoNumber +  
			  "</div>\n";

	var hasEUCFR = $$.isSet(data.EUCFR);
	var EUCFRNumber = hasEUCFR ? data.EUCFR : "-------";
	var EUCFR = "<div class='vesselEUCFR vesselAttribute" + ( hasEUCFR ? " selectableOnClick" : " empty") + "'>" +
					EUCFRNumber +  
				"</div>\n";

	var hasTypeID = $$.isSet(data.vesselTypeID);
	var hasTypeName = $$.isSet(data.vesselType);
	var vesselType = hasTypeID ? $$.Metadata.VESSEL_TYPES_MAP[data.vesselTypeID] : null;
	
	var typeDescription = hasTypeName ? data.vesselType : ( $$.isSet(vesselType) ? vesselType.name : ( hasTypeID ? $$.I18n.getText("vessels.display.results.type.vessel.description.missing", data.vesselTypeID) : "-----" ) );
	
	var typeLabel = ( $$.isSet(vesselType) ? 
						"[ " + vesselType.sourceSystem + 
							 ( $$.isSet(vesselType.countryId) ? " / " + 
							   $$.Metadata.getCountryByID(vesselType.countryId).iso2Code + " " : "" ) + " ] " + 
							   vesselType.originalVesselTypeId + " - " 
					  : 
						"" ) + 
					  $$.isSet(typeDescription) ? typeDescription : null;
	
	var type = "<div class='hidden vesselType vesselAttribute'>\n" + 
				  "<div class='vesselTypeLabel expandOnHover" + ( hasTypeName ? " selectableOnClick" : "" ) + "'" + ( $$.isSet(typeLabel) ? " title='" + typeLabel + "'" : "" ) + ">" +
				  	typeDescription + 
				  "</div>\n" +
			   "</div>\n";

	var hasGearID = $$.isSet(data.gearTypeID);
	var hasGearName = $$.isSet(data.gearType);
	var gearType = hasGearID ? $$.Metadata.GEAR_TYPES_MAP[data.gearTypeID] : null;
	
	var gearDescription = hasGearName ? data.gearType : ( $$.isSet(gearType) ? gearType.name : ( hasGearID ? $$.I18n.getText("vessels.display.results.type.gear.description.missing", data.gearTypeID) : "-----" ) );
	
	var gearLabel = ( $$.isSet(gearType) ? 
					 	"[ " + gearType.sourceSystem + " ] " + 
					 	gearType.originalGearTypeCode + " - "
					  : 
						"" ) + 
					  $$.isSet(gearDescription) ? gearDescription : null;
	
	var gear = "<div class='gearType vesselAttribute'>\n" + 
				  "<div class='gearTypeLabel expandOnHover" + ( hasGearName ? " selectableOnClick" : "" ) + "'" + ( $$.isSet(gearLabel) ? " title='" + gearLabel + "'" : "" ) + ">" +
				  gearDescription +
				  "</div>\n" +
			   "</div>\n";

	var hasIRCS = $$.isSet(data.callsign);
	var hasIRCSCountry = false && $$.isSet(data.callsignCountryID);
	var IRCSCountry = hasIRCSCountry ? $$.Metadata.getCountryByID(data.callsignCountryID) : null;
	var callsign = hasIRCS ? data.callsign : "-------";
	var ircs = "<div class='vesselIRCS vesselAttribute'>\n" +
			   	  "<div class='vesselIRCSValue expandOnHover" + ( hasIRCS ? " selectableOnClick" : " empty" ) + "'" + ">" + 
			   	  	( $$.isSet(IRCSCountry) ? $$.Flags.getImageByCountryID(IRCSCountry.id) : "" ) + "<span" + ( hasIRCS ? " title='" + callsign + "'" : "") + ">" + callsign + "</span>" +   
			   	  "</div>\n" +
			   "</div>\n";

	var hasALength = $$.isSet(data.currentLengthValue);
	var hasLOA = $$.isSet(data.LOA);
	
	var hasLength = hasALength || hasLOA;
	
	var length = "<div class='vesselLength vesselAttribute" + ( hasLength ? "" : " empty" ) + "'>\n" +
					 ( hasLength ? $$.I18n.getText("vessels.display.results.length", ( hasLOA ? data.LOA : data.currentLengthValue ).toFixed(2)) : "-----" ) +
				 "</div>";

	var hasATonnage = $$.isSet(data.currentTonnageValue);
	var hasGT = $$.isSet(data.GT);
	
	var hasTonnage = hasATonnage || hasGT;
	
	var tonnage = "<div class='vesselTonnage vesselAttribute" + ( hasTonnage ? "" : " empty" ) + "'>\n" +
					 ( hasTonnage ? $$.I18n.getText("vessels.display.results.tonnage", ( hasGT ? data.GT : data.currentTonnageValue ).toFixed(2)) : "-----" ) +
				 "</div>";

	var hasRegCountry = $$.isSet(data.registrationCountryID);
	var regCountry = "<div class='hidden vesselRegFlag vesselAttribute" + ( hasRegCountry ? "" : " empty" ) + "'>\n" +
					 	( hasRegCountry ? $$.Flags.getImageByCountryID(data.registrationCountryID): "-----" ) +
					 "</div>";

 	var hasRegPort = $$.isSet(data.registrationPort);
	var regPort = "<div class='hidden vesselRegPort vesselAttribute'>\n" +
					"<div class='vesselRegPortValue expandOnHover" + ( hasRegPort ? " selectableOnClick" : " empty" ) + "'" + ">" + 
						"<span" + ( hasRegPort ? " title='" + data.registrationPort + "'" : "") + ">" + ( hasRegPort ? data.registrationPort : "-------" ) + "</span>" +
					"</div>" +
				  "</div>";
					 						 
	var hasRegNo = $$.isSet(data.registrationNumber);
	var regNoValue = hasRegNo ? data.registrationNumber : "-------";
	var regNo = "<div class='hidden vesselRegNo vesselAttribute'>\n" +
			   	  "<div class='vesselRegNoValue expandOnHover" + ( hasRegNo ? " selectableOnClick" : " empty" ) + "'" + ">" + 
			   	  	"<span" + ( hasRegNo ? " title='" + regNoValue + "'" : "") + ">" + regNoValue + "</span>" + 
			   	  "</div>\n" +
			   "</div>\n";

	var vesselMeta = "<div class='vesselMeta vesselAttribute'>\n";

	if(displayAllSources) {
		var numSources = "<div class='vesselSources vesselAttribute smaller'>" + data.sourceSystems.length + "</div>\n";

		var sources = [ data.sourceSystem ];
		var currentSource = null;
		var alreadyChecked = false;

		for(var s=0; s<data.sourceSystems.length; s++) {
			currentSource = data.sourceSystems[s];

			if(sources.indexOf(currentSource) < 0)
				sources.push(currentSource);
			else {
				if(!alreadyChecked)
					alreadyChecked = true;
				else
					sources.push(currentSource);
			}
		}

		var allSources = "<div class='vesselTotalSources vesselAttribute smaller' title='" + sources.join(", ") + "'>";
				allSources += sources[0];
		if(sources.length > 1) {
				allSources += " +" + ( sources.length - 1) + "</span>";
		}
			allSources += "</div>";

		vesselMeta += numSources;
		vesselMeta += allSources;

		vesselMeta += "</div>\n";
	}

	var hasUpdateDate = $$.isSet(data.updateDate);
	
	var updateDate = hasUpdateDate ? data.updateDate : null;
	
	var dateValue = updateDate != null ? new Date(updateDate).formatDate() : "----------";

	var update = "<div class='vesselLastUpdate vesselAttribute smaller" + ( hasUpdateDate ? " selectableOnClick" : "" ) + "'>" + 
					dateValue + 
				 "</div>\n";

	var li = "<li class='vesselResultEntry'>\n";

	if(displayInternalID) {
		var vesselSource = "<div class='vesselOriginalSource vesselAttribute' title='" + data.sourceSystem + " - " + $$.Metadata.SYSTEMS_MAP[data.sourceSystem].name + "'>\n" +
						   	"<span>&nbsp;" + data.sourceSystem + "</span>" + 
						   "</div>\n";

		var vesselOriginalID = "<div class='vesselOriginalID vesselAttribute'>\n"; 
		var originalID = null;

		if($$.isSet(data.identifiers)) {
			for(var i=0; i<data.identifiers.length && originalID == null; i++) {
				if(isSourceID(data.identifiers[i], data.sourceSystem)) {
					originalID = "<span title='" + $$.I18n.getText("vessels.display.results.id.source.tip", data.sourceSystem) + "'>" + data.identifiers[i].identifier + "</span>";
				}
			}
		}

		if(originalID == null) {
			originalID = "<span title='" + $$.I18n.getText("vessels.display.results.id.source.missing.tip", data.sourceSystem) + "'>" +
						 	"----" +
						 "</span>\n";
		}

		vesselOriginalID += originalID;
		vesselOriginalID += "</div>\n";

		li += vesselSource;
		li += vesselOriginalID;
	} else {
		li += UID;
		li += IID;
	}

	li += flag;
	li += name;
	li += EUCFR;
	li += imo;
	li += type;
	li += gear;
	li += ircs;
	li += length;
	li += tonnage;
	li += regCountry;
	li += regPort;
	li += regNo;
	li += update;

	if(displayAllSources) {
		li += vesselMeta;
	} 

	return li;
};

function showParticipatingVesselDetails(UID) {
	var formattedUID = formatID(UID);

	disableInputs($$.I18n.getText("vessels.display.message.blocking.participating.retrieving", formattedUID));

	$.ajax(_WRAP_URL(_VRMF_BROWSER_SERVICES_VESSELS_GET_PARTICIPANTS_URL_PREFIX + "UID/" + UID), {
		cache: false,
		dataType: 'json',
		type: 'GET',
		success: function(results, textStatus, JQXHR) {
			var target = $("#vesselDetailsContainer");
			target.empty();

			var currentVesselUID = $("#vesselDetails .currentVesselUID");
			currentVesselUID.text(formattedUID);

			var dataset = results.data.dataSet;

			var html = "";
			if(dataset.length > 0) {
				for(var d=0; d<dataset.length; d++) {
					html += buildParticipatingVesselData(convertVesselData(dataset[d]));
				}

				target.append(html);
			}

			var counter = 0;
			$(".vesselResultEntry", target).each(function() {
				var data = dataset[counter];

				var $this = $(this);
				var even = counter % 2 == 0;

				$this.data("data", data);
				$this.addClass(even ? _EVEN_ROW_CLASS : _ODD_ROW_CLASS).data("originalClass", even ? _EVEN_ROW_CLASS : _ODD_ROW_CLASS);

				$(".vesselName", $this).click(function() {
					openOriginalSourceData($this.data("data"));
					return false;
				});

				counter++;
			});

			configureTooltips([ { selector: "#vesselDetails .vesselResultEntry [title]", config: TOOLTIP_SEARCH_RESULTS_DEFAULTS} ]);

			$(".vesselResultEntry", target).hover(function() {
				$(this).removeClass(_EVEN_ROW_CLASS).
						removeClass(_ODD_ROW_CLASS).
						addClass(_HOVER_ROW_CLASS);
			}, function() {
				var originalClass = $(this).data("originalClass");
				$(this).removeClass(_EVEN_ROW_CLASS).
						removeClass(_ODD_ROW_CLASS).
						removeClass(_HOVER_ROW_CLASS).
						addClass(originalClass);
			});

			showVesselDetailsPanel();
		},
		complete: function() {
			enableInputs();
		}
	});
};