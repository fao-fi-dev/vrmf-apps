function installVesselDisplayTooltips() {
	configureTooltips([
	                   	{ selector: "#current .menu a[title], " +
	                   				"#historical .menu a[title], " +
	                   				"#timeline .menu a[title]", config: TOOLTIP_LINK_DEFAULTS },
	                   	{ selector: "#current .attributeMeta[title], " +
	                   				"#historical .attributeMeta[title], " + 
	                   				"#timeline .attributeMeta[title]", config: { position: { my: 'right bottom', at: 'right top' } } }, 
	                   	{ selector: "#current a.previousDate, " +
	                   				"#current a.nextDate", config: TOOLTIP_DEFAULTS },
	                   	{ selector: "#current [title], " +
	                   				"#historical [title], " +
	                   				"#timeline [title]", config: TOOLTIP_DEFAULTS }
	                   ]);
};