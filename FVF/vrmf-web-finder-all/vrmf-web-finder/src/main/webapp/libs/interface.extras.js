var _POLLER_LAST_REQUEST = null;
var _POLLER = null;
var _POLLER_INTERVAL = 150;
var _DELAYED_POLLER_INTERVAL = 600;

function startPolling(pollingServiceID, title, immediate) {
	if($$.isSet(pollingServiceID))
		_POLLING_SERVICE_ID = pollingServiceID;
	else
		_POLLING_SERVICE_ID = "DEFAULT_PROCESS";

	$("#processStatus").text("Sending request...");
	$("#processProgress").progressbar("value", 0);
	$("#processProgress div").show();

	$.blockUI({ 
		title: "<div class='ui-helper-clearfix'><div class='ui-dialog-title'><h3>" + title + "</h3></div></div>", 
		message: $("#processTracker"),
		blockMsgClass: "ui-dialog"
	});

	if($$.isSet(immediate) && immediate) {
		_POLLER = window.setInterval(poller, _POLLER_INTERVAL);
	} else {
		var delayed = function() {
			_POLLER = window.setInterval(poller, _POLLER_INTERVAL);
		};

		window.setTimeout(delayed, _DELAYED_POLLER_INTERVAL);
	}
};

function endPolling() {
	if($$.isSet(_POLLER))
		window.clearInterval(_POLLER);

	enableInputs();
};

function poller() {
	var now = new Date().getTime();
	_POLLER_LAST_REQUEST = now;

	$.ajax({
		async: true,
		cache: false,
		timeout: VRMF_FINDER_CONFIGURATION.timeouts.polling,
		type: 'get', 
		dataType: 'json',
		data: { requestToken: now },
		url: _COMMON_PUBLIC_SERVICES_URL_PREFIX + "meta/track/process/" + _POLLING_SERVICE_ID,
		success: function(results, textStatus, jqXHR) {
			var returnedToken = getResponseToken(jqXHR);

			if(returnedToken != null && returnedToken === _POLLER_LAST_REQUEST) {
				var completed = $$.isSet(results) && $$.isSet(results.progress);
				completed &= results.progress < 0 && 'Idle' != results.action;

				$("#processStatus").text(results.action);

				if($$.isSet(results.error) && results.error) {
					completed |= results.error;

					$$.Interface.error("Unexpected error caught: " + result.errorMessage);
				}

				if(completed) {
					endPolling();
				} else {
					$("#processStatus").text(results.action);

					if($$.isSet(results.progress)) {
//						if(results.progress == 0 || results.progress == 100)
							$("#processProgress").progressbar("value", results.progress);
//						else
//							$(".ui-progressbar-value").animate({ width: Math.round(results.progress) + "%" }, _POLLER_INTERVAL * .95);
					}
				}
			} else
				$$.Log.warn("Skipping result display as it was related to a previous request (response token header: " + returnedToken + ", last request token: " + _POLLER_LAST_REQUEST);
		},
		error: function() {
			endPolling();
		}
	});
};

function buildSources(excludeQuickSearch) {
	var sources = $("#source").empty();
	var issuingAuthority = $("#issuingAuthority").empty();

	var optgroup = null;
	var system = null;
	var group = null;

	var uniqueVesselSources = new $$.Set();
	var uniqueAuthSources = new $$.Set();

	for(var g=0; g<$$.Metadata.SYSTEMS_GROUPS.length; g++) {
		group = $$.Metadata.SYSTEMS_GROUPS[g];

		optgroup = "<optgroup label='" + group.description + "'>";

		for(var s=0; s<group.systems.length; s++) {
			system = group.systems[s];

			if(system.vesselSource) {
				uniqueVesselSources.add(system);

				optgroup += "<option value='" + system.id + "'>";

				if($$.Browser.msie )
					optgroup += system.id + " - " + system.name;

				optgroup += "</option>";
			}
		}

		optgroup += "</optgroup>";

		if(group.systems.length > 0)
			sources.append(optgroup);
	}

	if($$.Metadata.AUTH_SYSTEMS_GROUPS.length == 0) {
		$("#authorizationsSearch").parent("li").hide();
	} else {
		for(var g=0; g<$$.Metadata.AUTH_SYSTEMS_GROUPS.length; g++) {
			group = $$.Metadata.AUTH_SYSTEMS_GROUPS[g];

			optgroup = "<optgroup label='" + group.description + "'>";

			for(var s=0; s<group.systems.length; s++) {
				system = group.systems[s];

				uniqueAuthSources.add(system);

				optgroup += "<option value='" + system.id + "'>";

				if($$.Browser.msie )
					optgroup += system.id + " - " + system.name;

				optgroup += "</option>";
			}

			optgroup += "</optgroup>";

			issuingAuthority.append(optgroup);
		}

		issuingAuthority.unbind("multiselectclick").bind("multiselectclick", function(event, ui) {
			updateAuthTypes();
		});
	}

	if(!$$.Browser.msie ) {
		var currentSystem = null;

		$("#source option").each(function() {
			var $this = $(this);
			currentSystem = $$.Metadata.SYSTEMS_MAP[$this.val()]; 
			$this.append("<span>" + $$.Sources.getSourceIconByID(currentSystem.id) + currentSystem.id + " - " + currentSystem.name + "</span>");
		});

		$("#issuingAuthority option").each(function() {
			var $this = $(this);
			currentSystem = $$.Metadata.AUTH_SYSTEMS_MAP[$this.val()]; 
			$this.append("<span>" + $$.Sources.getSourceIconByID(currentSystem.id) + currentSystem.id + " - " + currentSystem.name + "</span>");
		});
	}

	var availableSources = 0;

	try {
		availableSources = uniqueVesselSources.asArray().length;
	} catch (E) {
		$$.Log.error(E);
	}

	$("#numSourcesTo").val(availableSources);

	$(".numSources.slider").slider({
		max: parseInt($("#numSourcesTo").val(), 10),
		values: [ parseInt($("#numSourcesFrom").val(), 10), parseInt($("#numSourcesTo").val(), 10) ]
	});

	var liveSources = $("#liveSources");
	liveSources.addClass("displaySourceLogos");

	var html = "";
	var source;
	
	for(var u=0; u<availableSources; u++) {
		source = uniqueVesselSources.asArray()[u];

		html += "<label for=\"liveSearchSource" + source.id + "\" title=\"Include data sourced by " + source.name + " [ " + source.id + " ]\" class=\"widthMin85\" tooltipConfiguration=\"{ my: 'BOTTOM_CENTER', at: 'TOP_CENTER' }\">" + 
					"<div class='liveSourceIcon'>" + $$.Sources.getSourceIconByID(source.id) + "</div>" + 
					"<div class='liveSourceLabel'>" + source.id + "</div>" + 
				 "</label>";
		
		html += "<input id='liveSearchSource" + source.id + "' type='checkbox' name='liveSearchSource' value='" + source.id + "'/>";
	}
	
	html = $(html);
	
	liveSources.append(html);

	/* REMOVED */ //$$.TooltipManager.apply(html);
	
	_AVAILABLE_SOURCES_NUM = availableSources;

	if(availableSources == 1) {
		$("#liveStatsSourcesFieldset, #sourcesSearch, #liveSources, #liveSourcesHeading").hide();
	}

	var creditsSourcesListContainer = $("#creditsSourcesList");

	var sourceItem;
	var currentSource;

	for(var u=0; u<availableSources; u++) {
		currentSource = uniqueVesselSources.asArray()[u];

		sourceItem = $("<li/>");
		sourceItem.append("<a class=\"external { target:'_" + currentSource.id + "' }\">" +
							"<img src=\"../cdn/media/images/decorations/sources/logo/" + currentSource.id + ".png\" class=\"sourceLogo\" alt=\"" + currentSource.id + "\"/>" +
						  "</a>");

		/* REMOVED */ //$$.TooltipManager.apply($("img", sourceItem).attr('title', currentSource.id + " - " + $$.Metadata.SYSTEMS_MAP[currentSource.id].name));
		$("img", sourceItem).attr('title', currentSource.id + " - " + $$.Metadata.SYSTEMS_MAP[currentSource.id].name);
		
		$("a", sourceItem).attr('href', $$.Metadata.SYSTEMS_MAP[currentSource.id].website);

		creditsSourcesListContainer.append(sourceItem);
	}

	return sources;
};

function completeInitialization(callback) {
	var end, start = new Date().getTime();

	$$.Log.debug("interface :: completeInitialization");

	$("#tabsContainer").tabs({ activate: function(event, ui) {
		if(ui.newTab.context.hash == '#pictures') {
			$("img.thumbs").lazyload();
			$("#thumbs").scrollTo(0, 0);
			displayFirstPicture();
		}

		return true;
	}});

	$("#tabsContainer").tabs({ disabled: 3});
	
	$("#tabs").removeClass("ui-corner-all");;
	
	additionalThemeConfiguration();

	try {
		buildSources();
		buildStatuses();
		buildFlags();
		buildIdentifierTypes();
		buildAgeClass();
		buildLengthAndTonnages();
		buildEnginePowers();
		updateAuthTypes();
	} catch (E) {
		$$.Log.warn(E);
	}
	
	var TOOLTIP_CONFIGURATION = [ 
	                   		  { selector: "#FAOFisheryHeader [title]", config: TOOLTIP_HEADER_DEFAULTS },
	                   		  { selector: "#pageHeader [title]", config: TOOLTIP_DEFAULTS },
	                   		  { selector: "#searchCriteria [title], #searchResults [title], #vesselDetails [title]", config: TOOLTIP_SEARCH_FILTER_DEFAULTS },
	                   		  { selector: "#selectedVesselsDetails [title]", config: TOOLTIP_DEFAULTS },
	                   		  { selector: "#footer", config: $.extend(true, {}, TOOLTIP_FOOTER_DEFAULTS, $$.TooltipManager.MY_BOTTOM_CENTER_DEFAULTS, $$.TooltipManager.AT_TOP_CENTER_DEFAULTS) },
	                   		  { selector: "#footer [title]", config: $.extend(true, {}, TOOLTIP_FOOTER_DEFAULTS, { show: { solo: true } }) },
//	                   		  { selector: "#applicationDisclaimer [title]", config: TOOLTIP_DISCLAIMER_DEFAULTS },
	                   		  { selector: "#applicationCredits [title]", config: TOOLTIP_CREDITS_DEFAULTS },
	                   		  { selector: ".searchResultsHeader div", config: TOOLTIP_SEARCH_RESULTS_DEFAULTS }  
	                   	]; 

	                   	var tConf;
	                   	var target;
	                   	for(var t=0; t<TOOLTIP_CONFIGURATION.length; t++) {
	                   		tConf = TOOLTIP_CONFIGURATION[t];
	                   		
	                   		target = $(tConf.selector);
	                   		
	                   		target.each(function() {
	                   			var $this = $(this);
	                   			
	                   			if(!$$.isSet($this.attr("tooltipConfiguration")))
	                   				$this.attr("tooltipConfiguration", JSON.stringify(tConf.config));
	                   		});		
	                   	}

	end = new Date().getTime();

	$$.Log.debug("interface :: completeInitialization took " + ( end - start ) + " mSec.");


	
	displayInterface(callback);
};