<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<?xml version="1.0" encoding="utf-8"?>
<%@page contentType="text/html; charset=UTF-8" isErrorPage="false" trimDirectiveWhitespaces="true"%>

<%@include file="components/commonInitialization.jspf"%>
<%response.sendRedirect(requestInfo.getScheme() + "://" + request.getServerName() + "/fishery/collection/fvf/" + languageWithFallback);%>