<%@page contentType="text/html; charset=UTF-8" isErrorPage="false" trimDirectiveWhitespaces="true"%>
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%@page import="org.fao.fi.vrmf.common.models.stats.StatusReportBySource"%>

<%@include file="components/commonInitialization.jspf"%>

<%
	//breadcrumbs.add(new Pair<String, Pair<String, String>>("Introduction", new Pair<String, String>(request.getRequestURI(), "Go to the introductory page")));
%>
<html xmlns="http://www.w3.org/1999/xhtml" lang="<%=languageWithFallback%>" xml:lang="<%=languageWithFallback%>" dir="<%=isRTL ? "rtl" : "ltr"%>" class="<%=isRTL ? "rtl" : "ltr"%>">
	<head> 
		<base href="<%=baseHREF%>"/>

		<title><%=defaultTitle%></title>

		<meta name="description" content="<%=defaultDescription%>"/>
		<meta name="keywords" content="<%=CollectionsHelper.join(keywords, ", ")%>"/>
		<meta name="author" content="<%=metaAuthor%>"/>
		<meta name="generator" content="FAO VRMF engine<%=specificSource == null ? "" : " for " + specificSourceID + " vessels"%>"/>
		<meta http-equiv="content-language" content="<%=languageWithFallback%>"/>

		<%@include file="components/commonLibraries.jspf"%>
		<%@include file="components/commonScripts.jspf"%>

		<link id="baseCSS" rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/themes/base/jquery-ui.css"/>
		<link id="themeCSS" rel="stylesheet" type="text/css" href="<%=URLHelper.unescape(themeCSS)%>"/>

		<link rel="stylesheet" type="text/css" href="../cdn/styles/themes/jquery/jquery.qtip.min.css"/>
		<link rel="stylesheet" type="text/css" href="../cdn/styles/base/all.css"/>
		<link rel="stylesheet" type="text/css" href="styles/default.css|login.css|index.css"/>

		<!-- Damn' that IE thing... -->
		<link rel="stylesheet" type="text/css" href="styles/default.ie.css" />
		<link rel="stylesheet" type="text/css" href="../cdn/styles/default.ie.css" />
<%
	Collection<SSystems> availableSourceSystems = (Collection<SSystems>)request.getAttribute(RequestAttributesConstants.DATA_SOURCES_REQUEST_ATTRIBUTE);
	Collection<StatusReportBySource> availableSourceSystemsStats = (List<StatusReportBySource>)request.getAttribute(RequestAttributesConstants.AVAILABLE_DATA_SOURCES_STATS_REQUEST_ATTRIBUTE);

	Map<String, SSystems> systemsMap = new HashMap<String, SSystems>();
	for(SSystems system : availableSourceSystems) {
		systemsMap.put(system.getId(), system);
	}

	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd @ hh:mm:ss");
%>
	</head>
	<body class="ui-helper-clearfix">

		<!-- Header -->
		<%@include file="components/header.jspf"%>

		<div id="padder" class="ui-widget-content borderless removeBackground">&#160;</div>

		<!-- Main content -->
		<div id="mainContainer" class="hidden">
			<div class="left" style="clear: both">
				<h2 class="ui-state-highlight">Welcome to the FAO Fishing Vessels Finder [ FVF ]</h2>
				<div id="text" class="scrollable ui-widget-content">
					<%if(specificSourceID != null) {%>
					<div id="sourceSpecificNotice" class="ui-state-highlight noShadow">
						<h3>Important notice</h3>
						<div>
							<p>
								<b>You are currently accessing the FVF version dedicated to fishing vessels as reported by <%=specificSourceID%> [ <%=sourceName%> ]. This means that you will be able to search
								and display only data coming from this source. To enter the global FVF (with details of vessels retrieved from <a href="<%=langPath%>#availableSources" class="default<%=specificSourceID == null ? "" : " externalStatic newWin"%>">each currently available source</a>) 
								please click <a href="<%=langPath%>" class="default externalStatic newWin" title="Enter the global FVF">here</a>.</b>
							</p> 
						</div>
					</div>
					<%}%>

					<div class="sectionContainer ui-widget-content">
						<h3 class="ui-widget-header">Overview</h3>
						<div class="section resizable">
							<p>
								The FAO Fishing Vessels Finder [ FVF ] is an online tool to locate information on individual fishing vessels (including supporting vessels, carriers, fishery research vessels and inspection boats)
								that are disseminated - or were disseminated in the past - by a range of national, multi-national, regional and international organizations.
							</p>

							<p>
								All the information accessible via the FVF is shown exactly as originally disseminated by its source, with proper identification of owners and date of retrieval for each detail. Thus FAO will
								accept no liability on credibility and accurateness of the information reported.  
							</p>

							<p>
								FAO has made its best effort in identifying potential duplication of vessels disseminated by multiple sources within the range allowed by the available information and resources allocated for this
								task.</p>
						</div>
					</div>

					<div class="sectionContainer ui-widget-content">
						<h3 class="ui-widget-header">Background</h3>
						<div class="section resizable">
							<p>
								The FVF is supported by the <a href="http://www.fao.org/fishery/statistics/software/fvrmf/<%=languageWithFallback%>" class="externalStatic default newWin { target: '_VRMF_HOME' }">Vessel Record Management Framework</a> [ VRMF ], an expanding set of tools, methodologies and software components developed
								by FAO with the support of the Japanese Trust Fund.
							</p>
							<p>
								The VRMF, as well as the FVF, is constantly upgraded and improved in response to feedbacks from users. 
							</p>
						</div>
					</div>

					<a id="availableSources"></a>

					<div class="sectionContainer ui-widget-content">
						<h3 class="ui-widget-header">Available data sources</h3>
						<div class="section resizable">
							<div>
								<p>
								The currently available data source<%=availableSourceSystems.size() > 1 ? "s are" : " is"%>:
								</p>

								<ul class="availableDataSources">
								<%
									int counter, totalVessels, totalGroupedVessels;
									counter = totalVessels = totalGroupedVessels = 0;

									String systemID, systemName, systemWebsite;

									for(StatusReportBySource stats : availableSourceSystemsStats) {
										systemID = stats.getSourceSystem();
										systemName = systemsMap.get(systemID).getName();
										systemWebsite = systemsMap.get(systemID).getWebsite();

										totalVessels += stats.getVessels();
										totalGroupedVessels += stats.getGroupedVessels();
									%>
									<li class="removeBackground noBorder <%=counter % 2 == 0 ? "ui-state-highlight" : "ui-state-active"%>">
										<span>
											<a href="<%=stats.getSourceSystem()%>/<%=langPath%>" title="Click here to access the dedicated FVF main page for <%=systemID%> vessels" class="externalStatic simplest newWin { target: '_VRMF_<%=systemID%>', position: { my: 'center bottom', at: 'center top' } } ">
												<b><i>&nbsp;<%=systemName%></i> [ <%=systemID%> ] </b>
											</a>
											&nbsp;
											<a href="<%=systemWebsite%>" class="externalStatic simplest newWin { target: '_<%=systemID%>_HOME_PAGE' }" title="Click here to enter the <%=systemName%> [ <%=systemID%> ] home page">
												<img class="sourceLogo" src="../cdn/media/images/decorations/sources/logo/small/<%=systemID%>.png"></img>
											</a>
										</span> 
										<span class="right sourceStatsSummary">
											<a href="<%=systemID%>/search/<%=langPath%>" class="externalStatic pre simplest newWin { target: '_VRMF_SEARCH_<%=systemID%>', my: 'BOTTOM_CENTER', at: 'TOP_CENTER' }" title="Click here to enter the FVF search page for <%=systemID%> vessels">
												<b><%=stats.getGroupedVessels()%></b> vessels
												<img src="../cdn/media/images/icons/default/magnifier.png"/>
											</a> 
											&nbsp;[ last update: <%=formatter.format(stats.getLastUpdate())%> ]&nbsp;
										</span>
									</li>
									<%
										counter++;
									}%>
								</ul>

							</div>

							<%if(specificSourceID == null) {%>
							<p>
								Summing up to <b><%=totalGroupedVessels%></b> vessels - <em class="{ position: { my: 'center bottom', at: 'center top' } }" title="Actual figures may differ as duplicate vessels shared among many sources do account separately for each source they belong to">including duplicates</em> - retrieved from <b><%=availableSourceSystemsStats.size()%></b> different sources. 
							</p>
							<%}%>

							<p>
								Additional information regarding vessel pictures and current locations is provided by the FVF with the help of the following external services, whose work is hereby acknowledged and used in accordance
								with the respective terms of services:
							</p>
							<div>
								<p>
									<b>Picture identification:</b>
								</p>

								<ul>
									<li><a href="http://www.marinetraffic.com/ais/gallery.aspx" class="externalStatic default newWin { target: '_MARINE_TRAFFIC_PICS' }">www.marinetraffic.com</a></li>
									<li><a href="http://www.shipspotting.com/gallery" class="externalStatic default newWin { target: '_SHIP_SPOTTING_PICS' }">www.shipspotting.com</a></li>
									<li><a href="http://commons.wikimedia.org/" class="externalStatic default newWin { target: '_WIKIMEDIA_PICS' }">commons.wikimedia.org</a></li>
								</ul>
							</div>

							<div>
								<p>
									<b>Ship positions:</b>
								</p>
								<ul>
									<li><a href="http://www.marinetraffic.com/ais" class="externalStatic default newWin { target: '_MARINE_TRAFFIC_AIS' }">www.marinetraffic.com</a></li>
									<li><a href="http://www.shipspotting.com/ais" class="externalStatic default newWin { target: '_SHIP_SPOTTING_AIS' }">www.shipspotting.com</a></li>
								</ul>
							</div>

							<p>
								The FVF is aiming toward providing full coverage of fishing vessels operating in the world. If your are a fishery-related organization and are interested in disseminating your fishing vessels data
								through the FVF, please contact the <a href="mailto:<%=adminEMail%>?subject=Addition%20of%20a%20new%20data%20source%20to%20the%20Fishing%20Vessels%20Finder" class="default" title="Send an e-mail to <%=adminEMail%>">Fishing Vessels Finder Administrators</a>.
							</p>
						</div>
					</div>

					<div class="sectionContainer ui-widget-content">
						<h3 class="ui-widget-header">System requirements</h3>
						<div class="section resizable">
							<p>
								We recommend using W3C-compliant browsers such as <i>Google Chrome™</i>, <i>Mozilla Firefox™</i>, or <i>Apple Safari™</i>. <i>Internet Explorer™</i> users shall be aware that <strong>versions lower than - or equal to - 
								Internet Explorer™ 7 are <b>not</b> officially supported</strong>. Additionally, a screen resolution of at least 1280 x 1024 pixels is recommended.
							</p>
						</div>
					</div>
					<div class="sectionContainer ui-widget-content">
						<h3 class="ui-widget-header">Contacts</h3>
						<div class="section resizable">
							<p>
								Please direct inquiries, error reporting, or any other matters regarding the FVF to the  
								<a href="mailto:<%=adminEMail%>?subject=Fishing%20Vessels%20Finder%20inquiry" class="default" title="Send an e-mail to <%=adminEMail%>">Fishing Vessels Finder Administrators</a>.
							</p>
						</div>
					</div>
				</div>
				<div id="actions">
					<%String[][] searchOptions = new String[][] { 
						new String[] { "stats", "Statistics driven", "default" },
						new String[] { "quick", "Quick", null },
						new String[] { "basic", "Basic", null },
						new String[] { "advanced", "Advanced", null } 
					};%>
					<label class="{ position: { my: 'center bottom', at: 'center top' } }" title="Select a search mode. You could bookmark these links to directly access the corresponding search functionality and skip this introductory text.">
						Select a search mode:
					</label>
					<span class="buttonSet uCase smallButtons">
					<%
						boolean isDefaultSearchType = false;
						String URL = null;

						for(String[] option : searchOptions) {
							isDefaultSearchType = "default".equalsIgnoreCase(option[2]);
							URL = baseHREF + ( specificSource == null ? "": specificSource.getId() + "/" ) + "search/" + langPath + "#" + option[0];
					%>
						<a href="<%=URL%>"
						   class="widthMin100 { position: { my: 'center bottom', at: 'center top' }, buttonOptions: { icons: { secondary: 'ui-icon-search'<%=isDefaultSearchType ? ", primary: 'ui-icon-check'" : "" %> } } }"
						   title="Enter the '<%=option[1]%>' search panel.<%=isDefaultSearchType ? " This is the default search mode." : ""%>">
						   <%=option[0]%>
						</a> 
					<%}%>
					</span>
				</div>
			</div>
		</div>

		<!-- Login -->
		<div id="loginDialog" class="hidden">
			<%@include file="components/login.jspf" %>
		</div>

		<!-- Footer -->
		<%@include file="components/footer.jspf"%>

		<!-- Credits -->
		<div id="applicationCredits" class="hidden">
			<%@include file="components/credits.jspf"%>
		</div>
		
		<!-- Browser list -->
		<%@include file="components/browserList.jspf"%>
		
		<%@include file="components/externalLibraries.jspf"%>

		<script type="text/javascript" src="libs/<%if(trackRequest){%>tracking.js|<%}%>defaults.js|login.js|externalReferences.js|interface.common.js|_sourcesReport.js"></script>
	</body>
</html>