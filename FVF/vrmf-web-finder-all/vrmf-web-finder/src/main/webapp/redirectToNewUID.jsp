<%@page contentType="text/html; charset=UTF-8" isErrorPage="false" trimDirectiveWhitespaces="true"%>
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@include file="components/commonInitialization.jspf"%>
<% 
	CustomBundleManager $NEWUID = new CustomBundleManager("i18n/finder/web/server", new Locale(languageWithFallback), new String[] {
		"other/redirectToNewUID"
	});

	Integer redirectionDelay = 10;
	String staleUID = (String)request.getAttribute(DisplayVesselByIDRedirectorConstants.VRMF_STALE_UID_REQUEST_ATTRIBUTE);
	String newUID = (String)request.getAttribute(DisplayVesselByIDRedirectorConstants.VRMF_NEW_UID_REQUEST_ATTRIBUTE);

	breadcrumbs.add(new BasicPair<String, Pair<String, String>>($NEWUID.getText("vrmf.other.newuid.breadcrumb", staleUID), new BasicPair<String, String>(requestedURI, $NEWUID.getText("vrmf.other.newuid.breadcrumb.tip", staleUID))));
%>
<html xmlns="http://www.w3.org/1999/xhtml" lang="<%=languageWithFallback%>" xml:lang="<%=languageWithFallback%>" dir="<%=isRTL ? "rtl" : "ltr"%>" class="<%=isRTL ? "rtl" : "ltr"%>">
	<head> 
		<base href="<%=baseHREF%>"/>
 
		<title><%=$NEWUID.getText("vrmf.other.newuid.title", staleUID, newUID, defaultTitle)%></title>

		<meta name="description" content="<%=defaultDescription%>"/>
		<meta name="keywords" content="<%=CollectionsHelper.join(keywords, ", ")%>"/>
		<meta name="author" content="<%=metaAuthor%>"/>
		<meta name="generator" content="FAO VRMF engine<%=specificSource == null ? "" : " for " + specificSourceID + " vessels"%>"/>
		<meta http-equiv="content-language" content="<%=languageWithFallback%>"/>

		<meta http-equiv="refresh" content="<%=redirectionDelay%>; url=<%=ServletsHelper.localizeURI(specificSourceID + "/!/display/vessel/UID/" + newUID, language)%>"/>

		<%@include file="components/commonStyles.jspf"%>
		<%@include file="components/commonLibraries.jspf"%>
		<%@include file="components/commonScripts.jspf"%>

		<link type="image/png" rel="icon" href="../cdn/media/images/icons/default/warning.png"/>

		<link id="baseCSS" class="additionalStyles" rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css"/>
		<link id="themeCSS" rel="stylesheet" type="text/css" href="<%=URLHelper.unescape(themeCSS)%>"/>

		<link rel="stylesheet" type="text/css" href="../cdn/styles/base/all.css"/>
		<link rel="stylesheet" type="text/css" href="styles/default.css|login.css|redirectToNewUID.css"/>

		<!-- Damn' that IE thing... -->
		<link rel="stylesheet" type="text/css" href="styles/default.ie.css" />
		<link rel="stylesheet" type="text/css" href="../cdn/styles/default.ie.css" />
	</head>
	<body class="ui-helper-clearfix">

		<!-- Header -->
		<%@include file="components/header.jspf"%>

		<div id="padder" class="ui-widget-content borderless removeBackground">&#160;</div>

		<!-- Main content -->
		<div id="mainContainer" class="ui-helper-clearfix ui-widget-content">
			<h1 class="ui-state-highlight">
				<%=$NEWUID.getText("vrmf.other.newuid.report.header", staleUID)%>
			</h1>
			<h2>
				<%=$NEWUID.getText("vrmf.other.newuid.report.1")%><br/>
				<%=$NEWUID.getText("vrmf.other.newuid.report.2", newUID, redirectionDelay)%><br/>
			</h2>
				
			<h3>
				<%=$NEWUID.getText("vrmf.other.newuid.report.3.1")%>&#160;
				<a class="default" href="<%=specificSource == null ? "" : ( specificSourceID + "/" )%>!/display/vessel/UID/<%=newUID%>"><%=$NEWUID.getText("vrmf.other.newuid.report.3.2")%></a>&#160;
				<%=$NEWUID.getText("vrmf.other.newuid.report.3.3")%>
			</h3>
		</div>

		<!-- Login -->
		<div id="loginDialog" class="hidden">
			<%@include file="components/login.jspf" %>
		</div>

		<!-- Footer -->
		<%@include file="components/footer.jspf"%>

		<!-- Credits -->
		<div id="applicationCredits" class="hidden">
			<%@include file="components/credits.jspf"%>
		</div>

		<!-- Browser list -->
		<%@include file="components/browserList.jspf"%>

		<%@include file="components/externalLibraries.jspf"%>

		<script type="text/javascript" src="libs/<%if(trackRequest){%>tracking.js|<%}%>defaults.js|
																					   login.js|
																					   interface.common.js|
																					   _redirectToNewUID.js">
		</script>
	</body>
</html>