<%@page contentType="text/html; charset=UTF-8" isErrorPage="false" trimDirectiveWhitespaces="true"%>
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page import="org.fao.fi.vrmf.common.models.extended.ExtendedEntity"%>
<%@page import="org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToShipbuilder"%>
<%@page import="org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToFlags"%>
<%@page import="org.fao.fi.vrmf.common.models.extended.vessels.ExtendedVesselToRegistration"%>
<%@page import="org.fao.fi.vrmf.common.models.generated.VesselsToFishingLicense"%>
<%@page import="org.fao.fi.vrmf.common.models.generated.VesselsToExternalMarkings"%>
<%@page import="org.fao.fi.vrmf.common.models.generated.VesselsToMmsi"%>
<%@page import="org.fao.fi.vrmf.common.models.generated.VesselsToCallsigns"%>
<%@page import="org.fao.fi.vrmf.common.models.generated.VesselsToName"%>
<%@page import="org.fao.fi.vrmf.common.models.generated.VesselsToIdentifiers"%>
<%@page import="org.fao.fi.vrmf.common.models.extended.FullVessel"%> 
<%@page import="org.fao.fi.vrmf.web.apps.finder.controllers.services.pub.redirect.DisplayVesselByIDRedirectorConstants"%>
 
<%@include file="components/commonInitialization.jspf"%>

<%
	CustomBundleManager $DISPLAY = new CustomBundleManager("i18n/common/web/server", new Locale(languageWithFallback), new String[] {
		"vessel/common", 
		"vessel/current",
		"vessel/historical",
		"vessel/timeline",
		"vessel/pictures",
		"vessel/tracks"
	}); 

	CustomBundleManager $FVF_DISPLAY = new CustomBundleManager("i18n/finder/web/server", new Locale(languageWithFallback), new String[] {
		"vessel/common", 
		"vessel/current",
		"vessel/historical",
		"vessel/timeline",
		"vessel/pictures",
		"vessel/tracks"
	}); 

	isSearchPage = false;

	Boolean groupByUID = Boolean.TRUE.equals((Boolean)request.getAttribute(DisplayVesselByIDRedirectorConstants.VRMF_GROUP_BY_UID_REQUEST_ATTRIBUTE));
	String groupBy = groupByUID ? "UID" : "ID"; 
	String ID = request.getParameter(DisplayVesselByIDRedirectorConstants.ID_REQUEST_PARAMETER);
	String formattedID = ID == null ? "" : ID;

	while(formattedID.length() < 9)
		formattedID = "0" + formattedID;

	NameValuePair identificationCriteria = (NameValuePair)request.getAttribute(DisplayVesselByIDRedirectorConstants.VRMF_VESSELS_REDIRECT_BY_ID_CRITERIA_REQUEST_ATTRIBUTE);

	Boolean RELOAD = Boolean.parseBoolean(request.getParameter(DisplayVesselByIDRedirectorConstants.RELOAD_REQUEST_PARAMETER));
	FullVessel VRMF_VESSEL_DATA = (FullVessel)request.getAttribute(DisplayVesselByIDRedirectorConstants.VRMF_VESSEL_DATA_REQUEST_ATTRIBUTE);

	String formattedUID = "" + VRMF_VESSEL_DATA.getUid();

	while(formattedUID.length() < 9)
		formattedUID = "0" + formattedUID;

	String[] allAvailableSources = VRMF_VESSEL_DATA.getAllAvailableSourceSystemsForVessel() != null ? VRMF_VESSEL_DATA.getAllAvailableSourceSystemsForVessel().toArray(new String[0]) : null; 
	 
	String currentSource = specificSourceID != null ? specificSourceID : !groupByUID ? VRMF_VESSEL_DATA.getSourceSystem() : null;

	boolean hasOtherSources = ( currentSource != null || !groupByUID ) && 
	  							allAvailableSources != null &&
	  							allAvailableSources.length > 1;

	String VRMF_SERIALIZED_VESSEL_DATA = RELOAD ? null : (String)request.getAttribute(DisplayVesselByIDRedirectorConstants.VRMF_SERIALIZED_VESSEL_DATA_REQUEST_ATTRIBUTE);

	String title = "";
	String label = "";

	String serializedVesselKeywordsAndLabels = VRMF_VESSEL_DATA == null ? null : StringsHelper.trim(VRMF_VESSEL_DATA.getSerializedVesselKeywords(true));
	String serializedVesselKeywords = VRMF_VESSEL_DATA == null ? null : StringsHelper.trim(VRMF_VESSEL_DATA.getSerializedVesselKeywords(false));

	String vesselName = null;
	String vesselFlag = null;
	String vesselType = null;
	
	if(VRMF_VESSEL_DATA != null) {
		String simplifiedVesselName = null;
		
		if(VRMF_VESSEL_DATA.getNameData() != null && !VRMF_VESSEL_DATA.getNameData().isEmpty()) {
			vesselName = VRMF_VESSEL_DATA.getNameData().get(0).getName();
			simplifiedVesselName = VRMF_VESSEL_DATA.getNameData().get(0).getSimplifiedName();
		}
		
		if(VRMF_VESSEL_DATA.getFlags() != null && !VRMF_VESSEL_DATA.getFlags().isEmpty()) {
			vesselFlag = VRMF_VESSEL_DATA.getFlags().get(0).getVesselFlag().getName();
		}
		
		if(VRMF_VESSEL_DATA.getTypes() != null && !VRMF_VESSEL_DATA.getTypes().isEmpty()) {
			vesselType = VRMF_VESSEL_DATA.getTypes().get(0).getVesselType().getName();
		}
	
		if(vesselName != null && simplifiedVesselName != null) {
			title += vesselName + "";
		
			if(simplifiedVesselName != null && !simplifiedVesselName.equalsIgnoreCase(vesselName)) {
				title += " (" + simplifiedVesselName + ")";
			}
		} else {
			title += "< " + $DISPLAY.getText("vrmf.vessel.common.title.name.unknown") + " >";
		}
		
		title += currentSource == null ? "" : " @ " + currentSource;
		
		Collection<String> additionalData = new ArrayList<String>();
		
		if(vesselFlag != null) {
			additionalData.add(vesselFlag);
		}
		
		if(vesselType != null) {
			additionalData.add(vesselType);
		}
		
		if(!additionalData.isEmpty())
			title += " [ " + CollectionsHelper.join(additionalData, ", ") + " ]";
		
		label = title;
		
		title += " | " + $FVF_DISPLAY.getText("vessel.common.title.details") + " ";
	
		if(specificSourceID != null) 
			title += specificSourceID + " ";
	
		title += $DISPLAY.getText("vrmf.vessel") + " ";
	
		if(identificationCriteria != null) {
			title += $FVF_DISPLAY.getText("vessel.common.title.criteria.other", 
										  identificationCriteria.getName(), 
										  identificationCriteria.getValue(),
										  groupBy,
										  formattedID);
		} else {
			title += $FVF_DISPLAY.getText("vessel.common.title.criteria.vrmf", 
										  groupBy,
										  formattedID);
		}
	
		title = title.replaceAll("\"", "\\\"");
	}

	String allKeywordsAndLabels = ( serializedVesselKeywords == null ? "" : serializedVesselKeywordsAndLabels + ", " ) + CollectionsHelper.join(keywords, ", ");
	String allKeywords = ( serializedVesselKeywords == null ? "" : serializedVesselKeywords + ", " ) + CollectionsHelper.join(keywords, ", ");

	String description = title + ", " + allKeywordsAndLabels;
	description = description.substring(0, Math.min(252, description.length()) - 1) + ( description.length() > 252 ? "..." : ""); //See: http://en.wikipedia.org/wiki/Meta_element#The_description_attribute

	String vid = label;
	
	breadcrumbs.add(new BasicPair<String, Pair<String, String>>($FVF_DISPLAY.getText("vessel.common.breadcrumb.search"), new BasicPair<String, String>(ServletsHelper.localizeURI(baseHREF + ( specificSourceID == null ? "" : specificSourceID + "/" ) + "search/", language), $FVF_DISPLAY.getText("vessel.common.breadcrumb.search.tip"))));
	breadcrumbs.add(new BasicPair<String, Pair<String, String>>(vid, new BasicPair<String, String>(request.getRequestURI(), $FVF_DISPLAY.getText("vessel.common.breadcrumb.search.vessel.tip", vid))));
%>
<html xmlns="http://www.w3.org/1999/xhtml" lang="<%=languageWithFallback%>" xml:lang="<%=languageWithFallback%>" dir="<%=isRTL ? "rtl" : "ltr"%>" class="<%=isRTL ? "rtl" : "ltr"%>">
	<head>
		<base href="<%=baseHREF%>"/>

		<title><%=title%></title>
		<meta name="description" content="<%=description.replaceAll("\\\"", "&quot;")%>"/>
		<meta name="keywords" content="<%=allKeywords.replaceAll("\\\"", "&quot;")%>"/>
		<meta name="author" content="<%=metaAuthor%>"/>
		<meta name="generator" content="FAO VRMF engine<%=specificSource == null ? "" : ( " for " + specificSourceID + " vessels" )%>"/>
		<meta http-equiv="content-language" content="<%=languageWithFallback%>"/>

		<%@include file="components/commonStyles.jspf"%>
		<%@include file="components/commonLibraries.jspf"%>
		<%@include file="components/commonScripts.jspf"%>

		<%-- 'Canonical' link --%>
		<%
			if(currentSource != null && !hasOtherSources) {
		%>
		<link rel="canonical" href="!/display/vessel/<%=groupBy%>/<%=formattedID%><%=langPath == null ? "" : "/" + langPath%>"/>
		<%
			}
		%>

		<link id="baseCSS" class="additionalStyles" rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css"/>

		<link id="themeCSS" rel="stylesheet" type="text/css" href="<%=URLHelper.unescape(themeCSS)%>"/>

		<link rel="stylesheet" type="text/css" href="../cdn/styles/themes/jquery/jquery.multiselect.css|jquery.multiselect.filter.css"/>

		<!-- <link rel="stylesheet" type="text/css" href="../cdn/styles/themes/jquery/jquery.qtip.min.css"/ -->

		<link rel="stylesheet" type="text/css" href="../cdn/styles/base/all.css"/>
		<link rel="stylesheet" type="text/css" href="../cdn/styles/vessels/display.css"/>

		<link rel="stylesheet" type="text/css" href="styles/default.css|login.css|search.css"/>

		<!-- Damn' that IE thing... -->
		<link rel="stylesheet" type="text/css" href="styles/default.ie.css" />
		<link rel="stylesheet" type="text/css" href="../cdn/styles/default.ie.css" />
		<link rel="stylesheet" type="text/css" href="../cdn/styles/vessels/display.ie.css" />
		
		<link rel="stylesheet" type="text/css" href="styles/print.css" media="print" />
		
 		<script type="text/javascript">
 		google.load("maps", "3.0", { other_params: "sensor=false" });
 		</script>
	</head>
	<body class="lockOnStartup">
		<!-- Header -->
		<%@include file="components/header.jspf"%>

		<div id="padder" class="ui-widget-content borderless removeBackground">&#160;</div>

		<!-- Main content -->
		<div id="mainContainer" class="hidden">
			<!-- Combined vessel data -->
			<div id="vesselsData" class="standalone panel ui-widget-content borderless removeBackground hidden">
				<%@include file="components/combinedVesselData.jspf"%>
			</div>

			<%if(serializedVesselKeywords != null) {%>
			<div id="keywords" class="ui-state-highlight ui-helper-clearfix">
				<span><b>Raw data:&#160;&#160;</b> <code class="selectable">{ <%=serializedVesselKeywordsAndLabels%> }</code></span>
			</div>
			<%}%>
		</div>

		<!-- Login dialog -->
		<div id="loginDialog" class="hidden">
			<%@include file="components/login.jspf" %>
		</div>

		<!-- Footer -->
		<%@include file="components/footer.jspf"%>

		<!-- Credits -->
		<div id="applicationCredits" class="hidden">
			<%@include file="components/credits.jspf"%>
		</div>
		
		<!-- Browser list -->
		<%@include file="components/browserList.jspf"%>

		<%@include file="components/externalLibraries.jspf"%>

		<script type="text/javascript" src="../cdn/libs/other/jquery/jquery.scrollTo-1.4.5.min.js|
																		jquery.lazyload.min.js"></script>
		<script type="text/javascript" src="../cdn/libs/other/utils/arboreal.js"></script>
		<script type="text/javascript" src="../cdn/libs/framework/vessels/attributeFormatters.js|
																			 newExternalReferences.js|
																			 externalReferences.builders.js|
																			 externalReferences.builders.custom.js|
																			 externalReferences.preprocessors.js|
																			 externalReferences.postprocessors.js|
																			 externalReferences.urls.js|
																			 filters.js|
																			 formatters.js|
																			 labelFormatters.js|
																			 picturesManager.js|
																			 positionsManager.js|
																			 infringementsManager.js|
																			 vesselDetailsDisplay.js|
																			 vesselTimelineDisplay.js">
		</script>

		<script type="text/javascript" src="libs/<%if(trackRequest) {%>tracking.js|<%}%>defaults.js|
																						login.js|
																						externalReferences.js|
																						configuration.js|
																						interface.common.js|
																						interface.display.vessel.js|
																						vesselDetailsDisplayOverride.js|
																						displayBasicVesselDetails.js|
																						displayVesselDetails.js|
																						_displayVessel.js">
		</script>

		<!-- VRMF JS common bundles -->
		<script type="text/javascript" src="../cdn/libs/framework/dynamic/i18n/localize.js?lang=<%=i18nLang%>&bundles=vessel/attributeFormatters,
																														 vessel/display,
																														 vessel/externalReferences,
																														 vessel/formatters,
																														 vessel/labelFormatters,
																														 vessel/picturesManager,
																														 vessel/positionsManager">
		</script>

		<!-- FVF JS common bundles -->
		<script type="text/javascript" src="libs/dynamic/i18n/localize.js?lang=<%=i18nLang%>&bundles=vessels/displayVessel,vessels/displayResults">
		</script>

		<script type="text/javascript">
			var ID = -1;

			<%-- Embedded vessel data --%>
			var VRMF_VESSEL_DATA = <%=VRMF_SERIALIZED_VESSEL_DATA%>;

			$(function() {
				try {
					ID = parseInt('<%=ID%>');

					var callback = function() {
						updateHeaderAccordingToEntryPoint(false);

						if($$.isSet(ID) && ID > 0) {
							if(VRMF_VESSEL_DATA == null)
								$$.Interface.error("No data is currently available for vessel with <%=groupBy%> #<%=ID%>. Either no vessel do exist with the given <%=groupBy%>, or you are not authorized to browse its details.");
							else {
								showVesselDetails(<%=groupByUID%>, ID);
							}
						}
					}

					initializeDisplayInterface(callback);
				} catch (E) {
					$$.Interface.error("Wrong <%=groupBy%> value '<%=ID%>'");
				}
			});
		</script>
	</body>
</html>