<%@page contentType="text/html; charset=UTF-8" isErrorPage="false" trimDirectiveWhitespaces="true"%>
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@include file="components/commonInitialization.jspf"%>
<%-- ============================================================= 
	 Temporarily left to enhance handling of current Google Search 
	 displayed breadcrumbs. Does a simple redirect to the proper 
	 Search URL 
     ============================================================= --%>
<%response.sendRedirect(ServletsHelper.localizeURI(baseHREF + ( specificSourceID == null ? "" : specificSourceID + "/" ) + "search/", language));%>