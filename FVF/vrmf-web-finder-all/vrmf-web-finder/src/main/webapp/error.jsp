<%@page contentType="text/html; charset=UTF-8" isErrorPage="true" trimDirectiveWhitespaces="true"%>
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page import="java.io.Writer"%>
<%@page import="java.io.StringWriter"%>
<%@page import="java.io.PrintWriter"%>
<%@include file="components/commonInitialization.jspf"%>
<%
	CustomBundleManager $ERROR = new CustomBundleManager("i18n/finder/web/server", new Locale(languageWithFallback), new String[] {
		"other/error"
	});

	isErrorPage = true;

	Integer statusCode = (Integer)request.getAttribute("javax.servlet.error.status_code");
	String statusDescription = null;

	try {
		if(statusCode != null) {
			statusDescription = HttpStatus.valueOf(statusCode).getReasonPhrase();
		}
	} catch (Throwable t) {
		switch(statusCode) {
			case 509:
				statusDescription = $ERROR.getText("vrmf.error.description.509");
				break;
			default:
				statusDescription = $ERROR.getText("vrmf.error.description.unspecified");
		}
	}

	String errorMessage = StringsHelper.trim((String)request.getAttribute("javax.servlet.error.message"));
	Class<?> exceptionType = (Class<?>)request.getAttribute("javax.servlet.error.exception_type");
	Throwable thrownException = (Throwable)request.getAttribute("javax.servlet.error.exception");
	String requestURI = (String)request.getAttribute("javax.servlet.error.request_uri");
%>
<html xmlns="http://www.w3.org/1999/xhtml" lang="<%=languageWithFallback%>" xml:lang="<%=languageWithFallback%>" dir="<%=isRTL ? "rtl" : "ltr"%>" class="<%=isRTL ? "rtl" : "ltr"%>">
	<head> 
		<base href="<%=baseHREF%>"/>

		<title><%=statusCode == null ? $ERROR.getText("vrmf.error.unknown") : String.valueOf(statusCode) + " " + statusDescription%> | <%=defaultTitle%></title>

		<meta name="description" content="<%=defaultDescription%>"/>
		<meta name="keywords" content="<%=CollectionsHelper.join(keywords, ", ")%>"/>
		<meta name="author" content="<%=metaAuthor%>"/>
		<meta name="generator" content="FAO VRMF engine<%=specificSource == null ? "" : " for " + specificSourceID + " vessels"%>"/>
		<meta http-equiv="content-language" content="<%=languageWithFallback%>"/>
 
		<link type="image/png" rel="icon" href="../cdn/media/images/icons/default/error.png"/>

		<link id="baseCSS" class="additionalStyles" rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css"/>
		<link id="themeCSS" rel="stylesheet" type="text/css" href="<%=URLHelper.unescape(themeCSS)%>"/>
		
		<link rel="stylesheet" type="text/css" href="../cdn/styles/base/all.css"/>
		<link rel="stylesheet" type="text/css" href="styles/default.css|login.css|error.css"/>

		<%@include file="/components/commonStyles.jspf"%>
		<%@include file="components/commonLibraries.jspf"%>
		<%@include file="components/commonScripts.jspf"%>

		<!-- Damn' that IE thing... -->
		<link rel="stylesheet" type="text/css" href="../cdn/styles/default.ie.css" />
		<link rel="stylesheet" type="text/css" href="styles/default.ie.css" />
	</head>
	<body class="ui-helper-clearfix">

		<!-- Header -->
		<%@include file="components/header.jspf"%>

		<div id="padder" class="ui-widget-content borderless removeBackground">&#160;</div>

		<!-- Main content -->
		<div id="mainContainer" class="hidden ui-helper-clearfix ui-widget-content">
			<div id="errorDetails">
				<%if(statusCode != null) {%>
				<div class="ui-state-error">
					<h1>
						<a class="newWin simplest" href="http://en.wikipedia.org/wiki/Titanic">
							<img src="media/images/decorations/misc/titanic.jpg" class="errorImage ui-state-error"></img>
							<%--<img src="http://upload.wikimedia.org/wikipedia/commons/7/7f/.titanic..jpeg" class="errorImage ui-state-error"></img> --%>
						</a>
					<%=$ERROR.getText("vrmf.error")%>&#160;<a href="http://httpstatus.es/<%=statusCode%>" class="externalStatic simplest" title="<%=$ERROR.getText("vrmf.error.description.display.tip")%>"><%=statusCode%> (<%=statusDescription%>)</a>&#160;<%=$ERROR.getText("vrmf.error.description.display.uri")%>&#160;<code class="selectableOnHover selectableOnClick"><%=requestURI%></code>
					</h1>
				</div>
				<%}%>
				<h2><%=$ERROR.getText("vrmf.error.message")%></h2>
				<div class="errorMessage ui-state-highlight noShadow selectableOnHover">
					<code>
						<%=ServletsHelper.sanitizeHTML(errorMessage == null ? $ERROR.getText("vrmf.error.message.not.set") : errorMessage)%>
					</code>
				</div>
				<%if(exceptionType != null) {%>
				<h2><%=$ERROR.getText("vrmf.error.message.type")%>&#160;<textarea><%=exceptionType.getClass().getName()%></textarea></h2>
				<h2><%=$ERROR.getText("vrmf.error.message.text")%></h2> 
				<pre><%=thrownException.getMessage()%></pre>
				<h2><%=$ERROR.getText("vrmf.error.message.trace")%></h2>
				<%
					Writer stackTraceSerializer = new StringWriter();
					PrintWriter printWriter = new PrintWriter(stackTraceSerializer);
					thrownException.printStackTrace(printWriter);
				%>
				<textarea id="stackTrace">
				<%=ServletsHelper.sanitizeHTML(stackTraceSerializer.toString(), true)%>
				</textarea>
				<%}%>
			</div>
		</div>

		<!-- Login dialog -->
		<div id="loginDialog" class="hidden">
			<%@include file="components/login.jspf" %>
		</div>

		<!-- Footer -->
		<%@include file="components/footer.jspf"%>

		<!-- Credits -->
		<div id="applicationCredits" class="hidden">
			<%@include file="components/credits.jspf"%>
		</div>
		
		<!-- Browser list -->
		<%@include file="components/browserList.jspf"%>

		<%@include file="components/externalLibraries.jspf"%>

		<script type="text/javascript" src="libs/<%if(trackRequest){%>tracking.js|<%}%>defaults.js|
																					   login.js|
																					   interface.common.js|
																					   _error.js">
		</script>
	</body>
</html>