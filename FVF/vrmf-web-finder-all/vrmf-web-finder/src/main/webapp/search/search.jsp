<%@page contentType="text/html; charset=UTF-8" isErrorPage="false" trimDirectiveWhitespaces="true"%>
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@include file="/components/commonInitialization.jspf"%>
<%
	CustomBundleManager $SEARCH = new CustomBundleManager("i18n/finder/web/server", new Locale(languageWithFallback), new String[] {
		"search/common", 
		"search/stats", 
		"search/quick", 
		"search/basic", 
		"search/advanced",
		"search/results"
	});

	CustomBundleManager $DISPLAY = new CustomBundleManager("i18n/common/web/server", new Locale(languageWithFallback), new String[] {
		"vessel/common", 
		"vessel/current",
		"vessel/historical",
		"vessel/timeline",
		"vessel/pictures",
		"vessel/tracks"
	}); 

	breadcrumbs.add(new BasicPair<String, Pair<String, String>>($SEARCH.getText("search.common.header.breadcrumbs.search"), new BasicPair<String, String>(ServletsHelper.localizeURI(requestedURI, language), $SEARCH.getText("search.common.header.breadcrumbs.search.tip"))));

	//These declarations left to avoid compilation problems when including the combined vessel data page fragment... :(
	String formattedUID = "";
	String[] allAvailableSources = new String[0]; 
	boolean hasOtherSources = false;
	String currentSource = null;
	String groupBy = null;
%>
<html xmlns="http://www.w3.org/1999/xhtml" lang="<%=languageWithFallback%>" xml:lang="<%=languageWithFallback%>" dir="<%=isRTL ? "rtl" : "ltr"%>" class="<%=isRTL ? "rtl" : "ltr"%>">
	<head> 
		<base href="<%=baseHREF%>"/>

		<title><%=defaultTitle%></title>

		<meta name="description" content="<%=defaultDescription%>"/>
		<meta name="keywords" content="<%=CollectionsHelper.join(keywords, ", ")%>"/>
		<meta name="author" content="<%=metaAuthor%>"/>
		<meta name="generator" content="FAO VRMF engine<%=specificSource == null ? "" : ( " for " + specificSourceID + " vessels" )%>"/>
		<meta http-equiv="content-language" content="<%=languageWithFallback%>"/>

		<link id="baseCSS" class="additionalStyles" rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css"/>
		<link id="themeCSS" rel="stylesheet" type="text/css" href="<%=URLHelper.unescape(themeCSS)%>"/>

		<link rel="stylesheet" type="text/css" href="../cdn/styles/themes/jquery/jquery.multiselect.css|
																				    jquery.multiselect.filter.css"/>

		<link rel="stylesheet" type="text/css" href="../cdn/styles/base/all.css"/>
		<link rel="stylesheet" type="text/css" href="../cdn/styles/vessels/display.css"/>

		<link rel="stylesheet" type="text/css" href="styles/default.css|search.css|login.css"/>

		<%@include file="/components/commonStyles.jspf"%>
		<%@include file="/components/commonLibraries.jspf"%>
		<%@include file="/components/commonScripts.jspf"%>

 		<script type="text/javascript">
	 		google.load("visualization", "1.0", { "packages": [ "geochart", "corechart", "table" ]});
	 		google.load("maps", "3.0", { other_params: "sensor=false" });
 		</script>
 
		<!-- Damn' that IE thing... -->
		<link rel="stylesheet" type="text/css" href="styles/default.ie.css|search.ie.css" />
		<link rel="stylesheet" type="text/css" href="../cdn/styles/default.ie.css" />
		<link rel="stylesheet" type="text/css" href="../cdn/styles/vessels/display.ie.css" />
	</head>
	
	<body class="lockOnStartup ui-helper-clearfix">
		
		<!-- Header -->
		<%@include file="/components/header.jspf"%>

		<div id="padder" class="ui-widget-content borderless removeBackground">&#160;</div>

		<!-- Main container -->
		<div id="mainContainer" class="hidden ui-helper-clearfix">
			<div id="searchCriteria" class="ui-widget-content borderless removeBackground">
				<div class="actionsTop ui-state-highlight ui-corner-all { position: { my: 'bottom center', at: 'top center' } }">
					<div class="actionsHeader">
						<label><%=$SEARCH.getText("search.common.available.types")%></label>
					</div>
					<div>
						<span class="buttonSet smallButtons uCase">
							<label for="selectLiveStatsSearch" title="<%=$SEARCH.getText("search.common.type.stats.tip")%>" class="widthMin64"><%=$SEARCH.getText("search.common.type.stats")%></label>
							<input id="selectLiveStatsSearch" name="searchType" type="radio" value="stats" checked="checked"/>
							<label for="selectLiveSearch" title="<%=$SEARCH.getText("search.common.type.quick.tip")%>" class="widthMin64"><%=$SEARCH.getText("search.common.type.quick")%></label>
							<input id="selectLiveSearch" name="searchType" type="radio" value="quick"/>
							<label for="selectNormalSearch" title="<%=$SEARCH.getText("search.common.type.basic.tip")%>" class="widthMin64"><%=$SEARCH.getText("search.common.type.basic")%></label>
							<input id="selectNormalSearch" name="searchType" type="radio" value="basic"/>
							<label for="selectAdvancedSearch" title="<%=$SEARCH.getText("search.common.type.advanced.tip")%>" class="widthMin64"><%=$SEARCH.getText("search.common.type.advanced")%></label>
							<input id="selectAdvancedSearch" name="searchType" type="radio" value="advanced"/>
						</span>
					</div> 
				</div>
				<%@include file="/components/quickSearchFilters.jspf"%>
				<%@include file="/components/statsSearchFilters.jspf"%>
				<%@include file="/components/searchFilters.jspf"%>
			</div> 

			<!-- Search results panel -->
			<div id="searchResults" class="empty panel ui-widget-content borderless removeBackground">
				<%@include file="/components/searchResults.jspf"%>
			</div>

			<!-- Combined vessel details panel -->
			<div id="vesselDetails" class="panel ui-widget-content borderless removeBackground hidden">
				<%@include file="/components/participantsVesselData.jspf"%>
			</div>

			<!-- Vessel data panel -->
			<div id="vesselsData" class="panel ui-widget-content borderless removeBackground hidden">
				<%@include file="/components/combinedVesselData.jspf"%>
			</div>

			<!-- Process tracker -->
			<div id="processTracker" class="hidden ui-helper-clearfix">
				<div><h3><label>STATUS: </label><span id="processStatus"><%=$SEARCH.getText("search.common.wait.please")%></span></h3></div>
				<div id="processProgress"></div>
			</div>
		</div>

		<!-- Login dialog -->
		<div id="loginDialog" class="hidden">
			<%@include file="/components/login.jspf" %>
		</div>

		<!-- Footer -->
		<%@include file="/components/footer.jspf"%>

		<!-- Credits -->
		<div id="applicationCredits" class="hidden">
			<%@include file="/components/credits.jspf"%>
		</div>

		<!-- Browser list -->
		<%@include file="/components/browserList.jspf"%>

		<%@include file="/components/externalLibraries.jspf"%>

		<script type="text/javascript" src="../cdn/libs/other/jquery/jquery-ui-autocomplete.html_extension.js|
																		jquery.multiselect.1.14.min.js|
																		jquery.multiselect.1.14.filter.min.js|
																		jquery.multiselect.1.14.patch.min.js|
																		jquery.sortElements.js|
																		jquery.scrollTo-1.4.5.min.js|
																		jquery.lazyload.min.js"></script>

		<%if(languageWithFallback != null && !"en".equals(languageWithFallback)) {%>
		<script type="text/javascript" src="../cdn/libs/other/jquery/localized/<%=languageWithFallback%>/jquery.multiselect.js|
																											jquery.multiselect.filter.js"></script>
		<%}%>

		<script type="text/javascript" src="../cdn/libs/other/utils/arboreal.js"></script>
		<script type="text/javascript" src="../cdn/libs/framework/vessels/attributeFormatters.js|
																			 newExternalReferences.js|
																			 externalReferences.builders.js|
																			 externalReferences.builders.custom.js|
																			 externalReferences.preprocessors.js|
																			 externalReferences.postprocessors.js|
																			 externalReferences.urls.js|
																			 filters.js|
																			 formatters.js|
																			 labelFormatters.js|
																			 picturesManager.js|
																			 positionsManager.js|
																			 infringementsManager.js|
																			 vesselDetailsDisplay.js|
																			 vesselTimelineDisplay.js"></script>
																			 
		<script type="text/javascript" src="libs/<%if(trackRequest){%>tracking.js|<%}%>defaults.js|
																					   login.js|
																					   externalReferences.js|
																					   configuration.js|
																					   interface.common.js|
																					   interface.search.stats.js|
																					   interface.search.quick.js|
																					   interface.search.controls.autocompletion.support.js|
																					   interface.search.controls.autocompletion.js|
																					   interface.search.controls.multiselect.support.js|
																					   interface.search.controls.multiselect.js|
																					   interface.search.controls.js|
																					   interface.search.js|
																					   interface.display.vessel.js|
																					   interface.js|
																					   _search.js|
																					   vesselDetailsDisplayOverride.js|
																					   displayBasicVesselDetails.js|
																					   displayVesselDetails.js">
		</script>
		
		<!-- VRMF JS common bundles -->
		<script type="text/javascript" src="../cdn/libs/framework/dynamic/i18n/localize.js?lang=<%=i18nLang%>&bundles=common/geo,
																														 vessel/attributeFormatters,
																														 vessel/display,
																														 vessel/externalReferences,
																														 vessel/formatters,
																														 vessel/labelFormatters,
																														 vessel/picturesManager,
																														 vessel/positionsManager">
		</script>
		
		<!-- FVF JS common bundles -->
		<script type="text/javascript" src="libs/dynamic/i18n/localize.js?lang=<%=i18nLang%>&bundles=search/common,
																									 search/controls,
																									 search/quick,
																									 search/stats,
																									 vessels/displayResults,
																									 vessels/displayVessel">
		</script>
	</body>
</html>