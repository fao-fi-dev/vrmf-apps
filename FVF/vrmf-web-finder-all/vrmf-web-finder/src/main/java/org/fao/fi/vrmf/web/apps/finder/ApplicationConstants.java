/**
 * (c) 2010-2015 FIPS / FAO of the UN (project: vrmf-web-finder)
 */
package org.fao.fi.vrmf.web.apps.finder;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 14 Nov 2012   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 14 Nov 2012
 */
public interface ApplicationConstants {
//	String RELEASE_VERSION = "1.03";
//	String RELEASE_NAME    = "Actinia Cari";
	
//	String RELEASE_VERSION = "1.10";
//	String RELEASE_NAME    = "Alepisaurus Ferox";

//	String RELEASE_VERSION = "1.2";
//	String RELEASE_NAME    = "Babylonia Areolata";
	
	String RELEASE_VERSION = "1.3";
	String RELEASE_NAME    = "Acanthurus chirurgus";
}