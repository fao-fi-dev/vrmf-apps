/**
 * (c) 2016 FAO / UN (project: vrmf-web-finder)
 */
package org.fao.fi.vrmf.web.apps.finder;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;

import org.fao.fi.vrmf.common.search.dsl.impl.Column;
import org.fao.fi.vrmf.common.search.dsl.impl.common.meta.CommonDataColumns;
import org.fao.fi.vrmf.common.search.dsl.impl.vessels.meta.VesselRecordDataColumns;
import org.fao.fi.vrmf.common.web.DataCustomizer;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * Jan 21, 2016   Fabio     Creation.
 *
 * @version 1.0
 * @since Jan 21, 2016
 */
@Named @Singleton
public class BasicDataCustomizer implements DataCustomizer {
	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.web.apps.finder.DataCustomizer#getTargetTable(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public String getTargetTable(HttpServletRequest request) {
		return DataCustomizer.DEFAULT_VESSELS_TABLE;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.vrmf.web.apps.finder.DataCustomizer#getDataColumns(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public Column[] getDataColumns(HttpServletRequest request) {
		return new Column[] {
			CommonDataColumns.ID,
			CommonDataColumns.UID,
			VesselRecordDataColumns.FLAG,
			VesselRecordDataColumns.NAME,
			VesselRecordDataColumns.EU_CFR,
			VesselRecordDataColumns.IMO,
			VesselRecordDataColumns.GEAR_TYPE,
			VesselRecordDataColumns.CALLSIGN,
			VesselRecordDataColumns.LOA,
			VesselRecordDataColumns.GT,
			CommonDataColumns.NUM_SOURCES,
			CommonDataColumns.SOURCE
		};
	}

}
